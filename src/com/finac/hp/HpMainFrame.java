/*
 *  FinacMainFrame.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 20, 2014, 10:25:38 AM
 *  Copyrights Channa Mohan, All rights reserved.
 *  
 */
package com.finac.hp;

import com.finac.loan.*;
import com.mac.zresources.FinacResources;
import com.mac.af.core.environment.mainframe.DefaultMainframe;
import static com.mac.zresources.FinacResources.*;
import com.mac.zsystem.settings.theme_chooser.Theme;
import com.mac.af.resources.ApplicationResources;
import com.mac.zreport.ReportUtil;
import com.mac.zreport.object.ReportBand;
import com.mac.zreport.object.ReportFile;
import com.mac.zreport.object.ReportTask;
import java.util.List;

/**
 *
 * @author mohan
 */
public class HpMainFrame extends DefaultMainframe {

    @Override
    protected void createGUI() {
        createRegistration();
//        createHigherPurchase();
//        createLand();
        createLoan();
        createCashier();
        createAccount();
        createReports2();
        //createReports();
        createSystem();
        createApplicationMenu();
    }

    private void createRegistration() {
        addTask("Registration");

        addBand("People", FinacResources.getImageIconURL(REGISTRATION_PEOPLE));
        startGroup();
        addButton("Client", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.client.REGClient.class, null);
//        addButton("Supplier", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.supplier.REGSupplier.class, null);
        addButton("Client Approval", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.client.ClientApproval.class, null);
        addButton("Loan Group", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.loan_group.REGLoanGroup.class, null);
        addButton("Client Information", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.client.ClientInformation.class, null);
        addButton("Route", FinacResources.getImageIconURL(REGISTRATION_ROUTE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.route.REGRoute.class, null);
        startGroup();
        addButton("Employee", FinacResources.getImageIconURL(REGISTRATION_EMPLOYEE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.employee.REGEmployee.class, null);

        addBand("Company", FinacResources.getImageIconURL(REGISTRATION_COMPANY));
        addButton("Company", FinacResources.getImageIconURL(REGISTRATION_COMPANY), DefaultMainframe.ElementPriority.TOP, com.mac.registration.company.REGCompany.class, null);
        addButton("Company Branch", FinacResources.getImageIconURL(REGISTRATION_BRANCH), DefaultMainframe.ElementPriority.TOP, com.mac.registration.branch.REGBranch.class, null);
        addButton("Leave Day", FinacResources.getImageIconURL(REGISTRATION_LEAVE_DAY), DefaultMainframe.ElementPriority.TOP, com.mac.registration.company_leave_day.REGCompanyLeaveDay.class, null);

        addBand("Loan", FinacResources.getImageIconURL(REGISTRATION_LOAN));
        addButton("Loan Type", FinacResources.getImageIconURL(REGISTRATION_LOAN_TYPE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.loan_type.REGLoanType.class, null);

        addBand("Cashier", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT));
        startGroup();
        addButton("Cashier Point", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.cashier_point.REGCashier.class, null);

    }

    private void createHigherPurchase() {
        addTask("Higher Purchase");

        addBand("Registration", FinacResources.getImageIconURL(LOAN));
        addButton("Item", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.registration.item.REGItem.class, null);
        addButton("Item Category", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.registration.item_category.REGItemCategory.class, null);
        addButton("Item Department", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.registration.item_department.REGItemDepartment.class, null);


        addBand("Transactions", FinacResources.getImageIconURL(LOAN));
        addButton("Sales Invoice", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.item.sales_item.REGSalesItem.class, null);
    }

    //TEST LAND
    private void createLand() {
        addTask("Land");

        addBand("Registration", FinacResources.getImageIconURL(LOAN));
        addButton("Land", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.land.land_purchase.REGLandPurchase.class, null);
        addButton("Land Project", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.land.land_project.REGProject.class, null);
        addButton("Capital Expenditure", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.land.capital_expenditure.REGCapitalExpenditure.class, null);
        addButton("Block Creation", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.land.block_creation.REGBlockCreation.class, null);

        addBand("Transactions", FinacResources.getImageIconURL(LOAN));
        addButton("Land Reservation", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.item.sales_item.REGSalesItem.class, null);
    }

    private void createLoan() {
        addTask("Loan");

        addBand("Loan Afford", FinacResources.getImageIconURL(LOAN));
        startGroup();
        addButton("Loan Application", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_application.TRALoanApplication.class, null);
        addButton("Opening Loan", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.loan.opening_loan.TRAOpeningLoanApplication.class, null);
//        addButton("HP Loan Application", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.loan.hp_loan_application.TRALoanApplication.class, null);
        addButton("Application Edit", ApplicationResources.getResource(ApplicationResources.ACTION_EDIT), DefaultMainframe.ElementPriority.LOW, null, null);
        addButton("Application Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null);
        startGroup();
        addButton("Loan Application Approval", FinacResources.getImageIconURL(LOAN_APPROVAL), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_approval.LoanApplicationApproval.class, null);
        startGroup();
        addButton("Loan", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan.TRALoan.class, null);
        addButton("Started Loan Approval", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_approval.StartedLoanApproval.class, null);
        addButton("Loan Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan.TRALoanCancel.class, null);
        addButton("Loan Closing", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan_close.REGLoanClose.class, null);
        startGroup();
        addButton("Voucher", FinacResources.getImageIconURL(LOAN_VOUCHER), DefaultMainframe.ElementPriority.TOP, com.mac.loan.voucher.Voucher.class, null);
        addButton("Voucher Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.voucher.VoucherCancel.class, null);

        addBand("Loan Transaction", FinacResources.getImageIconURL(LOAN));
        startGroup();
        addButton("Receipt", FinacResources.getImageIconURL(LOAN_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.temparary_receipt_receive.TempararyReceiptReceive.class, null);
        addButton("Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.receipt.ReceiptCancel.class, null);
        startGroup();
        addButton("Loan Charges", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), ElementPriority.TOP, com.mac.loan.loan_charges.LoanCharges.class, null);
        addButton("Loan Charges Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null);
//        addButton("Client Rebit", FinacResources.getImageIconURL(LOAN_REBIT), ElementPriority.TOP, com.mac.loan.rebit.ClientRebit.class, null);
//        addButton("Rebits Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null);
//        addButton("Rebit Cancel", FinacResources.getImageIconURL(REBIT_CANCEL), DefaultMainframe.ElementPriority.LOW, com.mac.loan.rebit.TRARebitClose.class, null);

//        addBand("Finalize", FinacResources.getImageIconURL(LOAN));
//        startGroup();
//        addButton("Loan Close", FinacResources.getImageIconURL(LOAN_CLOSE), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_close.LoanClose.class, null);
//        addButton("Loan Close Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null);
        startGroup();
        addButton("Day End", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), ElementPriority.TOP, com.mac.zsystem.transaction.day_end.SystemDayEnd.class, null);
//        addButton("Opening Loan", FinacResources.getImageIconURL(LOAN_OPENING), DefaultMainframe.ElementPriority.TOP, null, null);
    }

    private void createCashier() {
        addTask("Cashier");
        addBand("Cashier", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));

//        startGroup();
//        addButton("Voucher", FinacResources.getImageIconURL(LOAN_VOUCHER), DefaultMainframe.ElementPriority.TOP, com.mac.loan.voucher.Voucher.class, null);
//        addButton("Voucher Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.voucher.VoucherCancel.class, null);

        startGroup();
        addButton("Temperary Receipt", FinacResources.getImageIconURL(LOAN_TEMPERERY_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.temperary_receipt.TemperaryReceipt.class, null);
        addButton("Temperary Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null);

        startGroup();
        addButton("Receipt", FinacResources.getImageIconURL(LOAN_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.receipt.Receipt.class, null);
        addButton("Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.receipt.ReceiptCancel.class, null);

        startGroup();
        addButton("Cashier Close", FinacResources.getImageIconURL(CASHIER_CLOSE), ElementPriority.TOP, com.mac.account.cashier_closing.CashierClosing.class, null);
    }

    private void createAccount() {
        addTask("Account");
        addBand("Registration", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));

        addBand("Account Registration", FinacResources.getImageIconURL(REGISTRATION_ACCOUNT));
        addButton("Account Category", FinacResources.getImageIconURL(REGISTRATION_ACCOUNT_CATEGORY), DefaultMainframe.ElementPriority.LOW, com.mac.registration.account_category.REGAccountCategory.class, null);
        addButton("Account", FinacResources.getImageIconURL(REGISTRATION_ACCOUNT), DefaultMainframe.ElementPriority.LOW, com.mac.registration.account.REGAccount.class, null);

        addBand("Account Transaction", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        startGroup();
        addButton("Opening Balance", FinacResources.getImageIconURL(LOAN_OPENING_BALANCE), ElementPriority.TOP, com.mac.account.opening_balance.OpeningBalance.class, null);
        startGroup();
        addButton("Bank Deposit", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.bank_deposit.BankDeposit.class, null);
        startGroup();
        addButton("General Receipt", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.general_receipt.GeneralReceipt.class, null);
        addButton("General Voucher", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.general_voucher.GeneralVoucher.class, null);
        addButton("General Voucher 2", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.general_voucher_2.GeneralVoucher.class, null);

        startGroup();
        addButton("Journal", FinacResources.getImageIconURL(LOAN_JOURNAL), ElementPriority.TOP, com.mac.account.journal.Journel.class, null);

        addBand("Cheque Handling", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        startGroup();
        addButton("Cheque Deposit", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), ElementPriority.LOW, com.mac.account.cheque_deposit.ChequeDeposit.class, null);
        addButton("Cheque Realize", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), ElementPriority.LOW, com.mac.account.cheque_realize.ChequeRealize.class, null);
        addButton("Cheque Return", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), ElementPriority.LOW, com.mac.account.cheque_return.ChequeReturn.class, null);
        startGroup();
        addButton("Cheque Issue", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), ElementPriority.LOW, com.mac.account.cheque_issue.ChequeIssue.class, null);
        startGroup();
        addButton("Bank", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.LOW, com.mac.registration.bank.REGBank.class, null);
        addButton("Branch", FinacResources.getImageIconURL(REGISTRATION_BANK_BRANCH), ElementPriority.LOW, com.mac.registration.bank_branch.REGBanckBranch.class, null);
        addButton("Bank Account", FinacResources.getImageIconURL(REGISTRATION_BANK_BRANCH), ElementPriority.LOW, com.mac.registration.bank_account.REGBankAccount.class, null);
    }

    private void createSystem() {
        addTask("System");
        addBand("Transaction Settings", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        startGroup();
        addButton("Account Settings", FinacResources.getImageIconURL(ACCOUNTT_SETTING), ElementPriority.TOP, com.mac.zsystem.transaction.account.account_setting.gui.SystemAccountSettings.class, null);
        addButton("Payment Settings", FinacResources.getImageIconURL(PAYMENT_SETTING), ElementPriority.TOP, com.mac.zsystem.transaction.payment.payment_setting.gui.SystemPaymentSettings.class, null);

        addBand("Application Settings", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        startGroup();
        addButton("Configuration", FinacResources.getImageIconURL(SETTING), ElementPriority.TOP, com.mac.zsystem.settings.settings.Settings.class, null);
        addButton("Theme Selector", FinacResources.getImageIconURL(APPLICATION_MENU_THEME), ElementPriority.TOP, com.mac.zsystem.settings.theme_chooser.Theme.class, null);

        addBand("Other Settings", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
//        addButton("Remind Letter Templates", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.registration.remind_letter.REGTemplateRegistration.class, null);
//        addButton("Remind Letter Generator", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zutil.remind_letter.LetterGenerator.class, null);

        addBand("Dash Boards", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addButton("Recovery Dash Board", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.dash_board.recovery.RecoveryDashBoard.class, null);
        addButton("Client Information", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.dash_board.client.ClientInformationDashBoard.class, null);

        addBand("Permission", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addButton("User Role", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zsystem.permission.user_role.REGUserRole.class, null);
        addButton("Permission", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zsystem.permission.permission.REGPermission.class, null);
        addButton("User Role Permission", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zsystem.permission.user_role_permission.REGUserRolePermission.class, null);
        addButton("Backup", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.af.templates.system.backup.BackupRestore.class, null);
        
        addButton("Report Registration", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.report_registration.REGReportRegistration.class, null);
        addButton("Report Permission", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.report_permission.REGUserRoleReportPermission.class, null);
    }

    private void createReports2() {
        addTask("Report");
        addBand("Reports", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addButton("Loan Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.LoanReports.class, null);
        addButton("Account Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.AccountReports.class, null);
        addButton("Cahier Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.CashierReports.class, null);
        addButton("Administration Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.AdministrationReports.class, null);

    }

    private void createApplicationMenu() {
        startApplicationMenu();
        addPrimaryMenu("Theme Selector", FinacResources.getImageIconURL(APPLICATION_MENU_THEME), Theme.class, null);
        addPrimaryMenu("Configuration", FinacResources.getImageIconURL(APPLICATION_MENU_CONFIG), Theme.class, null);
        addPrimaryMenu("Cache Manager", FinacResources.getImageIconURL(APPLICATION_MENU_CACHE_MANAGER), com.mac.af.core.database.hibernate.ui.cache_manager.CacheManager.class, null);
        addPrimaryMenu("Task Manager", FinacResources.getImageIconURL(APPLICATION_MENU_TASK_MANAGER), Theme.class, null);
    }

    private void createReports() {
        List<ReportTask> reportTasks = ReportUtil.getReportTasks();

        for (ReportTask reportTask : reportTasks) {
            addTask(ReportUtil.getFormattedString(reportTask.getName()));
            for (ReportBand reportBand : reportTask.getReportBands()) {
                addBand(ReportUtil.getFormattedString(reportBand.getName()), FinacResources.getImageIconURL(FinacResources.REPORT));
                for (ReportFile reportFile : reportBand.getReportFiles()) {
                    addButton(ReportUtil.getFormattedString(reportFile.getName()), FinacResources.getImageIconURL(REPORT), ElementPriority.LOW, com.mac.zreport.ReportPanel.class, reportFile.getReportFile());
                }
            }
        }
//        for (ReportTask reportTask : reportTasks) {
//            addTask(ReportUtil.getFormattedString(reportTask.getName()));
//            for (ReportBand reportBand : reportTask.getReportBands()) {
//                addBand(reportBand.getName(), FinacResources.getImageIconURL(FinacResources.REPORT));
//                for (ReportFile reportFile : reportBand.getReportFiles()) {
//                    addButton(reportFile.getName(), FinacResources.getImageIconURL(REPORT), ElementPriority.LOW, com.mac.zsystem.report.ReportPanel.class, reportFile.getReportFile());
//                }
//            }
//        }
    }
}