/*
 *  FileBugOut.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 16, 2014, 11:55:34 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.finac.loan;

import com.mac.af.component.base.button.action.Action;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author mohan
 */
public class FileBugOut {

    public static void initFileBugOut() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd@HH.mm");
            String fileName = dateFormat.format(new Date()) + ".txt";

            File file = new File(fileName);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            PrintStream printStream = new PrintStream(fileOutputStream);

            System.setOut(printStream);
            System.setErr(printStream);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    private static  void method() throws NullPointerException{
        String s = null;

        assert s != null;
        
        if (s==null) {
            throw new NullPointerException("string should be not-null");
        }
    }
    
    public static void main(String[] args) {
//        FileBugOut.initFileBugOut();

        try {
            method();
        } catch (NullPointerException e) {
        }
        
        String s = null;

        assert s != null;
        
        if (s==null) {
            throw new NullPointerException("string should be not-null");
        }

        System.out.println("hi");
    }
}
