/*
 * Finac.java
 * 
 * @author channa mohan
 *     hjchanna@gmail.com
 * 
 * Created on Apr 2, 2013
 * Copyright Channa Mohan, All rights reserved.
 * 
 */
package com.finac.loan;

import com.mac.registration.branch.object.Branch;
import com.mac.registration.employee.object.Employee;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.SettlementType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Frame;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.CSplashScreen;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import static com.mac.af.core.environment.CApplication.STORE_ID;
import static com.mac.af.core.environment.CApplication.USER_ID;
import static com.mac.af.core.environment.CApplication.WORKING_DATE;
import static com.mac.af.core.environment.CApplication.addSessionVariable;
import com.mac.af.core.environment.mainframe.DefaultMainframe;
import com.mac.af.core.setting.SettingException;
import com.mac.af.core.setting.framework.Framework;
import com.mac.af.core.setting.framework.store.Store;
import com.mac.af.core.setting.gui.Settings;
import com.mac.af.core.message.mOptionPane;
import com.mac.registration.client.ImageWriter;
import com.mac.registration.employee.object.Permission;
import com.mac.registration.employee.object.Photo;
import com.mac.registration.employee.object.UserRole;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.transaction.object.SystemTable;
import com.mac.zsystem.transaction.transaction.object.TransactionType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Mechatrons
 */
public class Finac extends CApplication {

    private static String[] args;
    private static String xSystemVersion = "5.1";
    Authentication authentication;
    private boolean CHECKED = false;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] arg) {
//        FileBugOut.initFileBugOut();
        args = arg;
        launch(new Finac());
    }

    @Override
    public void Restart() {
        CApplication.getInstance().getMainFrame().dispose();
        main(args);
    }

    @Override
    public boolean isAllowedTab(Class c) {
        String cl = c.getName();

        boolean permissionIncluded = allPermissionTabs.contains(cl);
        boolean hasPermission = allowedPermissionTabs.contains(cl);
        return permissionIncluded ? hasPermission : true;
    }

    @Override
    protected Frame getApplicationMainFrame() {
        initDatabaseService();
//        String checkFinacType = new SystemStatusCheck().checkFinacType(
//                databaseService,
//                SystemSettingsStatus.HP_LOAN_SYSTEM);
//        
//        if(checkFinacType.equals(SystemSettingsStatus.HP))

//        mainframe = new HPMainFrame();
//        }else{
        mainframe = new FinacMainFrame();
//        }
        return mainframe;
    }

    @Override
    public String getApplicationName() {
//        return "Finac ";
        return "Finac ( " + getBranch().getName() + " )";
    }

    @Override
    public String getApplicationVersion() {
        return "5.1";
    }

    @Override
    protected void initialize() {
        super.initialize();

        try {
            CApplication.checkFrameworkVersion("1.0");
        } catch (Exception e) {
            e.printStackTrace();
            try {
                Thread.sleep(100000);
                System.exit(0);
            } catch (InterruptedException ex) {
                Logger.getLogger(CApplication.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    protected void startup() {

        Log("Connecting to the database...");
        initDatabaseService();
        //INITIALIZE DEFAULT SETTINGS

        //CHECKING MAC ADDRESS
        Log("Check system registration...");
        System.out.println("DeveloperLogin.getMacAddress()__" + DeveloperLogin.getMacAddress());
        System.out.println("DeveloperLogin.getPCNAME()__" + DeveloperLogin.getPCNAME());
        authentication = new Authentication(databaseService, DeveloperLogin.getMacAddress(), DeveloperLogin.getPCNAME());
//        try {
//            if(authentication.getAthentication()==false)
//            {
//          
//                 mOptionPane.showMessageDialog(null, "The system not register  your machine  \n Please Contact System Administrator"
//                                        , "System failure", mOptionPane.WARNING_MESSAGE);
//                System.exit(0);
//            }
//        } catch (DatabaseException ex) {
//            Logger.getLogger(Finac.class.getName()).log(Level.SEVERE, null, ex);
//        }
        Log("Checking registration settings...");
        Branch b = getBranch();
        String version = b.getCompany().getVersion();


        if (!xSystemVersion.equals(version)) {
            mOptionPane.showMessageDialog(null, "The Exists System  Version ( " + version + " ) Is Invalied \n Please Contact System Administrator", "System Version " + xSystemVersion, mOptionPane.WARNING_MESSAGE);
            System.exit(0);
        }
        // SYSTEM EXPIRATION 
        //check active system  use `system_table`  DT    2016-02-01       1       
        Log("Checking default settings...");
        try {
            //SYSTEM TRANSACTION SETTINGS
            Log("Checking default settings...");
            List<TransactionType> systemTransactionSettings = SystemTransactions.getSystemTransactionTypes();
            List<TransactionType> databaseTransactionSettings = databaseService.getCollection(TransactionType.class);
            boolean foundTransactionSetting;
            databaseService.beginLocalTransaction();
            for (TransactionType systemTrasactionSetting : systemTransactionSettings) {
                foundTransactionSetting = false;
                for (TransactionType databaseTransactionSetting : databaseTransactionSettings) {
                    if (systemTrasactionSetting.getCode().equals(databaseTransactionSetting.getCode())) {
                        foundTransactionSetting = true;
                    }
                }

                if (!foundTransactionSetting) {
                    databaseService.save(systemTrasactionSetting);
                }
            }
            databaseService.commitLocalTransaction();

            //SYSTEM SETTLEMENT SETTINGS
            List<SettlementType> systemSettlementSettings = SystemSettlement.getSystemSettlementTypes();
            List<SettlementType> databaseSettlementSettings = databaseService.getCollection(SettlementType.class);
            boolean foundSettlementSetting;
            databaseService.beginLocalTransaction();
            for (SettlementType systemPaymentSetting : systemSettlementSettings) {
                foundSettlementSetting = false;
                for (SettlementType databasePaymentSetting : databaseSettlementSettings) {
                    if (systemPaymentSetting.getCode().equals(databasePaymentSetting.getCode())) {
                        foundSettlementSetting = true;
                    }
                }

                if (!foundSettlementSetting) {
                    databaseService.save(systemPaymentSetting);
                }
            }
            databaseService.commitLocalTransaction();

            //SYSTEM ACCOUNT SETTINGS
            List<AccountInterface> systemAccountInterfaces = SystemAccountTransaction.getAccountInterfaces();
            List<com.mac.zsystem.transaction.account.object.TransactionType> databaseTransactionType = databaseService.getCollection(com.mac.zsystem.transaction.account.object.TransactionType.class);
            boolean foundAccountSetting;

            //check account interface and transaction type
            for (AccountInterface accountInterface : systemAccountInterfaces) {
                for (com.mac.zsystem.transaction.account.object.TransactionType transactionType : databaseTransactionType) {

                    if (accountInterface.getTransactionTypeCode().equals(transactionType.getCode())) {
                        foundAccountSetting = false;

                        //check account transaction setting
                        for (AccountSetting accountSetting : accountInterface.getAccountSettings()) {
                            for (AccountSetting transaction : transactionType.getAccountSettings()) {
                                if (accountSetting.getCode().equals(transaction.getCode())) {
                                    foundAccountSetting = true;
                                }
                            }

                            if (!foundAccountSetting) {
                                accountSetting.setTransactionType(transactionType);
                                databaseService.save(accountSetting);
                            }
                        }

                    }

                }
            }

            //SYSTEM PAYMENT SETTINGS
            List<PaymentInterface> systemPaymentInterfaces = SystemPayment.getPaymentInterfaces();
            List<com.mac.zsystem.transaction.payment.object.TransactionType> databasePaymentTransactionType = databaseService.getCollection(com.mac.zsystem.transaction.payment.object.TransactionType.class);
            boolean foundPaymentSetting;

            //check account interface and transaction type
            for (PaymentInterface paymentInterface : systemPaymentInterfaces) {
                for (com.mac.zsystem.transaction.payment.object.TransactionType transactionType : databasePaymentTransactionType) {

                    if (paymentInterface.getTransactionTypeCode().equals(transactionType.getCode())) {
                        foundPaymentSetting = false;

                        //check account transaction setting
                        for (PaymentSetting paymentSetting : paymentInterface.getPaymentSettings()) {
                            for (PaymentSetting transaction : transactionType.getPaymentSettings()) {
                                if (paymentSetting.getCode().equals(transaction.getCode())) {
                                    foundPaymentSetting = true;
                                }
                            }

                            if (!foundPaymentSetting) {
                                paymentSetting.setTransactionType(transactionType);
                                databaseService.save(paymentSetting);
                            }
                        }

                    }

                }
            }

// new update
            try {
                List<SystemTable> systemTableList = getsystempermition();

                for (SystemTable systemTable : systemTableList) {
                    if (systemTable.getCode().equals("DT")) {
                        boolean check = systemTable.getDate().equals(b.getWorkingDate());

                        if (check == true) {
                            mOptionPane.showMessageDialog(null, "The System  has expired You cant access the system \n For more details : Call to system Administrator !", "System Expired", mOptionPane.WARNING_MESSAGE);
                            System.exit(0);
                        }
                    } else if (systemTable.getCode().equals("DY")) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(b.getWorkingDate());
                        int month = cal.get(Calendar.DAY_OF_MONTH);

                        if (month == systemTable.getDay()) {
                            mOptionPane.showMessageDialog(null, "The System  has expired You cant access the system \n For more details : Call to system Administrator !", "System Expired", mOptionPane.WARNING_MESSAGE);
                            System.exit(0);
                        }
                    }

                }


                //-----------------------------------------------------

                String varstatus = "false";
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();

                systemexpiration systemEX = new systemexpiration();


                if (systemEX.createxml() == true) {

                    List<SystemTable> systemTableList2 = getsystempermition();

                    for (SystemTable systemTable : systemTableList2) {
                        if (systemTable.getCode().equals("DT")) {
                            System.out.println("systemTable.getDate()__" + systemTable.getDate());
                            System.out.println("dateFormat.format(date)__" + dateFormat.format(date));
                            boolean check = dateFormat.format(systemTable.getDate()).equals(dateFormat.format(date));


                            if (check == true) {
                                varstatus = "true";
                                systemEX.writeStatus(b.getCode(), varstatus, dateFormat.format(date));
                            }

                        }
                    }

                    if (systemEX.getStatus(b.getCode()) == true) {
                        mOptionPane.showMessageDialog(null, "The System  has expired You cant access the system \n For more details : Call to system Administrator !", "System Expired", mOptionPane.WARNING_MESSAGE);
                        System.exit(0);
                    }

                }


            } catch (Exception e) {
                e.printStackTrace();

            }






        } catch (DatabaseException ex) {
            Logger.getLogger(Finac.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean authenticate(String user, String password, Object store) throws Exception {
        initDatabaseService();

        // 7CDD907E5336 DC4A3E6B0388
        System.out.println("DC4A3E6B0388 "+DeveloperLogin.getMacAddress());
        System.out.println(DeveloperLogin.getMacAddress().equals("DC4A3E6B0388"));
        if (DeveloperLogin.getMacAddress().equals("DC4A3E6B0388") || DeveloperLogin.getMacAddress().equals("")) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("USER_NAME", "Admin");//"Admin"
            params.put("PASSWORD", "admin");//"admin123"
            List<Employee> list = databaseService.getCollection("from com.mac.registration.employee.object.Employee where active=true and user_name=:USER_NAME and password=:PASSWORD", params);
            Branch branch = getBranch();
            boolean permission = false;
            if (list.size() > 0) {
                Employee employee = list.get(0);
                permission = employee.getLoginAccess() == null ? false : employee.getLoginAccess();

                if (permission) {
                    UserRole userRole = employee.getUserRole();
                    BRANCH_PREFIX_KEY = branch.getPrefix();
                    //session variables
                    addSessionVariable(USER_ID, employee.getCode());
                    addSessionVariable(STORE_ID, branch.getCode());
                    addSessionVariable(WORKING_DATE, branch.getWorkingDate());
                    addSessionVariable(USER_PREFIX_KEY, employee.getPrefix());
                    addSessionVariable(USER_ROLE, userRole.getCode());

                    //permission
                    allowedTabs = new ArrayList<>();

                    if (userRole != null) {
                        for (Permission permission2 : userRole.getPermissions()) {
                            allowedTabs.add(permission2.getName());
                        }
                    }
                    allowedPermissionTabs = new ArrayList<>();
                    allowedPermissionTabs_equal = new ArrayList<>();
                    if (userRole != null) {
                        for (Permission permission1 : userRole.getPermissions()) {
                            allowedPermissionTabs.add(permission1.getClass_());
                        }
                    }
                    if (userRole != null) {
                        for (Permission permission2 : userRole.getPermissions()) {
                            allowedPermissionTabs_equal.add(permission2.getName());
                        }
                    }

                    allPermissionTabs = new ArrayList<>();
                    List<Permission> permissions = databaseService.getCollection(Permission.class);
                    for (Permission permission1 : permissions) {
                        allPermissionTabs.add(permission1.getClass_());
                    }
                    //side panel
                    createSidePanel(employee, branch);
                }
            }
            return permission;
        } else {

            HashMap<String, Object> params = new HashMap<>();
            params.put("USER_NAME", user);
            params.put("PASSWORD", password);
            List<Employee> list = databaseService.getCollection("from com.mac.registration.employee.object.Employee where active=true and user_name=:USER_NAME and password=:PASSWORD", params);
            Branch branch = getBranch();
            boolean permission = false;
            if (list.size() > 0) {
                Employee employee = list.get(0);
                permission = employee.getLoginAccess() == null ? false : employee.getLoginAccess();

                if (permission) {
                    UserRole userRole = employee.getUserRole();
                    BRANCH_PREFIX_KEY = branch.getPrefix();
                    //session variables
                    addSessionVariable(USER_ID, employee.getCode());
                    addSessionVariable(STORE_ID, branch.getCode());
                    addSessionVariable(WORKING_DATE, branch.getWorkingDate());
                    addSessionVariable(USER_PREFIX_KEY, employee.getPrefix());
                    addSessionVariable(USER_ROLE, userRole.getCode());
                    //permission
                    allowedTabs = new ArrayList<>();

                    if (userRole != null) {
                        for (Permission permission2 : userRole.getPermissions()) {
                            allowedTabs.add(permission2.getName());
                        }
                    }
                    allowedPermissionTabs = new ArrayList<>();
                    allowedPermissionTabs_equal = new ArrayList<>();
                    if (userRole != null) {
                        for (Permission permission1 : userRole.getPermissions()) {
                            allowedPermissionTabs.add(permission1.getClass_());
                        }
                    }
                    if (userRole != null) {
                        for (Permission permission2 : userRole.getPermissions()) {
                            allowedPermissionTabs_equal.add(permission2.getName());
                        }
                    }
                    allPermissionTabs = new ArrayList<>();
                    List<Permission> permissions = databaseService.getCollection(Permission.class);
                    for (Permission permission1 : permissions) {
                        allPermissionTabs.add(permission1.getClass_());
                    }
                    //side panel
                    createSidePanel(employee, branch);
                }
            }


            return permission;
        }
    }

    @Override
    public String getAuthenticationInformation() {
        Branch branch = getBranch();
        StringBuilder builder = new StringBuilder();
        builder.append("<HTML>");

        //application information
        builder.append("<DIV STYLE='text-align: center; font-size: 18px;'>");
        builder.append("<B>");
        builder.append(CApplication.getInstance().getApplicationName())
                .append(" - v")
                .append(CApplication.getInstance().getApplicationVersion());
        builder.append("</B>");
        builder.append("</DIV>");

        //company information
        builder.append(branch.getCompany() != null ? "<B>Company:</B><BR/>" + branch.getCompany().getName() + "</B>" : "");
        builder.append("<BR/>");
        builder.append("<BR/>");
        builder.append("<B>Branch:</B>");
        builder.append("<BR/>");
        builder.append(branch.getName());
        builder.append("<BR/>");
        builder.append("<BR/>");
        builder.append("<B><U>");
        builder.append("Working Date: ");
        builder.append(branch.getWorkingDate());
        builder.append("</B></U>");

        //ending
        builder.append("</HTML>");

        return builder.toString();
    }

    private void createSidePanel(Employee employee, Branch branch) throws IOException {
        Photo photo = employee.getPhoto();
        byte[] imageData;
        if (photo != null) {
            imageData = photo.getPhoto();
        } else {
            imageData = null;
//            imageData = extractBytes(employee.getEmail());
//            System.out.println("imageData__"+imageData);
        }
        SidePanel sidePanel = new SidePanel(employee.getName(), branch.getWorkingDate(), imageData);
        mainframe.setSideBar(sidePanel);
    }
    //-------------------------------facebook image-------------------------

// public byte[] extractBytes (String  userID) throws IOException {
//     // open image
////      URL url = new URL("https://graph.facebook.com/" + userID + "/picture");
//      URL url = new URL("http://graph.facebook.com/67563683055/picture?type=small");
////     Image ImageName = getFacebookProfilePicture(fbName);
////     File imgPath = new File(ImageName);
// 
//     BufferedImage bufferedImage = ImageIO.read(url.openStream());
// System.out.println("bufferedImage_______"+bufferedImage);
//     // get DataBufferBytes from Raster
////     WritableRaster raster = bufferedImage .getRaster();
////     DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();
//     
//         ByteArrayOutputStream baos = new ByteArrayOutputStream();
//    ImageIO.write( bufferedImage, "jpg", baos );
//    baos.flush();
//    byte[] imageInByte = baos.toByteArray();
//    baos.close();
//
//     return ( imageInByte );
//}
    private Branch getBranch() {
        initDatabaseService();
        Branch branch = null;
        try {
            //getting info from database
            Store store = ((Framework) CApplication.getInstance().getSettingRoot().getSetting(Framework.class)).getStore();
            //read from db 
            branch = (Branch) databaseService.getObject(Branch.class, store.getStore().getValue());
            if (branch == null) {
                int q = mOptionPane.showOptionDialog(null, "Store cannot find by the code '" + store.getStore().getValue() + "', \n What do you wish to do ?", "No Branch Found", mOptionPane.YES_NO_OPTION, mOptionPane.ERROR_MESSAGE, null, new String[]{"Edit Settings", "Shut Down"}, "Edit Settings");

                switch (q) {
                    case mOptionPane.YES_OPTION:
                        Settings.viewSettingsPanel(store);
                        return getBranch();
                    case mOptionPane.NO_OPTION:
                        CApplication.getInstance().Shutdown();
                }
            } else {
                Date date = branch.getWorkingDate();
                if (date == null) {
                    int q = mOptionPane.showOptionDialog(null, "Not recognized working date, \n What do you wish to do ?", "Working date", mOptionPane.YES_NO_OPTION, mOptionPane.ERROR_MESSAGE, null, new String[]{"Set Working Date", "Shut Down"}, "Set Working Date");

                    switch (q) {
                        case mOptionPane.YES_OPTION:
                            String wDate = mOptionPane.showInputDialog(null, "Please enter the correct working date in pattern YYYY-MM-DD. \n WORNING: If you are not sure the working date, please contact your system administrator.", "Working Date", mOptionPane.WARNING_MESSAGE);
                            if (wDate == null) {
                                CApplication.getInstance().Shutdown();
                            } else {
                                DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    System.out.println(wDate);
                                    Date workingDate = format.parse(wDate);
                                    branch.setWorkingDate(workingDate);
                                    databaseService.save(branch);
                                } catch (ParseException ex) {
                                    mOptionPane.showMessageDialog(null, "You entered working date is not recognized. System will shutdown.", "Working Date", mOptionPane.WARNING_MESSAGE);
                                    CApplication.getInstance().Shutdown();
                                }
                            }
                            return getBranch();
                        case mOptionPane.NO_OPTION:
                            CApplication.getInstance().Shutdown();
                    }
                }
                if (branch.getCompany() != null ? branch.getCompany().getVersion() == null : false) {
                    if (!CApplication.getInstance().getApplicationVersion().equals(branch.getCompany().getVersion())) {
                        mOptionPane.showMessageDialog(null, "You are using a different version of the software. \nPlease install latest update.", "Version Mismatch", mOptionPane.ERROR_MESSAGE);
                        CApplication.getInstance().Shutdown();
                    }
                }
            }

        } catch (SettingException | DatabaseException ex) {
            Logger.getLogger(Finac.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (branch == null) {
            return getBranch();
        } else {
            //this.branch = branch;
            return branch;
        }
    }

    public List<SystemTable> getsystempermition() throws DatabaseException {
        List<SystemTable> list = databaseService.getCollection("from com.mac.zsystem.transaction.transaction.object.SystemTable where status=1");
        return list;
    }

    private void initDatabaseService() {

        if (databaseService == null) {
            databaseService = new HibernateDatabaseService(CPanel.GLOBAL);
        }
        new ImageWriter().writeConfigurationSettings();
    }
    private HibernateDatabaseService databaseService;

    @Override
    protected CSplashScreen getSplashScreen() {
        return new FinacSplashScreenNew();
    }
    //
    public static final String USER_PREFIX_KEY = "USER_PREFIX";
    public static final String USER_ROLE = "USER_ROLE";
    public static String BRANCH_PREFIX_KEY;
    private DefaultMainframe mainframe;
    private List<String> allowedPermissionTabs;
    private List<String> allowedPermissionTabs_equal;
    private List<String> allowedTabs;
    private List<String> allPermissionTabs;

    @Override
    public boolean isAllowedTab(String title) {
        for (String allowedPermission : allowedPermissionTabs_equal) {
            System.out.println("allowedPermission: "+allowedPermission+" -title "+title);
            if (title.equals(allowedPermission)) {
                return true;
//            }
//        }
//       return true;
            }
        }
        return false;
    }
}
