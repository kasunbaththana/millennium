/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finac.loan;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NIMESH-PC
 */
public class DeveloperLogin {

    
    //7CDD907F32C2
    public static String getMacAddress() {
        try {
            InetAddress address;
            address = InetAddress.getLocalHost();
            NetworkInterface interface1 = NetworkInterface.getByInetAddress(address);
            byte[] mac = interface1.getHardwareAddress();

            StringBuilder builder = new StringBuilder();
            for (byte b : mac) {
                builder.append(String.format("%02X", b));
            }

            return builder.toString();
        } catch (UnknownHostException | SocketException ex) {
            Logger.getLogger(DeveloperLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "no";
    }
    
    public static String getPCNAME() {
        try {
            InetAddress address;
            address = InetAddress.getLocalHost();
            
            String mac = address.getHostName();

            System.out.println("PC_NAME----"+mac);

            return mac.toString();
        } catch (Exception ex) {
            Logger.getLogger(DeveloperLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "no";
    }
    
    public static void main(String[] args) {
        System.out.println(DeveloperLogin.getMacAddress());   
    }
}
