/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finac.loan;

import com.finac.loan.menu_object.MainMenu;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.mainframe.DefaultMainframe;
import com.mac.af.resources.ApplicationResources;
import com.mac.zreport.ReportUtil;
import com.mac.zreport.object.ReportBand;
import com.mac.zreport.object.ReportFile;
import com.mac.zreport.object.ReportTask;
import com.mac.zresources.FinacResources;
import static com.mac.zresources.FinacResources.*;
import com.mac.zsystem.settings.theme_chooser.Theme;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction.object.TransactionType;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author NIMESH-PC
 */
public class DBMainFrame extends DefaultMainframe {

    @Override
    protected void createGUI() {
        createRegistration();
//        createHigherPurchase();
//        createLoan();
//        createCashier();
//        createAccount();
//        createReports2();
//        createDashBoard();
//        createSystem();
//        createApplicationMenu();
    }

    private void createRegistration() {
        addTask("Registration");
        addBand("People", FinacResources.getImageIconURL(REGISTRATION_PEOPLE));
        addVisiableButton("Client", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.client.REGClient.class, null, "createRegistration");
//        addButton("Supplier", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.supplier.REGSupplier.class, null);
        addVisiableButton("Client Approval", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.client.ClientApproval.class, null, "createRegistration");
        addVisiableButton("Client Information", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.client.ClientInformation.class, null, "createRegistration");
        addVisiableButton("Loan Group", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.loan_group.REGLoanGroup.class, null, "createRegistration");
        addVisiableButton("Route", FinacResources.getImageIconURL(REGISTRATION_ROUTE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.route.REGRoute.class, null, "createRegistration");
        addVisiableButton("Employee", FinacResources.getImageIconURL(REGISTRATION_EMPLOYEE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.employee.REGEmployee.class, null, "createRegistration");
//        addBand("Company", FinacResources.getImageIconURL(REGISTRATION_COMPANY));
        addVisiableButton("Company", FinacResources.getImageIconURL(REGISTRATION_COMPANY), DefaultMainframe.ElementPriority.TOP, com.mac.registration.company.REGCompany.class, null, "createRegistration");
        addVisiableButton("Company Branch", FinacResources.getImageIconURL(REGISTRATION_BRANCH), DefaultMainframe.ElementPriority.TOP, com.mac.registration.branch.REGBranch.class, null, "createRegistration");
        addVisiableButton("Leave Day", FinacResources.getImageIconURL(REGISTRATION_LEAVE_DAY), DefaultMainframe.ElementPriority.TOP, com.mac.registration.company_leave_day.REGCompanyLeaveDay.class, null, "createRegistration");
//        addBand("Loan", FinacResources.getImageIconURL(REGISTRATION_LOAN));
        addVisiableButton("Loan Type", FinacResources.getImageIconURL(REGISTRATION_LOAN_TYPE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.loan_type.REGLoanType.class, null, "createRegistration");
        addVisiableButton("Charge Scheme", FinacResources.getImageIconURL(REGISTRATION_LOAN_TYPE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.charge_scheme.REGChargeScheme.class, null, "createRegistration");
//        addBand("Cashier", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT));
        addVisiableButton("Cashier Point", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.cashier_point.REGCashier.class, null, "createRegistration");
        

    }

    private void createHigherPurchase() {
        //addTask("Hire purchase");
        addBand("Registration", FinacResources.getImageIconURL(LOAN));
        addVisiableButton("Item", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.registration.item.REGItem.class, null, "createHigherPurchase");
        addVisiableButton("Item Category", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.registration.item_category.REGItemCategory.class, null, "createHigherPurchase");
        addVisiableButton("Item Department", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.registration.item_department.REGItemDepartment.class, null, "createHigherPurchase");
        addBand("Transactions", FinacResources.getImageIconURL(LOAN));
        addVisiableButton("Sales Invoice", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.item.sales_item.REGSalesItem.class, null, "createHigherPurchase");
    }

    //TEST LAND
    private void createLand() {
        //addTask("Land");
        addBand("Registration", FinacResources.getImageIconURL(LOAN));
        addVisiableButton("Land", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.land.land_purchase.REGLandPurchase.class, null, "createLand");
        addVisiableButton("Land Project", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.land.land_project.REGProject.class, null, "createLand");
        addVisiableButton("Capital Expenditure", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.land.capital_expenditure.REGCapitalExpenditure.class, null, "createLand");
        addVisiableButton("Block Creation", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.land.block_creation.REGBlockCreation.class, null, "createLand");
        addBand("Transactions", FinacResources.getImageIconURL(LOAN));
        addVisiableButton("Land Reservation", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.item.sales_item.REGSalesItem.class, null, "createLand");
    }

    private void createLoan() {
        //addTask("Loan");
        addBand("Loan Afford", FinacResources.getImageIconURL(LOAN));
//        addButton("Loan Application", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_application.TRALoanApplication.class, null);
//        addButton("Opening Loan", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.loan.opening_loan.TRAOpeningLoanApplication.class, null);
        addVisiableButton("HP Loan Application", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.loan.hp_loan_application.TRALoanApplication.class, null, "createLoan");
        if (getTransaction(SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE).isApprove()) {
            addVisiableButton("Loan Application Approval", FinacResources.getImageIconURL(LOAN_APPROVAL), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_approval.LoanApplicationApproval.class, null, "createLoan");
        }
        addVisiableButton("Loan", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan.TRALoan.class, null, "");
        if (getTransaction(SystemTransactions.LOAN_TRANSACTION_CODE).isApprove()) {
            addVisiableButton("Started Loan Approval", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_approval.StartedLoanApproval.class, null, "createLoan");
        }
        addVisiableButton("Loan Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan.TRALoanCancel.class, null, "createLoan");
        addVisiableButton("Loan Closing", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan_close.REGLoanClose.class, null, "createLoan");
//        addButton("Voucher", FinacResources.getImageIconURL(LOAN_VOUCHER), DefaultMainframe.ElementPriority.TOP, com.mac.loan.voucher.Voucher.class, null);
//        addButton("Voucher Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.voucher.VoucherCancel.class, null);
        addBand("Loan Transaction", FinacResources.getImageIconURL(LOAN));
        addVisiableButton("Receipt", FinacResources.getImageIconURL(LOAN_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.temparary_receipt_receive.TempararyReceiptReceive.class, null, "createLoan");
        addVisiableButton("Bank Deposit Receipt", FinacResources.getImageIconURL(LOAN_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.bank_deposit.BankDeposit.class, null, "createLoan");
        addVisiableButton("Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.receipt.ReceiptCancel.class, null, "createLoan");
        addVisiableButton("Loan Charges", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_charges.LoanCharges.class, null, "createLoan");
//        addButton("Loan Charges Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null);
//        addButton("Client Rebit", FinacResources.getImageIconURL(LOAN_REBIT), ElementPriority.TOP, com.mac.loan.rebit.ClientRebit.class, null);
        addVisiableButton("Client Rebit", FinacResources.getImageIconURL(LOAN_REBIT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.rebit.Rebit.class, null, "createLoan");
//        addButton("Rebits Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null);
//        addButton("Rebit Cancel", FinacResources.getImageIconURL(REBIT_CANCEL), DefaultMainframe.ElementPriority.LOW, com.mac.loan.rebit.TRARebitClose.class, null);
        addBand("Finalize", FinacResources.getImageIconURL(LOAN));
//        addButton("Loan Close", FinacResources.getImageIconURL(LOAN_APPROVAL), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_close.LoanClose.class, null);
//        addVisiableButton("Loan Close Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null, "createLoan");
        addVisiableButton("Day End", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.transaction.day_end.SystemDayEnd.class, null, "createLoan");
        addVisiableButton("Day End Cancel", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), DefaultMainframe.ElementPriority.LOW, com.mac.zsystem.transaction.day_end.DayEndRollback.class, null, "createLoan");
//        addVisiableButton("Opening Loan", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, null, null, "createLoan");
    }

    private void createCashier() {
        //addTask("Cashier");
        addBand("Cashier", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addVisiableButton("Voucher", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.loan.voucher.Voucher.class, null, "createCashier");
        addVisiableButton("Voucher Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.voucher.VoucherCancel.class, null, "createCashier");
        addVisiableButton("Temperary Receipt", FinacResources.getImageIconURL(LOAN_TEMPERERY_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.temperary_receipt.TemperaryReceipt.class, null, "createCashier");
//        addVisiableButton("Temperary Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null, "createCashier");
        addVisiableButton("Receipt", FinacResources.getImageIconURL(LOAN_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.receipt.Receipt.class, null, "createCashier");
        addVisiableButton("Group Receipt", FinacResources.getImageIconURL(LOAN_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.group_receipt.Receipt.class, null, "createCashier");
        addVisiableButton("Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.receipt.ReceiptCancel.class, null, "createCashier");
        addVisiableButton("Bank Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.bank_deposit.ReceiptCancel.class, null, "createCashier");
        addVisiableButton("Cashier Close", FinacResources.getImageIconURL(CASHIER_CLOSE), DefaultMainframe.ElementPriority.TOP, com.mac.account.cashier_closing.CashierClosing.class, null, "createCashier");
    }

    private void createAccount() {
        //addTask("Account");
        addBand("Registration", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addBand("Account Registration", FinacResources.getImageIconURL(REGISTRATION_ACCOUNT));
        addVisiableButton("Account Category", FinacResources.getImageIconURL(REGISTRATION_ACCOUNT_CATEGORY), DefaultMainframe.ElementPriority.LOW, com.mac.registration.account_category.REGAccountCategory.class, null, "createAccount");
        addVisiableButton("Account", FinacResources.getImageIconURL(REGISTRATION_ACCOUNT), DefaultMainframe.ElementPriority.LOW, com.mac.registration.account.REGAccount.class, null, "createAccount");
        addBand("Account Transaction", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addVisiableButton("Opening Balance", FinacResources.getImageIconURL(LOAN_OPENING_BALANCE), DefaultMainframe.ElementPriority.TOP, com.mac.account.opening_balance.OpeningBalance.class, null, "createAccount");
        addVisiableButton("Bank Deposit", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), DefaultMainframe.ElementPriority.TOP, com.mac.account.bank_deposit.BankDeposit.class, null, "createAccount");
        addVisiableButton("General Receipt", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), DefaultMainframe.ElementPriority.TOP, com.mac.account.general_receipt.GeneralReceipt.class, null, "createAccount");
        addVisiableButton("General Voucher", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), DefaultMainframe.ElementPriority.TOP, com.mac.account.general_voucher.GeneralVoucher.class, null, "createAccount");
        addVisiableButton("General Voucher 2", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), DefaultMainframe.ElementPriority.TOP, com.mac.account.general_voucher_2.GeneralVoucher.class, null, "createAccount");
        addVisiableButton("Journal", FinacResources.getImageIconURL(LOAN_JOURNAL), DefaultMainframe.ElementPriority.TOP, com.mac.account.journal.Journel.class, null, "createAccount");
        addVisiableButton("Bank Reconsilation", FinacResources.getImageIconURL(LOAN_JOURNAL), DefaultMainframe.ElementPriority.TOP, com.mac.bank_rec.REGBankRec.class, null, "createAccount");
        addBand("Cheque Handling", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addVisiableButton("Cheque Deposit", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), DefaultMainframe.ElementPriority.LOW, com.mac.account.cheque_deposit.ChequeDeposit.class, null, "createAccount");
        addVisiableButton("Cheque Realize", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), DefaultMainframe.ElementPriority.LOW, com.mac.account.cheque_realize.ChequeRealize.class, null, "createAccount");
        addVisiableButton("Cheque Return", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), DefaultMainframe.ElementPriority.LOW, com.mac.account.cheque_return.ChequeReturn.class, null, "createAccount");
        addVisiableButton("Cheque Issue", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), DefaultMainframe.ElementPriority.LOW, com.mac.account.cheque_issue.ChequeIssue.class, null, "createAccount");
        addVisiableButton("Bank", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.LOW, com.mac.registration.bank.REGBank.class, null, "createAccount");
        addVisiableButton("Branch", FinacResources.getImageIconURL(REGISTRATION_BANK_BRANCH), DefaultMainframe.ElementPriority.LOW, com.mac.registration.bank_branch.REGBanckBranch.class, null, "createAccount");
        addVisiableButton("Bank Account", FinacResources.getImageIconURL(REGISTRATION_BANK_BRANCH), DefaultMainframe.ElementPriority.LOW, com.mac.registration.bank_account.REGBankAccount.class, null, "createAccount");
    }

    private void createSystem() {
        //addTask("System");
        addBand("Transaction Settings", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addVisiableButton("Account Settings", FinacResources.getImageIconURL(ACCOUNTT_SETTING), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.transaction.account.account_setting.gui.SystemAccountSettings.class, null, "createSystem");
        addVisiableButton("Payment Settings", FinacResources.getImageIconURL(PAYMENT_SETTING), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.transaction.payment.payment_setting.gui.SystemPaymentSettings.class, null, "createSystem");
        addVisiableButton("Transaction Settings", FinacResources.getImageIconURL(PAYMENT_SETTING), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.settings.transaction_settings.TransactionSettings.class, null, "createSystem");
        addBand("Application Settings", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addVisiableButton("Configuration", FinacResources.getImageIconURL(SETTING), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.settings.settings.Settings.class, null, "createSystem");
        addVisiableButton("Theme Selector", FinacResources.getImageIconURL(APPLICATION_MENU_THEME), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.settings.theme_chooser.Theme.class, null, "createSystem");
        addBand("Other Settings", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addVisiableButton("Remind Letter Templates", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.registration.remind_letter.REGTemplateRegistration.class, null, "createSystem");
//        addVisiableButton("Remind Letter Generator", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zutil.remind_letter.LetterGenerator.class, null, "createSystem");
        addBand("Dash Boards", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addVisiableButton("Recovery Dash Board", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.dash_board.recovery.RecoveryDashBoard.class, null, "createSystem");
        addVisiableButton("Client Information", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.dash_board.client.ClientInformationDashBoard.class, null, "createSystem");
        addBand("Permission", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addVisiableButton("User Role", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.permission.user_role.REGUserRole.class, null, "createSystem");
        addVisiableButton("Module", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.permission.permission.REGPermission.class, null, "createSystem");
        addVisiableButton("User Role Permission", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.permission.user_role_permission.REGUserRolePermission.class, null, "createSystem");
        addVisiableButton("Backup", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.af.templates.system.backup.BackupRestore.class, null, "createSystem");
        addVisiableButton("Report Registration", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zreport.report_registration.REGReportRegistration.class, null, "createSystem");
        addVisiableButton("Report Permission", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zreport.report_permission.REGUserRoleReportPermission.class, null, "createSystem");
//        addVisiableButton("Email And SMS Configuration", FinacResources.getImageIconURL(SMS), DefaultMainframe.ElementPriority.TOP, com.mac.zsystem.settings.msg_and_email_settings.REGSmsAndEmailTemplate.class, null, "createSystem");
    }

    private void createReports2() {
        //addTask("Report");
        addBand("Reports", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addVisiableButton("Loan Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zreport.reports_viewers.LoanReports.class, null, "createReports2");
        addVisiableButton("Account Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zreport.reports_viewers.AccountReports.class, null, "createReports2");
        addVisiableButton("Cahier Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zreport.reports_viewers.CashierReports.class, null, "createReports2");
        addVisiableButton("Administration Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), DefaultMainframe.ElementPriority.TOP, com.mac.zreport.reports_viewers.AdministrationReports.class, null, "createReports2");

    }

    private void createDashBoard() {
        //addTask("Dash Board");
        addBand("Dash Board", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT));
        addVisiableButton("Client Information", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.TOP, com.mac.registration.client.ClientInformation.class, null, "createDashBoard");
        addVisiableButton("Agrement Details", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT), DefaultMainframe.ElementPriority.TOP, com.mac.agrement_details.PCAgrementDetailss.class, null, "createDashBoard");
        addVisiableButton("Recovery Visit", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT), DefaultMainframe.ElementPriority.TOP, com.mac.recovery_visit.PCRecoveryVisit.class, null, "createDashBoard");
    }

    private void createApplicationMenu() {
        startApplicationMenu();
        addPrimaryMenu("Theme Selector", FinacResources.getImageIconURL(APPLICATION_MENU_THEME), Theme.class, null);
        addPrimaryMenu("Configuration", FinacResources.getImageIconURL(APPLICATION_MENU_CONFIG), Theme.class, null);
        addPrimaryMenu("Cache Manager", FinacResources.getImageIconURL(APPLICATION_MENU_CACHE_MANAGER), com.mac.af.core.database.hibernate.ui.cache_manager.CacheManager.class, null);
        addPrimaryMenu("Task Manager", FinacResources.getImageIconURL(APPLICATION_MENU_TASK_MANAGER), Theme.class, null);
    }

    private void createReports() {
        List<ReportTask> reportTasks = ReportUtil.getReportTasks();

        for (ReportTask reportTask : reportTasks) {
            //addTask(ReportUtil.getFormattedString(reportTask.getName()));
            for (ReportBand reportBand : reportTask.getReportBands()) {
                addBand(ReportUtil.getFormattedString(reportBand.getName()), FinacResources.getImageIconURL(FinacResources.REPORT));
                for (ReportFile reportFile : reportBand.getReportFiles()) {
                    addButton(ReportUtil.getFormattedString(reportFile.getName()), FinacResources.getImageIconURL(REPORT), DefaultMainframe.ElementPriority.LOW, com.mac.zreport.ReportPanel.class, reportFile.getReportFile());
                }
            }
        }
    }

    private TransactionType getTransaction(String code) {
        HibernateDatabaseService databaseService = CPanel.GLOBAL.getDatabaseService();

        TransactionType transactionType = null;
        try {
            transactionType = (TransactionType) databaseService.getObject(TransactionType.class, code);
        } catch (DatabaseException ex) {
            Logger.getLogger(HPMainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        return transactionType;
    }

    private void addVisiableButton(
            String title,
            URL iconURL,
            DefaultMainframe.ElementPriority priority,
            Class<? extends CPanel> classx,
            Object initMethod,
            String bandType) {
        try {
            List taskList = CPanel.GLOBAL.getDatabaseService().initCriteria(MainMenu.class)
                    .setProjection(Projections.distinct(Projections.property("bandType")))
                    .list();

            boolean isTask = true;

            for (Object object : taskList) {


                List<MainMenu> mainMenusList = CPanel.GLOBAL.getDatabaseService().initCriteria(MainMenu.class)
                        .add(Restrictions.eq("visiable", true))
                        //                    .add(Restrictions.eq("bandType", bandType))
                        .add(Restrictions.eq("bandType", object.toString()))
                        .list();

                if (mainMenusList != null && classx != null) {
//                    if (isTask) {
//                        addTask(object.toString());
//                    }

                    String methodClass = classx.getCanonicalName();

                    for (MainMenu mainMenu : mainMenusList) {
                        isTask=false;
                        if (mainMenu.getClass_().equals(methodClass)) {
                            addButton(title, iconURL, priority, classx, initMethod);
                            if (mainMenu.isStartGroup()) {
                                startGroup();
                            }
                        }
                    }

                }



            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}