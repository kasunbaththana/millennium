/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finac.loan;

import javax.swing.ImageIcon;

/**
 *
 * @author NIMESH-PC
 */
public class NotificationList {

    private final String title;
    private final String imagePath;
    private ImageIcon image;
    private String borderTitel;
    private String type;

    public NotificationList(String title, String imagePath, String borderTitel,String type) {
        this.title = title;
        this.imagePath = imagePath;
        this.borderTitel = borderTitel;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public ImageIcon getImage() {
        if (image == null) {
            image = new ImageIcon(imagePath);
        }
        return image;
    }

    public String getBorderTitel() {
        return borderTitel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

  
    

    // Override standard toString method to give a useful result
    public String toString() {
        return title;
    }
}
