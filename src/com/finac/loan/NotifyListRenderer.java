/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finac.loan;

import com.mac.zresources.FinacResources;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JList;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultListCellRenderer;

/**
 *
 * @author NIMESH-PC
 */
public class NotifyListRenderer extends SubstanceDefaultListCellRenderer {

    private NotificationList notificationList;
    private ImageIcon image;

    public NotifyListRenderer() {
        image = FinacResources.getImageIcon(FinacResources.LOAN, 20, 20);
    }


    @Override
    public void setText(String text) {
        if (notificationList != null) {
            text = notificationList.getTitle();
        }
        super.setText(text);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value instanceof NotificationList) {
            this.notificationList = (NotificationList) value;
        } else {
            this.notificationList = null;
        }
        return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
    
    @Override
    public void setIcon(Icon icon) {
        icon =notificationList.getImage();
        super.setIcon(icon);
    }
}
