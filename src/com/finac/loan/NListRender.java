package com.finac.loan;

import com.mac.zresources.FinacResources;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class NListRender  extends JLabel implements ListCellRenderer<NotificationList> {
 
    @Override
    public Component getListCellRendererComponent(JList<? extends NotificationList> list, NotificationList nlist, int index,
        boolean isSelected, boolean cellHasFocus) {
            ImageIcon image= FinacResources.getImageIcon(FinacResources.REGISTRATION_CLIENT, 20, 20);
            ImageIcon image2= FinacResources.getImageIcon(FinacResources.LOAN_APPROVAL, 20, 20);
            ImageIcon image3= FinacResources.getImageIcon(FinacResources.LOAN_REBIT, 20, 20);
            ImageIcon image4= FinacResources.getImageIcon(FinacResources.AUTHORIZATION, 20, 20);
            ImageIcon image5= FinacResources.getImageIcon(FinacResources.AUTHORIZATION, 20, 20);
            ImageIcon image6= FinacResources.getImageIcon(FinacResources.LOAN_APPROVAL, 20, 20);
//        String code = country.getCode();
//        ImageIcon imageIcon = new ImageIcon(getClass().getResource("icons/registration/people.png"));
//        ImageIcon imageIcon = new ImageIcon(image);
           if(nlist.getType().equals(NotificationType.CLIENT_APPROVAL))
           {
               
            setIcon(image);
            setText(nlist.getTitle());
           }
           else if(nlist.getType().equals(NotificationType.LOAN_APPLICATION_APPROVAL))
           {
            setIcon(image2);
            setText(nlist.getTitle());
               
           }
           else if(nlist.getType().equals(NotificationType.REBATE_APPROVAL))
           {
            setIcon(image3);
            setText(nlist.getTitle());
               
           }
           else if(nlist.getType().equals(NotificationType.SAV_ACCOUNT_APPROVAL))
           {
            setIcon(image4);
            setText(nlist.getTitle());
               
           }
           else if(nlist.getType().equals(NotificationType.SAV_ACCOUNT_APPROVAL1))
           {
            setIcon(image5);
            setText(nlist.getTitle());
               
           }
           else if(nlist.getType().equals(NotificationType.RECEIPT_APPROVE))
           {
            setIcon(image6);
            setText(nlist.getTitle());
               
           }
            
         
         
        return this;
    } 

}
   
