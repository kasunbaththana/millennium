/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finac.loan;

/**
 *
 * @author NIMESH-PC
 */
public class NotificationType {
    public static final String LOAN_APPLICATION_APPROVAL="LOAN_APPLICATION_APPROVAL";
    public static final String LOAN_OPEN_APPROVAL="LOAN_OPEN_APPROVAL";
    public static final String CLIENT_APPROVAL="CLIENT_APPROVAL";
    public static final String REBATE_APPROVAL="REBATE_APPROVAL";
    public static final String SAV_ACCOUNT_APPROVAL="SAV_ACCOUNT_APPROVAL";
    public static final String SAV_ACCOUNT_APPROVAL1="SAV_ACCOUNT_APPROVAL1";
    public static final String RECEIPT_APPROVE="RECEIPT_APPROVE";
}
