/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finac.loan;

import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author NIMESH-PC
 */
class ListRender extends JLabel implements ListCellRenderer {

    private static final Color HIGHLIGHT_COLOR = new Color(0, 0, 128);

    public ListRender() {
        setOpaque(true);
        setIconTextGap(12);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
//        setBorder(new BevelBorder(BevelBorder.RAISED));
        
         NotificationList entry = (NotificationList) value;
        
//        setFont(new java.awt.Font("Tahoma", 1, 11));
        setBorder(BorderFactory.createTitledBorder(null, entry.getBorderTitel(), 0, 0, new java.awt.Font("Tahoma", 1, 10)));
        setText(entry.getTitle());
        setIcon(entry.getImage());
        
        
        if (isSelected) {
            setBackground(new Color(255,153,51));
            setForeground(Color.GRAY);
        } else {
            Color c = null;
            if ((index % 2) == 0) {
                c = new Color(204,153,205);
            } else {
                c = new Color(204,205,255);
            }
            setBackground(c);
//            setBackground(new Color(204,204,204));
            setForeground(Color.black);
        }
        return this;
    }
}
