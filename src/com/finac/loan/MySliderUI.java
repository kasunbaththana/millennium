/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finac.loan;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JSlider;
import javax.swing.plaf.basic.BasicSliderUI;

/**
 *
 * @author Mohan
 */
public class MySliderUI extends BasicSliderUI {

    public MySliderUI(JSlider b) {
        super(b);
    }

    @Override
    public void paintTrack(Graphics g) {
        double tickWidth = 10.0;
        
    }

    

    public Color getColor(double percent) {
        int variations = 4;
        int high = 255;
        int low = 0;
        double range = (high - low) * variations;
        double value = range * percent / 100.0;
        double peiceRange = range / (double) variations;

        double currentPeice = (int) (value / peiceRange);
        double peiceValue = value - (currentPeice * peiceRange);

        int r, g, b;

        switch ((int) currentPeice) {
            case 0:
                r = high;
                g = (int) peiceValue;
                b = low;
                break;
            case 1:
                r = (int) (high - peiceValue);
                g = high;
                b = low;
                break;
            case 2:
                r = low;
                g = high;
                b = (int) peiceValue;
                break;
            case 3:
                r = low;
                g = (int) (high - peiceValue);
                b = high;
                break;
            case 4:
                r = (int) peiceValue;
                g = low;
                b = high;
                break;
            case 5:
                r = high;
                g = low;
                b = (int) (high - peiceValue);
                break;
            default:
                r = 0;
                g = 0;
                b = 0;
        }

        return new Color(r, g, b);
    }

    public static void main(String[] args) {
        MySliderUI sliderUI = new MySliderUI(null);
        sliderUI.getColor(26);
    }
}
