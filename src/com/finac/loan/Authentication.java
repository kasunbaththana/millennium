/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.finac.loan;

import com.finac.loan.menu_object.ZAuthentication;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import java.util.List;

/**
 *
 * @author KASUN
 */
public class Authentication {

    public Authentication(HibernateDatabaseService databaseService,String   MACADDRESS,String PCNAME) {
        this.databaseService = databaseService;
        this.MACADDRESS = MACADDRESS;
        this.PCNAME = PCNAME;
    }
    
  
    private HibernateDatabaseService databaseService;
    private String MACADDRESS;
    private String PCNAME;
    
    public boolean getAthentication() throws DatabaseException
    {
        String MAC_MACH="",NAME_MACH="";
        List<ZAuthentication> list =  getsystempermition(PCNAME);   
        for(ZAuthentication LAuthentication:list)
        {
            MAC_MACH = LAuthentication.getMac();
            NAME_MACH = LAuthentication.getPcName();
        }
        System.out.println("MACADDRESS :"+MACADDRESS+"   PCNAME:"+PCNAME);
            System.out.println("MAC_MACH :"+MAC_MACH+"   NAME_MACH:"+NAME_MACH);
        if(MACADDRESS.equals(MAC_MACH) && PCNAME.equals(NAME_MACH) )
        {
            
           return true; 
        }
        return false;
    }
    
       public boolean getAthenticationClean() throws DatabaseException
    {
        String MAC_MACH="",NAME_MACH="";
        boolean CLEAN = false;
        List<ZAuthentication> list =  getsystempermition(PCNAME);   
        for(ZAuthentication LAuthentication:list)
        {
            MAC_MACH = LAuthentication.getMac();
            NAME_MACH = LAuthentication.getPcName();
            CLEAN = LAuthentication.isClean();
        }
        System.out.println("MACADDRESS :"+MACADDRESS+"   PCNAME:"+PCNAME);
            System.out.println("MAC_MACH :"+MAC_MACH+"   NAME_MACH:"+NAME_MACH);
        if(MACADDRESS.equals(MAC_MACH) && PCNAME.equals(NAME_MACH) && CLEAN==true)
        {
            
           return true; 
        }
        return false;
    }
    
    public List<ZAuthentication> getsystempermition(String PCNAME) throws DatabaseException
    {
         List<ZAuthentication> list = databaseService.getCollection
                 ("from com.finac.loan.menu_object.ZAuthentication where status='ACTIVE' and PC_NAME='"+PCNAME+"' ");
         return list;
    }
    
}
    
    

