/*
 *  FinacMainFrame.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 20, 2014, 10:25:38 AM
 *  Copyrights Channa Mohan, All rights reserved.
 *  
 */
package com.finac.loan;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.zresources.FinacResources;
import com.mac.af.core.environment.mainframe.DefaultMainframe;
import static com.mac.zresources.FinacResources.*;
import com.mac.zsystem.settings.theme_chooser.Theme;
import com.mac.af.resources.ApplicationResources;
import com.mac.registration.client.REGClient;
import com.mac.zreport.ReportUtil;
import com.mac.zreport.object.ReportBand;
import com.mac.zreport.object.ReportFile;
import com.mac.zreport.object.ReportTask;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction.object.TransactionType;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class FinacMainFrame extends DefaultMainframe {

    @Override
    protected void createGUI() {
        createRegistration();
//      createHigherPurchasse();
        createMicroFinance();
        createLoan();
        createLoanVoucher();
        createCashier();
       // createMobile();
        createAccount();
        createReports2();
        createDashBoard();
        createSystem();
       // createSaving();
//      createUtility();
        createApplicationMenu();
    }

    private void createRegistration() {
        addTask("Registration");

        addBand("People", FinacResources.getImageIconURL(REGISTRATION_PEOPLE));
        startGroup();
        addButton("New Customer", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.client.REGClient.class,null);
        addButton("Customer Approval", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.client.ClientApproval.class, null);
        addButton("Customer Information", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.client.ClientInformation.class, null);
       // addButton("Loan Group", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.loan_group.REGLoanGroup.class, null);
        addBand("Supplier Registration", FinacResources.getImageIconURL(REGISTRATION_PEOPLE));
        startGroup();
        addButton("Supplier Registration", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.supplier.REGSupplier.class, null);
        startGroup();
        addButton("Employee", FinacResources.getImageIconURL(REGISTRATION_EMPLOYEE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.employee.REGEmployee.class, null);
        addBand("Company", FinacResources.getImageIconURL(REGISTRATION_COMPANY));
        addButton("Company", FinacResources.getImageIconURL(REGISTRATION_COMPANY), DefaultMainframe.ElementPriority.TOP, com.mac.registration.company.REGCompany.class, null);
        addButton("Company Branch", FinacResources.getImageIconURL(REGISTRATION_BRANCH), DefaultMainframe.ElementPriority.TOP, com.mac.registration.branch.REGBranch.class, null);
        addButton("Leave Day", FinacResources.getImageIconURL(REGISTRATION_LEAVE_DAY), DefaultMainframe.ElementPriority.TOP, com.mac.registration.company_leave_day.REGCompanyLeaveDay.class, null);
        addBand("Loan", FinacResources.getImageIconURL(REGISTRATION_LOAN));
        addButton("Loan Type", FinacResources.getImageIconURL(LOAN_TYPE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.loan_type.REGLoanType.class, null);
        addButton("Charge Scheme", FinacResources.getImageIconURL(REGISTRATION_LOAN_TYPE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.charge_scheme.REGChargeScheme.class, null);
        addButton("Advance Scheme", FinacResources.getImageIconURL(ADVANCE_SCHEME), DefaultMainframe.ElementPriority.TOP, com.mac.registration.advance_scheme.REGAdvanceScheme.class, null);
        addButton("Reason Setup", FinacResources.getImageIconURL(REGISTRATION_LEAVE_DAY), DefaultMainframe.ElementPriority.TOP, com.mac.registration.reason_setup.REGReason.class, null);
        addBand("Cashier", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT));
        startGroup();
        addButton("Cashier Point", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.cashier_point.REGCashier.class, null);
        addBand("Map", FinacResources.getImageIconURL(REGISTRATION_PEOPLE));
        addButton("Route", FinacResources.getImageIconURL(ROUTE), DefaultMainframe.ElementPriority.TOP, com.mac.registration.route.REGRoute.class, null);
        startGroup();
        addButton("Vehicle", FinacResources.getImageIconURL(BANK_DEPOSIT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.vehicle.REGVehicle.class, null);
        startGroup();
        addButton("Customer Payment Chargs", FinacResources.getImageIconURL(ACTION_START), DefaultMainframe.ElementPriority.TOP, com.mac.registration.customer_chargs.REGCustomerChargs.class, null);
        startGroup();
        addButton("Remind Letter Setup", FinacResources.getImageIconURL(ACTION_START), DefaultMainframe.ElementPriority.TOP, com.mac.registration.remind_letter_setup.REGRemindLetterSetup.class, null);
        addButton("Loan Editor", FinacResources.getImageIconURL(REGISTRATION_LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.registration.loan_aditor.REGLoanEdit.class, null);
        //addButton("Fund Type", FinacResources.getImageIconURL(MICRO_FUND), DefaultMainframe.ElementPriority.TOP, com.mac.registration.fund_type.REGFundType.class, null);
    }

    private void createHigherPurchase() {
        addTask("Hire purchase");

        addBand("Registration", FinacResources.getImageIconURL(LOAN));
        addButton("Item", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.registration.item.REGItem.class, null);
        addButton("Item Category", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.registration.item_category.REGItemCategory.class, null);
        addButton("Item Department", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.registration.item_department.REGItemDepartment.class, null);

//        addBand("Transactions", FinacResources.getImageIconURL(LOAN));
        //HP
//        addButton("Sales Invoice", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.item.sales_item.REGSalesItem.class, null);
    }

    private void createLoanVoucher() {
          addTask("Voucher");
        addBand("Loan Afford", FinacResources.getImageIconURL(LOAN));
       // addButton("Voucher", FinacResources.getImageIconURL(LOAN_VOUCHER), DefaultMainframe.ElementPriority.TOP, com.mac.loan.voucher.Voucher.class, null);
       // addButton("Voucher Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.voucher.VoucherCancel.class, null);
        startGroup();
        addButton("Supplier Voucher", FinacResources.getImageIconURL(LOAN_VOUCHER), DefaultMainframe.ElementPriority.TOP, com.mac.loan.voucher_supplier.SupplierVoucher.class, null);
      
     // addButton("Delivery Order", FinacResources.getImageIconURL(LOAN_VOUCHER), DefaultMainframe.ElementPriority.TOP, com.mac.loan.do_genarate.PCDoGenerator.class, null);
 //        addButton("Voucher Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.voucher.VoucherCancel.class, null);
    
      addButton("Delivery Order", FinacResources.getImageIconURL(LOAN_VOUCHER), DefaultMainframe.ElementPriority.TOP, com.mac.loan.do_genarate.PCDoGenerator.class, null);
      addButton("Voucher Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.voucher.VoucherCancel.class, null);
      startGroup();
      addButton("Fund Issue", FinacResources.getImageIconURL(LOAN_VOUCHER), DefaultMainframe.ElementPriority.TOP, com.mac.account.fund_issue.FundIssueVoucher.class, null);
      
      startGroup();
        addButton("General Receipt", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.general_receipt.GeneralReceipt.class, null);
        addButton("General Voucher", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.general_voucher.GeneralVoucher.class, null);
        addButton("Petty Cash Voucher ", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.general_voucher_2.GeneralVoucher.class, null);
        addButton("General Voucher Approve", FinacResources.getImageIconURL(LOAN_APPROVAL), ElementPriority.TOP, com.mac.account.general_voucher.GeneralVoucherApproval.class, null);
        
    }
    private void createLoan() {
        addTask("Loan");
        addBand("Loan Afford", FinacResources.getImageIconURL(LOAN));
        startGroup();
        //normal
        addButton("Loan Request", FinacResources.getImageIconURL(LOAN_APPLICATION2), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_application.TRALoanApplication.class, null);
        //HP
//        addButton("HP Loan Application", FinacResources.getImageIconURL(LOAN_APPLICATION), DefaultMainframe.ElementPriority.TOP, com.mac.loan.hp_loan_application.TRALoanApplication.class, null);
        if (getTransaction(SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE).isApprove()) {
            startGroup();
            addButton("Loan Approval", FinacResources.getImageIconURL(LOAN_APPROVAL), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_approval.LoanApplicationApproval.class, null);
        }

        startGroup();
        addButton("Loan Active", FinacResources.getImageIconURL(OPEN_LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan.TRALoan.class, null);
        addButton("Loan Disbursement", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.loan.disbursement.Disbursement.class, null);
        if (getTransaction(SystemTransactions.LOAN_TRANSACTION_CODE).isApprove()) {
            addButton("Active Loan Approval", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_approval.StartedLoanApproval.class, null);
        }
        startGroup();
        addButton("Loan Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan.TRALoanCancel.class, null);
        addButton("Loan Closing", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan_close.REGLoanClose.class, null);
        addButton("Loan Restart", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan_restart.REGLoanRestart.class, null);
//        addButton("Loan Suspend", FinacResources.getImageIconURL(CANCEL), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan_suspend.REGSuspend.class, null);
        startGroup();
        

        addBand("Loan Transaction", FinacResources.getImageIconURL(LOAN));
        startGroup();
        addButton("Issue Receipt For Cheque", FinacResources.getImageIconURL(LOAN_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.temparary_receipt_receive.TempararyReceiptReceive.class, null);
        addButton("Bank Deposit Receipt", FinacResources.getImageIconURL(LOAN_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.bank_deposit.BankDeposit.class, null);
        
        startGroup();
        addButton("Loan Charges", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), ElementPriority.TOP, com.mac.loan.loan_charges.LoanCharges.class, null);
//        addButton("Seize Charges", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), ElementPriority.TOP, com.mac.loan.seized.SeizeCharges.class, null);
        
        addButton("Client Rebate", FinacResources.getImageIconURL(LOAN_REBIT), ElementPriority.TOP, com.mac.loan.rebit.Rebit.class, null);
        addButton("Rebate Approve", FinacResources.getImageIconURL(LOAN_REBIT), ElementPriority.TOP, com.mac.loan.rebate_approve.RebitAp.class, null);
        addButton("Early Settlement Rebate", FinacResources.getImageIconURL(LOAN_REBIT), ElementPriority.TOP, com.mac.loan.rebit_early_settle.Rebit.class, null);
        startGroup();
        addButton("Loan Charges Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan_charges.LoanChargesCancel.class, null);
        addButton("Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.receipt.ReceiptCancel.class, null);
        addButton("Day End Cancel", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), ElementPriority.LOW, com.mac.zsystem.transaction.day_end.DayEndRollback.class, null);
        
        //        addButton("Rebits Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null);
//        addButton("Rebit Cancel", FinacResources.getImageIconURL(REBIT_CANCEL), DefaultMainframe.ElementPriority.LOW, com.mac.loan.rebit.TRARebitClose.class, null);

//        addBand("Finalize", FinacResources.getImageIconURL(LOAN));s
//        startGroup();
//        addButton("Loan Close", FinacResources.getImageIconURL(LOAN_CLOSE), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_close.REGLoanClose.class, null);
//        addButton("Loan Close Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, null, null);
        startGroup();
        addButton("Day End", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), ElementPriority.TOP, com.mac.zsystem.transaction.day_end.SystemDayEnd.class, null);
       
//        startGroup();
      //  addButton("Loan Vehicle", FinacResources.getImageIconURL(ACTION_ACCEPT), ElementPriority.LOW, com.mac.loan.vehicle.REGVehicleAdding.class, null);
//        addBand("Insurance", FinacResources.getImageIconURL(LOAN));
//        addButton("Insurance Chargs", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.insurance.CpnelInstallmentCharg.class, null);
        addBand("Opening Loan", FinacResources.getImageIconURL(LOAN));
        addButton("Opening Loan", FinacResources.getImageIconURL(OPENING_LOAN2), DefaultMainframe.ElementPriority.TOP, com.mac.loan.opening_loan.TRAOpeningLoanApplication.class, null);
        
        addBand("Loan Suspend", FinacResources.getImageIconURL(LOAN));
        addButton("Write Off", FinacResources.getImageIconURL(TRANSACTION_s), DefaultMainframe.ElementPriority.TOP, com.mac.loan.write_off.REGWriteOff.class, null);

    }
    
    public void createMicroFinance(){
         addTask("Micro Finance");
         addBand("Loan Afford", FinacResources.getImageIconURL(LOAN));
         startGroup();
         addButton("Center Creation", FinacResources.getImageIconURL(MICRO_CENTER), DefaultMainframe.ElementPriority.TOP, com.mac.micro_finance.center.REGCenter.class, null);
         addButton("Group Creation", FinacResources.getImageIconURL(MICRO_GROUP), DefaultMainframe.ElementPriority.TOP, com.mac.registration.loan_group.REGLoanGroup.class, null);
        addButton("Add New Member", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.registration.client.REGClientMC.class, null);
        addButton("Member Approval", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.client.ClientApprovalMC.class, null);
        addButton("Member Information", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.LOW, com.mac.registration.client.ClientInformation.class, null);
        addButton("Member Transfer", FinacResources.getImageIconURL(REGISTRATION_PEOPLE), DefaultMainframe.ElementPriority.LOW, com.mac.registration.clientChanges.REGClientChangeMC.class, null);
      
        addBand("Loan Application", FinacResources.getImageIconURL(LOAN));
         startGroup();
          addButton("Advance Loan", FinacResources.getImageIconURL(LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_application_ql.TRALoanApplication.class, null);
          addButton("Advance Loan Disbursement", FinacResources.getImageIconURL(OPEN_LOAN), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_application_ql.loan_start.TRALoan.class, null);
      
         
    }

    private void createCashier() {
        addTask("Cashier");
        addBand("Cashier", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));

        startGroup();
        addButton("Temperary Receipt", FinacResources.getImageIconURL(LOAN_TEMPERERY_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.temperary_receipt.TemperaryReceipt.class, null);
        addButton("Temperary Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.temperary_receipt.TempReceiptCancel.class, null);

        startGroup();
        addButton("Cash Receipt", FinacResources.getImageIconURL(RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.receipt.Receipt.class, null);
        addButton("Group Receipt", FinacResources.getImageIconURL(LOAN_RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.group_receipt.Receipt.class, null);
        addButton("Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.receipt.ReceiptCancel.class, null);
        addButton("Bank Receipt Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.bank_deposit.ReceiptCancel.class, null);
        addButton("Advance Payment Cancel", ApplicationResources.getResource(ApplicationResources.ACTION_DELETE), DefaultMainframe.ElementPriority.LOW, com.mac.loan.loan_advance_payment.AdvancePaymentCancel.class, null);

        startGroup();
        addButton("Advance Payment", FinacResources.getImageIconURL(RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_advance_payment.PCAdvancePayment.class, null);
        addButton("Advance Receipt", FinacResources.getImageIconURL(RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_advance_payment.AdvanceReceipt.class, null);
        addButton("Advance Refund", FinacResources.getImageIconURL(RECEIPT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_advance_payment.refund.PCAdvanceRefund.class, null);
//        startGroup();
//        addButton("3 Present Receipt", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.loan.present_receipt.PreReceipt.class, null);

        startGroup();
        addButton("Cash Denomination", FinacResources.getImageIconURL(CASHIER), ElementPriority.TOP, com.mac.account.cash_denomination.CashDenominationPanel.class, null);
        addBand("Other", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addButton("Cashier Close", FinacResources.getImageIconURL(CASHIER), ElementPriority.TOP, com.mac.account.cashier_closing.CashierClosing.class, null);
        startGroup();
        addButton("Fund Registration", FinacResources.getImageIconURL(MICRO_FUND), DefaultMainframe.ElementPriority.TOP, com.mac.account.fund_registration.FundRgistration.class, null);
    
        startGroup();
        addButton("Branch Summary", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_ISSUE), DefaultMainframe.ElementPriority.TOP, com.mac.account.branch_summary.BranchSummaryPanel.class, null);
        addButton("Branch Summary Approve", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_ISSUE), DefaultMainframe.ElementPriority.TOP, com.mac.account.branch_summary.BranchSummaryReportApproval.class, null);
    
//        addButton("Customer Payments", FinacResources.getImageIconURL(APPLICATION_MENU_TASK_MANAGER), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_customer_payment.REGCustomerLoanPayment.class, null);
//        addButton("Customer Payments 2", FinacResources.getImageIconURL(APPLICATION_MENU_TASK_MANAGER), DefaultMainframe.ElementPriority.TOP, com.mac.loan.loan_customer_payment2.REGCustomerLoanPayment.class, null);
    }
    
      private void createMobile() {
        addTask("Mobile");
        addBand("Mobile Cash", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
         addButton("Mobile Cash View", FinacResources.getImageIconURL(REGISTRATION_CLIENT), DefaultMainframe.ElementPriority.TOP, com.mac.cash_collection.template.CashCollection.class, null);
    
    }

    private void createAccount() {
        addTask("Account");
        addBand("Registration", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));

        addBand("Account Registration", FinacResources.getImageIconURL(REGISTRATION_ACCOUNT));
        addButton("Account Category", FinacResources.getImageIconURL(REGISTRATION_ACCOUNT_CATEGORY), DefaultMainframe.ElementPriority.LOW, com.mac.registration.account_category.REGAccountCategory.class, null);
        addButton("Account", FinacResources.getImageIconURL(REGISTRATION_ACCOUNT), DefaultMainframe.ElementPriority.LOW, com.mac.registration.account.REGAccount.class, null);

        addBand("Account Transaction", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        startGroup();
        addButton("Opening Balance", FinacResources.getImageIconURL(LOAN_OPENING_BALANCE), ElementPriority.TOP, com.mac.account.opening_balance.OpeningBalance.class, null);
        startGroup();
        addButton("Bank Deposit", FinacResources.getImageIconURL(BANK_DEPOSIT), ElementPriority.LOW, com.mac.account.bank_deposit.BankDeposit.class, null);
        addButton("Bank Charg Entry", FinacResources.getImageIconURL(BANK_DEPOSIT), ElementPriority.LOW, com.mac.account.bank_entry.BankChargEntry.class, null);
        addButton("Bank Charg Cancel", FinacResources.getImageIconURL(CANCEL), ElementPriority.LOW, com.mac.account.bank_entry.BankChargEntryCancel.class, null);
//        startGroup();
//        addButton("General Receipt", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.general_receipt.GeneralReceipt.class, null);
//        addButton("General Voucher", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.general_voucher.GeneralVoucher.class, null);
//        addButton("Petty Cash Voucher ", FinacResources.getImageIconURL(LOAN_MULTI_TRANSACTION), ElementPriority.TOP, com.mac.account.general_voucher_2.GeneralVoucher.class, null);
//        addButton("General Voucher Approve", FinacResources.getImageIconURL(LOAN_APPROVAL), ElementPriority.TOP, com.mac.account.general_voucher.GeneralVoucherApproval.class, null);
//        
        startGroup();
        addButton("Journal", FinacResources.getImageIconURL(LOAN_JOURNAL), ElementPriority.TOP, com.mac.account.journal.Journel.class, null);
        addButton("Bank Reconsilation", FinacResources.getImageIconURL(LOAN_JOURNAL), ElementPriority.TOP, com.mac.bank_rec.REGBankRec.class, null);

        addBand("Cheque Handling", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        startGroup();
        addButton("Cheque Deposit", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), ElementPriority.LOW, com.mac.account.cheque_deposit.ChequeDeposit.class, null);
        addButton("Cheque Realize", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), ElementPriority.LOW, com.mac.account.cheque_realize.ChequeRealize.class, null);
        addButton("Cheque Return", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_ISSUE), ElementPriority.LOW, com.mac.account.cheque_return.ChequeReturn.class, null);
        startGroup();
        addButton("Cheque Issue", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_REALIZE), ElementPriority.LOW, com.mac.account.cheque_issue.ChequeIssue.class, null);    
        addButton("Issue Cheque Return", FinacResources.getImageIconURL(ACCOUNT_CHEQUE_ISSUE), ElementPriority.LOW, com.mac.account.cheque_issue_return.IssueChequeReturn.class, null);
        startGroup();
        addButton("Bank", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.LOW, com.mac.registration.bank.REGBank.class, null);
        addButton("Branch", FinacResources.getImageIconURL(REGISTRATION_BANK_BRANCH), ElementPriority.LOW, com.mac.registration.bank_branch.REGBanckBranch.class, null);
        addButton("Bank Account", FinacResources.getImageIconURL(REGISTRATION_BANK_BRANCH), ElementPriority.LOW, com.mac.registration.bank_account.REGBankAccount.class, null);
        addBand("Company  Handling", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        startGroup();
         addButton("General Receipt Cancel", FinacResources.getImageIconURL(CANCEL), ElementPriority.LOW, com.mac.account.general_receipt.GenaralReceiptCancel.class, null);
         addButton("General Voucher Cancel", FinacResources.getImageIconURL(CANCEL), ElementPriority.LOW, com.mac.account.general_voucher.GenaralVoucherCancel.class, null);
         addButton("Petty Cash Voucher Cancel", FinacResources.getImageIconURL(CANCEL), ElementPriority.LOW, com.mac.account.general_voucher_2.GenaralVoucher2Cancel.class, null);
       // addButton("Budget", FinacResources.getImageIconURL(REGISTRATION_BANK_BRANCH), ElementPriority.LOW, com.mac.account.budget.REGBudget.class, null);
      //  addButton("Target", FinacResources.getImageIconURL(REGISTRATION_BANK_BRANCH), ElementPriority.LOW, com.mac.account.target.REGTarget.class, null);
    }

    private void createSystem() {
        addTask("System");
        addBand("Transaction Settings", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        startGroup();
        addButton("Account Settings", FinacResources.getImageIconURL(ACCOUNTT_SETTING), ElementPriority.TOP, com.mac.zsystem.transaction.account.account_setting.gui.SystemAccountSettings.class, null);
        addButton("Payment Settings", FinacResources.getImageIconURL(PAYMENT_SETTING1), ElementPriority.TOP, com.mac.zsystem.transaction.payment.payment_setting.gui.SystemPaymentSettings.class, null);
        addButton("Transaction Settings", FinacResources.getImageIconURL(TRANSACTION_SETTING), ElementPriority.TOP, com.mac.zsystem.settings.transaction_settings.TransactionSettings.class, null);

        addBand("Application Settings", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        startGroup();
        addButton("Configuration", FinacResources.getImageIconURL(SETTING), ElementPriority.TOP, com.mac.zsystem.settings.settings.Settings.class, null);
        addButton("Theme Selector", FinacResources.getImageIconURL(APPLICATION_MENU_THEME), ElementPriority.TOP, com.mac.zsystem.settings.theme_chooser.Theme.class, null);

        addBand("Other Settings", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
//        addButton("Remind Letter Templates", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.registration.remind_letter.REGTemplateRegistration.class, null);
//        addButton("Remind Letter Generator", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zutil.remind_letter.LetterGenerator.class, null);
       //Royan
        addButton("Remind Letter Generator", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zutil.royan_remind_letter.LetterGenerator.class, null);
        addButton("Loan Summery", FinacResources.getImageIconURL(REPORT), ElementPriority.TOP, com.mac.zreport.special_report_view.SummeryReport.class, null);

        addBand("Dash Boards", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addButton("Recovery Dash Board", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.dash_board.recovery.RecoveryDashBoard.class, null);
        addButton("Client Information", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.dash_board.client.ClientInformationDashBoard.class, null);

        addBand("Permission", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addButton("User Role", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zsystem.permission.user_role.REGUserRole.class, null);
        addButton("Module", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zsystem.permission.permission.REGPermission.class, null);
        addButton("User Role Permission", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zsystem.permission.user_role_permission.REGUserRolePermission.class, null);
        addButton("Backup", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.af.templates.system.backup.BackupRestore.class, null);

        addButton("Report Registration", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.report_registration.REGReportRegistration.class, null);
        addButton("Report Permission", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.report_permission.REGUserRoleReportPermission.class, null);
    }

    private void createReports2() {
        addTask("Report");
        addBand("Reports", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addButton("Loan Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.LoanReports.class, null);
        addButton("Account Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.AccountReports.class, null);
        addButton("Cahier Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.CashierReports.class, null);
        addButton("Administration Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.AdministrationReports.class, null);
       // addButton("Registration Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.RegistrationReport.class, null);
        addButton("Arreas Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.RegistrationReport.class, null);
       // addButton("Creditor Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.SavingReport.class, null);

    }
    private void createSaving() {
        addTask("Creditors");
        addBand("Creditors", FinacResources.getImageIconURL(SARVING));
        addButton("Type", FinacResources.getImageIconURL(SARVING_TYPE), ElementPriority.TOP, com.mac.sarving.saving_type.REGSaving_type.class, null);
        addButton("Schema", FinacResources.getImageIconURL(SARVING_SCHEMA), ElementPriority.TOP, com.mac.sarving.saving_schema.REGSaving_Schema.class, null);
        addButton("Account", FinacResources.getImageIconURL(SARVING_ACCOUNT), ElementPriority.TOP, com.mac.sarving.saving_account.REGSaving_Account.class, null);
        addButton("Account Start", FinacResources.getImageIconURL(LOAN), ElementPriority.TOP, com.mac.sarving.saving_start.SSavingStart.class, null);
        addButton("Account Close", FinacResources.getImageIconURL(CANCEL), ElementPriority.LOW, com.mac.sarving.savingclose.SSavingClose.class, null);
        addBand("Transaction SReceipt", FinacResources.getImageIconURL(TRANSACTION_s));
        addButton("SReceipt", FinacResources.getImageIconURL(SARVING_DEPOSIT), ElementPriority.TOP, com.mac.sarving.saving_deposit.PcSDeposit.class, null);
        addButton("SReceipt Cancel", FinacResources.getImageIconURL(CANCEL), ElementPriority.LOW, com.mac.sarving.saving_deposit.SvDepositCancel.class, null);
       
        addBand("Transaction SPayment", FinacResources.getImageIconURL(TRANSACTION_s));
        addButton("SPayment", FinacResources.getImageIconURL(SARVING_WITHDRAW), ElementPriority.TOP, com.mac.sarving.saving_withdraw.PcSWithdraw.class, null);
        addButton("SPayment Cancel", FinacResources.getImageIconURL(CANCEL), ElementPriority.LOW, com.mac.sarving.saving_withdraw.SvWithdrawCancel.class, null);
              
        addBand("Authorization", FinacResources.getImageIconURL(TRANSACTION_s));
        addButton("Creditors_Authorization1", FinacResources.getImageIconURL(AUTHORIZATION), ElementPriority.TOP, com.mac.sarving.SAccountApprove.SAccountApproval.class, null);
        addButton("Creditors_Authorization2", FinacResources.getImageIconURL(AUTHORIZATION), ElementPriority.TOP, com.mac.sarving.SAccountApprove.SAccountApprovalL1.class, null);
        
        addBand("other", FinacResources.getImageIconURL(TRANSACTION_s));
        addButton("Creditors_rebate", FinacResources.getImageIconURL(LOAN_REBIT), ElementPriority.TOP, com.mac.sarving.rebate.PcSRebate.class, null);
        addButton("Creditors_chage", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES), ElementPriority.TOP, com.mac.sarving.charges.PcSCharges.class, null);
        
    }

//      private void createReports3() {
//        addTask("Report");
//        addBand("Reports", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
//        addButton("Master Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.MasterReports.class, null);
//        addButton("Loan Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.LoanReports.class, null);
//        addButton("Receipt Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.ReceiptReports.class, null);
//        addButton("Recovery Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.RecoveryReports.class, null);
//        addButton("Account Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.AccountReports.class, null);
//        addButton("Cahier Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.CashierReports.class, null);
//        addButton("Administration Reports", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.zreport.reports_viewers.AdministrationReports.class, null);
//
//    }
    private void createDashBoard() {
        addTask("Dash Board");
        addBand("Dash Board", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT));
        addButton("Client ", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.TOP, com.mac.registration.client.ClientInformation.class, null);
        addButton("Agrement Details", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT), DefaultMainframe.ElementPriority.TOP, com.mac.agrement_details.PCAgrementDetailss.class, null);
        addButton("Recovery Visit", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT), DefaultMainframe.ElementPriority.TOP, com.mac.recovery_visit.PCRecoveryVisit.class, null);
//        addButton("Client Remind", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT), DefaultMainframe.ElementPriority.TOP, com.mac.dash_board.customer_remind.PCCustomerRemind.class, null);
        addButton("Client Remind", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT), DefaultMainframe.ElementPriority.TOP, com.mac.dash_board.customer_remind.PCCustomerRemind1.class, null);
        addButton("Receipt Approve", FinacResources.getImageIconURL(LOAN_APPROVAL), DefaultMainframe.ElementPriority.TOP, com.mac.loan.receipt_approve.ReceiptApproval.class, null);

        addBand("Admin Dash Boards", FinacResources.getImageIconURL(LOAN_OTHER_CHARGES));
        addButton("Recovery Dash Board", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.dash_board.recovery.RecoveryDashBoard.class, null);
        addButton("Client Information", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.dash_board.client.ClientInformationDashBoard.class, null);
//        addButton("Client Full Details", FinacResources.getImageIconURL(REGISTRATION_BANK), ElementPriority.TOP, com.mac.dash_board.customer_details.ClientFulldetails.class, null);
        addButton("Loan Document View", FinacResources.getImageIconURL(LOAN_APPLICATION), ElementPriority.TOP, com.mac.loan.loan_document_view.PCDocumentView.class, null);
//        addButton("Vehicle Valuation", FinacResources.getImageIconURL(VEHICLE_VALUATION), ElementPriority.TOP, com.mac.zutil.vehicle_valuation.Valuation.class, null);
       // addButton("CLEAN", FinacResources.getImageIconURL(APPLICATION_MENU_CONFIG), ElementPriority.TOP, com.finac.loan.killProcess.class, null);
    }

//    private void createUtility() {
//        addTask("Utility");
////        addBand("Notification", FinacResources.getImageIconURL(REGISTRATION_CASHIER_POINT));
////        addButton("Email", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.TOP, com.mac.notification.e_mail.REGMailSettings.class, null);
////        addButton("SMS", FinacResources.getImageIconURL(REGISTRATION_LOAN_GROUP), DefaultMainframe.ElementPriority.TOP, com.mac.notification.sms.REGSmsAlert.class, null); 
//    }
    
    private void createApplicationMenu() {
        startApplicationMenu();
        addPrimaryMenu("Theme Selector", FinacResources.getImageIconURL(APPLICATION_MENU_THEME), Theme.class, null);
        addPrimaryMenu("Configuration", FinacResources.getImageIconURL(APPLICATION_MENU_CONFIG), Theme.class, null);
        addPrimaryMenu("Cache Manager", FinacResources.getImageIconURL(APPLICATION_MENU_CACHE_MANAGER), com.mac.af.core.database.hibernate.ui.cache_manager.CacheManager.class, null);
        addPrimaryMenu("Task Manager", FinacResources.getImageIconURL(APPLICATION_MENU_TASK_MANAGER), Theme.class, null);
    }
    private void createFinalizeApplicationMenu(){
        
        
        
    }

    private void createReports() {
        List<ReportTask> reportTasks = ReportUtil.getReportTasks();

        for (ReportTask reportTask : reportTasks) {
            addTask(ReportUtil.getFormattedString(reportTask.getName()));
            for (ReportBand reportBand : reportTask.getReportBands()) {
                addBand(ReportUtil.getFormattedString(reportBand.getName()), FinacResources.getImageIconURL(FinacResources.REPORT));
                for (ReportFile reportFile : reportBand.getReportFiles()) {
                    addButton(ReportUtil.getFormattedString(reportFile.getName()), FinacResources.getImageIconURL(REPORT), ElementPriority.LOW, com.mac.zreport.ReportPanel.class, reportFile.getReportFile());
                
                }
            }
        }
//        for (ReportTask reportTask : reportTasks) {
//            addTask(ReportUtil.getFormattedString(reportTask.getName()));
//            for (ReportBand reportBand : reportTask.getReportBands()) {
//                addBand(reportBand.getName(), FinacResources.getImageIconURL(FinacResources.REPORT));
//                for (ReportFile reportFile : reportBand.getReportFiles()) {
//                    addButton(reportFile.getName(), FinacResources.getImageIconURL(REPORT), ElementPriority.LOW, com.mac.zsystem.report.ReportPanel.class, reportFile.getReportFile());
//                }
//            }
//        }
    }

    private TransactionType getTransaction(String code) {
        HibernateDatabaseService databaseService = CPanel.GLOBAL.getDatabaseService();

        TransactionType transactionType = null;
        try {
            transactionType = (TransactionType) databaseService.getObject(TransactionType.class, code);
        } catch (DatabaseException ex) {
            Logger.getLogger(FinacMainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        return transactionType;
    }
}
