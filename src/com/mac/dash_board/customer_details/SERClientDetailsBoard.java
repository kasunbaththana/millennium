/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.dash_board.customer_details;

import com.mac.dash_board.client.*;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.dash_board.client.object2.Loan;
import com.mac.dash_board.client.object2.LoanStatement;
import com.mac.dash_board.client.object2.Settlement;
import com.mac.dash_board.client.object2.SettlementHistory;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class SERClientDetailsBoard extends AbstractService {

    public SERClientDetailsBoard(Component component) {
        super(component);
    }
 public List<Loan> getLoans() {
        List<Loan> loan;
        try {
            String hql = "FROM com.mac.dash_board.client.object2.Loan WHERE status='LOAN_START' OR status='LOAN_SUSPEND' ";
            loan = getDatabaseService().getCollection(hql);
//            loan = getDatabaseService().getCollection(hql);
            
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }
 public List<Settlement> getLoanSttlement(int loanindex) {
        List<Settlement> loan;
        try {
            String hql = "FROM com.mac.dash_board.client.object2.Settlement WHERE loan='"+loanindex+"' ";
            loan = getDatabaseService().getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }
 public List<SettlementHistory> getLoanSttlement_history(int loanindex) {
          List<SettlementHistory> loan;
     try {
            String hql =    "SELECT h.* FROM `settlement_history` h \n" +
                            "LEFT JOIN `settlement` s ON h.settlement=s.index_no\n" +
                            "WHERE s.loan = '"+loanindex+"' \n"
                        + "  and (s.settlement_type='LOAN_CAPITAL' \n"
                    + "OR s.settlement_type='LOAN_INTEREST' \n"
                    + "OR s.settlement_type='LOAN_PANALTY' )     ";
            HibernateSQLQuery sqlQuery = new HibernateSQLQuery(hql, SettlementHistory.class);
             loan = getDatabaseService().getCollection(sqlQuery, null);
//           List<SettlementHistory>   loan = getDatabaseService().getCollection(SettlementHistory.class)
                   
//                     .add(Restrictions.eq("loan", ""));
//         loan = getDatabaseService().getCollection(SettlementHistory.class, Collections.singletonMap("loan", loanindex));
//         
//        Settlement settlement=(Settlement) getDatabaseService().initCriteria(Settlement.class)
//                 .add(Restrictions.eq("loan", loanindex));
//         
//         
//         loan =  (List<SettlementHistory>) getDatabaseService().initCriteria(SettlementHistory.class)
//                 .add(Restrictions.eq("settlement", settlement));


        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }

    public List<LoanStatement> getLoanStatements(Loan loan) {
        String sql = "SELECT\n"
                + "	transaction.index_no,\n"
                + "	settlement.due_date AS `date`,\n"
                + "	IF(settlement.settlement_type='LOAN_PANALTY',settlement.description,'Loan Installation') as description,\n"
                + "	'' AS reference_no,\n"
                + "	SUM(settlement.amount) AS debit,\n"
                + "	0.0 AS credit\n"
                + "FROM\n"
                + "	settlement\n"
                + "	LEFT JOIN transaction ON transaction.index_no = settlement.transaction\n"
                + "	LEFT JOIN  `loan` l ON l.`index_no`=settlement.`loan` \n"
                + "WHERE\n"
                + "	(settlement.settlement_type='LOAN_CAPITAL' OR settlement.settlement_type='LOAN_INTEREST' OR settlement.settlement_type='LOAN_PANALTY' )\n"
                + "	AND settlement.`status`<>'CANCEL'\n"
                + "	AND l.`status`='LOAN_START' \n"
                + "	AND settlement.amount <> 0.0\n"
                + "	AND settlement.loan = :LOAN\n"
                + "\n"
                + "GROUP BY settlement.due_date,settlement.transaction\n"
                + "\n"
                + "UNION\n"
                + "\n"
                + "#RECEIPTS\n"
                + "SELECT\n"
                + "	transaction.index_no,\n"
                + "	transaction.transaction_date AS `date`,\n"
                + "	'Receipt' AS description,\n"
                + "	transaction.reference_no,\n"
                + "	0.0 AS debit,\n"
                + "	(IFNULL(SUM(settlement_history.settlement_amount),0.0) + IFNULL((SELECT SUM(st1.amount) FROM settlement st1 WHERE st1.transaction = settlement_history.`transaction`),0.0)) AS credit\n"
                + "FROM\n"
                + "	`transaction`\n"
                + "	LEFT JOIN settlement_history ON settlement_history.`transaction` = `transaction`.index_no\n"
                +"      LEFT JOIN settlement s ON s.index_no = settlement_history.`settlement`  \n"
                + "	LEFT JOIN `loan` l ON l.`index_no`=s.`loan` \n"
                + "WHERE\n"
                + "	( transaction.transaction_type = 'RECEIPT' OR transaction.transaction_type = 'BANK_RECEIPT' OR transaction.transaction_type = 'CHEQUE_RECEIPT' ) \n"
                + "     and transaction.`note` NOT IN ('DOWN PAYMENT','DOCUMENT CHARGES','INSUARANCE CHARGE','RMV CHARGERS','TRANCPORT CHARGERS') \n"
                + "	AND transaction.status <> 'CANCEL'\n"
                  + "	AND l.`status`='LOAN_START' \n"
                 + "	and s.settlement_type='APPLICATION_CHARGE'        \n"
                + "	AND transaction.loan = :LOAN\n"
                + "GROUP BY\n"
                + "	transaction.index_no\n"
                + "HAVING\n"
                + "	credit <> 0.0\n"
                + "\n"
                + "\n"
                + "ORDER BY `date`";


        HibernateSQLQuery sqlQuery = new HibernateSQLQuery(sql, LoanStatement.class);
        try {
            List<LoanStatement> loanStatements = getDatabaseService().getCollection(sqlQuery, Collections.singletonMap((String) "LOAN", (Object) loan.getIndexNo()));

            return loanStatements;
        } catch (DatabaseException ex) {
            Logger.getLogger(SERClientInformationDashBoard.class.getName()).log(Level.SEVERE, null, ex);

            return new ArrayList();
        }
    }

//    

   
}
