/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.dash_board.client;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.dash_board.client.object2.Client;
import com.mac.dash_board.client.object2.Loan;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author user
 */
public class LoanHistoryPanel extends javax.swing.JPanel {

    /**
     * Creates new form LoanHistoryPanel
     */
    public LoanHistoryPanel(
            ClientInformationDashBoard clientInformationDashBoard,
            SERClientInformationDashBoard serClientInformationDashBoard) {
        this.clientInformationDashBoard = clientInformationDashBoard;
        this.serClientInformationDashBoard = serClientInformationDashBoard;
        initComponents();
        initOthers();
    }

    public void setClient(Client client) {
        List<Loan> loans = serClientInformationDashBoard.getLoansByClient(client);

        tblLoans.setCValue(loans);

        setLoanTotal(loans);
    }

    public void setLoan(Loan loan) {
        if (loan != null) {
            clientInformationDashBoard.getLoanInformationPanel().setLoan(loan);
        }
    }

    private void setLoanTotal(List<Loan> loans) {
        double loanAmount = 0.0;
        double loanCapital = 0.0;
        double loanInterest = 0.0;
        double loanPanalty = 0.0;

        for (Loan loan : loans) {
            loanAmount += loan.getLoanAmount();
            loanCapital += loan.getLoanCapital();
            loanInterest += loan.getLoanInterest();
            loanPanalty += loan.getLoanPanalty();
        }

        txtLoanAmount.setCValue(loanAmount);
        txtLoanCapital.setCValue(loanCapital);
        txtLoanInterest.setCValue(loanInterest);
        txtLoanPenalty.setCValue(loanPanalty);

        //pie data
        DefaultPieDataset pieDataset = new DefaultPieDataset();
        pieDataset.setValue("LOAN CAPITAL", loanCapital);
        pieDataset.setValue("LOAN INTEREST", loanInterest);
        pieDataset.setValue("LOAN PENALTY", loanPanalty);

        ((PiePlot) chart.getPlot()).setDataset(pieDataset);
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        tblLoans.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Agreement No.", new String[]{"agreementNo"}),
            new CTableColumn("Status", new String[]{"status"}),
            new CTableColumn("Loan Amount", new String[]{"loanAmount"}),
            new CTableColumn("Loan Capital", new String[]{"loanCapital"}),
            new CTableColumn("Loan Capital Balance", new String[]{"loanCapitalBalance"}),
            new CTableColumn("Loan Interest", new String[]{"loanInterest"}),
            new CTableColumn("Loan Interest Balance", new String[]{"loanInterestBalance"}),
            new CTableColumn("Loan Penalty", new String[]{"loanPanalty"}),
            new CTableColumn("Loan Penalty Balance", new String[]{"loanPanaltyBalance"}),
            new CTableColumn("Installment Amount", new String[]{"installmentAmount"}),
            new CTableColumn("Installment Count", new String[]{"installmentCount"}),
            new CTableColumn("Last Due", new String[]{"lastDueDate"})
        }));

        txtLoanAmount.setValueEditable(false);
        txtLoanCapital.setValueEditable(false);
        txtLoanInterest.setValueEditable(false);
        txtLoanPenalty.setValueEditable(false);

        tblLoans.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!tblLoans.getSelectionModel().isSelectionEmpty()) {
                    Loan loan = (Loan) tblLoans.getValueAt(tblLoans.getSelectedRow());
                    setLoan(loan);
                } else {
                    setLoan(null);
                }
            }
        });

        pnlSettlementTotal.setLayout(new BoxLayout(pnlSettlementTotal, BoxLayout.LINE_AXIS));
        chart = serClientInformationDashBoard.createEmptyChart("SETTLEMENT TOTAL");
        pnlSettlementTotal.add(new ChartPanel(chart));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel8 = new javax.swing.JPanel();
        pnlSettlementTotal = new javax.swing.JPanel();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanCapital = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtLoanInterest = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanPenalty = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblLoans = new com.mac.af.component.derived.input.table.CITable();

        jSplitPane1.setDividerLocation(300);

        javax.swing.GroupLayout pnlSettlementTotalLayout = new javax.swing.GroupLayout(pnlSettlementTotal);
        pnlSettlementTotal.setLayout(pnlSettlementTotalLayout);
        pnlSettlementTotalLayout.setHorizontalGroup(
            pnlSettlementTotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pnlSettlementTotalLayout.setVerticalGroup(
            pnlSettlementTotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 106, Short.MAX_VALUE)
        );

        cDLabel3.setText("Total Loan Amount:");

        cDLabel4.setText("Total Capital:");

        cDLabel5.setText("Total Interest:");

        cDLabel6.setText("Total Penalty:");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLoanCapital, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                    .addComponent(txtLoanInterest, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                    .addComponent(txtLoanPenalty, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(pnlSettlementTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlSettlementTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanCapital, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanInterest, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanPenalty, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58))
        );

        jSplitPane1.setLeftComponent(jPanel8);

        tblLoans.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblLoans);

        jSplitPane1.setRightComponent(jScrollPane2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 546, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel pnlSettlementTotal;
    private com.mac.af.component.derived.input.table.CITable tblLoans;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanCapital;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanInterest;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanPenalty;
    // End of variables declaration//GEN-END:variables
    private ClientInformationDashBoard clientInformationDashBoard;
    private SERClientInformationDashBoard serClientInformationDashBoard;
    private JFreeChart chart;
}
