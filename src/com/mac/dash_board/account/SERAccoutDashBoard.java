/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.dash_board.account;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.dash_board.client.object2.Client;
import com.mac.dash_board.client.object2.Loan;
import com.mac.dash_board.client.object2.LoanStatement;
import com.mac.loan.LoanStatus;
import java.awt.Color;
import java.awt.Component;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author user
 */
public class SERAccoutDashBoard extends AbstractService {

    public SERAccoutDashBoard(Component component) {
        super(component);
    }

    public List<Client> findClients(
            String code,
            String nicNo,
            String name,
            String address,
            String telephone,
            String eMail,
            //loan information
            String agreementNo,
            String referenceNo,
            String documentNo,
            Double loanAmountLT,
            Double loanAmountGT,
            //document information
            String description,
            String note,
            boolean findAnyletter) {

        try {
            Criteria criteria = getDatabaseService().initCriteria(Client.class);

            if (code != null ? !code.isEmpty() : false) {
                criteria.add(Restrictions.like("code", getLikeValue(code, findAnyletter)));
            }
            if (nicNo != null ? !nicNo.isEmpty() : false) {
                criteria.add(Restrictions.like("nicNo", getLikeValue(nicNo, findAnyletter)));
            }
            if (name != null ? !name.isEmpty() : false) {
                criteria.add(Restrictions.like("name", getLikeValue(name, findAnyletter)));
            }
            if (address != null ? !address.isEmpty() : false) {
                criteria.add(Restrictions.or(Restrictions.like("addressLine1", getLikeValue(address, findAnyletter)), Restrictions.or(Restrictions.like("addressLine2", getLikeValue(address, findAnyletter)), Restrictions.like("addressLine3", getLikeValue(address, findAnyletter)))));
            }

            if (telephone != null ? !telephone.isEmpty() : false) {
                criteria.add(Restrictions.or(Restrictions.like("mobile", getLikeValue(telephone, findAnyletter)), Restrictions.or(Restrictions.like("telephone1", getLikeValue(telephone, findAnyletter)), Restrictions.like("telephone2", getLikeValue(telephone, findAnyletter)))));
            }

            if (eMail != null ? !eMail.isEmpty() : false) {
                criteria.add(Restrictions.like("name", getLikeValue(eMail, findAnyletter)));
            }

//            if ((agreementNo != null ? !agreementNo.isEmpty() : false)
//                    || (referenceNo != null ? !referenceNo.isEmpty() : false)
//                    || (documentNo != null ? !documentNo.isEmpty() : false)
//                    || (loanAmountLT != null ? loanAmountLT.doubleValue() != 0.0 : false)
//                    || (loanAmountGT != null ? loanAmountGT.doubleValue() != 0.0 : false)) {
            criteria.createAlias("loans", "loan", Criteria.LEFT_JOIN);
            criteria.createAlias("loan.documents", "document", Criteria.LEFT_JOIN);
//            }

            if ((agreementNo != null ? !agreementNo.isEmpty() : false)) {
                criteria.add(Restrictions.like("loan.agreementNo", getLikeValue(agreementNo, findAnyletter)));
            }

            if ((referenceNo != null ? !referenceNo.isEmpty() : false)) {
                criteria.add(Restrictions.like("loan.loanReferenceNo", getLikeValue(referenceNo, findAnyletter)));
            }

            if ((documentNo != null ? !documentNo.isEmpty() : false)) {
                criteria.add(Restrictions.like("loan.loanDocumentNo", getLikeValue(documentNo, findAnyletter)));
            }

            if ((loanAmountLT != null ? loanAmountLT.doubleValue() != 0.0 : false)) {
                criteria.add(Restrictions.lt("loan.loanAmount", loanAmountLT));
            }

            if ((loanAmountGT != null ? loanAmountGT.doubleValue() != 0.0 : false)) {
                criteria.add(Restrictions.gt("loan.loanAmount", loanAmountGT));
            }

            //document
            if ((description != null ? !description.isEmpty() : false)) {
                criteria.add(Restrictions.like("document.description", getLikeValue(description, findAnyletter)));
            }

            if ((note != null ? !note.isEmpty() : false)) {
                criteria.add(Restrictions.like("document.note", getLikeValue(note, findAnyletter)));
            }

            return criteria.list();
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAccoutDashBoard.class.getName()).log(Level.SEVERE, null, ex);

            return new ArrayList<>();
        }
    }

    private String getLikeValue(String value, boolean findAnyLetter) {
        if (findAnyLetter) {
            return "%" + value + "%";
        } else {
            return value;
        }
    }

    public List<Loan> getLoansByClient(Client client) {
        try {
            Criteria criteria = getDatabaseService().initCriteria(Loan.class);

            criteria.add(Restrictions.eq("client", client));
            criteria.add(Restrictions.eq("status", LoanStatus.LOAN_START));

            return criteria.list();
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAccoutDashBoard.class.getName());
            return new ArrayList();
        }
    }

    public List<Loan> getLoansByGuarantor(Client client) {
        try {

            String sql = "SELECT \n"
                    + "	loan.*\n"
                    + "FROM \n"
                    + "	loan\n"
                    + "	LEFT JOIN loan_guarantor ON loan_guarantor.loan = loan.index_no\n"
                    + "WHERE\n"
                    + "	loan_guarantor.guarantor IN ('"+client.getCode()+"');";
            System.out.println(sql);
            HibernateSQLQuery hibernateSQLQuery =new HibernateSQLQuery(sql, Loan.class);
            
            return getDatabaseService().getCollection(hibernateSQLQuery, new HashMap<String, Object>());
            
//            Criteria criteria = getDatabaseService().initCriteria(Loan.class);
//
//            criteria.add(Restrictions.eq("client", client));
//            criteria.add(Restrictions.eq("status", LoanStatus.LOAN_START));


//            return criteria.list();
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAccoutDashBoard.class.getName());
            return new ArrayList();
        }
    }

//    public List<Client> findClients(String text) throws SQLException {
//
//        String sql = "SELECT\n"
//                + "	code,\n"
//                + "	nic_no,\n"
//                + "	name,\n"
//                + "	CONCAT(address_line1,address_line2,address_line3) AS address,\n"
//                + "	mobile,\n"
//                + "	telephone1,\n"
//                + "	telephone2,\n"
//                + "	fax,\n"
//                + "	email,\n"
//                + "	note\n"
//                + "FROM\n"
//                + "	client "
//                + "WHERE "
//                + "     code LIKE '%" + text + "%'"
//                + "     OR nic_no LIKE '%" + text + "%' "
//                + "     OR name LIKE '%" + text + "%'";
//
//
//        List<Client> clients = new ArrayList<>();
//        Connection connection = getConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        while (resultSet.next()) {
//            Client client = new Client();
//            client.setCode(resultSet.getString("code"));
//            client.setNicNo(resultSet.getString("nic_no"));
//            client.setName(resultSet.getString("name"));
//            client.setAddress(resultSet.getString("address"));
//            client.setMobile(resultSet.getString("mobile"));
//            client.setTelephone1(resultSet.getString("telephone1"));
//            client.setTelephone2(resultSet.getString("telephone2"));
//            client.setFax(resultSet.getString("fax"));
//            client.setEmail(resultSet.getString("email"));
//            client.setNote(resultSet.getString("note"));
//
//            clients.add(client);
//        }
//
//        closeConnection(connection);
//
//        return clients;
//    }
//
//    public List<Loan> getLoansByClient(Client client) throws SQLException {
//        String sql = "SELECT \n"
//                + "	loan.index_no,\n"
//                + "	loan_type.name AS loan_type,\n"
//                + "	loan.loan_reference_no,\n"
//                + "	loan.loan_document_no,\n"
//                + "	loan.loan_transaction_date,\n"
//                + "	loan.agreement_no,\n"
//                + "	loan.loan_amount,\n"
//                + "	loan.installment_amount,\n"
//                + "	loan.installment_count,\n"
//                + "	loan.interest_rate,\n"
//                + "	loan.loan_date,\n"
//                + "	loan.payment_term,\n"
//                + "	loan.panalty,\n"
//                + "	loan.available_receipt,\n"
//                + "	loan.available_voucher,\n"
//                + "	loan.`status`,\n"
//                + "	(SELECT SUM(settlement.amount) FROM settlement WHERE settlement.loan = loan.index_no AND settlement.settlement_type = 'LOAN_CAPITAL') AS loan_capital,\n"
//                + "	(SELECT SUM(settlement.amount) FROM settlement WHERE settlement.loan = loan.index_no AND settlement.settlement_type = 'LOAN_INTEREST') AS loan_interest,\n"
//                + "	(SELECT SUM(settlement.amount) FROM settlement WHERE settlement.loan = loan.index_no AND settlement.settlement_type = 'LOAN_PANALTY') AS loan_panalty,\n"
//                + "	(SELECT SUM(settlement.balance_amount) FROM settlement WHERE settlement.loan = loan.index_no AND settlement.settlement_type = 'LOAN_CAPITAL') AS loan_capital_balance,\n"
//                + "	(SELECT SUM(settlement.balance_amount) FROM settlement WHERE settlement.loan = loan.index_no AND settlement.settlement_type = 'LOAN_INTEREST') AS loan_interest_balance,\n"
//                + "	(SELECT SUM(settlement.balance_amount) FROM settlement WHERE settlement.loan = loan.index_no AND settlement.settlement_type = 'LOAN_PANALTY') AS loan_panalty_balance,\n"
//                + "     (SELECT MAX(settlement.due_date) FROM settlement WHERE settlement.loan = loan.index_no AND settlement.settlement_type = 'LOAN_CAPITAL') AS last_due_date\n"
//                + "FROM\n "
//                + "	loan\n "
//                + "	LEFT JOIN loan_type ON loan_type.code = loan.loan_type "
//                + "WHERE"
//                + "     loan.client = ?";
//
//        List<Loan> loans = new ArrayList<>();
//
//
//        Connection connection = getConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement(sql);
//        preparedStatement.setString(1, client.getCode());
//
//        ResultSet resultSet = preparedStatement.executeQuery();
//        while (resultSet.next()) {
//            Loan loan = new Loan();
//            loan.setIndexNo(resultSet.getInt("index_no"));
//            loan.setLoanType(resultSet.getString("loan_type"));
//            loan.setLoanReferenceNo(resultSet.getString("loan_reference_no"));
//            loan.setLoanDocumentNo(resultSet.getString("loan_document_no"));
//            loan.setLoanTransactionDate(resultSet.getDate("loan_transaction_date"));
//            loan.setAgreementNo(resultSet.getString("agreement_no"));
//            loan.setLoanAmount(resultSet.getDouble("loan_amount"));
//            loan.setInstallmentAmount(resultSet.getDouble("installment_amount"));
//            loan.setInstallmentCount(resultSet.getInt("installment_count"));
//            loan.setInterestRate(resultSet.getDouble("interest_rate"));
//            loan.setLoanDate(resultSet.getDate("loan_date"));
//            loan.setPaymentTerm(resultSet.getString("payment_term"));
//            loan.setPanalty(resultSet.getBoolean("panalty"));
//            loan.setAvailableReceipt(resultSet.getBoolean("available_receipt"));
//            loan.setAvailableVoucher(resultSet.getBoolean("available_voucher"));
//            loan.setStatus(resultSet.getString("status"));
//
//            loan.setLoanCapital(resultSet.getDouble("loan_capital"));
//            loan.setLoanInterest(resultSet.getDouble("loan_interest"));
//            loan.setLoanPanalty(resultSet.getDouble("loan_panalty"));
//            loan.setLoanCapitalBalance(resultSet.getDouble("loan_capital_balance"));
//            loan.setLoanInterestBalance(resultSet.getDouble("loan_interest_balance"));
//            loan.setLoanPanaltyBalance(resultSet.getDouble("loan_panalty_balance"));
//
//            loan.setLastDueDate(resultSet.getDate("last_due_date"));
//
//            loan.setClient(client);
//
//            loans.add(loan);
//        }
//        closeConnection(connection);
//
//        return loans;
//    }
    public List<LoanStatement> getLoanStatements(Loan loan) {
        String sql = "SELECT\n"
                + "	transaction.index_no,\n"
                + "	settlement.due_date AS `date`,\n"
                + "	IF(settlement.settlement_type='LOAN_PANALTY' OR settlement.settlement_type='OTHER_CHARGE',settlement.description,'Loan Installation') as description,\n"
                + "	'' AS reference_no,\n"
                + "	SUM(settlement.amount) AS debit,\n"
                + "	0.0 AS credit\n"
                + "FROM\n"
                + "	settlement\n"
                + "	LEFT JOIN transaction ON transaction.index_no = settlement.transaction\n"
                + "WHERE\n"
                + "	(settlement.settlement_type='LOAN_CAPITAL' OR settlement.settlement_type='LOAN_INTEREST' OR settlement.settlement_type='LOAN_PANALTY' OR settlement.settlement_type='OTHER_CHARGE')\n"
                + "	AND settlement.`status`<>'CANCEL'\n"
                + "	AND settlement.amount <> 0.0\n"
                + "	AND settlement.loan = :LOAN\n"
                + "\n"
                + "GROUP BY settlement.due_date,settlement.transaction\n"
                + "\n"
                + "UNION\n"
                + "\n"
                + "#RECEIPTS\n"
                + "SELECT\n"
                + "	transaction.index_no,\n"
                + "	transaction.transaction_date AS `date`,\n"
                + "	'Receipt' AS description,\n"
                + "	transaction.reference_no,\n"
                + "	0.0 AS debit,\n"
                + "	(IFNULL(SUM(settlement_history.settlement_amount),0.0) + IFNULL((SELECT SUM(st1.amount) FROM settlement st1 WHERE st1.transaction = settlement_history.`transaction`),0.0)) AS credit\n"
                + "FROM\n"
                + "	`transaction`\n"
                + "	LEFT JOIN settlement_history ON settlement_history.`transaction` = `transaction`.index_no\n"
                + "WHERE\n"
                + "	transaction.transaction_type = 'RECEIPT'\n"
                + "	AND transaction.status <> 'CANCEL'\n"
                + "	AND transaction.loan = :LOAN\n"
                + "GROUP BY\n"
                + "	transaction.index_no\n"
                + "HAVING\n"
                + "	credit <> 0.0\n"
                + "\n"
                + "\n"
                + "ORDER BY `date`";


        HibernateSQLQuery sqlQuery = new HibernateSQLQuery(sql, LoanStatement.class);
        try {
            List<LoanStatement> loanStatements = getDatabaseService().getCollection(sqlQuery, Collections.singletonMap((String) "LOAN", (Object) loan.getIndexNo()));

            return loanStatements;
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAccoutDashBoard.class.getName()).log(Level.SEVERE, null, ex);

            return new ArrayList();
        }
    }

    /*  public List<Settlement> getSettlementsByLoan(Loan loan) throws SQLException {
     //        Component c;
     //        c.addMouseListener(new MouseAdapter() {
     //            @Override
     //            public void mouseClicked(MouseEvent e) {
     //                super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
     //            }
     //
     //            @Override
     //            public void mouseDragged(MouseEvent e) {
     //                super.mouseDragged(e); //To change body of generated methods, choose Tools | Templates.
     //            }
     //
     //            @Override
     //            public void mouseEntered(MouseEvent e) {
     //                super.mouseEntered(e); //To change body of generated methods, choose Tools | Templates.
     //            }
     //
     //            @Override
     //            public void mouseExited(MouseEvent e) {
     //                super.mouseExited(e); //To change body of generated methods, choose Tools | Templates.
     //            }
     //
     //            @Override
     //            public void mouseMoved(MouseEvent e) {
     //                super.mouseMoved(e); //To change body of generated methods, choose Tools | Templates.
     //            }
     //
     //            @Override
     //            public void mousePressed(MouseEvent e) {
     //                super.mousePressed(e); //To change body of generated methods, choose Tools | Templates.
     //            }
     //
     //            @Override
     //            public void mouseReleased(MouseEvent e) {
     //                super.mouseReleased(e); //To change body of generated methods, choose Tools | Templates.
     //            }
     //
     //            @Override
     //            public void mouseWheelMoved(MouseWheelEvent e) {
     //                super.mouseWheelMoved(e); //To change body of generated methods, choose Tools | Templates.
     //            }
     //        });
     * 
     * 

     //        JComponent component;
     //        JTextField field;
     //        field.addA
     //        JFormattedTextField formattedTextField;
     //        formattedTextField.addA
     //        JPasswordField a;
     //        a.addActionListener(null);


     String sql = "SELECT\n"
     + "	transaction.index_no,\n"
     + "	transaction.transaction_date,\n"
     + "	loan.installment_amount,\n"
     + "	SUM(IF(settlement.settlement_type = 'LOAN_CAPITAL', settlement_history.settlement_amount,0.0)) AS loan_capital_paid,\n"
     + "	SUM(IF(settlement.settlement_type = 'LOAN_INTEREST', settlement_history.settlement_amount,0.0)) AS loan_interest_paid,\n"
     + "	SUM(IF(settlement.settlement_type = 'LOAN_PANALTY', settlement_history.settlement_amount,0.0)) AS loan_panalty_paid\n"
     + "FROM\n"
     + "	transaction\n"
     + "	LEFT JOIN loan ON loan.index_no = transaction.loan\n"
     + "	LEFT JOIN settlement_history ON settlement_history.`transaction` = transaction.index_no\n"
     + "	LEFT JOIN settlement ON settlement.index_no = settlement_history.settlement\n"
     + "WHERE\n"
     + "	transaction.transaction_type = 'RECEIPT'\n"
     + "	AND transaction.loan = ?\n"
     + "GROUP BY\n"
     + "	`transaction`.index_no\n"
     + "ORDER BY\n"
     + "	`transaction`.transaction_date";
     List<Settlement> settlements = new ArrayList<>();

     Connection connection = getConnection();
     PreparedStatement preparedStatement = connection.prepareStatement(sql);
     preparedStatement.setInt(1, loan.getIndexNo());

     ResultSet resultSet = preparedStatement.executeQuery();
     while (resultSet.next()) {
     Settlement settlement = new Settlement();
     settlement.setIndexNo(resultSet.getInt("index_no"));
     settlement.setTransactionDate(resultSet.getDate("transaction_date"));
     settlement.setInstallmentAmount(resultSet.getDouble("installment_amount"));
     settlement.setLoanCapitalPaid(resultSet.getDouble("loan_capital_paid"));
     settlement.setLoanInterestPaid(resultSet.getDouble("loan_capital_paid"));
     settlement.setLoanPanaltyPaid(resultSet.getDouble("loan_capital_paid"));

     //            settlement.setLoan(loan);

     settlements.add(settlement);
     }
     closeConnection(connection);

     return settlements;
     }*/
    public JFreeChart createEmptyChart(String title) {
        JFreeChart chart = ChartFactory.createPieChart(title, new DefaultPieDataset(), false, false, false);
        chart.setBackgroundPaint(TRANSAPARENT_COLOR);
        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setSimpleLabels(true);
        plot.setOutlineVisible(false);
        plot.setCircular(true);
        plot.setIgnoreZeroValues(true);
        plot.setBackgroundPaint(TRANSAPARENT_COLOR);

        return chart;
    }

    private Connection getConnection() throws SQLException {
        return CConnectionProvider.getInstance().getConnection();
    }

    private void closeConnection(Connection connection) throws SQLException {
        CConnectionProvider.getInstance().closeConnection(connection);
    }
    private Color TRANSAPARENT_COLOR = new Color(255, 255, 255, 0);
}
