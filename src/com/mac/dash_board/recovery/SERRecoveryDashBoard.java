/*
 *  SERRecoveryDashBoard.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.dash_board.recovery;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.dash_board.recovery.custom_object.LoanBalance;
import com.mac.dash_board.recovery.object.Employee;
import com.mac.dash_board.recovery.object.RemindLetterHistory;
import com.mac.registration.client.object.Route;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class SERRecoveryDashBoard extends AbstractService {

    public SERRecoveryDashBoard(Component component) {
        super(component);
//        com.mac.af.core.database.connection_pool.CConnectionProvider
    }

    public List<Route> getRoutes() {
        try {
            return getDatabaseService().getCollection("FROM com.mac.dash_board.recovery.object.Route");
        } catch (DatabaseException exception) {
            Logger.getLogger(SERRecoveryDashBoard.class.getName()).log(Level.SEVERE, null, exception);
            return Collections.emptyList();
        }
    }

    public List<Employee> getEmployee() {
        try {
            return getDatabaseService().getCollection("FROM com.mac.dash_board.recovery.object.Employee");
        } catch (DatabaseException exception) {
            Logger.getLogger(SERRecoveryDashBoard.class.getName()).log(Level.SEVERE, null, exception);
            return Collections.emptyList();
        }
    }

    public List<RemindLetterHistory> getRemindLetter1() {
        String hql = "FROM com.mac.dash_board.recovery.object.RemindLetterHistory WHERE level BETWEEN 0 AND 9";

        try {
            return getDatabaseService().getCollection(hql);
        } catch (DatabaseException exception) {
            Logger.getLogger(SERRecoveryDashBoard.class.getName()).log(Level.SEVERE, null, exception);
            return Collections.emptyList();
        }
    }

    public List<RemindLetterHistory> getRemindLetter2() {
        String hql = "FROM com.mac.dash_board.recovery.object.RemindLetterHistory WHERE level BETWEEN 10 AND 19";

        try {
            return getDatabaseService().getCollection(hql);
        } catch (DatabaseException exception) {
            Logger.getLogger(SERRecoveryDashBoard.class.getName()).log(Level.SEVERE, null, exception);
            return Collections.emptyList();
        }
    }

    public List<RemindLetterHistory> getRemindLetter3() {
        String hql = "FROM com.mac.dash_board.recovery.object.RemindLetterHistory WHERE level >20";

        try {
            return getDatabaseService().getCollection(hql);
        } catch (DatabaseException exception) {
            Logger.getLogger(SERRecoveryDashBoard.class.getName()).log(Level.SEVERE, null, exception);
            return Collections.emptyList();
        }
    }

    //BALANCES
    public LoanBalance getArrears() {
        String sql = "SELECT\n"
                + "	1 AS row_id,\n"
                + "	SUM(settlement.amount) AS receivable_amount,\n"
                + "	SUM(settlement.balance_amount) AS received_amount,\n"
                + "	SUM(settlement.amount - settlement.balance_amount) AS balance_amount\n"
                + "FROM\n"
                + "	settlement\n"
                + "	LEFT JOIN settlement_type ON settlement.settlement_type = settlement_type.code\n"
                + "WHERE\n"
                + "	settlement.due_date <= '" + getFormattedDate() + "'\n"
                + "	AND settlement.`status` <> 'CANCEL'\n"
                + "	AND settlement_type.credit_or_debit = 'CREDIT'";

        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(sql, LoanBalance.class);
        try {
            List<LoanBalance> balances = getDatabaseService().getCollection(hibernateSQLQuery, null);
            for (LoanBalance loanBalance : balances) {
                System.out.println(loanBalance.getBalanceAmount());
            }
            LoanBalance loanBalance = balances.get(0);

            return loanBalance;
        } catch (DatabaseException ex) {
            Logger.getLogger(SERRecoveryDashBoard.class.getName()).log(Level.SEVERE, null, ex);

            return null;
        }
    }
    //BALANCES

    public LoanBalance getToday() {
        String sql = "SELECT\n"
                + "	2 AS row_id,\n"
                + "	SUM(settlement.amount) AS receivable_amount,\n"
                + "	SUM(settlement.balance_amount) AS received_amount,\n"
                + "	SUM(settlement.amount - settlement.balance_amount) AS balance_amount\n"
                + "FROM\n"
                + "	settlement\n"
                + "	LEFT JOIN settlement_type ON settlement.settlement_type = settlement_type.code\n"
                + "WHERE\n"
                + "	settlement.due_date = '" + getFormattedDate() + "'\n"
                + "	AND settlement.`status` <> 'CANCEL'\n"
                + "	AND settlement_type.credit_or_debit = 'CREDIT'";

        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(sql, LoanBalance.class);
        try {
            List<LoanBalance> balances = getDatabaseService().getCollection(hibernateSQLQuery, null);
            LoanBalance loanBalance = balances.get(0);

            return loanBalance;
        } catch (DatabaseException ex) {
            Logger.getLogger(SERRecoveryDashBoard.class.getName()).log(Level.SEVERE, null, ex);

            return new LoanBalance();
        }
    }

    private String getFormattedDate() {
        return dateFormat.format(CApplication.getSessionVariable(CApplication.WORKING_DATE));
    }
    private SimpleDateFormat dateFormat = new SimpleDateFormat(("yyyy-MM-dd"));
}
