/*
 *  LoanBalance.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 5, 2015, 11:00:32 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.dash_board.recovery.custom_object;

/**
 *
 * @author mohan
 */
public class LoanBalance {

    private int rowId;
    private double receivableAmount;
    private double receivedAmount;
    private double balanceAmount;

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public double getReceivableAmount() {
        return receivableAmount;
    }

    public void setReceivableAmount(double receivableAmount) {
        this.receivableAmount = receivableAmount;
    }

    public double getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(double receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
}
