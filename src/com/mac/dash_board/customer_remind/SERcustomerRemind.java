/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.dash_board.customer_remind;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.dash_board.customer_remind.object.CustomerRemind;
import com.mac.dash_board.customer_remind.object.CustomerRemind2;
import com.mac.dash_board.customer_remind.object.Loan;
import com.mac.loan.LoanStatus;
import java.awt.Component;
import java.awt.HeadlessException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Udayanga
 */
public class SERcustomerRemind extends AbstractService {
private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public SERcustomerRemind(Component component) {
        super(component);
    }

    public void saveCustomerRemind (Collection<CustomerRemind> customerRemind)
    {
        
        
        try {
                  
//                    String call = "CALL z_customerRemind(" + customerRemind.getIndexNo() + ")";
                   
                    for (CustomerRemind customerRemind1 : customerRemind) {
                        
                        
                        
                        savequeue(
                                getDatabaseService(),
                      customerRemind1.getIndexNo(),
                      customerRemind1.getDescription(),
                      customerRemind1.getLoan(),
                      customerRemind1.getMonthArreas(),
                      customerRemind1.getAmountArreas(),
                      customerRemind1.getPanlty(),
                      customerRemind1.getTotalAmount(),
                      
                      customerRemind1.getDateCall(),
                      customerRemind1.getDateRLetter(),
                     customerRemind1.getDateHome(),
                     customerRemind1.getPayment(),
                       customerRemind1.getDateSettlement(),
                       customerRemind1.getDateCase(),
                       customerRemind1.getSeized(),
                       customerRemind1.getStatus(),
                       customerRemind1.getRdate()
                      );
                    }
                    mOptionPane.showMessageDialog(null, "Save Success !"
                                        , "Save", mOptionPane.YES_OPTION);
                    
               
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    
    }
      public boolean saveCustomerResponse (Collection<CustomerRemind2> customerRemind)
    {
        boolean check = false;
        
        try {
                  
//                    String call = "CALL z_customerRemind(" + customerRemind.getIndexNo() + ")";
                   
                    for (CustomerRemind2 customerRemind1 : customerRemind) {
                        
                        
                        if(customerRemind1.isCheck()==true)
                        {
                        check = savequeueResponse(
                                getDatabaseService(),
                      customerRemind1.getIndexNo(),
                      customerRemind1.getLoan(),
                      customerRemind1.getDateCall(),
                      customerRemind1.getFAnswerCall(),
                      customerRemind1.getSAnswerCall(),
                      customerRemind1.getTAnswerCall(),
                      
                      customerRemind1.getDateLetter(),
                      customerRemind1.getFAnswerLetter(),
                     customerRemind1.getSAnswerLetter(),
                     customerRemind1.getTAnswerLetter(),
                       customerRemind1.getDateHome(),
                       customerRemind1.getFAnswerHome(),
                       customerRemind1.getSAnswerHome(),
                       customerRemind1.getSeizedDate(),
                       customerRemind1.getDescription(),
                       customerRemind1.getIssuesettlement(),
                       customerRemind1.getCasenote()
                                
                      );
                    }
                    }
               
        } catch (Exception e) {
            e.printStackTrace();
        }
    return check;
    
    }
       private boolean savequeueResponse(
               HibernateDatabaseService databaseService,
               int index,
               String loan,
               String datecall,
               String fcallanswer,
               String scallanswer,
               String tcallanswer,
               String dateletter,
               String fletter,
               String sletter,
               String tletter,
               String datehome,
               String fhome,
               String shome,
               String dateseized,
               String description,
               String settlementnote,
               String casenote
               
               ) 
    {
        try {
            
        
        CustomerRemind2  CcustomerRemind =new CustomerRemind2();
                    CcustomerRemind.setIndexNo(index);
                    
                    CcustomerRemind.setLoan(loan);
                    CcustomerRemind.setDateCall(datecall);
                    CcustomerRemind.setFAnswerCall(fcallanswer);
                    CcustomerRemind.setSAnswerCall(scallanswer);
                    CcustomerRemind.setTAnswerCall(tcallanswer);
                    
                    CcustomerRemind.setDateLetter(dateletter);
                    CcustomerRemind.setFAnswerLetter(fletter);
                    CcustomerRemind.setSAnswerLetter(sletter);
                    CcustomerRemind.setTAnswerLetter(tletter);
                    
                    CcustomerRemind.setDateHome(datehome);
                    CcustomerRemind.setFAnswerHome(fhome);
                    CcustomerRemind.setSAnswerHome(shome);
                    
                    CcustomerRemind.setSeizedDate(dateseized);
                    CcustomerRemind.setDescription(description);
                    CcustomerRemind.setIssuesettlement(settlementnote);
                    CcustomerRemind.setCasenote(casenote);
                    
                    databaseService.beginLocalTransaction();
                    databaseService.update(CcustomerRemind);
                    databaseService.commitLocalTransaction();
                  return true;
        } catch (Exception e) {
            return false;
            
        }  
    
    }
//     private static Connection getConnection() throws SQLException {
//        Connection connection = CConnectionProvider.getInstance().getConnection();
//        
//        return connection;
//    }
//    public boolean save_sp_note(int indexno,String Note) throws SQLException
//    {
//        try {
////            Loan loan = new Loan();
//            
//           String sql="update loan set sp_note='"+Note+"' where index_no='"+indexno+"'";
////           
//            
//            
//            Statement statement = getConnection().createStatement();
//            statement.executeUpdate(sql);
//            
//            
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
////        finally
////        {
////        getConnection().close();
////        getConnection().commit();
////        }
//    }
//    
    
    
    public void save2 (int index_no,String call,String letter,String home,double payment,String settlement,String dcase,String seized,String Note, String rdate,int loan,String Snote)
    {
        
         
        try {
            String callf = "CALL z_customer_remind_table_update("+index_no+",'"+call+"','"+letter+"','"+home+"',"+payment+",'"+settlement+"','"+dcase+"','"+seized+"','"+rdate+"','"+Note+"','"+loan+"','"+Snote+"')";
        getDatabaseService().callUpdateProcedure(callf);
            
            JOptionPane.showMessageDialog(null, "Save Success !");
        } catch (DatabaseException | HeadlessException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Save Fail !");
        }
    }
    
    private void savequeue(HibernateDatabaseService databaseService,int index,String Description, Loan loan,int MonthArreas,double AmountArreas,double Panlty,double TotalAmount,String datecall,String dateletter,String datehome,double payment,String datesettle,String datecase,String Seized,String Status,String rdate) throws DatabaseException
    {
        CustomerRemind  CcustomerRemind =new CustomerRemind();
                    CcustomerRemind.setIndexNo(index);
                    CcustomerRemind.setDescription(Description);
                    CcustomerRemind.setLoan(loan);
                    CcustomerRemind.setMonthArreas(MonthArreas);
                    CcustomerRemind.setAmountArreas(AmountArreas);
                    CcustomerRemind.setPanlty(Panlty);
                    CcustomerRemind.setTotalAmount(TotalAmount);
                    CcustomerRemind.setDateCall(datecall);
                    CcustomerRemind.setPayment(payment);
                    CcustomerRemind.setDateRLetter(dateletter);
                    CcustomerRemind.setDateHome(datehome);
                    CcustomerRemind.setDateSettlement(datesettle);
                    CcustomerRemind.setDateCase(datecase);
                    CcustomerRemind.setSeized(Seized);
                    CcustomerRemind.setStatus(Status);
                    CcustomerRemind.setRdate(rdate);
                    
                    databaseService.update(CcustomerRemind);
    
    }
    
    

    public List getLoanDetailss() throws DatabaseException {
        List<CustomerRemind> customerRemind = getDatabaseService().getCollection("from com.mac.dash_board.customer_remind.object.CustomerRemind where status<>'REMOVE'");
//        makeModifications(loans, date);
        return customerRemind;
    }
     public List getLoanDetailss2() throws DatabaseException {
        List<CustomerRemind2> customerRemind = getDatabaseService().getCollection("from com.mac.dash_board.customer_remind.object.CustomerRemind2 order by loan");
//        makeModifications(loans, date);
        return customerRemind;
    }
    public List getloan(String loan) throws DatabaseException {
        System.out.println("loan_______"+loan);
        List<Loan> loanindex = getDatabaseService().getCollection("from com.mac.dash_board.customer_remind.object.Loan where agreementNo='"+loan+"'");
//        makeModifications(loans, date);
        return loanindex;
    }
    public List getLoanDetails_by_loan(int loan) throws DatabaseException {
        List<CustomerRemind> customerRemind = getDatabaseService().getCollection("from com.mac.dash_board.customer_remind.object.CustomerRemind where loan='"+loan+"'");
//        makeModifications(loans, date);
        return customerRemind;
    }
      public List getLoans() {
        List<Loan> loans;

        String hql = "FROM com.mac.dash_board.customer_remind.object.Loan where status=:STATUS ";
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("STATUS", LoanStatus.LOAN_START);
//        hashMap.put("APPROVED", true);

        try {
            loans = getDatabaseService().getCollection(hql, hashMap);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERcustomerRemind.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }
    
    public void loadDataAndSaveData() throws DatabaseException
    {
        String call = "CALL z_customerRemind()";
        getDatabaseService().callUpdateProcedure(call);
        
    }
     public void loadDataAndSaveData2() throws DatabaseException
    {
        String call = "CALL z_customer_response()";
        getDatabaseService().callUpdateProcedure(call);
        getDatabaseService().commitLocalTransaction();
    }
   
    
    

//    private void makeModifications(Collection<Loan> loans, Date asAtDate) {
//        for (Loan loan : loans) {//getArrearsDueDate();
//            Date d = loan.getExpectedLoanDate();
//            Date dateStart = asAtDate;
//            Date dateEnd = d;
//            if((dateEnd != null) && (dateStart!=null)){
//            int diff =(int) Math.round((dateEnd.getDay()- dateStart.getDay()));
//            loan.setAge(diff);
//            }
//        }
//        System.out.println("");
//    }
}
