/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.dash_board.customer_remind.object;

import com.mac.af.component.base.table.CTable;

/**
 *
 * @author KASUN
 */
public class remind {
    private String  note;
    private String  Agreement;
    private CTable table;
    private int row;
    private int column;

    
    public remind(String note,String agreement,CTable table,int row,int column)
    {
        this.note = note;
        this.Agreement = agreement;
        this.table = table;
        this.row = row;
        this.column = column;
        
    }
    
    
    
    
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAgreement() {
        return Agreement;
    }

    public void setAgreement(String Agreement) {
        this.Agreement = Agreement;
    }

    public CTable getTable() {
        return table;
    }

    public void setTable(CTable table) {
        this.table = table;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }
    
    
    
    
    
    
}
