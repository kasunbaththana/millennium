/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.dash_board.customer_remind;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.dash_board.customer_remind.object.CustomerRemind;
import com.mac.dash_board.customer_remind.object.CustomerRemind2;
import com.mac.dash_board.customer_remind.object.Loan;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author kasun
 */
public class PCCustomerRemind1 extends CPanel {

    private SERcustomerRemind sERRecoveryVisit;
    private String LoanType;
    private List<CustomerRemind> loansv;
    public int index_no;
    PCCustomerPopUp pCCustomerPopUp;
    public boolean status=true;
     int a=1;
    public String getLoanType() {
        return LoanType;
    }

    public void setLoanType(String LoanType) {
        this.LoanType = LoanType;
    }

    /**
     * Creates new form PCCustomerRemind
     */
    public PCCustomerRemind1() {
        initComponents();
        initOthers();
    }
int  xx=0;
    @SuppressWarnings("unchecked")
    public void initOthers() {
        sERRecoveryVisit = new SERcustomerRemind(this);

        CTableModel cTableModel = new CTableModel(new CTableColumn[]{
            new CTableColumn("Agrement No","loan"),
            new CTableColumn("Client","Loan","client"),
            new CTableColumn("Arrears", "monthArreas"),
            new CTableColumn("Arrears Amount", "amountArreas"),
            new CTableColumn("Panalty", "panlty"),
            new CTableColumn("Total", "totalAmount"),
            new CTableColumn("Call", new String[]{"dateCall"}, true),
            new CTableColumn("Promised Date", new String[]{"rdate"}, true),
            new CTableColumn("Letter", new String[]{"dateRLetter"}, true),
            new CTableColumn("Home", new String[]{"dateHome"}, true),
            new CTableColumn("Payment", new String[]{"payment"}, true),
            new CTableColumn("Settlement boad", new String[]{"dateSettlement"}, true),
            new CTableColumn("Case", new String[]{"dateCase"}, true),
            new CTableColumn("Seized", new String[]{"seized"}, true)
        });
        pnlLoanTable.setCModel(cTableModel);
        
        
        CTableModel cTableModelR = new CTableModel(new CTableColumn[]{
            new CTableColumn("Agreement No","loan"),
            new CTableColumn("Date Call",new String[]{"dateCall"}, true),
            new CTableColumn("1st Time Response", new String[]{"FAnswerCall"}, true),
            new CTableColumn("2nd Time Response", new String[]{"SAnswerCall"}, true),
            new CTableColumn("3rd Time Response", new String[]{"TAnswerCall"}, true),
            new CTableColumn("Date Letter",new String[]{"dateLetter"}, true),
            new CTableColumn("1st Letter Response", new String[]{"FAnswerLetter"}, true),
            new CTableColumn("2nd Letter Response", new String[]{"SAnswerLetter"}, true),
            new CTableColumn("3rd Letter Response", new String[]{"TAnswerLetter"}, true),
            new CTableColumn("Date Home",new String[]{"dateHome"}, true),
            new CTableColumn("1st Home Response", new String[]{"FAnswerHome"}, true),
            new CTableColumn("2nd Home Response", new String[]{"SAnswerHome"}, true),
            new CTableColumn("Date Seized",new String[]{"seizedDate"}, true),
            new CTableColumn("Description",new String[]{"description"}, true),
            new CTableColumn("Check",new String[]{"check"}, true),
            new CTableColumn("Settlement Board",new String[]{"issuesettlement"}, true),
            new CTableColumn("Case",new String[]{"casenote"}, true)
            
//            new CTableColumn("Total", "totalAmount"),
//            new CTableColumn("Call", new String[]{"dateCall"}, true),
//            new CTableColumn("Letter", new String[]{"dateRLetter"}, true),
//            new CTableColumn("Home", new String[]{"dateHome"}, true),
//            new CTableColumn("Payment", new String[]{"payment"}, true),
//            new CTableColumn("Settlement boad", new String[]{"dateSettlement"}, true),
//            new CTableColumn("Case", new String[]{"dateCase"}, true),
//            new CTableColumn("Seized", new String[]{"seized"}, true)
        });
        pnlLoanTableremind.setCModel(cTableModelR);

        
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnSearch, "doSearch");
        actionUtil.setAction(btnRefresh, "doReFresh");
        actionUtil.setAction(btnRefresh1, "doReFresh1");
        actionUtil.setAction(btnFind, "doFind");
        actionUtil.setAction(btnSave, "doSave");
//        actionUtil.setAction(btnsave2, "doSave2");
//        actionUtil.setAction(btnFind1, "doFind1");
        actionUtil.setAction(btnFind2, "doFind2");
        actionUtil.setAction(btnSaveremind, "doSaveremind");

        btnSearch.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_NEW_ICON));
        btnRefresh.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_REFRESH_ICON));
        btnRefresh1.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_REFRESH_ICON));
        btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_SAVE_ICON));
//        btnsave2.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_SAVE_ICON));
        btnSaveremind.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_SAVE_ICON));
        btnFind.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON));
        btnFind2.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON));

        
//     cboAgreementNo.setExpressEditable(true);
 
       
      pnlLoanTableremind.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
       
            @Override
            public void valueChanged(ListSelectionEvent e) {
//           System.out.println("xx=="+xx);
               
                  
                if (!pnlLoanTableremind.getSelectionModel().isSelectionEmpty()) {
                    CustomerRemind2 customerRemind2 = (CustomerRemind2) pnlLoanTableremind.getValueAt(pnlLoanTableremind.getSelectedRow());
//               mOptionPane.showMessageDialog(null, "Message  "+customerRemind2.getLoan(), SERLoan.TITLE, mOptionPane.INFORMATION_MESSAGE);
               
                int row = pnlLoanTableremind.getSelectedRow();
                int column = pnlLoanTableremind.getSelectedColumn();
//                String o = (String)pnlLoanTableremind.getValueAt(row, column);
                if(!pnlLoanTableremind.getColumnName(column).equals("Check")){
//                mOptionPane.showMessageDialog(null, "Message  "+o, SERLoan.TITLE, mOptionPane.INFORMATION_MESSAGE);
          String o = (String)pnlLoanTableremind.getValueAt(row, column);
                    xx +=1;
                if(0 != (xx%2))
                {
                pCCustomerPopUp = new PCCustomerPopUp(o,customerRemind2.getLoan(),pnlLoanTableremind,row, column);
               
//                if(pCCustomerPopUp.isVisible()==false){
//                   System.out.println("pCCustomerPopUp.isVisible()______"+pCCustomerPopUp.isVisible());
                pCCustomerPopUp.setVisible(true);
                }
                }
//               }
//             else
//               {
//                   System.out.println("______one____sss");
//                   pCCustomerPopUp.setVisible(false);
//               
//               }
                } else {
                    
                }
            
               
            }
        });
      
//      cboAgreementNo.addItemListener(new ItemListener() {
//
//            @Override
//            public void itemStateChanged(ItemEvent e) {
//                try {
//                    Find1();
//                } catch (DatabaseException ex) {
//                    Logger.getLogger(PCCustomerRemind1.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        });
      
    
    }
    
//    privatce int progrSize = 0;
   @Action(asynchronous = true)
   public void doSaveremind ()
   {
       
     boolean message =  sERRecoveryVisit.saveCustomerResponse(pnlLoanTableremind.getCValue());
       
     if(message == true)
     {
          mOptionPane.showMessageDialog(this, "Save Success  !", "Save !", mOptionPane.INFORMATION_MESSAGE);
         
     }
     else
     {
        mOptionPane.showMessageDialog(this, "Save Faild  !", "Error !", mOptionPane.WARNING_MESSAGE); 
     }
       
   }
    private void loadCuatomerResponseTable() 
    {
        try {
            sERRecoveryVisit.loadDataAndSaveData2();
          pnlLoanTableremind.setCValue(sERRecoveryVisit.getLoanDetailss2());
        } catch (DatabaseException ex) {
            Logger.getLogger(PCCustomerRemind1.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    
    }
    
    
    @Action(asynchronous = true, text = "Process", keyBinding = "F5")
    public void doSearch() throws DatabaseException {

        double amount = 0.0;
        double TotalAmount = 0.0;
        double panaltyAmount = 0.0;

        sERRecoveryVisit.loadDataAndSaveData();
        pnlLoanTable.setCValue(sERRecoveryVisit.getLoanDetailss());

        loansv = (List<CustomerRemind>) pnlLoanTable.getCValue();
        
        for (CustomerRemind loanv : loansv) {
            amount += loanv.getAmountArreas();
            TotalAmount += loanv.getTotalAmount();
            panaltyAmount += loanv.getPanlty();
        }
        txtLoanAmount.setCValue(amount);
        txtTotalInstallmentAmount.setCValue(TotalAmount);
        txtPanaltyAmount.setCValue(panaltyAmount);



    }
   int indexno=0;
   //  @Action(asynchronous = true, text = "Search")
   
    @Action
    public void doRefresh() throws DatabaseException {
        pnlLoanTable.setCValue(sERRecoveryVisit.getLoanDetailss());
    }
    @Action
    public void doReFresh1() throws DatabaseException {
         loadCuatomerResponseTable();
//        pnlLoanTableremind.setCValue(sERRecoveryVisit.getLoanDetailss2());
        
        
    }

    @Action(asynchronous = true)
    public void doSave() throws DatabaseException {
        sERRecoveryVisit.saveCustomerRemind(pnlLoanTable.getCValue());
    }
    
    @Action
    public void doFind() throws DatabaseException {
        pnlLoanTable.find(txtTableFind.getCValue());
        
    }
    @Action
    public void doFind2() throws DatabaseException {
        pnlLoanTableremind.find(txtTableFind2.getCValue());
        
    }
    
            
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        btnSearch = new com.mac.af.component.derived.command.button.CCButton();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        pnlLoanTable = new com.mac.af.component.base.table.CTable();
        btnRefresh = new com.mac.af.component.derived.command.button.CCButton();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanAmount = new com.mac.af.component.derived.display.textfield.CDDoubleField();
        txtTotalInstallmentAmount = new com.mac.af.component.derived.display.textfield.CDDoubleField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPanaltyAmount = new com.mac.af.component.derived.display.textfield.CDDoubleField();
        txtTableFind = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnFind = new com.mac.af.component.derived.command.button.CCButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        pnlLoanTableremind = new com.mac.af.component.base.table.CTable();
        btnRefresh1 = new com.mac.af.component.derived.command.button.CCButton();
        btnSaveremind = new com.mac.af.component.derived.command.button.CCButton();
        txtTableFind2 = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnFind2 = new com.mac.af.component.derived.command.button.CCButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jSplitPane1.setDividerLocation(100);

        btnSearch.setText("Process");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 445, Short.MAX_VALUE)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(jPanel1);

        pnlLoanTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(pnlLoanTable);

        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        cDLabel5.setText("Total Arreas Amount:");

        cDLabel6.setText("Total Amount:");

        cDLabel7.setText("Panalty Amount:");

        btnFind.setText("Find");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1053, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtLoanAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalInstallmentAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtPanaltyAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtTableFind, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 596, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 447, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalInstallmentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPanaltyAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTableFind, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jSplitPane1.setRightComponent(jPanel2);

        jTabbedPane1.addTab("Arreas", jSplitPane1);

        pnlLoanTableremind.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(pnlLoanTableremind);

        btnRefresh1.setText("Process");
        btnRefresh1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefresh1ActionPerformed(evt);
            }
        });

        btnSaveremind.setText("Save");
        btnSaveremind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveremindActionPerformed(evt);
            }
        });

        btnFind2.setText("Find");

        jLabel1.setBackground(new java.awt.Color(204, 204, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Telephone Convetation");
        jLabel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel1.setOpaque(true);

        jLabel2.setBackground(new java.awt.Color(204, 204, 255));
        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Letter Post");
        jLabel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel2.setOpaque(true);

        jLabel3.setBackground(new java.awt.Color(204, 204, 255));
        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Home Visit");
        jLabel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel3.setOpaque(true);

        jLabel4.setBackground(new java.awt.Color(204, 204, 255));
        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Seized");
        jLabel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel4.setOpaque(true);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnRefresh1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSaveremind, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtTableFind2, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnFind2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefresh1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSaveremind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTableFind2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFind2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Customer Response", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnRefresh1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefresh1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRefresh1ActionPerformed

    private void btnSaveremindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveremindActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSaveremindActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnFind;
    private com.mac.af.component.derived.command.button.CCButton btnFind2;
    private com.mac.af.component.derived.command.button.CCButton btnRefresh;
    private com.mac.af.component.derived.command.button.CCButton btnRefresh1;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.command.button.CCButton btnSaveremind;
    private com.mac.af.component.derived.command.button.CCButton btnSearch;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.mac.af.component.base.table.CTable pnlLoanTable;
    private com.mac.af.component.base.table.CTable pnlLoanTableremind;
    private com.mac.af.component.derived.display.textfield.CDDoubleField txtLoanAmount;
    private com.mac.af.component.derived.display.textfield.CDDoubleField txtPanaltyAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTableFind;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTableFind2;
    private com.mac.af.component.derived.display.textfield.CDDoubleField txtTotalInstallmentAmount;
    // End of variables declaration//GEN-END:variables
}
