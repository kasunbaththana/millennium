


/*
 *  SERLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.loan_application_ql;

import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.concurrent.CExecutable;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.LoanProfile;
import com.mac.loan.LoanSituation;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zutil.default_charges.LoanDefaultChargesInformation;
import com.mac.registration.branch.object.Branch;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanApplicationAccountInterface;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import java.util.Map;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author user
 */
public class SERLoanApplication extends AbstractService {

    public SERLoanApplication(Component component) {
        super(component);
    }

    public List getRegetRecentTransactions() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status1", LoanStatus.APPLICATION_PENDING);
            params.put("status2", LoanStatus.APPLICATION_SUSPEND);
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Loan where status=:status1 or status=:status2", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(Loan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getClients() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Client where active=true AND client =true ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public List getSupplier() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Client where active=true  AND supplier=true");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getLoanGroups() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanGroup");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getLoanTypes() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanType where is_quick=true ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
  

    public void executeSave(final Object o) throws Exception {
        CExecutable<Void> executable = new CExecutable<Void>("Save Loan Application - " + ((Loan) o).getApplicationReferenceNo(), getCPanel()) {
            @Override
            public Void execute() throws Exception {
                Loan loanOld = (Loan) o;
                Loan loan = (Loan) o;


                List<LoanDefaultChargesInformation> defaultChargesInformations = loan.getDefaultChargesInformations();

                loan.setProfile(LoanProfile.LOAN);
                loan.setSituation(LoanSituation.LOAN_APPLICATION);

                loan.setStatus(LoanStatus.APPLICATION_PENDING_QL);
               
                loan.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));

                loan.setPurpose("");
                
                loanApplicationDocumentNo(loan);
                
                loan = (Loan) getDatabaseService().save(loan);
              
              
                //save transaction info
                int transaction = SystemTransactions.insertTransaction(
                        getDatabaseService(),
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        loan.getApplicationReferenceNo(),
                        loan.getApplicationDocumentNo(),
                        loan.getIndexNo(),
                        null,
                        loan.getClient().getCode(),
                        null);

                //SETTLEMENT
                SystemSettlement systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();
                for (LoanDefaultChargesInformation loanDefaultChargesInformation : defaultChargesInformations) {
                    systemSettlement.addSettlementQueue(
                            loan.getApplicationTransactionDate(),
                            loan.getClient().getCode(),
                            loan.getIndexNo(),
                            null,
                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                            transaction,
                            loanDefaultChargesInformation.getChargeScheme().getName(),
                            loanDefaultChargesInformation.getChargeAmount(),
                            SystemSettlement.APPLICATION_CHARGE);
                }
                
                systemSettlement.flushSettlementQueue(getDatabaseService());

                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_CREDIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_DEBIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);


                //LOAN DEFAULT CHARG CREDIT
                List<LoanDefaultChargesInformation> loanDefaultChargesInformations = loanOld.getDefaultChargesInformations();

                double chargAmount = 0;
                System.out.println("cccccccccccccc_" + loanDefaultChargesInformations);
                for (LoanDefaultChargesInformation tblchargeScheme : loanDefaultChargesInformations) {
                    accountTransaction.saveAccountTransaction(
                            getDatabaseService(),
                            transaction,
                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                            AccountTransactionType.AUTO,
                            AccountSettingCreditOrDebit.CREDIT,
                            tblchargeScheme.getChargeAmount(),
                            tblchargeScheme.getChargeScheme().getAccount(),
                            tblchargeScheme.getChargeScheme().getName(),0 );
                    chargAmount += tblchargeScheme.getChargeAmount();
                }
                //LOAN DEFAULT CHARG DEBIT
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.DEFAULT_CHARG_DEBIT,
                        "Default Charg Amount", chargAmount, AccountTransactionType.AUTO);

//                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.DEFAULT_CHARG_DEBIT, "Default Charg Amount", chargAmount, AccountTransactionType.AUTO);
                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction,
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE);

               
                //REPORT
                Map<String, Object> params = new HashMap<>();
                params.put("TRANSACTION_NO", transaction);
                TransactionUtil.PrintReport(getDatabaseService(), SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE, params);

                return null;
            }
        };
        CApplication.getExecutionManager().execute(executable);
    }
    
      public void loanApplicationDocumentNo(Loan object) throws DatabaseException{
         Loan loan = (Loan) object;
        
        String branch_code = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
        
         Branch b = (Branch) getDatabaseService().initCriteria(Branch.class)
                        .add(Restrictions.eq("code", branch_code))
                        .uniqueResult();
         
        Integer l = ((Integer) getDatabaseService().initCriteria(Loan.class)
                            .setProjection(Projections.count("indexNo"))
                        .add(Restrictions.eq("branch", branch_code))
                        .add(Restrictions.ne("status", "CANCEL"))
                        .uniqueResult()).intValue()+1;
         
        String val = String.format("%03d", l);    
        String request_no = b.getPrefix()+"/"+val;
        
       loan.setApplicationDocumentNo(request_no);
        
    }

    public void executeUpdate(final Object o) throws DatabaseException {
        CExecutable<Void> executable = new CExecutable<Void>("Save Loan Application - " + ((Loan) o).getApplicationReferenceNo(), getCPanel()) {
            @Override
            public Void execute() throws Exception {
                Loan loan = (Loan) o;
                
                loan.setNote(null);
                loan.setSituation(LoanSituation.LOAN_APPLICATION);
                loan.setStatus(LoanStatus.APPLICATION_PENDING);
                loan.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));

                loan = (Loan) getDatabaseService().save(loan);
                getDatabaseService().save(loan.getDocuments());
                getDatabaseService().save(loan.getClients());
                //  getDatabaseService().save(null);

                //save transaction history
                Integer transaction = SystemTransactions.getTransactionIndexNo(
                        getDatabaseService(),
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        loan.getApplicationReferenceNo());
                SystemTransactions.insertTransactionHistory(
                        getDatabaseService(),
                        transaction,
                        SystemTransactions.ACTION_EDIT,
                        null);

                //SETTLEMENT
                SystemSettlement systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();
                for (LoanDefaultChargesInformation loanDefaultChargesInformation : loan.getDefaultChargesInformations()) {
                    systemSettlement.addSettlementQueue(
                            loan.getApplicationTransactionDate(),
                            loan.getClient().getCode(),
                            loan.getIndexNo(),
                            null,
                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                            transaction,
                            loanDefaultChargesInformation.getChargeScheme().getName(),
                            loanDefaultChargesInformation.getChargeAmount(),
                            SystemSettlement.APPLICATION_CHARGE);
                }
                
                
                
                
                systemSettlement.flushSettlementQueue(getDatabaseService());

                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_CREDIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_DEBIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE);

                return null;
            }
        };
        CApplication.getExecutionManager().execute(executable);
    }

   
    public void executeDelete(Object o) throws DatabaseException {
        throw new UnsupportedOperationException("Delete not supported");
    }
}
