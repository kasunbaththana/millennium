/*
 *  SERLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.loan_application_ql.loan_start;


import com.mac.af.core.ApplicationException;
import com.mac.loan.loan_application_ql.loan_start.header.LoanHeaderData;
import com.mac.loan.zutil.calculation.LoanCalculationException;
import com.mac.loan.zutil.calculation.LoanCalculationUtil;
import com.mac.loan.zutil.calculation.LoanPaymentInformation;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.loan.LoanProfile;
import com.mac.loan.LoanSituation;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanType;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.AdvanceLoanAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanAccountInterface;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zutil.system_settings.SystemSettingsStatus;
import com.mac.zutil.system_settings.SystemStatusCheck;
import com.mac.zutil.system_settings.object.Settings;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author user
 */
public class SERLoan extends AbstractService {

    public SERLoan(Component component) {
        super(component);
    }

    public boolean startLoan(Loan application, LoanHeaderData headerData) {
        try {
            Loan loan = getLoan(application, headerData);

            //CHECK AGREEMENT NO DUPLICATES
            String hql = "FROM com.mac.loan.zobject.Loan WHERE agreementNo=:AGREEMENT_NO";
            HashMap<String, Object> existLoanParams = new HashMap<>();
            existLoanParams.put("AGREEMENT_NO", loan.getAgreementNo());
            List<Loan> existLoans = getDatabaseService().getCollection(hql, existLoanParams);
            if (!existLoans.isEmpty()) {
                throw new ApplicationException("The agreement number '" + loan.getAgreementNo() + "' is already exists."
                        + "\nPlease enter a different agreement number.");
            }

            //SAVE LOAN
            getDatabaseService().beginLocalTransaction();
            loan = (Loan) getDatabaseService().save(loan);
            //update transaction info
            Integer transaction = SystemTransactions.insertTransaction(
                    getDatabaseService(),
                    SystemTransactions.ADVANCE_LOAN_TRANSACTION_CODE,
                    loan.getLoanReferenceNo(),
                    loan.getLoanDocumentNo(),
                    loan.getIndexNo(),
                    null,
                    loan.getClient().getCode(),
                    headerData.getTransactionNote());

            getDatabaseService().commitLocalTransaction();

            //UPDATE SETTLEMENT
            List<LoanPaymentInformation> informations = getLoanPayments(application);
            SystemSettlement systemSettlement = SystemSettlement.getInstance();
            systemSettlement.beginSettlementQueue();
            int x = 1;
            double total_interest = 0.0;
            for (LoanPaymentInformation loanPaymentInformation : informations) {

//                x++;
                //LOAN CAPITAL
                systemSettlement.addSettlementQueue(
                        loanPaymentInformation.getPaymentDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        (loanPaymentInformation.getIndex() + 1),
                        SystemTransactions.LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Capital",
                        loanPaymentInformation.getCapitalAmount(),
                        SystemSettlement.LOAN_CAPITAL);

                //LOAN INTEREST
                systemSettlement.addSettlementQueue(
                        loanPaymentInformation.getPaymentDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        (loanPaymentInformation.getIndex() + 1),
                        SystemTransactions.LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Interest",
                        loanPaymentInformation.getInterestAmount(),
                        SystemSettlement.LOAN_INTEREST);
                total_interest += loanPaymentInformation.getInterestAmount();
            }
            systemSettlement.flushSettlementQueue(getDatabaseService());

            //VOUCHER AMOUNT SETTLEMENT
            if (loan.getProfile().equals(LoanProfile.LOAN)) {
                systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();
                systemSettlement.addSettlementQueue(
                        loan.getLoanTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Amount",
                        loan.getLoanAmount(),
                        SystemSettlement.LOAN_AMOUNT);
                systemSettlement.flushSettlementQueue(getDatabaseService());
            }
            
            //Down Payment
            if (loan.getProfile().equals(LoanProfile.LOAN)) {
                systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();
                systemSettlement.addSettlementQueue(
                        loan.getLoanTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.LOAN_TRANSACTION_CODE,
                        transaction,
                        "Down Payment",
                        getDownPayment(loan.getIndexNo()),
                        SystemSettlement.LOAN_AMOUNT);
                systemSettlement.flushSettlementQueue(getDatabaseService());
            }
            //ACCOUNT TRANSACTION

             double total_debit_amount = loan.getLoanAmount()+total_interest;
                //is normal loan
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();
                
                accountTransaction.addAccountTransactionQueue(AdvanceLoanAccountInterface.LOAN_AMOUNT_CREDIT_CODE, "Loan Amount - " + loan.getAgreementNo(), loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(AdvanceLoanAccountInterface.INTEREST_AMOUNT_CREDIT_CODE, "Loan Interest - " + loan.getAgreementNo(), total_interest, AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(AdvanceLoanAccountInterface.LOAN_AMOUNT_DEBIT_CODE, "Loan Amount - " + loan.getAgreementNo(), total_debit_amount, AccountTransactionType.AUTO);
                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.ADVANCE_LOAN_TRANSACTION_CODE);
            
            
            
                Map<String, Object> params = new HashMap<>();
                params.put("TRANSACTION_NO", transaction);
                TransactionUtil.PrintReport(getDatabaseService(), SystemTransactions.LOAN_TRANSACTION_CODE, params);
            

            if (new SystemStatusCheck().isEnableThisSettings(
                    getCPanel().getDatabaseService() //database service
                    , SystemSettingsStatus.AG_NO_FOR_LOANTYPE) //ag no genarate code
                    ) {
                LoanType xloanType = (LoanType) CPanel.GLOBAL.getDatabaseService().initCriteria(LoanType.class)
                        .add(Restrictions.eq("code", loan.getLoanType().getCode())).uniqueResult();

                int count = (int) CPanel.GLOBAL.getDatabaseService()
                        .getUniqueResultHQL("SELECT lastAgrementNo FROM com.mac.loan.zobject.LoanType WHERE code='" + loan.getLoanType().getCode() + "'", null);

                xloanType.setLastAgrementNo((count + 1));
                getCPanel().getDatabaseService().beginLocalTransaction();
                getCPanel().getDatabaseService().update(xloanType);
                getCPanel().getDatabaseService().commitLocalTransaction();
            }

            if (new SystemStatusCheck().isEnableThisSettings(
                    getCPanel().getDatabaseService() //database service
                    , SystemSettingsStatus.AG_NO_FOR_INT) //ag no genarate code
                    ) {
                Settings setting = (Settings) CPanel.GLOBAL.getDatabaseService().initCriteria(Settings.class)
                        .add(Restrictions.eq("code", SystemSettingsStatus.AG_NO_FOR_INT)).uniqueResult();

                int count = (int) CPanel.GLOBAL.getDatabaseService()
                        .getUniqueResultHQL("SELECT value FROM com.mac.zutil.system_settings.object.Settings WHERE code='" + SystemSettingsStatus.AG_NO_FOR_INT + "'", null);
                setting.setValue((count + 1));
                getCPanel().getDatabaseService().beginLocalTransaction();
                getCPanel().getDatabaseService().update(setting);
                getCPanel().getDatabaseService().commitLocalTransaction();

            }
            mOptionPane.showMessageDialog(null, "Loan started successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (DatabaseException ex) {
            Logger.getLogger(SERLoan.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to start the loan. \nERROR: " + ex.getMessage(), TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        } catch (LoanCalculationException ex) {
            Logger.getLogger(SERLoan.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, ex.getMessage(), SERLoan.TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        } catch (ApplicationException ex) {
            mOptionPane.showMessageDialog(null, ex.getMessage(), SERLoan.TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    public boolean rollbackLoan(Loan loan) {
        try {
            loan.setStatus(LoanStatus.APPLICATION_PENDING);

            Integer transaction = SystemTransactions.getTransactionIndexNo(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                    loan.getApplicationReferenceNo());
            SystemTransactions.insertTransactionHistory(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.ACTION_ROLLBACK,
                    null);
            getDatabaseService().save(loan);
            return true;
        } catch (DatabaseException | ApplicationException e) {
            Logger.getLogger(SERLoan.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }
 
    private Loan getLoan(Loan application, LoanHeaderData headerData) throws LoanCalculationException, DatabaseException {
        //HEADER DATA
        application.setLoanReferenceNo(headerData.getReferenceNo());
        application.setLoanDocumentNo(headerData.getDocumentNo());
        application.setLoanTransactionDate(headerData.getTransactionDate());
        application.setAgreementNo(headerData.getAgreementNo());
        application.setLoanDate(headerData.getLoanDate());
        application.setEmployeeByLoanOfficer(headerData.getEmployeeByLoanOfficer());
        application.setEmployeeByRecoveryOfficer(headerData.getEmployeeByRecoveryOfficer());
        //STATUS
        application.setAvailableVoucher(false);
        application.setAvailableReceipt(false);
        if (TransactionUtil.isSendToApprove(getDatabaseService(), SystemTransactions.LOAN_TRANSACTION_CODE)) {
            application.setStatus(LoanStatus.LOAN_PENDING);
        } else {
            application.setStatus(LoanStatus.LOAN_START);
            application.setAvailableReceipt(true);
            application.setAvailableVoucher(false);
            application.setAvailableDisbursement(false);
        }
        application.setSituation(LoanSituation.LOAN);
        return application;
    }

    private List<LoanPaymentInformation> getLoanPayments(Loan application) throws LoanCalculationException, DatabaseException {
        List<LoanPaymentInformation> paymentInformations = LoanCalculationUtil.getPaymentSchedule(
                getDatabaseService(),
                application.getLoanAmount(),
                application.getInterestRate(),
                application.getInstallmentCount(),
                application.getPaymentTerm(),
                application.getLoanType().getInterestMethod(),
                application.getLoanDate());
        return paymentInformations;
    }
    public static final String TITLE = "Loan";
    
    private double getDownPayment(int loan)
    {
        double DownPayment=0.0;
        List<Settlement> list;
        try {
            list = getDatabaseService().getCollection("from com.mac.zsystem.transaction.settlement.object.Settlement where status<>'CANCEL' AND description='DOWN PAYMENT' and  loan='"+loan+"' ");
        for(Settlement getlist:list)
        {
           DownPayment= getlist.getAmount();
        }
        } catch (DatabaseException ex) {
            Logger.getLogger(SERLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return DownPayment;
    }
}
