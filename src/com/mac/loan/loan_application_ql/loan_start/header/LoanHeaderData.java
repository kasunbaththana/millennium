/*
 *  LoanHeaderData.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jun 26, 2014, 1:01:59 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.loan_application_ql.loan_start.header;

import com.mac.loan.zobject.Employee;
import java.util.Date;

/**
 *
 * @author mohan
 */
public class LoanHeaderData {

    private String agreementNo;
    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private Date loanDate;
    private Employee employeeByRecoveryOfficer;
    private Employee employeeByLoanOfficer;
    private String TransactionNote;
    

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }

    public Employee getEmployeeByRecoveryOfficer() {
        return employeeByRecoveryOfficer;
    }

    public void setEmployeeByRecoveryOfficer(Employee employeeByRecoveryOfficer) {
        this.employeeByRecoveryOfficer = employeeByRecoveryOfficer;
    }

    public Employee getEmployeeByLoanOfficer() {
        return employeeByLoanOfficer;
    }

    public void setEmployeeByLoanOfficer(Employee employeeByLoanOfficer) {
        this.employeeByLoanOfficer = employeeByLoanOfficer;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public String getTransactionNote() {
        return TransactionNote;
    }

    public void setTransactionNote(String TransactionNote) {
        this.TransactionNote = TransactionNote;
    }
    
    
}
