/*
 *  StartedLoanApproval.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 3, 2014, 8:42:37 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.loan_approval;

/**
 *
 * @author mohan
 */
public class StartedLoanApproval extends LoanApplicationApproval {

    @Override
    public LoanApprovalServiceInterface initService() {
        return new SERStartedLoanApproval(this);
    }
}
