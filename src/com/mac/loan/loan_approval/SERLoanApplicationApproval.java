/*
 *  SERLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.loan_approval;

import com.mac.af.core.ApplicationException;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanAdvanceTrans;
import com.mac.loan.zobject.LoanType;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanApprovalAccountInterface;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author user
 */
public class SERLoanApplicationApproval extends AbstractService implements LoanApprovalServiceInterface {

    public SERLoanApplicationApproval(Component component) {
        super(component);
        
    }

    @Override
    public boolean acceptApplication(Loan loan) {
        loan.setStatus(LoanStatus.APPLICATION_ACCEPT);

        try {
            getDatabaseService().save(loan);
//            save(loan);

            //save transaction history
            Integer transaction = SystemTransactions.getTransactionIndexNo(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                    loan.getApplicationReferenceNo());
            SystemTransactions.insertTransactionHistory(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.ACTION_ACCEPT,
                    null);

            //ACCOUNT TRANSACTION
            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
            accountTransaction.beginAccountTransactionQueue();
            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.APPROVAL_AMOUNT_CREDIT_CODE, "Loan Approval Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.APPROVAL_AMOUNT_DEBIT_CODE, "Loan Approval Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE);

            mOptionPane.showMessageDialog(null, "Loan application accepted successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(SERLoanApplicationApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to accept loan application.", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    @Override
    public boolean suspandApplication(Loan loan) {
        String note = mOptionPane.showInputDialog(null, "Please enter a note :", TITLE, mOptionPane.QUESTION_MESSAGE);
        loan.setNote(note);
        loan.setStatus(LoanStatus.APPLICATION_SUSPEND);

        try {
            getDatabaseService().save(loan);

            //save transaction history
            Integer transaction = SystemTransactions.getTransactionIndexNo(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                    loan.getApplicationReferenceNo());
            SystemTransactions.insertTransactionHistory(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.ACTION_SUSPEND,
                    note);

            //ACCOUNT TRANSACTION
            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
            accountTransaction.beginAccountTransactionQueue();
            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.SUSPEND_AMOUNT_CREDIT_CODE, "Loan Suspend Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.SUSPEND_AMOUNT_DEBIT_CODE, "Loan Suspend Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE);

            mOptionPane.showMessageDialog(null, "Loan application suspended successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (DatabaseException | ApplicationException ex) {
            Logger.getLogger(SERLoanApplicationApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to suspand loan application.", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    @Override
    public boolean rejectApplication(Loan loan) {
        String note = mOptionPane.showInputDialog(null, "Please enter a note :", TITLE, mOptionPane.QUESTION_MESSAGE);
        loan.setNote(note);
        loan.setStatus(LoanStatus.APPLICATION_REJECT);

        try {
            getDatabaseService().save(loan);
            //save transaction history
            Integer transaction = SystemTransactions.getTransactionIndexNo(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                    loan.getApplicationReferenceNo());
            SystemTransactions.insertTransactionHistory(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.ACTION_REJECT,
                    note);
            
            LoanAdvanceTrans advanceTrans = (LoanAdvanceTrans) CPanel.GLOBAL.getDatabaseService().initCriteria(LoanAdvanceTrans.class)
                        .add(Restrictions.eq("loan", loan.getIndexNo())).uniqueResult();
            
            getDatabaseService().delete(advanceTrans);
           

            //ACCOUNT TRANSACTION
            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
            accountTransaction.beginAccountTransactionQueue();
            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.REJECT_AMOUNT_CREDIT_CODE, "Loan Reject Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.REJECT_AMOUNT_CREDIT_CODE, "Loan Reject Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE);

            mOptionPane.showMessageDialog(null, "Loan application rejected successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (DatabaseException | ApplicationException ex) {
            Logger.getLogger(SERLoanApplicationApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to reject loan application.", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    @Override
    public List<Loan> getLoans() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status", LoanStatus.APPLICATION_PENDING);
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Loan where status=:status", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(LoanApplicationApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    private void save(Object object) throws Exception {
        Thread.sleep(1000000);
        getDatabaseService().save(object);
    }
    public static final String TITLE = "Loan Approval";
    
      public List<Settlement> getChearges(String Loan) {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status", SettlementStatus.SETTLEMENT_CANCEL);
            params.put("loan", 608);
            list = getDatabaseService().getCollection("from com.mac.zsystem.transaction.settlement.object.Settlement where status<>:status and loan=:loan", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(LoanApplicationApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
