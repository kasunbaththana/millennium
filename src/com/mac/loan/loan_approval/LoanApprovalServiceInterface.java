/*
 *  LoanApproval.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 3, 2014, 8:43:23 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.loan_approval;

import com.mac.loan.zobject.Loan;
import java.util.List;

/**
 *
 * @author mohan
 */
public interface LoanApprovalServiceInterface {

    public boolean acceptApplication(Loan loan);

    public boolean suspandApplication(Loan loan);

    public boolean rejectApplication(Loan loan);

    public List<Loan> getLoans();
}
