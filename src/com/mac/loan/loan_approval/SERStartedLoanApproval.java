/*
 *  SERLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.loan_approval;

import com.mac.af.core.ApplicationException;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.loan.LoanProfile;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author user
 */
public class SERStartedLoanApproval extends AbstractService implements LoanApprovalServiceInterface {

    public SERStartedLoanApproval(Component component) {
        super(component);
    }

    @Override
    public boolean acceptApplication(Loan loan) {
        loan.setStatus(LoanStatus.LOAN_START);
        loan.setAvailableReceipt(true);
        loan.setAvailableVoucher(loan.getProfile().equals(LoanProfile.LOAN));
        
        if(loan.getLoanType().getIshp())
        {
           loan.setAvailableVoucher(false);
        }
        else
        {
            loan.setAvailableVoucher(loan.getProfile().equals(LoanProfile.LOAN));
        }
        
        
        try {
            getDatabaseService().save(loan);
            
            
            //save transaction history
            
            getDatabaseService().beginLocalTransaction();
            
            
            //update transaction info
             SystemTransactions.insertTransaction(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE,
                    loan.getLoanReferenceNo(),
                    loan.getLoanDocumentNo(),
                    loan.getIndexNo(),
                    null,
                    loan.getClient().getCode(),
                    loan.getNote());

            getDatabaseService().commitLocalTransaction();
            
            Integer transaction = SystemTransactions.getTransactionIndexNo(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                    loan.getApplicationReferenceNo());
            SystemTransactions.insertTransactionHistory(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.ACTION_ACCEPT,
                    null);
            
            ///  
            

            //kasun
            getDatabaseService().beginLocalTransaction();
            
            //update transaction info loan approve
             SystemTransactions.insertTransaction(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE,
                    loan.getLoanReferenceNo(),
                    loan.getLoanDocumentNo(),
                    loan.getIndexNo(),
                    null,
                    loan.getClient().getCode(),
                    loan.getNote());

            getDatabaseService().commitLocalTransaction();
            
            
            
            
            
            

            //ACCOUNT TRANSACTION
//            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
//            accountTransaction.beginAccountTransactionQueue();
//            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.APPROVAL_AMOUNT_CREDIT_CODE, "Loan Approval Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
//            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.APPROVAL_AMOUNT_DEBIT_CODE, "Loan Approval Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
//            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE);

            mOptionPane.showMessageDialog(null, "Loan accepted successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (DatabaseException | ApplicationException ex) {
            Logger.getLogger(SERStartedLoanApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to accept loan application.", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    @Override
    public boolean suspandApplication(Loan loan) {
        String note = mOptionPane.showInputDialog(null, "Please enter a note :", TITLE, mOptionPane.QUESTION_MESSAGE);
        loan.setNote(note);
        loan.setStatus(LoanStatus.LOAN_PENDING);

        try {
            getDatabaseService().save(loan);

            //save transaction history
            Integer transaction = SystemTransactions.getTransactionIndexNo(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                    loan.getApplicationReferenceNo());
            SystemTransactions.insertTransactionHistory(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.ACTION_SUSPEND,
                    note);

            //ACCOUNT TRANSACTION
//            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
//            accountTransaction.beginAccountTransactionQueue();
//            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.SUSPEND_AMOUNT_CREDIT_CODE, "Loan Suspend Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
//            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.SUSPEND_AMOUNT_DEBIT_CODE, "Loan Suspend Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
//            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE);

            mOptionPane.showMessageDialog(null, "Loan application suspended successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (DatabaseException | ApplicationException ex) {
            Logger.getLogger(SERStartedLoanApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to suspand loan application.", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    @Override
    public boolean rejectApplication(Loan loan) {
        String note = mOptionPane.showInputDialog(null, "Please enter a note :", TITLE, mOptionPane.QUESTION_MESSAGE);
        loan.setNote(note);
        loan.setStatus(LoanStatus.LOAN_REJECT);

        try {
            getDatabaseService().save(loan);
            //save transaction history
            Integer transaction = SystemTransactions.getTransactionIndexNo(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                    loan.getApplicationReferenceNo());
            SystemTransactions.insertTransactionHistory(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.ACTION_REJECT,
                    note);

            //ACCOUNT TRANSACTION
//            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
//            accountTransaction.beginAccountTransactionQueue();
//            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.REJECT_AMOUNT_CREDIT_CODE, "Loan Reject Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
//            accountTransaction.addAccountTransactionQueue(LoanApprovalAccountInterface.REJECT_AMOUNT_CREDIT_CODE, "Loan Reject Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
//            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE);

            mOptionPane.showMessageDialog(null, "Loan application rejected successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (DatabaseException | ApplicationException ex) {
            Logger.getLogger(SERStartedLoanApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to reject loan application.", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    @Override
    public List<Loan> getLoans() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status", LoanStatus.LOAN_PENDING);
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Loan where status=:status", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(LoanApplicationApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public static final String TITLE = "Loan Approval";
}
