/*
 *  LoanApproval.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jun 25, 2014, 1:00:55 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.loan_approval;

import com.mac.af.component.base.button.action.Action;
import java.util.Collection;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.transaction.SystemTransactions;

/**
 *
 * @author mohan
 */
public class LoanApplicationApproval extends AbstractGridObject {
 
    public LoanApplicationApproval() {
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE);
        this.recentButton.setDatabseService(getDatabaseService());
        
        
    }

    protected int loanindex;
    
    
    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
        
    }   
    
    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Index No", new String[]{"indexNo"}),
            new CTableColumn("Req No", new String[]{"applicationDocumentNo"}),
            new CTableColumn("Client", new String[]{"client", "name"}),
            new CTableColumn("Group", new String[]{"client", "group_name"}),
            new CTableColumn("Loan Type", new String[]{"loanType", "name"}),
            new CTableColumn("Loan Amount", new String[]{"loanAmount"}),
            new CTableColumn("Installment Amount", new String[]{"installmentAmount"}),
            new CTableColumn("Installment Count", new String[]{"installmentCount"}),
            new CTableColumn("Interest Rate", new String[]{"interestRate"})
        });
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        return new PCLoanApproval(getService());
    }

    @Override
    protected Collection getTableData() {
        return getService().getLoans();
        
    }

    private LoanApprovalServiceInterface getService() {
        if (loanApproval == null) {
            loanApproval = initService();
        }

        return loanApproval;
    }

    public LoanApprovalServiceInterface initService() {
        return new SERLoanApplicationApproval(this);
    }
    
    
    

    private RecentButton recentButton;    
    private LoanApprovalServiceInterface loanApproval;
}
