/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_document_view;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.loan_application.SERLoanApplication;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NIMESH-PC
 */
public class SERDocumentView extends AbstractService{

    public SERDocumentView(Component component) {
        super(component);
    }
    
    
    
    public List getLoans() {
      List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Loan where status in ('LOAN_START','COLLECTED','COLLECTED') ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
    public List getLoansApplication() {
      List list;
        try {
            list = getDatabaseService().getCollection("select applicationReferenceNo from com.mac.loan.zobject.Loan");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    
}
