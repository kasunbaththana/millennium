/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_advance_payment;

import com.mac.af.core.ApplicationException;
import com.mac.cash_collection.template.*;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.loan.loan_advance_payment.object.AdvanceScheme;
import com.mac.loan.loan_advance_payment.object.AdvanceSchemeCustom;
import com.mac.loan.loan_advance_payment.object.Loan;
import com.mac.loan.loan_advance_payment.object.LoanAdvanceSum;
import com.mac.loan.loan_advance_payment.object.LoanAdvanceTrans;
import com.mac.loan.ztemplate.receipt.SERAbstractReceipt;
import com.mac.registration.advance_scheme.AdvanceSchemeType;
import com.mac.registration.charge_scheme.ChargeSchemeType;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.AdvanceAccountInterface;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class SERAdvancePayment extends AbstractService {
private SystemPayment systemPayment;
private Date WORKING_DATE =(Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
private CashierSession cashierSession;
    public SERAdvancePayment(Component component) {
        super(component);
        try {
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException | DatabaseException ex) {
            Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
  private int getCashierSession() throws DatabaseException, ApplicationException{
      
     return SystemCashier.getCurrentCashierSession(getDatabaseService()).getIndexNo();
  }
    
    
    public List getClients(String center,boolean res){
        List list;
        try {
            if(center!=null && res){
              
              list = getDatabaseService().getCollection("from com.mac.loan.loan_advance_payment.object.Client WHERE active='1' and client='1' and center='"+center+"'  ");
            }else{
             
            list = getDatabaseService().getCollection("from com.mac.loan.loan_advance_payment.object.Client WHERE active='1' and client='1'   ");
            }
            } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERAdvancePayment.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
     public List getCenter(){
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.registration.loan_group.object.LoanCenter ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERAdvancePayment.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List getLoan(String client) {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.loan_advance_payment.object.Loan where client='"+client+"' and status='LOAN_START' ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERCashCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public void saveTransaction(String transactionCode,String getReferenceNo, LoanAdvanceSum advanceSum){
      try {
        boolean isPaymentOk = showPaymentDialog(transactionCode, advanceSum,getReferenceNo);
        if (isPaymentOk) {
        
            getDatabaseService().beginLocalTransaction();
            int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                transactionCode,
                getReferenceNo,
                null,
                null,
                getCashierSession(),
                advanceSum.getCustomer().getCode(),
                advanceSum.getMemo());
            LoanAdvanceSum advanceSum2 = advanceSum;
            LoanAdvanceTrans advanceTrans =new LoanAdvanceTrans();
            advanceTrans.setTransactionDate(WORKING_DATE);
            advanceTrans.setTransaction(transactionIndex);
            advanceTrans.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
            advanceTrans.setCrAmount(0.00);
            advanceTrans.setDrAmount(advanceSum.getAmount());
            advanceTrans.setLoan(advanceSum.getLoan());
            advanceTrans.setClient(advanceSum.getCustomer());
            advanceTrans.setStatus("ACTIVE");
            advanceTrans.setIsDownpay(advanceSum.isIsDownpay());
            
            
            advanceSum.setTransaction(transactionIndex);
            advanceSum2 = (LoanAdvanceSum) getDatabaseService().save(advanceSum);
            
            advanceTrans.setTransSum(advanceSum2.getIndexNo());
            getDatabaseService().save(advanceTrans);
            
            savePayment(transactionIndex, advanceSum);
            
            saveAccount(transactionIndex,transactionCode,advanceSum);
            
            
            getDatabaseService().commitLocalTransaction();
            mOptionPane.showMessageDialog(null, "Successfully saved !!!", "Save", mOptionPane.INFORMATION_MESSAGE);
            
            Map<String, Object> params = new HashMap<>();
            params.put("TRANSACTION_NO", transactionIndex);
            TransactionUtil.PrintReport(getDatabaseService(), transactionCode, params);
        }
        } catch (Exception e) {
            e.printStackTrace();
            
            
            
        }
      
        
    }
    
    public void saveAccount(int transactionIndex,String transactionCode,LoanAdvanceSum advanceSum){
        try {
            
            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
             accountTransaction.beginAccountTransactionQueue();
        
             
//                accountTransaction.addAccountTransactionQueue(
//                AdvanceAccountInterface.ADVANCE_AMOUNT_DEBIT_CODE,
//                "Cash Receipt Advanced Amount",
//                0.0,
//                AccountTransactionType.AUTO);
             
                accountTransaction.addAccountTransactionQueue(
                AdvanceAccountInterface.ADVANCE_AMOUNT_CREDIT_CODE,
                "Advance Receipt Amount "+advanceSum.getCustomer(),
                advanceSum.getAmount(),
                AccountTransactionType.AUTO);
            
       accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, transactionCode);     
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
     public boolean showPaymentDialog(String transactionTypeCode, LoanAdvanceSum advanceSum,String getReferenceNo) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                getReferenceNo,
                "",
                WORKING_DATE,
                advanceSum.getAmount(),
                ChequeType.CLIENT);

        return isPaymentOk;
    }
     
     public void savePayment(int transactionIndex, LoanAdvanceSum advanceSum) throws DatabaseException {
        systemPayment.savePayment(
                getDatabaseService(),
                cashierSession,
                transactionIndex,
                advanceSum.getCustomer().getCode());
    }
     
     public static double getDefaultCharges(Collection<AdvanceScheme> defaultCharges, Loan loan ) {

        
        double betweenFrom;
        double betweenTo;

        double chargeAmount = 0.0;



        for (AdvanceScheme chargeScheme : defaultCharges) {
            switch (chargeScheme.getType()) {
                case ChargeSchemeType.VALUE:
                    chargeAmount = chargeScheme.getAmount();
                    break;
                case ChargeSchemeType.PERCENT:
                    chargeAmount = loan.getLoanAmount() * chargeScheme.getAmount() / 100;
                    break;
                case ChargeSchemeType.CUSTOM:

                    
                    for (AdvanceSchemeCustom chargeSchemeCustom : chargeScheme.getAdvanceSchemeCustoms()) {
                        betweenFrom = chargeSchemeCustom.getFromAmount();
                        betweenTo = chargeSchemeCustom.getToAmount();

                        if (betweenFrom <= loan.getLoanAmount()
                                && betweenTo >= loan.getLoanAmount()) {
                            chargeAmount = chargeSchemeCustom.getAmount();
                            
                             switch (chargeSchemeCustom.getType()) {
                                        case AdvanceSchemeType.CUSTOM_AMOUNT:
                                            chargeAmount = chargeSchemeCustom.getAmount();
                                            break;
                                        case AdvanceSchemeType.CUSTOM_RATE:
                                            chargeAmount = loan.getLoanAmount() * (chargeSchemeCustom.getAmount() / 100);
                                            break;
                                        case AdvanceSchemeType.CUSTOM_RENT:
                                            chargeAmount = loan.getInstallmentAmount() * chargeSchemeCustom.getAmount();
                                            break;
                                        default:
                                            throw new AssertionError();
                                    }
                             
                        }
                    }


                    break;
            }

         
        }

        return chargeAmount;
    }
    
    
  
}
