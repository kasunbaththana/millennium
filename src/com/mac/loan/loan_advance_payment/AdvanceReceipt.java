/*
 *  Receipt.java
 * 
 *  @author kasun
 *     
 * 
 *  Created on Oct 12, 2014, 4:11:48 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.loan_advance_payment;

import com.mac.af.core.database.DatabaseException;
import com.mac.loan.loan_advance_payment.object.Loan;
import com.mac.loan.loan_advance_payment.receipt.AbstractReceipt;
import com.mac.loan.loan_advance_payment.receipt.SERAbstractAdvanceReceipt;
import com.mac.zsystem.transaction.account.account_setting.system_interface.AdvanceSettleAccountInterface;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kasun
 */
public class AdvanceReceipt extends AbstractReceipt {

    @Override
    public List<Loan> getLoans() {
        String hql = "FROM com.mac.loan.loan_advance_payment.object.LoanAdvanceTrans WHERE  status='ACTIVE' group by loan having sum(drAmount)-sum(crAmount) > 0 ";
        List<Loan> loans;
        try {
            loans = getDatabaseService().getCollection(hql);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAbstractAdvanceReceipt.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }

    
    @Override
    protected String getReferenceGeneratorType() {
        return ReferenceGenerator.ADVANE_SETTLE;
    }

    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.ADVANCE_SETTLEMENT_CODE;
    }

    @Override
    protected String getSettlementCreitOrDebit() {
        return SettlementCreditDebit.CREDIT;
    }

    @Override
    protected String getChequeType() {
        return ChequeType.CLIENT;
    }

    @Override
    protected String getAccountSettingCode() {
        return AdvanceSettleAccountInterface.ADVANCE_SETTLE_AMOUNT_DEBIT_CODE;
    }

    @Override
    protected String getTransactionName() {
        return "Advcance Settle Receipt";
    }

    @Override
    protected String getOverAmountSettlementType() {
        return SystemSettlement.OVER_PAY_RECEIPT;
    }

    @Override
    protected boolean isOverPayAvailable() {
        return false;
    }

    @Override
    protected Loan getModifiedLoan(Loan loan, Double afterBalance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

  
}
