/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_advance_payment.refund;


import com.mac.loan.loan_advance_payment.*;
import com.finac.loan.Finac;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.loan.loan_advance_payment.object.*;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.loan.loan_advance_payment.object.LoanAdvanceSum;
import com.mac.registration.loan_group.object.LoanCenter;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.Collection;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author kasun
 */
public class PCAdvanceRefund extends CPanel {

    private SERAdvanceRefund advancePayment;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyMMddHHmmss");
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    CRadioButtonGroup buttonGroup;
    CRadioButtonGroup buttonGroup1;
    /**
     * Creates new form CashCollection
     */
    public PCAdvanceRefund() {
        initComponents();
        initOthers();
    }

    protected String getTransactionTypeCode() {
        return SystemTransactions.ADVACNE_REF_TRANSACTION_CODE;
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
         btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
         btnLoadDetails.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON, 16, 16));

         cboCenter.setVisible(false);
         cDLabel7.setVisible(false);
         
        txtTransactionDate.setCValue((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        advancePayment = new SERAdvanceRefund(this);
        txtReferanceNo.setCValue(getRefernceenarate());
        txtReferanceNo.setValueEditable(false);
        txtTransactionDate.setValueEditable(false);


        buttonGroup1 = new CRadioButtonGroup();



        buttonGroup1.addRadioButton(rdMicro);
        buttonGroup1.addRadioButton(rdNoneMicro);


        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnLoadDetails, "doLoad");
        actionUtil.setAction(btnSave, "doSave");

        resetValue();

        cboCustomer.addActionListener(new ActionListener() {

             @Override
             public void actionPerformed(ActionEvent e) {

                 Client client =(Client) cboCustomer.getCValue();
                 if(client!=null){

                 }
              




             }
         });
        cboCenter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                setCliantListner();
                
            }
        });

    
    
        rdMicro.addActionListener(new ActionListener() {

             @Override
             public void actionPerformed(ActionEvent e) {
                 if(rdMicro.isSelected()){
                     cboCenter.setVisible(true);
                    cDLabel7.setVisible(true);
                 }else{
                     cboCenter.setVisible(false);
                    cDLabel7.setVisible(false);
                 }
                 cboCustomer.setEnabled(true);
                 cboCustomer.setValueEditable(true);
                  txtTotalCollection.setEnabled(true);
                 setCliantListner();
             }
         });
        
        rdNoneMicro.addActionListener(new ActionListener() {

             @Override
             public void actionPerformed(ActionEvent e) {
                 if(rdNoneMicro.isSelected()){
                     cboCenter.setVisible(false);
                    cDLabel7.setVisible(false);
                 }else{
                     cboCenter.setVisible(true);
                    cDLabel7.setVisible(true);
                 }
                 cboCustomer.setEnabled(true);
                 cboCustomer.setValueEditable(true);
                  txtTotalCollection.setEnabled(true);
                  List list = advancePayment.getClients(null,false);
                  cboCustomer.setCValue(list);
                  cboCustomer.doRefresh();
             }
         });
        
         txtTotalCollection.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                setAdvanceAmount();
            }
        });


    }
    public void resetValue(){
        txtMemo.resetValue();
        cboCustomer.resetValue();
        txtTotalCollection.resetValue();
        txtMemo.setEnabled(false);
        cboCustomer.setValueEditable(false);
        txtTotalCollection.setEnabled(false);
        txtTotalBalanceAmount.setEnabled(false);



    }
    private void setCliantListner(){
            // cboCustomer.setCValue("");
                LoanCenter center=(LoanCenter)cboCenter.getCValue();
                
                if(center!=null){
                    List list = advancePayment.getClients(center.getCode(),true);
                    if(list!=null){
                     cboCustomer.setCValue(list);
                    }
                   
                }
                  
                  cboCustomer.doRefresh();
            
        
        
    }

    private String getRefernceenarate() {
        return  ReferenceGenerator.ADVANE_REF + (String) CApplication.getSessionVariable(Finac.USER_PREFIX_KEY) + DATE_FORMAT.format(new Date());
    }

    @Action
    public void doSave() {

            try {

                advancePayment.saveTransaction(getTransactionTypeCode(),txtReferanceNo.getCValue(),setObject());




                resetValues();
            } catch (Exception e) {
                e.printStackTrace();
                mOptionPane.showMessageDialog(this, "Sace Faild", "Data saving ... :( \n"+e.getMessage(), mOptionPane.ERROR_MESSAGE);
            }


    }

    @Action
    public void doLoad() {


    }

      public void getclientDetails(String androidReceipt) {
        List<Loan> cashList;

        try {
            String hql = "FROM com.mac.cash_collection.template.zobject.Loan  where  agreementNo='"+androidReceipt+"' ";
            cashList = getDatabaseService().getCollection(hql, null);

            getDatabaseService().commitLocalTransaction();
        } catch (DatabaseException ex) {
            Logger.getLogger(LoanAdvanceSum.class.getName()).log(Level.SEVERE, null, ex);
            cashList = new ArrayList<>();
        }



    }





    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        tblCollection = new com.mac.af.component.derived.input.table.CITable();
        jPanel3 = new javax.swing.JPanel();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        cboCustomer = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                LoanCenter center=(LoanCenter)cboCenter.getCValue();

                if(center!=null && !rdNoneMicro.isSelected()){
                    return advancePayment.getClients(center.getCode(),true);
                }

                return advancePayment.getClients(null,false);
            }
        };
        btnLoadDetails = new com.mac.af.component.derived.command.button.CCButton();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtMemo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferanceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalCollection = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jPanel1 = new javax.swing.JPanel();
        rdMicro = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdNoneMicro = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cboCenter = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return advancePayment.getCenter();
            }
        };
        txtTotalBalanceAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();

        tblCollection.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblCollection);

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnLoadDetails.setText("Print");

        cDLabel4.setText("Transaction Date :");

        cDLabel3.setText("Reason:");

        cDLabel2.setText("Reference No:");

        cDLabel5.setText("Customer :");

        cDLabel9.setText("Payment:");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtering"));

        rdMicro.setText("MICRO");
        rdMicro.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        rdNoneMicro.setText("NONE MICRO");
        rdNoneMicro.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        cDLabel7.setText("Center:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(rdMicro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdNoneMicro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboCenter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdMicro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdNoneMicro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboCenter, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        cDLabel10.setText("Balance Amount:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
                            .addComponent(txtReferanceNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLoadDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotalBalanceAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalCollection, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMemo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferanceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotalBalanceAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotalCollection, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMemo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLoadDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(210, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 711, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 537, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSaveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnLoadDetails;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboCenter;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboCustomer;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdMicro;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdNoneMicro;
    private com.mac.af.component.derived.input.table.CITable tblCollection;
    private com.mac.af.component.derived.input.textfield.CIStringField txtMemo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferanceNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalBalanceAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalCollection;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables

    public String getFormattedDate(Date date) {
        return dateFormat.format(date);
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }

    private void resetValues() {


        txtMemo.setCValue(null);
        cboCustomer.resetValue();
        txtReferanceNo.setCValue(getRefernceenarate());


    }
    private LoanAdvanceRefund setObject(){

        LoanAdvanceRefund advanceSum=new LoanAdvanceRefund();

        Client client=(Client)  cboCustomer.getCValue();
        advanceSum.setLoan(null);
        advanceSum.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
        advanceSum.setTransaction(null);
        advanceSum.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        advanceSum.setCustomer(client);
        advanceSum.setMemo(txtMemo.getCValue());
        advanceSum.setAmount(txtTotalCollection.getCValue());
        advanceSum.setStatus("ACTIVE");
                
        return advanceSum;
    }
    
    
    double advance_amount=0.0;
    public void setAdvanceAmount(){
        
      //  LoanAdvanceTrans loan =(LoanAdvanceTrans) cboAgreementNo.getCValue();
        Client loan =(Client) cboCustomer.getCValue();
        
        
        
              Criteria  cr =   CPanel.GLOBAL.getDatabaseService().initCriteria(LoanAdvanceTrans.class).
                    add(Restrictions.eq("client", loan)).
                    add(Restrictions.eq("isDownpay", false)).
                    setProjection(Projections.sum("crAmount"));
              Criteria  dr =   CPanel.GLOBAL.getDatabaseService().initCriteria(LoanAdvanceTrans.class).
                    add(Restrictions.eq("client", loan)).
                      add(Restrictions.eq("isDownpay", false)).
                    setProjection(Projections.sum("drAmount"));
               advance_amount=0.0;
              if(dr.uniqueResult()!=null || cr.uniqueResult()!=null){
                   advance_amount = (double) dr.uniqueResult() - (double) cr.uniqueResult();
              }
              
              if(advance_amount<=0){
                  
                  txtTotalCollection.setEditable(false);
              }else{
                  
                   txtTotalCollection.setEditable(true);
              }
                      
        
              txtTotalBalanceAmount.setCValue(advance_amount);
              
              Double paymentAmount = txtTotalCollection.getCValue();
              
              
              if(paymentAmount>0 && paymentAmount<=advance_amount){
                  
                 
             
              }else{
            
                txtTotalCollection.setCValue(0.00);
                
              }
              
       
    }
}
