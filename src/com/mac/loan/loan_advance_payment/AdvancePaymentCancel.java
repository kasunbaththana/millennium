
package com.mac.loan.loan_advance_payment;

import com.mac.af.core.database.DatabaseException;
import com.mac.zsystem.transaction.transaction.SystemTransactionStatus;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction_cancel.TransactionCancel;
import com.mac.zsystem.transaction.transaction_cancel.object.Transaction;
import java.util.HashMap;

/**
 *
 * @author kasun
 */
public class AdvancePaymentCancel extends TransactionCancel {

    @Override
    protected String getTransactionType() {
        return SystemTransactions.ADVACNE_TRANSACTION_CODE;
    }

    @Override
    protected void cancelAdditional(Transaction transaction) throws DatabaseException {
        SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.TRANSACTION_CANCEL_TRANSACTION_CODE,
                transaction.getReferenceNo(),
                transaction.getDocumentNo(),
                null,
                null,
                transaction.getClient(),
                "Advane Receipt Cancel");

        
        cancelAdvancedPament(transaction.getIndexNo());
        
        
      
    }
    
     public void cancelAdvancedPament(int transaction) throws DatabaseException {
        String hql = "DELETE "
                + "com.mac.loan.loan_advance_payment.object.LoanAdvanceTrans "
                + "WHERE transaction=:TRANSACTION";
 
         HashMap<String, Object> hashMapAdvancedPayment = new HashMap<>();
        hashMapAdvancedPayment.put("TRANSACTION", transaction);
    
        
        String hql2 = "UPDATE "
                + "com.mac.loan.loan_advance_payment.object.LoanAdvanceSum "
                + "SET status=:STATUS "
                + "WHERE transaction=:TRANSACTION";
       
         HashMap<String, Object> hashMapAdvancedPayment2 = new HashMap<>();
        hashMapAdvancedPayment2.put("STATUS", SystemTransactionStatus.CANCEL);
        hashMapAdvancedPayment2.put("TRANSACTION", transaction);

        getDatabaseService().executeUpdate(hql, hashMapAdvancedPayment);
        getDatabaseService().executeUpdate(hql2, hashMapAdvancedPayment2);
    }

    
    
}
