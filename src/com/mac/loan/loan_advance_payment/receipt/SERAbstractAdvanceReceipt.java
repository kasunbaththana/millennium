/*
 *  SERAbstractReceipt.java
 * 
 *  @author Channa kasun
 *    
 * 
 *  Created on Sep 29, 2014, 9:51:39 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.loan_advance_payment.receipt;


import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.af.util.compare.comparison_chain.ComparisonChain;
import com.mac.cash_collection.template.SERCashCollection;
import com.mac.loan.loan_advance_payment.object.Client;
import com.mac.loan.loan_advance_payment.object.Loan;
import com.mac.loan.loan_advance_payment.object.LoanAdvanceSettlement;
import com.mac.loan.loan_advance_payment.object.LoanAdvanceTrans;
import com.mac.loan.loan_advance_payment.receipt.custom_object.Receipt;



import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.AdvanceSettleAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ReceiptAccountInterface;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.AdvancedPayment;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction.object.SettlementBalance;
import com.mac.zutil.royan_remind_letter.object.RemindLetterHistory;
import java.awt.Component;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author kasun
 */
public abstract class SERAbstractAdvanceReceipt extends AbstractService {
private Date WORKING_DATE =(Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
    public SERAbstractAdvanceReceipt(Component component) {
        super(component);
        try {
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException | DatabaseException ex) {
            Logger.getLogger(SERAbstractAdvanceReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public abstract List<Loan> getLoans();
    
   

    protected abstract void loanChanged();

    protected abstract String getSettlementCreitOrDebit();

    protected abstract String getChequeType();

    protected abstract String getAccountSettingCode();

    protected abstract String getOverAmountSettlementType();

    protected abstract String getTransactionName();

    protected abstract Loan getModifiedLoan(Loan loan, Double afterBalance);

    public int saveReceipt(final String transactionTypeCode, Receipt receipt) throws DatabaseException {

        boolean isPaymentOk = validation(transactionTypeCode, receipt);
        if (isPaymentOk) {

            //SAVE TRANSACTION
            final int transactionIndex = saveTransaction(transactionTypeCode, receipt);
          
            //SAVE SETTLEMENT
            int saveSettlement = saveSettlement(transactionTypeCode, receipt, transactionIndex);

     
            //ACCOUNT TRANSACTION
            saveAccount(transactionIndex, transactionTypeCode, receipt,interest,panalty,others,capital);

            
            //trans table
            
            saveAdvanceSettlement(transactionIndex, receipt);
            
            //REPORT
            if(saveSettlement==1){
            System.out.println(transactionTypeCode + "Printing....");
            Map<String, Object> params = new HashMap<>();
            params.put("TRANSACTION_NO", transactionIndex);
            TransactionUtil.PrintReport(getDatabaseService(), transactionTypeCode, params);
       //   
            }
            return AbstractRegistrationForm.SAVE_SUCCESS;
            
        
        } else {
           mOptionPane.showMessageDialog(null, "Save Faild", "Data saving ...", mOptionPane.ERROR_MESSAGE);
            return AbstractRegistrationForm.SAVE_FAILED;
        }
    }

    public boolean validation(String transactionTypeCode, Receipt receipt) throws DatabaseException {
        System.out.println(receipt.getAdvanceAmount()+" - "+receipt.getPaymentAmount());
        if(receipt.getAdvanceAmount() >= receipt.getPaymentAmount()){
            
            return true;
        }else{
            
            return false;
        }
        
        
      
    }
    public List<Client> getClients() {
        String hql = "FROM com.mac.loan.loan_advance_payment.object.Client where active=1";
        List<Client> loans;
        try {
            loans = getDatabaseService().getCollection(hql);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAbstractAdvanceReceipt.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }
     public List getLoanList(String client) {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.loan_advance_payment.object.Loan where client='"+client+"' and status='LOAN_START' ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERCashCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void savePayment(int transactionIndex, Receipt receipt) throws DatabaseException {
        systemPayment.savePayment(
                getDatabaseService(),
                cashierSession,
                transactionIndex,
                receipt.getLoan().getClient().getCode());
    }
    
       public void saveRemindLetter(int transactionIndex, Receipt receipt)  {
           try {
           RemindLetterHistory uniqueResult = (RemindLetterHistory) getDatabaseService()
                    .initCriteria(RemindLetterHistory.class)
                    .add(Restrictions.eq("loan.indexNo", receipt.getLoan().getIndexNo())).uniqueResult();
               
               
               System.out.println("receipt.getLoan().getIndexNo() REMIND LATTER "+receipt.getLoan().getIndexNo());
        if(uniqueResult!=null)
           {
               RemindLetterHistory remindLetterHistory =uniqueResult;
        remindLetterHistory.setStatus("PAY");
            getDatabaseService().update(remindLetterHistory);
               
           }
           } catch (Exception e) {
               e.printStackTrace();
           }
        
    }

    public int saveTransaction(String transactionTypeCode, Receipt receipt) throws DatabaseException {
        //SAVE TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                transactionTypeCode,
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getLoan().getIndexNo(),
                cashierSession.getIndexNo(),
                receipt.getLoan().getClient().getCode(),
                receipt.getNote());

        return transactionIndex;
    }
public void saveAdvanceSettlement(int transactionIndex,Receipt receipt){
        
    try {
        LoanAdvanceSettlement advanceSettlement=new LoanAdvanceSettlement();
     advanceSettlement.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
     advanceSettlement.setTransaction(transactionIndex);
     advanceSettlement.setTransactionDate(WORKING_DATE);
     advanceSettlement.setLoan(receipt.getLoan());
     advanceSettlement.setCustomer(receipt.getLoan().getClient().getCode());
     advanceSettlement.setAmount(receipt.getPaymentAmount());
     advanceSettlement.setMemo(receipt.getNote());
     advanceSettlement.setStatus("ACTIVE");
    
    
     LoanAdvanceTrans advanceTrans =new LoanAdvanceTrans();
            advanceTrans.setTransactionDate(WORKING_DATE);
            advanceTrans.setTransaction(transactionIndex);
            advanceTrans.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
            advanceTrans.setCrAmount(receipt.getPaymentAmount());
            advanceTrans.setDrAmount(0.00);
            advanceTrans.setLoan(advanceSettlement.getLoan());
            advanceTrans.setClient(advanceSettlement.getLoan().getClient());
            advanceTrans.setStatus("ACTIVE");
            advanceTrans.setIsDownpay(false);
            
            
            advanceSettlement=(LoanAdvanceSettlement) getDatabaseService().save(advanceSettlement);
            advanceTrans.setTransSettle(advanceSettlement.getIndexNo());
            getDatabaseService().save(advanceTrans);
            
            
    } catch (Exception e) {
        e.printStackTrace();
    }
     
            
    
}
    
    
    
    
    
    public void saveAccount(int transactionIndex, String transactionTypeCode, Receipt receipt,double interest,double panelty,double othercharge,double capital) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        
    
//      
       accountTransaction.addAccountTransactionQueue(
                AdvanceSettleAccountInterface.ADVANCE_SETTLE_AMOUNT_CREDIT_CODE,
                "Advance Settle  Amount",
                receipt.getPaymentAmount(),
                AccountTransactionType.AUTO);
       
       accountTransaction.addAccountTransactionQueue(
                AdvanceSettleAccountInterface.ADVANCE_SETTLE_AMOUNT_DEBIT_CODE,
                "Advance Settle  Amount",
                receipt.getPaymentAmount(),
                AccountTransactionType.AUTO);
         
         
//   ;
         accountTransaction.addAccountTransactionQueue(
                ReceiptAccountInterface.RECEIPT_PANELTY_CREDIT_CODE,
                "Advance Receipt Panelty Amount",
                panelty,
                AccountTransactionType.AUTO);
         
         accountTransaction.addAccountTransactionQueue(
                ReceiptAccountInterface.RECEIPT_INT_PNL_DEBIT_CODE,
                "Advance Receipt Panelty Amount",
                panelty,
                AccountTransactionType.AUTO);
         

      
        
        
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, transactionTypeCode);
    }

   

    public int saveSettlement(String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            systemSettlement.addSettlementHistoryQueue(
                    settlementHistory.getSettlement(),
                    transactionTypeCode,
                    transactionIndex,
                    settlementHistory.getSettlementAmount());
        }
        int flushSettlementHistoryQueue = systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
        return flushSettlementHistoryQueue;
        
    }
     double capital=0.0 , interest=0.0,panalty=0.0,others=0.0;
      double capitalover=0.0 , interestover=0.0,panaltyover=0.0,othersover=0.0;
    public void ListSettlement(String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        interest=0.0;panalty=0.0;others=0.0;
        Date NEXT_DUE_DATE;
        
        Calendar  calander = Calendar.getInstance();
        
                
        AdvancedPayment s = new AdvancedPayment();
//        SystemSettlement systemSettlement = SystemSettlement.getInstance();
//        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            
            String d= settlementHistory.getSettlement().getSettlementType().getCode().toString();
            Date systemdate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
            String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
            System.out.println("COMPAIRE : "+settlementHistory.getSettlement().getDueDate().compareTo(systemdate));
            
            
             if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_PANALTY"))
                {
                    panalty += settlementHistory.getSettlementAmount();
                }
            
            if(settlementHistory.getSettlement().getDueDate().before(systemdate) || settlementHistory.getSettlement().getDueDate().equals(systemdate))
            {
                
                if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_INTEREST"))
                {
                    interest += settlementHistory.getSettlementAmount();
                }
                
                else if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_CAPITAL"))
                {
                    capital += settlementHistory.getSettlementAmount();
                }
                else
                {
                    others += settlementHistory.getSettlementAmount();
                }
            }else if(settlementHistory.getSettlementAmount() > 0)
            {
               calander.setTime(settlementHistory.getSettlement().getDueDate());
               calander.add(calander.MONTH, 1);
               NEXT_DUE_DATE = calander.getTime();
              //  System.out.println("SETTLEMENT TYPE :"+settlementHistory.getSettlement().getSettlementType().getCode());
                
//                s.setAccount(settlementHistory.getSettlement().getDueDate());
                //LOAN_CAPITAL
              if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_CAPITAL))
              {
                s.setTransactionDate(systemdate);
                s.setBranch(branch);
                s.setClient(settlementHistory.getSettlement().getClient());
                s.setLoan(settlementHistory.getSettlement().getLoan());
                s.setInstallmentNo(0);
                s.setTransaction(transactionIndex);
                s.setTransactionType(settlementHistory.getSettlement().getTransactionType());
                s.setDescription(settlementHistory.getSettlement().getDescription());
                s.setAmount(settlementHistory.getSettlementAmount());
                s.setBalanceAmount(settlementHistory.getSettlementAmount());
                s.setDueDate(settlementHistory.getSettlement().getDueDate());
                s.setSettlementType(settlementHistory.getSettlement().getSettlementType().getCode());
                s.setAccount("11-100"); 
//                s.setAccount(ReceiptAccountInterface.RECEIPT_INTREST_CREDIT_CODE); 
              }
              //LOAN_INTEREST
              if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST))
              {
                s.setTransactionDate(systemdate);
                s.setBranch(branch);
                s.setClient(settlementHistory.getSettlement().getClient());
                s.setLoan(settlementHistory.getSettlement().getLoan());
                s.setInstallmentNo(0);
                s.setTransaction(transactionIndex);
                s.setTransactionType(settlementHistory.getSettlement().getTransactionType());
                s.setDescription(settlementHistory.getSettlement().getDescription());
                s.setAmount(settlementHistory.getSettlementAmount());
                s.setBalanceAmount(settlementHistory.getSettlementAmount());
                s.setDueDate(settlementHistory.getSettlement().getDueDate());
                s.setSettlementType(settlementHistory.getSettlement().getSettlementType().getCode());
                s.setAccount("31-100"); 
              }
             if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_CAPITAL)||
                     settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST))
             {
               s.setSend(false); 
               s.setStatus("ACTIVE"); 
                
                
                
                advancedPayment.add(s);
                
                 getDatabaseService().beginLocalTransaction();
                 getDatabaseService().save(advancedPayment);
                 getDatabaseService().commitLocalTransaction();
                advancedPayment.clear();
             }
                
                
            }
        }
        
       
        
        
    }
   
    
    
     public void saveSettlementrpt(String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
         initgetarearsaAount(receipt.getLoan());
         
        
        for (SettlementHistory settlementHistory : settlementHistories) {
            systemSettlement.addSettlementHistoryQueuerpt(
                    getDatabaseService(),
                    settlementHistory.getSettlement(),
                    transactionTypeCode,
                    transactionIndex,
                    settlementHistory.getSettlementAmount(),
                    getArrearsAmountRpt());
            break;
        }
         
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
    }
     public void savebalance_amount(Receipt receipt,String transactionTypeCode,int transactionIndex )throws DatabaseException 
     {
         SystemSettlement systemSettlement = SystemSettlement.getInstance();
         SettlementBalance settlementBalance=new SettlementBalance();
         settlementBalance.setDescription(transactionTypeCode);
         settlementBalance.setTransaction(transactionIndex);
         settlementBalance.setDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
//         settlementBalance.setBalanceAmount(systemSettlement.getbalance_amount(receipt.getLoan().getIndexNo(), settlementAmount,getDatabaseService()));
         settlementBalance.setBalanceAmount(getAfterBalanceAmount());
         settlementBalance.setInstallmentCount(systemSettlement.getinstallment_count(receipt.getLoan().getIndexNo()));
         settlementBalance.setStatus("Active");
         
         getDatabaseService().save(settlementBalance);
         
     }

    public void saveLoan(Loan loan) throws DatabaseException {
        loan = getModifiedLoan(loan, getAfterBalanceAmount());
        getDatabaseService().save(loan);
    }

    public void saveOverAmount(Receipt receipt, String transactionTypeCode, int transaction) throws DatabaseException {
        if (overAmount > 0) {
            SystemSettlement systemSettlement = SystemSettlement.getInstance();
            systemSettlement.beginSettlementQueue();
            systemSettlement.addSettlementQueue(
                    receipt.getTransactionDate(),
                    receipt.getLoan().getClient().getCode(),
                    receipt.getLoan().getIndexNo(),
                    null,
                    transactionTypeCode,
                    transaction,
                    getTransactionName() + " over amount",
                    overAmount,
                    getOverAmountSettlementType());

            systemSettlement.flushSettlementQueue(getDatabaseService());
        }
    }

    public List<SettlementHistory> getSettlementHistories() {
        if (settlementHistories == null) {
            return new ArrayList<>();
        }

        return settlementHistories;
    }
    

      public List getAdvanced(int loan) {
        List<AdvancedPayment> loans;

        String hql = "FROM com.mac.zsystem.transaction.settlement.object.AdvancedPayment WHERE send=:SEND and status=:STATUS and loan=:LOAN  ";
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("STATUS", "ACTIVE");
        hashMap.put("SEND", false);
        hashMap.put("LOAN", loan);

        try {
            loans = getDatabaseService().getCollection(hql, hashMap);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAbstractAdvanceReceipt.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }
    public void initSettlementHistory(Loan loan) {
        if (loan != null) {
            settlementHistories = new ArrayList<>();




            String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan=:LOAN AND settlementType.creditOrDebit=:CREDIT_OR_DEBIT AND status=:STATUS";
            HashMap<String, Object> params = new HashMap<>();
            params.put("LOAN", loan.getIndexNo());
            params.put("CREDIT_OR_DEBIT", getSettlementCreitOrDebit());
            params.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);
            
        


            /////////////////////////////////////// These for get Arrears Amount
            try {


                String hqlx = "SELECT SUM(balanceAmount) FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE status=:STATUS AND   settlementType<>'LOAN_AMOUNT' AND  loan=:LOAN AND dueDate<=:DUEDATE";
                HashMap<String, Object> paramsx = new HashMap<>();
                paramsx.put("LOAN", loan.getIndexNo());
                paramsx.put("DUEDATE", (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                paramsx.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);

                arrearsAmount = (Double) getDatabaseService().getUniqueResultHQL(hqlx, paramsx);



            } catch (Exception ex) {
                Logger.getLogger(SERAbstractAdvanceReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }



            try {
                List<Settlement> settlements = getDatabaseService().getCollection(hql, params);
                for (Settlement settlement : settlements) {

                    SettlementHistory settlementHistory = new SettlementHistory();

                    settlementHistory.setIndexNo(null);
                    settlementHistory.setSettlement(settlement);
                    settlementHistories.add(settlementHistory);
                }

            } catch (Exception ex) {
                Logger.getLogger(SERAbstractAdvanceReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        sortReceiptReceipts(settlementHistories);
        initSettlementAmount();
    }
public void initgetarearsaAount(Loan loan)
    {
           List<Settlement> list;
            
            try {
                String hqlx = " FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE status=:STATUS AND  settlementType<>'LOAN_AMOUNT' AND  loan=:LOAN AND dueDate<=:DUEDATE";
                HashMap<String, Object> paramsx = new HashMap<>();
                paramsx.put("LOAN", loan.getIndexNo());
                paramsx.put("DUEDATE", (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                paramsx.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);
                list = getDatabaseService().getCollection(hqlx,paramsx);
                //arrearsAmount = (Double) getDatabaseService().getUniqueResultHQL(hqlx, paramsx);
                
                for(Settlement listSettlement:list)
                {
                 arrearsAmountRpt += listSettlement.getBalanceAmount();
                }
                
                    System.out.println("arrearsAmount_____"+arrearsAmountRpt);

            } catch (Exception ex) {
                Logger.getLogger(SERAbstractAdvanceReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }
    }


public double initBlockAgreement(Loan loan)
    {
           List<Settlement> list;
            double arrearsAmounta=0.0;
            try {
                String hqlx = "SELECT SUM(balanceAmount)/"+loan.getInstallmentAmount()+" FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE status=:STATUS AND  settlementType<>'LOAN_AMOUNT' AND  loan=:LOAN AND dueDate<=:DUEDATE";
                HashMap<String, Object> paramsx = new HashMap<>();
                paramsx.put("LOAN", loan.getIndexNo());
                paramsx.put("DUEDATE", (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                paramsx.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);
                list = getDatabaseService().getCollection(hqlx,paramsx);
                //arrearsAmount = (Double) getDatabaseService().getUniqueResultHQL(hqlx, paramsx);
                
                arrearsAmounta = (Double) getDatabaseService().getUniqueResultHQL(hqlx, paramsx);
                
                    System.out.println("arrearsAmount_____"+arrearsAmountRpt);

            } catch (Exception ex) {
                Logger.getLogger(SERAbstractAdvanceReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }
            
           return arrearsAmounta;
    }


    public void sortReceiptReceipts(List<SettlementHistory> settlementHistory) {
        List<SettlementHistory> panalties = new ArrayList<>();
        for (SettlementHistory panalty : settlementHistory) {
            if (panalty.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                panalties.add(panalty);
            }
        }
        settlementHistory.removeAll(panalties);

        Collections.sort(settlementHistory, new Comparator<SettlementHistory>() {
            @Override
            public int compare(SettlementHistory o1, SettlementHistory o2) {
                return ComparisonChain.start()
                        .compare(o1.getSettlement().getDueDate(), o2.getSettlement().getDueDate())
                        .compare(o1.getSettlement().getSettlementType().getPriority(), o2.getSettlement().getSettlementType().getPriority())
                        .result();
            }
        });
        settlementHistory.addAll(0, panalties);
    }
//nimesh
    private void initSettlementAmount() {
        //SET BALANCE AMOUNT

        double temp = settlementAmount;
        balanceAmount = 0.0;
        penaltyAmount = 0.0;

        Iterator<SettlementHistory> iterator = settlementHistories.iterator();
        SettlementHistory settlementHistory;
        double settlement;
        double balance;
        while (iterator.hasNext()) {
            settlementHistory = iterator.next();
            balance = settlementHistory.getSettlement().getBalanceAmount();
            settlement = Math.min(balance, temp);
            temp = temp - settlement;
            settlementHistory.setSettlementAmount(settlement);
            loan = settlementHistory.getSettlement().getLoan();
            balanceAmount = balanceAmount + balance;

            if (settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                penaltyAmount += settlementHistory.getSettlement().getBalanceAmount();
            }
        }

        if (temp > 0) {
            //OVER PAYMENT
            overAmount = temp;
        } else {
            overAmount = 0.0;
        }

        loanChanged();
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public Double getPenaltyAmount() {
        return penaltyAmount;
    }

    public Double getAfterBalanceAmount() {
        return balanceAmount - settlementAmount;
    }

    public Double getOverPaymentAmount() {
        return overAmount;
    }
    public Double getReciptOverPaymentAmount() {
        return overAmountrecipt;
    }

    public void setSettlementAmount(Double settlementAmount) {
        this.settlementAmount = settlementAmount;

        initSettlementAmount();
    }

    public Double getArrearsAmount() {
        return arrearsAmount;
    }
    public int getloan() {
        return loan;
    }
    
    public Double getArrearsAmountRpt() {
        return arrearsAmountRpt;
    }
            private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();

        return connection;
    }
    public double checkDueInstallment()
    {
       
       
         Date systemdate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
   
       
        overAmountrecipt = 0.0;

        Iterator<SettlementHistory> iterator = settlementHistories.iterator();
        SettlementHistory settlementHistory;
      
        while (iterator.hasNext()) {
            settlementHistory = iterator.next();
            
                if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_CAPITAL")
                        || settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_INTEREST"))
                {
                    if (settlementHistory.getSettlement().getDueDate().before(systemdate) 
                            || settlementHistory.getSettlement().getDueDate().equals(systemdate)) 
                    {
                overAmountrecipt +=  settlementHistory.getSettlementAmount();
                    }
                }
                else //if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY))
                {
                overAmountrecipt +=  settlementHistory.getSettlementAmount();   
                }
            
        }
        return overAmountrecipt;
//       
    }
    //
    protected List<SettlementHistory> settlementHistories = new ArrayList<>();
    protected List<AdvancedPayment> advancedPayment = new ArrayList<>();
    protected Double settlementAmount = 0.0;
    protected Double balanceAmount = 0.0;
    protected Double arrearsAmount = 0.0;
    protected Double overAmount = 0.0;
    protected Double overAmountrecipt = 0.0;
    protected Double arrearsAmountRpt = 0.0;
    protected Double penaltyAmount = 0.0;
    private static DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
    private CashierSession cashierSession;
    private SystemPayment systemPayment;
    private int loan;
}
