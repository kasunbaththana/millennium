package com.mac.loan.loan_customer_payment.zobject;

/**
  *	@author Channa Mohan
  *	
  *	Created On Jun 15, 2016 2:15:24 PM 
  *	Mohan Hibernate Mapping Generator
  */


import java.util.HashSet;
import java.util.Set;

/**
 * CusChargs generated by hbm2java
 */
public class CusChargs  implements java.io.Serializable {


     private String code;
     private String name;
     private Double amount;
     private String account;
     private Set<CusPaymentHistory> cusPaymentHistories = new HashSet<CusPaymentHistory>(0);

    public CusChargs() {
    }

	
    public CusChargs(String code, String name, String account) {
        this.code = code;
        this.name = name;
        this.account = account;
    }
    public CusChargs(String code, String name, Double amount, String account, Set<CusPaymentHistory> cusPaymentHistories) {
       this.code = code;
       this.name = name;
       this.amount = amount;
       this.account = account;
       this.cusPaymentHistories = cusPaymentHistories;
    }
   
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public Double getAmount() {
        return this.amount;
    }
    
    public void setAmount(Double amount) {
        this.amount = amount;
    }
    public String getAccount() {
        return this.account;
    }
    
    public void setAccount(String account) {
        this.account = account;
    }
    public Set<CusPaymentHistory> getCusPaymentHistories() {
        return this.cusPaymentHistories;
    }
    
    public void setCusPaymentHistories(Set<CusPaymentHistory> cusPaymentHistories) {
        this.cusPaymentHistories = cusPaymentHistories;
    }


	@Override
     public String toString() {
		return code + "-" + name;
     }

@Override
public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof CusChargs) ) return false;
		 CusChargs castOther = ( CusChargs ) other; 

	if(!java.util.Objects.equals(this.code, castOther.code)) {
            return false;
     }
	if(!java.util.Objects.equals(this.name, castOther.name)) {
            return false;
     }
	if(!java.util.Objects.equals(this.account, castOther.account)) {
            return false;
     }
         
		 return true;
   }

   @Override
   public int hashCode() {
         int result = 17;
         
	result = result * 17 + java.util.Objects.hashCode(this.code);
	result = result * 17 + java.util.Objects.hashCode(this.name);
	result = result * 17 + java.util.Objects.hashCode(this.account);

         return result;
   }   

}


