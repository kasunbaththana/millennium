/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_customer_payment;

import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.loan_customer_payment.zobject.CusPayment;
import com.mac.loan.loan_customer_payment.zobject.CusPaymentHistory;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.CustomerPaymentAccountInterface;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author NIMESH-PC
 */
public class SERCustomerLoanPayment extends AbstractService {

    public SERCustomerLoanPayment(Component component) {
        super(component);
    }

    public void save(CusPayment transaction, Collection<CusPaymentHistory> chargers) throws DatabaseException {
        //TRANSACTION
        int transactionIndex = saveTransaction(transaction);
        //ACCOUNT TRANSACTION
        saveAccount(transactionIndex,SystemTransactions.CUSTOMER_PAYMENT, (Set<CusPaymentHistory>) chargers);
    }

    public int saveTransaction(CusPayment payment) throws DatabaseException {
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.CUSTOMER_PAYMENT,
                payment.getReferanceNo(),
                payment.getDocumentNo(),
                null,
                null,
                null,
                "Note");

        return transactionIndex;
    }

//    public void saveAccountEntries(int transactionIndex, Collection<CusPaymentHistory> chargers) throws DatabaseException {
//        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
//        for (CusPaymentHistory entry : chargers) {
//            accountTransaction.saveAccountTransaction(
//                    getDatabaseService(),
//                    transactionIndex,
//                    SystemTransactions.CUSTOMER_PAYMENT,
//                    AccountTransactionType.AUTO,
//                    AccountSettingCreditOrDebit.CREDIT,
//                    entry.getAmount(),
//                    AccountSetting.CUSTOMER_PAYMENT_AMOUNT_CREDIT_CODE,
//                    entry.getChargName());
//        }
//    }
    
     public void saveAccount(int transactionIndex, String transactionTypeCode, Set<CusPaymentHistory>   histories) throws DatabaseException {
       double totalAmount=0;
         SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        
         for (CusPaymentHistory getTotAmount : histories) {
             totalAmount=totalAmount+getTotAmount.getAmount();
         }
        accountTransaction.addAccountTransactionQueue(
                CustomerPaymentAccountInterface.CUSTOMER_PAYMENT_AMOUNT_CREDIT_CODE,
                "Customer Payment Amount", totalAmount,
                AccountTransactionType.AUTO);
        
         for (CusPaymentHistory cusHistory : histories) {
        accountTransaction.saveAccountTransaction(
                            getDatabaseService(),
                            transactionIndex,
                            SystemTransactions.CUSTOMER_PAYMENT,
                            AccountTransactionType.AUTO,
                            AccountSettingCreditOrDebit.CREDIT,
                            cusHistory.getAmount(),
                            cusHistory.getCusChargs().getAccount(),//account
                            "Customer Payment - " + cusHistory.getCusChargs().getCode(),0);
         }
        accountTransaction.flushAccountTransactionQueue(getCPanel().getDatabaseService(), transactionIndex, transactionTypeCode);
    }

    public boolean showPaymentDialog(CusPayment payment, String chequeType) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(SystemTransactions.CUSTOMER_PAYMENT);

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                payment.getReferanceNo(),
                payment.getDocumentNo(),
                payment.getTransactionDate(),
                payment.getTotalAmount(),
                chequeType);

        return isPaymentOk;
    }

    public void savePayment(int transactionIndex, CusPayment payment) throws DatabaseException, ApplicationException {
        systemPayment.savePayment(
                getDatabaseService(),
                SystemCashier.getCurrentCashierSession(getDatabaseService()),
                transactionIndex,
                null);
    }

//    public void saveCusPaymentHistory(CusPaymentHistory cusPaymentHistory) {
//        try {
//            getDatabaseService().save(cusPaymentHistory);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(SERCustomerLoanPayment.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }
    
    //getNew ReceiptNO
//    int maxReceiptNo = 0;
//    public int getReceiptNo(LoanType loanType) {
//        try {
//            ResultSet executeQuery = getConnection().createStatement().executeQuery("SELECT MAX(cus_payment.receipt_no) FROM cus_payment WHERE cus_payment.`type`='"+loanType.getCode()+"'");
//            while (executeQuery.next()) {
//                maxReceiptNo = executeQuery.getInt(1);
//            }
//            
//            System.out.println("maxReceiptNo_________________________________"+maxReceiptNo);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return maxReceiptNo + 1;
//    }

    
    
    
    
    
    private SystemPayment systemPayment;
}
