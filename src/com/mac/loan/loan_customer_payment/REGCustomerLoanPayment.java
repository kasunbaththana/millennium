/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_customer_payment;

import com.mac.account.ztemplate.entry_payment.EntryPayment;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import static com.mac.af.templates.registration.AbstractRegistrationForm.SAVE_FAILED;
import com.mac.loan.loan_customer_payment.zobject.CusPayment;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NIMESH-PC
 */
public class REGCustomerLoanPayment extends AbstractRegistrationForm<CusPayment> {

    public REGCustomerLoanPayment() {
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.CUSTOMER_PAYMENT);
        this.recentButton.setDatabseService(getDatabaseService());
    }

    @Override
    public AbstractObjectCreator<CusPayment> getObjectCreator() {
        return new PCCustomerPayment();
    }

    @Override
    public Class<? extends CusPayment> getObjectClass() {
        return CusPayment.class;
    }

    @Override
    public CTableModel<CusPayment> getTableModel() {
        return new CTableModel();
    }

//    @Override
//    protected int getRegistrationType() {
//        return NEW_ONLY_REGISTRATION_TYPE;
//    }

    public String getTransActionType() {
        return SystemTransactions.CUSTOMER_PAYMENT;

    }

    @Override
    protected int save(CusPayment object) throws DatabaseException {
        try {
            boolean isPaymentOK = getService().showPaymentDialog(
                    object,
                    "Cheque");

            if (isPaymentOK) {
                //SAVE TRANSACTION
                int transactionIndex = getService().saveTransaction(object);

                //SAVE ACCOUNT ENTRIES
                getService().saveAccount(transactionIndex, getTransActionType(), object.getCusPaymentHistories());

                //SAVE PAYMENTS
                getService().savePayment(transactionIndex, object);

                object.setTransaction(transactionIndex);


                getDatabaseService().save(object);



                //REPORT
                Map<String, Object> params = new HashMap<>();
                params.put("TRANSACTION_NO", transactionIndex);
                TransactionUtil.PrintReport(getDatabaseService(), getTransActionType(), params);


                return SAVE_SUCCESS;
            }

            return SAVE_FAILED;
        } catch (DatabaseException ex) {
            Logger.getLogger(EntryPayment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ApplicationException ex) {
            Logger.getLogger(EntryPayment.class.getName()).log(Level.SEVERE, null, ex);
        }
        return SAVE_FAILED;
    }

    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
    }

    public SERCustomerLoanPayment getService() {
        initService();
        return sERCustomerLoanPayment;
    }

    private void initService() {
        if (sERCustomerLoanPayment == null) {
            sERCustomerLoanPayment = new SERCustomerLoanPayment(this);
        }
    }
    private SERCustomerLoanPayment sERCustomerLoanPayment;
    private RecentButton recentButton;
}
