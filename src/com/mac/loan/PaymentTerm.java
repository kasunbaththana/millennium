/*
 *  PaymentTerm.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 4, 2014
 *  
 */
package com.mac.loan;

/**
 *
 * @author mohan
 */
public class PaymentTerm {

    public static final String DAILY = "DAILY";
    public static final String WEEKLY = "WEEKLY";
    public static final String MONTHLY = "MONTHLY";
    public static final String QUARTER = "QUARTER";
    //all
    public static final String[] ALL = {
        DAILY,
        WEEKLY,
        MONTHLY,
        QUARTER
    };
    //
}
