/*
 *  LoanInformation.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 26, 2014, 10:32:58 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.zutil.calculation;

import java.util.Date;

/**
 *
 * @author mohan
 */
public class LoanInformation {

    private double loanAmount;
    private double nominateInterestRate;
    private int paymentCount;
    private String paymentTerm;
    private String interestMethod;
    private Date loanDate;
    //
    private double totalAmount;
    private double interestAmount;
    private double installmentAmount;
    private double effectiveInterestRate;
    //

    public LoanInformation() {
    }

    public double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public double getNominateInterestRate() {
        return nominateInterestRate;
    }

    public void setNominateInterestRate(double nominateInterestRate) {
        this.nominateInterestRate = nominateInterestRate;
    }

    public int getPaymentCount() {
        return paymentCount;
    }

    public void setPaymentCount(int paymentCount) {
        this.paymentCount = paymentCount;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getInterestMethod() {
        return interestMethod;
    }

    public void setInterestMethod(String interestMethod) {
        this.interestMethod = interestMethod;
    }

    public Date getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }

    public double getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(double installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public double getInterestAmount() {
        return interestAmount;
    }

    public void setInterestAmount(double interestAmount) {
        this.interestAmount = interestAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getEffectiveInterestRate() {
        return effectiveInterestRate;
    }

    public void setEffectiveInterestRate(double effectiveInterestRate) {
        this.effectiveInterestRate = effectiveInterestRate;
    }
}
