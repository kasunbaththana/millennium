/*
 *  LoanCalculationException.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 26, 2014, 10:20:29 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.zutil.calculation;

/**
 *
 * @author mohan
 */
public class LoanCalculationException extends Exception {

    public LoanCalculationException() {
    }

    public LoanCalculationException(String message) {
        super(message);
    }
}
