/*
 *  LoanPaymentInformation.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 26, 2014, 10:33:17 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.zutil.calculation;

import java.util.Date;

/**
 *
 * @author mohan
 */
public class LoanPaymentInformation {

    private int index;
    private double capitalAmount;
    private double interestAmount;
    private double paymentAmount;
    private Date paymentDate;

    public LoanPaymentInformation() {
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public double getCapitalAmount() {
        return capitalAmount;
    }

    public void setCapitalAmount(double capitalAmount) {
        this.capitalAmount = capitalAmount;
    }

    public double getInterestAmount() {
        return interestAmount;
    }

    public void setInterestAmount(double interestAmount) {
        this.interestAmount = interestAmount;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
}
