/*
 *  LoanCalculationUtil.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 26, 2014, 9:54:36 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.zutil.calculation;

import com.mac.loan.InterestMethod;
import com.mac.loan.PanaltyType;
import com.mac.loan.PaymentTerm;
import com.mac.loan.loan_application.object_creator.PCLoan;
import com.mac.registration.company_leave_day.object.CompanyLeaveDay;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import java.text.DecimalFormat;

/**
 *
 * @author mohan
 */
public class LoanCalculationUtil {
    //INSTALLMENT AMOUNT

    public static LoanInformation getInstallmentAmount(
            Double loanAmount,
            Double annualInterestRate,
            Integer installmentCount,
            String paymentTerm,
            String interestMethod,
            Date loanDate) throws LoanCalculationException {

        if (loanAmount == null
                || annualInterestRate == null
                || installmentCount == null
                || paymentTerm == null
                || interestMethod == null) {
            throw new LoanCalculationException("Required data not found");
        }

        if (installmentCount == 0) {
            throw new LoanCalculationException("Zero payment count");
        }
        if (Arrays.binarySearch(PaymentTerm.ALL, paymentTerm) == -1) {
            throw new LoanCalculationException("Unrecognized payment term");
        }

        if (loanDate == null) {
            loanDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
            if (loanDate == null) {
                throw new LoanCalculationException("Unrecognized loan date");
            }
        }


        LoanInformation loanInformation;

        double _loanAmount = loanAmount.doubleValue();
        double _annualInterestRate = annualInterestRate.doubleValue();
        int _installmentCount = installmentCount.intValue();
        switch (interestMethod) {
            case InterestMethod.FLAT:
                loanInformation = getFlatLoanInformation(_loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.REDUCING:
                loanInformation = getRedusingLoanInformation(_loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.FLAT_INTERST_FIRST:
                loanInformation = getFlatInterstFirstLoanInformation(_loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.REDUCING_100:
                loanInformation = getFlatRedusing100LoanInformation(_loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.FLAT_QUARTER:
                loanInformation = getFlatQuarterLoanInformation(_loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.FLAT_INTERST_INSTALLMENT_FIRST:
                loanInformation = getFlatInterstInstallmentFirstLoaninformation(_loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;

            default:
                throw new LoanCalculationException("Unrecognized interest method");
        }

        return loanInformation;
    }

    private static LoanInformation getFlatLoanInformation(
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        LoanInformation loanInformation = new LoanInformation();

        loanInformation.setLoanAmount(loanAmount);
        loanInformation.setNominateInterestRate(annualInterestRate);
        loanInformation.setPaymentCount(installmentCount);
        loanInformation.setPaymentTerm(paymentTerm);
        loanInformation.setInterestMethod(InterestMethod.FLAT);
        loanInformation.setLoanDate(loanDate);

        double currentInterestRate = 0.0;
        double installmentAmount = 0.0;
        double paymentCapital = loanAmount / installmentCount;
        switch (paymentTerm) {
            case PaymentTerm.DAILY:
                currentInterestRate = annualInterestRate / 300;
                break;
            case PaymentTerm.WEEKLY:
                currentInterestRate = annualInterestRate / 48;
                break;
            case PaymentTerm.MONTHLY:
                currentInterestRate = annualInterestRate / 12;
                break;
            case PaymentTerm.QUARTER:
                currentInterestRate = annualInterestRate / 2;
                break;
        }
        if (paymentTerm.equals(PaymentTerm.QUARTER)) {
            installmentAmount = (paymentCapital + (loanAmount * currentInterestRate / 100)) * 6;
        } else {
            installmentAmount = paymentCapital + (loanAmount * currentInterestRate / 100);
        }
        double interestAmount = loanAmount * (currentInterestRate * installmentCount) / 100;
        double totalAmount = loanAmount + interestAmount;

        loanInformation.setTotalAmount(totalAmount);
        loanInformation.setInterestAmount(interestAmount);
        loanInformation.setInstallmentAmount(installmentAmount);
        loanInformation.setEffectiveInterestRate(annualInterestRate);

        return loanInformation;
    }

    private static LoanInformation getFlatInterstInstallmentFirstLoaninformation(
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        LoanInformation loanInformation = new LoanInformation();

        loanInformation.setLoanAmount(loanAmount);
        loanInformation.setNominateInterestRate(annualInterestRate);
        loanInformation.setPaymentCount(installmentCount);
        loanInformation.setPaymentTerm(paymentTerm);
        loanInformation.setInterestMethod(InterestMethod.FLAT);
        loanInformation.setLoanDate(loanDate);

        double currentInterestRate = 0.0;
        double installmentAmount = 0.0;
        double paymentCapital = loanAmount / installmentCount;
        switch (paymentTerm) {
            case PaymentTerm.DAILY:
                currentInterestRate = annualInterestRate / 300;
                break;
            case PaymentTerm.WEEKLY:
                currentInterestRate = annualInterestRate / 48;
                break;
            case PaymentTerm.MONTHLY:
                currentInterestRate = annualInterestRate / 12;
                break;
            case PaymentTerm.QUARTER:
                currentInterestRate = annualInterestRate / 2;
                break;
        }
        if (paymentTerm.equals(PaymentTerm.QUARTER)) {
            installmentAmount = (paymentCapital + (loanAmount * currentInterestRate / 100)) * 6;
        } else {
            installmentAmount = paymentCapital + (loanAmount * currentInterestRate / 100);
        }
        double interestAmount = loanAmount * (currentInterestRate * installmentCount) / 100;
        double totalAmount = loanAmount + interestAmount;

        loanInformation.setTotalAmount(totalAmount);
        loanInformation.setInterestAmount(interestAmount);
        loanInformation.setInstallmentAmount(installmentAmount);
        loanInformation.setEffectiveInterestRate(annualInterestRate);

        return loanInformation;
    }

    private static LoanInformation getFlatQuarterLoanInformation(
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        LoanInformation loanInformation = new LoanInformation();

        loanInformation.setLoanAmount(loanAmount);
        loanInformation.setNominateInterestRate(annualInterestRate);
        loanInformation.setPaymentCount(installmentCount);
        loanInformation.setPaymentTerm(paymentTerm);
        loanInformation.setInterestMethod(InterestMethod.FLAT);
        loanInformation.setLoanDate(loanDate);

        double currentInterestRate = 0.0;
        double installmentAmount = 0.0;
        double paymentCapital = loanAmount / installmentCount;
        switch (paymentTerm) {
            case PaymentTerm.DAILY:
                currentInterestRate = annualInterestRate / 300;
                break;
            case PaymentTerm.WEEKLY:
                currentInterestRate = annualInterestRate / 48;
                break;
            case PaymentTerm.MONTHLY:
                currentInterestRate = annualInterestRate / 12;
                break;
            case PaymentTerm.QUARTER:
                currentInterestRate = annualInterestRate / 2;
                break;
        }
        installmentAmount = paymentCapital + (loanAmount * currentInterestRate / 100);
        double interestAmount = loanAmount * (currentInterestRate * installmentCount) / 100;
        double totalAmount = loanAmount + interestAmount;

        loanInformation.setTotalAmount(totalAmount);
        loanInformation.setInterestAmount(interestAmount);
        loanInformation.setInstallmentAmount(installmentAmount);
        loanInformation.setEffectiveInterestRate(annualInterestRate);

        return loanInformation;
    }

    //new wee singhe puthram finance 100 ok
    private static LoanInformation getFlatRedusing100LoanInformation(
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        LoanInformation loanInformation = new LoanInformation();

        loanInformation.setLoanAmount(loanAmount);
        loanInformation.setNominateInterestRate(annualInterestRate);
        loanInformation.setPaymentCount(installmentCount);
        loanInformation.setPaymentTerm(paymentTerm);
        loanInformation.setInterestMethod(InterestMethod.REDUCING_100);
        loanInformation.setLoanDate(loanDate);

         double currentInterestRate = 0.0;
        double installmentAmount = 0.0;
        double paymentCapital = loanAmount / installmentCount;
        switch (paymentTerm) {
            case PaymentTerm.DAILY:
                currentInterestRate = annualInterestRate / 300;
                break;
            case PaymentTerm.WEEKLY:
                currentInterestRate = annualInterestRate / 48;
                break;
            case PaymentTerm.MONTHLY:
                currentInterestRate = annualInterestRate / 12;
                break;
            case PaymentTerm.QUARTER:
                currentInterestRate = annualInterestRate / 2;
                break;
        }
        if (paymentTerm.equals(PaymentTerm.QUARTER)) {
            installmentAmount = (paymentCapital + (loanAmount * currentInterestRate / 100)) * 6;
        } else {
            installmentAmount = paymentCapital + (loanAmount * currentInterestRate / 100);
        }
        double interestAmount = loanAmount * (currentInterestRate * installmentCount) / 100;
        double totalAmount = loanAmount + interestAmount;

        loanInformation.setTotalAmount(totalAmount);
        loanInformation.setInterestAmount(interestAmount);
        loanInformation.setInstallmentAmount(installmentAmount);
        loanInformation.setEffectiveInterestRate(annualInterestRate);

        return loanInformation;
    }

    private static LoanInformation getFlatInterstFirstLoanInformation(
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        LoanInformation loanInformation = new LoanInformation();

        loanInformation.setLoanAmount(loanAmount);
        loanInformation.setNominateInterestRate(annualInterestRate);
        loanInformation.setPaymentCount(installmentCount);
        loanInformation.setPaymentTerm(paymentTerm);
        loanInformation.setInterestMethod(InterestMethod.FLAT_INTERST_FIRST);
        loanInformation.setLoanDate(loanDate);

        double currentInterestRate = 0.0;
        double paymentCapital = loanAmount / installmentCount;
        switch (paymentTerm) {
            case PaymentTerm.DAILY:
                currentInterestRate = annualInterestRate / 300;
                break;
            case PaymentTerm.WEEKLY:
                currentInterestRate = annualInterestRate / 48;
                break;
            case PaymentTerm.MONTHLY:
                currentInterestRate = annualInterestRate / 12;
                break;
        }
        double installmentAmount = loanAmount * currentInterestRate / 100;
        double interestAmount = loanAmount * (currentInterestRate * installmentCount) / 100;
        double totalAmount = loanAmount + interestAmount;

        loanInformation.setTotalAmount(totalAmount);
        loanInformation.setInterestAmount(interestAmount);
        loanInformation.setInstallmentAmount(installmentAmount);
        loanInformation.setEffectiveInterestRate(annualInterestRate);

        return loanInformation;
    }

    private static LoanInformation getRedusingLoanInformation(
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        LoanInformation loanInformation = new LoanInformation();

        loanInformation.setLoanAmount(loanAmount);
        loanInformation.setNominateInterestRate(annualInterestRate);
        loanInformation.setPaymentCount(installmentCount);
        loanInformation.setPaymentTerm(paymentTerm);
        loanInformation.setInterestMethod(InterestMethod.REDUCING);
        loanInformation.setLoanDate(loanDate);

        double currentInterestRate = 0.0;
        double paymentCapital = loanAmount / installmentCount;
        switch (paymentTerm) {
            case PaymentTerm.DAILY:
                currentInterestRate = annualInterestRate / 300 / 100;
                break;
            case PaymentTerm.WEEKLY:
                currentInterestRate = annualInterestRate / 48 / 100;
                break;
            case PaymentTerm.MONTHLY:
                currentInterestRate = annualInterestRate / 12 / 100;
                break;
        }

        double installmentAmount = (loanAmount * currentInterestRate) / (1 - Math.pow(1 + currentInterestRate, installmentCount * -1));
       
        double interestAmount = (installmentAmount * (double) installmentCount) - loanAmount;
        double totalAmount = loanAmount + interestAmount;

        loanInformation.setTotalAmount(totalAmount);
        loanInformation.setInterestAmount(interestAmount);
        loanInformation.setInstallmentAmount(installmentAmount);
        loanInformation.setEffectiveInterestRate(annualInterestRate);
//         double installmentAmount = 0.0;
//         double paymentCapital = loanAmount / installmentCount;
//        switch (paymentTerm) {
//            case PaymentTerm.DAILY:
//                currentInterestRate = annualInterestRate / 300;
//                break;
//            case PaymentTerm.WEEKLY:
//                currentInterestRate = annualInterestRate / 48;
//                break;
//            case PaymentTerm.MONTHLY:
//                currentInterestRate = annualInterestRate / 12;
//                break;
//        }
//        
//        installmentAmount = paymentCapital + (loanAmount * currentInterestRate / 100);
//         double interestAmount = loanAmount * (currentInterestRate * installmentCount) / 100;
//        double totalAmount = loanAmount + interestAmount;
//
//        loanInformation.setTotalAmount(totalAmount);
//        loanInformation.setInterestAmount(interestAmount);
//        loanInformation.setInstallmentAmount(installmentAmount);
//        loanInformation.setEffectiveInterestRate(annualInterestRate);

        return loanInformation;
    }

    //PAYMENT SCHEDULE
    public static List<LoanPaymentInformation> getPaymentSchedule(
            HibernateDatabaseService databaseService,
            Double loanAmount,
            Double annualInterestRate,
            Integer installmentCount,
            String paymentTerm,
            String interestMethod,
            Date loanDate) throws LoanCalculationException {
        
        System.out.println("loanAmount_"+loanAmount);
        System.out.println("annualInterestRate_"+annualInterestRate);
        System.out.println("installmentCount_"+installmentCount);
        System.out.println("paymentTerm_"+paymentTerm);
        System.out.println("interestMethod_"+interestMethod);

        if (loanAmount == null
                || annualInterestRate == null
                || installmentCount == null
                || paymentTerm == null
                || interestMethod == null) {
            throw new LoanCalculationException("Required data not found");
        }

        if (installmentCount == 0) {
            throw new LoanCalculationException("Zero payment count");
        }
        if (Arrays.binarySearch(PaymentTerm.ALL, paymentTerm) == -1) {
            throw new LoanCalculationException("Unrecognized payment term");
        }

        if (loanDate == null) {
            loanDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
            if (loanDate == null) {
                throw new LoanCalculationException("Unrecognized loan date");
            }
        }

        List<LoanPaymentInformation> scheduleObjects = Collections.emptyList();
        double _loanAmount = loanAmount.doubleValue();
        double _annualInterestRate = annualInterestRate.doubleValue();
        int _installmentCount = installmentCount.intValue();

        switch (interestMethod) {
            case InterestMethod.FLAT:
                scheduleObjects = getFlatPaymentSchedule(databaseService, _loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.REDUCING:
                scheduleObjects = getRedusingPaymentSchedule(databaseService, _loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.FLAT_INTERST_FIRST:
                scheduleObjects = getFirstInterstPaymentSchedule(databaseService, _loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.REDUCING_100:
                scheduleObjects = getFlatResuding100PaymentSchedule(databaseService, _loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.FLAT_QUARTER:
                scheduleObjects = getFlatQuarterPaymentSchedule(databaseService, _loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            case InterestMethod.FLAT_INTERST_INSTALLMENT_FIRST:
                scheduleObjects = getFlatInterstInstallmentFirstPaymentSchedule(databaseService, _loanAmount, _annualInterestRate, _installmentCount, paymentTerm, loanDate);
                break;
            default:
                throw new LoanCalculationException("Unrecognized interest method");
        }

        return scheduleObjects;
    }

    private static List<LoanPaymentInformation> getFlatPaymentSchedule(
            HibernateDatabaseService databaseService,
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        List<LoanPaymentInformation> paymentSchedule = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(loanDate);
        double currentInterestRate = 0.0;

        //Leav Days
        List list = getLeaveDays(databaseService, loanDate, installmentCount, paymentTerm);
        //
        double capitalAmount;
        double interestAmount;
        double totalAmount;
        Date paymentDate;
        for (int i = 0; i < installmentCount; i++) {
            LoanPaymentInformation information = new LoanPaymentInformation();
            switch (paymentTerm) {
                case PaymentTerm.DAILY:
                    currentInterestRate = annualInterestRate / 300;
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    }
                    calendar = getAvailableDay(list, calendar);
                    break;
                case PaymentTerm.WEEKLY:
                    currentInterestRate = annualInterestRate / 48;
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 7);
                    }
                    calendar = getAvailableDayWeekly(list, calendar);
                    break;
                case PaymentTerm.MONTHLY:
                    currentInterestRate = annualInterestRate / 12;
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 1);
                    }
                    break;
                case PaymentTerm.QUARTER:
                    currentInterestRate = annualInterestRate / 2;
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 1);
                    }
                    break;
                default:
                    throw new AssertionError();
            }
            capitalAmount = loanAmount / installmentCount;
            interestAmount = loanAmount * currentInterestRate / 100;
            totalAmount = capitalAmount + interestAmount;
            paymentDate = calendar.getTime();

            information.setIndex(i);
            information.setInterestAmount(interestAmount);
            information.setCapitalAmount(capitalAmount);
            information.setPaymentAmount(totalAmount);
            information.setPaymentDate(paymentDate);

            paymentSchedule.add(information);
        }

        return paymentSchedule;
    }

    //kalawewa Calculation
    private static List<LoanPaymentInformation> getFlatQuarterPaymentSchedule(
            HibernateDatabaseService databaseService,
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        List<LoanPaymentInformation> paymentSchedule = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(loanDate);
        double currentInterestRate = 0.0;
        //Leav Days
        List list = getLeaveDays(databaseService, loanDate, installmentCount, paymentTerm);
        //
        double capitalAmount;
        double interestAmount;
        double totalAmount;
        Date paymentDate;
        for (int i = 0; i < installmentCount; i++) {
            LoanPaymentInformation information = new LoanPaymentInformation();
            switch (paymentTerm) {
                case PaymentTerm.DAILY:
//                    currentInterestRate = annualInterestRate / 240;
                    currentInterestRate = annualInterestRate / 300;
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    }
                    calendar = getAvailableDay(list, calendar);
                    break;
                case PaymentTerm.WEEKLY:
                    currentInterestRate = annualInterestRate / 48;
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 7);
                    }
                    break;
                case PaymentTerm.MONTHLY:
                    currentInterestRate = annualInterestRate / 12;
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 1);
                    }
                    break;
                case PaymentTerm.QUARTER:
//                   installmentCount= installmentCount/6;
                    currentInterestRate = annualInterestRate / 2;
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 6);
                    }
                    break;
                default:
                    throw new AssertionError();
            }
            capitalAmount = loanAmount / installmentCount;
            interestAmount = loanAmount * currentInterestRate / 100;
            totalAmount = capitalAmount + interestAmount;
            paymentDate = calendar.getTime();

            information.setIndex(i);
            information.setInterestAmount(interestAmount);
            information.setCapitalAmount(capitalAmount);
            information.setPaymentAmount(totalAmount);
            information.setPaymentDate(paymentDate);

            paymentSchedule.add(information);
        }

        return paymentSchedule;
    }
     private static Calendar getAvailableDayWeekly(List<CompanyLeaveDay> list, Calendar calendar) {
        for (CompanyLeaveDay companyLeaveDay : list) {
            if (companyLeaveDay.getLeaveDate().equals(calendar.getTime())) {
                calendar.add(Calendar.DAY_OF_YEAR, 7);
                return getAvailableDayWeekly(list, calendar);
            }
        }
        return calendar;
    }

    private static List<LoanPaymentInformation> getFlatInterstInstallmentFirstPaymentSchedule(
            HibernateDatabaseService databaseService,
            double loanAmount,
            double annualInterestRate,
            Integer installmentCount,
            String paymentTerm,
            Date loanDate) {

        List<LoanPaymentInformation> paymentSchedule = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(loanDate);
        calendar.add(Calendar.MONTH, -1);
        double currentInterestRate = 0.0;
        //Leav Days
        List list = getLeaveDays(databaseService, loanDate,installmentCount , paymentTerm);
        //
        double capitalAmount;
        double interestAmount;
        double totalAmount;
        Date paymentDate;
        double cap = 100;
        double payInterst = 0;
        double payCapital = 0;


        switch (paymentTerm) {
            case PaymentTerm.DAILY:
                currentInterestRate = annualInterestRate / 300;
                break;
            case PaymentTerm.WEEKLY:
                currentInterestRate = annualInterestRate / 48;
                break;
            case PaymentTerm.MONTHLY:
                currentInterestRate = annualInterestRate / 12;
                break;
            case PaymentTerm.QUARTER:
                currentInterestRate = annualInterestRate / 2;
                break;
            default:
                throw new AssertionError();
        }

        interestAmount = (loanAmount * annualInterestRate) / 100;
//        System.out.println("interestAmount___________"+interestAmount);
        Double yers=installmentCount/12.0;
//        System.out.println("installmentCount____"+installmentCount);
//        System.out.println("yearrrrrrrrrr____"+yers);
        double installmentAmount = (loanAmount + ( interestAmount * yers) ) / installmentCount;
       interestAmount= interestAmount * yers;
//        System.out.println("loanAmount_"+loanAmount+"---interestAmount_"+interestAmount+"----installmentCount-"+installmentCount);
//        System.out.println("installmentAmount-------------------"+installmentAmount);

        for (int i = 1; i < (installmentCount + 1); i++) {
            LoanPaymentInformation information = new LoanPaymentInformation();
            switch (paymentTerm) {
                case PaymentTerm.DAILY:
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    }
                    calendar = getAvailableDay(list, calendar);
                    break;
                case PaymentTerm.WEEKLY:
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 7);
                    }
                    break;
                case PaymentTerm.MONTHLY:
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 1);
                    }
                    break;
                case PaymentTerm.QUARTER:
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 6);
                    }
                    break;
                default:
                    throw new AssertionError();
            }

            paymentDate = calendar.getTime();

            int installmentNumber = i - 1;

            ///////////////////////////////////////////////////////////////////

            if (interestAmount != 0) {
                if ((interestAmount - installmentAmount) > 0) {
                    payCapital = 0;
                    payInterst = installmentAmount;
                    information.setIndex(installmentNumber);
                    information.setInterestAmount(payInterst);
                    information.setCapitalAmount(payCapital);
                    information.setPaymentAmount((payInterst + payCapital));
                    information.setPaymentDate(paymentDate);

                    interestAmount = interestAmount - installmentAmount;
                } else {
                    payInterst = interestAmount;
                    payCapital = installmentAmount - payInterst;
                    information.setIndex(installmentNumber);
                    information.setInterestAmount(payInterst);
                    information.setCapitalAmount(payCapital);
                    information.setPaymentAmount((payInterst + payCapital));
                    information.setPaymentDate(paymentDate);


                    loanAmount = loanAmount - payCapital;
                    interestAmount = 0;
                }
            } else {
                payInterst = 0;
                payCapital = installmentAmount;
                information.setIndex(installmentNumber);
                information.setInterestAmount(payInterst);
                information.setCapitalAmount(payCapital);
                information.setPaymentAmount((payInterst + payCapital));
                information.setPaymentDate(paymentDate);

                loanAmount = loanAmount - payCapital;
            }

            /////////////////////////////////////////////////////////////////// 


            paymentSchedule.add(information);
        }

        return paymentSchedule;
    }
    //new wee singhe puthram finance 100
    private static List<LoanPaymentInformation> getFlatResuding100PaymentSchedule(
            HibernateDatabaseService databaseService,
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        List<LoanPaymentInformation> paymentSchedule = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(loanDate);
        calendar.add(Calendar.MONTH, -1);
        double currentInterestRate = 0.0;
        //Leav Days
        List list = getLeaveDays(databaseService, loanDate, installmentCount, paymentTerm);
        //
        double capitalAmount;
        double interestAmount;
        double totalAmount;
        Date paymentDate;
        double cap = 100;
        double payInterst = 0;
        double payCapital = 0;


        switch (paymentTerm) {
            case PaymentTerm.DAILY:
                currentInterestRate = annualInterestRate / 300;
                break;
            case PaymentTerm.WEEKLY:
                currentInterestRate = annualInterestRate / 48;
                break;
            case PaymentTerm.MONTHLY:
                currentInterestRate = annualInterestRate / 12;
                break;
            case PaymentTerm.QUARTER:
                currentInterestRate = annualInterestRate / 2;
                break;
            default:
                throw new AssertionError();
        }

        interestAmount = ((loanAmount * currentInterestRate) / 100)*installmentCount;
//        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxx__interestAmount___"+interestAmount);
        
        double installmentAmount = (loanAmount + interestAmount) / installmentCount;


        for (int i = 1; i < (installmentCount + 1); i++) {
            LoanPaymentInformation information = new LoanPaymentInformation();
            switch (paymentTerm) {
                case PaymentTerm.DAILY:
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    }
                    calendar = getAvailableDay(list, calendar);
                    break;
                case PaymentTerm.WEEKLY:
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 7);
                    }
                    break;
                case PaymentTerm.MONTHLY:
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 1);
                    }
                    break;
                case PaymentTerm.QUARTER:
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 6);
                    }
                    break;
                default:
                    throw new AssertionError();
            }

            paymentDate = calendar.getTime();

            int installmentNumber = i - 1;
            //---------------------------------------------------------------------------------------

            if (interestAmount != 0) {
                if ((interestAmount - payInterst) > 0) {
                payInterst = installmentAmount - (cap * i);
                    interestAmount = interestAmount - payInterst;
                    payCapital = (cap * i);
                    
                     information.setIndex(installmentNumber);
                    information.setInterestAmount(payInterst);
                    information.setCapitalAmount(payCapital);
                    information.setPaymentAmount((payInterst + payCapital));
                    information.setPaymentDate(paymentDate);
                    
                    loanAmount = loanAmount - (cap * i);
//                    System.out.println("111");
                } else {
                    payInterst = interestAmount;
                    payCapital = installmentAmount - payInterst;

                    information.setIndex(installmentNumber);
                    information.setInterestAmount(payInterst);
                    information.setCapitalAmount(payCapital);
                    information.setPaymentAmount((payInterst + payCapital));
                    information.setPaymentDate(paymentDate);

                    loanAmount = loanAmount - payCapital;
                    interestAmount = 0;
//                    System.out.println("222");
                }
            } else {
                payInterst = 0;
                payCapital = installmentAmount;
                
                information.setIndex(installmentNumber);
                information.setInterestAmount(payInterst);
                information.setCapitalAmount(payCapital);
                information.setPaymentAmount((payInterst + payCapital));
                information.setPaymentDate(paymentDate);




                loanAmount = loanAmount - installmentAmount;
//                    System.out.println("333");
            }
//                    System.out.println(i+"interestAmount__"+payInterst+"___XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX_"+interestAmount);

            //---------------------------------------------------------------------------------------

            paymentSchedule.add(information);
        }

        return paymentSchedule;
    }

    private static List<LoanPaymentInformation> getFirstInterstPaymentSchedule(
            HibernateDatabaseService databaseService,
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        List<LoanPaymentInformation> paymentSchedule = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(loanDate);
        double currentInterestRate = 0.0;

        //Leav Days
        List list = getLeaveDays(databaseService, loanDate, installmentCount, paymentTerm);
        //
        double capitalAmount;
        double interestAmount;
        double totalAmount;
        Date paymentDate;
        int countlast = 0;
        for (int i = 0; i < installmentCount; i++) {

            LoanPaymentInformation information = new LoanPaymentInformation();
            switch (paymentTerm) {
                case PaymentTerm.DAILY:
                    currentInterestRate = annualInterestRate / 300;
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    }
                    calendar = getAvailableDay(list, calendar);
                    break;
                case PaymentTerm.WEEKLY:
                    currentInterestRate = annualInterestRate / 48;
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 7);
                    }
                    break;
                case PaymentTerm.MONTHLY:
                    currentInterestRate = annualInterestRate / 12;
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 1);
                    }
                    break;
                default:
                    throw new AssertionError();
            }



            capitalAmount = loanAmount / installmentCount;
            interestAmount = loanAmount * currentInterestRate / 100;
            totalAmount = interestAmount;
            paymentDate = calendar.getTime();



            information.setIndex(i);
            information.setInterestAmount(interestAmount);
            information.setCapitalAmount(0.00);
            information.setPaymentAmount(totalAmount);
            information.setPaymentDate(paymentDate);

            countlast++;
            if (countlast == (installmentCount)) {
                information.setIndex(i);
                information.setInterestAmount(interestAmount);
                information.setCapitalAmount(loanAmount);
                information.setPaymentAmount(loanAmount + interestAmount);
                information.setPaymentDate(paymentDate);
            }

            paymentSchedule.add(information);
        }

        return paymentSchedule;
    }

    private static List<LoanPaymentInformation> getRedusingPaymentSchedule(
            HibernateDatabaseService databaseService,
            double loanAmount,
            double annualInterestRate,
            int installmentCount,
            String paymentTerm,
            Date loanDate) {

        List<LoanPaymentInformation> paymentSchedule = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(loanDate);
        double currentInterestRate;

        //Leav Days
        List list = getLeaveDays(databaseService, loanDate, installmentCount, paymentTerm);
        //
        double capitalAmount;
        double interestAmount;
        double totalAmount;
        Date paymentDate;

        LoanInformation loanInformation = getRedusingLoanInformation(loanAmount, annualInterestRate, installmentCount, paymentTerm, loanDate);

        double loanRunningBalance = loanAmount;
        totalAmount = loanInformation.getInstallmentAmount();
        for (int i = 0; i < installmentCount; i++) {
            switch (paymentTerm) {
                case PaymentTerm.DAILY:
                    currentInterestRate = annualInterestRate / 300 / 100;
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    }
                    calendar = getAvailableDay(list, calendar);
                    break;
                case PaymentTerm.WEEKLY:
                    currentInterestRate = annualInterestRate / 48 / 100;
                    if (i != 0) {
                        calendar.add(Calendar.DAY_OF_YEAR, 7);
                    }
                    break;
                case PaymentTerm.MONTHLY:
                    currentInterestRate = annualInterestRate / 12 / 100;
                    if (i != 0) {
                        calendar.add(Calendar.MONTH, 1);
                    }
                    break;
                default:
                    throw new AssertionError();
            }

            LoanPaymentInformation information = new LoanPaymentInformation();
           
//            interestAmount = currentInterestRate * loanRunningBalance;
//            capitalAmount = totalAmount - interestAmount;
//            loanRunningBalance = loanRunningBalance - capitalAmount;
//            paymentDate = calendar.getTime();

            interestAmount = currentInterestRate * loanRunningBalance;
            capitalAmount = totalAmount - interestAmount;
            loanRunningBalance = loanRunningBalance - capitalAmount;
            paymentDate = calendar.getTime();
            


            information.setIndex(i);
            information.setInterestAmount(interestAmount);
            information.setCapitalAmount(capitalAmount);
            information.setPaymentAmount(totalAmount);
            information.setPaymentDate(paymentDate);

            paymentSchedule.add(information);
        }

        return paymentSchedule;
    }

    private static List getLeaveDays(HibernateDatabaseService databaseService, Date loanDate, int installmentCount, String paymentTerm) {
        List list;
        Map map = new HashMap();
        Date endDate = getEndDate(loanDate, installmentCount, paymentTerm);

        map.put("LOAN_DATE", loanDate);
        map.put("END_DATE", endDate);
        try {
            list = databaseService.getCollection("from com.mac.registration.company_leave_day.object.CompanyLeaveDay where leave_date between :LOAN_DATE and :END_DATE", map);
        } catch (Exception e) {
            list = new ArrayList<>();
            Logger.getLogger(PCLoan.class.getName()).log(Level.SEVERE, null, e);
        }

        return list;
    }

    private static Date getEndDate(Date loanDate, int installmentCount, String paymentTerm) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(loanDate);
        switch (paymentTerm) {
            case PaymentTerm.DAILY:
                calendar.add(Calendar.DAY_OF_YEAR, installmentCount + 31);
                break;
            case PaymentTerm.WEEKLY:
                calendar.add(Calendar.DAY_OF_YEAR, installmentCount);
                break;
            case PaymentTerm.MONTHLY:
                calendar.add(Calendar.MONTH, installmentCount);
                break;
            case PaymentTerm.QUARTER:
                calendar.add(Calendar.MONTH, installmentCount);
                break;
            default:
                throw new AssertionError();
        }
        return calendar.getTime();
    }

    private static Calendar getAvailableDay(List<CompanyLeaveDay> list, Calendar calendar) {
        for (CompanyLeaveDay companyLeaveDay : list) {
            if (companyLeaveDay.getLeaveDate().equals(calendar.getTime())) {
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                return getAvailableDay(list, calendar);
            }
        }
        return calendar;
    }

    //PANALTY CALCULATION
    public static Double getPanaltyAmount(
            HibernateDatabaseService databaseService,
            Double balanceAmount,
            Date dueDate,
            Date panaltyOnDate,
            String panaltyType,
            Double panaltyRate,
            Double panaltyAmount,
            Integer gracePeriod) {

        Double panalty;

        long diff = panaltyOnDate.getTime() - dueDate.getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(diff);
        int passedDays = calendar.get(Calendar.DAY_OF_YEAR);


        if (passedDays > gracePeriod) {
            if (!isLeaveDay(panaltyOnDate, databaseService)) {
                switch (panaltyType) {
                    case PanaltyType.RATE:
                        panalty = balanceAmount * panaltyRate / 100;
                        break;
                    case PanaltyType.AMOUNT:
                        panalty = panaltyAmount;
                        break;
                    case PanaltyType.NONE:
                        panalty = 0.0;
                        break;
                    default:
                        panalty = 0.0;
                }
            } else {
                panalty = 0.0;
            }
        } else {
            panalty = 0.0;
        }

        return panalty;
    }

    private static void initLeaveDays(HibernateDatabaseService databaseService) {
        if (leaveDays == null) {
            try {
                resetLeaveDays(databaseService);
            } catch (DatabaseException ex) {
                Logger.getLogger(LoanCalculationUtil.class.getName()).log(Level.SEVERE, null, ex);
                leaveDays = null;
            }
        }
    }

    private static void resetLeaveDays(HibernateDatabaseService databaseService) throws DatabaseException {
        List<CompanyLeaveDay> companyLeaveDays = databaseService.getCollection(CompanyLeaveDay.class);
        leaveDays = new ArrayList<>();
        for (CompanyLeaveDay companyLeaveDay : companyLeaveDays) {
            leaveDays.add(companyLeaveDay.getLeaveDate());
        }
    }

    private static boolean isLeaveDay(Date date, HibernateDatabaseService databaseService) {
        initLeaveDays(databaseService);

        for (Date date1 : leaveDays) {
            if (date1.equals(date)) {
                return true;
            }
        }
        return false;
    }
    private static List<Date> leaveDays;

    public static void main(String[] args) {
//        LoanInformation information = getRedusingLoanInformation(100000, 15, 60, PaymentTerm.WEEKLY, new Date());
//        System.out.println(information.getInstallmentAmount());
//        System.out.println(information.getEffectiveInterestRate());

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        List<LoanPaymentInformation> informations = getRedusingPaymentSchedule(null, 100000, 15, 60, PaymentTerm.MONTHLY, new Date());
        for (LoanPaymentInformation loanPaymentInformation : informations) {
            System.out.println(
                    loanPaymentInformation.getIndex()
                    + ",\t" + decimalFormat.format(loanPaymentInformation.getPaymentAmount())
                    + ",\t" + decimalFormat.format(loanPaymentInformation.getCapitalAmount())
                    + ",\t" + decimalFormat.format(loanPaymentInformation.getInterestAmount()));
        }
    }
}
