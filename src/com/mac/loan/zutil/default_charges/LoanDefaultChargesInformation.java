/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.zutil.default_charges;

import com.mac.loan.zobject.ChargeScheme;

/**
 *
 * @author Udayanga
 */
public class LoanDefaultChargesInformation {

//    private LoanDefaultCharge loanDefaultCharge;
    private double chargeAmount;
    private double fundAmount;
    private ChargeScheme chargeScheme;

    //    public LoanDefaultCharge getLoanDefaultCharge() {
    //        return loanDefaultCharge;
    //    }
    //
    //    public void setLoanDefaultCharge(LoanDefaultCharge loanDefaultCharge) {
    //        this.loanDefaultCharge = loanDefaultCharge;
    //    }
    public ChargeScheme getChargeScheme() {
        return chargeScheme;
    }

    public void setChargeScheme(ChargeScheme chargeScheme) {
        this.chargeScheme = chargeScheme;
    }

    public double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public double getFundAmount() {
        return fundAmount;
    }

    public void setFundAmount(double fundAmount) {
        this.fundAmount = fundAmount;
    }
    
}
