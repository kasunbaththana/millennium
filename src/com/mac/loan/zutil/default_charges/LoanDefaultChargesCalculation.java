/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.zutil.default_charges;

import com.mac.loan.PaymentTerm;
import com.mac.loan.zobject.ChargeScheme;
import com.mac.loan.zobject.ChargeSchemeCustom;
import com.mac.loan.zobject.LoanFundDocumentChg;
import com.mac.loan.zobject.LoanType;
import com.mac.registration.charge_scheme.ChargeSchemeType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Udayanga
 */
public class LoanDefaultChargesCalculation {

//    public static List<LoanDefaultChargesInformation> getDefaultCharges(HibernateDatabaseService databaseService) throws DatabaseException {
//        Double betweenFrom;
//        Double betweenTo;
//        String name;
//        List<LoanDefaultCharge> loanDefaultCharge;
//        LoanDefaultChargesInformation loanDefaultChargesInformation = null;
//        loanDefaultCharge = databaseService.getCollection(LoanDefaultCharge.class);
//        List<LoanDefaultChargesInformation> DefaultCharges = new ArrayList<>();
//        Iterator iterator = loanDefaultCharge.iterator();
//        while (iterator.hasNext()) {
//            loanDefaultChargesInformation = new LoanDefaultChargesInformation();
//            LoanDefaultCharge object = (LoanDefaultCharge) iterator.next();
//            betweenFrom = object.getBetweenFrom();
//            betweenTo = object.getBetweenTo();
//            //loanDefaultChargesInformation.setPresentageCharge(betweenTo - betweenFrom / 100);
//            loanDefaultChargesInformation.setAmmount(betweenTo - betweenFrom / 100);
//            loanDefaultChargesInformation.setName(object.getName());
//        }
//        DefaultCharges.add(loanDefaultChargesInformation);
//        return DefaultCharges;
//    }
    /*
     public static List<LoanDefaultChargesInformation> getDefaultCharges(List<LoanDefaultCharge> defaultCharges, double loanAmount, String loanType) {
     double betweenFrom;
     double betweenTo;

     double chargeAmount;

     List<LoanDefaultChargesInformation> informations = new ArrayList<>();

     for (LoanDefaultCharge loanDefaultCharge : defaultCharges) {
     if (loanType.equals(loanDefaultCharge.getLoanType().getCode())) {


     betweenFrom = loanDefaultCharge.getBetweenFrom();
     betweenTo = loanDefaultCharge.getBetweenTo();

     if (betweenFrom <= loanAmount
     && betweenTo >= loanAmount) {

     switch (loanDefaultCharge.getAmountType()) {
     case LoanDefaultChargeType.RATE:
     chargeAmount = loanAmount * loanDefaultCharge.getAmountRate() / 100;
     break;
     case LoanDefaultChargeType.AMOUNT:
     chargeAmount = loanDefaultCharge.getAmountValue();
     break;
     default:
     chargeAmount = 0.0;
     throw new AssertionError();
     }

     //CREATE LOAN DEFAULT CHARGE INFO
     LoanDefaultChargesInformation chargesInformation = new LoanDefaultChargesInformation();
     chargesInformation.setChargeAmount(chargeAmount);
     chargesInformation.setLoanDefaultCharge(loanDefaultCharge);
     informations.add(chargesInformation);
     }
     }

     }

     return informations;
     }*/
    public static List<LoanDefaultChargesInformation> getDefaultCharges(Collection<ChargeScheme> defaultCharges, double loanAmount, LoanType loanType,double InstallmentCount) {
      
        double betweenFrom;
        double betweenTo;

        double chargeAmount = 0.0;
        double fundAmount = 0.0;
        
       

        List<LoanDefaultChargesInformation> informations = new ArrayList<>();

        for (ChargeScheme chargeScheme : defaultCharges) {
            switch (chargeScheme.getType()) {
                case ChargeSchemeType.VALUE:
                    chargeAmount = chargeScheme.getAmount();
                    break;
                case ChargeSchemeType.PERCENT:
                    
                    double amtt=0.0;
                     amtt = (chargeScheme.getAmount());
                switch (loanType.getPaymentTerm()) {
                    case PaymentTerm.WEEKLY:
                        if((int)InstallmentCount > 48){
                             amtt =(chargeScheme.getAmount()*1.5);
                         }
                        break;
                    case PaymentTerm.MONTHLY:
                        if((int)InstallmentCount > 12){
                             amtt = (chargeScheme.getAmount()*1.5);
                         }
                        break;
                }

                    chargeAmount = loanAmount * (amtt / 100); 
                    
//            switch (loanType.getPaymentTerm()) {
//                case PaymentTerm.WEEKLY:
//                    if((int)InstallmentCount > 46){
//                         chargeScheme.setAmount(chargeScheme.getAmount()+1.5);
//                     }
//                    break;
//                case PaymentTerm.MONTHLY:
//                    if((int)InstallmentCount > 12){
//                         chargeScheme.setAmount(chargeScheme.getAmount()+1.5);
//                     }
//                    break;
//            }
//                        
//                chargeAmount = loanAmount * (chargeScheme.getAmount() / 100);  

                    
                    break;
                case ChargeSchemeType.CUSTOM:

                    
                   for (ChargeSchemeCustom chargeSchemeCustom : chargeScheme.getChargeSchemeCustoms()) {
                        betweenFrom = chargeSchemeCustom.getFromAmount();
                        betweenTo = chargeSchemeCustom.getToAmount();

                        if (betweenFrom <= loanAmount
                                && betweenTo >= loanAmount) {
                            chargeAmount = chargeSchemeCustom.getAmount();
                            
                             switch (chargeSchemeCustom.getType()) {
                                        case ChargeSchemeType.CUSTOM_AMOUNT:
                                            chargeAmount = chargeSchemeCustom.getAmount();
                                            break;
                                        case ChargeSchemeType.CUSTOM_RATE:
                                            double amt=0.0;
                                             amt = (chargeSchemeCustom.getAmount());
                                        switch (loanType.getPaymentTerm()) {
                                            case PaymentTerm.WEEKLY:
                                                if((int)InstallmentCount > 48){
                                                     amt =(chargeSchemeCustom.getAmount()*1.5);
                                                 }
                                                break;
                                            case PaymentTerm.MONTHLY:
                                                if((int)InstallmentCount > 12){
                                                     amt = (chargeSchemeCustom.getAmount()*1.5);
                                                 }
                                                break;
                                        }
                      
                                            chargeAmount = loanAmount * (amt / 100);  
                                            
                                            
                                            
                                            break;
                                        default:
                                            throw new AssertionError();
                                    }
                             
                        }
                        else if (chargeSchemeCustom.getRent()>0) {
                         
                           if((int)InstallmentCount == chargeSchemeCustom.getRent()){
                             switch (chargeSchemeCustom.getType()) {
                                        case ChargeSchemeType.CUSTOM_AMOUNT:
                                            chargeAmount = chargeSchemeCustom.getAmount();
                                            break;
                                        case ChargeSchemeType.CUSTOM_RATE:
                                            System.out.println((int)InstallmentCount+" - "+chargeSchemeCustom.getRent());
                                            
                                           
                                             chargeAmount = loanAmount * (chargeSchemeCustom.getAmount() / 100);   
                                          
                                            break;
                                        default:
                                            throw new AssertionError();
                                    }
                   }
                             
                        }
                    }


                    break;
            }

            LoanDefaultChargesInformation defaultChargesInformation = new LoanDefaultChargesInformation();
            defaultChargesInformation.setChargeAmount(chargeAmount);
            defaultChargesInformation.setChargeScheme(chargeScheme);
            informations.add(defaultChargesInformation);
            
            
            
            
            fundAmount = 0.0;
        }


        /*for (LoanDefaultCharge loanDefaultCharge : defaultCharges) {
         if (loanType.equals(loanDefaultCharge.getLoanType().getCode())) {


         betweenFrom = loanDefaultCharge.getBetweenFrom();
         betweenTo = loanDefaultCharge.getBetweenTo();

         if (betweenFrom <= loanAmount
         && betweenTo >= loanAmount) {

         switch (loanDefaultCharge.getAmountType()) {
         case LoanDefaultChargeType.RATE:
         chargeAmount = loanAmount * loanDefaultCharge.getAmountRate() / 100;
         break;
         case LoanDefaultChargeType.AMOUNT:
         chargeAmount = loanDefaultCharge.getAmountValue();
         break;
         default:
         chargeAmount = 0.0;
         throw new AssertionError();
         }

         //CREATE LOAN DEFAULT CHARGE INFO
         LoanDefaultChargesInformation chargesInformation = new LoanDefaultChargesInformation();
         chargesInformation.setChargeAmount(chargeAmount);
         chargesInformation.setLoanDefaultCharge(loanDefaultCharge);
         informations.add(chargesInformation);
         }
         }

         }*/

        return informations;
    }
}
