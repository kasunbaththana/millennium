package com.mac.loan.do_genarate;

import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.do_genarate.object.Loan;
import com.mac.loan.do_genarate.object.LoanType;
//import com.mac.zutil.remind_letter.object.RemindLetterSetup;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kasun
 */
public class SERDoGenerator extends AbstractService {

    public SERDoGenerator(Component component) {
        super(component);
    }

    public List<Loan> getLoan(LoanType LoanType)
    {
        List<Loan> Loan = new ArrayList<>();
        try {
          Loan= getDatabaseService().getCollection("from com.mac.loan.do_genarate.object.Loan WHERE status='LOAN_START' and loanType='"+LoanType.getCode()+"' and printdo<>'1' ");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Loan;
    }
     public List<LoanType> getLoanType()
    {
        List<LoanType> Loan = new ArrayList<>();
        try {
          Loan= getDatabaseService().getCollection("from com.mac.loan.do_genarate.object.LoanType WHERE isHp='1'");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Loan;
    }

   
}
