/*
 *  LetterGenerator.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 1, 2014, 2:07:07 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.do_genarate;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.loan.do_genarate.object.Loan;
import com.mac.loan.do_genarate.object.LoanType;
import com.mac.zreport.ReportBuilder;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Kasun
 */
public class PCDoGenerator extends CPanel {

    /**
     * Creates new form LetterGenerator
     */
    public PCDoGenerator() {
        initComponents();

        initOthers();
       
    }

  

    

    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();

        return connection;
    }

    @Action
    public void doClose() {
        tblReminds.setCValue(null);
        TabFunctions.closeTab(this);
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        //SERVICE
        SERDoGenerator = new SERDoGenerator(this);

         //ACTIONS
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnGenerate, "doGenerate");
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnFind, "doFind");
        actionUtil.setAction(btnVeiw, "doView");
   
        //
        btnGenerate.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_GENERATE, 16, 16));
     
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        btnFind.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON));
        btnVeiw.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_PRINT_ICON));
     
        tblReminds.setCModel(
                new CTableModel(
            new CTableColumn[]{
            new CTableColumn("Selected", new String[]{"printdo"}, true),
            new CTableColumn("Agreement No",  "agreementNo"),
            new CTableColumn("Supplier", "supplier"),
            new CTableColumn("Amount", "loanAmount")
        }));
        
        cboLoanType.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                addTableData();
                txtTableFind.setCValue("");
            }
        });

    }
  
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        pnlLeft = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReminds = new com.mac.af.component.derived.input.table.CITable();
        jPanel1 = new javax.swing.JPanel();
        btnGenerate = new com.mac.af.component.derived.command.button.CCButton();
        txtTableFind = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnFind = new com.mac.af.component.derived.command.button.CCButton();
        btnVeiw = new com.mac.af.component.derived.command.button.CCButton();
        cboLoanType = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return SERDoGenerator.getLoanType();
            }
        };
        pnlRight = new javax.swing.JPanel();

        jSplitPane1.setDividerLocation(300);

        tblReminds.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblReminds);

        btnGenerate.setText("Generate");
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        btnClose.setText("Close");

        btnFind.setText("Find");
        btnFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindActionPerformed(evt);
            }
        });

        btnVeiw.setText("View");
        btnVeiw.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVeiwActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(txtTableFind, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnVeiw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTableFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVeiw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        cboLoanType.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cboLoanTypeMouseClicked(evt);
            }
        });
        cboLoanType.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                cboLoanTypePropertyChange(evt);
            }
        });

        javax.swing.GroupLayout pnlLeftLayout = new javax.swing.GroupLayout(pnlLeft);
        pnlLeft.setLayout(pnlLeftLayout);
        pnlLeftLayout.setHorizontalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboLoanType, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlLeftLayout.setVerticalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(cboLoanType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16))
        );

        jSplitPane1.setLeftComponent(pnlLeft);

        pnlRight.setLayout(new javax.swing.BoxLayout(pnlRight, javax.swing.BoxLayout.LINE_AXIS));
        jSplitPane1.setRightComponent(pnlRight);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnFindActionPerformed

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGenerateActionPerformed

    private void cboLoanTypeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cboLoanTypeMouseClicked
    }//GEN-LAST:event_cboLoanTypeMouseClicked

    private void cboLoanTypePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_cboLoanTypePropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_cboLoanTypePropertyChange

    private void btnVeiwActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVeiwActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVeiwActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnFind;
    private com.mac.af.component.derived.command.button.CCButton btnGenerate;
    private com.mac.af.component.derived.command.button.CCButton btnVeiw;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanType;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlRight;
    private com.mac.af.component.derived.input.table.CITable tblReminds;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTableFind;
    // End of variables declaration//GEN-END:variables
    private SERDoGenerator SERDoGenerator;

  
    @Action
    public void doFind() throws DatabaseException {
         
           tblReminds.find(txtTableFind.getCValue());
           
    }

    
    
    public void addTableData()
    {
        
        tblReminds.resetValue();
        LoanType LoanType = (LoanType) cboLoanType.getCValue();
        
        if(LoanType!=null)
        {
        List<Loan> listLoan= new ArrayList<>();
                
            listLoan=   SERDoGenerator.getLoan(LoanType);
          
        
//        if(listLoan!=null){
//            
//         tblReminds.setCValue(listLoan);
//        }else
//        {
//            
//         tblReminds.setCValue(new ArrayList<>());
//        }
//        
//        }
//        else {
            tblReminds.setCValue(listLoan);
        }
    }
    
      @Action(asynchronous = true)
    public void doGenerate() {
       

        String path1 = "";
        String letterStatus = "";

            path1 = "do_prinout.jrxml";

        try {

            JasperDesign jasperDesign1 = JRXmlLoader.load(path1);
            JasperReport jasperReport1 = JasperCompileManager.compileReport(jasperDesign1);

            HashMap<String, Object> params = new HashMap<>();
            ReportBuilder.initDefaultParameters();
            params.putAll(ReportBuilder.DEFAULT_PARAMETERS);
          

            // print Report
            
             Loan listLoan;
             Loan savloan;
             for(Object Object :tblReminds.getCValue())
             {
                 listLoan = (Loan) Object;
                 savloan = listLoan;
                 if(listLoan.isPrintdo())
                 {
                    params.put("TRANSACTION_NO", listLoan.getIndexNo());  
                   
                     try {
                        
                         savloan.setPrintdo(true);
                         getDatabaseService().save(savloan);
                         getDatabaseService().commitLocalTransaction();
                        
                        
                     } catch (Exception ex) {
                         Logger.getLogger(PCDoGenerator.class.getName()).log(Level.SEVERE, null, ex);
                     }
                    
                 }
                 
             }
             
             
           
            final JasperPrint jasperPrint1 = JasperFillManager.fillReport(jasperReport1, params, getConnection());

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    JRViewer viewer1 = new JRViewer(jasperPrint1);

                    pnlRight.removeAll();

                    pnlRight.add(viewer1);

                }
            };

            CApplication.invokeEventDispatch(runnable);


        } catch (JRException | SQLException e) {
            mOptionPane.showMessageDialog(null, "Unable to generate Delevery Order.", "Error", mOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        finally
        {
            addTableData();
        }
    }
   
      @Action(asynchronous = true)
    public void doView() {
       

        String path1 = "";
        String letterStatus = "";

            path1 = "do_prinout.jrxml";

        try {

            JasperDesign jasperDesign1 = JRXmlLoader.load(path1);
            JasperReport jasperReport1 = JasperCompileManager.compileReport(jasperDesign1);

            HashMap<String, Object> params = new HashMap<>();
            ReportBuilder.initDefaultParameters();
            params.putAll(ReportBuilder.DEFAULT_PARAMETERS);
          

            // print Report
            
             Loan listLoan;
             for(Object Object :tblReminds.getCValue())
             {
                 listLoan = (Loan) Object;
                 
                 if(listLoan.isPrintdo())
                 {
                    params.put("TRANSACTION_NO", listLoan.getIndexNo());  
                 }
                 
             }
             
             
           
            final JasperPrint jasperPrint1 = JasperFillManager.fillReport(jasperReport1, params, getConnection());

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    JRViewer viewer1 = new JRViewer(jasperPrint1);

                    pnlRight.removeAll();

                    pnlRight.add(viewer1);

                }
            };

            CApplication.invokeEventDispatch(runnable);


        } catch (JRException | SQLException e) {
            mOptionPane.showMessageDialog(null, "Unable to generate remind letters.", "Error", mOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
    
    
}
