/*
 *  InterestMethod.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 26, 2014, 9:33:17 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan;

/**
 *
 * @author mohan
 */
public class InterestMethod {

    public static final String FLAT = "FLAT";
    public static final String REDUCING = "REDUCING";
    public static final String FLAT_INTERST_INSTALLMENT_FIRST = "FLAT_INTERST_INSTALLMENT_FIRST";
    //First Interst and finala capital payment comes This Type
    public static final String FLAT_INTERST_FIRST = "FLAT_INTERST_FIRST";
    public static final String REDUCING_100 = "REDUCING_100";
    public static final String FLAT_QUARTER = "FLAT_QUARTER";
    //
    public static final String[] ALL = {
        FLAT,
        REDUCING,
        FLAT_INTERST_INSTALLMENT_FIRST,
        FLAT_INTERST_FIRST,
        REDUCING_100,
        FLAT_QUARTER
    };
}