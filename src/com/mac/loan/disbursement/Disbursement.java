/*
 *  Voucher.java
 * 
 *  @author kasun
 *     
 * 
 *  Created on Oct 12, 2014, 11:37:52 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.disbursement;

import com.mac.loan.voucher.*;
import com.mac.af.core.database.DatabaseException;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.ztemplate.disbursement.AbstractReceipt;
import com.mac.zsystem.transaction.account.account_setting.system_interface.DisbursementAccountInterface;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kasun
 */
public class Disbursement extends AbstractReceipt {

    @Override
    public List<Loan> getLoans() {
        String hql = "from com.mac.loan.zobject.Loan where availableDisbursement=true and status='LOAN_DISBURSEMENT' AND loanType.ishp='0'";
        List<Loan> loans;

       // HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(hql, Loan.class);
        try {
            loans = getDatabaseService().getCollection(hql, null);

        } catch (DatabaseException ex) {
            Logger.getLogger(Voucher.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }

    @Override
    protected String getReferenceGeneratorType() {
        return ReferenceGenerator.DISBURSEMENT;
    }

    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.DISBURSEMENT_TRANSACTION_CODE;
    }

    @Override
    protected String getSettlementCreitOrDebit() {
        return SettlementCreditDebit.DEBIT;
    }

    @Override
    protected String getChequeType() {
        return ChequeType.COMPANY;
    }

    @Override
    protected String getAccountSettingCode() {
        return DisbursementAccountInterface.DISBURSEMENT_AMOUNT_DEBIT_CODE;
    }

    @Override
    protected String getTransactionName() {
        return "Disbursement";
    }

    @Override
    protected Loan getModifiedLoan(Loan loan, Double afterBalance) {
        if (afterBalance.doubleValue() <= 0.0) {
            loan.setAvailableDisbursement(false);
             loan.setStatus(LoanStatus.LOAN_START);
        } else {
            loan.setAvailableDisbursement(true);
           
            
        }

        return loan;
    }

    @Override
    protected String getOverAmountSettlementType() {
        return SystemSettlement.OVER_PAY_VOUCHER;
    }

    @Override
    protected boolean isOverPayAvailable() {
        return false;
    }
}
