/*
 *  LoanCharges.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Oct 31, 2014, 1:56:36 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.loan_charges;

import com.mac.loan.ztemplate.charge.Charge;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;

/**
 *
 * @author mohan
 */
public class LoanCharges extends Charge {

    @Override
    protected String referenceGeneratorPrefix() {
        return ReferenceGenerator.OTHER_CHARGE;
    }

    @Override
    public String getTransactionName() {
        return "Loan Charges";
    }

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.OTHER_CHARGE_TRANSACTION_CODE;
    }

    @Override
    protected String getAccountTransactionTypeCode() {
        return AccountTransactionType.AUTO;
    }

    @Override
    protected boolean isAccountCreditDebitEqualNeeded() {
        return false;
    }
}
