/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_charges;

import com.mac.af.core.database.DatabaseException;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction_cancel.TransactionCancel;
import com.mac.zsystem.transaction.transaction_cancel.object.Transaction;

/**
 *
 * @author NIMESH-PC
 */
public class LoanChargesCancel extends TransactionCancel {

    @Override
    protected String getTransactionType() {
        return SystemTransactions.OTHER_CHARGE_TRANSACTION_CODE;
    }

    @Override
    protected void cancelAdditional(Transaction transaction) throws DatabaseException {
        SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.TRANSACTION_CANCEL_TRANSACTION_CODE,
                transaction.getReferenceNo(),
                transaction.getDocumentNo(),
                transaction.getLoan().getIndexNo(),
                null,
                transaction.getLoan().getClient(),
                "Other Chargers Cancel");

        transaction.getLoan().setAvailableReceipt(true);
        getDatabaseService().save(transaction.getLoan());
    }
}