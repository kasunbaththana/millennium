/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_customer_payment2;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.loan_customer_payment2.zobject.CusPayment;
import com.mac.loan.loan_customer_payment2.zobject.CusPaymentHistory;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author NIMESH-PC
 */
public class PCCustomerPayment extends DefaultObjectCreator<CusPayment> {

    /**
     * Creates new form PCCustomerPayment
     */
    public PCCustomerPayment() {
        initComponents();
        initOthers();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.CUSTOMER_PAYMENT));


        tblChargers.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Chargs", "cusChargs"),
            new CTableColumn("Amount", "amount")
        }));
        tblChargers.setObjectCreator(new PCChargers());
        tblChargers.setDescrption("Charge Details");

        tblChargers.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                double totalAmount = 0.0;
                Collection cValue = tblChargers.getCValue();
                for (Object object : cValue) {
                    CusPaymentHistory chargs = (CusPaymentHistory) object;
                    totalAmount = totalAmount + chargs.getAmount();
                }
                txtTotalAmount.setCValue(totalAmount);

            }
        });

    }

    @Override
    public void setNewMood() {
        super.setNewMood();
        tblChargers.resetValue();
        txtTotalAmount.resetValue();
    }

   

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReceiptNo = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAgrementNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtVehivleNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        lblTotalCreditAmount = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalAmount = new com.mac.af.component.derived.display.textfield.CDDoubleField();
        tblChargers = new com.mac.af.component.derived.input.table.CIAddingTable();

        cDLabel1.setText("Reference No :");

        cDLabel3.setText("Transaction Date :");

        cDLabel2.setText("Receipt No :");

        cDLabel4.setText("Document No :");

        cDLabel5.setText("Agrement No :");

        cDLabel6.setText("Note :");

        lblTotalCreditAmount.setText("Total Amount :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtReceiptNo, javax.swing.GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTotalCreditAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtTotalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(tblChargers, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(28, 28, 28)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtAgrementNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                                .addComponent(txtVehivleNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtDocumentNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReceiptNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAgrementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtVehivleNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tblChargers, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotalCreditAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel lblTotalCreditAmount;
    private com.mac.af.component.derived.input.table.CIAddingTable tblChargers;
    private com.mac.af.component.derived.input.textfield.CIStringField txtAgrementNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtReceiptNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.display.textfield.CDDoubleField txtTotalAmount;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    private com.mac.af.component.derived.input.textfield.CIStringField txtVehivleNo;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList(txtReferenceNo);
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList(
                txtReceiptNo,
                txtVehivleNo,
                txtTotalAmount,
                txtTransactionDate);
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(
                txtAgrementNo,
                txtDocumentNo);
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtTransactionDate, "transactionDate"),
                new CInputComponentBinder(txtDocumentNo, "documentNo"),
                new CInputComponentBinder(txtAgrementNo, "agrementNo"),
                new CInputComponentBinder(txtReferenceNo, "referanceNo"),
                new CInputComponentBinder(txtVehivleNo, "vehicleNo"));
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
                txtReceiptNo,
                txtReferenceNo,
                txtTransactionDate);
    }

    @Override
    protected Class<? extends CusPayment> getObjectClass() {
        return CusPayment.class;
    }

    
    
    @Override
    protected void afterInitObject(CusPayment object) throws ObjectCreatorException {
        object.setTotalAmount(txtTotalAmount.getCValue());
        object.setStatus("ACTIVE");

        for (Object custom : tblChargers.getCValue()) {
            ((CusPaymentHistory) custom).setCusPayment(object);
        }

        object.setCusPaymentHistories(new HashSet<CusPaymentHistory>(tblChargers.getCValue()));
    }

   
}
