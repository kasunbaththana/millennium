/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.hp_loan_application.object_creator;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.PanaltyType;
import com.mac.loan.PaymentTerm;
import com.mac.loan.hp_loan_application.SERLoanApplication;
import com.mac.loan.zobject.ChargeScheme;
import com.mac.loan.zobject.Client;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanType;
import com.mac.loan.zobject.SalesInvoice;
import com.mac.loan.zutil.calculation.LoanCalculationUtil;
import com.mac.loan.zutil.calculation.LoanInformation;
import com.mac.loan.zutil.calculation.LoanPaymentInformation;
import com.mac.loan.zutil.default_charges.LoanDefaultChargesCalculation;
import com.mac.loan.zutil.default_charges.LoanDefaultChargesInformation;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author soft-master
 */
public class PCLoan extends DefaultObjectCreator<Loan> {

    /**
     * Creates new form PCLoan
     */
    public PCLoan() {
        initComponents();
        initOthers();
    }

    @Action
    public void doRefreshPaymentSchedule() {
        try {
            calculatePaymentSchedule();
        } catch (Exception ex) {
            Logger.getLogger(PCLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doRefreshDefaultCharges() {
        try {
            //calculatePaymentSchedule();
            calculateDefaultCharges();
        } catch (Exception ex) {
            Logger.getLogger(PCLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private List getSalesInvoices() {
        return serLoanApplication.getSalesInvoices();
    }

    private void loanTypeChanged() {
        LoanType loanType = (LoanType) cboLoanType.getCValue();
        SalesInvoice salesInvoice = (SalesInvoice) cboSalesInvoice.getCValue();

        if (loanType != null && salesInvoice != null) {
            Client client = salesInvoice.getClient();

            if (loanType.getMinimumAllocation() > client.getMaximumAllocation() && client.getConsiderMaximumAllocation()) {
                mOptionPane.showMessageDialog(this, "Loan minimum allocation exceeds the clients maximum allocation.\n"
                        + "This loan type cannot apply to this client.", "Allocation mismatch", mOptionPane.ERROR_MESSAGE);
                cboLoanType.requestFocus();

                return;
            }
//            txtLoanAmount.setCValue(loanType.getMinimumAllocation());
            cboPaymentTerm.setCValue(loanType.getPaymentTerm());
            txtInterestRate.setCValue(loanType.getMaximumInterestRate());
            txtGraceDays.setCValue(loanType.getGraceDays());
            txtInstallmentCount.setCValue(loanType.getInstallmentCount());
            try {
                calculateInstallmentAmount();
            } catch (Exception ex) {
//                Logger.getLogger(PCLoan.class.getName()).log(Level.SEVERE, null, ex);
            }

            //panalty
            chkPanalty.setSelected(loanType.getPanaltyAvailable());
            txtPanaltyAmount.setCValue(loanType.getPanaltyAmount());
            txtPanaltyRate.setCValue(loanType.getPanaltyRate());
            radioButtonGroup.setCValue(loanType.getPanaltyType());

            //minimum and maximum
//            txtLoanAmount.setMinimumValue(loanType.getMinimumAllocation());
//            txtLoanAmount.setMaximumValue(
//                    client.getConsiderMaximumAllocation()
//                    ? Math.min(loanType.getMaximumAllocation(), client.getMaximumAllocation())
//                    : loanType.getMaximumAllocation());

            txtInterestRate.setMinimumValue(loanType.getMinimumInterestRate());
            txtInterestRate.setMaximumValue(loanType.getMaximumInterestRate());

            //loan group
             if(loanType.getAvailableGroupLoan()){
            cboLoanGroup.setValueEditable(loanType.getAvailableGroupLoan());
            cboLoanGroup.setCValue(null);
            }
        }

    }

    private void calculateInterrest() {
        double loanAmount = txtLoanAmount.getCValue();
        double installmentCount = txtInstallmentCount.getCValue();
        double installAmount = txtInstallmentAmount.getCValue();
        double paymentCapital=loanAmount/installmentCount;
        
       double x= ((installAmount-paymentCapital)*100)/loanAmount;
       
        double interstRate = 0.0;
       
       switch (cboPaymentTerm.getCValue().toString()) {
           case PaymentTerm.MONTHLY:
               interstRate=x * 12;
               break;
           case PaymentTerm.WEEKLY:
               interstRate=x * 52;
               break;
           case PaymentTerm.DAILY:
               interstRate=x * 300;
               break;
           default:
               throw new AssertionError();
       }
       
        
//        System.out.println("myRate..........._________"+interstRate);

        txtInterestRate.setCValue(interstRate);
    }

    private void calculateInstallmentAmount() throws Exception {
        Double loanAmount;
        Double interestRate;
        Integer installmentCount;
        String paymentTerm;
        String interestMethod;
        Date expectedLoanDate;
        try {
            loanAmount = txtLoanAmount.getCValue();
            interestRate = txtInterestRate.getCValue();
            installmentCount = txtInstallmentCount.getCValue();

            if (cboLoanType.getCValue() != null && installmentCount != 0) {

                paymentTerm = (String) cboPaymentTerm.getCValue();
                interestMethod = ((LoanType) cboLoanType.getCValue()).getInterestMethod();
                expectedLoanDate = txtExpectedLoanDate.getCValue();

                LoanInformation information = LoanCalculationUtil.getInstallmentAmount(
                        loanAmount,
                        interestRate,
                        installmentCount,
                        paymentTerm,
                        interestMethod,
                        expectedLoanDate);

                //SET LOAN INFORMATION
                txtInstallmentAmount.setCValue(information.getInstallmentAmount());
            }

//            lblInfo.setVisible(false);
        } catch (Exception ex) {
            txtInstallmentAmount.setCValue(0.0);

//            lblInfo.setVisible(true);
//            lblInfo.setCValue(ex.getMessage());

            throw ex;
        }
    }

    private void calculateDefaultCharges() {
        /*List<LoanDefaultCharge> loanDefaultCharge;
         List<LoanDefaultChargesInformation> information;
         HibernateDatabaseService databaseService = null;
         try {
         databaseService = getCPanel().getDatabaseService();

         loanDefaultCharge = databaseService.getCollection("FROM com.mac.loan.zobject.LoanDefaultCharge WHERE active=true");
         //            double loanAmount = txtLoanAmount.getCValue();
         double loanAmount = ((SalesInvoice)cboSalesInvoice.getCValue()).getSalesValue();
         LoanType loanType = (LoanType) cboLoanType.getCValue();

         if (loanType != null) {
         information = LoanDefaultChargesCalculation.getDefaultCharges(loanDefaultCharge, loanAmount, loanType.getCode());
         } else {
         information = new ArrayList<>();
         }

         //Set Loan Default Charges Information
         tblDefaultCharges.setCValue(information);
         } catch (Exception e) {
         }*/


        LoanType loanType = (LoanType) cboLoanType.getCValue();
        double loanAmount = txtLoanAmount.getCValue();

        Collection<ChargeScheme> chargeSchemes = loanType.getChargeSchemes();
        double InstallmentCount = txtInstallmentCount.getCValue();
        List<LoanDefaultChargesInformation> information = LoanDefaultChargesCalculation.getDefaultCharges(chargeSchemes, loanAmount, loanType,InstallmentCount);
        tblDefaultCharges.setCValue(information);
    }

    private void calculatePaymentSchedule() throws Exception {
        Double loanAmount;
        Double interestRate;
        Integer installmentCount;
        String paymentTerm;
        String interestMethod;
        HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
        Date expectedLoanDate;
        try {
            loanAmount = txtLoanAmount.getCValue();
            interestRate = txtInterestRate.getCValue();
            installmentCount = txtInstallmentCount.getCValue();

            paymentTerm = (String) cboPaymentTerm.getCValue();
            interestMethod = ((LoanType) cboLoanType.getCValue()).getInterestMethod();
            expectedLoanDate = txtExpectedLoanDate.getCValue();

            List<LoanPaymentInformation> information = LoanCalculationUtil.getPaymentSchedule(
                    databaseService,
                    loanAmount,
                    interestRate,
                    installmentCount,
                    paymentTerm,
                    interestMethod,
                    expectedLoanDate);

            //SET LOAN INFORMATION
            ((CTableModel) tblPaymentSchedule.getModel()).setTableData(information);

//            lblInfo.setVisible(false);

        } catch (Exception ex) {
            ((CTableModel) tblPaymentSchedule.getModel()).setTableData(Collections.emptyList());

//            lblInfo.setVisible(true);
//            lblInfo.setCValue(ex.getMessage());

            throw ex;
        }
    }

    private void resetLoanAmount() {
        SalesInvoice salesInvoice = (SalesInvoice) cboSalesInvoice.getCValue();

        txtLoanAmount.setCValue(salesInvoice.getCreditAmount());
        txtClient.setText(salesInvoice.getClient().getName());
        try {
            calculateInstallmentAmount();
        } catch (Exception ex) {
            Logger.getLogger(PCLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
         cboLoanGroup.setEnabled(false);
        radioButtonGroup = new CRadioButtonGroup();

        radPanaltyNone.setValue(PanaltyType.NONE);
        radPanaltyRate.setValue(PanaltyType.RATE);
        radPanaltyAmount.setValue(PanaltyType.AMOUNT);

        radioButtonGroup.addRadioButton(radPanaltyNone);
        radioButtonGroup.addRadioButton(radPanaltyRate);
        radioButtonGroup.addRadioButton(radPanaltyAmount);
        radioButtonGroup.setDefaultRadioButton(radPanaltyNone);

        radPanaltyRate.addEnabilityComponent(txtPanaltyRate);
        radPanaltyAmount.addEnabilityComponent(txtPanaltyAmount);

        chkPanalty.addRadioButtonGroup(radioButtonGroup);
        chkPanalty.addComponent(txtGraceDays);

        serLoanApplication = new SERLoanApplication(this);

        cboLoanGroup.setExpressEditable(true);
        cboLoanType.setExpressEditable(true);

        //manual actions
        cboLoanType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loanTypeChanged();
                if (cboLoanType.getCValue() != null && txtLoanAmount.getCValue() != null && txtLoanAmount.getCValue() != 0.00) {
                    calculateDefaultCharges();
                }
            }
        });
        FocusAdapter focusAdapter = new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                try {
                    calculateInstallmentAmount();
                } catch (Exception ex) {
                }
            }
        };
        FocusAdapter focusAdapterInterst = new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                try {
                    calculateInterrest();
                } catch (Exception ex) {
                }
            }
        };


        txtLoanAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                if (cboLoanType.getCValue() != null && txtLoanAmount.getCValue() != null && txtLoanAmount.getCValue() != 0.00) {
                    calculateDefaultCharges();
                }
            }
        });

        txtLoanAmount.addFocusListener(focusAdapter);
        txtInstallmentCount.addFocusListener(focusAdapter);
        txtInterestRate.addFocusListener(focusAdapter);
        txtInstallmentAmount.addFocusListener(focusAdapterInterst);

//        lblInfo.setVisible(false);

        tblPaymentSchedule.setCModel(
                new CTableModel(new CTableColumn[]{
            new CTableColumn("Date", "paymentDate"),
            new CTableColumn("Capital Amount", "capitalAmount"),
            new CTableColumn("Interest Amount", "interestAmount"),
            new CTableColumn("Payment Amount", "paymentAmount")
        }));

        tblDefaultCharges.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Name", "chargeScheme", "name"),
            new CTableColumn("Charge Amount", new String[]{"chargeAmount"}, true)
        }));

        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnRefreshSchedule, "doRefreshPaymentSchedule");
        actionUtil.setAction(btnRefreshDefaultCharge, "doRefreshDefaultCharges");

        //
        cboSalesInvoice.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                resetLoanAmount();
            }
        });
        cboSalesInvoice.getComboBox().addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetLoanAmount();
            }
        });
        cboSalesInvoice.setExpressEditable(true);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        txtInstallmentCount = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        lblInfo1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInstallmentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cboPaymentTerm = new com.mac.af.component.derived.input.combobox.CIComboBox(PaymentTerm.ALL);
        cboLoanGroup = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return serLoanApplication.getLoanGroups();
            }
        };
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtExpectedLoanDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        txtInterestRate = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        radPanaltyNone = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        chkPanalty = new com.mac.af.component.derived.input.checkbox.CIEnabilityCheckBox();
        cLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        cboLoanType = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return serLoanApplication.getLoanTypes();
            }

        };
        radPanaltyRate = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        txtGraceDays = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        radPanaltyAmount = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPanaltyRate = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPanaltyAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cboSalesInvoice = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getSalesInvoices();
            }
        };
        txtClient = new com.mac.af.component.derived.display.textfield.CDStringField();
        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReason = new com.mac.af.component.derived.input.textfield.CIStringField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPaymentSchedule = new com.mac.af.component.derived.display.table.CDTable();
        btnRefreshSchedule = new com.mac.af.component.derived.command.button.CCButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblDefaultCharges = new com.mac.af.component.derived.display.table.CDTable();
        btnRefreshDefaultCharge = new com.mac.af.component.derived.command.button.CCButton();

        cLabel10.setText("Grace Days :");

        lblInfo1.setText("ANNUAL");

        cLabel11.setText("Expected Date :");

        cLabel4.setText("Sales Invoice :");

        cLabel2.setText("Loan Group :");

        radPanaltyNone.setText("None");

        chkPanalty.setText("Panalty");

        cLabel9.setText("Reason :");

        radPanaltyRate.setText("Rate :");

        radPanaltyAmount.setText("Amount :");

        cLabel3.setText("Loan Type :");

        cLabel6.setText("Installment Count :");

        cLabel7.setText("Interest Rate :");

        cLabel1.setText("Client :");

        cLabel8.setText("Payment Term  :");

        cLabel5.setText("Installment Amount :");

        cDLabel1.setText("Loan Amount :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(chkPanalty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radPanaltyNone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radPanaltyRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPanaltyRate, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(radPanaltyAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPanaltyAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtExpectedLoanDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboSalesInvoice, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtClient, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboLoanType, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboLoanGroup, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboPaymentTerm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtInstallmentCount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtInterestRate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblInfo1, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtGraceDays, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtReason, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtExpectedLoanDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboSalesInvoice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoanType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoanGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboPaymentTerm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInterestRate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblInfo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInstallmentCount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkPanalty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radPanaltyNone, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radPanaltyRate, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPanaltyRate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radPanaltyAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPanaltyAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtGraceDays, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReason, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Loan Information", jPanel1);

        tblPaymentSchedule.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblPaymentSchedule);

        btnRefreshSchedule.setText("Refresh");
        btnRefreshSchedule.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshScheduleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnRefreshSchedule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRefreshSchedule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Payment Schedule", jPanel2);

        tblDefaultCharges.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(tblDefaultCharges);

        btnRefreshDefaultCharge.setText("Refresh");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnRefreshDefaultCharge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(276, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRefreshDefaultCharge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Default Charges", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshScheduleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshScheduleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRefreshScheduleActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnRefreshDefaultCharge;
    private com.mac.af.component.derived.command.button.CCButton btnRefreshSchedule;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanGroup;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanType;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboPaymentTerm;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboSalesInvoice;
    private com.mac.af.component.derived.input.checkbox.CIEnabilityCheckBox chkPanalty;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblInfo1;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton radPanaltyAmount;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton radPanaltyNone;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton radPanaltyRate;
    private com.mac.af.component.derived.display.table.CDTable tblDefaultCharges;
    private com.mac.af.component.derived.display.table.CDTable tblPaymentSchedule;
    private com.mac.af.component.derived.display.textfield.CDStringField txtClient;
    private com.mac.af.component.derived.input.textfield.CIDateField txtExpectedLoanDate;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtGraceDays;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtInstallmentAmount;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtInstallmentCount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtInterestRate;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPanaltyAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPanaltyRate;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReason;
    // End of variables declaration//GEN-END:variables
    private CRadioButtonGroup radioButtonGroup;
    private SERLoanApplication serLoanApplication;

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList(
                cboSalesInvoice,
                txtExpectedLoanDate,
                cboLoanType,
                txtLoanAmount,
                txtInstallmentAmount,
                txtInstallmentCount,
                txtInterestRate,
                cboPaymentTerm,
                chkPanalty,
                txtGraceDays);
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList( //                cboLoanGroup,
                txtReason);

    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtExpectedLoanDate, "expectedLoanDate"),
                new CInputComponentBinder(cboLoanGroup, "loanGroup"),
                new CInputComponentBinder(cboLoanType, "loanType"),
                new CInputComponentBinder(txtLoanAmount, "loanAmount"),
                new CInputComponentBinder(txtInstallmentAmount, "installmentAmount"),
                new CInputComponentBinder(txtInstallmentCount, "installmentCount"),
                new CInputComponentBinder(txtInterestRate, "interestRate"),
                new CInputComponentBinder(cboPaymentTerm, "paymentTerm"),
                new CInputComponentBinder(chkPanalty, "panalty"),
                new CInputComponentBinder(radioButtonGroup, "panaltyType"),
                new CInputComponentBinder(txtPanaltyRate, "panaltyRate"),
                new CInputComponentBinder(txtPanaltyAmount, "panaltyAmount"),
                new CInputComponentBinder(txtGraceDays, "graceDays"),
                new CInputComponentBinder(txtReason, "reason"));
    }

    @Override
    protected List<Component> getUneditableComponents() {
        return Arrays.asList((Component) txtLoanAmount);
    }

    @Override
    protected Class getObjectClass() {
        return Loan.class;
    }

    @Override
    protected void initObject() throws ObjectCreatorException {
        doRefreshDefaultCharges();

        super.initObject(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void afterInitObject(Loan object) throws ObjectCreatorException {
        SalesInvoice invoice = (SalesInvoice) cboSalesInvoice.getCValue();

        object.setClient(invoice.getClient());
        object.setSalesInvoice(invoice);
        object.setSalesInvoiceNo(invoice.getIndexNo());
        object.setDefaultChargesInformations((List<LoanDefaultChargesInformation>) tblDefaultCharges.getCValue());
    }
}