/*
 *  TRALoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 8:32:10 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.hp_loan_application;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.transaction.AbstractTransactionPanel;
import com.mac.loan.hp_loan_application.object_creator.PCLoanInit;
import com.mac.loan.zobject.Loan;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.List;

/**
 *
 * @author user
 */
public class TRALoanApplication extends AbstractTransactionPanel {

    public TRALoanApplication() {
        initOthers();
    }

    private void initOthers() {
        if (!init) {
            pcLoanApplication = new PCLoanApplicationContent();
            pcLoanApplicationInit = new PCLoanInit();
            serLoanApplication = new SERLoanApplication(this);

            this.recentButton = new RecentButton();
            this.recentButton.setTransactionType(SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE);
            this.recentButton.setDatabseService(getDatabaseService());

            init = true;
        }
    }

    @Override
    protected AbstractObjectCreator getInitDialogObjectCreator() {
        initOthers();
        return pcLoanApplicationInit;
    }

    @Override
    protected AbstractObjectCreator getInitDialogExternalObjectCreator() {
        initOthers();
        return pcLoanApplicationInit;
    }

    @Override
    protected List getRecentTransactions() {
        initOthers();
        return serLoanApplication.getRegetRecentTransactions();
    }

    @Override
    protected CTableModel getInitDialogTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Index No", "indexNo"),
            new CTableColumn("Reference No", "applicationReferenceNo"),
            new CTableColumn("Transaction Date", "applicationTransactionDate"),
            new CTableColumn("Client", new String[]{"client", "name"}),
            new CTableColumn("Loan Type", "loanType", "name"),
            new CTableColumn("Status", new String[]{"status"}),
            new CTableColumn("Note", new String[]{"note"})
        });
    }

    @Override
    protected void executeSave(Object o) throws Exception {
        initOthers();

        System.out.println(((Loan) o).getSalesInvoice() + "------------ TRA LOAN APPLICATION");

        serLoanApplication.executeSave(o);
    }

    @Override
    protected void executeUpdate(Object o) throws Exception {
        initOthers();
        serLoanApplication.executeUpdate(o);
    }

    @Override
    protected void executeDelete(Object o) throws Exception {
        initOthers();
        serLoanApplication.executeDelete(o);
    }

    @Override
    protected Object getObjectFromInitDataForNew(Object o) {
        return o;
    }

    @Override
    protected Object getObjectFromInitDataForEdit(Object o) {
        return o;
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        initOthers();
        return pcLoanApplication;
    }

    @Override
    public int[] getDialogModes() {
        return new int[]{
            NEW_MOOD,
            EDIT_MOOD
        };
    }

    @Override
    protected String getTitle() {
        return "Loan Application";
    }

    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
    }
    private PCLoanInit pcLoanApplicationInit;
    private PCLoanInit pcLoanApplicationInitExternal;
    private PCLoanApplicationContent pcLoanApplication;
    private SERLoanApplication serLoanApplication;
    private boolean init = false;
    private RecentButton recentButton;
}
