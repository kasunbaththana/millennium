/*
 *  SERLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.hp_loan_application;

import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.concurrent.CExecutable;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.LoanProfile;
import com.mac.loan.LoanSituation;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.ChargeScheme;
import com.mac.loan.zobject.ChargeSchemeCustom;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.SalesInvoice;
import com.mac.loan.zutil.default_charges.LoanDefaultChargesInformation;
import com.mac.registration.charge_scheme.ChargeSchemeType;
import com.mac.registration.client.ClientStatus;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanApplicationAccountInterface;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import java.util.Set;

/**
 *
 * @author user
 */
public class SERLoanApplication extends AbstractService {

    public SERLoanApplication(Component component) {
        super(component);
    }

    public List getSalesInvoices() {
        String hql = "FROM com.mac.loan.zobject.SalesInvoice WHERE loanCreated=false";

        List<SalesInvoice> salesInvoices;
        try {
            salesInvoices = getDatabaseService().getCollection(hql);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
            salesInvoices = new ArrayList<>();
        }

        return salesInvoices;
    }

    public List getRegetRecentTransactions() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status1", LoanStatus.APPLICATION_PENDING);
            params.put("status2", LoanStatus.APPLICATION_SUSPEND);
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Loan where status=:status1 or status=:status2", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(Loan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getClients() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Client where active=true  and status='" + ClientStatus.ACTIVE + "'");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getLoanGroups() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanGroup");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getLoanTypes() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanType");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public void executeSave(final Object o) throws DatabaseException {
        CExecutable<Void> executable = new CExecutable<Void>("Save Loan Application - " + ((Loan) o).getApplicationReferenceNo(), getCPanel()) {
            @Override
            public Void execute() throws Exception {
                 Loan loanOld = (Loan) o;
                Loan loan = (Loan) o;

                //SALES INVOICE
                SalesInvoice salesInvoice = loan.getSalesInvoice();
                salesInvoice.setLoanCreated(true);
                getDatabaseService().save(salesInvoice);

                //
                List<LoanDefaultChargesInformation> defaultChargesInformations = loan.getDefaultChargesInformations();

                //LOAN
                loan.setProfile(LoanProfile.HIGHER_PURCHASE);
                loan.setSituation(LoanSituation.LOAN_APPLICATION);

                if (TransactionUtil.isSendToApprove(getDatabaseService(), SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE)) {
                    loan.setStatus(LoanStatus.APPLICATION_PENDING);
                } else {
                    loan.setStatus(LoanStatus.APPLICATION_ACCEPT);
                }

                loan.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
                loan = (Loan) getDatabaseService().save(loan);
                getDatabaseService().save(loan.getDocuments());
                getDatabaseService().save(loan.getClients());

                //save transaction info
                int transaction = SystemTransactions.insertTransaction(
                        getDatabaseService(),
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        loan.getApplicationReferenceNo(),
                        loan.getApplicationDocumentNo(),
                        loan.getIndexNo(),
                        null,
                        loan.getClient().getCode(),
                        null);

                //SETTLEMENT
                SystemSettlement systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();
                for (LoanDefaultChargesInformation loanDefaultChargesInformation : defaultChargesInformations) {
                    systemSettlement.addSettlementQueue(
                            loan.getApplicationTransactionDate(),
                            loan.getClient().getCode(),
                            loan.getIndexNo(),
                            null,
                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                            transaction,
                            loanDefaultChargesInformation.getChargeScheme().getName(),
                            loanDefaultChargesInformation.getChargeAmount(),
                            SystemSettlement.APPLICATION_CHARGE);
                }
                systemSettlement.flushSettlementQueue(getDatabaseService());

                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();
//                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_CREDIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
//                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_DEBIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);

               //LOAN DEFAULT CHARG CREDIT
                List<LoanDefaultChargesInformation> chargeSchemes = loanOld.getDefaultChargesInformations();
                
                double chargAmount = 0;
                for (LoanDefaultChargesInformation chargeScheme : chargeSchemes) {
                    accountTransaction.saveAccountTransaction(
                            getDatabaseService(),
                            transaction,
                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                            AccountTransactionType.AUTO,
                            AccountSettingCreditOrDebit.CREDIT,
                            chargeScheme.getChargeAmount(),
                            chargeScheme.getChargeScheme().getAccount(),
                            "Default Charg - " + loanOld.getAgreementNo(),0);
                    chargAmount+=chargeScheme.getChargeAmount();
                }
                    //LOAN DEFAULT CHARG DEBIT
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.DEFAULT_CHARG_DEBIT, "Default Charg Amount", chargAmount, AccountTransactionType.AUTO);

                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE);

                return null;
            }
        };
        CApplication.getExecutionManager().execute(executable);
    }

    public void executeUpdate(final Object o) throws DatabaseException {
        CExecutable<Void> executable = new CExecutable<Void>("Save Loan Application - " + ((Loan) o).getApplicationReferenceNo(), getCPanel()) {
            @Override
            public Void execute() throws Exception {
                Loan loan = (Loan) o;

                loan.setNote(null);
                loan.setSituation(LoanSituation.LOAN_APPLICATION);
                loan.setStatus(LoanStatus.APPLICATION_PENDING);
                loan.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));

                loan = (Loan) getDatabaseService().save(loan);
                getDatabaseService().save(loan.getDocuments());
                getDatabaseService().save(loan.getClients());


                SalesInvoice salesInvoice = loan.getSalesInvoice();
                salesInvoice.setLoanCreated(true);
                getDatabaseService().save(salesInvoice);


                //save transaction history
                Integer transaction = SystemTransactions.getTransactionIndexNo(
                        getDatabaseService(),
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        loan.getApplicationReferenceNo());
                SystemTransactions.insertTransactionHistory(
                        getDatabaseService(),
                        transaction,
                        SystemTransactions.ACTION_EDIT,
                        null);

                //SETTLEMENT
                SystemSettlement systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();
                for (LoanDefaultChargesInformation loanDefaultChargesInformation : loan.getDefaultChargesInformations()) {
                    systemSettlement.addSettlementQueue(
                            loan.getApplicationTransactionDate(),
                            loan.getClient().getCode(),
                            loan.getIndexNo(),
                            null,
                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                            transaction,
                            loanDefaultChargesInformation.getChargeScheme().getName(),
                            loanDefaultChargesInformation.getChargeAmount(),
                            SystemSettlement.APPLICATION_CHARGE);
                }
                systemSettlement.flushSettlementQueue(getDatabaseService());

                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_CREDIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_DEBIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE);

                return null;
            }
        };
        CApplication.getExecutionManager().execute(executable);
    }

    public void executeDelete(Object o) throws DatabaseException {
        throw new UnsupportedOperationException("Delete not supported");
    }
}
