/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.vehicle;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author NIMESH-PC
 */
public class REGVehicleAdding extends AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.loan.vehicle.zobject.LoanVehicle.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Agreement No", new String[]{"loan", "agreementNo"}),
//            new CTableColumn("Loan Amount", new String[]{"loan", "loanAmount"}),
//            new CTableColumn("Vehicle No", new String[]{"vehicle", "vehicleNo"}),
            new CTableColumn("Vehicle chassis Number", new String[]{"vehicle", "chassisNumber"}),
            new CTableColumn("Vehicle Model", new String[]{"vehicle", "model"}),
            new CTableColumn("Vehicle Brand", new String[]{"vehicle", "brand"})
        });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCVehicleAdding();
    }

   
}
