/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.vehicle;

import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.vehicle.zobject.LoanVehicle;
import com.mac.loan.vehicle.zobject.Vehicle;
import com.mac.registration.item.PCItem;
import com.mac.zsystem.model.table_model.LoanTableModel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NIMESH-PC
 */
public class PCVehicleAdding extends DefaultObjectCreator {

    /**
     * Creates new form PCVehicleAdding
     */
    public PCVehicleAdding() {
        initComponents();
        initOthers();
    }

    private List getVehicles() {
        List vehicles;
        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            vehicles = databaseService.getCollection("from com.mac.loan.vehicle.zobject.Vehicle");
        } catch (Exception e) {
            vehicles = new ArrayList<>();
            Logger.getLogger(PCItem.class.getName()).log(Level.SEVERE, null, e);
        }

        return vehicles;

    }
  

    private List getLoan() {
        List loans;
        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            loans = databaseService.getCollection("from com.mac.loan.vehicle.zobject.Loan WHERE status='LOAN_START' ");
        } catch (Exception e) {
            loans = new ArrayList<>();
            Logger.getLogger(PCItem.class.getName()).log(Level.SEVERE, null, e);
        }

        return loans;

    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        cboLoans.setExpressEditable(true);
        cboVehicleNos.setExpressEditable(true);

        cboLoans.setTableModel(new LoanTableModel());

        cboVehicleNos.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Vehicle vehicle = (Vehicle) cboVehicleNos.getCValue(); 
                txtBrand.setCValue(vehicle.getBrand());
                txtModel.setCValue(vehicle.getModel());


            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cboLoans = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoan();
            }
        };
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cboVehicleNos = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getVehicles();
            }
        };
        txtModel = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBrand = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtNote = new com.mac.af.component.derived.input.textarea.CITextArea();

        cLabel3.setText("Loan :");

        cLabel4.setText("Vehicle No :");

        cLabel5.setText("Model :");

        cLabel7.setText("Brand:");

        cLabel8.setText("Note :");

        txtNote.setColumns(20);
        txtNote.setRows(5);
        jScrollPane2.setViewportView(txtNote);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cboVehicleNos, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cboLoans, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(12, 12, 12))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtModel, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBrand, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cboLoans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cboVehicleNos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtModel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBrand, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(60, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoans;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboVehicleNos;
    private javax.swing.JScrollPane jScrollPane2;
    private com.mac.af.component.derived.input.textfield.CIStringField txtBrand;
    private com.mac.af.component.derived.input.textfield.CIStringField txtModel;
    private com.mac.af.component.derived.input.textarea.CITextArea txtNote;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList( //                txtCode
                );
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList(
                cboLoans,
                cboVehicleNos);
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(cboVehicleNos, "vehicle"),
//                new CInputComponentBinder(cboVehicleNos, "vehicleNo"),
                new CInputComponentBinder(cboLoans, "loan"),
                new CInputComponentBinder(txtNote, "note"));
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
                txtBrand,
                txtModel);

    }

    @Override
    protected void afterInitObject(Object object) throws ObjectCreatorException {
        super.afterInitObject(object);
        LoanVehicle v = (LoanVehicle) object;
        v.setStatus("ACTIVE");
        v.setIndexNo(v.getLoan().getIndexNo());
       
    }

    @Override
    protected Class getObjectClass() {
        return com.mac.loan.vehicle.zobject.LoanVehicle.class;
    }
    
   
    
}
