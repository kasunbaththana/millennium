/*
 *  PCClient.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.loan.receipt_approve;

import com.mac.registration.client.*;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.af.resources.ApplicationResources;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.loan.receipt_approve.object.Loan;
import com.mac.registration.Salutation;
import com.mac.zresources.FinacResources;
import com.mac.loan.receipt_approve.object.Settlement;
import java.awt.Component;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class PCReceipttApprove extends DefaultObjectCreator {

    private ImageWriter imageWriter;

    /**
     * Creates new form PCClient
     */
    public PCReceipttApprove() {
        initComponents();
        initOthers();
    }



    @Action
    public void doApprove() {

        int q = mOptionPane.showConfirmDialog(null, "Do you sure want to Approve Loan ?", "Loan Approve", mOptionPane.YES_NO_OPTION);
        try {
            if (q == mOptionPane.YES_OPTION) {
                Loan Loan = (Loan) getValue();
                Loan.setReceiptBlock(false);
                getCPanel().getDatabaseService().save(Loan);

                ((AbstractGridObject) getCPanel()).refreshTable();
            }
        } catch (ObjectCreatorException | DatabaseException ex) {
            Logger.getLogger(PCClientApprove.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doClose() {
        TabFunctions.closeTab(this.getCPanel());
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {


        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnApprove, "doApprove");
        actionUtil.setAction(btnClose, "doClose");

        btnApprove.setIcon(FinacResources.getImageIcon(FinacResources.ACTION_ACCEPT, 16, 16));
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        
        
        
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtCode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtName = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtLongName = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtNicNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtAddressLine1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtAddressLine2 = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtAddressLine3 = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtMobile = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtTelephone1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtTelephone2 = new com.mac.af.component.derived.input.textfield.CIStringField();
        cboSalutation = new com.mac.af.component.derived.input.combobox.CIComboBox(
            Salutation.ALL
        )
        ;
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDateOfBirth = new com.mac.af.component.derived.input.textfield.CIDateField();
        btnApprove = new com.mac.af.component.derived.command.button.CCButton();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        lblImage = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        txtArreasAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtArreasVolume = new com.mac.af.component.derived.input.textfield.CIDoubleField();

        txtTelephone2.setPreferredSize(new java.awt.Dimension(79, 16));

        cLabel1.setText("Code :");

        cLabel2.setText("NIC No :");

        cLabel3.setText("Salutation :");

        cLabel4.setText("Name :");

        cLabel5.setText("Long Name :");

        cLabel7.setText("Address Line 1 :");

        cLabel8.setText("Address Line 2 :");

        cLabel9.setText("Address Line 3 :");

        cLabel10.setText("Mobile :");

        cLabel11.setText("Telephone 1 :");

        cLabel12.setText("Telephone 2 :");

        btnApprove.setText("Approve");

        btnClose.setText("Close");

        cLabel13.setText("Arreas Amount :");

        cLabel14.setText("Arreas Volume :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(btnApprove, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtAddressLine1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtLongName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtMobile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtTelephone1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtAddressLine2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtAddressLine3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtTelephone2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(cboSalutation, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtCode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(txtNicNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtDateOfBirth, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(22, 22, 22)
                                .addComponent(txtArreasAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(25, 25, 25)
                                .addComponent(txtArreasVolume, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNicNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDateOfBirth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboSalutation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLongName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddressLine3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTelephone1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTelephone2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtArreasAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtArreasVolume, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(54, 54, 54)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnApprove, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(49, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnApprove;
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboSalutation;
    private com.mac.af.component.derived.display.label.CDLabel lblImage;
    private com.mac.af.component.derived.input.textfield.CIStringField txtAddressLine1;
    private com.mac.af.component.derived.input.textfield.CIStringField txtAddressLine2;
    private com.mac.af.component.derived.input.textfield.CIStringField txtAddressLine3;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtArreasAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtArreasVolume;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCode;
    private com.mac.af.component.derived.input.textfield.CIDateField txtDateOfBirth;
    private com.mac.af.component.derived.input.textfield.CIStringField txtLongName;
    private com.mac.af.component.derived.input.textfield.CIStringField txtMobile;
    private com.mac.af.component.derived.input.textfield.CIStringField txtName;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNicNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTelephone1;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTelephone2;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList(
                (Component) txtCode);
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList(
                );
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                (Component) txtAddressLine1,
                (Component) txtAddressLine2,
                (Component) txtAddressLine3,
                (Component) txtMobile,
                (Component) txtTelephone1,
                (Component) txtTelephone2,
                (Component) txtArreasVolume,
                (Component) txtArreasAmount,
                (Component) txtName,
                (Component) txtNicNo,
                (Component) txtLongName,
                (Component) cboSalutation
                );
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(txtDateOfBirth);
    }
    

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtCode, "agreementNo"),
                new CInputComponentBinder(txtNicNo,"client", "nicNo"),
                new CInputComponentBinder(cboSalutation,"client", "salutation"),
                new CInputComponentBinder(txtName,"client", "name"),
                new CInputComponentBinder(txtLongName,"client", "longName"),
                new CInputComponentBinder(txtAddressLine1,"client", "addressLine1"),
                new CInputComponentBinder(txtAddressLine2,"client", "addressLine2"),
                new CInputComponentBinder(txtAddressLine3,"client", "addressLine3"),
                new CInputComponentBinder(txtMobile,"client", "mobile"),
                new CInputComponentBinder(txtTelephone1,"client", "telephone1"),
                new CInputComponentBinder(txtArreasVolume, "volume"),
                new CInputComponentBinder(txtArreasAmount, "arAmount"),
                new CInputComponentBinder(txtTelephone2,"client", "telephone2")// new CInputComponentBinder(chkActive, "active")
                );
    }

    @Override
    protected Class getObjectClass() {
        return com.mac.loan.receipt_approve.object.Loan.class;
    }

    @Override
    protected void afterNewObject(Object object) {
        ((com.mac.loan.receipt_approve.object.Loan) object).setReceiptBlock(false);
//        Loan loan = (Loan) object;
//        double balanceAmount=0.0;
//        try {
//            List<Settlement> listLoan= getSettlements(loan);
//            for(Settlement lList:listLoan){
//                balanceAmount+=lList.getBalanceAmount();
//            }
//            cdl.setCValue(balanceAmount);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(PCReceipttApprove.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        super.initInterface();

    }
    
       
     private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }
    

    
    
    public List<Settlement> getSettlements(Loan loan) throws DatabaseException {
        if (loan != null) {

            String hql = "FROM com.mac.loan.receipt_approve.object.Settlement WHERE loan=:LOANX";

            HashMap<String, Object> params = new HashMap();
            params.put("LOANX", loan == null ? 0 : loan.getIndexNo());

            return getCPanel().getDatabaseService().getCollection(hql, params);
        } else {
            return Collections.emptyList();
        }
    }
    
}
