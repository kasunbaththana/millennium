/*
 *  ClientApproval.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 3, 2014, 7:57:46 AM 
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.receipt_approve;


import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.loan.receipt_approve.object.Loan;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kasun
 */
public class ReceiptApproval extends AbstractGridObject<Loan> {

    @Override
    protected CTableModel<Loan> getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Agreement NO", new String[]{"agreementNo"}),
            new CTableColumn("Client", "client","longName"),
            new CTableColumn("Type", "LoanType","name"),
            new CTableColumn("Installment", new String[]{"installmentAmount"}),
            new CTableColumn("Preiod", new String[]{"installmentCount"}),
//            new CTableColumn("Telephone 2", new String[]{"telephone2"}),
            new CTableColumn("Loan Amount", new String[]{"loanAmount"})});
    }

    @Override
    protected AbstractObjectCreator<Loan> getObjectCreator() {
        return new PCReceipttApprove();
    }

    @Override
    protected Collection<Loan> getTableData() {
        try {
            return getDatabaseService().getCollection("FROM com.mac.loan.receipt_approve.object.Loan WHERE receiptBlock=1   ");
        } catch (DatabaseException ex) {
            Logger.getLogger(ReceiptApproval.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }
}
