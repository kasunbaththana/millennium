/*
 *  TemperaryReceiptStatus.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 16, 2014, 8:54:42 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan;

/**
 *
 * @author mohan
 */
public class TemperaryReceiptStatus {
    public static final String PENDING = "PENDING";
    public static final String SETTLED = "SETTLED";
    public static final String REALIZE = "REALIZE";
}
