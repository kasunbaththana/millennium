/*
 *  SERRebit.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 13, 2014, 11:21:06 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.rebit;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.util.compare.comparison_chain.ComparisonChain;
import com.mac.loan.LoanStatus;
import com.mac.loan.rebit.custom_object.RebitObject;
import com.mac.loan.zobject.Loan;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.RebitLoanAccountInterface;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class SERRebit extends AbstractService {

    public SERRebit(Component component) {
        super(component);
    }

    public List getSettlements(Loan loan) {
//        String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE settlementType.creditOrDebit=:CREDIT_OR_DEBIT AND loan=:LOAN ";
//        String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE settlementType.creditOrDebit=:CREDIT_OR_DEBIT AND loan=:LOAN AND settlementType.code <> :LOAN_CAPITAL AND status=:STATUS";
        String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE settlementType.creditOrDebit=:CREDIT_OR_DEBIT AND loan=:LOAN  AND status=:STATUS ";
        HashMap<String, Object> params = new HashMap<>();
        params.put("LOAN", loan.getIndexNo());
        params.put("CREDIT_OR_DEBIT", SettlementCreditDebit.CREDIT);
//        params.put("LOAN_CAPITAL", SystemSettlement.LOAN_CAPITAL);
        params.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);

        List<Settlement> settlements;
        List<SettlementHistory> settlementHistory = new ArrayList();

        try {
            settlements = getDatabaseService().getCollection(hql, params);

            for (Settlement settlement : settlements) {
                SettlementHistory history = new SettlementHistory();

                history.setSettlement(settlement);
                history.setSettlementAmount(0.0);
                history.setBeforeBalance(settlement.getBalanceAmount());
                history.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                history.setTransactionType(SystemTransactions.REBIT_APPROVE_TRANSACTION_CODE);
                history.setStatus(RebitStatus.RAPPROVE);

                settlementHistory.add(history);
            }
            sortReceiptReceipts(settlementHistory);
        } catch (DatabaseException ex) {
            Logger.getLogger(ClientRebit.class.getName()).log(Level.SEVERE, null, ex);
        }

        return settlementHistory;
    }

    public void sortReceiptReceipts(List<SettlementHistory> settlementHistory) {
        List<SettlementHistory> panalties = new ArrayList<>();
        for (SettlementHistory panalty : settlementHistory) {
            if (panalty.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                panalties.add(panalty);
            }
        }
//        for (SettlementHistory interst : panalties) {
//            if (interst.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST)) {
//                panalties.add(interst);
//            }
//        }
        settlementHistory.removeAll(panalties);

        Collections.sort(settlementHistory, new Comparator<SettlementHistory>() {
            @Override
            public int compare(SettlementHistory o1, SettlementHistory o2) {
                return ComparisonChain.start()
//                        .compare(o1.getSettlement().getDueDate(), o2.getSettlement().getDueDate())
                        .compare(o1.getSettlement().getSettlementType().getPriority(), o2.getSettlement().getSettlementType().getPriority())
                        .result();
            }
        });
        settlementHistory.addAll(0, panalties);
    }

    public List getLoans() {
        List<Loan> loans;

        String hql = "FROM com.mac.loan.zobject.Loan WHERE status=:STATUS";
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("STATUS", LoanStatus.LOAN_START);

        try {
            loans = getDatabaseService().getCollection(hql, hashMap);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERRebit.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }

    public void save(RebitObject rebit) throws DatabaseException {
        //transaction
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.REBIT_APPROVE_TRANSACTION_CODE,
                rebit.getReferenceNo(),
                rebit.getDocumentNo(),
                rebit.getLoan().getIndexNo(),
                null,
                rebit.getLoan().getClient().getCode(),
                rebit.getNote());

        //settlement
        double interstRebit = 0.0;
        double capitalRebit = 0.0;
        double panaltyRebit = 0.0;
        double otherChargRebit = 0.0;
        double insuranceRebit = 0.0;

        double totalRebit = 0.0;

        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();

        for (SettlementHistory settlementHistory : rebit.getSettlementHistorys()) {
            systemSettlement.addSettlementHistoryQueue(
                    settlementHistory.getSettlement(),
                    SystemTransactions.REBIT_APPROVE_TRANSACTION_CODE,
                    transactionIndex,
                    settlementHistory.getSettlementAmount());
            String stmType = settlementHistory.getSettlement().getSettlementType().getCode();

//            System.out.println(settlementHistory.getSettlement().getSettlementType().getCode()+"-"
//                    + "-----"+SystemSettlement.LOAN_INTEREST);
            totalRebit = totalRebit + settlementHistory.getSettlementAmount();
            switch (stmType) {
                case SystemSettlement.LOAN_INTEREST:
                    interstRebit = interstRebit + settlementHistory.getSettlementAmount();
                    break;
                case SystemSettlement.LOAN_PANALTY:
                    panaltyRebit = panaltyRebit + settlementHistory.getSettlementAmount();
                    break;
                case SystemSettlement.OTHER_CHARGE:
                    otherChargRebit = otherChargRebit + settlementHistory.getSettlementAmount();
                    break;
                case SystemSettlement.INSURANCE:
                    insuranceRebit = insuranceRebit + settlementHistory.getSettlementAmount();
                    break;
                case SystemSettlement.LOAN_CAPITAL:
                    capitalRebit = capitalRebit + settlementHistory.getSettlementAmount();
                    break;
            }


        }
        Loan loan=new Loan(); 
        loan = rebit.getLoan();
        loan.setRebateapprove(true);
        getDatabaseService().save(loan);
        getDatabaseService().commitLocalTransaction();

        System.out.println("interstRebit" + interstRebit);
        System.out.println("panaltyRebit" + panaltyRebit);
        System.out.println("otherChargRebit" + otherChargRebit);
        System.out.println("insuranceRebit" + insuranceRebit);
        System.out.println("LOAN_CAPITALRebit" + capitalRebit);
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());







///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
//        accountTransaction.beginAccountTransactionQueue();
//
//        //Interst Amount
//        accountTransaction.addAccountTransactionQueue(
//                RebitLoanAccountInterface.REBIT_INTEREST_AMOUNT_DEBIT_CODE,
//                "Client Rebit",
//                interstRebit,
//                AccountTransactionType.AUTO);
//        //Capital Amount
//        accountTransaction.addAccountTransactionQueue(
//                RebitLoanAccountInterface.LOAN_CAPITAL_AMOUNT_DEBIT_CODE,
//                "Client Rebit",
//                capitalRebit,
//                AccountTransactionType.AUTO);
//        //PAnalty
//        accountTransaction.addAccountTransactionQueue(
//                RebitLoanAccountInterface.REBIT_PANALTY_AMOUNT_DEBIT_CODE,
//                "Client Rebit",
//                panaltyRebit,
//                AccountTransactionType.AUTO);
//
////                   Other Charg
//        accountTransaction.addAccountTransactionQueue(
//                RebitLoanAccountInterface.REBIT_OTHER_CHARGE_AMOUNT_DEBIT_CODE,
//                "Client Rebit",
//                otherChargRebit,
//                AccountTransactionType.AUTO);
//
//        //Insurance Amount
//        accountTransaction.addAccountTransactionQueue(
//                RebitLoanAccountInterface.REBIT_INSURANCE_AMOUNT_DEBIT_CODE,
//                "Client Rebit",
//                insuranceRebit,
//                AccountTransactionType.AUTO);
//        //Account Credit
//        accountTransaction.addAccountTransactionQueue(
//                RebitLoanAccountInterface.REBIT_AMOUNT_CREDIT_CODE,
//                "Client Rebit Tot",
//                totalRebit,
//                AccountTransactionType.AUTO);
//
//
//        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex,
//                SystemTransactions.REBIT_TRANSACTION_CODE);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
