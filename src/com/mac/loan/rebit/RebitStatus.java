/*
 *  RebitStatus.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 15, 2015, 9:42:00 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.rebit;

/**
 *
 * @author mohan
 */
public class RebitStatus {

    public static final String ACTIVE = "ACTIVE";
    public static final String CANCEL = "CANCEL";
    public static final String RAPPROVE = "RAPPROVE";
}
