/*
 *  Rebit.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 12, 2015, 12:07:52 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.rebit;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.renderer.table.CTableRenderer;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.loan.rebit.custom_object.RebitObject;
import com.mac.loan.zobject.Loan;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author mohan
 */
public class Rebit extends CPanel {

    /**
     * Creates new form Rebit
     */
    public Rebit() {
        initComponents();

        initOthers();
    }

    @Action
    public void doNew() {
        setNewMode();
    }

    @Action
    public void doSave() {
        int q = mOptionPane.showConfirmDialog(null, "Do you sure want to save current rebit?", "Save", mOptionPane.YES_NO_OPTION);

        if (q == mOptionPane.YES_OPTION) {
            RebitObject object = getObject();
            try {
                serRebit.save(object);
//...
                setDefaultMode();

                mOptionPane.showMessageDialog(null, "Successfully saved !!!", "Save", mOptionPane.INFORMATION_MESSAGE);
            } catch (DatabaseException ex) {
                Logger.getLogger(Rebit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Action
    public void doDiscard() {
        int q = mOptionPane.showConfirmDialog(null, "Do you sure want to discard current rebit?", "Save", mOptionPane.YES_NO_OPTION);

        if (q == mOptionPane.YES_OPTION) {
            setNewMode();
        }

    }

    @Action
    public void doPrint() {
        this.recentButton.doClick();
    }

    @Action
    public void doClose() {
        TabFunctions.closeTab(this);
    }

    private void loanChanged() {
        initLoanInformation();
    }

    private void changeRebitAmount() {
        List<SettlementHistory> settlementHistorys = tableModel_.getSettlementHistorys();

        double rebitAmount = 0.0,capitalRebate = 0.0;
        for (SettlementHistory settlementHistory : settlementHistorys) {
            rebitAmount += settlementHistory.getSettlementAmount();
            if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_CAPITAL"))
            {
                capitalRebate +=settlementHistory.getSettlementAmount();
            }
        }
        
        txtRebitAmount.setCValue(rebitAmount);
        txtcapitalRebate.setCValue(capitalRebate);
        txtAfterBalance.setCValue(loanBalance - rebitAmount);
    }

    private void setRebitAmount() {
        double rebitBalance = txtRebitAmount.getCValue();
        double rebit = 0.0;
        List<SettlementHistory> settlementHistorys = tableModel_.getSettlementHistorys();
        for (SettlementHistory settlementHistory : settlementHistorys) {
            rebit = Math.min(settlementHistory.getSettlement().getBalanceAmount(), rebitBalance);
            settlementHistory.setSettlementAmount(rebit);
            rebitBalance -= rebit;
        }
        tableModel_.fireTableModelListeners();
    }

    private void initLoanInformation() {
        //settlements
        Loan loan = (Loan) cboAgreementNo.getCValue();

        loanBalance = 0.0;
        double penaltyAmount = 0.0;
        if (loan != null) {
            List<SettlementHistory> settlements = serRebit.getSettlements(loan);

            for (SettlementHistory settlementHistory : settlements) {
                loanBalance += settlementHistory.getSettlement().getBalanceAmount();

                if (settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                    penaltyAmount += settlementHistory.getSettlement().getBalanceAmount();
                }
            }

            tableModel_.setSettlementHistorys(settlements);
        }

        //other information
        txtClient.setCValue(loan.getClient().getName());
        txtBranch.setCValue(loan.getBranch());

        txtLoanBalance.setCValue(loanBalance);
        txtPenaltyBalance.setCValue(penaltyAmount);

        changeRebitAmount();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        serRebit = new SERRebit(this);

        //table model
        tableModel_ = new SettlementHistoryTableModel();
        tblSettlements.setModel((TableModel) tableModel_);
        tblSettlements.setDefaultRenderer(Double.class, new CTableRenderer());

        tblSettlements.getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                changeRebitAmount();
            }
        });

        //evevnts

        cboAgreementNo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                //change selected loan
                loanChanged();
            }
        });

        txtRebitAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                setRebitAmount();
            }
        });

        //reference generator
        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.REBIT));

        //actions
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnNew, "doNew");
        actionUtil.setAction(btnSave, "doSave");
        actionUtil.setAction(btnDiscard, "doDiscard");
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnPrint, "doPrint");

        setDefaultMode();

        btnNew.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACCOUNTING_NEW_ICON, 16, 16));
        btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
        btnDiscard.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_DISCARD, 16, 16));
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        btnPrint.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_PRINT_ICON, 16, 16));

        cboAgreementNo.setExpressEditable(true);

        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.REBIT_APPROVE_TRANSACTION_CODE);
        this.recentButton.setDatabseService(getDatabaseService());

        CTableRenderer renderer = new CTableRenderer();
        tblSettlements.setDefaultRenderer(Double.class, renderer);
        tblSettlements.setDefaultRenderer(Integer.class, renderer);
        tblSettlements.setDefaultRenderer(Date.class, renderer);
    }

    private void setNewMode() {
        //change mode
        changeMode(NEW_MODE);

        //set reference numbers
        resetValues();

    }

    private void setDefaultMode() {
        changeMode(DEFAULT_MODE);
    }

    private void resetValues() {
        txtReferenceNo.resetValue();
        txtDocumentNo.resetValue();
        txtTransactionDate.resetValue();

        cboAgreementNo.resetValue();
        txtClient.resetValue();
        txtBranch.resetValue();
        txtLoanAmount.resetValue();
        txtLoanBalance.resetValue();
        txtRebitAmount.resetValue();
        txtcapitalRebate.resetValue();
        txtAfterBalance.resetValue();
    }

    private RebitObject getObject() {
        RebitObject object = new RebitObject();

        object.setReferenceNo(txtReferenceNo.getCValue());
        object.setDocumentNo(txtDocumentNo.getCValue());
        object.setTransactionDate(txtTransactionDate.getCValue());
        object.setLoan((Loan) cboAgreementNo.getCValue());
        object.setNote(txtNote.getCValue());

        object.setSettlementHistorys(tableModel_.getSettlementHistorys());


        return object;
    }

    private void changeMode(int mode) {
        currentMode = mode;

        switch (mode) {
            case DEFAULT_MODE:
                txtReferenceNo.setValueEditable(false);
                txtDocumentNo.setValueEditable(false);
                txtTransactionDate.setValueEditable(false);

                cboAgreementNo.setValueEditable(false);
                txtClient.setValueEditable(false);
                txtBranch.setValueEditable(false);
                txtLoanAmount.setValueEditable(false);
                txtLoanBalance.setValueEditable(false);
                txtPenaltyBalance.setValueEditable(false);
                txtRebitAmount.setValueEditable(false);
                txtcapitalRebate.setValueEditable(false);
                txtAfterBalance.setValueEditable(false);
                txtNote.setValueEditable(false);

                btnNew.setVisible(true);
                btnSave.setVisible(false);
                btnDiscard.setVisible(false);
                btnClose.setVisible(true);

                break;
            case NEW_MODE:
                txtReferenceNo.setValueEditable(false);
                txtDocumentNo.setValueEditable(true);
                txtTransactionDate.setValueEditable(false);

                cboAgreementNo.setValueEditable(true);
                txtClient.setValueEditable(false);
                txtBranch.setValueEditable(false);
                txtLoanAmount.setValueEditable(false);
                txtLoanBalance.setValueEditable(false);
                txtPenaltyBalance.setValueEditable(false);
                txtRebitAmount.setValueEditable(true);
                txtcapitalRebate.setValueEditable(false);
                txtAfterBalance.setValueEditable(false);
                txtNote.setValueEditable(true);

                btnNew.setVisible(false);
                btnSave.setVisible(true);
                btnDiscard.setVisible(true);
                btnClose.setVisible(false);
                break;
            default:
                throw new AssertionError();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cboAgreementNo = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return serRebit.getLoans();
            }
        };
        jLabel2 = new javax.swing.JLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        jLabel3 = new javax.swing.JLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        jLabel4 = new javax.swing.JLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        jLabel5 = new javax.swing.JLabel();
        txtClient = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtBranch = new com.mac.af.component.derived.input.textfield.CIStringField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtLoanBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtRebitAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel10 = new javax.swing.JLabel();
        txtAfterBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel11 = new javax.swing.JLabel();
        txtNote = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnNew = new com.mac.af.component.derived.command.button.CCButton();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        btnDiscard = new com.mac.af.component.derived.command.button.CCButton();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnPrint = new com.mac.af.component.derived.command.button.CCButton();
        jLabel12 = new javax.swing.JLabel();
        txtPenaltyBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel13 = new javax.swing.JLabel();
        txtcapitalRebate = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSettlements = new javax.swing.JTable();

        jSplitPane1.setDividerLocation(400);

        jLabel1.setText("Agreement No.:");

        jLabel2.setText("Reference No.:");

        jLabel3.setText("Document No.:");

        jLabel4.setText("Transaction Date :");

        jLabel5.setText("Client:");

        jLabel6.setText("Branch :");

        jLabel7.setText("Loan Amount :");

        txtLoanAmount.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        txtLoanBalance.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel8.setText("Loan Balance :");

        jLabel9.setText("Rebit Amount :");

        txtRebitAmount.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel10.setText("After Balance :");

        txtAfterBalance.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel11.setText("Note :");

        btnNew.setText("New");

        btnSave.setText("Save");

        btnDiscard.setText("Discard");

        btnClose.setText("Close");

        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        jLabel12.setText("Penalty Balance :");

        txtPenaltyBalance.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel13.setText("Capital Rebate:");

        txtcapitalRebate.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12))
                        .addGap(16, 16, 16)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDiscard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtAfterBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtRebitAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtLoanBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtLoanAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtBranch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtClient, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboAgreementNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtPenaltyBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel13)
                        .addGap(24, 24, 24)
                        .addComponent(txtcapitalRebate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtClient, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtBranch, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtLoanBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtPenaltyBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtRebitAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtAfterBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtcapitalRebate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtNote, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDiscard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel1);

        tblSettlements.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblSettlements);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 525, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 411, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane1.setRightComponent(jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPrintActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnDiscard;
    private com.mac.af.component.derived.command.button.CCButton btnNew;
    private com.mac.af.component.derived.command.button.CCButton btnPrint;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAgreementNo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTable tblSettlements;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAfterBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtBranch;
    private com.mac.af.component.derived.input.textfield.CIStringField txtClient;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNote;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPenaltyBalance;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtRebitAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtcapitalRebate;
    // End of variables declaration//GEN-END:variables
    private SERRebit serRebit;
    private SettlementHistoryTableModel tableModel_;
    private double loanBalance;
    //modes
    private int currentMode = DEFAULT_MODE;
    private static final int DEFAULT_MODE = 0;
    private static final int NEW_MODE = 1;
    private RecentButton recentButton;
}
