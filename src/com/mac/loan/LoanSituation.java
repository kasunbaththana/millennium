/*
 *  LoanSituation.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Sep 30, 2014, 2:52:10 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan;

/**
 *
 * @author mohan
 */
public class LoanSituation {

    public static final String LOAN = "LOAN";
    public static final String LOAN_APPLICATION = "LOAN_APPLICATION";
}
