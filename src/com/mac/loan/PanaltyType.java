/*
 *  PanaltyType.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 18, 2014, 2:56:28 PM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan;

/**
 *
 * @author user
 */
public class PanaltyType {
    public static final String NONE = "NONE";
    public static final String RATE = "RATE";
    public static final String AMOUNT = "AMOUNT";
}
