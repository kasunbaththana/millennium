/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_restart;

import com.mac.loan.loan_close.*;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.ReasonSetup;
import com.mac.zsystem.transaction.account.object.Account;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thilanga
 */
public class SERLoanRestart extends AbstractService {

//    public static String LOAN_CLOSE = "LOAN CLOSE";
//    public static String LOAN_CANCEL = "LOAN CANCEL";
    public static String LOAN_RESTART = "Loan Restart";
    //
    private Loan loan;
    private REGLoanClose cPanel;

    public SERLoanRestart(Component component) {
        super(component);
    }

    private void initCPanel() {
        if (cPanel == null) {
            cPanel = (REGLoanClose) getCPanel();
        }
    }

    public List<Settlement> getSettlements() throws DatabaseException {
        if (loan != null) {

            String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan=:LOANX AND settlementType<>'LOAN_AMOUNT' ";

            HashMap<String, Object> params = new HashMap();
            params.put("LOANX", loan == null ? 0 : loan.getIndexNo());

            return getDatabaseService().getCollection(hql, params);
        } else {
            return Collections.emptyList();
        }
    }

    public List<Loan> getLoans() {
        List<Loan> loan;
        try {
            String hql = "FROM com.mac.loan.zobject.Loan WHERE  status='SUSPEND'";
            loan = getDatabaseService().getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }
    public List<ReasonSetup> getReason() {
        List<ReasonSetup> reasonSetup;
        try {
            String hql = "FROM com.mac.loan.zobject.ReasonSetup ";
            reasonSetup = getDatabaseService().getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            reasonSetup = new ArrayList();
        }
        return reasonSetup;
    }

    public List<Account> getAccounts() {
        List<Account> loan;
        try {
            loan = getDatabaseService().getCollection(Account.class);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }

    /**
     * Consider following transactions Loan Application/ Loan/ Receipts/ Voucher
     */

   
public void ReasonClose(Loan loan)
{
    try {
//        getDatabaseService().beginLocalTransaction();
         int transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LOAN_TRANSACTION_CODE,
                loan.getLoanReferenceNo(),
                loan.getLoanDocumentNo(),
                loan.getIndexNo(),
                null,
                loan.getClient().getCode(),
                "loan restart");
         loan.setNote(null);
        loan.setStatus(LoanStatus.LOAN_START);
        loan.setAvailableReceipt(true);
        //loan.setAvailableVoucher(true);
        getDatabaseService().save(loan);
        
        
//        getDatabaseService().commitLocalTransaction();
    } catch (Exception e) {
        e.printStackTrace();
    }
}
   public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
}
}
   
