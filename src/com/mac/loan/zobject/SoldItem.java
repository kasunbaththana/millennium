package com.mac.loan.zobject;

/**
  *	@author Channa Mohan
  *	
  *	Created On Nov 29, 2016 10:57:19 AM 
  *	Mohan Hibernate Mapping Generator
  */


import java.util.HashSet;
import java.util.Set;

/**
 * SoldItem generated by hbm2java
 */
public class SoldItem  implements java.io.Serializable {


     private Integer indexNo;
     private Item item;
     private double salesPrice;
     private String serialNo1;
     private String serialNo2;
     private String note;
     private Set<Loan> loans = new HashSet<Loan>(0);

    public SoldItem() {
    }

	
    public SoldItem(Item item, double salesPrice, String serialNo1, String serialNo2) {
        this.item = item;
        this.salesPrice = salesPrice;
        this.serialNo1 = serialNo1;
        this.serialNo2 = serialNo2;
    }
    public SoldItem(Item item, double salesPrice, String serialNo1, String serialNo2, String note, Set<Loan> loans) {
       this.item = item;
       this.salesPrice = salesPrice;
       this.serialNo1 = serialNo1;
       this.serialNo2 = serialNo2;
       this.note = note;
       this.loans = loans;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public Item getItem() {
        return this.item;
    }
    
    public void setItem(Item item) {
        this.item = item;
    }
    public double getSalesPrice() {
        return this.salesPrice;
    }
    
    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }
    public String getSerialNo1() {
        return this.serialNo1;
    }
    
    public void setSerialNo1(String serialNo1) {
        this.serialNo1 = serialNo1;
    }
    public String getSerialNo2() {
        return this.serialNo2;
    }
    
    public void setSerialNo2(String serialNo2) {
        this.serialNo2 = serialNo2;
    }
    public String getNote() {
        return this.note;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    public Set<Loan> getLoans() {
        return this.loans;
    }
    
    public void setLoans(Set<Loan> loans) {
        this.loans = loans;
    }



@Override
public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof SoldItem) ) return false;
		 SoldItem castOther = ( SoldItem ) other; 

	if(!java.util.Objects.equals(this.item, castOther.item)) {
            return false;
     }
	if(!java.util.Objects.equals(this.salesPrice, castOther.salesPrice)) {
            return false;
     }
	if(!java.util.Objects.equals(this.serialNo1, castOther.serialNo1)) {
            return false;
     }
	if(!java.util.Objects.equals(this.serialNo2, castOther.serialNo2)) {
            return false;
     }
         
		 return true;
   }

   @Override
   public int hashCode() {
         int result = 17;
         
	result = result * 17 + java.util.Objects.hashCode(this.item);
	result = result * 17 + java.util.Objects.hashCode(this.salesPrice);
	result = result * 17 + java.util.Objects.hashCode(this.serialNo1);
	result = result * 17 + java.util.Objects.hashCode(this.serialNo2);

         return result;
   }   

}


