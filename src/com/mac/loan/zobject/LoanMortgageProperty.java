package com.mac.loan.zobject;
// Generated Jun 22, 2021 2:40:45 PM by Hibernate Tools 3.2.1.GA



/**
 * LoanMortgageProperty generated by hbm2java
 */
public class LoanMortgageProperty  implements java.io.Serializable {


     private Integer indexNo;
     private Integer loan;
     private String address;
     private Double extend;
     private Double floorArea;
     private Double purchasePrice;
     private Double forSaleVal;
     private Double constCost;
     private Double marketVal;

    public LoanMortgageProperty() {
    }

    public LoanMortgageProperty(Integer loan, String address, Double extend, Double floorArea, Double purchasePrice, Double forSaleVal, Double constCost, Double marketVal) {
       this.loan = loan;
       this.address = address;
       this.extend = extend;
       this.floorArea = floorArea;
       this.purchasePrice = purchasePrice;
       this.forSaleVal = forSaleVal;
       this.constCost = constCost;
       this.marketVal = marketVal;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public Integer getLoan() {
        return this.loan;
    }
    
    public void setLoan(Integer loan) {
        this.loan = loan;
    }
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    public Double getExtend() {
        return this.extend;
    }
    
    public void setExtend(Double extend) {
        this.extend = extend;
    }
    public Double getFloorArea() {
        return this.floorArea;
    }
    
    public void setFloorArea(Double floorArea) {
        this.floorArea = floorArea;
    }
    public Double getPurchasePrice() {
        return this.purchasePrice;
    }
    
    public void setPurchasePrice(Double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
    public Double getForSaleVal() {
        return this.forSaleVal;
    }
    
    public void setForSaleVal(Double forSaleVal) {
        this.forSaleVal = forSaleVal;
    }
    public Double getConstCost() {
        return this.constCost;
    }
    
    public void setConstCost(Double constCost) {
        this.constCost = constCost;
    }
    public Double getMarketVal() {
        return this.marketVal;
    }
    
    public void setMarketVal(Double marketVal) {
        this.marketVal = marketVal;
    }




}


