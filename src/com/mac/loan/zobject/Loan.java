package com.mac.loan.zobject;

/**
 * @author Channa Mohan
 *
 * Created On Dec 12, 2014 1:08:47 PM Mohan Hibernate Mapping Generator
 */
import com.mac.loan.zutil.default_charges.LoanDefaultChargesInformation;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Loan generated by private Integer indexNo;
    private Employee employeeByRecoveryOfficer;
    private Employee employeeByLoanOfficer;
    private Client client;
    private Client supplier;
    private LoanType loanType;
    private Employee employeeByApproveEmployee;
    private LoanGroup loanGroup;
    private Integer salesInvoiceNo;
    private String applicationReferenceNo;
    private String applicationDocumentNo;
    private Date applicationTransactionDate;
    private String loanReferenceNo;
    private String loanDocumentNo;
    private Date loanTransactionDate;
    private String branch;
    private String agreementNo;
    private double loanAmount;
    private double installmentAmount;
    private int installmentCount;
    private double interestRate;
    private Date expectedLoanDate;
    private Date loanDate;
    private String paymentTerm;
    private boolean panalty;
    private String panaltyType;
    private double panaltyRate;
    private double panaltyAmount;
    private int graceDays;
    private String reason;
    private String note;
    private boolean availableReceipt;
    private boolean availableVoucher;
    private boolean rebateapprove;
    private boolean receiptBlock;
    private String profile;
    private String situation;
    private String status;
    private String vehicle; hbm2java
 */
public class Loan implements java.io.Serializable {

    private Integer indexNo;
    private Employee employeeByRecoveryOfficer;
    private Employee employeeByLoanOfficer;
    private Client client;
    private Client supplier;
    private LoanType loanType;
    private Employee employeeByApproveEmployee;
    private LoanGroup loanGroup;
    private Integer salesInvoiceNo;
    private String applicationReferenceNo;
    private String applicationDocumentNo;
    private Date applicationTransactionDate;
    private String loanReferenceNo;
    private String loanDocumentNo;
    private Date loanTransactionDate;
    private String branch;
    private String agreementNo;
    private double loanAmount;
    private double installmentAmount;
    private int installmentCount;
    private double interestRate;
    private Date expectedLoanDate;
    private Date loanDate;
    private String paymentTerm;
    private boolean panalty;
    private String panaltyType;
    private double panaltyRate;
    private double panaltyAmount;
    private int graceDays;
    private String reason;
    private String note;
    private boolean availableReceipt;
    private boolean availableVoucher;
    private boolean availableDisbursement;
    private boolean rebateapprove;
    private boolean receiptBlock;
    private String profile;
    private String situation;
    private String status;
    private String vehicle;
    private String purpose;
    private double dealer;
    private double economicProfit;
    private double basicPayment;
    private boolean writeOff;
    private String witness1;
    private String witness2;
    private Set<TemperaryReceipt> temperaryReceipts = new HashSet<TemperaryReceipt>(0);
    private Set<Client> clients = new HashSet<Client>(0);
    private Set<Settlement> settlement = new HashSet<Settlement>(0);
    private Set<Document> documents = new HashSet<Document>(0);
    private Set<LoanItemDetails> loanItemDetailses = new HashSet<LoanItemDetails>(0);
//    private Set<Vehicle> vehicles = new HashSet<Vehicle>(0);
    
    private Set<LoanBussinessInformation> loanBussinessInformations = new HashSet<LoanBussinessInformation>(0);
    private Set<LoanBorrowerDetails> loanBorrowerDetails = new HashSet<LoanBorrowerDetails>(0);
    private List<LoanExistingFacility> loanExistingFacilitys = new ArrayList<LoanExistingFacility>(0);
    private Set<LoanIncomeExpencess> loanIncomeExpencesses = new HashSet<LoanIncomeExpencess>(0);
    
    private Set<LoanMortgageDetails> loanMortgageDetailses = new HashSet<LoanMortgageDetails>(0);
    private Set<LoanMortgageProperty> loanMortgagePropertys = new HashSet<LoanMortgageProperty>(0);
    private List<LoanMInstitution> loanMInstitutions = new ArrayList<LoanMInstitution>(0);
    
    private Set<LoanPersonalBusinessInfor> loanPersonalBusinessInfors = new HashSet<LoanPersonalBusinessInfor>(0);
    private Set<LoanPersonalFinacInfor> loanPersonalFinacInfors = new HashSet<LoanPersonalFinacInfor>(0);
    private List<LoanPersonalFacilityInfor> loanPersonalFacilityInforn = new ArrayList<LoanPersonalFacilityInfor>(0);

    private boolean elProduct;
    private boolean mlProduct;
    private boolean plProduct;
    
    private boolean individual;
    private boolean join;
    public Loan() {
    }

    public Loan(Client client, LoanType loanType, Date applicationTransactionDate, String branch, double loanAmount, double installmentAmount, int installmentCount, double interestRate, Date expectedLoanDate, String paymentTerm, boolean panalty, String panaltyType, double panaltyRate,boolean receiptBlock, double panaltyAmount, int graceDays, boolean availableReceipt,boolean rebateapprove, boolean availableVoucher, String profile, String situation, String status) {
        this.client = client;
        this.loanType = loanType;
        this.applicationTransactionDate = applicationTransactionDate;
        this.branch = branch;
        this.loanAmount = loanAmount;
        this.installmentAmount = installmentAmount;
        this.installmentCount = installmentCount;
        this.interestRate = interestRate;
        this.expectedLoanDate = expectedLoanDate;
        this.paymentTerm = paymentTerm;
        this.panalty = panalty;
        this.panaltyType = panaltyType;
        this.panaltyRate = panaltyRate;
        this.panaltyAmount = panaltyAmount;
        this.graceDays = graceDays;
        this.availableReceipt = availableReceipt;
        this.availableVoucher = availableVoucher;
        this.rebateapprove = rebateapprove;
        this.receiptBlock = receiptBlock;
        this.profile = profile;
        this.situation = situation;
        this.status = status;
    }

    public Loan(Employee employeeByRecoveryOfficer, Employee employeeByLoanOfficer,boolean rebateapprove, Client client, LoanType loanType, Employee employeeByApproveEmployee, LoanGroup loanGroup, String applicationReferenceNo, String applicationDocumentNo, Date applicationTransactionDate, String loanReferenceNo, String loanDocumentNo, Date loanTransactionDate, String branch, String agreementNo, double loanAmount, double installmentAmount, int installmentCount, double interestRate, Date expectedLoanDate, Date loanDate, String paymentTerm, boolean panalty, String panaltyType, double panaltyRate, double panaltyAmount, int graceDays, String reason, String note, boolean availableReceipt, boolean availableVoucher, String profile, String situation, String status, Set<TemperaryReceipt> temperaryReceipts, Set<Client> clients,Set<Settlement> settlement,Set<LoanItemDetails> loanItemDetailses, Set<Document> documents,String purpose) {
        this.employeeByRecoveryOfficer = employeeByRecoveryOfficer;
        this.employeeByLoanOfficer = employeeByLoanOfficer;
        this.client = client;
        this.loanType = loanType;
        this.employeeByApproveEmployee = employeeByApproveEmployee;
        this.loanGroup = loanGroup;
        this.applicationReferenceNo = applicationReferenceNo;
        this.applicationDocumentNo = applicationDocumentNo;
        this.applicationTransactionDate = applicationTransactionDate;
        this.loanReferenceNo = loanReferenceNo;
        this.loanDocumentNo = loanDocumentNo;
        this.loanTransactionDate = loanTransactionDate;
        this.branch = branch;
        this.agreementNo = agreementNo;
        this.loanAmount = loanAmount;
        this.installmentAmount = installmentAmount;
        this.installmentCount = installmentCount;
        this.interestRate = interestRate;
        this.expectedLoanDate = expectedLoanDate;
        this.loanDate = loanDate;
        this.paymentTerm = paymentTerm;
        this.panalty = panalty;
        this.panaltyType = panaltyType;
        this.panaltyRate = panaltyRate;
        this.panaltyAmount = panaltyAmount;
        this.graceDays = graceDays;
        this.reason = reason;
        this.note = note;
        this.availableReceipt = availableReceipt;
        this.availableVoucher = availableVoucher;
        this.profile = profile;
        this.situation = situation;
        this.status = status;
        this.temperaryReceipts = temperaryReceipts;
        this.clients = clients;
        this.settlement = settlement;
        this.documents = documents;
        this.purpose = purpose;
        this.loanItemDetailses = loanItemDetailses;
    }

    public Integer getIndexNo() {
        return this.indexNo;
    }

    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }

    public Employee getEmployeeByRecoveryOfficer() {
        return this.employeeByRecoveryOfficer;
    }

    public void setEmployeeByRecoveryOfficer(Employee employeeByRecoveryOfficer) {
        this.employeeByRecoveryOfficer = employeeByRecoveryOfficer;
    }

    public Employee getEmployeeByLoanOfficer() {
        return this.employeeByLoanOfficer;
    }

    public void setEmployeeByLoanOfficer(Employee employeeByLoanOfficer) {
        this.employeeByLoanOfficer = employeeByLoanOfficer;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getSupplier() {
        return supplier;
    }

    public void setSupplier(Client supplier) {
        this.supplier = supplier;
    }
    

    public LoanType getLoanType() {
        return this.loanType;
    }

    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }

    public Employee getEmployeeByApproveEmployee() {
        return this.employeeByApproveEmployee;
    }

    public void setEmployeeByApproveEmployee(Employee employeeByApproveEmployee) {
        this.employeeByApproveEmployee = employeeByApproveEmployee;
    }

    public LoanGroup getLoanGroup() {
        return this.loanGroup;
    }

    public void setLoanGroup(LoanGroup loanGroup) {
        this.loanGroup = loanGroup;
    }

    public Integer getSalesInvoiceNo() {
        return salesInvoiceNo;
    }

    public void setSalesInvoiceNo(Integer salesInvoice) {
        this.salesInvoiceNo = salesInvoice;
    }

    public String getApplicationReferenceNo() {
        return this.applicationReferenceNo;
    }

    public void setApplicationReferenceNo(String applicationReferenceNo) {
        this.applicationReferenceNo = applicationReferenceNo;
    }

    public String getApplicationDocumentNo() {
        return this.applicationDocumentNo;
    }

    public void setApplicationDocumentNo(String applicationDocumentNo) {
        this.applicationDocumentNo = applicationDocumentNo;
    }

    public Date getApplicationTransactionDate() {
        return this.applicationTransactionDate;
    }

    public void setApplicationTransactionDate(Date applicationTransactionDate) {
        this.applicationTransactionDate = applicationTransactionDate;
    }

    public String getLoanReferenceNo() {
        return this.loanReferenceNo;
    }

    public void setLoanReferenceNo(String loanReferenceNo) {
        this.loanReferenceNo = loanReferenceNo;
    }

    public String getLoanDocumentNo() {
        return this.loanDocumentNo;
    }

    public void setLoanDocumentNo(String loanDocumentNo) {
        this.loanDocumentNo = loanDocumentNo;
    }

    public Date getLoanTransactionDate() {
        return this.loanTransactionDate;
    }

    public void setLoanTransactionDate(Date loanTransactionDate) {
        this.loanTransactionDate = loanTransactionDate;
    }

    public String getBranch() {
        return this.branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAgreementNo() {
        return this.agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public double getLoanAmount() {
        return this.loanAmount;
    }

    public void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public double getInstallmentAmount() {
        return this.installmentAmount;
    }

    public void setInstallmentAmount(double installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public int getInstallmentCount() {
        return this.installmentCount;
    }

    public void setInstallmentCount(int installmentCount) {
        this.installmentCount = installmentCount;
    }

    public double getInterestRate() {
        return this.interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public Date getExpectedLoanDate() {
        return this.expectedLoanDate;
    }

    public void setExpectedLoanDate(Date expectedLoanDate) {
        this.expectedLoanDate = expectedLoanDate;
    }

    public Date getLoanDate() {
        return this.loanDate;
    }

    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }

    public String getPaymentTerm() {
        return this.paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public boolean isPanalty() {
        return this.panalty;
    }

    public void setPanalty(boolean panalty) {
        this.panalty = panalty;
    }

    public String getPanaltyType() {
        return this.panaltyType;
    }

    public void setPanaltyType(String panaltyType) {
        this.panaltyType = panaltyType;
    }

    public double getPanaltyRate() {
        return this.panaltyRate;
    }

    public void setPanaltyRate(double panaltyRate) {
        this.panaltyRate = panaltyRate;
    }

    public double getPanaltyAmount() {
        return this.panaltyAmount;
    }

    public void setPanaltyAmount(double panaltyAmount) {
        this.panaltyAmount = panaltyAmount;
    }

    public int getGraceDays() {
        return this.graceDays;
    }

    public void setGraceDays(int graceDays) {
        this.graceDays = graceDays;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isAvailableReceipt() {
        return this.availableReceipt;
    }

    public void setAvailableReceipt(boolean availableReceipt) {
        this.availableReceipt = availableReceipt;
    }

    public boolean isAvailableVoucher() {
        return this.availableVoucher;
    }

    public void setAvailableVoucher(boolean availableVoucher) {
        this.availableVoucher = availableVoucher;
    }

    public String getProfile() {
        return this.profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getSituation() {
        return this.situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String Vehicle) {
        this.vehicle = Vehicle;
    }

    public Set<Settlement> getSettlement() {
        return settlement;
    }

    public void setSettlement(Set<Settlement> settlement) {
        this.settlement = settlement;
    }

    public double getDealer() {
        return dealer;
    }

    public void setDealer(double dealer) {
        this.dealer = dealer;
    }
    
    

    public Set<TemperaryReceipt> getTemperaryReceipts() {
        return this.temperaryReceipts;
    }

    public void setTemperaryReceipts(Set<TemperaryReceipt> temperaryReceipts) {
        this.temperaryReceipts = temperaryReceipts;
    }

    public Set<Client> getClients() {
        return this.clients;
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }

    public Set<LoanItemDetails> getLoanItemDetailses() {
        return loanItemDetailses;
    }

    public void setLoanItemDetailses(Set<LoanItemDetails> loanItemDetailses) {
        this.loanItemDetailses = loanItemDetailses;
    }
    public boolean isRebateapprove() {
        return rebateapprove;
    }

    public void setRebateapprove(boolean rebateapprove) {
        this.rebateapprove = rebateapprove;
    }

    public boolean isReceiptBlock() {
        return receiptBlock;
    }

    public void setReceiptBlock(boolean receiptBlock) {
        this.receiptBlock = receiptBlock;
    }
    
    

    public Set<Document> getDocuments() {
        return this.documents;
    }

    public void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    public Set<LoanMortgageDetails> getLoanMortgageDetailses() {
        return loanMortgageDetailses;
    }

    public void setLoanMortgageDetailses(Set<LoanMortgageDetails> loanMortgageDetailses) {
        this.loanMortgageDetailses = loanMortgageDetailses;
    }

    public Set<LoanMortgageProperty> getLoanMortgagePropertys() {
        return loanMortgagePropertys;
    }

    public void setLoanMortgagePropertys(Set<LoanMortgageProperty> loanMortgagePropertys) {
        this.loanMortgagePropertys = loanMortgagePropertys;
    }

    public List<LoanMInstitution> getLoanMInstitutions() {
        return loanMInstitutions;
    }

    public void setLoanMInstitutions(List<LoanMInstitution> loanMInstitutions) {
        this.loanMInstitutions = loanMInstitutions;
    }
    

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null) {
            return false;
        }

        if (!(other instanceof Loan)) {
            return false;
        }

        Loan castOther = (Loan) other;

        if (this.indexNo == null && castOther.indexNo == null) {
            return false;
        }

        if (!java.util.Objects.equals(this.indexNo, castOther.indexNo)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = result * 17 + java.util.Objects.hashCode(this.indexNo);

        return result;
    }

    @Override
    public String toString() {
        return agreementNo;
    }
    //---------------------------------------------
    private SalesInvoice salesInvoice;
    private List<LoanDefaultChargesInformation> defaultChargesInformations;

    public SalesInvoice getSalesInvoice() {
        return salesInvoice;
    }

    public void setSalesInvoice(SalesInvoice salesInvoice) {
        this.salesInvoice = salesInvoice;
    }

    public List<LoanDefaultChargesInformation> getDefaultChargesInformations() {
        return defaultChargesInformations;
    }

    public void setDefaultChargesInformations(List<LoanDefaultChargesInformation> defaultChargesInformations) {
        this.defaultChargesInformations = defaultChargesInformations;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Set<LoanBussinessInformation> getLoanBussinessInformations() {
        return loanBussinessInformations;
    }

    public void setLoanBussinessInformations(Set<LoanBussinessInformation> loanBussinessInformations) {
        this.loanBussinessInformations = loanBussinessInformations;
    }

    public Set<LoanBorrowerDetails> getLoanBorrowerDetails() {
        return loanBorrowerDetails;
    }

    public void setLoanBorrowerDetails(Set<LoanBorrowerDetails> loanBorrowerDetails) {
        this.loanBorrowerDetails = loanBorrowerDetails;
    }

    public List<LoanExistingFacility> getLoanExistingFacilitys() {
        return loanExistingFacilitys;
    }

    public void setLoanExistingFacilitys(List<LoanExistingFacility> loanExistingFacilitys) {
        this.loanExistingFacilitys = loanExistingFacilitys;
    }

    public Set<LoanIncomeExpencess> getLoanIncomeExpencesses() {
        return loanIncomeExpencesses;
    }

    public void setLoanIncomeExpencesses(Set<LoanIncomeExpencess> loanIncomeExpencesses) {
        this.loanIncomeExpencesses = loanIncomeExpencesses;
    }

    public boolean isAvailableDisbursement() {
        return availableDisbursement;
    }

    public void setAvailableDisbursement(boolean availableDisbursement) {
        this.availableDisbursement = availableDisbursement;
    }

    public boolean isElProduct() {
        return elProduct;
    }

    public void setElProduct(boolean elProduct) {
        this.elProduct = elProduct;
    }

    public boolean isMlProduct() {
        return mlProduct;
    }

    public void setMlProduct(boolean mlProduct) {
        this.mlProduct = mlProduct;
    }

    public boolean isPlProduct() {
        return plProduct;
    }

    public void setPlProduct(boolean plProduct) {
        this.plProduct = plProduct;
    }

    public boolean isIndividual() {
        return individual;
    }

    public void setIndividual(boolean individual) {
        this.individual = individual;
    }

    public boolean isJoin() {
        return join;
    }

    public void setJoin(boolean join) {
        this.join = join;
    }

    public Set<LoanPersonalBusinessInfor> getLoanPersonalBusinessInfors() {
        return loanPersonalBusinessInfors;
    }

    public void setLoanPersonalBusinessInfors(Set<LoanPersonalBusinessInfor> loanPersonalBusinessInfors) {
        this.loanPersonalBusinessInfors = loanPersonalBusinessInfors;
    }

    public Set<LoanPersonalFinacInfor> getLoanPersonalFinacInfors() {
        return loanPersonalFinacInfors;
    }

    public void setLoanPersonalFinacInfors(Set<LoanPersonalFinacInfor> loanPersonalFinacInfors) {
        this.loanPersonalFinacInfors = loanPersonalFinacInfors;
    }

    public List<LoanPersonalFacilityInfor> getLoanPersonalFacilityInforn() {
        return loanPersonalFacilityInforn;
    }

    public void setLoanPersonalFacilityInforn(List<LoanPersonalFacilityInfor> loanPersonalFacilityInforn) {
        this.loanPersonalFacilityInforn = loanPersonalFacilityInforn;
    }

    public double getEconomicProfit() {
        return economicProfit;
    }

    public void setEconomicProfit(double economicProfit) {
        this.economicProfit = economicProfit;
    }

    public double getBasicPayment() {
        return basicPayment;
    }

    public void setBasicPayment(double basicPayment) {
        this.basicPayment = basicPayment;
    }

    public boolean isWriteOff() {
        return writeOff;
    }

    public void setWriteOff(boolean writeOff) {
        this.writeOff = writeOff;
    }

    public String getWitness1() {
        return witness1;
    }

    public void setWitness1(String witness1) {
        this.witness1 = witness1;
    }

    public String getWitness2() {
        return witness2;
    }

    public void setWitness2(String witness2) {
        this.witness2 = witness2;
    }

    
    

   
    

    
    
   
    
    
    
}
