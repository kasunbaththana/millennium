/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.present_receipt;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.derived.display.table.CDTable;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.af.util.compare.comparison_chain.ComparisonChain;
import com.mac.loan.zobject.Loan;
import com.mac.loan.ztemplate.receipt.PCReceipt;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.settlement.object.SettlementType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;



public class PreReceipt extends CPanel {

    public PreReceipt() {
        initComponents();
        initOthers();
        try {
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (Exception ex) {
            Logger.getLogger(SERPreReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   

    @SuppressWarnings("unchecked")
    private void initOthers() {
        btnCalculate.setVisible(false);
        btnSave.setVisible(false);
        cboAgreementNo.setValueEditable(false);
        cboAgreementNo.setExpressEditable(false);
        
        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.RECEIPT));

        btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
        btnDiscard.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_DISCARD, 16, 16));
        btnCalculate.setIcon(ApplicationResources.getImageIcon(ApplicationResources.PRINT_PREVIEW, 16, 16));
        btnNew.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_OK, 16, 16));
        btnPrint.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACCOUNTING_PRINT_ICON, 16, 16));
        unEditableComponents();
        tblPaymentSchedule.setCModel(
                new CTableModel(new CTableColumn[]{
            new CTableColumn("Due Date", new String[]{"dueDate"}),
            new CTableColumn("Description", new String[]{"description"}),
            new CTableColumn("Amount", new String[]{"amount"}),
            new CTableColumn("Balance Amount", new String[]{"balanceAmount"}),
            new CTableColumn("Type", new String[]{"settlementType", "description"})
        }));


        cboAgreementNo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
//                resetFields();
                Loan loan = (Loan) cboAgreementNo.getCValue();
                
                System.out.println("cboAgreementNo.getCValue()__"+loan);
                
                double paymentAmount = getPaymentAmount(loan);
                txtLoanBalance.setCValue(paymentAmount);
                txtLoanAmount.setCValue(loan.getLoanAmount());
                txtClient.setCValue(loan.getClient().getName());
                txtBranch.setCValue(loan.getBranch());
                getTableData(loan);
                calculate();
            }
        });

        txtPaymentAmount.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
//                calculate();
            }
        });
        
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnPrint, "doPrint");
        
        this.recentButton = new RecentButton();
        
        this.recentButton.setTransactionType("RECEIPT");
        this.recentButton.setDatabseService(getDatabaseService());

    }

    
    
    
   @Action
    public void doPrint() {
        this.recentButton.doClick();
       
    }
   
   

            

    private void calculate() {
        try {
            Collection<Settlement> settlementColletion = tblPaymentSchedule.getCValue();
            boolean isExist=true;
            double total_amt=0;
//            for (Settlement settlement : settlementColletion) {
//                System.out.println("IN");
//                if(settlement.getTransactionDate().equals(txtTransactionDate.getCValue())){
//                isExist=false;
//                }
//            }
            
            Loan loan = (Loan) cboAgreementNo.getCValue();
            int dayCount = getDateCount(loan);
            txtDayCount.setCValue("" + dayCount);
            Double LoanBalance = txtLoanBalance.getCValue();
            double paymentInterst = Math.round(((LoanBalance * 0.03) / 30) * dayCount);
            System.out.println("paymentInterst__"+paymentInterst);
            txtInterstAmount.setCValue(paymentInterst);
            if (paymentInterst > 0 && isExist) {
//                txtInterstAmount.setCValue(paymentInterst);
                //-------------------------------------------------
                Settlement settlement = new Settlement();
                settlement.setDueDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                settlement.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                settlement.setDescription("Loan Interest");
                settlement.setAmount(paymentInterst);
                settlement.setBalanceAmount(paymentInterst);
                SettlementType type = (SettlementType) getDatabaseService()
                        .getObject(SettlementType.class, SystemSettlement.LOAN_INTEREST);
                settlement.setSettlementType(type);
                settlement.setStatus("NEW");

                tblPaymentSchedule.addData(settlement);
                //-------------------------------------------------

//                mOptionPane.showMessageDialog(null, "Pable Interst Amount RS. " + paymentInterst,
//                        "Calculate....", mOptionPane.INFORMATION_MESSAGE);
                    total_amt = Math.round((LoanBalance + paymentInterst));
                txtLoanBalance.setCValue(total_amt);
                System.out.println("total_amt__"+total_amt);
            }
        } catch (Exception ex) {
            Logger.getLogger(PreReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getTableData(Loan loan) {

        List<Settlement> settlementList = new ArrayList<>();
        try {
            settlementList = getDatabaseService()
                    .getCollection("FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan='" + loan.getIndexNo() + "'"
                    + " AND settlementType<>'"+SystemSettlement.LOAN_AMOUNT+"' AND status='PENDING' ");
        } catch (DatabaseException ex) {
            Logger.getLogger(PreReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
        tblPaymentSchedule.setCValue(settlementList);

    }

    private void unEditableComponents() {
        txtReferenceNo.setValueEditable(false);
        txtTransactionDate.setValueEditable(false);
        txtClient.setValueEditable(false);
        txtBranch.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
        txtAfterBalance.setValueEditable(false);
        txtInterstAmount.setValueEditable(false);
        txtDayCount.setValueEditable(false);
    }

    private void resetFields() {
        cboAgreementNo.resetValue();
        txtClient.setCValue("");
        txtBranch.setCValue("");
        txtLoanAmount.setCValue(0.0);
        txtLoanBalance.setCValue(0.0);
        txtAfterBalance.setCValue(0.0);
        txtPaymentAmount.setCValue(0.0);
        txtInterstAmount.setCValue(0.0);
        txtDayCount.setCValue("");
    }

    public List getLoans() {
        List list;
        try {
            list = getDatabaseService().getCollection("FROM com.mac.loan.zobject.Loan WHERE "
                    + " availableReceipt=true AND loanType='3PRE'");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(PreReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cboAgreementNo = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoans();
            }
        };
        txtBranch = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtNote = new com.mac.af.component.derived.input.textarea.CITextArea();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPaymentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAfterBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInterstAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtClient = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblPaymentSchedule = new com.mac.af.component.derived.display.table.CDTable();
        cDLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDayCount = new com.mac.af.component.derived.input.textfield.CIStringField();
        jPanel1 = new javax.swing.JPanel();
        btnNew = new com.mac.af.component.derived.command.button.CCButton();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        btnCalculate = new com.mac.af.component.derived.command.button.CCButton();
        btnDiscard = new com.mac.af.component.derived.command.button.CCButton();
        btnPrint = new com.mac.af.component.derived.command.button.CCButton();

        cDLabel4.setText("Transaction Date :");

        cDLabel3.setText("Document No.:");

        cDLabel2.setText("Reference No.:");

        cDLabel1.setText("Agreement No.:");

        txtNote.setColumns(20);
        txtNote.setRows(5);
        jScrollPane1.setViewportView(txtNote);

        cDLabel10.setText("Loan Amount :");

        cDLabel11.setText("Note :");

        cDLabel9.setText("Branch :");

        cDLabel7.setText("Payment Amount :");

        cDLabel8.setText("After Balance :");

        cDLabel5.setText("Client :");

        cDLabel14.setText("Interst :");

        cDLabel6.setText("Loan Balance :");

        tblPaymentSchedule.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblPaymentSchedule);

        cDLabel15.setText("Day Count :");

        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.LINE_AXIS));

        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        jPanel1.add(btnNew);

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        jPanel1.add(btnSave);

        btnCalculate.setText("Calculate");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });
        jPanel1.add(btnCalculate);

        btnDiscard.setText("Discard");
        btnDiscard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDiscardActionPerformed(evt);
            }
        });
        jPanel1.add(btnDiscard);

        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });
        jPanel1.add(btnPrint);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAfterBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPaymentAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLoanBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtClient, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtBranch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(txtInterstAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDayCount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtClient, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBranch, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLoanBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPaymentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAfterBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtInterstAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDayCount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(77, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
//        calculate();
        System.out.println("___Calculate__");
         Loan loan = (Loan) cboAgreementNo.getCValue();
         System.out.println("Loan___"+loan);
            int dayCount = getDateCount(loan);
            System.out.println("dayCount___"+dayCount);
            txtDayCount.setCValue("" + dayCount);
            Double LoanBalance = txtLoanBalance.getCValue();
            double paymentInterst = ((LoanBalance * 0.03) / 30) * dayCount;
             mOptionPane.showMessageDialog(null, "..Pable Interst Amount RS. " + paymentInterst,
                        "Calculate....", mOptionPane.INFORMATION_MESSAGE);
//                txtInterstAmount.setCValue(paymentInterst);

    }//GEN-LAST:event_btnCalculateActionPerformed

    private void btnDiscardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDiscardActionPerformed
        TabFunctions.closeTab(this);
    }//GEN-LAST:event_btnDiscardActionPerformed
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:

        int isOk = mOptionPane.showConfirmDialog(null, "Are you sure want to save changes ?",
                "Save", mOptionPane.YES_NO_OPTION);
        if (isOk == 0) {
            serPreReceipt = new SERPreReceipt(this);
            String loanxxx = null;
            try {
//            if (txtInterstAmount.getCValue() > 0) {

                Loan loan = (Loan) cboAgreementNo.getCValue();
                
                PReceipt pReceipt = new PReceipt();
                pReceipt.setDocumentNo(txtDocumentNo.getCValue());
                pReceipt.setLoan(loan);
                pReceipt.setNote(txtNote.getCValue());
                pReceipt.setPaymentAmount(txtPaymentAmount.getCValue());
                pReceipt.setReferenceNo(txtReferenceNo.getCValue());
                pReceipt.setTransactionDate(txtTransactionDate.getCValue());

                boolean isPaymentOk = showPaymentDialog(SystemTransactions.RECEIPT_TRANSACTION_CODE, pReceipt);
                if (isPaymentOk) {
                    //save Code here
                    //SAVE INTERST AMOUNT INSTANCE   
                    saveInterstInstancely(loan, tblPaymentSchedule);
                    loanxxx = "" + loan.getIndexNo();

                    //LOAD FROM DATANASE AND SET HISTORIES
                    List<SettlementHistory> settlementHistoriesx = new ArrayList<>();
                    double pay = txtPaymentAmount.getCValue();
                    if(pay==0)
                {
                    pReceipt.setDocumentNo("ZERO_RECEIPT");
                }
                    List<Settlement> settlements = getDatabaseService()
                            .getCollection("FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan='" + loan.getIndexNo() + "'"
                    + " AND settlementType<>'"+SystemSettlement.LOAN_AMOUNT+"' AND status='PENDING' ");
                    sortReceiptReceipts(settlements);

                    for (Settlement settlement : settlements) {
                        SettlementHistory settlementHistory = new SettlementHistory();
                        settlementHistory.setIndexNo(null);
                        settlementHistory.setSettlement(settlement);
                        settlementHistoriesx.add(settlementHistory);
                    }
                    //save account part
                    
                    int  transactionIndex = serPreReceipt.saveReceipt(
                            SystemTransactions.RECEIPT_TRANSACTION_CODE,
                            pReceipt,
                            cashierSession,
                            systemPayment);

                   double paymentAmount = pay;
                    for (SettlementHistory setHistory : settlementHistoriesx) {
                        double settlementAmount = 0;
                        setHistory.setTransaction(transactionIndex);
                        setHistory.setTransactionType("RECEIPT");
                        setHistory.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                        setHistory.setStatus("ACTIVE");
                        setHistory.setBeforeBalance(setHistory.getSettlement().getBalanceAmount());
//                        System.out.println("paymentAmount____"+paymentAmount);
                        
                        if (paymentAmount != 0) {
//                            settlementAmount = 0;
//                            setHistory.getSettlement().setBalanceAmount(0.0);
//                            setHistory.setSettlementAmount(settlementAmount);
//                            System.out.println("setHistory.getSettlement().getBalanceAmount()--"+setHistory.getSettlement().getBalanceAmount());
                        if (setHistory.getSettlement().getBalanceAmount() == paymentAmount) {
//                            System.out.println(setHistory.getSettlement().getIndexNo()+"________11111111");
                            settlementAmount =settlementAmount + paymentAmount;
                            setHistory.getSettlement().setBalanceAmount(0.0);
                            setHistory.setSettlementAmount(settlementAmount);
                            setHistory.getSettlement().setStatus("SETTLED");
//                            System.out.println(paymentAmount+"settlementAmountXXXXXXXXXXXX_______"+settlementAmount);
//                            paymentAmount = 0.0;
                        }
                        
                        //error
                        else if (setHistory.getSettlement().getBalanceAmount() > paymentAmount && paymentAmount>0) {
//                            System.out.println(setHistory.getSettlement().getIndexNo()+"___________22222222222");
                            double pp = setHistory.getSettlement().getBalanceAmount() - paymentAmount;
//                            System.out.println("pp_"+pp);
                            setHistory.getSettlement().setBalanceAmount(pp);
//                            setHistory.getSettlement().setDescription("xxxx");
                            settlementAmount = paymentAmount;
                            paymentAmount = 0.0;
                            setHistory.setSettlementAmount(settlementAmount);
//                            System.out.println(paymentAmount+"settlementAmountXXXXXXXXXXXX_______"+settlementAmount);
                        }
                        else if (setHistory.getSettlement().getBalanceAmount() < paymentAmount) {
//                            System.out.println(setHistory.getSettlement().getIndexNo()+"___________3333333333333");
                            settlementAmount = setHistory.getSettlement().getBalanceAmount();
                            paymentAmount = paymentAmount - setHistory.getSettlement().getBalanceAmount();
                            setHistory.getSettlement().setBalanceAmount(0.0);
                            setHistory.setSettlementAmount(settlementAmount);
                            setHistory.getSettlement().setStatus("SETTLED");
//                            System.out.println(paymentAmount+"settlementAmountXXXXXXXXXXXX_______"+settlementAmount);
                        }

//                        System.out.println("paymentAmount__" + paymentAmount);
                         }
                        getDatabaseService().beginLocalTransaction();
                        getDatabaseService().save(setHistory.getSettlement());
                        getDatabaseService().commitLocalTransaction();
                        getDatabaseService().save(setHistory);
                        getDatabaseService().commitLocalTransaction();
                    }

                    //REPORT
                    Map<String, Object> params = new HashMap<>();
                    params.put("TRANSACTION_NO", transactionIndex);
                    TransactionUtil.PrintReport(getDatabaseService(), SystemTransactions.RECEIPT_TRANSACTION_CODE, params);

                }

//            } else {
//                mOptionPane.showMessageDialog(null, "Please Calculate Interst Amount",
//                        "Calculate....", mOptionPane.WARNING_MESSAGE);
//
//            }


                mOptionPane.showMessageDialog(null, "Successfully Saved !",
                        "Data Saving....", mOptionPane.INFORMATION_MESSAGE);


                resetFields();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//         btnCalculate.setVisible(false);
//        btnSave.setVisible(false);
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
       
        ReferenceGenerator ss=new ReferenceGenerator("REC");
        txtReferenceNo.setCValue(ss.getValue());
        
        
        btnCalculate.setVisible(true);
        btnSave.setVisible(true);
        resetFields();
     
        cboAgreementNo.setValueEditable(true);
        cboAgreementNo.setExpressEditable(false);
        
//        btnNew.setVisible(false);
    }//GEN-LAST:event_btnNewActionPerformed
   
    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPrintActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnCalculate;
    private com.mac.af.component.derived.command.button.CCButton btnDiscard;
    private com.mac.af.component.derived.command.button.CCButton btnNew;
    private com.mac.af.component.derived.command.button.CCButton btnPrint;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAgreementNo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private com.mac.af.component.derived.display.table.CDTable tblPaymentSchedule;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAfterBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtBranch;
    private com.mac.af.component.derived.input.textfield.CIStringField txtClient;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDayCount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtInterstAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanBalance;
    private com.mac.af.component.derived.input.textarea.CITextArea txtNote;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtPaymentAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables

    private void saveInterstInstancely(Loan loan, CDTable cDTable) throws Exception {
        Collection<Settlement> cValue = cDTable.getCValue();
        for (Settlement settlementt : cValue) {
            if (settlementt.getStatus().equals("NEW")) {
//                        SystemSettlement systemSettlement = SystemSettlement.getInstance();
//                        systemSettlement.beginSettlementQueue();

                Integer transaction = SystemTransactions.insertTransaction(
                        getDatabaseService(),
                        SystemTransactions.LOAN_TRANSACTION_CODE,
                        ReferenceGenerator.getInstance(ReferenceGenerator.LOAN).getValue(),
                        loan.getLoanDocumentNo(),
                        loan.getIndexNo(),
                        null,
                        loan.getClient().getCode(),
                        "note");

                getDatabaseService().beginLocalTransaction();
                Settlement s = new Settlement();
                s.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                s.setBranch(loan.getBranch());
                s.setClient(loan.getClient().getCode());
                s.setLoan(loan.getIndexNo());
                s.setInstallmentNo(0);
                s.setTransaction(transaction);
                s.setTransactionType("LOAN");
                s.setDescription("Loan Interst");
                s.setAmount(settlementt.getAmount());
                s.setBalanceAmount(settlementt.getAmount());
                s.setDueDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                SettlementType type = (SettlementType) getDatabaseService()
                        .getObject(SettlementType.class, SystemSettlement.LOAN_INTEREST);
                s.setSettlementType(type);
                s.setStatus("PENDING");

                getDatabaseService().save(s);
                getDatabaseService().commitLocalTransaction();
            }
        }
    }

    public boolean showPaymentDialog(String transactionTypeCode, PReceipt receipt) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getTransactionDate(),
                receipt.getPaymentAmount(),
                ChequeType.CLIENT);

        return isPaymentOk;
    }

    public void sortReceiptReceipts(List<Settlement> settlements) {
        List<Settlement> interstes = new ArrayList<>();
        for (Settlement interst : settlements) {
            if (interst.getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST)) {
                interstes.add(interst);
            }
        }
        settlements.removeAll(interstes);

        Collections.sort(settlements, new Comparator<Settlement>() {
            @Override
            public int compare(Settlement o1, Settlement o2) {
                return ComparisonChain.start()
                        .compare(o1.getSettlementType().getPriority(), o2.getSettlementType().getPriority())
                        .result();
            }
        });
        settlements.addAll(0, interstes);
    }

    public void saveSettlement(String transactionTypeCode, int transactionIndex, List<SettlementHistory> settlementHistories) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            systemSettlement.addSettlementHistoryQueue(
                    settlementHistory.getSettlement(),
                    transactionTypeCode,
                    transactionIndex,
                    settlementHistory.getSettlementAmount());
        }
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
    }

    private double getPaymentAmount(Loan loan) {
        double paymentAmount = 0;
        try {
            List<Settlement> settlements = getDatabaseService()
                    .getCollection("FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan='" + loan.getIndexNo() + "' and status='PENDING' and settlementType<>'"+SystemSettlement.LOAN_AMOUNT+"' ");
            for (Settlement settlement : settlements) {
                paymentAmount = paymentAmount + settlement.getBalanceAmount();
                 System.out.println("payment__"+paymentAmount);
            }
            System.out.println("paymentAmount__"+paymentAmount);
            return paymentAmount;
        } catch (Exception ex) {
            Logger.getLogger(PreReceipt.class.getName()).log(Level.SEVERE, null, ex);
            return 0.0;
        }
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }
    int dateCount = 0;
    Date duedate = null;

    private int getDateCount(Loan loan) {
      Date currentDate = null;
        try {
           currentDate=  (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE); 
           
            String sqlx="SELECT\n"
                    + "DATEDIFF('" + currentDate + "', max(settlement.due_date)) as datecount,\n"
                    + "  max(settlement.due_date) as monthx\n"
                    + "FROM settlement left join settlement_type on settlement.settlement_type=settlement_type.code\n"
                    + "where settlement.loan='" + loan.getIndexNo() + "' and settlement_type.credit_or_debit='CREDIT'";
            System.out.println(sqlx);
            
            ResultSet executeQuery = getConnection()
                    .createStatement()
                    .executeQuery(sqlx);

            while (executeQuery.next()) {
                dateCount = executeQuery.getInt(1);
                duedate=executeQuery.getDate(2);
            }
            if (dateCount < 0) {
                dateCount = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(duedate.getMonth()==currentDate.getMonth())
        {
            return dateCount;
        }
        else
        {
            dateCount=getdate(duedate, currentDate, dateCount);
        }
          
//        if(currentDate.getMonth()==2 ){
//        if(currentDate.getDay()== duedate.getDay()){
//        dateCount=30;
//        }
//        
//        }else if(dateCount>30 && dateCount<60){
//        dateCount=dateCount-1;
//        
//        }else if(dateCount==61 ){
//        dateCount=dateCount-1;
//        
//        }else if(dateCount>61 && dateCount<90){
//        dateCount=dateCount-2;
//        
//        }else if(dateCount==91 ){
//        dateCount=dateCount-1;
//        
//        }else if(dateCount>91 && dateCount<90){
//        dateCount=dateCount-2;
//        }
        
        
        
            
        return dateCount;
    }
    
    

public int getdate(Date duedate,Date currentdate,int datecount)
    {
//        System.out.println("duedate__"+duedate);
    Calendar startCalendar = new GregorianCalendar();
    startCalendar.setTime(duedate);
    Calendar endCalendar = new GregorianCalendar();
    endCalendar.setTime(currentdate);
    Date date=null;
    int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
    int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
    int count31=0;
//        System.out.println("datecount__1"+datecount);
    for (int i=1;i<=datecount;i++)
    {
        
        startCalendar.add(Calendar.DATE, 1);
        date = startCalendar.getTime();
//        System.out.println("date"+date);
            Date dateds=date;
//        System.out.println("startCalendar.get(Calendar.DATE)"+startCalendar.get(Calendar.DATE));
        if(startCalendar.get(Calendar.DATE)==31)
        {
           
        }
        else
        {
            count31+=1;
        }
//        System.out.println("count31__"+count31);
    }
    return count31;
    }
    
    SERPreReceipt serPreReceipt;
    private CashierSession cashierSession;
    private SystemPayment systemPayment;
    private RecentButton recentButton;
}
