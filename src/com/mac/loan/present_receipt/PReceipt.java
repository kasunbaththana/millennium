/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.present_receipt;

import com.mac.loan.zobject.Loan;
import java.util.Date;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class PReceipt {
    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private String note;
    //
    private Loan loans;
    private Double paymentAmount;


    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Loan getLoan() {
        return loans;
    }

    public void setLoan(Loan loan) {
        this.loans = loan;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
}
