/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.present_receipt;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ReceiptAccountInterface;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.Component;

/**
 *
 * @author NIMESH-PC
 */
public class SERPreReceipt extends AbstractService {

    public SERPreReceipt(Component component) {
        super(component);
    }
    
     
    protected String getReferenceGeneratorType() {
        return ReferenceGenerator.RECEIPT;
    }

    protected String getTransactionTypeCode() {
        return SystemTransactions.RECEIPT_TRANSACTION_CODE;
    }

    protected String getSettlementCreitOrDebit() {
        return SettlementCreditDebit.CREDIT;
    }

    protected String getChequeType() {
        return ChequeType.CLIENT;
    }

    protected String getAccountSettingCode() {
        return ReceiptAccountInterface.RECEIPT_AMOUNT_CREDIT_CODE;
    }
    
     public int saveReceipt(final String transactionTypeCode, PReceipt receipt,
             CashierSession cashierSession,SystemPayment systemPayment) throws DatabaseException {

            //SAVE TRANSACTION
            final int transactionIndex = saveTransaction(transactionTypeCode, receipt,cashierSession);

//            //SAVE SETTLEMENT
//            saveSettlement(transactionTypeCode, receipt, transactionIndex);

//            //SAVE OVER AMOUNT
//            saveOverAmount(receipt, transactionTypeCode, transactionIndex);

            //SAVE PAYMENT
            savePayment(transactionIndex, receipt,cashierSession, systemPayment);

            //ACCOUNT TRANSACTION
            saveAccount(transactionIndex, transactionTypeCode, receipt);

            

            return transactionIndex;
    }
     
   

    public void savePayment(int transactionIndex, PReceipt receipt,
            CashierSession cashierSession,SystemPayment systemPayment) throws DatabaseException {
        systemPayment.savePayment(
                getDatabaseService(),
                cashierSession,
                transactionIndex,
                receipt.getLoan().getClient().getCode());
    }

    public int saveTransaction(String transactionTypeCode, PReceipt receipt,
            CashierSession cashierSession) throws DatabaseException {
        //SAVE TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                transactionTypeCode,
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getLoan().getIndexNo(),
                cashierSession.getIndexNo(),
                receipt.getLoan().getClient().getCode(),
                receipt.getNote());

        return transactionIndex;
    }

    public void saveAccount(int transactionIndex, String transactionTypeCode, PReceipt receipt) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        accountTransaction.addAccountTransactionQueue(getAccountSettingCode(), transactionTypeCode + " Amount", receipt.getPaymentAmount(), AccountTransactionType.AUTO);
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, transactionTypeCode);
    }
    
    
    
    
    
    
    
    
    
    
   
}
