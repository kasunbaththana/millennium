/*
 *  TRALoan.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jun 25, 2014, 1:00:55 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.loan;

import com.mac.af.component.base.button.action.Action;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.loan.LoanStatus;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.transaction.SystemTransactions;

/**
 *
 * @author mohan
 */
public class TRALoan extends AbstractGridObject {

    public TRALoan() {
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.LOAN_TRANSACTION_CODE);
        this.recentButton.setDatabseService(getDatabaseService());
    }

    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Index No", new String[]{"indexNo"}),
            new CTableColumn("Req No", new String[]{"applicationDocumentNo"}),
            new CTableColumn("Client", new String[]{"client", "name"}),
            new CTableColumn("Group", new String[]{"client", "group_name"}),
            new CTableColumn("Agreement No.", new String[]{"agreementNo"}),
            new CTableColumn("Loan Group", new String[]{"loanGroup", "name"}),
            new CTableColumn("Loan Type", new String[]{"loanType", "name"}),
//            new CTableColumn("Referance No", new String[]{"loanReferenceNo"}),
            new CTableColumn("Loan Amount", new String[]{"loanAmount"}),
            new CTableColumn("Installment Amount", new String[]{"installmentAmount"}),
            new CTableColumn("Installment Count", new String[]{"installmentCount"}),
            new CTableColumn("Interest Rate", new String[]{"interestRate"})
        });
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        return new PCLoan();
    }

    @Override
    protected Collection getTableData() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status", LoanStatus.APPLICATION_ACCEPT);
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Loan where status=:status", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(TRALoan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
    }

    private RecentButton recentButton;

}
