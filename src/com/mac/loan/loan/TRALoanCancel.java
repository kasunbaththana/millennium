/*
 *  TRALoanCancel.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 5:16:09 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.loan;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.loan.LoanStatus;
import com.mac.loan.loan_advance_payment.object.LoanAdvanceSettlement;
import com.mac.loan.zobject.LoanAdvanceTrans;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction_cancel.TransactionCancel;
import com.mac.zsystem.transaction.transaction_cancel.object.Transaction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mohan
 */
public class TRALoanCancel extends TransactionCancel {

    @Override
    protected String getTransactionType() {
        return SystemTransactions.LOAN_TRANSACTION_CODE;
    }

    @Override
    protected void cancelAdditional(Transaction transaction) throws DatabaseException {
        SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.TRANSACTION_CANCEL_TRANSACTION_CODE,
                transaction.getReferenceNo(),
                transaction.getDocumentNo(),
                transaction.getLoan().getIndexNo(),
                null,
                transaction.getLoan().getClient(),
                "Loan Cancel");
        
        LoanAdvanceTrans advanceTrans = (LoanAdvanceTrans) CPanel.GLOBAL.getDatabaseService().initCriteria(LoanAdvanceTrans.class)
                        .add(Restrictions.eq("loan", transaction.getLoan().getIndexNo())).uniqueResult();
            if(advanceTrans!=null){
                
            getDatabaseService().delete(advanceTrans);
            }
            
         LoanAdvanceSettlement advanceSettlement = (LoanAdvanceSettlement) CPanel.GLOBAL.getDatabaseService().initCriteria(LoanAdvanceSettlement.class)
                        .add(Restrictions.eq("loan", transaction.getLoan())).uniqueResult();   
         
         if(advanceSettlement!=null){
            advanceSettlement.setStatus("CANCEL");    
            getDatabaseService().update(advanceSettlement);
            }
        

        transaction.getLoan().setStatus(LoanStatus.APPLICATION_PENDING);
        transaction.getLoan().setAgreementNo(null);
        transaction.getLoan().setAvailableReceipt(false);
        transaction.getLoan().setAvailableVoucher(false);

        getDatabaseService().save(transaction.getLoan());
    }

//    @Override
//    protected Collection getTableData() {
//        try {
//            String sql = "SELECT \n"
//                    + "	tra_1.*\n"
//                    + "FROM \n"
//                    + "	transaction AS tra_1\n"
//                    + "WHERE\n"
//                    + "	(SELECT COUNT(*) FROM transaction AS tra_2 WHERE tra_2.loan=tra_1.loan AND (tra_2.transaction_type='RECEIPT' OR tra_2.transaction_type='VOUCHER') AND tra_2.status='ACTIVE') =0\n"
//                    + "	AND tra_1.status='ACTIVE'\n"
//                    + "	AND tra_1.transaction_type = 'LOAN'";
//            HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(sql, Transaction.class);
//            return getDatabaseService().getCollection(hibernateSQLQuery, null);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(TRALoanCancel.class.getName()).log(Level.SEVERE, null, ex);
//            return new ArrayList();
//        }
//
//    }
    @Override
    protected Collection getTableData() {
//        String ltype = "LOAN";
//        List<Transaction> transaction;
//        try {
//            String sql = "SELECT \n"
//                    + "	tra_1.*\n"
//                    + "FROM \n"
//                    + "	transaction AS tra_1\n"
//                    + "WHERE\n"
//                    + "	(SELECT COUNT(*) FROM transaction AS tra_2 WHERE tra_2.loan=tra_1.loan AND (tra_2.transaction_type='RECEIPT' OR tra_2.transaction_type='VOUCHER') AND tra_2.status='ACTIVE') =0\n"
//                    + "	AND tra_1.status='ACTIVE'\n"
//                    + "	AND tra_1.transaction_type = 'LOAN'";
//            HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(sql, Transaction.class);
//            transaction = getDatabaseService().getCollection("FROM com.mac.zsystem.transaction.transaction.object.Transaction WHERE status=:1ACTIVE ", null);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(TRALoanCancel.class.getName()).log(Level.SEVERE, null, ex);
//            return new ArrayList();
//        }

        List<Transaction> transaction;
        try {
            HashMap params = new HashMap<>();
            params.put("STATUS", "ACTIVE");
            params.put("TRANSACTION_TYPE", "LOAN");
            transaction = getDatabaseService().getCollection("from com.mac.zsystem.transaction.transaction_cancel.object.Transaction where status=:STATUS AND transactionType=:TRANSACTION_TYPE", params);
        } catch (DatabaseException ex) {
            transaction = new ArrayList();
            Logger.getLogger(TransactionCancel.class.getName()).log(Level.SEVERE, null, ex);
        }
        // return list;
        return transaction;
    }
}
