/*
 *  AbstractRebit.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 13, 2014, 9:45:38 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.rebit;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.templates.grid_selection.AbstractGridSelection;
import com.mac.af.util.compare.comparison_chain.ComparisonChain;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author mohan
 */
public abstract class AbstractRebit extends AbstractGridSelection<Settlement> {

    protected abstract List<Settlement> getSettlements();

    @Override
    protected Collection<Settlement> getLeftData() {
        List<Settlement> settlements = getSettlements();

        sortReceiptReceipts(settlements);

        return settlements;
    }

    public void sortReceiptReceipts(List<Settlement> settlementHistory) {
        Collections.sort(settlementHistory, new Comparator<Settlement>() {
            @Override
            public int compare(Settlement o1, Settlement o2) {
                return ComparisonChain.start()
                        .compare(o1.getDueDate(), o2.getDueDate())
                        .compare(o1.getSettlementType().getPriority(), o2.getSettlementType().getPriority())
                        .result();
            }
        });
    }

    @Override
    protected CTableModel<Settlement> getLeftTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Due Date", new String[]{"dueDate"}),
            new CTableColumn("Description", new String[]{"description"}),
            new CTableColumn("Amount", new String[]{"amount"}),
            new CTableColumn("Balance Amount", new String[]{"balanceAmount"}),
            new CTableColumn("Type", new String[]{"settlementType", "description"})
        });
    }

    @Override
    protected CTableModel<Settlement> getRightTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Due Date", new String[]{"dueDate"}),
            new CTableColumn("Description", new String[]{"description"}),
            new CTableColumn("Amount", new String[]{"amount"}),
            new CTableColumn("Balance Amount", new String[]{"balanceAmount"}),
            new CTableColumn("Type", new String[]{"settlementType", "description"})
        });
    }

    @Override
    protected void afterLeft(Collection<Settlement> leftData) {
        if (leftData instanceof List) {
            sortReceiptReceipts((List) leftData);
        }
    }

    @Override
    protected void afterRight(Collection<Settlement> rightData) {
        if (rightData instanceof List) {
            sortReceiptReceipts((List) rightData);
        }
    }
}
