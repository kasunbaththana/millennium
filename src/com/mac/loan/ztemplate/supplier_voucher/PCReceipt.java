/*
 *  PCReceipt.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 6, 2014, 3:25:36 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.supplier_voucher;

import com.mac.af.component.derived.input.combobox.CIComboBox;
import com.mac.af.core.environment.CApplication;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.zobject.Client;
import com.mac.loan.zobject.Loan;
import com.mac.loan.ztemplate.supplier_voucher.custom_object.Receipt;
import com.mac.zsystem.model.table_model.recept_table_model.ReceiptTableModel;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mohan
 */
public abstract class PCReceipt extends AbstractObjectCreator<Receipt> {

    /**
     * Creates new form PCReceipt
     */
    public PCReceipt(SERAbstractReceipt serReceipt) {
        this.serReceipt = serReceipt;
        initComponents();

        initOthers();
    }

    protected abstract String getReferenceGeneratorType();

    public Client getSelectedLoan(CIComboBox comboBox) {
        return (Client) comboBox.getCValue();
    }

    public List getSupplier() {
        return serReceipt.getSupplier();
    }

    public boolean isPaymentAmountFixed() {
        return false;
    }

    public Double getDefaultPaymentAmount(CIComboBox comboBox) {
        return 0.0;
    }

    private void newReceipt() {
        receipt = new Receipt();

        txtReferenceNo.resetValue();
        txtDocumentNo.setCValue(null);
        txtTransactionDate.setCValue((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));

        loanSelected();
    }

    private void loanSelected() {
        Client supplier = getSelectedLoan(cboSupplier);
        if (supplier != null) {
            serReceipt.initSettlementHistory(supplier);

            double totalLoanAmount = 0.0;
            double totalInstallmentAmount = 0.0;
            double totalBalanceAmount = 0.0;
            double totalAfterBalance = 0.0;
            for (Loan loan : supplier.getLoans()) {
                totalLoanAmount += loan.getLoanAmount();
                totalInstallmentAmount += loan.getInstallmentAmount();

            }
            totalBalanceAmount = serReceipt.getBalanceAmount();
            totalAfterBalance = serReceipt.getAfterBalanceAmount();

//            txtBranch.setCValue(loan.getBranch());

            txtLoanAmount.setCValue(totalLoanAmount);
            txtInstallmentAmount.setCValue(totalInstallmentAmount);
            txtLoanBalance.setCValue(totalBalanceAmount);
            txtAfterBalance.setCValue(totalAfterBalance);

            txtPaymentAmount.setCValue(getDefaultPaymentAmount(cboSupplier));
            resetPaymentAmount();
        } else {
            txtBranch.setCValue(null);
            txtLoanAmount.setCValue(null);
            txtInstallmentAmount.setCValue(null);
            txtLoanBalance.setCValue(null);
            txtPaymentAmount.setCValue(null);
            txtAfterBalance.setCValue(null);

            txtPaymentAmount.setCValue(0.0);
            resetPaymentAmount();
        }



    }

    private void resetPaymentAmount() {
        Double paymentAmount = txtPaymentAmount.getCValue();
        serReceipt.setSettlementAmount(paymentAmount);

        txtAfterBalance.setCValue(serReceipt.getAfterBalanceAmount());
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        cboSupplier.setExpressEditable(true);
        cboSupplier.setTableModel(new ReceiptTableModel());
        txtPaymentAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetPaymentAmount();
            }
        });

        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(getReferenceGeneratorType()));
        cboSupplier.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                loanSelected();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cboSupplier = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getSupplier();
            }
        };
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPaymentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAfterBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBranch = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtNote = new com.mac.af.component.derived.input.textarea.CITextArea();
        cDLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInstallmentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();

        cDLabel1.setText("Supplier :");

        cDLabel2.setText("Reference No.:");

        cDLabel3.setText("Document No.:");

        cDLabel4.setText("Transaction Date :");

        cDLabel6.setText("Total Loan Balance :");

        cDLabel7.setText("Total Payment Amount :");

        cDLabel8.setText("Total After Balance :");

        cDLabel9.setText("Branch :");

        cDLabel10.setText("Total Loan Amount :");

        cDLabel11.setText("Note :");

        txtNote.setColumns(20);
        txtNote.setRows(5);
        jScrollPane1.setViewportView(txtNote);

        cDLabel12.setText("Total Installment Amount :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAfterBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPaymentAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLoanBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtBranch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboSupplier, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBranch, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPaymentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAfterBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboSupplier;
    private javax.swing.JScrollPane jScrollPane1;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAfterBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtBranch;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtInstallmentAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanBalance;
    private com.mac.af.component.derived.input.textarea.CITextArea txtNote;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtPaymentAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables
    private Receipt receipt;
    private SERAbstractReceipt serReceipt;

    @Override
    public void setNewMood() {
        cboSupplier.setValueEditable(true);
        txtBranch.setValueEditable(false);
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(true);
        txtTransactionDate.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
        txtInstallmentAmount.setValueEditable(false);
        txtPaymentAmount.setValueEditable(!isPaymentAmountFixed());
        txtAfterBalance.setValueEditable(false);
        txtNote.setValueEditable(true);

        newReceipt();
    }

    @Override
    public void setEditMood() {
        cboSupplier.setValueEditable(true);
        txtBranch.setValueEditable(false);
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(true);
        txtTransactionDate.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
        txtInstallmentAmount.setValueEditable(false);
        txtPaymentAmount.setValueEditable(!isPaymentAmountFixed());
        txtAfterBalance.setValueEditable(false);
        txtNote.setValueEditable(true);
    }

    @Override
    public void setIdleMood() {
        cboSupplier.setValueEditable(false);
        txtBranch.setValueEditable(false);
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(false);
        txtTransactionDate.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
        txtInstallmentAmount.setValueEditable(false);
        txtPaymentAmount.setValueEditable(false);
        txtAfterBalance.setValueEditable(false);
        txtNote.setValueEditable(false);
    }

    @Override
    public void resetFields() {
        cboSupplier.resetValue();
        txtBranch.resetValue();
        txtReferenceNo.resetValue();
        txtDocumentNo.resetValue();
        txtTransactionDate.resetValue();
        txtLoanAmount.resetValue();
        txtLoanBalance.resetValue();
        txtPaymentAmount.resetValue();
        txtAfterBalance.resetValue();
    }

    @Override
    protected void setValueAbstract(Receipt value) throws ObjectCreatorException {
        //DO NOTHING
        //NO OUTSIDE VALUE ACCEPTED
    }

    @Override
    protected Receipt getValueAbstract() throws ObjectCreatorException {
        return receipt;
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        //DO NOTHING
        //NO OUTSIDE VALUE ACCEPTED
    }

    @Override
    protected void initObject() throws ObjectCreatorException {
        receipt.setReferenceNo(txtReferenceNo.getCValue());
        receipt.setDocumentNo(txtDocumentNo.getCValue());
        receipt.setTransactionDate(txtTransactionDate.getCValue());
        receipt.setNote(txtNote.getCValue());

        Client client = (Client) cboSupplier.getCValue();
        
//        receipt.setLoan(getSelectedLoan(cboAgreementNo));
        receipt.setLoans(client.getLoans_clnt());
        receipt.setPaymentAmount(txtPaymentAmount.getCValue());
    }
}
