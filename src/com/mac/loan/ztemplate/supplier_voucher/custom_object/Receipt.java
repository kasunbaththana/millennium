/*
 *  Receipt.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 6, 2014, 3:13:07 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.supplier_voucher.custom_object;

import com.mac.loan.zobject.Client;
import com.mac.loan.zobject.Loan;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author mohan
 */
public class Receipt {

    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private String note;
    private String DES;
    //
    private Client client;
    private Collection<Loan> loans;
    private Double paymentAmount;
    

    public Receipt() {
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getNote() {
        return note;
    }

    public String getDES() {
        return DES;
    }

    public void setDES(String DES) {
        this.DES = DES;
    }
    

    public void setNote(String note) {
        this.note = note;
    }

    public Collection<Loan> getLoans() {
        return loans;
    }

    public void setLoans(Collection<Loan> loan) {
        this.loans = loan;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
}
