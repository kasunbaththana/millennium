/*
 *  SERAbstractReceipt.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Sep 29, 2014, 9:51:39 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.supplier_voucher;

import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.af.util.compare.comparison_chain.ComparisonChain;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Client;
import com.mac.loan.zobject.Loan;
import com.mac.loan.ztemplate.supplier_voucher.custom_object.Receipt;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SupplierPaymentAccountInterface;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mohan
 */
public abstract class SERAbstractReceipt extends AbstractService {

    public SERAbstractReceipt(Component component) {
        super(component);
        try {
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException | DatabaseException ex) {
            Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public abstract List<Client> getSupplier();

    protected abstract void loanChanged();

    protected abstract String getSettlementCreitOrDebit();

    protected abstract String getChequeType();

    protected abstract String getAccountSettingCode();

    protected abstract String getOverAmountSettlementType();

    protected abstract String getTransactionName();

    protected abstract Loan getModifiedLoan(Loan loan, Double afterBalance);

    public int saveReceipt(final String transactionTypeCode, Receipt receipt) throws DatabaseException {
        Set<Integer> loanNumbers = new HashSet<>();
        double down_payment = 0.0, loan_amount = 0.0;
        String concat_AGR ="" ,cocat_AGR2=""; 
        for (SettlementHistory settlementHistory : settlementHistories) {
            if (settlementHistory.getSettlementAmount() > 0.0) {
                loanNumbers.add(settlementHistory.getSettlement().getLoan());

                if (settlementHistory.getSettlement().getDescription().equals("Down Payment")) {
                    down_payment += settlementHistory.getSettlementAmount();
                     concat_AGR +=settlementHistory.getSettlement().getLoan()+",";
                } else {
                    loan_amount += settlementHistory.getSettlementAmount();
                    cocat_AGR2 +=settlementHistory.getSettlement().getLoan()+",";
                }

            }
        }

        Map<Integer, Loan> loanMap = new HashMap<>();
        Map<Integer, Set<SettlementHistory>> settlementHistoryMap = new HashMap();
        for (Integer integer : loanNumbers) {

            for (Loan loan : receipt.getLoans()) {
                if (loan.getIndexNo().equals(integer)) {
                    loanMap.put(integer, loan);
                   
                }
            }

            Set<SettlementHistory> settlements = new HashSet();
            for (SettlementHistory settlementHistory : settlementHistories) {
                if (settlementHistory.getSettlement().getLoan().equals(integer)) {
                    settlements.add(settlementHistory);
                }
            }
            settlementHistoryMap.put(integer, settlements);
        }

        boolean isPaymentOK = showPaymentDialog(transactionTypeCode, receipt);

        if (isPaymentOK) {
            //save group loan transaction
            final int transactionIndex = saveTransaction(transactionTypeCode, receipt);

            //save payment for the group loan
            savePayment(transactionIndex, receipt);

            saveAccount(transactionIndex, transactionTypeCode, down_payment, loan_amount,concat_AGR,cocat_AGR2);

            for (Integer integer : settlementHistoryMap.keySet()) {
                //save transaction
//                int receiptTransaction = saveTransaction2(transactionTypeCode, receipt, loanMap.get(integer));

                //save settlement
                saveSettlement2(settlementHistoryMap.get(integer), transactionTypeCode, receipt, transactionIndex);

                //save over amount

                //save payment

                //save account transaction

                //save loan
                saveLoan(loanMap.get(integer));
            }

            Map<String, Object> params = new HashMap<>();
            params.put("TRANSACTION_NO", transactionIndex);
            TransactionUtil.PrintReport(getDatabaseService(), SystemTransactions.SUPPLIER_PAYMENT, params);


            return AbstractRegistrationForm.SAVE_SUCCESS;
        } else {
            return AbstractRegistrationForm.SAVE_FAILED;
        }


    }

    private int saveTransaction2(String transactionTypeCode, Receipt receipt, Loan loan) throws DatabaseException {
        System.out.println("transactionTypeCode___" + transactionTypeCode);
        //SAVE TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                transactionTypeCode,
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                loan.getIndexNo(),
                cashierSession.getIndexNo(),
                loan.getSupplier().getCode(),
                receipt.getNote());

        return transactionIndex;
    }

    public void saveSettlement2(Collection<SettlementHistory> settlementHistories, String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            systemSettlement.addSettlementHistoryQueue(
                    settlementHistory.getSettlement(),
                    transactionTypeCode,
                    transactionIndex,
                    settlementHistory.getSettlementAmount());
        }
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
    }

    public void saveAccount2(int transactionIndex, String transactionTypeCode, Receipt receipt) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        accountTransaction.addAccountTransactionQueue(getAccountSettingCode(), getTransactionName() + " Amount", receipt.getPaymentAmount(), AccountTransactionType.AUTO);
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, transactionTypeCode);
    }

    public void saveLoan2(Loan loan) throws DatabaseException {
        loan = getModifiedLoan(loan, getAfterBalanceAmount());
        getDatabaseService().save(loan);
    }

    public boolean showPaymentDialog(String transactionTypeCode, Receipt receipt) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getTransactionDate(),
                receipt.getPaymentAmount(),
                getChequeType());

        return isPaymentOk;
    }

    public void savePayment(int transactionIndex, Receipt receipt) throws DatabaseException {
        systemPayment.savePayment(
                getDatabaseService(),
                cashierSession,
                transactionIndex,
                null);
//                receipt.getLoan().getClient().getCode());
    }

    public int saveTransaction(String transactionTypeCode, Receipt receipt) throws DatabaseException {

        //SAVE TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                transactionTypeCode,
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                null,//receipt.getLoan(),
                cashierSession.getIndexNo(),
                null,//receipt.getLoan().getClient().getCode(),
                receipt.getNote());

        return transactionIndex;
    }

    public void saveAccount(int transactionIndex, String transactionTypeCode,  double down_payment,double loan_amount,String c_AGR,String c_AGR2) throws DatabaseException {


        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();

//        accountTransaction.addAccountTransactionQueue(
//                getAccountSettingCode(),
//                getTransactionName() + " Amount",
//                receipt.getPaymentAmount(),
//                AccountTransactionType.AUTO);
        //loan_amount
        accountTransaction.addAccountTransactionQueue(SupplierPaymentAccountInterface.SUPPLIER_PAYMENT_DEBIT_CODE,
                "Supplier Payment"+c_AGR, loan_amount, AccountTransactionType.AUTO);

        //down_payment           
       
//        accountTransaction.saveAccountTransaction(
//                getDatabaseService(),
//                transactionIndex,
//                SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
//                AccountTransactionType.AUTO,
//                AccountSettingCreditOrDebit.CREDIT,
//                down_payment,
//                "32-800",
//                "Down Payment", 0);


        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, transactionTypeCode);

    }

    public void saveSettlement(String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            systemSettlement.addSettlementHistoryQueue(
                    settlementHistory.getSettlement(),
                    transactionTypeCode,
                    transactionIndex,
                    settlementHistory.getSettlementAmount());
        }
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
    }

    public void saveLoan(Loan loan) throws DatabaseException {
        loan = getModifiedLoan(loan, getAfterBalanceAmount());
        getDatabaseService().save(loan);
    }

    public void saveOverAmount(Receipt receipt, String transactionTypeCode, int transaction) throws DatabaseException {
//        if (overAmount > 0) {
//            SystemSettlement systemSettlement = SystemSettlement.getInstance();
//            systemSettlement.beginSettlementQueue();
//            systemSettlement.addSettlementQueue(
//                    receipt.getTransactionDate(),
//                    receipt.getLoan().getClient().getCode(),
//                    receipt.getLoan().getIndexNo(),
//                    null,
//                    transactionTypeCode,
//                    transaction,
//                    getTransactionName() + " over amount",
//                    overAmount,
//                    getOverAmountSettlementType());
//
//            systemSettlement.flushSettlementQueue(getDatabaseService());
//        }
    }

    public List<SettlementHistory> getSettlementHistories() {
        if (settlementHistories == null) {
            return new ArrayList<>();
        }

        return settlementHistories;
    }

    public void initSettlementHistory(Client supplier) {
        if (supplier != null) {
            settlementHistories = new ArrayList<>();

            try {
                List<Integer> loanIndexes = new ArrayList();

                Map<Integer, Loan> loanMap = new HashMap();
                String Supplier = null;
                for (Loan loan : supplier.getLoans_clnt()) {

                    if (loan.getStatus().equals(LoanStatus.LOAN_START) && loan.isAvailableVoucher()) {
                        loanIndexes.add(loan.getIndexNo());
                        loanMap.put(loan.getIndexNo(), loan);
                        Supplier = loan.getSupplier().toString();
                    }
                }

//                  List<Double> suplierpay=  getsupplierdouwnpayment(Supplier);


                if (!loanIndexes.isEmpty()) {
                    Criteria criteria = getDatabaseService().initCriteria(Settlement.class);
                    criteria.createAlias("settlementType", "settlementType");
//                criteria.createAlias("loan", "loan");

                    criteria.add(Restrictions.in("loan", loanIndexes));
                    criteria.add(Restrictions.eq("settlementType.creditOrDebit", getSettlementCreitOrDebit()));
                    criteria.add(Restrictions.eq("status", SettlementStatus.SETTLEMENT_PENDING));

                    List<Settlement> settlements = criteria.list();//getDatabaseService().getCollection(hql, params);

                    for (Settlement settlement : settlements) {
                        SettlementHistory settlementHistory = new SettlementHistory();

                        settlementHistory.setIndexNo(null);
                        settlementHistory.setSettlement(settlement);
                        settlementHistory.setAgreementNo(loanMap.get(settlement.getLoan()).getAgreementNo());

                        settlementHistories.add(settlementHistory);
                    }


                }
            } catch (DatabaseException ex) {
                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        sortReceiptReceipts(settlementHistories);
        initSettlementAmount();
    }

    public void sortReceiptReceipts(List<SettlementHistory> settlementHistory) {
        List<SettlementHistory> panalties = new ArrayList<>();
        for (SettlementHistory panalty : settlementHistory) {
            if (panalty.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                panalties.add(panalty);
            }
        }
        settlementHistory.removeAll(panalties);

        Collections.sort(settlementHistory, new Comparator<SettlementHistory>() {
            @Override
            public int compare(SettlementHistory o1, SettlementHistory o2) {
                return ComparisonChain.start()
                        .compare(o1.getSettlement().getDueDate(), o2.getSettlement().getDueDate())
                        .compare(o1.getAgreementNo(), o2.getAgreementNo())
                        .compare(o1.getSettlement().getSettlementType().getPriority(), o2.getSettlement().getSettlementType().getPriority())
                        .result();
            }
        });
        settlementHistory.addAll(0, panalties);
    }

    private void initSettlementAmount() {
        //SET BALANCE AMOUNT

        double temp = settlementAmount;
        balanceAmount = 0.0;

        Iterator<SettlementHistory> iterator = settlementHistories.iterator();
        SettlementHistory settlementHistory;
        double settlement;
        double balance;
        while (iterator.hasNext()) {
            settlementHistory = iterator.next();
            balance = settlementHistory.getSettlement().getBalanceAmount();
            settlement = Math.min(balance, temp);
            temp = temp - settlement;
            settlementHistory.setSettlementAmount(settlement);

            balanceAmount = balanceAmount + balance;
        }

        if (temp > 0) {
            //OVER PAYMENT
            overAmount = temp;
        } else {
            overAmount = 0.0;
        }

        loanChanged();
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public Double getAfterBalanceAmount() {
        return balanceAmount - settlementAmount;
    }

    public Double getOverPaymentAmount() {
        return overAmount;
    }

    public void setSettlementAmount(Double settlementAmount) {
        this.settlementAmount = settlementAmount;

        initSettlementAmount();
    }
//    public List<Double> getsupplierdouwnpayment  (String Supplier) throws Exception
//    {
//        List<Double> collection=new ArrayList<>();;
//        String sql="SELECT SUM(a.`debit_amount`- a.`credit_amount`) as debit_amount  FROM `account_transaction` a \n" +
//"LEFT JOIN `transaction` t ON a.`transaction`=t.`index_no`\n" +
//"LEFT JOIN `loan` l ON t.`loan`=`index_no`  AND l.`supplier`='"+Supplier+"'  \n" +
//"WHERE a.`account`='32-800' AND a.`status`<>'CANCEL'";
//        collection = getDatabaseService().getCollection(sql,null);
//        return collection;
//    }
    //
    protected List<SettlementHistory> settlementHistories = new ArrayList<>();
    protected Double settlementAmount = 0.0;
    protected Double balanceAmount = 0.0;
    protected Double overAmount = 0.0;
    private static DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
    private CashierSession cashierSession;
    private SystemPayment systemPayment;
}
