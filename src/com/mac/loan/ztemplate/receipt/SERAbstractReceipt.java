/*
 *  SERAbstractReceipt.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Sep 29, 2014, 9:51:39 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.receipt;

import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.af.util.compare.comparison_chain.ComparisonChain;
import com.mac.loan.TemperaryReceiptStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.TemperaryReceipt;
import com.mac.loan.ztemplate.receipt.custom_object.Receipt;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.BankDepositReceiptAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeReceiptAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ReceiptAccountInterface;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.AdvancedPayment;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction.object.SettlementBalance;
import com.mac.zutil.royan_remind_letter.object.RemindLetterHistory;
import java.awt.Component;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mohan
 */
public abstract class SERAbstractReceipt extends AbstractService {

    public SERAbstractReceipt(Component component) {
        super(component);
        try {
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException | DatabaseException ex) {
            Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public abstract List<Loan> getLoans();

    protected abstract void loanChanged();

    protected abstract String getSettlementCreitOrDebit();

    protected abstract String getChequeType();

    protected abstract String getAccountSettingCode();

    protected abstract String getOverAmountSettlementType();

    protected abstract String getTransactionName();

    protected abstract Loan getModifiedLoan(Loan loan, Double afterBalance);

    public int saveReceipt(final String transactionTypeCode, Receipt receipt) throws DatabaseException {

        boolean isPaymentOk = showPaymentDialog(transactionTypeCode, receipt);
        if (isPaymentOk) {

            //SAVE TRANSACTION
            final int transactionIndex = saveTransaction(transactionTypeCode, receipt);
             //save settlement report
            
            saveSettlementrpt(transactionTypeCode, receipt, transactionIndex);
            //SAVE SETTLEMENT
            int saveSettlement = saveSettlement(transactionTypeCode, receipt, transactionIndex);

            //list amount
            ListSettlement(transactionTypeCode, receipt, transactionIndex);
            //SAVE OVER AMOUNT
            saveOverAmount(receipt, transactionTypeCode, transactionIndex);

            //SAVE PAYMENT
            savePayment(transactionIndex, receipt);
            //UPDATE REMIND LETTER
            saveRemindLetter(transactionIndex, receipt);
            //ACCOUNT TRANSACTION
            saveAccount(transactionIndex, transactionTypeCode, receipt,interest,panalty,others,capital);

            //SAVE LOAN MODIFICATION
            saveLoan(receipt.getLoan());
            
            //save balance
            savebalance_amount(receipt, transactionTypeCode, transactionIndex);
            
            //REPORT
            if(saveSettlement==1){
            System.out.println(transactionTypeCode + "Printing....");
            Map<String, Object> params = new HashMap<>();
            params.put("TRANSACTION_NO", transactionIndex);
            TransactionUtil.PrintReport(getDatabaseService(), transactionTypeCode, params);
            }
            
            
//            Runnable runnable = new Runnable() {
//                @Override
//                public void run() {
//                    if (transactionTypeCode.equalsIgnoreCase(SystemTransactions.RECEIPT_TRANSACTION_CODE) || transactionTypeCode.equalsIgnoreCase(SystemTransactions.VOUCHER_TRANSACTION_CODE)
//                            || transactionTypeCode.equalsIgnoreCase(SystemTransactions.BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE)) {
////                       
//                            try {
//
//                                Thread.sleep(1000);
////                                Map<String, Object> params = new HashMap<>();
////                                params.put("TRANSACTION_NO", transactionIndex);
//
//
//                                Map<String, Object> params = new HashMap<>();
//                                params.put("TRANSACTION_NO", transactionIndex);
//                                TransactionUtil.PrintReport(getDatabaseService(), transactionTypeCode, params);
//
//
//
//
//
//                            } catch (InterruptedException ex) {
//                                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
//                            } catch (DatabaseException ex) {
//                                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
//                            }
//
////                        }
//                    }
//                }
//            };
//
//            CApplication.invokeEventDispatch(runnable);
//            System.out.println("receipt print");

            return AbstractRegistrationForm.SAVE_SUCCESS;
        } else {
            return AbstractRegistrationForm.SAVE_FAILED;
        }
    }

    public boolean showPaymentDialog(String transactionTypeCode, Receipt receipt) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getTransactionDate(),
                receipt.getPaymentAmount(),
                getChequeType());

        return isPaymentOk;
    }

    public void savePayment(int transactionIndex, Receipt receipt) throws DatabaseException {
        systemPayment.savePayment(
                getDatabaseService(),
                cashierSession,
                transactionIndex,
                receipt.getLoan().getClient().getCode());
    }
    
       public void saveRemindLetter(int transactionIndex, Receipt receipt)  {
           try {
           RemindLetterHistory uniqueResult = (RemindLetterHistory) getDatabaseService()
                    .initCriteria(RemindLetterHistory.class)
                    .add(Restrictions.eq("loan.indexNo", receipt.getLoan().getIndexNo())).uniqueResult();
               
               
               System.out.println("receipt.getLoan().getIndexNo() REMIND LATTER "+receipt.getLoan().getIndexNo());
        if(uniqueResult!=null)
           {
               RemindLetterHistory remindLetterHistory =uniqueResult;
        remindLetterHistory.setStatus("PAY");
            getDatabaseService().update(remindLetterHistory);
               
           }
           } catch (Exception e) {
               e.printStackTrace();
           }
        
    }

    public int saveTransaction(String transactionTypeCode, Receipt receipt) throws DatabaseException {
        //SAVE TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                transactionTypeCode,
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getLoan().getIndexNo(),
                cashierSession.getIndexNo(),
                receipt.getLoan().getClient().getCode(),
                receipt.getNote());

        return transactionIndex;
    }

    
    
    
    
    
    public void saveAccount(int transactionIndex, String transactionTypeCode, Receipt receipt,double interest,double panelty,double othercharge,double capital) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        Date systemdate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
         double dueInsAmount = checkDueInstallment();
         double paymentAmount=0;
        
//         if(dueInsAmount >= receipt.getPaymentAmount() )
//         {
//             paymentAmount = receipt.getPaymentAmount();
//         }
               
         if(transactionTypeCode.equals("VOUCHER"))
         {
               paymentAmount =  receipt.getPaymentAmount() ; 
               
               accountTransaction.addAccountTransactionQueue(
                getAccountSettingCode(), 
                getTransactionName() + " Amount",
//                receipt.getPaymentAmount(), 
                paymentAmount,
                AccountTransactionType.AUTO);
         }
         else
         {
                 paymentAmount =  receipt.getPaymentAmount() ;
        
         if(receipt.getLoan().getReason().equals("EARLY_SETTLEMENT"))
         {
             accountTransaction.addAccountTransactionQueue(
                getAccountSettingCode(), 
                getTransactionName() + " Amount",
//                receipt.getPaymentAmount(), 
                receipt.getPaymentAmount(),
                AccountTransactionType.AUTO);
             
//               accountTransaction.addAccountTransactionQueue(
//                ReceiptAccountInterface.RECEIPT_ADVANCED_CREDIT_CODE,
//                "Cash Receipt Advanced Amount",
//                0.0,
//                AccountTransactionType.AUTO);
         }else
         {
             double default_charge_amount =0.0;
             for (SettlementHistory settlementHistory : settlementHistories) {
                   if(settlementHistory.getSettlement().getSettlementType().getCode().equals("APPLICATION_CHARGE")){
                        if(settlementHistory.getSettlement().getAccount()!=null){
                           accountTransaction.saveAccountTransaction(
                                  getDatabaseService(),
                                  transactionIndex,
                                  transactionTypeCode,
                                  AccountTransactionType.AUTO,
                                  AccountSettingCreditOrDebit.CREDIT,
                                  settlementHistory.getSettlementAmount(),
                                  settlementHistory.getSettlement().getAccount(),
                                  settlementHistory.getSettlement().getDescription()+" - "+receipt.getLoan().getAgreementNo(),0 );

                        default_charge_amount += settlementHistory.getSettlementAmount(); 
                        }
                       
                   }
                        if(settlementHistory.getSettlement().getDueDate().after(systemdate) && settlementHistory.getSettlementAmount()>0)
                         {
                             //loan capital credit account settings
                             //INTEREST
                            if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST)){
                                
                                
                                //CREDIT
                                AccountSetting LOAN_INTEREST_CREDIT = (AccountSetting) getDatabaseService().initCriteria(AccountSetting.class)
                                        .add(Restrictions.eq("code", "LOAN_INTEREST_CREDIT"))
                                        .add(Restrictions.eq("transactionType.code", SystemTransactions.DAY_END_TRANSACTION_CODE))
                                        .uniqueResult();
                                
                                accountTransaction.saveAccountTransaction(
                                  getDatabaseService(),
                                  transactionIndex,
                                  transactionTypeCode,
                                  AccountTransactionType.AUTO,
                                  AccountSettingCreditOrDebit.CREDIT,
                                  settlementHistory.getSettlementAmount(),
                                  LOAN_INTEREST_CREDIT.getAccount().getCode(),
                                  settlementHistory.getSettlement().getDescription()+" - "+receipt.getLoan().getAgreementNo(),0 );
                                 //DEBIT
                                 AccountSetting LOAN_INTEREST_DEBIT = (AccountSetting) getDatabaseService().initCriteria(AccountSetting.class)
                                        .add(Restrictions.eq("code", "LOAN_INTEREST_DEBIT"))
                                        .add(Restrictions.eq("transactionType.code", SystemTransactions.DAY_END_TRANSACTION_CODE))
                                        .uniqueResult();
                                accountTransaction.saveAccountTransaction(
                                  getDatabaseService(),
                                  transactionIndex,
                                  transactionTypeCode,
                                  AccountTransactionType.AUTO,
                                  AccountSettingCreditOrDebit.DEBIT,
                                  settlementHistory.getSettlementAmount(),
                                  LOAN_INTEREST_DEBIT.getAccount().getCode(),
                                  settlementHistory.getSettlement().getDescription()+" - "+receipt.getLoan().getAgreementNo(),0 );

                            }
                            //CAPITAL
                            if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_CAPITAL)){
                                
                                
                                //CREDIT
                                AccountSetting LOAN_CAPITAL_CREDIT = (AccountSetting) getDatabaseService().initCriteria(AccountSetting.class)
                                        .add(Restrictions.eq("code", "LOAN_CAPITAL_CREDIT"))
                                        .add(Restrictions.eq("transactionType.code", SystemTransactions.DAY_END_TRANSACTION_CODE))
                                        .uniqueResult();
                                
                                accountTransaction.saveAccountTransaction(
                                  getDatabaseService(),
                                  transactionIndex,
                                  transactionTypeCode,
                                  AccountTransactionType.AUTO,
                                  AccountSettingCreditOrDebit.CREDIT,
                                  settlementHistory.getSettlementAmount(),
                                  LOAN_CAPITAL_CREDIT.getAccount().getCode(),
                                  settlementHistory.getSettlement().getDescription()+" - "+receipt.getLoan().getAgreementNo(),0 );
                                 //DEBIT
                                 AccountSetting LOAN_CAPITAL_DEBIT = (AccountSetting) getDatabaseService().initCriteria(AccountSetting.class)
                                        .add(Restrictions.eq("code", "LOAN_CAPITAL_DEBIT"))
                                        .add(Restrictions.eq("transactionType.code", SystemTransactions.DAY_END_TRANSACTION_CODE))
                                        .uniqueResult();
                                accountTransaction.saveAccountTransaction(
                                  getDatabaseService(),
                                  transactionIndex,
                                  transactionTypeCode,
                                  AccountTransactionType.AUTO,
                                  AccountSettingCreditOrDebit.DEBIT,
                                  settlementHistory.getSettlementAmount(),
                                  LOAN_CAPITAL_DEBIT.getAccount().getCode(),
                                  settlementHistory.getSettlement().getDescription()+" - "+receipt.getLoan().getAgreementNo(),0 );

                            }
                    }
             }
             if(default_charge_amount > 0){
                 accountTransaction.saveAccountTransaction(
                            getDatabaseService(),
                            transactionIndex,
                            transactionTypeCode,
                            AccountTransactionType.AUTO,
                            AccountSettingCreditOrDebit.DEBIT,
                            default_charge_amount,
                            "11-400",
                            "Default Charg Amount - "+receipt.getLoan().getAgreementNo(),0 );
                            
             }
             
            if(receipt.getLoan().isWriteOff()){//write of detor cd balance and write off dr /  receipt write of cr
                
                accountTransaction.addAccountTransactionQueue(
                getAccountSettingCode(), 
                getTransactionName() + " Amount",
                0.0,
                AccountTransactionType.AUTO);
                
                
                 AccountSetting WRITE_OFF_CREDIT = (AccountSetting) getDatabaseService().initCriteria(AccountSetting.class)
                                        .add(Restrictions.eq("code", "WRITE_OFF_DEBIT"))
                                        .add(Restrictions.eq("transactionType.code", SystemTransactions.WRITE_OFF_TRANSACTION_CODE))
                                        .uniqueResult();
                
                accountTransaction.saveAccountTransaction(
                getDatabaseService(),
                transactionIndex,
                transactionTypeCode,
                AccountTransactionType.AUTO,
                AccountSettingCreditOrDebit.CREDIT,
                paymentAmount,
                WRITE_OFF_CREDIT.getAccount().getCode(),
                "Payment "+receipt.getLoan().getAgreementNo(),0 );
            }else{
                accountTransaction.addAccountTransactionQueue(
                getAccountSettingCode(), 
                getTransactionName() + " Amount",
//                receipt.getPaymentAmount(), 
                paymentAmount,
                AccountTransactionType.AUTO);
            }
         
         }
         }
         
            
         
       //panelty update accounts   it is mmc goes to day end
//        switch(transactionTypeCode)
//        {
//            
//            case  "RECEIPT"  :
//         
//            
//         accountTransaction.addAccountTransactionQueue(
//                ReceiptAccountInterface.RECEIPT_PANELTY_CREDIT_CODE,
//                "Cash Receipt Panelty Amount",
//                panelty,
//                AccountTransactionType.AUTO);
//         
//         accountTransaction.addAccountTransactionQueue(
//                ReceiptAccountInterface.RECEIPT_INT_PNL_DEBIT_CODE,
//                "Cash Receipt Panelty Amount",
//                panelty,
//                AccountTransactionType.AUTO);
//                
//
//         
//                break;
//            case  "BANK_RECEIPT" :
//                
//                
//       
//        
//         accountTransaction.addAccountTransactionQueue(
//                BankDepositReceiptAccountInterface.RECEIPT_PANELTY_CREDIT_CODE,
//                "Bank Receipt Panelty Amount",
//                panelty,
//                AccountTransactionType.AUTO);
//         
//         accountTransaction.addAccountTransactionQueue(
//                BankDepositReceiptAccountInterface.RECEIPT_INT_PNL_DEBIT_CODE,
//                "Bank Receipt Panelty Amount",
//                panelty,
//                AccountTransactionType.AUTO); 
//         
//                break;
//                
//                
//        }
      
        
        
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, transactionTypeCode);
    }

    public void saveChequeReceiptIssueAccount(int transactionIndex, String transactionTypeCode, Receipt receipt) throws DatabaseException {
        
        interest=0.0;panalty=0.0;others=0.0;
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            
            
            if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_INTEREST"))
            {
                interest += settlementHistory.getSettlementAmount();
            }
            else if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_PANALTY"))
            {
                panalty += settlementHistory.getSettlementAmount();
            }
            else
            {
                others += settlementHistory.getSettlementAmount();
            }
        }
        
        //advance 
        
        
        double dueInsAmount = checkDueInstallment();
         double paymentAmount=0;
        
//         if(dueInsAmount >= receipt.getPaymentAmount() )
//         {
//             paymentAmount = receipt.getPaymentAmount();
//         }
         if(transactionTypeCode.equals("VOUCHER"))
         {
               paymentAmount =  receipt.getPaymentAmount() ;  
         }
         else
         {
                 paymentAmount =  dueInsAmount ;
         }
       
        
        
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
               
        accountTransaction.addAccountTransactionQueue(
                ChequeReceiptAccountInterface.CHEQUE_RECEIPT_AMOUNT_CREDIT_CODE,
                "Cheque Receipt Amount",
                paymentAmount,
                AccountTransactionType.AUTO);
         
                accountTransaction.addAccountTransactionQueue(
                ChequeReceiptAccountInterface.RECEIPT_ADVANCED_CREDIT_CODE,
                "Cheque Receipt Advanced Amount",
                (receipt.getPaymentAmount()-dueInsAmount)> 0?receipt.getPaymentAmount()-dueInsAmount:0,
                AccountTransactionType.AUTO);
             
                accountTransaction.addAccountTransactionQueue(
                ChequeReceiptAccountInterface.CHEQUE_RECEIPT_AMOUNT_DEBIT_CODE,
                "Cheque Receipt Amount",
                receipt.getPaymentAmount(),
                AccountTransactionType.AUTO);
        

//         accountTransaction.saveAccountTransaction(
//                            getDatabaseService(),
//                            transactionIndex,
//                            transactionTypeCode,
//                            AccountTransactionType.AUTO,
//                            AccountSettingCreditOrDebit.CREDIT,
//                            receipt.getPaymentAmount(),
//                            transactionObject.getBankAccount().getValueAccount(),
//                            "Cheque bank Amount ",0 );
//
     
        
        // additional modification for royan 
        
        
//        accountTransaction.addAccountTransactionQueue(
//                ChequeReceiptAccountInterface.RECEIPT_INTREST_CREDIT_CODE,
//                "Cheque Receipt Interest Amount",
//                interest,
//                AccountTransactionType.AUTO);
         
//         accountTransaction.addAccountTransactionQueue(
//                ChequeReceiptAccountInterface.RECEIPT_PANELTY_CREDIT_CODE,
//                "Cheque Receipt Panelty Amount",
//                panalty,
//                AccountTransactionType.AUTO);
//         
//         accountTransaction.addAccountTransactionQueue(
//                ChequeReceiptAccountInterface.RECEIPT_INT_PNL_DEBIT_CODE,
//                "Cheque Receipt int_pnl Amount",
//                panalty,
//                AccountTransactionType.AUTO);
//        
        
        
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, transactionTypeCode);


    }

    public int saveSettlement(String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            systemSettlement.addSettlementHistoryQueue(
                    settlementHistory.getSettlement(),
                    transactionTypeCode,
                    transactionIndex,
                    settlementHistory.getSettlementAmount());
        }
        int flushSettlementHistoryQueue = systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
        return flushSettlementHistoryQueue;
        
    }
     double capital=0.0 , interest=0.0,panalty=0.0,others=0.0;
      double capitalover=0.0 , interestover=0.0,panaltyover=0.0,othersover=0.0;
    public void ListSettlement(String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        interest=0.0;panalty=0.0;others=0.0;
        Date NEXT_DUE_DATE;
        
        Calendar  calander = Calendar.getInstance();
        
                
        AdvancedPayment s = new AdvancedPayment();
//        SystemSettlement systemSettlement = SystemSettlement.getInstance();
//        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            
            String d= settlementHistory.getSettlement().getSettlementType().getCode().toString();
            Date systemdate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
            String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
            System.out.println("COMPAIRE : "+settlementHistory.getSettlement().getDueDate().compareTo(systemdate));
            
          
            
             if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_PANALTY"))
                {
                    panalty += settlementHistory.getSettlementAmount();
                }
            
            if(settlementHistory.getSettlement().getDueDate().before(systemdate) || settlementHistory.getSettlement().getDueDate().equals(systemdate))
            {
                
                if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_INTEREST"))
                {
                    interest += settlementHistory.getSettlementAmount();
                }
                
                else if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_CAPITAL"))
                {
                    capital += settlementHistory.getSettlementAmount();
                }
                else
                {
                    others += settlementHistory.getSettlementAmount();
                }
            }else if(settlementHistory.getSettlementAmount() > 0)
            {
               calander.setTime(settlementHistory.getSettlement().getDueDate());
               calander.add(calander.MONTH, 1);
               NEXT_DUE_DATE = calander.getTime();
              //  System.out.println("SETTLEMENT TYPE :"+settlementHistory.getSettlement().getSettlementType().getCode());
                
//                s.setAccount(settlementHistory.getSettlement().getDueDate());
                //LOAN_CAPITAL
              if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_CAPITAL))
              {
                s.setTransactionDate(systemdate);
                s.setBranch(branch);
                s.setClient(settlementHistory.getSettlement().getClient());
                s.setLoan(settlementHistory.getSettlement().getLoan());
                s.setInstallmentNo(0);
                s.setTransaction(transactionIndex);
                s.setTransactionType(settlementHistory.getSettlement().getTransactionType());
                s.setDescription(settlementHistory.getSettlement().getDescription());
                s.setAmount(settlementHistory.getSettlementAmount());
                s.setBalanceAmount(settlementHistory.getSettlementAmount());
                s.setDueDate(settlementHistory.getSettlement().getDueDate());
                s.setSettlementType(settlementHistory.getSettlement().getSettlementType().getCode());
                s.setAccount("11-100"); 
//                s.setAccount(ReceiptAccountInterface.RECEIPT_INTREST_CREDIT_CODE); 
              }
              //LOAN_INTEREST
              if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST))
              {
                s.setTransactionDate(systemdate);
                s.setBranch(branch);
                s.setClient(settlementHistory.getSettlement().getClient());
                s.setLoan(settlementHistory.getSettlement().getLoan());
                s.setInstallmentNo(0);
                s.setTransaction(transactionIndex);
                s.setTransactionType(settlementHistory.getSettlement().getTransactionType());
                s.setDescription(settlementHistory.getSettlement().getDescription());
                s.setAmount(settlementHistory.getSettlementAmount());
                s.setBalanceAmount(settlementHistory.getSettlementAmount());
                s.setDueDate(settlementHistory.getSettlement().getDueDate());
                s.setSettlementType(settlementHistory.getSettlement().getSettlementType().getCode());
                s.setAccount("31-100"); 
              }
             if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_CAPITAL)||
                     settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST))
             {
               s.setSend(false); 
               s.setStatus("ACTIVE"); 
                
                
                
                advancedPayment.add(s);
                
                 getDatabaseService().beginLocalTransaction();
                 getDatabaseService().save(advancedPayment);
                 getDatabaseService().commitLocalTransaction();
                advancedPayment.clear();
             }
                
                
            }
        }
        
       
        
        
    }
    public void save_status(TemperaryReceipt   temperaryReceipt) throws DatabaseException
    {
        TemperaryReceipt temperary_Receipt = temperaryReceipt;
        
        temperary_Receipt.setStatus(TemperaryReceiptStatus.SETTLED);
        getDatabaseService().update(temperary_Receipt);
        getDatabaseService().commitLocalTransaction();
        
    }
    
    
     public void saveSettlementrpt(String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
         initgetarearsaAount(receipt.getLoan());
         
        
        for (SettlementHistory settlementHistory : settlementHistories) {
            systemSettlement.addSettlementHistoryQueuerpt(
                    getDatabaseService(),
                    settlementHistory.getSettlement(),
                    transactionTypeCode,
                    transactionIndex,
                    settlementHistory.getSettlementAmount(),
                    getArrearsAmountRpt());
            break;
        }
         
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
    }
     public void savebalance_amount(Receipt receipt,String transactionTypeCode,int transactionIndex )throws DatabaseException 
     {
         SystemSettlement systemSettlement = SystemSettlement.getInstance();
         SettlementBalance settlementBalance=new SettlementBalance();
         settlementBalance.setDescription(transactionTypeCode);
         settlementBalance.setTransaction(transactionIndex);
         settlementBalance.setDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
//         settlementBalance.setBalanceAmount(systemSettlement.getbalance_amount(receipt.getLoan().getIndexNo(), settlementAmount,getDatabaseService()));
         settlementBalance.setBalanceAmount(getAfterBalanceAmount());
         settlementBalance.setInstallmentCount(systemSettlement.getinstallment_count(receipt.getLoan().getIndexNo()));
         settlementBalance.setStatus("Active");
         
         getDatabaseService().save(settlementBalance);
         
     }

    public void saveLoan(Loan loan) throws DatabaseException {
        loan = getModifiedLoan(loan, getAfterBalanceAmount());
        getDatabaseService().save(loan);
    }

    public void saveOverAmount(Receipt receipt, String transactionTypeCode, int transaction) throws DatabaseException {
        if (overAmount > 0) {
            SystemSettlement systemSettlement = SystemSettlement.getInstance();
            systemSettlement.beginSettlementQueue();
            systemSettlement.addSettlementQueue(
                    receipt.getTransactionDate(),
                    receipt.getLoan().getClient().getCode(),
                    receipt.getLoan().getIndexNo(),
                    null,
                    transactionTypeCode,
                    transaction,
                    getTransactionName() + " over amount",
                    overAmount,
                    getOverAmountSettlementType());

            systemSettlement.flushSettlementQueue(getDatabaseService());
        }
    }

    public List<SettlementHistory> getSettlementHistories() {
        if (settlementHistories == null) {
            return new ArrayList<>();
        }

        return settlementHistories;
    }

      public List getAdvanced(int loan) {
        List<AdvancedPayment> loans;

        String hql = "FROM com.mac.zsystem.transaction.settlement.object.AdvancedPayment WHERE send=:SEND and status=:STATUS and loan=:LOAN  ";
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("STATUS", "ACTIVE");
        hashMap.put("SEND", false);
        hashMap.put("LOAN", loan);

        try {
            loans = getDatabaseService().getCollection(hql, hashMap);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }
    public void initSettlementHistory(Loan loan) {
        if (loan != null) {
            settlementHistories = new ArrayList<>();




            String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan=:LOAN AND settlementType.creditOrDebit=:CREDIT_OR_DEBIT AND status=:STATUS";
            HashMap<String, Object> params = new HashMap<>();
            params.put("LOAN", loan.getIndexNo());
            params.put("CREDIT_OR_DEBIT", getSettlementCreitOrDebit());
            params.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);
            
        


            /////////////////////////////////////// These for get Arrears Amount
            try {


                String hqlx = "SELECT SUM(balanceAmount) FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE status=:STATUS AND   settlementType<>'LOAN_AMOUNT' AND  loan=:LOAN AND dueDate<=:DUEDATE";
                HashMap<String, Object> paramsx = new HashMap<>();
                paramsx.put("LOAN", loan.getIndexNo());
                paramsx.put("DUEDATE", (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                paramsx.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);

                arrearsAmount = (Double) getDatabaseService().getUniqueResultHQL(hqlx, paramsx);



            } catch (Exception ex) {
                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }



            try {
                List<Settlement> settlements = getDatabaseService().getCollection(hql, params);
                for (Settlement settlement : settlements) {

                    SettlementHistory settlementHistory = new SettlementHistory();

                    settlementHistory.setIndexNo(null);
                    settlementHistory.setSettlement(settlement);
                    settlementHistories.add(settlementHistory);
                }

            } catch (Exception ex) {
                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        sortReceiptReceipts(settlementHistories);
        initSettlementAmount(loan);
    }
public void initgetarearsaAount(Loan loan)
    {
           List<Settlement> list;
            
            try {
                String hqlx = " FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE status=:STATUS AND  settlementType<>'LOAN_AMOUNT' AND  loan=:LOAN AND dueDate<=:DUEDATE";
                HashMap<String, Object> paramsx = new HashMap<>();
                paramsx.put("LOAN", loan.getIndexNo());
                paramsx.put("DUEDATE", (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                paramsx.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);
                list = getDatabaseService().getCollection(hqlx,paramsx);
                //arrearsAmount = (Double) getDatabaseService().getUniqueResultHQL(hqlx, paramsx);
                
                for(Settlement listSettlement:list)
                {
                 arrearsAmountRpt += listSettlement.getBalanceAmount();
                }
                
                    System.out.println("arrearsAmount_____"+arrearsAmountRpt);

            } catch (Exception ex) {
                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }
    }


public double initBlockAgreement(Loan loan)
    {
           List<Settlement> list;
            double arrearsAmounta=0.0;
            try {
                String hqlx = "SELECT SUM(balanceAmount)/"+loan.getInstallmentAmount()+" FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE status=:STATUS AND  settlementType<>'LOAN_AMOUNT' AND  loan=:LOAN AND dueDate<=:DUEDATE";
                HashMap<String, Object> paramsx = new HashMap<>();
                paramsx.put("LOAN", loan.getIndexNo());
                paramsx.put("DUEDATE", (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                paramsx.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);
                list = getDatabaseService().getCollection(hqlx,paramsx);
                //arrearsAmount = (Double) getDatabaseService().getUniqueResultHQL(hqlx, paramsx);
                
                arrearsAmounta = (Double) getDatabaseService().getUniqueResultHQL(hqlx, paramsx);
                
                    System.out.println("arrearsAmount_____"+arrearsAmountRpt);

            } catch (Exception ex) {
                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }
            
           return arrearsAmounta;
    }


    public void sortReceiptReceipts(List<SettlementHistory> settlementHistory) {
        List<SettlementHistory> panalties = new ArrayList<>();
        for (SettlementHistory panalty : settlementHistory) {
            if (panalty.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                panalties.add(panalty);
            }
        }
        settlementHistory.removeAll(panalties);

        Collections.sort(settlementHistory, new Comparator<SettlementHistory>() {
            @Override
            public int compare(SettlementHistory o1, SettlementHistory o2) {
                return ComparisonChain.start()
                        .compare(o1.getSettlement().getDueDate(), o2.getSettlement().getDueDate())
                        .compare(o1.getSettlement().getSettlementType().getPriority(), o2.getSettlement().getSettlementType().getPriority())
                        .result();
            }
        });
        settlementHistory.addAll(0, panalties);
    }
//nimesh
    private void initSettlementAmount(Loan l) {
        //SET BALANCE AMOUNT

        double temp = settlementAmount;
        balanceAmount = 0.0;
        penaltyAmount = 0.0;
    Date systemdate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        Iterator<SettlementHistory> iterator = settlementHistories.iterator();
        SettlementHistory settlementHistory;
        double settlement;
        double balance;
        double balance_installment;
      
        while (iterator.hasNext()) {
            settlementHistory = iterator.next();

            balance = settlementHistory.getSettlement().getBalanceAmount();
          
            balance_installment = settlementHistory.getSettlement().getBalanceInstallment();
            settlement = Math.min(balance, temp);
            
            if(settlementHistory.getSettlement().getDueDate().before(systemdate) 
                    || settlementHistory.getSettlement().getDueDate().equals(systemdate))
            {
                temp = temp - settlement;
                settlementHistory.setSettlementAmount(settlement);
               

                
                
            }else {//SEPARATE from over pay INTEREST AMOUNT AND CAPITAL AMOUNT
                System.out.println(balance_installment);
          
               
                if(balance_installment > temp && settlement>0 ){
                    switch (settlementHistory.getSettlement().getSettlementType().getCode()) {
                        case SystemSettlement.LOAN_INTEREST:
                            settlement = ((temp /(calInterest(l)+100))*calInterest(l));
                            settlement = Math.round(settlement*100.0)/100.0;
                            temp = Math.round(temp*100.0)/100.0;
                            temp = temp - settlement;
                            settlementHistory.setSettlementAmount(settlement);
                            break;
                        case SystemSettlement.LOAN_CAPITAL:
                            settlement = Math.round(settlement*100.0)/100.0;
                            temp = Math.round(temp*100.0)/100.0;
                            temp = temp - settlement;
                            // settlement = settlementHistory.getSettlement().getBalanceAmount() - settlement ;
                            settlementHistory.setSettlementAmount(settlement);
                            break;
                        default:
                            temp = temp - settlement;
                            settlementHistory.setSettlementAmount(settlement);
                            break;
                    }
                }else{
                    temp = temp - settlement;
                   settlementHistory.setSettlementAmount(settlement); 
                }
            }
            
            
            
                loan = settlementHistory.getSettlement().getLoan();
                balanceAmount = balanceAmount + balance;
            if (settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                    penaltyAmount += settlementHistory.getSettlement().getBalanceAmount();
                }
            
            
        }

        if (temp > 0) {
            //OVER PAYMENT
            overAmount = temp;
        } else {
            overAmount = 0.0;
        }

        loanChanged();
    }
    
    
    public double calInterest(Loan l){
        double int_rate = l.getInterestRate();
        double int_count = l.getInstallmentCount();
        double result=0.0;
        
        if(l.getPaymentTerm().equals("WEEKLY")){
            
            result = ((int_count / 48)*int_rate);
            
        }else if(l.getPaymentTerm().equals("MONTHLY")){
            
            result = ((int_count / 12)*int_rate);
        }
        
        
        
        return result;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public Double getPenaltyAmount() {
        return penaltyAmount;
    }

    public Double getAfterBalanceAmount() {
        return balanceAmount - settlementAmount;
    }

    public Double getOverPaymentAmount() {
        return overAmount;
    }
    public Double getReciptOverPaymentAmount() {
        return overAmountrecipt;
    }

    public void setSettlementAmount(Double settlementAmount,Loan l) {
        this.settlementAmount = settlementAmount;

        initSettlementAmount(l);
    }

    public Double getArrearsAmount() {
        return arrearsAmount;
    }
    public int getloan() {
        return loan;
    }
    
    public Double getArrearsAmountRpt() {
        return arrearsAmountRpt;
    }
            private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();

        return connection;
    }
    public double checkDueInstallment()
    {
       
       
         Date systemdate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
   
       
        overAmountrecipt = 0.0;

        Iterator<SettlementHistory> iterator = settlementHistories.iterator();
        SettlementHistory settlementHistory;
      
        while (iterator.hasNext()) {
            settlementHistory = iterator.next();
            
                if(settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_CAPITAL")
                        || settlementHistory.getSettlement().getSettlementType().getCode().equals("LOAN_INTEREST"))
                {
//                    if (settlementHistory.getSettlement().getDueDate().before(systemdate) 
//                            || settlementHistory.getSettlement().getDueDate().equals(systemdate)) 
//                    {
                overAmountrecipt +=  settlementHistory.getSettlementAmount();
                   // }
                }
                else //if(settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY))
                {
                overAmountrecipt +=  settlementHistory.getSettlementAmount();   
                }
            
        }
        return overAmountrecipt;
//       
    }
    //
    protected List<SettlementHistory> settlementHistories = new ArrayList<>();
    protected List<AdvancedPayment> advancedPayment = new ArrayList<>();
    protected Double settlementAmount = 0.0;
    protected Double balanceAmount = 0.0;
    protected Double arrearsAmount = 0.0;
    protected Double overAmount = 0.0;
    protected Double overAmountrecipt = 0.0;
    protected Double arrearsAmountRpt = 0.0;
    protected Double penaltyAmount = 0.0;
    private static DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
    private CashierSession cashierSession;
    private SystemPayment systemPayment;
    private int loan;
}
