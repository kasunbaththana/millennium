/*
 *  PCReceipt.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 6, 2014, 3:25:36 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.receipt;

import com.mac.af.component.derived.input.combobox.CIComboBox;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.dash_board.client.ClientInformationDashBoard;
import com.mac.loan.zobject.Loan;
import com.mac.loan.ztemplate.receipt.custom_object.Receipt;
import com.mac.zsystem.model.table_model.recept_table_model.ReceiptTableModel;
import com.mac.zsystem.transaction.settlement.object.AdvancedPayment;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JDialog;

/**
 *
 * @author mohan
 */
public abstract class PCReceipt extends AbstractObjectCreator<Receipt> {

    /**
     * Creates new form PCReceipt
     */
    public PCReceipt(SERAbstractReceipt serReceipt) {
        this.serReceipt = serReceipt;
        initComponents();

        initOthers();
    }

    protected abstract String getReferenceGeneratorType();

    public Loan getSelectedLoan(CIComboBox comboBox) {
        return (Loan) comboBox.getCValue();
    }

    public List getLoans() {
        return serReceipt.getLoans();
    }

    public boolean isPaymentAmountFixed() {
        return false;
    }

    public Double getDefaultPaymentAmount(CIComboBox comboBox) {
        return 0.0;
    }

    private void newReceipt() {
        receipt = new Receipt();

        txtReferenceNo.resetValue();
        txtDocumentNo.setCValue(null);
        txtTransactionDate.setCValue((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));

        loanSelected();
    }
      
   
    
     private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }
     
     
    public String getArreasAmount(Loan loan)  {
        
       String value="0"; 
        
        DecimalFormat forma=new DecimalFormat("#,###.##");
        
            try {
                ResultSet rs = getConnection().createStatement().
                executeQuery("SELECT t.`arreasAmount`,t.`vol` FROM `tmp_blockloan` t WHERE t.`agreement_no`='"+loan.getAgreementNo()+"'");
        
                while(rs.next()){
                    value = "Arreas Amount: "+ forma.format(rs.getDouble("arreasAmount"))+"\n Volume: "+forma.format(rs.getDouble("vol"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        
        return value;
    }
private Loan loan_dump;
    private void loanSelected() {
        Loan loan = getSelectedLoan(cboAgreementNo);
        serReceipt.initSettlementHistory(loan);
        double advancedAmount=0.0;
       // txtPaymentAmount.setEnabled(true);
//                
        if (loan != null) {
//            if(!serReceipt.getTransactionName().equals("Voucher")){
//          //  System.out.println("serReceipt.getTransactionName() "+serReceipt.getTransactionName());
//            if(loan.isReceiptBlock()){
//                
//                if(loan_dump!=loan){
//                    
//              mOptionPane.showMessageDialog(this, 
//                      "This Customer Blook by System \n Check Arreas Or Contact Administrator \n"+getArreasAmount(loan),
//                      "Receipt Block", mOptionPane.WARNING_MESSAGE);  
//              loan_dump=loan;
//                }
//                txtPaymentAmount.setEnabled(false);
//            }
//            }
            
            
             List<AdvancedPayment> AdvancedPayment = serReceipt.getAdvanced(loan.getIndexNo());
               // txtPaymentAmount.setValueEditable(true);
                txtClient.setCValue(loan.getClient().getName());
               String address=loan.getClient().getAddressLine1()+", "+loan.getClient().getAddressLine2()+", "+loan.getClient().getAddressLine3();
                txtAddress.setCValue(address);
                txtLoanAmount.setCValue(loan.getLoanAmount());
                txtInstallmentAmount.setCValue(loan.getInstallmentAmount());
                txtLoanBalance.setCValue(serReceipt.getBalanceAmount());
                txtPenaltyBalance.setCValue(serReceipt.getPenaltyAmount());
                txtAfterBalance.setCValue(serReceipt.getAfterBalanceAmount());
                txtArrearsAmount.setCValue(serReceipt.getArrearsAmount());
//                double aRVol=serReceipt.getArrearsAmount()/loan.getInstallmentAmount();
             //   double aRVol2=serReceipt.getBalanceAmount()/loan.getInstallmentAmount();
                txtArrearsvolume.resetValue();
                txtArrearstotalvol.resetValue();
                txtinstAmt.resetValue();
                lblvehicle.setText("");
                if(serReceipt.getArrearsAmount()!=null)
                {
                    System.out.println(serReceipt.getArrearsAmount()+" / "+loan.getInstallmentAmount());
                txtArrearsvolume.setCValue((serReceipt.getArrearsAmount()/loan.getInstallmentAmount()));
                txtArrearstotalvol.setCValue(((serReceipt.getBalanceAmount()-serReceipt.getArrearsAmount())/loan.getInstallmentAmount()));
                }
                txtinstAmt.setCValue((double)loan.getInstallmentCount());
                if(loan.getVehicle()!=null)
                {
                    
              lblvehicle.setText(loan.getVehicle().toString());
                }
                txtPaymentAmount.setCValue(getDefaultPaymentAmount(cboAgreementNo));
                resetPaymentAmount();
                
                
                for(AdvancedPayment listAdvancedPayment : AdvancedPayment)
            {
               advancedAmount += listAdvancedPayment.getBalanceAmount();
            }
               
                txtAdvanceAmount.setCValue(advancedAmount);
            
//            }
        } else {
            txtClient.setCValue(null);
            txtAddress.setCValue(null);
            txtLoanAmount.setCValue(null);
            txtInstallmentAmount.setCValue(null);
            txtLoanBalance.setCValue(null);
            txtPenaltyBalance.setCValue(null);
            txtPaymentAmount.setCValue(null);
            txtAfterBalance.setCValue(null);
            txtAdvanceAmount.setCValue(null);
            txtArrearsAmount.setCValue(null);
            txtPaymentAmount.setCValue(0.0);
            txtArrearstotalvol.setCValue(null);
            txtinstAmt.setCValue(null);
            txtArrearsvolume.setCValue(null);
            resetPaymentAmount();
        }
//                }
    }

    private void resetPaymentAmount() {
        Double paymentAmount = txtPaymentAmount.getCValue();
        Loan loan = getSelectedLoan(cboAgreementNo);
        serReceipt.setSettlementAmount(paymentAmount,loan);
        txtAfterBalance.setCValue(serReceipt.getAfterBalanceAmount());
      //  txtAdvanceAmount.setCValue(serReceipt.getAfterBalanceAmount()>0 ? serReceipt.getAfterBalanceAmount() : (serReceipt.getAfterBalanceAmount()*-1) );
        
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        cboAgreementNo.setExpressEditable(true);
        cboAgreementNo.setTableModel(new ReceiptTableModel());
        txtPaymentAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetPaymentAmount();
            }
        });

        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(getReferenceGeneratorType()));
        cboAgreementNo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
               
                loanSelected();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cboAgreementNo = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoans();
            }
        };
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtClient = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPaymentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAfterBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAddress = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInstallmentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPenaltyBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtArrearsAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        jButton1 = new javax.swing.JButton();
        cDLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        txtArrearsvolume = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtArrearstotalvol = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        txtinstAmt = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel17 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel18 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAdvanceAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        lblvehicle = new javax.swing.JLabel();

        cDLabel1.setText("Agreement No.:");

        cDLabel2.setText("Reference No.:");

        cDLabel3.setText("Document No.:");

        cDLabel4.setText("Transaction Date :");

        cDLabel5.setText("Client :");

        cDLabel6.setText("Loan Balance :");

        cDLabel7.setText("Payment Amount :");

        cDLabel8.setText("After Balance :");

        cDLabel9.setText("Address :");

        cDLabel10.setText("Loan Amount :");

        cDLabel11.setText("Note :");

        cDLabel12.setText("Installment Amount :");

        cDLabel13.setText("Penalty Balance :");

        cDLabel14.setText("Arrears  Vol :");

        jButton1.setForeground(new java.awt.Color(255, 0, 0));
        jButton1.setText("Customer History..");
        jButton1.setBorder(null);
        jButton1.setBorderPainted(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.setMargin(new java.awt.Insets(2, 1, 2, 14));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        cDLabel15.setText("FAV   :");

        txtArrearstotalvol.setText("0.0");

        cDLabel16.setText("TIA   :");

        cDLabel17.setText("AMT   :");

        cDLabel18.setText("Advance Amount :");

        txtAdvanceAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAdvanceAmountActionPerformed(evt);
            }
        });

        lblvehicle.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        lblvehicle.setForeground(new java.awt.Color(0, 204, 51));
        lblvehicle.setText("vehicle ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblvehicle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAdvanceAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAfterBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPaymentAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLoanBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtClient, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAddress, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPenaltyBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtArrearstotalvol, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
                            .addComponent(txtArrearsvolume, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtinstAmt, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(txtArrearsAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(txtNote, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(cDLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addComponent(jButton1)
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtClient, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPenaltyBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(cDLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPaymentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAfterBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAdvanceAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtArrearsAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtArrearsvolume, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtArrearstotalvol, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtinstAmt, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNote, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblvehicle)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
     
        Loan loan = (Loan) cboAgreementNo.getCValue();
        if (loan != null) {
            ClientInformationDashBoard panel = new ClientInformationDashBoard(loan.getClient().getCode());
            //        panel.setC
            JDialog dialog = new JDialog();
            dialog.add(panel);
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            dialog.setBounds(0, 0, screenSize.width - 50, screenSize.height - 50);
            //        dialog.setLocationRelativeTo(CApplication.getInstance().getMainFrame());
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        } else {
            mOptionPane.showMessageDialog(null, "Please Select Client", "Selection ..", mOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtAdvanceAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAdvanceAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAdvanceAmountActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel17;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel18;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAgreementNo;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel lblvehicle;
    private com.mac.af.component.derived.input.textfield.CIStringField txtAddress;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAdvanceAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAfterBalance;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtArrearsAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtArrearstotalvol;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtArrearsvolume;
    private com.mac.af.component.derived.input.textfield.CIStringField txtClient;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtInstallmentAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNote;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtPaymentAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPenaltyBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtinstAmt;
    // End of variables declaration//GEN-END:variables
    private Receipt receipt;
    private SERAbstractReceipt serReceipt;

    @Override
    public void setNewMood() {
        cboAgreementNo.setValueEditable(true);
        txtClient.setValueEditable(false);
        txtAddress.setValueEditable(false);
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(true);
        txtTransactionDate.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
        txtPenaltyBalance.setValueEditable(false);
        txtInstallmentAmount.setValueEditable(false);
        txtPaymentAmount.setValueEditable(!isPaymentAmountFixed());
        txtAfterBalance.setValueEditable(false);
        txtAdvanceAmount.setValueEditable(false);
        txtNote.setValueEditable(true);
        txtArrearsAmount.setValueEditable(false);
        txtinstAmt.setValueEditable(false);
         cboAgreementNo.doRefresh();

        newReceipt();
    }

    @Override
    public void setEditMood() {
        cboAgreementNo.setValueEditable(true);
        txtClient.setValueEditable(false);
        txtAddress.setValueEditable(false);
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(true);
        txtTransactionDate.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
        txtPenaltyBalance.setValueEditable(false);
        txtInstallmentAmount.setValueEditable(false);
        txtPaymentAmount.setValueEditable(!isPaymentAmountFixed());
        txtAfterBalance.setValueEditable(false);
        txtAdvanceAmount.setValueEditable(false);
        txtNote.setValueEditable(true);
        txtArrearsAmount.setValueEditable(false);
        txtArrearstotalvol.setValueEditable(false);
        txtArrearsvolume.setValueEditable(false);
        txtinstAmt.setValueEditable(false);
         cboAgreementNo.doRefresh();
    }

    @Override
    public void setIdleMood() {
        cboAgreementNo.doRefresh();
        cboAgreementNo.setValueEditable(false);
        txtClient.setValueEditable(false);
        txtAddress.setValueEditable(false);
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(false);
        txtTransactionDate.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
        txtPenaltyBalance.setValueEditable(false);
        txtInstallmentAmount.setValueEditable(false);
        txtPaymentAmount.setValueEditable(false);
        txtAfterBalance.setValueEditable(false);
        txtAdvanceAmount.setValueEditable(false);
        txtNote.setValueEditable(false);
        txtArrearsAmount.setValueEditable(false);
        txtArrearstotalvol.setValueEditable(false);
        txtArrearsvolume.setValueEditable(false);
        txtinstAmt.setValueEditable(false);
    }

    @Override
    public void resetFields() {
        cboAgreementNo.resetValue();
        txtClient.resetValue();
        txtAddress.resetValue();
        txtReferenceNo.resetValue();
        txtDocumentNo.resetValue();
        txtTransactionDate.resetValue();
        txtLoanAmount.resetValue();
        txtLoanBalance.resetValue();
        txtPenaltyBalance.resetValue();
        txtPaymentAmount.resetValue();
        txtAfterBalance.resetValue();
        txtAdvanceAmount.resetValue();
        txtArrearsAmount.resetValue();
        txtArrearstotalvol.resetValue();
        txtArrearsvolume.resetValue();
        txtinstAmt.resetValue();
        lblvehicle.setText("vehicle no");
    }

    @Override
    protected void setValueAbstract(Receipt value) throws ObjectCreatorException {
        //DO NOTHING
        //NO OUTSIDE VALUE ACCEPTED
    }

    @Override
    protected Receipt getValueAbstract() throws ObjectCreatorException {
        return receipt;
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        //DO NOTHING
        //NO OUTSIDE VALUE ACCEPTED
    }

    @Override
    protected void initObject() throws ObjectCreatorException {
        receipt.setReferenceNo(txtReferenceNo.getCValue());
        receipt.setDocumentNo(txtDocumentNo.getCValue());
        receipt.setTransactionDate(txtTransactionDate.getCValue());
        receipt.setNote(txtNote.getCValue());

        receipt.setLoan(getSelectedLoan(cboAgreementNo));
        receipt.setPaymentAmount(txtPaymentAmount.getCValue());
    }
}
