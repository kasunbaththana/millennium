/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.ztemplate.charge;

import com.mac.account.AccountTypes;
import com.mac.account.ztemplate.account_entry.object.Entry;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.LoanStatus;
import com.mac.loan.ztemplate.charge.object.ChargeTransaction;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OtherChargeAccountInterface;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SMTK
 */
public abstract class SERCharge extends AbstractService {

    public SERCharge(Component component) {
        super(component);
    }

    protected abstract String getTransactionTypeCode();

    protected abstract String getAccountTransactionTypeCode();

    public List getAccouns() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.zsystem.transaction.account.object.Account");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERCharge.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getLoans() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Loan where status='" + LoanStatus.LOAN_START + "' and agreementNo <> null");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERCharge.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void save(ChargeTransaction transaction, Collection<Entry> journals) throws DatabaseException {
        //TRANSACTION
        int transactionIndex = saveTransaction(transaction);

        //ACCOUNT TRANSACTION
        saveAccountEntries(transactionIndex, transaction, journals);

        //SAVE SETTLEMENT
        saveSettlement(transaction, journals, transactionIndex);
        
        //SAVE LOAN CHARGE
        //saveChageSetting(transaction, journals, transactionIndex);
        
    }

    private void saveSettlement(ChargeTransaction chargeTransaction, Collection<Entry> journals, int transaction) throws DatabaseException {
        SystemSettlement settlement = SystemSettlement.getInstance();

        settlement.beginSettlementQueue();
        for (Entry entry : journals) {
            settlement.addSettlementQueueForCharge(
                    chargeTransaction.getTransactionDate(),
                    chargeTransaction.getLoan().getClient().getCode(),
                    chargeTransaction.getLoan().getIndexNo(),
                    null,
                    SystemTransactions.OTHER_CHARGE_TRANSACTION_CODE,
                    transaction,
                    entry.getDescription(),
                    entry.getCreditAmount() + entry.getDebitAmount(),
                    SystemSettlement.OTHER_CHARGE,
                    entry.getAccount());
        }
        
        settlement.flushSettlementQueue(getDatabaseService());
    }
    

    public int saveTransaction(ChargeTransaction entryTransaction) throws DatabaseException {
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                getTransactionTypeCode(),
                entryTransaction.getReferenceNo(),
                entryTransaction.getDocumentNo(),
                entryTransaction.getLoan().getIndexNo(),
                null,
                null,
                entryTransaction.getNote());

        return transactionIndex;
    }

    public void saveAccountEntries(int transactionIndex,ChargeTransaction chargeTransaction, Collection<Entry> entries) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (Entry entry : entries) {
            accountTransaction.saveAccountTransaction(
                    getDatabaseService(),
                    transactionIndex,
                    getTransactionTypeCode(),
                    getAccountTransactionTypeCode(),
                    entry.getCreditOrDebit(),
                    entry.getCreditOrDebit().equals(AccountSettingCreditOrDebit.CREDIT) ? entry.getCreditAmount() : entry.getDebitAmount(),
                    entry.getAccount(),
                    entry.getDescription(),
                    chargeTransaction.getLoan().getIndexNo());
        }
        
        accountTransaction.beginAccountTransactionQueue();
        accountTransaction.addAccountTransactionQueue(OtherChargeAccountInterface.OTHER_CHARGE_AMOUNT_DEBIT_CODE, "Loan Charges - " + chargeTransaction.getLoan().getAgreementNo(), chargeTransaction.getTotalCreditAmount(), AccountTransactionType.AUTO);
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, SystemTransactions.OTHER_CHARGE_TRANSACTION_CODE);
    }

    public boolean showPaymentDialog(String transactionTypeCode, ChargeTransaction receipt, String chequeType, String accountEntryType) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getTransactionDate(),
                (accountEntryType.equals(Charge.ACCOUNT_ENTRY_CREDIT_ONLY)
                ? receipt.getTotalCreditAmount()
                : accountEntryType.equals(Charge.ACCOUNT_ENTRY_DEBIT_ONLY)
                ? receipt.getTotalDebitAmount()
                : 0.0),
                chequeType);

        return isPaymentOk;
    }

    public void savePayment(int transactionIndex, ChargeTransaction receipt) throws DatabaseException {
        systemPayment.savePayment(
                getDatabaseService(),
                null,
                transactionIndex,
                null);
    }
    private SystemPayment systemPayment;
}
