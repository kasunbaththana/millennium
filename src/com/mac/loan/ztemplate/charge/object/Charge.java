/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.ztemplate.charge.object;

import com.mac.zsystem.transaction.account.object.Account;

/**
 *
 * @author SMTK
 */
public class Charge {

    private Account account;
    private String description;
    private Double creditAmount;
    private Double debitAmount;
    private ChargeTransaction transaction;
    private String creditOrDebit;

    public Charge() {
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Double creditAmount) {
        this.creditAmount = creditAmount;
    }

    public Double getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(Double debitAmount) {
        this.debitAmount = debitAmount;
    }

    public ChargeTransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(ChargeTransaction transaction) {
        this.transaction = transaction;
    }

    public String getCreditOrDebit() {
        return creditOrDebit;
    }

    public void setCreditOrDebit(String creditOrDebit) {
        this.creditOrDebit = creditOrDebit;
    }
}
