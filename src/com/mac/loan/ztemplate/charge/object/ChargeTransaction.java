/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.ztemplate.charge.object;

import com.mac.loan.zobject.Loan;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Thilanga-pc
 */
public class ChargeTransaction {

    private Loan loan;
    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private String note;
    private Set<Charge> journels = new HashSet<>(0);
    private Double totalCreditAmount;
    private Double totalDebitAmount;

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public Double getTotalCreditAmount() {
        return totalCreditAmount;
    }

    public void setTotalCreditAmount(Double totalCreditAmount) {
        this.totalCreditAmount = totalCreditAmount;
    }

    public Double getTotalDebitAmount() {
        return totalDebitAmount;
    }

    public void setTotalDebitAmount(Double totalDebitAmount) {
        this.totalDebitAmount = totalDebitAmount;
    }

    public ChargeTransaction() {
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<Charge> getJournels() {
        return journels;
    }

    public void setJournels(Set<Charge> journels) {
        this.journels = journels;
    }
}
