/*
 *  PCReceipt.java
 * 
 *  @author  kasun
 *     kasun
 * 
 *  Created on Oct 6, 2014, 3:25:36 PM
 *  Copyrights kasun, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.disbursement;


import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.derived.input.combobox.CIComboBox;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanAdvanceTrans;
import com.mac.loan.ztemplate.disbursement.custom_object.Receipt;
import com.mac.zsystem.model.table_model.recept_table_model.ReceiptTableModel;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author kasun
 */
public abstract class PCReceipt extends AbstractObjectCreator<Receipt> {

    /**
     * Creates new form PCReceipt
     */
    public PCReceipt(SERAbstractReceipt serReceipt) {
        this.serReceipt = serReceipt;
        initComponents();

        initOthers();
    }

    protected abstract String getReferenceGeneratorType();

    public Loan getSelectedLoan(CIComboBox comboBox) {
        return (Loan) comboBox.getCValue();
    }

    public List getLoans() {
        return serReceipt.getLoans();
    }

    public boolean isPaymentAmountFixed() {
        return false;
    }

    public Double getDefaultPaymentAmount(CIComboBox comboBox) {
        return 0.0;
    }

    private void newReceipt() {
        receipt = new Receipt();

        txtReferenceNo.resetValue();
        txtDocumentNo.setCValue(null);
        txtTransactionDate.setCValue((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));

        loanSelected();
    }
      
   
    
     private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }
     
     


    private void loanSelected() {
        Loan loan = getSelectedLoan(cboAgreementNo);
        serReceipt.initSettlementHistory(loan);
        double advancedAmount=0.0;
        txtPaymentAmount.setEnabled(true);
//                
        if (loan != null) {
         
            
            
   
               // txtPaymentAmount.setValueEditable(true);
                txtClient.setCValue(loan.getClient().getName());
               String address=loan.getClient().getAddressLine1()+", "+loan.getClient().getAddressLine2()+", "+loan.getClient().getAddressLine3();
                txtAddress.setCValue(address);
                txtLoanAmount.setCValue(loan.getLoanAmount());
                txtInstallmentAmount.setCValue(loan.getInstallmentAmount());
                txtLoanBalance.setCValue(serReceipt.getBalanceAmount());
               
                txtAfterBalance.setCValue(serReceipt.getAfterBalanceAmount());
                txtGroup.setCValue(loan.getClient().getGroup_name());
                txtPaymentAmount.setCValue(getDefaultPaymentAmount(cboAgreementNo));
                resetPaymentAmount();
                
                
            
     

        } else {
            txtClient.setCValue(null);
            txtAddress.setCValue(null);
            txtLoanAmount.setCValue(null);
            txtInstallmentAmount.setCValue(null);
            txtLoanBalance.setCValue(null);
           
            txtPaymentAmount.setCValue(null);
            txtAfterBalance.setCValue(null);
            txtGroup.setCValue(null);
           
            txtPaymentAmount.setCValue(0.0);
           
            resetPaymentAmount();
        }
//                }
    }

    private void resetPaymentAmount() {
        Double paymentAmount = txtPaymentAmount.getCValue();
        serReceipt.setSettlementAmount(paymentAmount);
        txtAfterBalance.setCValue(serReceipt.getAfterBalanceAmount());
      //  txtAdvanceAmount.setCValue(serReceipt.getAfterBalanceAmount()>0 ? serReceipt.getAfterBalanceAmount() : (serReceipt.getAfterBalanceAmount()*-1) );
        
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        cboAgreementNo.setExpressEditable(true);
        cboAgreementNo.setTableModel(new ReceiptTableModel());
        txtPaymentAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetPaymentAmount();
            }
        });

        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(getReferenceGeneratorType()));
        cboAgreementNo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
               
                loanSelected();
            }
        });
        
         ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnReject, "doReject");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cboAgreementNo = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoans();
            }
        };
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtClient = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPaymentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAfterBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAddress = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInstallmentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtNote = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnReject = new com.mac.af.component.derived.command.button.CCButton();
        jLabel1 = new javax.swing.JLabel();
        txtGroup = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel13 = new com.mac.af.component.derived.display.label.CDLabel();

        cDLabel1.setText("Agreement No.:");

        cDLabel2.setText("Reference No.:");

        cDLabel3.setText("Document No.:");

        cDLabel4.setText("Transaction Date :");

        cDLabel5.setText("Client :");

        cDLabel6.setText("Loan Balance :");

        cDLabel7.setText("Payment Amount :");

        cDLabel8.setText("After Balance :");

        cDLabel9.setText("Address :");

        cDLabel10.setText("Loan Amount :");

        cDLabel11.setText("Note :");

        cDLabel12.setText("Installment Amount :");

        btnReject.setForeground(new java.awt.Color(204, 0, 0));
        btnReject.setText("Reject");
        btnReject.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 153, 0));
        jLabel1.setText("Save Disbursement");

        cDLabel13.setText("Group :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(cDLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtGroup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addComponent(btnReject, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE))
                    .addComponent(txtAfterBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPaymentAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLoanBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtClient, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAddress, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtNote, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(cDLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtClient, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPaymentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAfterBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNote, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnReject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnReject;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAgreementNo;
    private javax.swing.JLabel jLabel1;
    private com.mac.af.component.derived.input.textfield.CIStringField txtAddress;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAfterBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtClient;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtGroup;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtInstallmentAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNote;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtPaymentAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables
    private Receipt receipt;
    private SERAbstractReceipt serReceipt;

    @Override
    public void setNewMood() {
        cboAgreementNo.setValueEditable(true);
        txtClient.setValueEditable(false);
        txtAddress.setValueEditable(false);
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(true);
        txtTransactionDate.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
        txtGroup.setValueEditable(false);
       
        txtInstallmentAmount.setValueEditable(false);
        txtPaymentAmount.setValueEditable(!isPaymentAmountFixed());
        txtAfterBalance.setValueEditable(false);
       
        txtNote.setValueEditable(true);
   
         cboAgreementNo.doRefresh();

        newReceipt();
    }

    @Override
    public void setEditMood() {
        cboAgreementNo.setValueEditable(true);
        txtClient.setValueEditable(false);
        txtAddress.setValueEditable(false);
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(true);
        txtTransactionDate.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
        txtGroup.setValueEditable(false);
      
        txtInstallmentAmount.setValueEditable(false);
        txtPaymentAmount.setValueEditable(!isPaymentAmountFixed());
        txtAfterBalance.setValueEditable(false);
       
        txtNote.setValueEditable(true);
       
         cboAgreementNo.doRefresh();
    }

    @Override
    public void setIdleMood() {
        cboAgreementNo.doRefresh();
        cboAgreementNo.setValueEditable(false);
        txtClient.setValueEditable(false);
        txtAddress.setValueEditable(false);
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(false);
        txtTransactionDate.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanBalance.setValueEditable(false);
       
        txtInstallmentAmount.setValueEditable(false);
        txtPaymentAmount.setValueEditable(false);
        txtAfterBalance.setValueEditable(false);
        txtGroup.setValueEditable(false);
       
        txtNote.setValueEditable(false);
       
    }

    @Override
    public void resetFields() {
        cboAgreementNo.resetValue();
        txtClient.resetValue();
        txtAddress.resetValue();
        txtReferenceNo.resetValue();
        txtDocumentNo.resetValue();
        txtTransactionDate.resetValue();
        txtLoanAmount.resetValue();
        txtLoanBalance.resetValue();
       
        txtPaymentAmount.resetValue();
        txtAfterBalance.resetValue();
        txtGroup.resetValue();
      
    }

    @Override
    protected void setValueAbstract(Receipt value) throws ObjectCreatorException {
        //DO NOTHING
        //NO OUTSIDE VALUE ACCEPTED
    }

    @Override
    protected Receipt getValueAbstract() throws ObjectCreatorException {
        return receipt;
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        //DO NOTHING
        //NO OUTSIDE VALUE ACCEPTED
    }

    @Override
    protected void initObject() throws ObjectCreatorException {
        receipt.setReferenceNo(txtReferenceNo.getCValue());
        receipt.setDocumentNo(txtDocumentNo.getCValue());
        receipt.setTransactionDate(txtTransactionDate.getCValue());
        receipt.setNote(txtNote.getCValue());

        receipt.setLoan(getSelectedLoan(cboAgreementNo));
        receipt.setPaymentAmount(txtPaymentAmount.getCValue());
    }
     private CashierSession cashierSession;
   @Action
    public void doReject() {
       
        int q = mOptionPane.showConfirmDialog(this, "Are you sure want to save changes ?", "Save", mOptionPane.YES_NO_OPTION, mOptionPane.QUESTION_MESSAGE);

        if (q == mOptionPane.YES_OPTION) {
       Loan loan = (Loan) cboAgreementNo.getCValue();
       
       loan.setAvailableDisbursement(false);
       loan.setAvailableReceipt(false);
       loan.setAvailableVoucher(false);
       loan.setStatus(LoanStatus.LOAN_REJECT);
        try {
            initObject();
            cashierSession=SystemCashier.getCurrentCashierSession(CPanel.GLOBAL.getDatabaseService());
            
            CPanel.GLOBAL.getDatabaseService().save(loan);
          
            SystemTransactions.insertTransaction(
                CPanel.GLOBAL.getDatabaseService(),
                SystemTransactions.DISBURSEMENT_REJECT_TRANSACTION_CODE,
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getLoan().getIndexNo(),
                cashierSession.getIndexNo(),
                receipt.getLoan().getClient().getCode(),
                receipt.getNote());
            
             LoanAdvanceTrans advanceTrans = (LoanAdvanceTrans) CPanel.GLOBAL.getDatabaseService().initCriteria(LoanAdvanceTrans.class)
                        .add(Restrictions.eq("loan", loan.getIndexNo())).uniqueResult();
            
            CPanel.GLOBAL.getDatabaseService().delete(advanceTrans);
        } catch (Exception ex) {
            Logger.getLogger(PCReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
       
    }
}
