/*
 *  Receipt.java
 * 
 *  @author kasun
 * 
 *  Created on Oct 6, 2014, 3:13:07 PM
 *  Copyrights Kasun, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.disbursement.custom_object;


import com.mac.loan.zobject.Loan;
import java.util.Date;


/**
 *
 * @author kasun
 */
public class Receipt {

    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private String note;
    //
    private Loan loans;
    private Double paymentAmount;
    private Double advanceAmount;

    public Receipt() {
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Loan getLoan() {
        return loans;
    }

    public void setLoan(Loan loan) {
        this.loans = loan;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Double getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(Double advanceAmount) {
        this.advanceAmount = advanceAmount;
    }
    
}
