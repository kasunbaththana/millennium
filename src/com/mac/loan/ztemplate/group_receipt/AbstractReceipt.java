/*
 *  AbstractReceipt.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Sep 29, 2014, 9:36:03 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.group_receipt;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanGroup;
import com.mac.loan.ztemplate.group_receipt.custom_object.Receipt;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public abstract class AbstractReceipt extends AbstractRegistrationForm {

    public AbstractReceipt() {
        initCashierSession();

        initService();

        initOthers();
    }

    public abstract List getLoanGroups();

    protected abstract String getReferenceGeneratorType();

    protected abstract String getTransactionTypeCode();

    protected abstract String getSettlementCreitOrDebit();

    protected abstract String getChequeType();

    protected abstract String getAccountSettingCode();

    protected abstract String getOverAmountSettlementType();

    protected abstract boolean isOverPayAvailable();

    protected abstract String getTransactionName();

    protected abstract Loan getModifiedLoan(Loan loan, Double afterBalance);

    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
    }

    protected int saveReceipt(String transactionTypeCode, Receipt receipt) throws DatabaseException {
        return serAbstractReceipt.saveReceipt(transactionTypeCode, receipt);
    }

    @Override
    protected int save(Object object) throws DatabaseException {
        if (serAbstractReceipt.getOverPaymentAmount() > 0 && !isOverPayAvailable()) {
            mOptionPane.showMessageDialog(this,
                    "You have over paid amount of " + serAbstractReceipt.getOverPaymentAmount() + ". \nPlease enter a low amount.",
                    getTransactionName(),
                    mOptionPane.ERROR_MESSAGE);

            return SAVE_FAILED;
        }

        Receipt receipt = (Receipt) object;
        return saveReceipt(AbstractReceipt.this.getTransactionTypeCode(), receipt);
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        initService();

        pcReceipt = new PCReceipt(serAbstractReceipt) {
            @Override
            protected String getReferenceGeneratorType() {
                return AbstractReceipt.this.getReferenceGeneratorType();
            }
        };

        return pcReceipt;
    }

    public SERAbstractReceipt getService() {
        initService();

        return serAbstractReceipt;
    }

    @Override
    protected List getTableData() throws DatabaseException {
        initService();

        return serAbstractReceipt.getSettlementHistories();
    }

    private void initCashierSession() {
        try {
            SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException ex) {
            Logger.getLogger(AbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initService() {
        //INIT SERVICE
        if (serAbstractReceipt == null) {
            serAbstractReceipt = new SERAbstractReceipt(this) {
                @Override
                public List<LoanGroup> getLoanGroups() {
                    return AbstractReceipt.this.getLoanGroups();
                }

                @Override
                protected void loanChanged() {
                    refreshTable();
                }

                @Override
                protected String getSettlementCreitOrDebit() {
                    return AbstractReceipt.this.getSettlementCreitOrDebit();
                }

                @Override
                protected String getChequeType() {
                    return AbstractReceipt.this.getChequeType();
                }

                @Override
                protected String getAccountSettingCode() {
                    return AbstractReceipt.this.getAccountSettingCode();
                }

                @Override
                protected String getTransactionName() {
                    return AbstractReceipt.this.getTransactionName();
                }

                @Override
                protected Loan getModifiedLoan(Loan loan, Double afterBalance) {
                    return AbstractReceipt.this.getModifiedLoan(loan, afterBalance);
                }
                
                @Override
                protected String getOverAmountSettlementType() {
                    return AbstractReceipt.this.getOverAmountSettlementType();
                }
            };
        }
    }

    private void initOthers() {
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(getTransactionTypeCode());
        this.recentButton.setDatabseService(getDatabaseService());

        tblMain.addContainerListener(new ContainerListener() {
            @Override
            public void componentAdded(ContainerEvent e) {
            }

            @Override
            public void componentRemoved(ContainerEvent e) {
                updateSettlementAmount();
            }
        });
    }

    private void updateSettlementAmount() {
        List<SettlementHistory> settlementHistorys = new ArrayList<>(tblMain.getCValue());
        double settlement = 0.0;
        for (SettlementHistory settlementHistory : settlementHistorys) {
            settlement += settlementHistory.getSettlementAmount();
        }
        pcReceipt.txtPaymentAmount.setCValue(settlement);
    }

    @Override
    protected Object getTableValueForObjectCreator(Object tableValue) {
        return null;
    }

    @Override
    public CTableModel getTableModel() {
        //CREATE DEFAULT CTABLE MODEL TO HERE
        return new CTableModel(new CTableColumn[]{
//            new CTableColumn("indexNo", new String[]{"settlement", "indexNo"}),
            new CTableColumn("Due Date", new String[]{"settlement", "dueDate"}),
            new CTableColumn("Agreement No.", new String[]{"agreementNo"}),
            new CTableColumn("Installment No", new String[]{"settlement", "installmentNo"}),
            new CTableColumn("Description", new String[]{"settlement", "description"}),
            new CTableColumn("Amount", new String[]{"settlement", "amount"}),
            new CTableColumn("Balance Amount", new String[]{"settlement", "balanceAmount"}),
            new CTableColumn("Settlement Amount", new String[]{"settlementAmount"}, true),
            //            new CTableColumn("Afeter Balance", new String[]{"amount"}),
            new CTableColumn("Type", new String[]{"settlement", "settlementType", "description"})
        });
    }

    @Override
    protected int getRegistrationType() {
        return AbstractRegistrationForm.NEW_ONLY_REGISTRATION_TYPE;
    }

    @Override
    public Class getObjectClass() {
        throw new UnsupportedOperationException("getObjectClass not supported");
    }
    //
    private SERAbstractReceipt serAbstractReceipt;
    private PCReceipt pcReceipt;
    private RecentButton recentButton;
}
