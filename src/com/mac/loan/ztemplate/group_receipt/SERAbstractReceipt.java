/*
 *  SERAbstractReceipt.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Sep 29, 2014, 9:51:39 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.ztemplate.group_receipt;

import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.af.util.compare.comparison_chain.ComparisonChain;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanGroup;
import com.mac.loan.ztemplate.group_receipt.custom_object.Receipt;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mohan
 */
public abstract class SERAbstractReceipt extends AbstractService {

    public SERAbstractReceipt(Component component) {
        super(component);
        try {
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException | DatabaseException ex) {
            Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public abstract List<LoanGroup> getLoanGroups();

    protected abstract void loanChanged();

    protected abstract String getSettlementCreitOrDebit();

    protected abstract String getChequeType();

    protected abstract String getAccountSettingCode();

    protected abstract String getOverAmountSettlementType();

    protected abstract String getTransactionName();

    protected abstract Loan getModifiedLoan(Loan loan, Double afterBalance);

    public int saveReceipt(final String transactionTypeCode, Receipt receipt) throws DatabaseException {
        Set<Integer> loanNumbers = new HashSet<>();
        for (SettlementHistory settlementHistory : settlementHistories) {
            if (settlementHistory.getSettlementAmount() > 0.0) {
                loanNumbers.add(settlementHistory.getSettlement().getLoan());
            }
        }

        Map<Integer, Loan> loanMap = new HashMap<>();
        Map<Integer, Set<SettlementHistory>> settlementHistoryMap = new HashMap();
        for (Integer integer : loanNumbers) {

            for (Loan loan : receipt.getLoans()) {
                if (loan.getIndexNo().equals(integer)) {
                    loanMap.put(integer, loan);
                }
            }

            Set<SettlementHistory> settlements = new HashSet();
            for (SettlementHistory settlementHistory : settlementHistories) {
                if (settlementHistory.getSettlement().getLoan().equals(integer)) {
                    settlements.add(settlementHistory);
                }
            }
            settlementHistoryMap.put(integer, settlements);
        }

        boolean isPaymentOK = showPaymentDialog(transactionTypeCode, receipt);

        if (isPaymentOK) {
            //save group loan transaction
            final int transactionIndex = saveTransaction(transactionTypeCode, receipt);

            //save payment for the group loan
            savePayment(transactionIndex, receipt);

            saveAccount(transactionIndex, transactionTypeCode, receipt);

            for (Integer integer : settlementHistoryMap.keySet()) {
                //save transaction
                int receiptTransaction = saveTransaction2(transactionTypeCode, receipt, loanMap.get(integer));

                //save settlement
                saveSettlement2(settlementHistoryMap.get(integer), transactionTypeCode, receipt, receiptTransaction);

                //save over amount

                //save payment

                //save account transaction

                //save loan
                saveLoan(loanMap.get(integer));
            }


            return AbstractRegistrationForm.SAVE_SUCCESS;
        } else {
            return AbstractRegistrationForm.SAVE_FAILED;
        }

        /*
         boolean isPaymentOk = showPaymentDialog(transactionTypeCode, receipt);

         if (isPaymentOk) {


         //SAVE TRANSACTION
         final int transactionIndex = saveTransaction(transactionTypeCode, receipt);

         //SAVE SETTLEMENT
         saveSettlement(transactionTypeCode, receipt, transactionIndex);

         //SAVE OVER AMOUNT
         saveOverAmount(receipt, transactionTypeCode, transactionIndex);

         //SAVE PAYMENT
         savePayment(transactionIndex, receipt);

         //ACCOUNT TRANSACTION
         saveAccount(transactionIndex, transactionTypeCode, receipt);

         //SAVE LOAN MODIFICATION
         saveLoan(receipt.getLoan());


         //REPORT
         Map<String, Object> params = new HashMap<>();
         params.put("TRANSACTION_NO", transactionIndex);
         TransactionUtil.PrintReport(getDatabaseService(), transactionTypeCode, params);


         //            Runnable runnable = new Runnable() {
         //                @Override
         //                public void run() {
         //                    if (transactionTypeCode.equalsIgnoreCase(SystemTransactions.RECEIPT_TRANSACTION_CODE)) {
         //                        int q = mOptionPane.showConfirmDialog(getCPanel(), "Do you want to print receipt?", "Print Receipt", mOptionPane.YES_NO_OPTION);
         ////                        int q = mOptionPane.NO_OPTION;
         //                        if (q == mOptionPane.YES_OPTION) {
         //                            Map<String, Object> params = new HashMap<>();
         //                            params.put("TRANSACTION_NO", transactionIndex);
         //                            
         //                            com.mac.zreport.ReportBuilder.initDefaultParameters();
         //                            Map<String, Object> defaultParams = com.mac.zreport.ReportBuilder.DEFAULT_PARAMETERS;
         //                            try {
         //                                InputStream inputStream = SERAbstractReceipt.class.getResource("receipt.jrxml").openStream();
         //                                
         //                                ReportDialog reportDialog = new ReportDialog(inputStream,
         //                                        defaultParams,
         //                                        params);
         //                                reportDialog.setTitle(ReportBuilder.getFormattedString("Receipt"));
         //                                reportDialog.setVisible(true);
         //                            } catch (IOException ex) {
         //                                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
         //                            }
         //                            
         //                        }
         //                    }
         //                }
         //            };
         //            
         //            CApplication.invokeEventDispatch(runnable);
         System.out.println("receipt print");

         return AbstractRegistrationForm.SAVE_SUCCESS;
         } else {
         return AbstractRegistrationForm.SAVE_FAILED;
         }*/
    }

    private int saveTransaction2(String transactionTypeCode, Receipt receipt, Loan loan) throws DatabaseException {
        System.out.println("transactionTypeCode___"+transactionTypeCode);
        //SAVE TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                transactionTypeCode,
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                loan.getIndexNo(),
                cashierSession.getIndexNo(),
                loan.getClient().getCode(),
                receipt.getNote());

        return transactionIndex;
    }

    public void saveSettlement2(Collection<SettlementHistory> settlementHistories, String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            systemSettlement.addSettlementHistoryQueue(
                    settlementHistory.getSettlement(),
                    transactionTypeCode,
                    transactionIndex,
                    settlementHistory.getSettlementAmount());
        }
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
    }

    public void saveAccount2(int transactionIndex, String transactionTypeCode, Receipt receipt) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        accountTransaction.addAccountTransactionQueue(getAccountSettingCode(), getTransactionName() + " Amount", receipt.getPaymentAmount(), AccountTransactionType.AUTO);
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, transactionTypeCode);
    }

    public void saveLoan2(Loan loan) throws DatabaseException {
        loan = getModifiedLoan(loan, getAfterBalanceAmount());
        getDatabaseService().save(loan);
    }

    public boolean showPaymentDialog(String transactionTypeCode, Receipt receipt) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getTransactionDate(),
                receipt.getPaymentAmount(),
                getChequeType());

        return isPaymentOk;
    }

    public void savePayment(int transactionIndex, Receipt receipt) throws DatabaseException {
        systemPayment.savePayment(
                getDatabaseService(),
                cashierSession,
                transactionIndex,
                null);
//                receipt.getLoan().getClient().getCode());
    }

    public int saveTransaction(String transactionTypeCode, Receipt receipt) throws DatabaseException {
        System.out.println("XXXXXXXX______________"+transactionTypeCode);
        //SAVE TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                transactionTypeCode,
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                null,//receipt.getLoan(),
                cashierSession.getIndexNo(),
                null,//receipt.getLoan().getClient().getCode(),
                receipt.getNote());

        return transactionIndex;
    }

    public void saveAccount(int transactionIndex, String transactionTypeCode, Receipt receipt) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        accountTransaction.addAccountTransactionQueue(getAccountSettingCode(), getTransactionName() + " Amount", receipt.getPaymentAmount(), AccountTransactionType.AUTO);
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, transactionTypeCode);
    }

    public void saveSettlement(String transactionTypeCode, Receipt receipt, int transactionIndex) throws DatabaseException {
        //SAVE SETTLEMENT HISTORIES AND UPDATE SETTLEMENT
        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (SettlementHistory settlementHistory : settlementHistories) {
            systemSettlement.addSettlementHistoryQueue(
                    settlementHistory.getSettlement(),
                    transactionTypeCode,
                    transactionIndex,
                    settlementHistory.getSettlementAmount());
        }
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
    }

    public void saveLoan(Loan loan) throws DatabaseException {
        loan = getModifiedLoan(loan, getAfterBalanceAmount());
        getDatabaseService().save(loan);
    }

    public void saveOverAmount(Receipt receipt, String transactionTypeCode, int transaction) throws DatabaseException {
//        if (overAmount > 0) {
//            SystemSettlement systemSettlement = SystemSettlement.getInstance();
//            systemSettlement.beginSettlementQueue();
//            systemSettlement.addSettlementQueue(
//                    receipt.getTransactionDate(),
//                    receipt.getLoan().getClient().getCode(),
//                    receipt.getLoan().getIndexNo(),
//                    null,
//                    transactionTypeCode,
//                    transaction,
//                    getTransactionName() + " over amount",
//                    overAmount,
//                    getOverAmountSettlementType());
//
//            systemSettlement.flushSettlementQueue(getDatabaseService());
//        }
    }

    public List<SettlementHistory> getSettlementHistories() {
        if (settlementHistories == null) {
            return new ArrayList<>();
        }

        return settlementHistories;
    }

    public void initSettlementHistory(LoanGroup loanGroup) {
        if (loanGroup != null) {
            settlementHistories = new ArrayList<>();





//            String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan=:LOAN AND settlementType.creditOrDebit=:CREDIT_OR_DEBIT AND status=:STATUS";
//            HashMap<String, Object> params = new HashMap<>();
//            params.put("LOAN", loan.getIndexNo());
//            params.put("CREDIT_OR_DEBIT", getSettlementCreitOrDebit());
//            params.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);

            try {
                List<Integer> loanIndexes = new ArrayList();
                
                Map<Integer, Loan> loanMap = new HashMap();
                
                for (Loan loan : loanGroup.getLoans()) {
                    if (loan.getStatus().equals(LoanStatus.LOAN_START) && loan.isAvailableReceipt()) {
                        loanIndexes.add(loan.getIndexNo());
                        loanMap.put(loan.getIndexNo(), loan);
                    }
                }

                if (!loanIndexes.isEmpty()) {
                    Criteria criteria = getDatabaseService().initCriteria(Settlement.class);
                    criteria.createAlias("settlementType", "settlementType");
//                criteria.createAlias("loan", "loan");

                    criteria.add(Restrictions.in("loan", loanIndexes));
                    criteria.add(Restrictions.eq("settlementType.creditOrDebit", getSettlementCreitOrDebit()));
                    criteria.add(Restrictions.eq("status", SettlementStatus.SETTLEMENT_PENDING));

                    List<Settlement> settlements = criteria.list();//getDatabaseService().getCollection(hql, params);

                    for (Settlement settlement : settlements) {
                        SettlementHistory settlementHistory = new SettlementHistory();

                        settlementHistory.setIndexNo(null);
                        settlementHistory.setSettlement(settlement);
                        settlementHistory.setAgreementNo(loanMap.get(settlement.getLoan()).getAgreementNo());

                        settlementHistories.add(settlementHistory);
                    }
                }
            } catch (DatabaseException ex) {
                Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        sortReceiptReceipts(settlementHistories);
        initSettlementAmount();
    }

    public void sortReceiptReceipts(List<SettlementHistory> settlementHistory) {
        List<SettlementHistory> panalties = new ArrayList<>();
        for (SettlementHistory panalty : settlementHistory) {
            if (panalty.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                panalties.add(panalty);
            }
        }
        settlementHistory.removeAll(panalties);

        Collections.sort(settlementHistory, new Comparator<SettlementHistory>() {
            @Override
            public int compare(SettlementHistory o1, SettlementHistory o2) {
                return ComparisonChain.start()
                        .compare(o1.getSettlement().getDueDate(), o2.getSettlement().getDueDate())
                        .compare(o1.getAgreementNo(), o2.getAgreementNo())
                        .compare(o1.getSettlement().getSettlementType().getPriority(), o2.getSettlement().getSettlementType().getPriority())
                        .result();
            }
        });
        settlementHistory.addAll(0, panalties);
    }

    private void initSettlementAmount() {
        //SET BALANCE AMOUNT

        double temp = settlementAmount;
        balanceAmount = 0.0;

        Iterator<SettlementHistory> iterator = settlementHistories.iterator();
        SettlementHistory settlementHistory;
        double settlement;
        double balance;
        while (iterator.hasNext()) {
            settlementHistory = iterator.next();
            balance = settlementHistory.getSettlement().getBalanceAmount();
            settlement = Math.min(balance, temp);
            temp = temp - settlement;
            settlementHistory.setSettlementAmount(settlement);

            balanceAmount = balanceAmount + balance;
        }

        if (temp > 0) {
            //OVER PAYMENT
            overAmount = temp;
        } else {
            overAmount = 0.0;
        }

        loanChanged();
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public Double getAfterBalanceAmount() {
        return balanceAmount - settlementAmount;
    }

    public Double getOverPaymentAmount() {
        return overAmount;
    }

    public void setSettlementAmount(Double settlementAmount) {
        this.settlementAmount = settlementAmount;

        initSettlementAmount();
    }
    //
    protected List<SettlementHistory> settlementHistories = new ArrayList<>();
    protected Double settlementAmount = 0.0;
    protected Double balanceAmount = 0.0;
    protected Double overAmount = 0.0;
    private static DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
    private CashierSession cashierSession;
    private SystemPayment systemPayment;
}
