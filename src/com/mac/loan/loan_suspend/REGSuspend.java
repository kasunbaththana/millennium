/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_suspend;

import com.mac.loan.loan_close.*;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import java.util.Collection;

/**
 *
 * @author thilanga
 */
public class REGSuspend extends AbstractGridObject {

    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Due Date", "dueDate"),
            new CTableColumn("Description", "description"),
            new CTableColumn("Amount", "amount"),
            new CTableColumn("Balance Amount", "balanceAmount"),
            new CTableColumn("Status", "status")
        });
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        initService();

        return new PCSuspend(this,serLoanSuspend);
    }

    @Override
    protected Collection getTableData() {
        initService();
//        try {
//            this.settlements = serLoanClose.getSettlements();
            return settlements;
//        } catch (DatabaseException ex) {
//            Logger.getLogger(REGSuspend.class.getName()).log(Level.SEVERE, null, ex);
//            return Collections.emptyList();
//        }
    }

    private void initService() {
        if (serLoanSuspend == null) {
            serLoanSuspend = new SERSuspend(this);
        }
    }

    public Collection getSettlements() {
        return settlements;
    }

    public void setSettlements(Collection settlements) {
        this.settlements = settlements;
    }
    
    
    private SERSuspend serLoanSuspend;
    private Collection settlements;
}
