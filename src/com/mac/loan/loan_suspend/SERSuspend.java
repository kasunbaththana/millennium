/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_suspend;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.zobject.Loan;
import com.mac.zsystem.transaction.account.object.Account;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thilanga
 */
public class SERSuspend extends AbstractService {

//    public static String LOAN_CLOSE = "LOAN CLOSE";
//    public static String LOAN_CANCEL = "LOAN CANCEL";
    public static String LOAN_SUSPEND_NAME = "Loan Suspend";
    //
    private Loan loan;
    private REGSuspend cPanel;

    public SERSuspend(Component component) {
        super(component);
    }

    private void initCPanel() {
        if (cPanel == null) {
            cPanel = (REGSuspend) getCPanel();
        }
    }

    public List<Settlement> getSettlements() throws DatabaseException {
        if (loan != null) {

            String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan=:LOANX";

            HashMap<String, Object> params = new HashMap();
            params.put("LOANX", loan == null ? 0 : loan.getIndexNo());

            return getDatabaseService().getCollection(hql, params);
        } else {
            return Collections.emptyList();
        }
    }

    public List<Loan> getLoans() {
        List<Loan> loan;
        try {
            String hql = "FROM com.mac.loan.zobject.Loan WHERE status='LOAN_START'";
            loan = getDatabaseService().getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }

    public List<Account> getAccounts() {
        List<Account> loan;
        try {
            loan = getDatabaseService().getCollection(Account.class);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }

    /**
     * Consider following transactions Loan Application/ Loan/ Receipts/ Voucher
     */
    public void closeLoanSuspend(Loan loan) throws DatabaseException {
//        SystemTransactions.insertTransaction(
//                getDatabaseService(),
//                SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE,
//                loan.getLoanReferenceNo(),
//                loan.getLoanDocumentNo(),
//                loan.getIndexNo(),
//                null,
//                loan.getClient().getCode(),
//                "Loan Suspend");
        System.out.println("loan.getIndexNo()______"+loan.getIndexNo());
        String call = "CALL z_loan_Suspend(" + loan.getIndexNo() + ")";
        getDatabaseService().callUpdateProcedure(call);
    }


    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;

//        initCPanel();
//        cPanel.refreshTable();
    }
}
