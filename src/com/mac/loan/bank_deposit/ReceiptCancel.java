/*
 *  ReceiptCancel.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 8:15:17 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.bank_deposit;

import com.mac.af.core.database.DatabaseException;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction_cancel.TransactionCancel;
import com.mac.zsystem.transaction.transaction_cancel.object.Transaction;

/**
 *
 * @author mohan
 */
public class ReceiptCancel extends TransactionCancel {

    @Override
    protected String getTransactionType() {
        return SystemTransactions.BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE;
    }

    @Override
    protected void cancelAdditional(Transaction transaction) throws DatabaseException {
        SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.TRANSACTION_CANCEL_TRANSACTION_CODE,
                transaction.getReferenceNo(),
                transaction.getDocumentNo(),
                transaction.getLoan().getIndexNo(),
                null,
                transaction.getLoan().getClient(),
                "Receipt Cancel");

        transaction.getLoan().setAvailableReceipt(true);
        getDatabaseService().save(transaction.getLoan());
    }
}
