/*
 *  TemperaryReceipt.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 8:22:02 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.temperary_receipt;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.loan.LoanStatus;
import com.mac.loan.TemperaryReceiptStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.ztemplate.receipt.AbstractReceipt;
import com.mac.loan.ztemplate.receipt.SERAbstractReceipt;
import com.mac.loan.ztemplate.receipt.custom_object.Receipt;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.TemperaryReceiptAccountInterface;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class TemperaryReceipt extends AbstractReceipt {

    @Override
    public List<Loan> getLoans() {
        String hql = "FROM com.mac.loan.zobject.Loan WHERE status=:STATUS";
        HashMap<String, Object> params = new HashMap<>();
        params.put("STATUS", LoanStatus.LOAN_START);

        List<Loan> loans;
        try {
            loans = getDatabaseService().getCollection(hql, params);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }

    @Override
    protected String getReferenceGeneratorType() {
        return ReferenceGenerator.RECEIPT;
    }

    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.TEMPORARY_RECEIPT_TRANSACTION_CODE;
    }

    @Override
    protected String getSettlementCreitOrDebit() {
        return SettlementCreditDebit.CREDIT;
    }

    @Override
    protected String getChequeType() {
        return ChequeType.CLIENT;
    }

    @Override
    protected int saveReceipt(String transactionTypeCode, Receipt receipt) throws DatabaseException {
        com.mac.loan.zobject.TemperaryReceipt temperaryReceipt = new com.mac.loan.zobject.TemperaryReceipt();

//        int transaction = getService().saveTransaction(transactionTypeCode, receipt);

        boolean isPaymentOk = getService().showPaymentDialog(transactionTypeCode, receipt);

        if (isPaymentOk) {
            int transactionIndex = getService().saveTransaction(transactionTypeCode, receipt);

            //SAVE PAYMENT
            getService().savePayment(transactionIndex, receipt);


            //SAVE ACCOUNT TRANSACTION
            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
            accountTransaction.beginAccountTransactionQueue();
            accountTransaction.addAccountTransactionQueue(
                    TemperaryReceiptAccountInterface.TEMP_RECEIPT_AMOUNT_CREDIT_CODE,
                    "Temp Receipt Amount",
                    receipt.getPaymentAmount(),
                    AccountTransactionType.AUTO);

            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, getTransactionTypeCode());





            //SAVE TEMPARARY RECEIPT
            temperaryReceipt.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
            temperaryReceipt.setLoan(receipt.getLoan());
            temperaryReceipt.setAmount(receipt.getPaymentAmount());
            temperaryReceipt.setTransaction(transactionIndex);
            temperaryReceipt.setStatus(TemperaryReceiptStatus.PENDING);

            getDatabaseService().save(temperaryReceipt);

            Map<String, Object> params = new HashMap<>();
            params.put("TRANSACTION_NO", transactionIndex);
            TransactionUtil.PrintReport(getDatabaseService(), transactionTypeCode, params);

            return AbstractRegistrationForm.SAVE_SUCCESS;
        } else {
            return AbstractRegistrationForm.SAVE_FAILED;
        }
    }

    @Override
    protected String getAccountSettingCode() {
        return TemperaryReceiptAccountInterface.TEMP_RECEIPT_AMOUNT_CREDIT_CODE;
    }

    @Override
    protected String getTransactionName() {
        return "Temperary Receipt";
    }

    @Override
    protected String getOverAmountSettlementType() {
        //THIS WILL NOT EXECUTE
        throw new UnsupportedOperationException("This will not execute");
    }

    @Override
    protected boolean isOverPayAvailable() {
        return true;
    }

    @Override
    protected Loan getModifiedLoan(Loan loan, Double afterBalance) {
        return loan;
    }
}
