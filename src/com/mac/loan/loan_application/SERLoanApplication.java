


/*
 *  SERLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.loan_application;

import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.concurrent.CExecutable;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.loan.LoanProfile;
import com.mac.loan.LoanSituation;
import com.mac.loan.LoanStatus;
import com.mac.loan.loan_application.object_creator.PCCustomerLoanInformation;
import com.mac.loan.zobject.Document;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanAdvanceSettlement;
import com.mac.loan.zobject.LoanAdvanceTrans;
import com.mac.loan.zobject.LoanBorrowerDetails;
import com.mac.loan.zobject.LoanBussinessInformation;
import com.mac.loan.zobject.LoanExistingFacility;
import com.mac.loan.zobject.LoanIncomeExpencess;
import com.mac.loan.zobject.LoanItemDetails;
import com.mac.loan.zobject.LoanMInstitution;
import com.mac.loan.zobject.LoanMortgageDetails;
import com.mac.loan.zobject.LoanMortgageProperty;
import com.mac.loan.zobject.LoanPersonalBusinessInfor;
import com.mac.loan.zobject.LoanPersonalFacilityInfor;
import com.mac.loan.zobject.LoanPersonalFinacInfor;
import com.mac.loan.zutil.default_charges.LoanDefaultChargesInformation;
import com.mac.registration.branch.object.Branch;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanApplicationAccountInterface;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import javax.imageio.ImageIO;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author user
 */
public class SERLoanApplication extends AbstractService {

    public SERLoanApplication(Component component) {
        super(component);
    }
PCCustomerLoanInformation cCustomerLoanInformation=new PCCustomerLoanInformation();
    public List getRegetRecentTransactions() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status1", LoanStatus.APPLICATION_PENDING);
            params.put("status2", LoanStatus.APPLICATION_SUSPEND);
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Loan where status=:status1 or status=:status2", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(Loan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getClients() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Client where active=true AND client =true ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public List getSupplier() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Client where active=true  AND supplier=true");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getLoanGroups() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanGroup");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getLoanTypes() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanType where isQuick<>'1' and ishp<>'1' and isMortgage<>'1' ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
    
    public List getLoanTypesEl() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanType where ishp='1' ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
    
     public List getLoanTypesMl() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanType where isMortgage='1' ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
     public List getLoanTypesPl() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanType where isPl='1' ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
     
  

    public void executeSave(final Object o) throws Exception {
        CExecutable<Void> executable = new CExecutable<Void>("Save Loan Application - " + ((Loan) o).getApplicationReferenceNo(), getCPanel()) {
            @Override
            public Void execute() throws Exception {
                Loan loanOld = (Loan) o;
                Loan loan = (Loan) o;
//                    System.out.println("loanOld.getLoanItems()_"+loan.getSoldItems().toString());

                //Document Save
                String loanIndex = loanOld.getApplicationReferenceNo().toString();
                File dcFile = new File("./" + "DOCUMENT");
                if (!dcFile.exists()) {
                    dcFile.mkdir();
                }
                File loanFile = new File("./" + "DOCUMENT/DOC_" + loanIndex);
                if (!loanFile.exists()) {
                    loanFile.mkdir();
                }
//                Set<LoanItem> items = loanOld.getLoanItems();
//                
//                for (LoanItem loanItem : items) {
//                    System.out.println("itemCode__"+loanItem.getItem());
//                    System.out.println("item Price__"+loanItem.getSalesPrice());
//                }
//                
           
            
                
                Set<Document> documents = loanOld.getDocuments();

                int count = 0;
                for (Document document : documents) {
                    count++;

                if(document.getReferenceLink()!=null){
                    try {
                        
                    File imageIcon = new File(document.getReferenceLink());

                    BufferedImage image = null;

                        image = ImageIO.read(imageIcon);

                        ImageIO.write(image, "jpg", new File("DOCUMENT/DOC_" + loanIndex + "/" + count + ".jpg"));

                    } catch (Exception e) {
                        e.printStackTrace();
                            mOptionPane.showMessageDialog(null, "Save Faild \n Please Select Image File", "Error", mOptionPane.ERROR_MESSAGE);

                    }
                }

                } //Document Save END


                List<LoanDefaultChargesInformation> defaultChargesInformations = loan.getDefaultChargesInformations();

                loan.setProfile(LoanProfile.LOAN);
                loan.setSituation(LoanSituation.LOAN_APPLICATION);

                if (TransactionUtil.isSendToApprove(getDatabaseService(), SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE)) {
                    loan.setStatus(LoanStatus.APPLICATION_PENDING);
                } else {
                    loan.setStatus(LoanStatus.APPLICATION_ACCEPT);
                }

                loan.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));

                loan.setPurpose("");
                loanApplicationDocumentNo(loan);
                
                
                loan = (Loan) getDatabaseService().save(loan);
              
                getDatabaseService().save(loan.getDocuments());
                getDatabaseService().save(loan.getClients());
              //  getDatabaseService().save(loan.getLoanItemDetailses());
                getDatabaseService().save(loan.getLoanBussinessInformations());
                

                 
                //save transaction info
                int transaction = SystemTransactions.insertTransaction(
                        getDatabaseService(),
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        loan.getApplicationReferenceNo(),
                        loan.getApplicationDocumentNo(),
                        loan.getIndexNo(),
                        null,
                        loan.getClient().getCode(),
                        null);
                saveCustomerInformation(loanOld,loan,transaction);
                //SETTLEMENT
                SystemSettlement systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();
                for (LoanDefaultChargesInformation loanDefaultChargesInformation : defaultChargesInformations) {
                    systemSettlement.addSettlementQueueForCharge(
                            loan.getApplicationTransactionDate(),
                            loan.getClient().getCode(),
                            loan.getIndexNo(),
                            null,
                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                            transaction,
                            loanDefaultChargesInformation.getChargeScheme().getName(),
                            loanDefaultChargesInformation.getChargeAmount(),
                            SystemSettlement.APPLICATION_CHARGE,
                            loanDefaultChargesInformation.getChargeScheme().getAccount()
                            );
                }
                
                systemSettlement.flushSettlementQueue(getDatabaseService());

                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_CREDIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_DEBIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.DEFAULT_CHARG_DEBIT,
                        "Default Charg Amount", 0.0, AccountTransactionType.AUTO);//because account error

                //LOAN DEFAULT CHARG CREDIT   its goin to receipt form
//                List<LoanDefaultChargesInformation> loanDefaultChargesInformations = loanOld.getDefaultChargesInformations();
//
//                double chargAmount = 0;
//                System.out.println("cccccccccccccc_" + loanDefaultChargesInformations);
//                for (LoanDefaultChargesInformation tblchargeScheme : loanDefaultChargesInformations) {
//                    accountTransaction.saveAccountTransaction(
//                            getDatabaseService(),
//                            transaction,
//                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
//                            AccountTransactionType.AUTO,
//                            AccountSettingCreditOrDebit.CREDIT,
//                            tblchargeScheme.getChargeAmount(),
//                            tblchargeScheme.getChargeScheme().getAccount(),
//                            tblchargeScheme.getChargeScheme().getName(),0 );
//                    chargAmount += tblchargeScheme.getChargeAmount();
//                }
//                //LOAN DEFAULT CHARG DEBIT
//                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.DEFAULT_CHARG_DEBIT,
//                        "Default Charg Amount", chargAmount, AccountTransactionType.AUTO);

//                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.DEFAULT_CHARG_DEBIT, "Default Charg Amount", chargAmount, AccountTransactionType.AUTO);
                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction,
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE);

               
                //REPORT
                Map<String, Object> params = new HashMap<>();
                params.put("TRANSACTION_NO", transaction);
                TransactionUtil.PrintReport(getDatabaseService(), SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE, params);

                return null;
            }
        };
        CApplication.getExecutionManager().execute(executable);
    }
    public void loanApplicationDocumentNo(Loan object) throws DatabaseException{
         Loan loan = (Loan) object;
        
        String branch_code = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
        
         Branch b = (Branch) getDatabaseService().initCriteria(Branch.class)
                        .add(Restrictions.eq("code", branch_code))
                        .uniqueResult();
         
        Integer l = ((Integer) getDatabaseService().initCriteria(Loan.class)
                            .setProjection(Projections.count("indexNo"))
                        .add(Restrictions.eq("branch", branch_code))
                        .add(Restrictions.ne("status", "CANCEL"))
                        .uniqueResult()).intValue()+1;
         
        String val = String.format("%03d", l);    
        String request_no = b.getPrefix()+"/"+val;
        
       loan.setApplicationDocumentNo(request_no);
        
    }

    public void executeUpdate(final Object o) throws DatabaseException {
        CExecutable<Void> executable = new CExecutable<Void>("Save Loan Application - " + ((Loan) o).getApplicationReferenceNo(), getCPanel()) {
            @Override
            public Void execute() throws Exception {
                Loan loan = (Loan) o;
                
                loan.setNote(null);
                loan.setSituation(LoanSituation.LOAN_APPLICATION);
                loan.setStatus(LoanStatus.APPLICATION_PENDING);
                loan.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));

                loan = (Loan) getDatabaseService().save(loan);
                getDatabaseService().save(loan.getDocuments());
                getDatabaseService().save(loan.getClients());
                  getDatabaseService().save(loan.getLoanItemDetailses());

                //save transaction history
                Integer transaction = SystemTransactions.getTransactionIndexNo(
                        getDatabaseService(),
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        loan.getApplicationReferenceNo());
                SystemTransactions.insertTransactionHistory(
                        getDatabaseService(),
                        transaction,
                        SystemTransactions.ACTION_EDIT,
                        null);

                //SETTLEMENT
                SystemSettlement systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();
                for (LoanDefaultChargesInformation loanDefaultChargesInformation : loan.getDefaultChargesInformations()) {
                    systemSettlement.addSettlementQueue(
                            loan.getApplicationTransactionDate(),
                            loan.getClient().getCode(),
                            loan.getIndexNo(),
                            null,
                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                            transaction,
                            loanDefaultChargesInformation.getChargeScheme().getName(),
                            loanDefaultChargesInformation.getChargeAmount(),
                            SystemSettlement.APPLICATION_CHARGE);
                }
                
                
                
                
                systemSettlement.flushSettlementQueue(getDatabaseService());

                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_CREDIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_DEBIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE);

                return null;
            }
        };
        CApplication.getExecutionManager().execute(executable);
    }

    public void saveCustomerInformation(Loan loan,Loan loansave,int transaction){
        
        try {
   
                System.out.println("oooooooooooooooooooooooooooo");
                System.out.println(loan.getLoanBussinessInformations());
                
                for(LoanBussinessInformation bussinessInformation:loan.getLoanBussinessInformations()){
                    LoanBussinessInformation bussinessInformation1=new LoanBussinessInformation();
                    
                    bussinessInformation1.setNatureOfBussiness(bussinessInformation.getNatureOfBussiness());
                    bussinessInformation1.setLoan(loansave);
                    bussinessInformation1.setClient(loansave.getClient().getCode());
                    bussinessInformation1.setName(bussinessInformation.getName());
                    bussinessInformation1.setAddress(bussinessInformation.getAddress());
                    bussinessInformation1.setHandleBy(bussinessInformation.getHandleBy());
                    bussinessInformation1.setBusinessPlace(bussinessInformation.getBusinessPlace());
                    bussinessInformation1.setDuration(bussinessInformation.getDuration());
                    bussinessInformation1.setStatus(bussinessInformation.getStatus());
                    
                    getDatabaseService().save(bussinessInformation1);
                }
                
                for(LoanBorrowerDetails borrowerDetails:loan.getLoanBorrowerDetails()){
                    LoanBorrowerDetails borrowerDetails1=new LoanBorrowerDetails();
                    
                    borrowerDetails1.setLoan(loansave);
                    borrowerDetails1.setClient(loansave.getClient().getCode());
                    borrowerDetails1.setRelationType(borrowerDetails.getRelationType());
                    borrowerDetails1.setFullName(borrowerDetails.getFullName());
                    borrowerDetails1.setNic(borrowerDetails.getNic());
                    borrowerDetails1.setMobileNo(borrowerDetails.getMobileNo());
                    borrowerDetails1.setEducationQl(borrowerDetails.getEducationQl());
                    borrowerDetails1.setGender(borrowerDetails.getGender());
                    borrowerDetails1.setDob(borrowerDetails.getDob());
                    borrowerDetails1.setReligion(borrowerDetails.getReligion());
                    borrowerDetails1.setOccupation(borrowerDetails.getOccupation());
                    borrowerDetails1.setEmail(borrowerDetails.getEmail());
                    
                    getDatabaseService().save(borrowerDetails1);
                }
                
                 for(LoanExistingFacility existingFacility:loan.getLoanExistingFacilitys()){
                    LoanExistingFacility existingFacility1=existingFacility;
                    
                    existingFacility1.setLoan(loansave);
                    existingFacility1.setClient(loansave.getClient().getCode());
               
                    getDatabaseService().save(existingFacility1);
                }
                 
                  for(LoanIncomeExpencess incomeExpencess:loan.getLoanIncomeExpencesses()){
                    LoanIncomeExpencess incomeExpencess1=incomeExpencess;
                    
                    incomeExpencess1.setLoan(loansave);
                    incomeExpencess1.setClient(loansave.getClient().getCode());
               
                    getDatabaseService().save(incomeExpencess1);
                }
                  
                  for(LoanIncomeExpencess incomeExpencess:loan.getLoanIncomeExpencesses()){
                    LoanIncomeExpencess incomeExpencess1=incomeExpencess;
                    
                    incomeExpencess1.setLoan(loansave);
                    incomeExpencess1.setClient(loansave.getClient().getCode());
               
                    getDatabaseService().save(incomeExpencess1);
                }
                  String status="DEFAULT";
                  for(LoanItemDetails itemDetails:loan.getLoanItemDetailses()){
                    LoanItemDetails loanItemDetails=itemDetails;
                    
                    loanItemDetails.setLoan(loansave);
                    loanItemDetails.setClient(loansave.getClient().getCode());
               
                    getDatabaseService().save(loanItemDetails);
                    
                    if(itemDetails.getBasicPayment()>0 && status.equals("DEFAULT")){
                        
                        LoanAdvanceSettlement advanceSettlement=new LoanAdvanceSettlement();
                        advanceSettlement.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                        advanceSettlement.setTransaction(transaction);
                        advanceSettlement.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
                        advanceSettlement.setCustomer(loansave.getClient().getCode());
                        advanceSettlement.setLoan(loansave.getIndexNo());
                        advanceSettlement.setAmount(itemDetails.getBasicPayment());
                        advanceSettlement.setStatus("ACTIVE");
                        
                      LoanAdvanceTrans advanceTrans =new LoanAdvanceTrans();
                    advanceTrans.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                    advanceTrans.setTransaction(transaction);
                    advanceTrans.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
                    advanceTrans.setCrAmount(itemDetails.getBasicPayment());
                    advanceTrans.setDrAmount(0.00);
                    advanceTrans.setLoan(loansave.getIndexNo());
                    advanceTrans.setClient(loansave.getClient().getCode());
                    advanceTrans.setStatus("ACTIVE");
                    advanceTrans.setIsDownpay(true);
                    
                    advanceSettlement = (LoanAdvanceSettlement) getDatabaseService().save(advanceSettlement);
                    advanceTrans.setTransSettle(advanceSettlement.getIndexNo());
                    getDatabaseService().save(advanceTrans);
                    status="SENT";
                    }
                }
                 //ML product
                  for(LoanMortgageDetails details:loan.getLoanMortgageDetailses()){
                      LoanMortgageDetails mortgageDetails= details;
                      
                      mortgageDetails.setLoan(loansave.getIndexNo());
                      
                      getDatabaseService().save(mortgageDetails);
                  }
                 for(LoanMortgageProperty lmp:loan.getLoanMortgagePropertys()){
                     LoanMortgageProperty mortgageProperty = lmp;
                     mortgageProperty.setLoan(loansave.getIndexNo());
                     getDatabaseService().save(mortgageProperty);
                 }
                 for(LoanMInstitution institution:loan.getLoanMInstitutions()){
                     LoanMInstitution mInstitution = institution;
                     
                     mInstitution.setLoan(loansave.getIndexNo());
                     
                     getDatabaseService().save(mInstitution);
                 }
                 //end ML product
                
                //Pl product
                 for(LoanPersonalBusinessInfor institution:loan.getLoanPersonalBusinessInfors()){
                     LoanPersonalBusinessInfor businessInfor = institution;
                     
                     businessInfor.setLoan(loansave.getIndexNo());
                     
                     getDatabaseService().save(businessInfor);
                 }
                  for(LoanPersonalFacilityInfor institution:loan.getLoanPersonalFacilityInforn()){
                     LoanPersonalFacilityInfor facilityInfor = institution;
                     
                     facilityInfor.setLoan(loansave.getIndexNo());
                     
                     getDatabaseService().save(facilityInfor);
                 }
                  for(LoanPersonalFinacInfor institution:loan.getLoanPersonalFinacInfors()){
                     LoanPersonalFinacInfor finacInfor = institution;
                     
                     finacInfor.setLoan(loansave.getIndexNo());
                     
                     getDatabaseService().save(finacInfor);
                 }
                 
                 
                 
                 
                //End Product
                
                
                
                

              


        } catch (Exception ex) {
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

   
        
        
        
    }
    public void executeDelete(Object o) throws DatabaseException {
        throw new UnsupportedOperationException("Delete not supported");
    }
}
