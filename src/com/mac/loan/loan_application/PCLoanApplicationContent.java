/*
 *  PCLoanApplicationContent.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 8:18:56 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.loan_application;

import com.mac.loan.loan_application.object_creator.PCLoanInit;
import com.mac.loan.loan_application.object_creator.PCLoan;
import com.mac.loan.loan_application.object_creator.PCClient;
import com.mac.loan.loan_application.object_creator.PCDocument;
import com.mac.loan.loan_application.object_creator.PCSalesItem;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.dash_board.client.ClientInformationDashBoard;
import com.mac.loan.loan_application.object_creator.PCCustomerLoanInformation;
import com.mac.loan.loan_application.object_creator.PCLoanEL;
import com.mac.loan.loan_application.object_creator.PCLoanItemDetails;
import com.mac.loan.loan_application.object_creator.PCLoanML;
import com.mac.loan.loan_application.object_creator.PCLoanPL;
import com.mac.loan.loan_application.object_creator.PCMortgageLoanInfor;
import com.mac.loan.loan_application.object_creator.PCPersonalLoanInfor;
import com.mac.loan.loan_application.object_creator.PCPersonalLoanjointInfor;
import com.mac.loan.zobject.Client;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanAdvanceTrans;
import com.mac.loan.zobject.LoanItemDetails;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author user
 */
public class PCLoanApplicationContent extends AbstractObjectCreator<Loan> {

    
    /**
     * Creates new form PCLoanApplicationContent
     */
    public PCLoanApplicationContent() {
        initComponents();
        initOthers();
//        jPanel3.setVisible(false);
    }

   
    
    @SuppressWarnings("unchecked")
    private void initOthers() {
        serLoanApplication = new SERLoanApplication(this);
        jPanel4.setVisible(false);
        
        pCLoanApplication = new PCLoan();
        pCLoanApplicationEL = new PCLoanEL();
        pCLoanApplicationInit = new PCLoanInit();
        pCCustomerLoanInformation = new PCCustomerLoanInformation();
        cLoanItemDetails = new PCLoanItemDetails();
        cMortgageLoanInfor =new PCMortgageLoanInfor();
        cLoanML = new PCLoanML();
        cLoanPL = new PCLoanPL();
        cPersonalLoanInfor = new PCPersonalLoanInfor();
        cPersonalLoanjointInfor = new PCPersonalLoanjointInfor();

        pnlLoanApplication.add(pCLoanApplication);
        pnlLoanApplicationInit.add(pCLoanApplicationInit);
        pnlLoanApplicationRight.add(pCCustomerLoanInformation);

        document = new PCDocument();
        client = new PCClient() {
            @Override
            public List<Client> getClient() {
                return serLoanApplication.getClients();
            }
        };

        tblDocument.setObjectCreator(document);
        tblGuarantor.setObjectCreator(client);
        

        tblDocument.setDescrption("Document");
        tblGuarantor.setDescrption("Guarantor");



        final JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem deleteItem = new JMenuItem("Guarantor History");
        deleteItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String toString = tblGuarantor.tblInput.getValueAt(tblGuarantor.tblInput.getSelectedRow(), 0).toString();
//                String toString ="";

                if (toString != null) {
                    ClientInformationDashBoard panel = new ClientInformationDashBoard(toString);
                    JDialog dialog = new JDialog();
                    dialog.add(panel);
                    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                    dialog.setBounds(0, 0, screenSize.width - 50, screenSize.height - 200);
                    dialog.setLocationRelativeTo(null);
                    dialog.setVisible(true);
                } else {
                    mOptionPane.showMessageDialog(null, "Please Select Client", "Selection ..", mOptionPane.WARNING_MESSAGE);
                }


            }
        });
        popupMenu.add(deleteItem);
        tblGuarantor.tblInput.setComponentPopupMenu(popupMenu);
        
        
        pcSalesItem = new PCSalesItem(this) {
            @Override
            protected CPanel getCPanel() {
                return PCLoanApplicationContent.this.getCPanel();
            }
        };
        
     
     
        
//        tblAddingTable.setCValue(new ArrayList());
        tblItemDetails.setObjectCreator(pcSalesItem);
        tblItemDetails.setDescrption("Item");
        
          tblItemDetails.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                System.out.println("dddddddddddddddddddddddddddd");
               
                calculateLoanAmount();
                
            }
        });

         

    }
    private double total_basic_payment=0.0;
     public double getBasicAmount(){
     
      try {
          Loan temp_loan = (Loan) pCLoanApplicationEL.getValue();
           Criteria  cr =   CPanel.GLOBAL.getDatabaseService().initCriteria(LoanAdvanceTrans.class).
                    add(Restrictions.eq("client", temp_loan.getClient().getCode())).
                    add(Restrictions.eq("isDownpay", true)).
                    setProjection(Projections.sum("crAmount"));
           
              Criteria  dr =   CPanel.GLOBAL.getDatabaseService().initCriteria(LoanAdvanceTrans.class).
                    add(Restrictions.eq("client", temp_loan.getClient().getCode())).
                      add(Restrictions.eq("isDownpay", true)).
                    setProjection(Projections.sum("drAmount"));
              
              double cr_amount = dr.uniqueResult()==null?0:(double) dr.uniqueResult();
              double dr_amount = cr.uniqueResult()==null?0:(double) cr.uniqueResult();
          
           double advance_amount =  (cr_amount - dr_amount) - total_basic_payment;
           
           if(advance_amount<0){
               return 0.0;
           }else{
                return (advance_amount);
           }
      } catch (Exception e) {
          e.printStackTrace();
      }
   
      return (0.0);
  }
     

    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        pnlLeft = new javax.swing.JPanel();
        pnlLoanApplication = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        pnlLoanApplicationInit = new javax.swing.JPanel();
        pnlRight = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        tblDocument = new com.mac.af.component.derived.input.table.CIAddingTable();
        jPanel2 = new javax.swing.JPanel();
        tblGuarantor = new com.mac.af.component.derived.input.table.CIAddingTable();
        pnlLoanApplicationRight = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        tblItemDetails = new com.mac.af.component.derived.input.table.CIAddingTable();

        jSplitPane1.setDividerLocation(500);

        pnlLoanApplication.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 499, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 581, Short.MAX_VALUE)
        );

        pnlLoanApplication.add(jPanel3, java.awt.BorderLayout.CENTER);

        pnlLoanApplicationInit.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout pnlLeftLayout = new javax.swing.GroupLayout(pnlLeft);
        pnlLeft.setLayout(pnlLeftLayout);
        pnlLeftLayout.setHorizontalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlLoanApplicationInit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlLoanApplication, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlLeftLayout.setVerticalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                .addComponent(pnlLoanApplicationInit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlLoanApplication, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(pnlLeft);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Document Details"));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tblDocument, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(tblDocument, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Guarantor Details"));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(tblGuarantor, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 8, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(tblGuarantor, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlLoanApplicationRight.setLayout(new java.awt.BorderLayout());

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Item Details"));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tblItemDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tblItemDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pnlRightLayout = new javax.swing.GroupLayout(pnlRight);
        pnlRight.setLayout(pnlRightLayout);
        pnlRightLayout.setHorizontalGroup(
            pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlRightLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pnlRightLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlRightLayout.createSequentialGroup()
                    .addComponent(pnlLoanApplicationRight, javax.swing.GroupLayout.DEFAULT_SIZE, 409, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        pnlRightLayout.setVerticalGroup(
            pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRightLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 296, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlRightLayout.createSequentialGroup()
                    .addComponent(pnlLoanApplicationRight, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 115, Short.MAX_VALUE)))
        );

        jSplitPane1.setRightComponent(pnlRight);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlLoanApplication;
    private javax.swing.JPanel pnlLoanApplicationInit;
    private javax.swing.JPanel pnlLoanApplicationRight;
    private javax.swing.JPanel pnlRight;
    private com.mac.af.component.derived.input.table.CIAddingTable tblDocument;
    private com.mac.af.component.derived.input.table.CIAddingTable tblGuarantor;
    private com.mac.af.component.derived.input.table.CIAddingTable tblItemDetails;
    // End of variables declaration//GEN-END:variables
    private Loan loan;
    private PCLoan pCLoanApplication;
    private PCLoanEL pCLoanApplicationEL;
    private PCLoanInit pCLoanApplicationInit;
    private PCCustomerLoanInformation pCCustomerLoanInformation;
    private PCLoanItemDetails cLoanItemDetails;
    private PCMortgageLoanInfor cMortgageLoanInfor;
    private PCLoanML cLoanML;
    private PCClient client;
    private PCDocument document;
    private PCSalesItem pcSalesItem;
    private SERLoanApplication serLoanApplication;
    private PCLoanPL cLoanPL;
    private PCPersonalLoanInfor cPersonalLoanInfor;
    private PCPersonalLoanjointInfor cPersonalLoanjointInfor;

    @Override
    public void setNewMood() {
        pCLoanApplication.setEditMood();
        pCLoanApplicationInit.setIdleMood();
    }

    @Override
    public void setEditMood() {
        pCLoanApplication.setEditMood();
        pCLoanApplicationInit.setIdleMood();
    }

    @Override
    public void setIdleMood() {
        pCLoanApplication.setIdleMood();
        pCLoanApplicationInit.setIdleMood();
    }

    @Override
    public void resetFields() {
        pCLoanApplication.resetFields();
        pCLoanApplicationInit.resetFields();
    }

    @Override
    protected void setValueAbstract(Loan t) throws ObjectCreatorException {
        this.loan = t;
    }

    @Override
    protected Loan getValueAbstract() throws ObjectCreatorException {
        return loan;
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        
        if(loan.isElProduct()){
           pnlLoanApplication.removeAll();
           pnlLoanApplication.add(pCLoanApplicationEL); 
           pnlLoanApplicationRight.removeAll();
           pnlLoanApplicationRight.add(jPanel4);
           jPanel4.setVisible(true);
           pCLoanApplicationEL.setValue(loan);
           cLoanItemDetails.setValue(loan);
          // pCLoanApplicationEL.txtLoanAmount.setValue(9000);
        }else if(loan.isMlProduct()){
           pnlLoanApplication.removeAll();
           pnlLoanApplication.add(cLoanML); 
           pnlLoanApplicationRight.removeAll();
           pnlLoanApplicationRight.add(cMortgageLoanInfor);
           cLoanML.setValue(loan);
           cMortgageLoanInfor.setValue(loan);
        }else if(loan.isPlProduct()){
           pnlLoanApplication.removeAll();
           pnlLoanApplication.add(cLoanPL); 
           pnlLoanApplicationRight.removeAll();
           cLoanPL.setValue(loan);
           if(loan.isIndividual()){
               
                pnlLoanApplicationRight.add(cPersonalLoanInfor);
                cPersonalLoanInfor.setValue(loan);
           }else if(loan.isJoin()){
                pnlLoanApplicationRight.add(cPersonalLoanjointInfor);
                cPersonalLoanjointInfor.setValue(loan);
           }
           
       ;
           
        }else{
             pCLoanApplication.setValue(loan);
             pCCustomerLoanInformation.setValue(loan);
           
        }
        
        
        pCLoanApplicationInit.setValue(loan);

        
        //table
        tblDocument.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Description", "description"),
            new CTableColumn("Owned Date", "ownedDate"),
            new CTableColumn("Note", "note")
        }));
        tblDocument.setCValue(loan.getDocuments());

        tblGuarantor.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Mobile", new String[]{"mobile"}),
            new CTableColumn("Telephone 1", new String[]{"telephone1"}),
            new CTableColumn("Telephone 2", new String[]{"telephone2"}),
            new CTableColumn("Fax", new String[]{"fax"}),
            new CTableColumn("E mail", new String[]{"email"})
        }));
        tblGuarantor.setCValue(loan.getClients());
        
        
        
        tblItemDetails.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Product Name", "productName"),
            new CTableColumn("Dealer Pay", "dealerPayment"),
            new CTableColumn("Product Val", "productValue"),
            new CTableColumn("Ep", "economicProfit"),
            new CTableColumn("Basic", "basicPayment"),
            new CTableColumn("Tot Ex", "totOtherPay"),
            new CTableColumn("Facility Amt", "facilityAmount"),
        }));
        
        tblItemDetails.setCValue(loan.getLoanItemDetailses());
       
       
        
        
        
    }

    @Override
    protected void initObject() throws ObjectCreatorException {
        if(loan.isElProduct()){
        
            loan = (Loan) pCLoanApplicationEL.getValue();
            loan = (Loan) pCLoanApplicationInit.getValue();
            loan.setLoanItemDetailses(new HashSet((Collection) tblItemDetails.getCValue()));
            //loan = (Loan) pCCustomerLoanInformation.getValue();
            

        }else if(loan.isMlProduct()){
            loan = (Loan) cLoanML.getValue();
            loan = (Loan) pCLoanApplicationInit.getValue();
            loan = (Loan) cMortgageLoanInfor.getValue();
            
        }else if(loan.isPlProduct()){
             loan = (Loan) cLoanPL.getValue();
            loan = (Loan) pCLoanApplicationInit.getValue();
            
            
            if(loan.isIndividual()){
               loan = (Loan) cPersonalLoanInfor.getValue();
               
           }else if(loan.isJoin()){
               loan = (Loan) cPersonalLoanjointInfor.getValue();
           
           }
             
         }else{
            loan = (Loan) pCLoanApplication.getValue();
            loan = (Loan) pCLoanApplicationInit.getValue();
            loan = (Loan) pCCustomerLoanInformation.getValue();
        }
        
        

       
        loan.setDocuments(new HashSet((Collection) tblDocument.getCValue()));
        
        loan.setClients(new HashSet((Collection) tblGuarantor.getCValue()));
        
        
       
        
    }
    
       private void calculateLoanAmount() {
        Collection<LoanItemDetails> items = tblItemDetails.getCValue();
        double itemTotal = 0.0;
        double basicTotal = 0.0;
        for (LoanItemDetails item : items) {
            itemTotal += item.getFacilityAmount();
            basicTotal += item.getBasicPayment();
        }
          total_basic_payment = basicTotal;
        pCLoanApplicationEL.txtLoanAmount.setCValue(itemTotal);
    }
    
    
}
