/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_application.object_creator;

import com.mac.loan.loan_application.SERLoanApplication;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanBorrowerDetails;
import com.mac.loan.zobject.LoanBussinessInformation;
import com.mac.loan.zobject.LoanExistingFacility;
import com.mac.loan.zobject.LoanIncomeExpencess;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author soft-master
 */
public class PCCustomerLoanInformation extends DefaultObjectCreator<Loan> {

    /**
     * Creates new form PCLoan
     */
    public PCCustomerLoanInformation() {
        initComponents();
        initOthers();
        
    }

    @Action
    public void doRefreshPaymentSchedule() {
        try {
      
           
        } catch (Exception ex) {
            Logger.getLogger(com.mac.loan.hp_loan_application.object_creator.PCLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doRefreshDefaultCharges() {
        
    }

   

    @Override
    public void setEditMood() {
       
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
      
        radioButtonGroup = new CRadioButtonGroup();
        radioButtonGroup2 = new CRadioButtonGroup();
        
        rdoGrowing.setValue("GROWING");
        rdoStrong.setValue("STRONG");
        rdoStabalized.setValue("STABALIZED");
        rdoProtentialToGrow.setValue("PTOTENTIAL_TO_GROW");
        
        rdoFemale.setValue("FEMALE");
        rdoMale.setValue("MALE");

        radioButtonGroup.addRadioButton(rdoStrong);
        radioButtonGroup.addRadioButton(rdoStabalized);
        radioButtonGroup.addRadioButton(rdoProtentialToGrow);
        radioButtonGroup.addRadioButton(rdoGrowing);
        
        radioButtonGroup2.addRadioButton(rdoFemale);
        radioButtonGroup2.addRadioButton(rdoMale);
        pCFacility = new PCFacility();
         tblExisting.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Org Name",new String[]{"orgName"}, true),
            new CTableColumn("Rental Type", new String[]{"rentalType"}, true),
            new CTableColumn("Amount", new String[]{"amount"}, true),
            new CTableColumn("Rental Amount", new String[]{"rentalAmount"}, true),
            new CTableColumn("Balance", new String[]{"balance"}, true)
        }));
         
         tblExisting.setObjectCreator(pCFacility);
            tblExisting.setDescrption("Facility");
            
            txtTotalIncome.setEditable(false);
                txtTotalExpenses.setEditable(false);
                txtPandL.setEditable(false);
                txtFtotalIncome.setEditable(false);
                txtFtotalExpenses.setEditable(false);
                tctFNetAmount.setEditable(false);
                
                btnBCal.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                        calculation1();
                }
        });
                
                btnFCal.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                        calculation2();
                }
        });
                tblExisting.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                calculateFacilityAmount();
            }
        });
    }

   
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cIStringField1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        cRadioButtonGroup1 = new com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        lblInfo = new com.mac.af.component.derived.display.label.CDLabel();
        txtNatureOFBusiness = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBusinessName = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtBusinessAddress = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBusinessHandleBy = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtBusinessPlace = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBusinessDuration = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        jSeparator1 = new javax.swing.JSeparator();
        rdoStabalized = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdoGrowing = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdoStrong = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdoProtentialToGrow = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        jPanel3 = new javax.swing.JPanel();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBFullName = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        jSeparator2 = new javax.swing.JSeparator();
        cboRelationShip = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getRelationShipType();
            }
        }
        ;
        cLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBnic = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBMobile = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtBEduQulification = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel17 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel18 = new com.mac.af.component.derived.display.label.CDLabel();
        rdoFemale = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdoMale = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        cLabel19 = new com.mac.af.component.derived.display.label.CDLabel();
        dtDOB = new com.mac.af.component.derived.input.textfield.CIDateField();
        cLabel20 = new com.mac.af.component.derived.display.label.CDLabel();
        cboReligion = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getReligon();
            }
        };
        cLabel21 = new com.mac.af.component.derived.display.label.CDLabel();
        txtOccupation = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel22 = new com.mac.af.component.derived.display.label.CDLabel();
        txtEmail = new com.mac.af.component.derived.input.textfield.CIStringField();
        jPanel2 = new javax.swing.JPanel();
        tblExisting = new com.mac.af.component.derived.input.table.CIAddingTable();
        jPanel4 = new javax.swing.JPanel();
        cLabel23 = new com.mac.af.component.derived.display.label.CDLabel();
        jSeparator3 = new javax.swing.JSeparator();
        cLabel24 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBincome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel25 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBOtherIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel26 = new com.mac.af.component.derived.display.label.CDLabel();
        txtRowMaterial = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel27 = new com.mac.af.component.derived.display.label.CDLabel();
        txtSalary = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel28 = new com.mac.af.component.derived.display.label.CDLabel();
        txtElectricity = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel29 = new com.mac.af.component.derived.display.label.CDLabel();
        txtOtherExpenses = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel30 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel31 = new com.mac.af.component.derived.display.label.CDLabel();
        txtfamilyIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel32 = new com.mac.af.component.derived.display.label.CDLabel();
        txtSpouseIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel33 = new com.mac.af.component.derived.display.label.CDLabel();
        txtOtherIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel34 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel35 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalExpenses = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel36 = new com.mac.af.component.derived.display.label.CDLabel();
        jSeparator5 = new javax.swing.JSeparator();
        txtPandL = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel37 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel38 = new com.mac.af.component.derived.display.label.CDLabel();
        txtFtotalIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel39 = new com.mac.af.component.derived.display.label.CDLabel();
        txtFoods = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel40 = new com.mac.af.component.derived.display.label.CDLabel();
        txtFElecricity = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel41 = new com.mac.af.component.derived.display.label.CDLabel();
        txtWater = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel42 = new com.mac.af.component.derived.display.label.CDLabel();
        txtChildEdu = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel43 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTpBill = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel44 = new com.mac.af.component.derived.display.label.CDLabel();
        txtFacilityPayment = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtFOtherEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel45 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel46 = new com.mac.af.component.derived.display.label.CDLabel();
        txtFtotalExpenses = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel47 = new com.mac.af.component.derived.display.label.CDLabel();
        tctFNetAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel48 = new com.mac.af.component.derived.display.label.CDLabel();
        btnBCal = new javax.swing.JToggleButton();
        btnFCal = new javax.swing.JToggleButton();

        cIStringField1.setText("cIStringField1");

        cLabel1.setText("Nature Of Business:");

        cLabel2.setText(" Business Name:");

        cLabel3.setText(" Business Address:");

        cLabel5.setText("Handle By:");

        cLabel6.setText("Business Place:");

        cLabel7.setText("Duration:");

        cLabel8.setText("Status:");

        cLabel4.setText("Business Details");

        rdoStabalized.setText("Stablized");

        rdoGrowing.setText("Growing");

        rdoStrong.setText("Strong");

        rdoProtentialToGrow.setText("Protential to grow");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(65, 65, 65))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(7, 7, 7)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtBusinessAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtBusinessName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtNatureOFBusiness, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtBusinessHandleBy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtBusinessPlace, javax.swing.GroupLayout.DEFAULT_SIZE, 513, Short.MAX_VALUE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtBusinessDuration, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(rdoStrong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(rdoStabalized, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(rdoProtentialToGrow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(rdoGrowing, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jSeparator1))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNatureOFBusiness, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusinessName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusinessAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusinessHandleBy, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusinessPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBusinessDuration, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rdoStrong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rdoStabalized, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rdoProtentialToGrow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rdoGrowing, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(163, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Business", jPanel1);

        cLabel12.setText("Ralationship Type:");

        cLabel13.setText("Joint Borrower Details");

        cLabel14.setText("Full Name:");

        cLabel15.setText("Nic:");

        cLabel16.setText("Mobile No:");

        cLabel17.setText("Education Ql:");

        cLabel18.setText("Gender:");

        rdoFemale.setText("Female");

        rdoMale.setText("Male");

        cLabel19.setText("DOB:");

        cLabel20.setText("Religion:");

        cLabel21.setText("Occupation:");

        cLabel22.setText("Email:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtBFullName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(txtBnic, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(cboRelationShip, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBEduQulification, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtOccupation, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(rdoMale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37)
                                .addComponent(rdoFemale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(dtDOB, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboReligion, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(txtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboRelationShip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBFullName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBnic, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBEduQulification, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rdoFemale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(rdoMale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dtDOB, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboReligion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOccupation, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(122, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Borrower", jPanel3);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tblExisting, javax.swing.GroupLayout.DEFAULT_SIZE, 590, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tblExisting, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(226, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Facility", jPanel2);

        cLabel23.setText("Business Income ");
        cLabel23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        cLabel24.setText("Main Business Income:");

        txtBincome.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtBincome.setMargin(new java.awt.Insets(0, 0, 0, 0));

        cLabel25.setText("Other Income:");

        txtBOtherIncome.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel26.setText("Row Material:");

        txtRowMaterial.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel27.setText("Salary:");

        txtSalary.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel28.setText("Electricity:");

        txtElectricity.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtElectricity.setText("0.0");

        cLabel29.setText("Other Expenses:");

        txtOtherExpenses.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel30.setForeground(new java.awt.Color(0, 0, 204));
        cLabel30.setText("Total Income:");
        cLabel30.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        txtTotalIncome.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel31.setText("Business P&L:");

        txtfamilyIncome.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel32.setText("Spouse Income:");

        txtSpouseIncome.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel33.setText("Other Income:");

        txtOtherIncome.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel34.setText("Expenses");
        cLabel34.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        cLabel35.setForeground(new java.awt.Color(204, 0, 0));
        cLabel35.setText("Total Expenses:");
        cLabel35.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        txtTotalExpenses.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel36.setForeground(new java.awt.Color(0, 153, 0));
        cLabel36.setText("Business Profit & Lost");
        cLabel36.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        txtPandL.setForeground(new java.awt.Color(0, 153, 0));
        txtPandL.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPandL.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        cLabel37.setText("Family Income");
        cLabel37.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        cLabel38.setText("Total Income:");
        cLabel38.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        txtFtotalIncome.setForeground(new java.awt.Color(0, 153, 0));
        txtFtotalIncome.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel39.setText("Foods:");

        txtFoods.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel40.setText("Electricity:");

        txtFElecricity.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtFElecricity.setText("0.0");

        cLabel41.setText("Water:");

        txtWater.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel42.setText("Child Edu:");

        txtChildEdu.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel43.setText("Tp Bill:");

        txtTpBill.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel44.setText("Facility Payment:");

        txtFacilityPayment.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtFOtherEx.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel45.setText("Other Expenses:");

        cLabel46.setText("Total Exp:");
        cLabel46.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        txtFtotalExpenses.setForeground(new java.awt.Color(204, 0, 0));
        txtFtotalExpenses.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel47.setText("Net Amount:");
        cLabel47.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        tctFNetAmount.setForeground(new java.awt.Color(153, 0, 153));
        tctFNetAmount.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        tctFNetAmount.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cLabel48.setText("Expenses");
        cLabel48.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        btnBCal.setText("Calculate");

        btnFCal.setText("Calculate");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnBCal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPandL, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator3)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(cLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(14, 14, 14)
                                .addComponent(txtRowMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtElectricity, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtOtherExpenses, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 2, Short.MAX_VALUE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cLabel47, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cLabel46, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cLabel38, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                    .addComponent(cLabel33, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cLabel32, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtSpouseIncome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtOtherIncome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtFtotalIncome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtFtotalExpenses, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tctFNetAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addGap(15, 15, 15)
                                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                                .addComponent(cLabel43, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtTpBill, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel4Layout.createSequentialGroup()
                                                .addComponent(cLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtFoods, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                                .addComponent(cLabel40, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtFElecricity, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                                .addComponent(cLabel41, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtWater, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                                .addComponent(cLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtChildEdu, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtFacilityPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtFOtherEx, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(cLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtfamilyIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtBincome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(cLabel23, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtBOtherIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtTotalIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(35, 35, 35))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(cLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtTotalExpenses, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addComponent(jSeparator5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(225, 225, 225)
                    .addComponent(btnFCal, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(297, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBincome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBOtherIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtRowMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtElectricity, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtOtherExpenses, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalExpenses, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtPandL, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnBCal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFoods, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFElecricity, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtWater, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtChildEdu, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTpBill, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFacilityPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtFOtherEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtfamilyIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSpouseIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtOtherIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFtotalIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFtotalExpenses, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tctFNetAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 42, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                    .addContainerGap(380, Short.MAX_VALUE)
                    .addComponent(btnFCal)
                    .addGap(20, 20, 20)))
        );

        jTabbedPane1.addTab("Income & Exp", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 615, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnBCal;
    private javax.swing.JToggleButton btnFCal;
    private com.mac.af.component.derived.input.textfield.CIStringField cIStringField1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cLabel17;
    private com.mac.af.component.derived.display.label.CDLabel cLabel18;
    private com.mac.af.component.derived.display.label.CDLabel cLabel19;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel20;
    private com.mac.af.component.derived.display.label.CDLabel cLabel21;
    private com.mac.af.component.derived.display.label.CDLabel cLabel22;
    private com.mac.af.component.derived.display.label.CDLabel cLabel23;
    private com.mac.af.component.derived.display.label.CDLabel cLabel24;
    private com.mac.af.component.derived.display.label.CDLabel cLabel25;
    private com.mac.af.component.derived.display.label.CDLabel cLabel26;
    private com.mac.af.component.derived.display.label.CDLabel cLabel27;
    private com.mac.af.component.derived.display.label.CDLabel cLabel28;
    private com.mac.af.component.derived.display.label.CDLabel cLabel29;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel30;
    private com.mac.af.component.derived.display.label.CDLabel cLabel31;
    private com.mac.af.component.derived.display.label.CDLabel cLabel32;
    private com.mac.af.component.derived.display.label.CDLabel cLabel33;
    private com.mac.af.component.derived.display.label.CDLabel cLabel34;
    private com.mac.af.component.derived.display.label.CDLabel cLabel35;
    private com.mac.af.component.derived.display.label.CDLabel cLabel36;
    private com.mac.af.component.derived.display.label.CDLabel cLabel37;
    private com.mac.af.component.derived.display.label.CDLabel cLabel38;
    private com.mac.af.component.derived.display.label.CDLabel cLabel39;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel40;
    private com.mac.af.component.derived.display.label.CDLabel cLabel41;
    private com.mac.af.component.derived.display.label.CDLabel cLabel42;
    private com.mac.af.component.derived.display.label.CDLabel cLabel43;
    private com.mac.af.component.derived.display.label.CDLabel cLabel44;
    private com.mac.af.component.derived.display.label.CDLabel cLabel45;
    private com.mac.af.component.derived.display.label.CDLabel cLabel46;
    private com.mac.af.component.derived.display.label.CDLabel cLabel47;
    private com.mac.af.component.derived.display.label.CDLabel cLabel48;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup cRadioButtonGroup1;
    public com.mac.af.component.derived.input.combobox.CIComboBox cboRelationShip;
    public com.mac.af.component.derived.input.combobox.CIComboBox cboReligion;
    public com.mac.af.component.derived.input.textfield.CIDateField dtDOB;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblInfo;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoFemale;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoGrowing;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoMale;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoProtentialToGrow;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoStabalized;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoStrong;
    private com.mac.af.component.derived.input.table.CIAddingTable tblExisting;
    public com.mac.af.component.derived.input.textfield.CIDoubleField tctFNetAmount;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBEduQulification;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBFullName;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBMobile;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtBOtherIncome;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtBincome;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBnic;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBusinessAddress;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBusinessDuration;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBusinessHandleBy;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBusinessName;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBusinessPlace;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtChildEdu;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtElectricity;
    public com.mac.af.component.derived.input.textfield.CIStringField txtEmail;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtFElecricity;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtFOtherEx;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtFacilityPayment;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtFoods;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtFtotalExpenses;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtFtotalIncome;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNatureOFBusiness;
    public com.mac.af.component.derived.input.textfield.CIStringField txtOccupation;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtOtherExpenses;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtOtherIncome;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtPandL;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtRowMaterial;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtSalary;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtSpouseIncome;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalExpenses;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalIncome;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtTpBill;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtWater;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtfamilyIncome;
    // End of variables declaration//GEN-END:variables
    public CRadioButtonGroup radioButtonGroup;
    public CRadioButtonGroup radioButtonGroup2;
    public PCFacility pCFacility;

      public List getRelationShipType(){
        
        
        
        List<String> nliList =new ArrayList<>();
        
        nliList.add("father");
        nliList.add("son");
        nliList.add("husband");
        nliList.add("brother");
        nliList.add("grandfather");
        nliList.add("grandson");
        nliList.add("uncle");
        nliList.add("nephew");
        nliList.add("cousin");
        
        nliList.add("mother");
        nliList.add("daughter");
        nliList.add("wife");
        nliList.add("sister");
        nliList.add("grandmother");
        nliList.add("granddaughter");
        nliList.add("aunt");
        nliList.add("niece");
     
        return nliList;

    }
      private void calculation1(){
          
        double bIncome = txtBincome.getCValue();
        double botherin =  txtBOtherIncome.getCValue();
        double bMatre =  txtRowMaterial.getCValue();
        
        double total_in = bIncome + botherin + bMatre  ;
          
        txtTotalIncome.setCValue(total_in);
        
        double ESalary = txtSalary.getCValue();
        double EEle =  txtElectricity.getCValue();
        double EOther =  txtOtherExpenses.getCValue();
        
        
        
        double total_ex = ESalary + EEle + EOther ;
        double total_pnl = total_in - total_ex ;
        
        
        
        txtTotalExpenses.setCValue(total_ex);
        txtPandL.setCValue(total_pnl);
        txtfamilyIncome.setCValue(total_pnl);
          
      }
      
      private void calculation2(){
          
         double fincome= txtfamilyIncome.getCValue();
         double fsincome= txtSpouseIncome.getCValue();
         double foincome= txtOtherIncome.getCValue();
          
         double income_total = fincome + fsincome + foincome;
         
          txtFtotalIncome.setCValue(income_total);
          
          
          double ffood= txtFoods.getCValue();
         double fele= txtFElecricity.getCValue();
         double fwater= txtChildEdu.getCValue();
         double ftpBill= txtTpBill.getCValue();
         double ffacility= txtFacilityPayment.getCValue();
         double fotherex= txtFOtherEx.getCValue();
          
         double ex_total = ffood + fele + fwater + ftpBill + ffacility + fotherex;
         
          txtFtotalExpenses.setCValue(ex_total);
          
          double net_amount = income_total - ex_total;
          tctFNetAmount.setCValue(net_amount);
          
          
      }
      public void calculateFacilityAmount(){
          double facility=0.0;
          List<LoanExistingFacility> loan_list = (List<LoanExistingFacility>) tblExisting.getCValue();
          for(LoanExistingFacility existingFacility: loan_list){
              facility +=existingFacility.getRentalAmount();
          }
          
          txtFacilityPayment.setCValue(facility);
      }
      
       public List getReligon(){
        
        
        
        List<String> nliList =new ArrayList<>();
        
        nliList.add("Buddhism");
        nliList.add("Christianity");
        nliList.add("Hinduism");
        nliList.add("Islam");
        nliList.add("Judaism");
        nliList.add("Myth");
      
     
        return nliList;

    }

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList();
          
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(
                txtTotalIncome,
                txtTotalExpenses,
                txtPandL,
                txtFtotalIncome,
                txtFtotalExpenses,
                tctFNetAmount
                );

    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
              );
    }

    @Override
    protected Class getObjectClass() {
        return Loan.class;
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
                txtTotalIncome,
                txtTotalExpenses,
                txtPandL,
                txtFtotalIncome,
                txtFtotalExpenses,
                tctFNetAmount
                );
    }

    @Override
    protected void afterInitObject(Loan object) throws ObjectCreatorException {
        super.afterInitObject(object);
        
        Set<LoanBussinessInformation> loanBussinessInformations =new HashSet<LoanBussinessInformation>();
        LoanBussinessInformation bussinessInformation=new LoanBussinessInformation();
        
                    bussinessInformation.setNatureOfBussiness(txtNatureOFBusiness.getCValue());
                    bussinessInformation.setName(txtBusinessName.getCValue());
                    bussinessInformation.setAddress(txtBusinessAddress.getCValue());
                    bussinessInformation.setHandleBy(txtBusinessHandleBy.getCValue());
                    bussinessInformation.setBusinessPlace(txtBusinessPlace.getCValue());
                    bussinessInformation.setDuration(txtBusinessDuration.getCValue());
                    bussinessInformation.setStatus((String) radioButtonGroup.getCValue());
                    loanBussinessInformations.add(bussinessInformation);
       
                    Set<LoanBorrowerDetails> borrowerDetailses =new HashSet<LoanBorrowerDetails>();
                    LoanBorrowerDetails borrowerDetails =new LoanBorrowerDetails();
                    
                    borrowerDetails.setRelationType((String) cboRelationShip.getCValue());
                    borrowerDetails.setFullName((String) txtBFullName.getCValue());
                    borrowerDetails.setNic((String) txtBnic.getCValue());
                    borrowerDetails.setMobileNo((String) txtBMobile.getCValue());
                    borrowerDetails.setEducationQl((String) txtBEduQulification.getCValue());
                    borrowerDetails.setGender((String) radioButtonGroup2.getCValue());
                    borrowerDetails.setDob(dtDOB.getCValue());
                    borrowerDetails.setReligion((String) cboReligion.getCValue());
                    borrowerDetails.setOccupation((String) txtOccupation.getCValue());
                    borrowerDetails.setEmail((String) txtEmail.getCValue());
                    borrowerDetailses.add(borrowerDetails);
                    
                    
                    Set<LoanIncomeExpencess> incomeExpencesses =new HashSet<LoanIncomeExpencess>();
                    LoanIncomeExpencess expencess =new LoanIncomeExpencess();
                    
                    expencess.setMainIncome(txtBincome.getCValue());
                    expencess.setOtherIncome(txtBOtherIncome.getCValue());
                    expencess.setRowMaterial(txtRowMaterial.getCValue());
                    expencess.setTotalIncome(txtTotalIncome.getCValue());
                    
                    expencess.setSalary(txtSalary.getCValue());
                    expencess.setElectricity(txtElectricity.getCValue());
                    expencess.setOtherExpencess(txtOtherExpenses.getCValue());
                    expencess.setTotalExpencess(txtTotalExpenses.getCValue());
                    expencess.setBusinessPnl(txtPandL.getCValue());
                    
                   
                    expencess.setFamilyIncome(txtfamilyIncome.getCValue());
                    expencess.setFamilySpouseIncome(txtSpouseIncome.getCValue());
                    expencess.setFamilyOtherIncome(txtOtherIncome.getCValue());
                    expencess.setFamilyTotalIncome(txtFtotalIncome.getCValue());
                    
                    
                    expencess.setFamilyExFood(txtFoods.getCValue());
                    expencess.setFamilyExElectricity(txtFElecricity.getCValue());
                    expencess.setWater(txtWater.getCValue());
                    expencess.setChildEducation(txtChildEdu.getCValue());
                    expencess.setTelephone(txtTpBill.getCValue());
                    expencess.setMonthlyFPayment(txtFacilityPayment.getCValue());
                    expencess.setFamilyOtherEx(txtFOtherEx.getCValue());
                    expencess.setFamilyTotalEx(txtFtotalExpenses.getCValue());
                    
                    expencess.setNetAmount(tctFNetAmount.getCValue());
                    incomeExpencesses.add(expencess);
                    
                    
       
       
        object.setLoanBussinessInformations(loanBussinessInformations);
        object.setLoanBorrowerDetails(borrowerDetailses);
        object.setLoanExistingFacilitys((List<LoanExistingFacility>) tblExisting.getCValue());
        object.setLoanIncomeExpencesses(incomeExpencesses);
        
        
        
        
    }
    
   
    

    @Override
    public Loan getValue() throws ObjectCreatorException {
        return super.getValue(); //To change body of generated methods, choose Tools | Templates.
    }
    

   
}