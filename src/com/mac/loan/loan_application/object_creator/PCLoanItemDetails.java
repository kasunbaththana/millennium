/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_application.object_creator;

import com.mac.loan.loan_application.SERLoanApplication;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanBorrowerDetails;
import com.mac.loan.zobject.LoanBussinessInformation;
import com.mac.loan.zobject.LoanExistingFacility;
import com.mac.loan.zobject.LoanIncomeExpencess;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author soft-master
 */
public class PCLoanItemDetails extends DefaultObjectCreator<Loan> {

    /**
     * Creates new form PCLoan
     */
    public PCLoanItemDetails() {
        initComponents();
        initOthers();
        
    }

    @Action
    public void doRefreshPaymentSchedule() {
        try {
      
           
        } catch (Exception ex) {
            Logger.getLogger(com.mac.loan.hp_loan_application.object_creator.PCLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doRefreshDefaultCharges() {
        
    }

   

    @Override
    public void setEditMood() {
       
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {

    
        
    }

   
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cIStringField1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        cRadioButtonGroup1 = new com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup();

        cIStringField1.setText("cIStringField1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 615, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 451, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.input.textfield.CIStringField cIStringField1;
    private com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup cRadioButtonGroup1;
    // End of variables declaration//GEN-END:variables
    public CRadioButtonGroup radioButtonGroup;
    public CRadioButtonGroup radioButtonGroup2;

    
    
    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList();
          
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(
                
                
                );

    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
              );
    }

    @Override
    protected Class getObjectClass() {
        return Loan.class;
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList();
            
    }

    @Override
    protected void afterInitObject(Loan object) throws ObjectCreatorException {
        super.afterInitObject(object);
        
        
        
    }
    
   
    

    @Override
    public Loan getValue() throws ObjectCreatorException {
        return super.getValue(); //To change body of generated methods, choose Tools | Templates.
    }
    

   
}