/*
 *  PCLoanInit.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 10:12:15 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.loan_application.object_creator;

import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.Arrays;
import java.util.List;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.zobject.Loan;
import com.mac.registration.branch.object.Branch;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author user
 */
public class PCLoanInit extends DefaultObjectCreator {

    /**
     * Creates new form PCLoanInit
     */
    public PCLoanInit() {
        initComponents();
        initOthers();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.LOAN));
        radioButtonGroup = new CRadioButtonGroup();
        radioButtonGroup1 = new CRadioButtonGroup();
        rdEl.setValue("EL");
        rdMl.setValue("ML");
        rdPl.setValue("PL");
        radioButtonGroup.addRadioButton(rdEl);
        radioButtonGroup.addRadioButton(rdMl);
        radioButtonGroup.addRadioButton(rdPl);
        
        pLPanel.setVisible(false);
        
        rdJoin.setValue("JOIN");
        rdIndividual.setValue("INDIVIDUAL");
        radioButtonGroup1.addRadioButton(rdJoin);
        radioButtonGroup1.addRadioButton(rdIndividual);
        
        ActionListener al =new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                setPersonalLoan();
            }
        };
        rdEl.addActionListener(al);
        rdMl.addActionListener(al);
        rdPl.addActionListener(al);
      
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pLPanel = new javax.swing.JPanel();
        rdIndividual = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdJoin = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        rdEl = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdMl = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdPl = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();

        pLPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        rdIndividual.setForeground(new java.awt.Color(0, 0, 204));
        rdIndividual.setText("Individual");

        rdJoin.setForeground(new java.awt.Color(0, 153, 51));
        rdJoin.setText("Join");

        javax.swing.GroupLayout pLPanelLayout = new javax.swing.GroupLayout(pLPanel);
        pLPanel.setLayout(pLPanelLayout);
        pLPanelLayout.setHorizontalGroup(
            pLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pLPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rdIndividual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rdJoin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pLPanelLayout.setVerticalGroup(
            pLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(rdIndividual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(rdJoin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        txtTransactionDate.setText("");

        cLabel2.setText("Transaction Date :");

        cLabel3.setText("Reference No :");

        cLabel1.setText("Request No :");

        txtDocumentNo.setText(" ");

        rdEl.setText("EL Product");

        rdMl.setText("ML Product");

        rdPl.setText("PL Product");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(rdEl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rdMl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rdPl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pLPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 4, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rdEl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(rdMl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(rdPl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(pLPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private javax.swing.JPanel pLPanel;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdEl;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdIndividual;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdJoin;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdMl;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdPl;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables
    private CRadioButtonGroup radioButtonGroup;
    private CRadioButtonGroup radioButtonGroup1;

    public void setPersonalLoan(){
        
        if(rdPl.isSelected()){
        
            pLPanel.setVisible(true);
            
        }else{
            pLPanel.setVisible(false);
        }
            
        
    }
    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(txtDocumentNo,rdEl,rdMl,rdPl);
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
                txtTransactionDate,
                txtReferenceNo,
                txtDocumentNo);
    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtTransactionDate, "applicationTransactionDate"),
                new CInputComponentBinder(txtReferenceNo, "applicationReferenceNo"),
                new CInputComponentBinder(txtDocumentNo, "applicationDocumentNo"));
    }

    @Override
    protected Class getObjectClass() {
        return Loan.class;
    }

    @Override
    protected void afterInitObject(Object object) throws ObjectCreatorException {
        
        Loan loan = (Loan) object;
        
        String branch_code = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
        
         Branch b = (Branch) CPanel.GLOBAL.getDatabaseService().initCriteria(Branch.class)
                        .add(Restrictions.eq("code", branch_code))
                        .uniqueResult();
         
        Integer l = ((Integer) CPanel.GLOBAL.getDatabaseService().initCriteria(Loan.class)
                            .setProjection(Projections.count("indexNo"))
                        .add(Restrictions.eq("branch", branch_code))
                        .add(Restrictions.ne("status", "CANCEL"))
                        .uniqueResult()).intValue()+1;
         
        String val = String.format("%03d", l);    
        String request_no = b.getPrefix()+"/"+val;
        
       loan.setApplicationDocumentNo(request_no);
        
        if(radioButtonGroup.getCValue()!=null){
            
        loan.setElProduct(radioButtonGroup.getCValue().equals("EL"));
        loan.setMlProduct(radioButtonGroup.getCValue().equals("ML"));
        loan.setPlProduct(radioButtonGroup.getCValue().equals("PL"));
        
        
         
        }
        if(radioButtonGroup1.getCValue()!=null){
           loan.setIndividual(radioButtonGroup1.getCValue().equals("INDIVIDUAL"));
         loan.setJoin(radioButtonGroup1.getCValue().equals("JOIN"));
        }
  
        
        
        
    }

  
    
}
