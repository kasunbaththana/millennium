/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_application.object_creator;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.loan_application.SERLoanApplication;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanPersonalBusinessInfor;
import com.mac.loan.zobject.LoanPersonalFacilityInfor;
import com.mac.loan.zobject.LoanPersonalFinacInfor;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author soft-master
 */
public class PCPersonalLoanInfor extends DefaultObjectCreator<Loan> {

    /**
     * Creates new form PCLoan
     */
    public PCPersonalLoanInfor() {
        initComponents();
        initOthers();
        
    }

    @Action
    public void doRefreshPaymentSchedule() {
        try {
      
           
        } catch (Exception ex) {
            Logger.getLogger(com.mac.loan.hp_loan_application.object_creator.PCLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doRefreshDefaultCharges() {
        
    }

   

    @Override
    public void setEditMood() {
       
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
      
        radioButtonGroup = new CRadioButtonGroup();
        radioButtonGroup2 = new CRadioButtonGroup();
        radioButtonGroup3 = new CRadioButtonGroup();
        applGrp = new CRadioButtonGroup();
        jointGrp = new CRadioButtonGroup();
        cusStatus = new CRadioButtonGroup();
        jointStatus = new CRadioButtonGroup();
        
      
        rdosp.setValue("Salaried Professional");
        rdoSE.setValue("Self Employment");
        rdoS.setValue("Salaried");
        
//        rdosp1.setValue("Salaried Professional");
//        rdoSE1.setValue("Self Employment");
//        rdoS1.setValue("Salaried");
        
    

        radioButtonGroup.addRadioButton(rdosp);
        radioButtonGroup.addRadioButton(rdoSE);
        radioButtonGroup.addRadioButton(rdoS);
        
//        radioButtonGroup3.addRadioButton(rdosp1);
//        radioButtonGroup3.addRadioButton(rdoSE1);
//        radioButtonGroup3.addRadioButton(rdoS1);
        
        
        rdPerOver5.setValue("Permanent Over 5 Years");
        rdProbation.setValue("Probation");
        rdBelow5.setValue("Permanent Below 5 Years");
        rdContract.setValue("Contract");
        
//        rdPerOver51.setValue("Permanent Over 5 Years");
//        rdProbation1.setValue("Probation");
//        rdBelow51.setValue("Permanent Below 5 Years");
//        rdContract1.setValue("Contract");
        
        cusStatus.addRadioButton(rdPerOver5);
        cusStatus.addRadioButton(rdProbation);
        cusStatus.addRadioButton(rdBelow5);
        cusStatus.addRadioButton(rdContract);
        
//        jointStatus.addRadioButton(rdPerOver51);
//        jointStatus.addRadioButton(rdProbation1);
//        jointStatus.addRadioButton(rdBelow51);
//        jointStatus.addRadioButton(rdContract1);
//        
        
    
        pCFacility = new PCFacilityPersonal("CUSTOMER");
         tblExisting.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Institution",new String[]{"institution"}, true),
            new CTableColumn("F Amount", new String[]{"amount"}, true),
            new CTableColumn("M Payment", new String[]{"rental"}, true),
        }));
         
         tblExisting.setObjectCreator(pCFacility);
         tblExisting.setDescrption("Facility");
         
         pCFacility = new PCFacilityPersonal("JOINT");
//         tblExisting1.setCModel(new CTableModel(new CTableColumn[]{
//            new CTableColumn("Institution",new String[]{"institution"}, true),
//            new CTableColumn("F Amount", new String[]{"amount"}, true),
//            new CTableColumn("M Payment", new String[]{"rental"}, true),
//        }));
//         
//         tblExisting1.setObjectCreator(pCFacility);
//         tblExisting1.setDescrption("Facility");
   
         FocusAdapter fa=new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                calculateCashInHand();
            }  
         };
         txtBasicSalary.addFocusListener(fa);
         txtAllowance.addFocusListener(fa);
         txtbIncome.addFocusListener(fa);
         txtOtherIncome.addFocusListener(fa);
         txtHouseHold.addFocusListener(fa);
         txtBusiEx.addFocusListener(fa);
         txtFinancEx.addFocusListener(fa);
         txtOtherEx.addFocusListener(fa);
         
//         txtBasicSalary1.addFocusListener(fa);
//         txtAllowance1.addFocusListener(fa);
//         txtbIncome1.addFocusListener(fa);
//         txtOtherIncome1.addFocusListener(fa);
//         txtHouseHold1.addFocusListener(fa);
//         txtBusiEx1.addFocusListener(fa);
//         txtFinancEx1.addFocusListener(fa);
//         txtOtherEx1.addFocusListener(fa);
         
                
          txtTotInc.setEditable(false);
       //   txtTotInc1.setEditable(false);
          txtTotalEx.setEditable(false);
       //   txtTotalEx1.setEditable(false);
          
          txtmCashInHand.setEditable(false);
//          txtmCashInHand1.setEditable(false);
// 
//          
//          txt_jn_fullName.setEditable(false);
//          txtjn_address1.setEditable(false);
//          txtjn_address2.setEditable(false);
//          txtjn_address3.setEditable(false);
//          txtjn_cn1.setEditable(false);
//          txtjn_cn2.setEditable(false);
//          
          applGrp.addRadioButton(rdcus_brrow);
          applGrp.addRadioButton(rdcus_new);
       //   jointGrp.addRadioButton(rdjn_brrow);
        //  jointGrp.addRadioButton(rdjn_new);
          
//          cbo_JointApplicant.addItemListener(new ItemListener() {
//
//            @Override
//            public void itemStateChanged(ItemEvent e) {
//                Client client =(Client) cbo_JointApplicant.getCValue();
//                if(client!=null){
//                     txt_jn_fullName.setCValue(client.getLongName());
//                     txtjn_address1.setCValue(client.getAddressLine1());
//                     txtjn_address2.setCValue(client.getAddressLine2());
//                     txtjn_address3.setCValue(client.getAddressLine3());
//                     txtjn_cn1.setCValue(client.getMobile());
//                     txtjn_cn2.setCValue(client.getTelephone1());  
//
//                }else{
//                     txt_jn_fullName.setCValue("");
//                     txtjn_address1.setCValue("");
//                     txtjn_address2.setCValue("");
//                     txtjn_address3.setCValue("");
//                     txtjn_cn1.setCValue("");
//                     txtjn_cn2.setCValue(""); 
//                }
//            }
//        });
          tblExisting.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                calculateCashInHand();
            }
        });
//          tblExisting1.addChangeListener(new ChangeListener() {
//
//            @Override
//            public void stateChanged(ChangeEvent e) {
//                calculateCashInHand();
//            }
//        });
      
         
    }
    public void calculateCashInHand(){
        double bc = txtBasicSalary.getCValue();
        double fa=txtAllowance.getCValue();
        double bi=txtbIncome.getCValue();
        double oi=txtOtherIncome.getCValue();
        
        double totInc = bc+fa+bi+oi;
        txtTotInc.setCValue(totInc);
        
        double ho = txtHouseHold.getCValue();
        double bx = txtBusiEx.getCValue();
        double fx = txtFinancEx.getCValue();
        double ox =txtOtherEx.getCValue();
        
        double totEx = ho+bx+fx+ox+calCusFacilityTable();
        txtTotalEx.setCValue(totEx);
        
        txtmCashInHand.setCValue(totInc-totEx);
        
//        double bc1 = txtBasicSalary1.getCValue();
//        double fa1=txtAllowance1.getCValue();
//        double bi1=txtbIncome1.getCValue();
//        double oi1=txtOtherIncome1.getCValue();
//        
//        double totInc1 = bc1+fa1+bi1+oi1;
//        txtTotInc1.setCValue(totInc1);
//        
//        double ho1 = txtHouseHold1.getCValue();
//        double bx1 = txtBusiEx1.getCValue();
//        double fx1 = txtFinancEx1.getCValue();
//        double ox1 =txtOtherEx1.getCValue();
//        
//        double totEx1 = ho1+bx1+fx1+ox1+calJointFacilityTable();
//        txtTotalEx1.setCValue(totEx1);
      
        
    }


   
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cIStringField1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        cRadioButtonGroup1 = new com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        cLabel27 = new com.mac.af.component.derived.display.label.CDLabel();
        rdcus_brrow = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdcus_new = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        jPanel1 = new javax.swing.JPanel();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        lblInfo = new com.mac.af.component.derived.display.label.CDLabel();
        txtProfession = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNameOfOrg = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtBusinessAddress = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtposition = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtcontactNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        jSeparator1 = new javax.swing.JSeparator();
        rdoSE = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdosp = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdoS = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        cLabel37 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        rdPerOver5 = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdProbation = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdBelow5 = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdContract = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        jPanel4 = new javax.swing.JPanel();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel17 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBasicSalary = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtAllowance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtbIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtOtherIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotInc = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel19 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel20 = new com.mac.af.component.derived.display.label.CDLabel();
        txtHouseHold = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel21 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel22 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel23 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel24 = new com.mac.af.component.derived.display.label.CDLabel();
        txtIncSource = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtBusiEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtFinancEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtOtherEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtOtherExSource = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel39 = new com.mac.af.component.derived.display.label.CDLabel();
        jPanel2 = new javax.swing.JPanel();
        tblExisting = new com.mac.af.component.derived.input.table.CIAddingTable();
        cLabel53 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel55 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel57 = new com.mac.af.component.derived.display.label.CDLabel();
        txtmCashInHand = new com.mac.af.component.derived.input.textfield.CIDoubleField();

        cIStringField1.setText("cIStringField1");

        cLabel27.setText("Applicant");
        cLabel27.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        rdcus_brrow.setText("Existing customer with borrowing");
        rdcus_brrow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdcus_brrowActionPerformed(evt);
            }
        });

        rdcus_new.setText("New customer");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(rdcus_brrow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rdcus_new, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(458, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdcus_brrow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdcus_new, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(364, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Relationship", jPanel3);

        cLabel1.setText("Profession/Business:");

        cLabel2.setText("Name Of Org:");

        cLabel3.setText("Address Of Employer/Business:");

        cLabel5.setText("Present Position");

        cLabel6.setText("Contact No");

        cLabel8.setText("Employment Type");

        cLabel4.setText("Employment/Business Details");

        rdoSE.setText("Self Employment");

        rdosp.setText("Salaried Professional");

        rdoS.setText("Salaried");

        cLabel37.setText("Applicant");
        cLabel37.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cLabel9.setText("Employment Status");

        rdPerOver5.setText("Permanent Over 5 Years");

        rdProbation.setText("Probation");

        rdBelow5.setText("Permanent Below 5 Years");

        rdContract.setText("Contract");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdosp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoSE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtposition, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(7, 7, 7)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(txtNameOfOrg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                                        .addComponent(txtProfession, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtBusinessAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(rdPerOver5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(14, 14, 14)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(rdProbation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtcontactNo, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(rdContract, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(rdBelow5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 244, Short.MAX_VALUE)
                        .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                        .addGap(65, 65, 65))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(cLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rdosp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(rdoSE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(rdoS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProfession, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNameOfOrg, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusinessAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtposition, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcontactNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdPerOver5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdProbation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rdContract, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(lblInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdBelow5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(129, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Business", jPanel1);

        cLabel11.setForeground(new java.awt.Color(0, 153, 0));
        cLabel11.setText("Income");
        cLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cLabel12.setText("Basic Salary");

        cLabel13.setText("Fixed Allowances");

        cLabel14.setText("Business Income");

        cLabel15.setText("Other Income");

        cLabel16.setText("Other Income Source");

        cLabel17.setText("Total Income");

        txtBasicSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBasicSalaryActionPerformed(evt);
            }
        });

        cLabel19.setForeground(new java.awt.Color(255, 0, 0));
        cLabel19.setText("Expenses");
        cLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cLabel20.setText("House Holding");

        cLabel21.setText("Business Expenses");

        cLabel22.setText("Financial Expenses");

        cLabel23.setText("Other Expenses");

        cLabel24.setText("Other Expenses Source");

        txtIncSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIncSourceActionPerformed(evt);
            }
        });

        txtOtherExSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOtherExSourceActionPerformed(evt);
            }
        });

        cLabel39.setText("Applicant");
        cLabel39.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(cLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(358, 458, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTotInc, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtOtherIncome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtIncSource, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(169, 169, 169))
                            .addComponent(txtbIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtBasicSalary, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtAllowance, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(183, 183, 183))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cLabel21, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel24, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel20, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel22, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtHouseHold, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtBusiEx, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtFinancEx, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtOtherEx, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtOtherExSource, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(121, 121, 121))))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBasicSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAllowance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtbIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtOtherIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIncSource, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtHouseHold, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBusiEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFinancEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtOtherEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtOtherExSource, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotInc, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(219, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Finance", jPanel4);

        cLabel53.setText("Applicant");
        cLabel53.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cLabel55.setText("Total Expenses");

        cLabel57.setText("Monthly Cash In Hand");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cLabel53, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtTotalEx, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cLabel57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtmCashInHand, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tblExisting, javax.swing.GroupLayout.PREFERRED_SIZE, 754, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtmCashInHand, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addComponent(tblExisting, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(239, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Facility", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtBasicSalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBasicSalaryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBasicSalaryActionPerformed

    private void txtIncSourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIncSourceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIncSourceActionPerformed

    private void txtOtherExSourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOtherExSourceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOtherExSourceActionPerformed

    private void rdcus_brrowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdcus_brrowActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdcus_brrowActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.input.textfield.CIStringField cIStringField1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cLabel17;
    private com.mac.af.component.derived.display.label.CDLabel cLabel19;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel20;
    private com.mac.af.component.derived.display.label.CDLabel cLabel21;
    private com.mac.af.component.derived.display.label.CDLabel cLabel22;
    private com.mac.af.component.derived.display.label.CDLabel cLabel23;
    private com.mac.af.component.derived.display.label.CDLabel cLabel24;
    private com.mac.af.component.derived.display.label.CDLabel cLabel27;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel37;
    private com.mac.af.component.derived.display.label.CDLabel cLabel39;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel53;
    private com.mac.af.component.derived.display.label.CDLabel cLabel55;
    private com.mac.af.component.derived.display.label.CDLabel cLabel57;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cLabel9;
    private com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup cRadioButtonGroup1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblInfo;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdBelow5;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdContract;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdPerOver5;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdProbation;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdcus_brrow;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdcus_new;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoS;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoSE;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdosp;
    private com.mac.af.component.derived.input.table.CIAddingTable tblExisting;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAllowance;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBasicSalary;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBusiEx;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBusinessAddress;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtFinancEx;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtHouseHold;
    private com.mac.af.component.derived.input.textfield.CIStringField txtIncSource;
    public com.mac.af.component.derived.input.textfield.CIStringField txtNameOfOrg;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtOtherEx;
    private com.mac.af.component.derived.input.textfield.CIStringField txtOtherExSource;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtOtherIncome;
    private com.mac.af.component.derived.input.textfield.CIStringField txtProfession;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotInc;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalEx;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtbIncome;
    public com.mac.af.component.derived.input.textfield.CIStringField txtcontactNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtmCashInHand;
    public com.mac.af.component.derived.input.textfield.CIStringField txtposition;
    // End of variables declaration//GEN-END:variables
    public CRadioButtonGroup radioButtonGroup;
    public CRadioButtonGroup radioButtonGroup2;
    public CRadioButtonGroup radioButtonGroup3;
    public PCFacilityPersonal  pCFacility;

    public CRadioButtonGroup applGrp;
    public CRadioButtonGroup jointGrp;
    
    
    public CRadioButtonGroup cusStatus;
    public CRadioButtonGroup jointStatus;
    
    
       public List getJointCustomer() {
        List list;
        try {
            list = getCPanel().getDatabaseService().getCollection("from com.mac.loan.zobject.Client where active=true  AND client=true");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
  
  

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList();
          
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(
         
            
                );

    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
              );
    }

    @Override
    protected Class getObjectClass() {
        return Loan.class;
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
            txtTotInc,txtTotalEx
                
                );
    }

    @Override
    protected void afterInitObject(Loan object) throws ObjectCreatorException {
        super.afterInitObject(object);
       
        Set<LoanPersonalBusinessInfor> loanPersonalBusinessInfors=new HashSet<LoanPersonalBusinessInfor>();
        LoanPersonalBusinessInfor businessInfors=new LoanPersonalBusinessInfor();
        
        businessInfors.setCusIsExist(rdcus_brrow.isSelected());
        businessInfors.setCusIsNew(rdcus_new.isSelected());
//        businessInfors.setJnIsExist(rdjn_brrow.isSelected());
//        businessInfors.setJnIsNew(rdjn_new.isSelected());
//        businessInfors.setJointApplicant(client.getCode());
        businessInfors.setCusEmType((String) radioButtonGroup.getCValue());
        businessInfors.setJnEmType((String) radioButtonGroup3.getCValue());
        businessInfors.setCusProfission(txtProfession.getCValue());
       // businessInfors.setJnProfission(txtProfession1.getCValue());
        businessInfors.setCusOrgName(txtNameOfOrg.getCValue());
      //  businessInfors.setJnOrgName(txtNameOfOrg1.getCValue());
        businessInfors.setCusOrgAddress(txtBusinessAddress.getCValue());
    //    businessInfors.setJnOrgAddress(txtBusinessAddress1.getCValue());
        businessInfors.setCusPosition(txtProfession.getCValue());
      //  businessInfors.setJnPosition(txtProfession1.getCValue());
        businessInfors.setCusContact(txtcontactNo.getCValue());
     //   businessInfors.setJnContact(txtcontactNo1.getCValue());
        businessInfors.setCusStatus((String) cusStatus.getCValue());
        businessInfors.setJnStatus((String) jointStatus.getCValue());
        loanPersonalBusinessInfors.add(businessInfors);
        
        Set<LoanPersonalFinacInfor> finacInfors=new HashSet<LoanPersonalFinacInfor>();
        LoanPersonalFinacInfor finacInfor=new LoanPersonalFinacInfor();
        
        finacInfor.setCusBasic(txtBasicSalary.getCValue());
        finacInfor.setCusAllowance(txtAllowance.getCValue());
        finacInfor.setCusBusiIncome(txtbIncome.getCValue());
        finacInfor.setCusOtherIncome(txtOtherIncome.getCValue());
        finacInfor.setCusOtherSource(txtIncSource.getCValue());
        finacInfor.setCusTotIncome(txtTotInc.getCValue());
        
        finacInfor.setCusHoHoEx(txtHouseHold.getCValue());
        finacInfor.setCusBusiEx(txtBusiEx.getCValue());
        finacInfor.setCusFinEx(txtFinancEx.getCValue());
        finacInfor.setCusOtherEx(txtOtherEx.getCValue());
        finacInfor.setCusSourceEx(txtOtherExSource.getCValue());;
        finacInfor.setCusTotEx(txtTotalEx.getCValue());
        finacInfor.setCusCashInHand(txtmCashInHand.getCValue());
        
//        finacInfor.setJnBasic(txtBasicSalary1.getCValue());
//        finacInfor.setJnAllowance(txtAllowance1.getCValue());
//        finacInfor.setJnBusiIncome(txtbIncome1.getCValue());
//        finacInfor.setJuOtherIncome(txtOtherIncome1.getCValue());
//        finacInfor.setJnOtherSource(txtIncSource1.getCValue());
//        finacInfor.setJnTotIncome(txtTotInc1.getCValue());
//        
//        finacInfor.setJnHoHoEx(txtHouseHold1.getCValue());
//        finacInfor.setJnBusiEx(txtBusiEx1.getCValue());
//        finacInfor.setJnFinEx(txtFinancEx1.getCValue());
//        finacInfor.setJnOtherEx(txtOtherEx1.getCValue());
//        finacInfor.setJnSourceEx(txtOtherExSource1.getCValue());;
        finacInfors.add(finacInfor);
        
        
        
        object.setLoanPersonalBusinessInfors(loanPersonalBusinessInfors);
        object.setLoanPersonalFinacInfors(finacInfors);
        
        List<LoanPersonalFacilityInfor> facilityInfors= (List<LoanPersonalFacilityInfor>) tblExisting.getCValue();
       // List<LoanPersonalFacilityInfor> facilityInfors1= (List<LoanPersonalFacilityInfor>) tblExisting1.getCValue();

        
       // facilityInfors.addAll(facilityInfors1);
       
        
        object.setLoanPersonalFacilityInforn(facilityInfors);
      
    }
    public double calCusFacilityTable(){
        
         Collection<LoanPersonalFacilityInfor> items = tblExisting.getCValue();
        double itemTotal = 0.0;
      
        for (LoanPersonalFacilityInfor item : items) {
            itemTotal += item.getRental();
        }
         
       return (itemTotal);
        
    }

   
    

    @Override
    public Loan getValue() throws ObjectCreatorException {
        return super.getValue(); //To change body of generated methods, choose Tools | Templates.
    }
    

   
}