/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_application.object_creator;

import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.loan.loan_application.PCLoanApplicationContent;
import com.mac.loan.zobject.Item;
import com.mac.loan.zobject.LoanItemDetails;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kasun
 */
public class PCSalesItem extends DefaultObjectCreator {

    /**
     * Creates new form PCSalesItem
     */
    PCLoanApplicationContent applicationContent;
    public PCSalesItem(PCLoanApplicationContent applicationContent) {
       this.applicationContent = applicationContent;
        
        initComponents();

        initOthers();
        
        
       
    }

  public List<Item> getItems() {
        try {
            return getCPanel().getDatabaseService().getCollection(Item.class);
        } catch (DatabaseException ex) {
            Logger.getLogger(PCSalesItem.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }
 
  

 

    @SuppressWarnings("unchecked")
    private void initOthers() {
       radioButtonGroup = new CRadioButtonGroup();
       
       radioButtonGroup.addRadioButton(rdConsummerGoods);
       radioButtonGroup.addRadioButton(rdElectronicGoods);
       radioButtonGroup.addRadioButton(rdFuniture);
       radioButtonGroup.addRadioButton(rdOtherGoods);
       
       
       txtDealerPayment.addFocusListener(new FocusListener() {

           @Override
           public void focusGained(FocusEvent e) {
              
           }

           @Override
           public void focusLost(FocusEvent e) {
               calculateFacility();
           }
       });
       
       txtProductValue.addFocusListener(new FocusListener() {

           @Override
           public void focusGained(FocusEvent e) {
              
           }

           @Override
           public void focusLost(FocusEvent e) {
               calculateFacility();
           }
       });
       txtOtherExAmount.addFocusListener(new FocusListener() {

           @Override
           public void focusGained(FocusEvent e) {
              
           }

           @Override
           public void focusLost(FocusEvent e) {
               calculateFacility();
           }
       });
       
       
       
    }
    CRadioButtonGroup radioButtonGroup;
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        rdConsummerGoods = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdElectronicGoods = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdOtherGoods = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdFuniture = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        txtProductName = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtProductDetails = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtDealerPayment = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtProductValue = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtEconomicProfit = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBasicPayment = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtOtherExpensesDes = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtOtherExAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        txtFacilityAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();

        cDLabel1.setText("Product Name:");

        cDLabel2.setText("Product Details:");

        cDLabel3.setText("Dealer Payment:");

        cDLabel4.setText("Economic Profit:");

        cDLabel6.setText("Product:");

        rdConsummerGoods.setText("Consumer Goods");

        rdElectronicGoods.setText("Electronic Goods");
        rdElectronicGoods.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdElectronicGoodsActionPerformed(evt);
            }
        });

        rdOtherGoods.setText("Other");
        rdOtherGoods.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdOtherGoodsActionPerformed(evt);
            }
        });

        rdFuniture.setText("Furniture");
        rdFuniture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdFunitureActionPerformed(evt);
            }
        });

        cDLabel5.setText("Product Value:");

        cDLabel7.setText("Basic Payment:");

        cDLabel8.setText("Other Expenses:");

        cDLabel9.setText("Total Other Ex Amount:");

        cDLabel10.setText("FacilityAmount:");
        cDLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        txtFacilityAmount.setForeground(new java.awt.Color(0, 102, 0));
        txtFacilityAmount.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtOtherExpensesDes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtProductName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                            .addComponent(txtProductDetails, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtDealerPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtEconomicProfit, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtProductValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtBasicPayment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtOtherExAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                                    .addComponent(txtFacilityAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdConsummerGoods, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdElectronicGoods, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdFuniture, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdOtherGoods, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdConsummerGoods, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdElectronicGoods, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdFuniture, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdOtherGoods, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProductName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProductDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDealerPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProductValue, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEconomicProfit, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBasicPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtOtherExpensesDes, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOtherExAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFacilityAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(46, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void rdElectronicGoodsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdElectronicGoodsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdElectronicGoodsActionPerformed

    private void rdOtherGoodsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdOtherGoodsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdOtherGoodsActionPerformed

    private void rdFunitureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdFunitureActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdFunitureActionPerformed

    public void calculateFacility(){
        txtBasicPayment.setCValue(applicationContent.getBasicAmount());
  
        double dl_pay = txtDealerPayment.getCValue();
        double pr_val = txtProductValue.getCValue();
        
        double basic_val = txtBasicPayment.getCValue();
        double other_val = txtOtherExAmount.getCValue();
        
        txtEconomicProfit.setCValue(pr_val-dl_pay);
        txtFacilityAmount.setCValue((pr_val+other_val)-basic_val);
        
        
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdConsummerGoods;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdElectronicGoods;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdFuniture;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton rdOtherGoods;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtBasicPayment;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtDealerPayment;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtEconomicProfit;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtFacilityAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtOtherExAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtOtherExpensesDes;
    private com.mac.af.component.derived.input.textfield.CIStringField txtProductDetails;
    private com.mac.af.component.derived.input.textfield.CIStringField txtProductName;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtProductValue;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList(
                txtProductName,
                txtProductDetails,
                txtDealerPayment,
                txtProductValue,
                txtEconomicProfit,
                txtBasicPayment,

                txtFacilityAmount
                );
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(
                txtOtherExpensesDes,
                txtOtherExAmount
                );
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(txtFacilityAmount,txtEconomicProfit,txtBasicPayment); //To change body of generated methods, choose Tools | Templates.
    }
    

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtProductName, "productName"),
                new CInputComponentBinder(txtProductDetails, "productDetails"),
                new CInputComponentBinder(txtDealerPayment, "dealerPayment"),
                new CInputComponentBinder(txtProductValue, "productValue"),
                new CInputComponentBinder(txtEconomicProfit, "economicProfit"),
                new CInputComponentBinder(txtBasicPayment, "basicPayment"),
                new CInputComponentBinder(txtOtherExpensesDes, "otherDes"),
                new CInputComponentBinder(txtOtherExAmount, "totOtherPay"),
                new CInputComponentBinder(txtFacilityAmount, "facilityAmount"));
    }

    @Override
    protected Class getObjectClass() {
        return LoanItemDetails.class;
    }
    
    
    @Override
    protected void afterNewObject(Object object) {
        LoanItemDetails details = (LoanItemDetails) object;
        details.setStatus("ACTIVE");
    }
}
