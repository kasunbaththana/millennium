/*
 *  PCClient.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.loan.loan_application.object_creator;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.panel.object.CInputComponentBinder;
import java.awt.Component;
import java.util.Arrays;
import java.util.List;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.loan.zobject.LoanExistingFacility;
import com.mac.loan.zobject.LoanMInstitution;
import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author mohan
 */
public class PCFinancialInstitution extends DefaultObjectCreator {

    private JFileChooser jFileChooser;
    private File file;

    /**
     * Creates new form PCClient
     */
    public PCFinancialInstitution() {
        initComponents();
        
    }

  
    
    
    
    


    @Action
    public void doBrowse() {
      
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtinstitution = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtMonthlyPay = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtBalanceOutstan = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtFamount = new com.mac.af.component.derived.input.textfield.CIDoubleField();

        cLabel2.setText("Institution");

        cLabel3.setText("Financial Amount");

        cLabel4.setText("Monthly Payment");

        cLabel5.setText("Balance Outstanding");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtinstitution, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtMonthlyPay, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
                    .addComponent(txtBalanceOutstan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
                    .addComponent(txtFamount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtinstitution, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFamount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMonthlyPay, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBalanceOutstan, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtBalanceOutstan;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtFamount;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtMonthlyPay;
    private com.mac.af.component.derived.input.textfield.CIStringField txtinstitution;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList();
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                (Component) txtinstitution,
                (Component) txtMonthlyPay,
                (Component) txtFamount,
                (Component) txtBalanceOutstan);
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtinstitution, "des"),
                new CInputComponentBinder(txtFamount, "FAmount"),
                new CInputComponentBinder(txtMonthlyPay, "MAmount"),
                new CInputComponentBinder(txtBalanceOutstan, "BOutstanding"));
    }

    @Override
    protected Class getObjectClass() {
        return LoanMInstitution.class;
    }

    @Override
    protected void afterNewObject(Object object) {
    }
}
