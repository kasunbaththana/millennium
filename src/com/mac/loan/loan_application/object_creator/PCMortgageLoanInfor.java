/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_application.object_creator;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanMInstitution;
import com.mac.loan.zobject.LoanMortgageDetails;
import com.mac.loan.zobject.LoanMortgageProperty;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author soft-master
 */
public class PCMortgageLoanInfor extends DefaultObjectCreator<Loan> {

    /**
     * Creates new form PCLoan
     */
    public PCMortgageLoanInfor() {
        initComponents();
        initOthers();
        
    }

    @Action
    public void doRefreshPaymentSchedule() {
        try {
      
           
        } catch (Exception ex) {
            Logger.getLogger(com.mac.loan.hp_loan_application.object_creator.PCLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doRefreshDefaultCharges() {
        
    }

   

    @Override
    public void setEditMood() {
       
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
      
        radioButtonGroup = new CRadioButtonGroup();
        radioButtonGroup2 = new CRadioButtonGroup();
        
      
        rdosp.setValue("Salaried Professional");
        rdoSE.setValue("Self Employment");
        rdoS.setValue("Salaried");
        
    

        radioButtonGroup.addRadioButton(rdosp);
        radioButtonGroup.addRadioButton(rdoSE);
        radioButtonGroup.addRadioButton(rdoS);
        
        
    
        pCFacility = new PCFinancialInstitution();
         tblExisting.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Institution",new String[]{"des"}, true),
            new CTableColumn("F Amount", new String[]{"FAmount"}, true),
            new CTableColumn("M Payment", new String[]{"MAmount"}, true),
            new CTableColumn("B Outstanding", new String[]{"BOutstanding"}, true)
        }));
         
         tblExisting.setObjectCreator(pCFacility);
         tblExisting.setDescrption("Facility");
   
         FocusAdapter fa=new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                calculateCashInHand();
            }  
         };
         txtBasicSalary.addFocusListener(fa);
         txtAllowance.addFocusListener(fa);
         txtbIncome.addFocusListener(fa);
         txtOtherIncome.addFocusListener(fa);
         txtHouseHold.addFocusListener(fa);
         txtBusiEx.addFocusListener(fa);
         txtFinancEx.addFocusListener(fa);
         txtOtherEx.addFocusListener(fa);
         
                
          txtTotInc.setEditable(false);
          txtTotalEx.setEditable(false);
          txtCashInHand.setEditable(false);     
      
         
    }
    public void calculateCashInHand(){
        double bc = txtBasicSalary.getCValue();
        double fa=txtAllowance.getCValue();
        double bi=txtbIncome.getCValue();
        double oi=txtOtherIncome.getCValue();
        
        double totInc = bc+fa+bi+oi;
        txtTotInc.setCValue(totInc);
        
        double ho = txtHouseHold.getCValue();
        double bx = txtBusiEx.getCValue();
        double fx = txtFinancEx.getCValue();
        double ox =txtOtherEx.getCValue();
        
        double totEx = ho+bx+fx+ox;
        txtTotalEx.setCValue(totEx);
        
        txtCashInHand.setCValue(totInc-totEx);
        
    }

   
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cIStringField1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        cRadioButtonGroup1 = new com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        lblInfo = new com.mac.af.component.derived.display.label.CDLabel();
        txtProfession = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNameOfOrg = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtBusinessAddress = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtposition = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtcontactNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        jSeparator1 = new javax.swing.JSeparator();
        rdoSE = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdosp = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdoS = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        txtYear = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        txtMonth = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        jPanel4 = new javax.swing.JPanel();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel17 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel18 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBasicSalary = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtAllowance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtbIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtOtherIncome = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotInc = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtCashInHand = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel19 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel20 = new com.mac.af.component.derived.display.label.CDLabel();
        txtHouseHold = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel21 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel22 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel23 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel24 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel25 = new com.mac.af.component.derived.display.label.CDLabel();
        txtIncSource = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtBusiEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtFinancEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtOtherEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtOtherExSource = new com.mac.af.component.derived.input.textfield.CIStringField();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        tblExisting = new com.mac.af.component.derived.input.table.CIAddingTable();
        jPanel3 = new javax.swing.JPanel();
        cLabel26 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel28 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel29 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel30 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel31 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel32 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel33 = new com.mac.af.component.derived.display.label.CDLabel();
        txtproprtydet = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtlandEx = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtPurPrice = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtConCost = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtFlArea = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtFsaleVal = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtMarkVal = new com.mac.af.component.derived.input.textfield.CIDoubleField();

        cIStringField1.setText("cIStringField1");

        cLabel1.setText("Profession/Business:");

        cLabel2.setText("Name Of Org:");

        cLabel3.setText("Address Of Employer/Business:");

        cLabel5.setText("Present Position");

        cLabel6.setText("Contact No");

        cLabel7.setText("Length Of Service");

        cLabel8.setText("Employment Type");

        cLabel4.setText("Employment/Business Details");

        rdoSE.setText("Self Employment");

        rdosp.setText("Salaried Professional");

        rdoS.setText("Salaried");

        cLabel9.setText("Years");

        cLabel10.setText("Months");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(65, 65, 65))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(7, 7, 7)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(txtNameOfOrg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
                                            .addComponent(txtProfession, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtBusinessAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(rdosp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(rdoSE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(rdoS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(txtMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(71, 71, 71)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(txtcontactNo, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
                                                    .addComponent(txtposition, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                                .addGap(0, 79, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rdosp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(rdoSE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(rdoS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProfession, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNameOfOrg, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusinessAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtposition, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcontactNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(lblInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(181, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Business", jPanel1);

        cLabel11.setForeground(new java.awt.Color(0, 153, 0));
        cLabel11.setText("Income");
        cLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cLabel12.setText("Basic Salary");

        cLabel13.setText("Fixed Allowances");

        cLabel14.setText("Business Income");

        cLabel15.setText("Other Income");

        cLabel16.setText("Other Income Source");

        cLabel17.setText("Total Income");

        cLabel18.setForeground(new java.awt.Color(0, 0, 255));
        cLabel18.setText("Monthly Cash In Hand");
        cLabel18.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        txtBasicSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBasicSalaryActionPerformed(evt);
            }
        });

        txtCashInHand.setForeground(new java.awt.Color(0, 0, 204));
        txtCashInHand.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        cLabel19.setForeground(new java.awt.Color(255, 0, 0));
        cLabel19.setText("Expenses");
        cLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cLabel20.setText("House Holding");

        cLabel21.setText("Business Expenses");

        cLabel22.setText("Financial Expenses");

        cLabel23.setText("Other Expenses");

        cLabel24.setText("Other Expenses Source");

        cLabel25.setText("Total Expenses");

        txtIncSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIncSourceActionPerformed(evt);
            }
        });

        txtOtherExSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOtherExSourceActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel17, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel18, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtTotInc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCashInHand, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtOtherIncome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtIncSource, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(208, 208, 208))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(169, 169, 169))
                    .addComponent(txtbIncome, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtBasicSalary, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtAllowance, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel21, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel24, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel25, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel20, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel19, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel22, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtHouseHold, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtBusiEx, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFinancEx, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtOtherEx, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTotalEx, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtOtherExSource, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(213, 213, 213))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtBasicSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtAllowance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtbIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtOtherIncome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtIncSource, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTotInc, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(48, 48, 48)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCashInHand, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtHouseHold, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBusiEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFinancEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtOtherEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtOtherExSource, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTotalEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(131, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Finance", jPanel4);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tblExisting, javax.swing.GroupLayout.PREFERRED_SIZE, 678, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(58, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tblExisting, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(233, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Facility", jPanel2);

        cLabel26.setText("Address:");

        cLabel28.setText("Land Extent");

        cLabel29.setText("Purchase Price");

        cLabel30.setText("Cunstruction Cost");

        cLabel31.setText("Floor Area");

        cLabel32.setText("For Sale Value");

        cLabel33.setText("Market Value");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(cLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtproprtydet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(cLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtConCost, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtlandEx, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPurPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFsaleVal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMarkVal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFlArea, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(206, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtproprtydet, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFlArea, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtlandEx, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFsaleVal, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPurPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMarkVal, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtConCost, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(294, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Property Details", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 751, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtBasicSalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBasicSalaryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBasicSalaryActionPerformed

    private void txtIncSourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIncSourceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIncSourceActionPerformed

    private void txtOtherExSourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOtherExSourceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOtherExSourceActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.input.textfield.CIStringField cIStringField1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cLabel17;
    private com.mac.af.component.derived.display.label.CDLabel cLabel18;
    private com.mac.af.component.derived.display.label.CDLabel cLabel19;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel20;
    private com.mac.af.component.derived.display.label.CDLabel cLabel21;
    private com.mac.af.component.derived.display.label.CDLabel cLabel22;
    private com.mac.af.component.derived.display.label.CDLabel cLabel23;
    private com.mac.af.component.derived.display.label.CDLabel cLabel24;
    private com.mac.af.component.derived.display.label.CDLabel cLabel25;
    private com.mac.af.component.derived.display.label.CDLabel cLabel26;
    private com.mac.af.component.derived.display.label.CDLabel cLabel28;
    private com.mac.af.component.derived.display.label.CDLabel cLabel29;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel30;
    private com.mac.af.component.derived.display.label.CDLabel cLabel31;
    private com.mac.af.component.derived.display.label.CDLabel cLabel32;
    private com.mac.af.component.derived.display.label.CDLabel cLabel33;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cLabel9;
    private com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup cRadioButtonGroup1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblInfo;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoS;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoSE;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdosp;
    private com.mac.af.component.derived.input.table.CIAddingTable tblExisting;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAllowance;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBasicSalary;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBusiEx;
    public com.mac.af.component.derived.input.textfield.CIStringField txtBusinessAddress;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtCashInHand;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtConCost;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtFinancEx;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtFlArea;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtFsaleVal;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtHouseHold;
    private com.mac.af.component.derived.input.textfield.CIStringField txtIncSource;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtMarkVal;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtMonth;
    public com.mac.af.component.derived.input.textfield.CIStringField txtNameOfOrg;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtOtherEx;
    private com.mac.af.component.derived.input.textfield.CIStringField txtOtherExSource;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtOtherIncome;
    private com.mac.af.component.derived.input.textfield.CIStringField txtProfession;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPurPrice;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotInc;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalEx;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtYear;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtbIncome;
    public com.mac.af.component.derived.input.textfield.CIStringField txtcontactNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtlandEx;
    public com.mac.af.component.derived.input.textfield.CIStringField txtposition;
    private com.mac.af.component.derived.input.textfield.CIStringField txtproprtydet;
    // End of variables declaration//GEN-END:variables
    public CRadioButtonGroup radioButtonGroup;
    public CRadioButtonGroup radioButtonGroup2;
    public PCFinancialInstitution pCFacility;


    
  

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList();
          
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(
         
            
                );

    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
              );
    }

    @Override
    protected Class getObjectClass() {
        return Loan.class;
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
            txtTotInc,txtTotalEx,txtCashInHand
                
                );
    }

    @Override
    protected void afterInitObject(Loan object) throws ObjectCreatorException {
        super.afterInitObject(object);
        
        Set<LoanMortgageDetails> loanMortgageDetailses =new HashSet<LoanMortgageDetails>();
        LoanMortgageDetails details=new LoanMortgageDetails();
        
                    details.setEmplType((String) radioButtonGroup.getCValue());
                    details.setProfession(txtProfession.getCValue());
                    details.setNameOfOrg(txtNameOfOrg.getCValue());
                    details.setAddressEmp(txtBusinessAddress.getCValue());
                    details.setPosition(txtposition.getCValue());
                    details.setContactNo(txtcontactNo.getCValue());
                    details.setLyear(txtYear.getCValue());
                    details.setLmonth(txtMonth.getCValue());
                    details.setIbasicSal(txtBasicSalary.getCValue());
                    details.setIfixAllowance(txtAllowance.getCValue());
                    details.setIbusinessIncome(txtbIncome.getCValue());
                    details.setIotherIncome(txtOtherIncome.getCValue());
                    details.setIotherIncomeSource(txtIncSource.getCValue());
                    details.setItotalIncome(txtTotInc.getCValue());
                    details.setEhouseHolding(txtHouseHold.getCValue());
                    details.setEbusiExp(txtBusiEx.getCValue());
                    details.setEfinacExp(txtFinancEx.getCValue());
                    details.setEotherExp(txtOtherEx.getCValue());
                    details.setEotherExpSou(txtOtherExSource.getCValue());
                    details.setEtotExp(txtTotalEx.getCValue());
                    details.setMonthlyCashInHand(txtCashInHand.getCValue());
                            
                    loanMortgageDetailses.add(details);
       
                    
                    Set<LoanMortgageProperty> loanMortgagePropertys =new HashSet<LoanMortgageProperty>();
                    LoanMortgageProperty mortgageProperty =new LoanMortgageProperty();
                    
                    mortgageProperty.setAddress(txtproprtydet.getCValue());
                    mortgageProperty.setExtend(txtlandEx.getCValue());
                    mortgageProperty.setFloorArea(txtFlArea.getCValue());
                    mortgageProperty.setPurchasePrice(txtPurPrice.getCValue());
                    mortgageProperty.setForSaleVal(txtFsaleVal.getCValue());
                    mortgageProperty.setConstCost(txtConCost.getCValue());
                    mortgageProperty.setMarketVal(txtMarkVal.getCValue());

                    loanMortgagePropertys.add(mortgageProperty);
       
                    
        object.setLoanMortgageDetailses(loanMortgageDetailses);
        object.setLoanMortgagePropertys(loanMortgagePropertys);
        object.setLoanMInstitutions((List<LoanMInstitution>) tblExisting.getCValue());
       
        
        
        
        
    }
    
   
    

    @Override
    public Loan getValue() throws ObjectCreatorException {
        return super.getValue(); //To change body of generated methods, choose Tools | Templates.
    }
    

   
}