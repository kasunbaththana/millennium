/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_application.object_creator;

import com.mac.loan.PanaltyType;
import com.mac.loan.PaymentTerm;
import com.mac.loan.loan_application.SERLoanApplication;
import com.mac.loan.zutil.calculation.LoanCalculationUtil;
import com.mac.loan.zutil.calculation.LoanInformation;
import com.mac.loan.zutil.calculation.LoanPaymentInformation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.af.resources.ApplicationResources;
import com.mac.dash_board.client.ClientInformationDashBoard;
import com.mac.loan.InterestMethod;
import com.mac.loan.zobject.ChargeScheme;
import com.mac.loan.zobject.Client;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanType;
import com.mac.loan.zutil.default_charges.LoanDefaultChargesCalculation;
import com.mac.loan.zutil.default_charges.LoanDefaultChargesInformation;
import com.mac.zsystem.model.table_model.loan_application.ClientTableModel;
import com.mac.zsystem.model.table_model.loan_application.LoanTypeTableModel;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DecimalFormat;
import java.util.Collection;
import javax.swing.JDialog;

/**
 *
 * @author soft-master
 */
public class PCLoanPL extends DefaultObjectCreator<Loan> {

    /**
     * Creates new form PCLoan
     */
    public PCLoanPL() {
        initComponents();
        initOthers();
        
    }

    @Action
    public void doRefreshPaymentSchedule() {
        try {
            lblTotCapital.setText("0.00");
            lblTotInterst.setText("0.00");
            lblTotPayment.setText("0.00");
            calculatePaymentSchedule();
        } catch (Exception ex) {
            Logger.getLogger(com.mac.loan.hp_loan_application.object_creator.PCLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doRefreshDefaultCharges() {
        try {
            calculateDefaultCharges();
        } catch (Exception ex) {
            Logger.getLogger(com.mac.loan.hp_loan_application.object_creator.PCLoan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loanTypeChanged() {
        LoanType loanType = (LoanType) cboLoanType.getCValue();
        Client client = (Client) cboClient.getCValue();

        if (loanType != null && client != null) {

            if (loanType.getMinimumAllocation() > client.getMaximumAllocation() && client.getConsiderMaximumAllocation()) {
                mOptionPane.showMessageDialog(this, "Loan minimum allocation exceeds the clients maximum allocation.\n"
                        + "This loan type cannot apply to this client.", "Allocation mismatch", mOptionPane.ERROR_MESSAGE);
                cboLoanType.requestFocus();

                return;
            }
            txtLoanAmount.setCValue(loanType.getMinimumAllocation());
            cboPaymentTerm.setCValue(loanType.getPaymentTerm());
            txtInterestRate.setCValue(loanType.getMaximumInterestRate());
            txtGraceDays.setCValue(loanType.getGraceDays());
            txtInstallmentCount.setCValue(loanType.getInstallmentCount());
            try {
                calculateInstallmentAmount();
            } catch (Exception ex) {
//                Logger.getLogger(PCLoan.class.getName()).log(Level.SEVERE, null, ex);
            }

            //panalty
            chkPanalty.setSelected(loanType.getPanaltyAvailable());
            txtPanaltyAmount.setCValue(loanType.getPanaltyAmount());
            txtPanaltyRate.setCValue(loanType.getPanaltyRate());
            radioButtonGroup.setCValue(loanType.getPanaltyType());

            //minimum and maximum
            txtLoanAmount.setMinimumValue(loanType.getMinimumAllocation());
            txtLoanAmount.setMaximumValue(
                    client.getConsiderMaximumAllocation()
                    ? Math.min(loanType.getMaximumAllocation(), client.getMaximumAllocation())
                    : loanType.getMaximumAllocation());

            txtInterestRate.setMinimumValue(loanType.getMinimumInterestRate());
            txtInterestRate.setMaximumValue(loanType.getMaximumInterestRate());

            System.out.println("loanType____" + loanType);
           
        }

    }

    private void calculateInstallmentAmount() throws Exception {
        Double loanAmount;
        Double interestRate;
        Integer installmentCount;
        String paymentTerm;
        String interestMethod;
        Date expectedLoanDate;
        try {
            calculateDefaultCharges();
            loanAmount = txtLoanAmount.getCValue();
            interestRate = txtInterestRate.getCValue();
            installmentCount = txtInstallmentCount.getCValue();

            paymentTerm = (String) cboPaymentTerm.getCValue();
            interestMethod = ((LoanType) cboLoanType.getCValue()).getInterestMethod();
            expectedLoanDate = txtExpectedLoanDate.getCValue();

            LoanInformation information = LoanCalculationUtil.getInstallmentAmount(
                    loanAmount,
                    interestRate,
                    installmentCount,
                    paymentTerm,
                    interestMethod,
                    expectedLoanDate);

            //SET LOAN INFORMATION
            txtInstallmentAmount.setCValue(information.getInstallmentAmount());

            lblInfo.setVisible(false);
        } catch (Exception ex) {
            txtInstallmentAmount.setCValue(0.0);

            lblInfo.setVisible(true);
            lblInfo.setCValue(ex.getMessage());

            throw ex;
        }
    }

    private void calculateDefaultCharges() {
        tblDefaultCharges.setCValue(null);
        /*
         List<com.mac.loan.zobject.LoanDefaultCharge> loanDefaultCharge;
         List<LoanDefaultChargesInformation> information;
         HibernateDatabaseService databaseService = null;
         try {
         databaseService = getCPanel().getDatabaseService();

         loanDefaultCharge = databaseService.getCollection("FROM com.mac.loan.zobject.LoanDefaultCharge WHERE active=true");
         double loanAmount = txtLoanAmount.getCValue();
         LoanType loanType = (LoanType) cboLoanType.getCValue();

         if (loanType != null) {
         information = LoanDefaultChargesCalculation.getDefaultCharges(loanDefaultCharge, loanAmount, loanType.getCode());
         } else {
         information = new ArrayList<>();
         }

         //Set Loan Default Charges Information
         tblDefaultCharges.setCValue(information);
         } catch (Exception e) {
         }
        
         */
//        information = LoanDefaultChargesCalculation.getDefaultCharges(loanDefaultCharge, loanAmount, loanType.getCode());

        LoanType loanType = (LoanType) cboLoanType.getCValue();
        double loanAmount = txtLoanAmount.getCValue();
        double InstallmentCount = txtInstallmentCount.getCValue();

        Collection<ChargeScheme> chargeSchemes = loanType.getChargeSchemes();

        List<LoanDefaultChargesInformation> information = LoanDefaultChargesCalculation.getDefaultCharges(chargeSchemes, loanAmount, loanType,InstallmentCount);
        tblDefaultCharges.setCValue(information);

       

    }

    private void calculatePaymentSchedule() throws Exception {
        Double loanAmount;
        Double interestRate;
        Integer installmentCount;
        String paymentTerm;
        String interestMethod;
        HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
        Date expectedLoanDate;
        try {
            loanAmount = txtLoanAmount.getCValue();
            interestRate = txtInterestRate.getCValue();
            installmentCount = txtInstallmentCount.getCValue();

            paymentTerm = (String) cboPaymentTerm.getCValue();
            interestMethod = ((LoanType) cboLoanType.getCValue()).getInterestMethod();
            expectedLoanDate = txtExpectedLoanDate.getCValue();

            List<LoanPaymentInformation> information = LoanCalculationUtil.getPaymentSchedule(
                    databaseService,
                    loanAmount,
                    interestRate,
                    installmentCount,
                    paymentTerm,
                    interestMethod,
                    expectedLoanDate);
            double xInterst = 0;
            double xCapital = 0;
            double xpayment = 0;
            for (LoanPaymentInformation xx : information) {
                xCapital = xCapital + xx.getCapitalAmount();
                xInterst = xInterst + xx.getInterestAmount();
                xpayment = xpayment + xx.getPaymentAmount();
            }

            DecimalFormat df = new DecimalFormat("#,##0.00");
            lblTotCapital.setText("Total Capital Amount : " + df.format(xCapital));
            lblTotInterst.setText("Total Interst Amount : " + df.format(xInterst));
            lblTotPayment.setText("Total Payment Amount : " + df.format(xpayment));



            //SET LOAN INFORMATION
            ((CTableModel) tblPaymentSchedule.getModel()).setTableData(information);

            lblInfo.setVisible(false);

        } catch (Exception ex) {
            ((CTableModel) tblPaymentSchedule.getModel()).setTableData(Collections.emptyList());

            lblInfo.setVisible(true);
            lblInfo.setCValue(ex.getMessage());

            throw ex;
        }
    }

    @Override
    public void setEditMood() {
        System.out.println("Edit");
        
        super.setEditMood();
       
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
       
        cboPaymentTerm.setValueEditable(false);
       
       
        
        radioButtonGroup = new CRadioButtonGroup();

        radPanaltyNone.setValue(PanaltyType.NONE);
        radPanaltyRate.setValue(PanaltyType.RATE);
        radPanaltyAmount.setValue(PanaltyType.AMOUNT);

        radioButtonGroup.addRadioButton(radPanaltyNone);
        radioButtonGroup.addRadioButton(radPanaltyRate);
        radioButtonGroup.addRadioButton(radPanaltyAmount);
        radioButtonGroup.setDefaultRadioButton(radPanaltyNone);

        radPanaltyRate.addEnabilityComponent(txtPanaltyRate);
        radPanaltyAmount.addEnabilityComponent(txtPanaltyAmount);

        chkPanalty.addRadioButtonGroup(radioButtonGroup);
        chkPanalty.addComponent(txtGraceDays);

        serLoanApplication = new SERLoanApplication(this);

        cboClient.setExpressEditable(true);
       
        cboClient.setTableModel(new ClientTableModel());
       
        cboLoanType.setExpressEditable(true);
        cboLoanType.setTableModel(new LoanTypeTableModel());

        //manual actions
        cboLoanType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loanTypeChanged();

                if (cboLoanType.getCValue() != null && txtLoanAmount.getCValue() != null && txtLoanAmount.getCValue() != 0.00) {
                    calculateDefaultCharges();
                }
            }
        });
        FocusAdapter focusAdapter = new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                try {
                    calculateInstallmentAmount();
                } catch (Exception ex) {
                }
            }
        };

        txtLoanAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                if (cboLoanType.getCValue() != null && txtLoanAmount.getCValue() != null && txtLoanAmount.getCValue() != 0.00) {
                    calculateDefaultCharges();
                }
            }
        });

        FocusAdapter focusAdapterInterst = new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
//                LoanType type= (LoanType) cboLoanType.getCValue();
                try {
//                    if(type.getInterestMethod().equals(InterestMethod.FLAT_INTERST_FIRST)){
//                    }else{
                    calculateInterrest();
//                        System.out.println("calculateInterrest OK");
//                    }
                } catch (Exception ex) {
                }
            }
        };

        txtLoanAmount.addFocusListener(focusAdapter);
        txtInstallmentCount.addFocusListener(focusAdapter);
        txtInterestRate.addFocusListener(focusAdapter);
       
        txtInstallmentAmount.addFocusListener(focusAdapterInterst);
          
             
            


//        jTabbedPane1.addChangeListener(new ChangeListener() {
//            @Override
//            public void stateChanged(ChangeEvent e) {
//                calculateInterrest();
//            }
//        });

        lblInfo.setVisible(false);

        tblPaymentSchedule.setCModel(
                new CTableModel(new CTableColumn[]{
            new CTableColumn("Date", "paymentDate"),
            new CTableColumn("Capital Amount", "capitalAmount"),
            new CTableColumn("Interest Amount", "interestAmount"),
            new CTableColumn("Payment Amount", "paymentAmount")
        }));

        tblDefaultCharges.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Name", "chargeScheme", "name"),
            new CTableColumn("Charge Amount", new String[]{"chargeAmount"}, false)
        }));

        btnRefreshSchedule.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_REFRESH, 16, 16));
        btnRefreshDefaultCharge.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_REFRESH, 16, 16));

        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnRefreshSchedule, "doRefreshPaymentSchedule");
        actionUtil.setAction(btnRefreshDefaultCharge, "doRefreshDefaultCharges");

        cboClient.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Client client = (Client) cboClient.getCValue();
                if (client != null) {
                    if (client.getStatus().equals("BLACK_LIST")) {
                        mOptionPane.showMessageDialog(null, "This Client BLACKLISTED!.", "Blacklist Message", mOptionPane.INFORMATION_MESSAGE);
                        cboClient.setCValue(null);
                    }
                }
            }
        });

        cboLoanType.doRefresh();

    }

    private void calculateInterrest() {
        double loanAmount = txtLoanAmount.getCValue();
        double installmentCount = txtInstallmentCount.getCValue();
        double installAmount = txtInstallmentAmount.getCValue();
        double paymentCapital = loanAmount / installmentCount;
  LoanType type= (LoanType) cboLoanType.getCValue();
  double monthInters=0.0;
        if(type.getInterestMethod().equals(InterestMethod.FLAT_INTERST_FIRST)){
            
             monthInters = ((installAmount) * 100) / loanAmount;
        }else if(type.getInterestMethod().equals(InterestMethod.REDUCING)){
            
           // monthInters = ((installAmount - paymentCapital) * 100) / loanAmount;
            
            monthInters = simpleCalculateRate(installmentCount, installAmount, loanAmount*-1) ;
        }
        
        else{
            
         monthInters = ((installAmount - paymentCapital)) / loanAmount;
        }
        

        double interstRate = 0.0;

        switch (cboPaymentTerm.getCValue().toString()) {
            case PaymentTerm.MONTHLY:
                interstRate = (monthInters * 12) * 100;
                break;
            case PaymentTerm.WEEKLY:
                interstRate = (monthInters * 48) * 100 ;
                break;
            case PaymentTerm.DAILY:
                interstRate = (monthInters * 300) * 100;
                break;
            case PaymentTerm.QUARTER:
                interstRate = monthInters * 2;
                break;
            default:
                throw new AssertionError();
        }
        System.out.println("XXX monthIntersAmount val---" + monthInters);


//        System.out.println("myRate..........._________"+interstRate);

        txtInterestRate.setCValue(interstRate);
    }
    private static DecimalFormat df2 = new DecimalFormat("#.############");
        public static double simpleCalculateRate(double nper, double pmt, double pv) {
 
        double fv = 0;
 
                 //0 or omitted - end of payment
        double type = 0;
 
                 // If the expected interest rate is omitted, the value is assumed to be 10%.
        double guess = 0.1;
        
      
         double rate = Double.parseDouble(df2.format(calculateRate(nper, pmt, pv, fv, type, guess)));
 
        return rate;
    }
    
        public static double calculateRate(double nper, double pmt, double pv, double fv, double type, double guess) {
        //FROM MS http://office.microsoft.com/en-us/excel-help/rate-HP005209232.aspx
        int FINANCIAL_MAX_ITERATIONS = 128;//Bet accuracy with 128
        double FINANCIAL_PRECISION = 0.0000001;//1.0e-8
 
        double y, y0, y1, x0, x1 = 0, f = 0, i = 0;
        double rate = guess;
        if (Math.abs(rate) < FINANCIAL_PRECISION) {
            y = pv * (1 + nper * rate) + pmt * (1 + rate * type) * nper + fv;
        } else {
            f = Math.exp(nper * Math.log(1 + rate));
            y = pv * f + pmt * (1 / rate + type) * (f - 1) + fv;
        }
        y0 = pv + pmt * nper + fv;
        y1 = pv * f + pmt * (1 / rate + type) * (f - 1) + fv;
 
        // find root by Newton secant method
        i = x0 = 0.0;
        x1 = rate;
        while ((Math.abs(y0 - y1) > FINANCIAL_PRECISION) && (i < FINANCIAL_MAX_ITERATIONS)) {
            rate = (y1 * x0 - y0 * x1) / (y1 - y0);
            x0 = x1;
            x1 = rate;
 
            if (Math.abs(rate) < FINANCIAL_PRECISION) {
                y = pv * (1 + nper * rate) + pmt * (1 + rate * type) * nper + fv;
            } else {
                f = Math.exp(nper * Math.log(1 + rate));
                y = pv * f + pmt * (1 / rate + type) * (f - 1) + fv;
            }
 
            y0 = y1;
            y1 = y;
            ++i;
        }
        return rate;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cIStringField1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        txtInstallmentCount = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        lblInfo1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInstallmentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cboPaymentTerm = new com.mac.af.component.derived.input.combobox.CIComboBox(PaymentTerm.ALL);
        txtExpectedLoanDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        txtInterestRate = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        radPanaltyNone = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        chkPanalty = new com.mac.af.component.derived.input.checkbox.CIEnabilityCheckBox();
        cLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        cboLoanType = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return serLoanApplication.getLoanTypesPl();
            }

        };
        radPanaltyRate = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        txtGraceDays = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        radPanaltyAmount = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPanaltyRate = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPanaltyAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cboClient = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return serLoanApplication.getClients();
            }
        };
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        lblInfo = new com.mac.af.component.derived.display.label.CDLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtReason = new com.mac.af.component.derived.input.textarea.CITextArea();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtVehicleNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        jButton1 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblDefaultCharges = new com.mac.af.component.derived.display.table.CDTable();
        btnRefreshDefaultCharge = new com.mac.af.component.derived.command.button.CCButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPaymentSchedule = new com.mac.af.component.derived.display.table.CDTable();
        btnRefreshSchedule = new com.mac.af.component.derived.command.button.CCButton();
        lblTotInterst = new javax.swing.JLabel();
        lblTotPayment = new javax.swing.JLabel();
        lblTotCapital = new javax.swing.JLabel();

        cIStringField1.setText("cIStringField1");

        cLabel10.setText("Grace Days :");

        lblInfo1.setText("ANNUAL");

        cLabel11.setText("Expected Date :");

        cLabel4.setText("Loan Amount :");

        radPanaltyNone.setText("None");

        chkPanalty.setText("Penalty");

        cLabel9.setText("Other Referance :");

        radPanaltyRate.setText("Rate :");

        radPanaltyAmount.setText("Amount :");

        cLabel3.setText("Loan Type :");

        cLabel6.setText("Installment Count :");

        cLabel7.setText("Interest Rate :");

        cLabel1.setText("Client :");

        cLabel8.setText("Payment Term  :");

        cLabel5.setText("Installment Amount :");

        txtReason.setColumns(20);
        txtReason.setRows(5);
        jScrollPane2.setViewportView(txtReason);

        cLabel12.setText("Purpose Of Funding :");

        jButton1.setText("?");
        jButton1.setBorder(null);
        jButton1.setBorderPainted(false);
        jButton1.setMargin(new java.awt.Insets(2, 1, 2, 14));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(chkPanalty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(radPanaltyNone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radPanaltyRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPanaltyRate, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(radPanaltyAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPanaltyAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cboLoanType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtExpectedLoanDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboPaymentTerm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtInterestRate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblInfo1, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtInstallmentCount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(cboClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(2, 2, 2)
                                        .addComponent(jButton1))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(txtVehicleNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(jScrollPane2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(114, 114, 114)
                        .addComponent(txtGraceDays, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtExpectedLoanDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoanType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboPaymentTerm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInterestRate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblInfo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInstallmentCount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkPanalty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radPanaltyNone, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radPanaltyRate, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPanaltyRate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radPanaltyAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPanaltyAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtGraceDays, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtVehicleNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jTabbedPane1.addTab("Loan Information", jPanel1);

        tblDefaultCharges.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(tblDefaultCharges);

        btnRefreshDefaultCharge.setText("Refresh");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnRefreshDefaultCharge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 442, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRefreshDefaultCharge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Default Charges", jPanel3);

        tblPaymentSchedule.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblPaymentSchedule);

        btnRefreshSchedule.setText("Refresh");
        btnRefreshSchedule.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshScheduleActionPerformed(evt);
            }
        });

        lblTotInterst.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotInterst.setText("0.00");

        lblTotPayment.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotPayment.setText("0.00");

        lblTotCapital.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotCapital.setText("0.00");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnRefreshSchedule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblTotPayment, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                    .addComponent(lblTotInterst, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTotCapital, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTotCapital)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotInterst)
                    .addComponent(btnRefreshSchedule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTotPayment)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Payment Schedule", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshScheduleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshScheduleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRefreshScheduleActionPerformed
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Client clnt = (Client) cboClient.getCValue();
        if (clnt != null) {
            ClientInformationDashBoard panel = new ClientInformationDashBoard(clnt.getCode());
//        panel.setC
            JDialog dialog = new JDialog();
            dialog.add(panel);
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            dialog.setBounds(0, 0, screenSize.width - 50, screenSize.height - 50);
//        dialog.setLocationRelativeTo(CApplication.getInstance().getMainFrame());
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
        } else {
            mOptionPane.showMessageDialog(null, "Please Select Client", "Selection ..", mOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_jButton1ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnRefreshDefaultCharge;
    private com.mac.af.component.derived.command.button.CCButton btnRefreshSchedule;
    private com.mac.af.component.derived.input.textfield.CIStringField cIStringField1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboClient;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanType;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboPaymentTerm;
    private com.mac.af.component.derived.input.checkbox.CIEnabilityCheckBox chkPanalty;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblInfo;
    private com.mac.af.component.derived.display.label.CDLabel lblInfo1;
    private javax.swing.JLabel lblTotCapital;
    private javax.swing.JLabel lblTotInterst;
    private javax.swing.JLabel lblTotPayment;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton radPanaltyAmount;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton radPanaltyNone;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton radPanaltyRate;
    private com.mac.af.component.derived.display.table.CDTable tblDefaultCharges;
    private com.mac.af.component.derived.display.table.CDTable tblPaymentSchedule;
    private com.mac.af.component.derived.input.textfield.CIDateField txtExpectedLoanDate;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtGraceDays;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtInstallmentAmount;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtInstallmentCount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtInterestRate;
    public com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPanaltyAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPanaltyRate;
    private com.mac.af.component.derived.input.textarea.CITextArea txtReason;
    private com.mac.af.component.derived.input.textfield.CIStringField txtVehicleNo;
    // End of variables declaration//GEN-END:variables
    private CRadioButtonGroup radioButtonGroup;
    private SERLoanApplication serLoanApplication;

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList(
                txtExpectedLoanDate,
                cboClient,
                cboLoanType,
                txtLoanAmount,
                txtInstallmentAmount,
                txtInstallmentCount,
                txtInterestRate,
                cboPaymentTerm,
                chkPanalty,
                txtGraceDays);
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(
                txtVehicleNo,
                txtReason);

    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtExpectedLoanDate, "expectedLoanDate"),
                new CInputComponentBinder(cboClient, "client"),
                new CInputComponentBinder(cboLoanType, "loanType"),
                new CInputComponentBinder(txtLoanAmount, "loanAmount"),
                new CInputComponentBinder(txtInstallmentAmount, "installmentAmount"),
                new CInputComponentBinder(txtInstallmentCount, "installmentCount"),
                new CInputComponentBinder(txtInterestRate, "interestRate"),
                new CInputComponentBinder(cboPaymentTerm, "paymentTerm"),
                new CInputComponentBinder(chkPanalty, "panalty"),
                new CInputComponentBinder(radioButtonGroup, "panaltyType"),
                new CInputComponentBinder(txtPanaltyRate, "panaltyRate"),
                new CInputComponentBinder(txtPanaltyAmount, "panaltyAmount"),
                new CInputComponentBinder(txtGraceDays, "graceDays"),
                new CInputComponentBinder(txtVehicleNo, "note"),
                new CInputComponentBinder(txtReason, "reason"));
    }

    @Override
    protected Class getObjectClass() {
        return Loan.class;
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
                cboPaymentTerm,
                txtInstallmentAmount,
                chkPanalty,
                txtGraceDays);
    }

    @Override
    protected void afterInitObject(Loan object) throws ObjectCreatorException {
        object.setDefaultChargesInformations((List<LoanDefaultChargesInformation>) tblDefaultCharges.getCValue());
//        System.out.println(tb);
    }

    @Override
    public Loan getValue() throws ObjectCreatorException {
        Loan loan = super.getValue();

     

        return loan;
    }
}