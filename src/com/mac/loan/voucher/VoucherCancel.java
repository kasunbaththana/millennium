/*
 *  VoucherCancel.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 8:17:06 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.voucher;

import com.mac.af.core.database.DatabaseException;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction_cancel.TransactionCancel;
import com.mac.zsystem.transaction.transaction_cancel.object.Transaction;

/**
 *
 * @author mohan
 */
public class VoucherCancel extends TransactionCancel {

    @Override
    protected String getTransactionType() {
        return SystemTransactions.VOUCHER_TRANSACTION_CODE;
    }

    @Override
    protected void cancelAdditional(Transaction transaction) throws DatabaseException {
        SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.TRANSACTION_CANCEL_TRANSACTION_CODE,
                transaction.getReferenceNo(),
                transaction.getDocumentNo(),
                transaction.getLoan().getIndexNo(),
                null,
                transaction.getLoan().getClient(),
                "Voucher Cancel");

        transaction.getLoan().setAvailableVoucher(true);
        getDatabaseService().save(transaction.getLoan());
    }
}
