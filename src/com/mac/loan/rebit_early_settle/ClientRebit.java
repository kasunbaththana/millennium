/*
 *  ClientRebit.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 13, 2014, 9:50:39 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.rebit_early_settle;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.dialog.object_creator_dialog.ObjectCreatorDialog;
import com.mac.loan.zobject.Loan;
import com.mac.loan.ztemplate.rebit.AbstractRebit;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class ClientRebit extends AbstractRebit {

    @Override
    protected List<Settlement> getSettlements() {
        initLoan();
        initService();

        return serRebit.getSettlements(loan);
    }

    @Override
    protected String getExecuteText() {
        return "Rebit";
    }

    @Override
    protected String getTitle() {
        if (loan != null) {
            return "<HTML><B>Client Rebit - Agreement No.:" + loan.getAgreementNo() + "</B></HTML>";
        } else {
            return "<HTML><B>Client Rebit" + "</B></HTML>";
        }
    }

    @Override
    protected void execute(Collection<Settlement> collection) {
        try {
            //        System.out.println(collection);
            
            for (Settlement settlement : collection) {
                settlement.setStatus(SettlementStatus.SETTLEMENT_REBIT);
            }
            
                    getDatabaseService().save(collection);
                    mOptionPane.showMessageDialog(this, "Rebit successful !", "Rebit", mOptionPane.INFORMATION_MESSAGE);
        } catch (DatabaseException ex) {
            Logger.getLogger(ClientRebit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initService() {
        if (serRebit == null) {
            serRebit = new SERRebit(this);
        }
    }

    private void initLoan() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (creatorDialog == null) {
                    initService();

                    creatorDialog = new ObjectCreatorDialog<>();
                    creatorDialog.setObjectCreator(new PCLoan(serRebit));
                    creatorDialog.setTitle("Rebit");
                }

                loan = creatorDialog.getNewObject();
            }
        };
        CApplication.invokeEventDispatch(runnable);
    }
    private Loan loan;
    private ObjectCreatorDialog<Loan> creatorDialog;
    private SERRebit serRebit;
}
