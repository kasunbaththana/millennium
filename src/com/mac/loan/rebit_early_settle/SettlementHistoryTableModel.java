/*
 *  SettlementHistoryTableModel.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 12, 2015, 3:29:44 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.rebit_early_settle;

import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author mohan
 */
public class SettlementHistoryTableModel implements TableModel {

    private List<SettlementHistory> settlementHistorys;
    private List<TableModelListener> modelListeners = new ArrayList<>();

    public List<SettlementHistory> getSettlementHistorys() {
        return settlementHistorys;
    }

    public void setSettlementHistorys(List<SettlementHistory> settlementHistorys) {
        this.settlementHistorys = settlementHistorys;
        
        fireTableModelListeners();
    }

    @Override
    public int getRowCount() {
        return settlementHistorys != null ? settlementHistorys.size() : 0;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Due Date";
            case 1:
                return "Description";
            case 2:
                return "Amount";
            case 3:
                return "Balance Amount";
            case 4:
                return "Rebit Amount";
            case 5:
                return "Type";
            default:
                throw new AssertionError();
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Date.class;
            case 1:
                return String.class;
            case 2:
                return Double.class;
            case 3:
                return Double.class;
            case 4:
                return Double.class;
            case 5:
                return String.class;
            default:
                throw new AssertionError();
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SettlementHistory settlementHistory = settlementHistorys.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return settlementHistory.getSettlement().getDueDate();
            case 1:
                return settlementHistory.getSettlement().getDescription();
            case 2:
                return settlementHistory.getSettlement().getAmount();
            case 3:
                return settlementHistory.getSettlement().getBalanceAmount();
            case 4:
                return settlementHistory.getSettlementAmount();
            case 5:
                return settlementHistory.getSettlement().getSettlementType().getDescription();
            default:
                throw new AssertionError();
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        SettlementHistory settlementHistory = settlementHistorys.get(rowIndex);

        if (aValue instanceof Double) {
            double rebitAmount = (double) aValue;
            
            if (settlementHistory.getSettlement().getBalanceAmount()<rebitAmount) {
                settlementHistory.setSettlementAmount(settlementHistory.getSettlement().getBalanceAmount());
            }else{
                settlementHistory.setSettlementAmount(rebitAmount);
            }
        }
        
        fireTableModelListeners();
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        modelListeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        modelListeners.remove(l);
    }

    public void fireTableModelListeners() {
        TableModelEvent modelEvent = new TableModelEvent(this);
        for (TableModelListener tableModelListener : modelListeners) {
            tableModelListener.tableChanged(modelEvent);
        }
    }
}
