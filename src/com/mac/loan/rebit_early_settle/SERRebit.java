/*
 *  SERRebit.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 13, 2014, 11:21:06 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.rebit_early_settle;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.util.compare.comparison_chain.ComparisonChain;
import com.mac.loan.LoanStatus;
import com.mac.loan.rebit.custom_object.RebitObject;
import com.mac.loan.zobject.Loan;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.EarlySettleLoanAccountInterface;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.AdvancedPayment;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class SERRebit extends AbstractService {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public SERRebit(Component component) {
        super(component);
    }

    public List getSettlements(Loan loan) {
        String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement "
                + "WHERE settlementType.creditOrDebit=:CREDIT_OR_DEBIT "
                + "AND loan=:LOAN "
                + "AND settlementType.code IN (:LOAN_INTEREST,:LOAN_CAPITAL) "
                + "AND status=:STATUS";

        HashMap<String, Object> params = new HashMap<>();
        params.put("LOAN", loan.getIndexNo());
        params.put("CREDIT_OR_DEBIT", SettlementCreditDebit.CREDIT);
        params.put("LOAN_INTEREST", SystemSettlement.LOAN_INTEREST);
        params.put("LOAN_CAPITAL", SystemSettlement.LOAN_CAPITAL);
        params.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);

        List<Settlement> settlements;

        List<SettlementHistory> settlementHistory = new ArrayList();

        try {
            settlements = getDatabaseService().getCollection(hql, params);

            for (Settlement settlement : settlements) {
                SettlementHistory history = new SettlementHistory();

                history.setSettlement(settlement);
                history.setSettlementAmount(0.0);
                history.setBeforeBalance(settlement.getBalanceAmount());
                history.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                history.setTransactionType(SystemTransactions.EARLY_SETTLE_REBIT_TRANSACTION_CODE);
                history.setStatus(RebitStatus.ACTIVE);

                settlementHistory.add(history);
            }
            sortReceiptReceipts(settlementHistory);
        } catch (DatabaseException ex) {
            Logger.getLogger(ClientRebit.class.getName()).log(Level.SEVERE, null, ex);
        }

        return settlementHistory;
    }
    
     public List getCPAmount(int loan) {
           List<Settlement> settlements;
        String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement "
                + "WHERE  loan=:LOAN "
                + "AND settlementType.code  IN (:LOAN_CAPITAL,:LOAN_PANALTY) "
                + "AND status=:STATUS";

        HashMap<String, Object> params = new HashMap<>();
        params.put("LOAN", loan);
        params.put("LOAN_CAPITAL", SystemSettlement.LOAN_CAPITAL);
        params.put("LOAN_PANALTY", SystemSettlement.LOAN_PANALTY);
        params.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);

         try {
            settlements = getDatabaseService().getCollection(hql,params);
             
         } catch (Exception e) {
             settlements =new ArrayList<>();
         }
        return settlements;
    }
    

    public void sortReceiptReceipts(List<SettlementHistory> settlementHistory) {
        List<SettlementHistory> panalties = new ArrayList<>();
        for (SettlementHistory panalty : settlementHistory) {
            if (panalty.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                panalties.add(panalty);
            }
        }
        settlementHistory.removeAll(panalties);

        Collections.sort(settlementHistory, new Comparator<SettlementHistory>() {
            @Override
            public int compare(SettlementHistory o1, SettlementHistory o2) {
                return ComparisonChain.start()
                        .compare(o1.getSettlement().getSettlementType().getPriority(), o2.getSettlement().getSettlementType().getPriority())
                        .compare(o1.getSettlement().getDueDate(), o2.getSettlement().getDueDate())
                        .result();
            }
        });
        settlementHistory.addAll(0, panalties);
    }

    public List getLoans() {
        List<Loan> loans;

        String hql = "FROM com.mac.loan.zobject.Loan WHERE status=:STATUS";
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("STATUS", LoanStatus.LOAN_START);

        try {
            loans = getDatabaseService().getCollection(hql, hashMap);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERRebit.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }
    
    public List getAdvanced(int loan) {
        List<AdvancedPayment> loans;

        String hql = "FROM com.mac.zsystem.transaction.settlement.object.AdvancedPayment WHERE send=:SEND and status=:STATUS and loan=:LOAN  ";
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("STATUS", "ACTIVE");
        hashMap.put("SEND", false);
        hashMap.put("LOAN", loan);

        try {
            loans = getDatabaseService().getCollection(hql, hashMap);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERRebit.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }

    public void save(RebitObject rebit) throws DatabaseException {
        //transaction
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.EARLY_SETTLE_REBIT_TRANSACTION_CODE,
                rebit.getReferenceNo(),
                rebit.getDocumentNo(),
                rebit.getLoan().getIndexNo(),
                null,
                rebit.getLoan().getClient().getCode(),
                rebit.getNote());
        //settlement
        double interstRebit = 0.0;
        double capitalRebit = 0.0;
        double panaltyRebit = 0.0;
        double otherChargRebit = 0.0;
        double insuranceRebit = 0.0;

        double totalRebit = 0.0;

        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();

        for (SettlementHistory settlementHistory : rebit.getSettlementHistorys()) {
            systemSettlement.addSettlementHistoryQueue(
                    settlementHistory.getSettlement(),
                    SystemTransactions.EARLY_SETTLE_REBIT_TRANSACTION_CODE,
                    transactionIndex,
                    settlementHistory.getSettlementAmount());
            String stmType = settlementHistory.getSettlement().getSettlementType().getCode();

//            System.out.println(settlementHistory.getSettlement().getSettlementType().getCode()+"-"
//                    + "-----"+SystemSettlement.LOAN_INTEREST);
            totalRebit = totalRebit + settlementHistory.getSettlementAmount();
            switch (stmType) {
                case SystemSettlement.LOAN_INTEREST:
                    interstRebit = interstRebit + settlementHistory.getSettlementAmount();
                    break;
                case SystemSettlement.LOAN_PANALTY:
                    panaltyRebit = panaltyRebit + settlementHistory.getSettlementAmount();
                    break;
                case SystemSettlement.OTHER_CHARGE:
                    otherChargRebit = otherChargRebit + settlementHistory.getSettlementAmount();
                    break;
                case SystemSettlement.INSURANCE:
                    insuranceRebit = insuranceRebit + settlementHistory.getSettlementAmount();
                    break;
                case SystemSettlement.LOAN_CAPITAL://
                    capitalRebit = capitalRebit + settlementHistory.getSettlementAmount();
                    break;
            }
        }

        System.out.println("interstRebit" + interstRebit);
        System.out.println("panaltyRebit" + panaltyRebit);
        System.out.println("otherChargRebit" + otherChargRebit);
        System.out.println("insuranceRebit" + insuranceRebit);
        System.out.println("LOAN_CAPITALRebit" + capitalRebit);
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());

        Loan loan=new Loan();
        loan = rebit.getLoan();
        loan.setRebateapprove(false);
        loan.setReason("EARLY_SETTLEMENT"); 
        getDatabaseService().save(loan);
        getDatabaseService().commitLocalTransaction();
        
          //Advanced Payment
        double valAdvanced=0.0;
        List<AdvancedPayment> listAdvancedPayment=getAdvanced(rebit.getLoan().getIndexNo());
        for(AdvancedPayment advancedPayment:listAdvancedPayment)
        {
           valAdvanced += advancedPayment.getBalanceAmount();
        }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        //Account Credit
        accountTransaction.addAccountTransactionQueue(
                EarlySettleLoanAccountInterface.REBIT_AMOUNT_CREDIT_CODE,
                "Client Early Rebit Tot",
                totalRebit,
                AccountTransactionType.AUTO);
        //Interst Amount
        accountTransaction.addAccountTransactionQueue(
                EarlySettleLoanAccountInterface.REBIT_INTEREST_AMOUNT_DEBIT_CODE,
                "Client Early Rebit",
                interstRebit,
                AccountTransactionType.AUTO);
        //Capital Amount
        accountTransaction.addAccountTransactionQueue(
                EarlySettleLoanAccountInterface.REBIT_CAPITAL_AMOUNT_DEBIT_CODE,
                "Client Early Receipt",
                capitalRebit,
                AccountTransactionType.AUTO);
        //Panalty
        accountTransaction.addAccountTransactionQueue(
                EarlySettleLoanAccountInterface.REBIT_PANALTY_AMOUNT_DEBIT_CODE,
                "Client Early Rebit",
                panaltyRebit,
                AccountTransactionType.AUTO);
        //Other Charg
        accountTransaction.addAccountTransactionQueue(
                EarlySettleLoanAccountInterface.REBIT_OTHER_CHARGE_AMOUNT_DEBIT_CODE,
                "Client Early Rebit",
                otherChargRebit,
                AccountTransactionType.AUTO);
        //Insurance Amount
        accountTransaction.addAccountTransactionQueue(
                EarlySettleLoanAccountInterface.REBIT_INSURANCE_AMOUNT_DEBIT_CODE,
                "Client Early Rebit",
                insuranceRebit,
                AccountTransactionType.AUTO);
        
        // advanced add account
        accountTransaction.addAccountTransactionQueue(
                EarlySettleLoanAccountInterface.ES_ADVANCED_AMOUNT_CREDIT_CODE,
                "Advanced Amount",
                valAdvanced,
                AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(
                EarlySettleLoanAccountInterface.ES_ADVANCED_AMOUNT_DEBIT_CODE,
                "Advanced Amount",
                valAdvanced,
                AccountTransactionType.AUTO);
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex,
                SystemTransactions.EARLY_SETTLE_REBIT_TRANSACTION_CODE);
        
        try {
            
         if(rebit.getLoan()!=null){
            
                //debit entry
                String advance_status_Procedure = "CALL z_advance_status_change( '" + rebit.getLoan().getIndexNo() + "' );";
                getDatabaseService().callUpdateProcedure(advance_status_Procedure);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        

    }

//    private static Connection getConnection() throws SQLException {
//        Connection connection = CConnectionProvider.getInstance().getConnection();
//        return connection;
//    }
// //settlement
//        double interstRebit = 0.0;
//        double panaltyRebit = 0.0;
//        double otherChargRebit = 0.0;
//        double insuranceRebit = 0.0;
////        double capitalRebit = 0.0;
//
//        double totalRebit = 0.0;
//
//        double dueInterst = 0.0;
////        double dueCapital = 0.0;
//        double dueTotal = 0.0;
//
//
//        SystemSettlement systemSettlement = SystemSettlement.getInstance();
//        systemSettlement.beginSettlementHistoryQueue();
//
//        for (SettlementHistory settlementHistory : rebit.getSettlementHistorys()) {
//
//            systemSettlement.addSettlementHistoryQueue(
//                    settlementHistory.getSettlement(),
//                    SystemTransactions.EARLY_SETTLE_REBIT_TRANSACTION_CODE,
//                    transactionIndex,
//                    settlementHistory.getSettlementAmount());
//            String stmType = settlementHistory.getSettlement().getSettlementType().getCode();
//
//            totalRebit = totalRebit + settlementHistory.getSettlementAmount();
//            switch (stmType) {
//                case SystemSettlement.LOAN_INTEREST:
//                    interstRebit = interstRebit + settlementHistory.getSettlementAmount();
//                    break;
//                case SystemSettlement.LOAN_PANALTY:
//                    panaltyRebit = panaltyRebit + settlementHistory.getSettlementAmount();
//                    break;
//                case SystemSettlement.OTHER_CHARGE:
//                    otherChargRebit = otherChargRebit + settlementHistory.getSettlementAmount();
//                    break;
//                case SystemSettlement.INSURANCE:
//                    insuranceRebit = insuranceRebit + settlementHistory.getSettlementAmount();
//                    break;
////                case SystemSettlement.LOAN_CAPITAL:
////                    capitalRebit = capitalRebit + settlementHistory.getSettlementAmount();
////                    break;
//            }
//
//
//        }
//
//
//        //-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//        String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement "
//                + "WHERE settlementType.creditOrDebit=:CREDIT_OR_DEBIT "
//                + "AND loan=:LOAN "
//                //                + "AND settlementType.code <> :LOAN_CAPITAL "
//                + "AND status=:STATUS";
//
//        HashMap<String, Object> params = new HashMap<>();
//        params.put("LOAN", rebit.getLoan().getIndexNo());
//        params.put("CREDIT_OR_DEBIT", SettlementCreditDebit.CREDIT);
////        params.put("LOAN_CAPITAL", SystemSettlement.LOAN_CAPITAL);
//        params.put("STATUS", SettlementStatus.SETTLEMENT_PENDING);
//
//        List<Settlement> settlements;
//        settlements = getDatabaseService().getCollection(hql, params);
//        for (Settlement settlement : settlements) {
//
//            String settlementType = settlement.getSettlementType().getCode();
//            switch (settlementType) {
//                case SystemSettlement.LOAN_INTEREST:
//                    dueInterst = dueInterst + settlement.getAmount();
//                    break;
//            }
//
//        }
//        dueTotal = dueInterst;
//        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
//
//
//        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
//        accountTransaction.beginAccountTransactionQueue();
//
//        accountTransaction.addAccountTransactionQueue(
//                EarlySettleLoanAccountInterface.LOAN_INTEREST_DUE_CREDIT_CODE,
//                "dueInterst",
//                dueInterst,
//                AccountTransactionType.AUTO);
//        accountTransaction.addAccountTransactionQueue(
//                EarlySettleLoanAccountInterface.LOAN_INSTALL_DUE_DEBIT_CODE,
//                "dueTotal",
//                dueTotal,
//                AccountTransactionType.AUTO);
//        accountTransaction.addAccountTransactionQueue(
//                EarlySettleLoanAccountInterface.REBIT_INTEREST_AMOUNT_DEBIT_CODE,
//                "Client Rebit",
//                interstRebit,
//                AccountTransactionType.AUTO);
//        accountTransaction.addAccountTransactionQueue(
//                EarlySettleLoanAccountInterface.REBIT_PANALTY_AMOUNT_DEBIT_CODE,
//                "Client Rebit",
//                panaltyRebit,
//                AccountTransactionType.AUTO);
//        accountTransaction.addAccountTransactionQueue(
//                EarlySettleLoanAccountInterface.REBIT_OTHER_CHARGE_AMOUNT_DEBIT_CODE,
//                "Client Rebit",
//                otherChargRebit,
//                AccountTransactionType.AUTO);
//        accountTransaction.addAccountTransactionQueue(
//                EarlySettleLoanAccountInterface.REBIT_INSURANCE_AMOUNT_DEBIT_CODE,
//                "Client Rebit",
//                insuranceRebit,
//                AccountTransactionType.AUTO);
//        accountTransaction.addAccountTransactionQueue(
//                EarlySettleLoanAccountInterface.REBIT_AMOUNT_CREDIT_CODE,
//                "Client Rebit Tot",
//                totalRebit,
//                AccountTransactionType.AUTO);
//
//
//        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex,
//                SystemTransactions.EARLY_SETTLE_REBIT_TRANSACTION_CODE);

    //    Date date = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
//
//        String sql = " SELECT a.`loan` as loan_index,"
//                + " a.`settlement_type` as type,\n"
//                + " SUM(a.`balance_amount`) as amount,\n"
//                + " a.`account` as account\n"
//                + " FROM advanced_payment a WHERE a.`status`='ACTIVE' \n"
//                + " AND a.`loan`='" + rebit.getLoan().getIndexNo() + "'\n"
//                + " GROUP BY a.`account` ";
//
//        System.out.println(sql);
//
//        try {
//            ResultSet rs = getConnection().createStatement().executeQuery(sql);
//            int loan_index = 0;
//            while (rs.next()) {
//
//                loan_index = rs.getInt("loan_index");
//                String type = rs.getString("type");
//                double amount = rs.getDouble("amount");
//                String account = rs.getString("account");
//
//                //credit entry
//                String cr_Procedure = "CALL z_account_advance_early_settle("
//                        + " '11-400', '" + dateFormat.format(date) + "', '" + amount + "' , '0.0' , '" + transactionIndex + "' ,  '" + loan_index + "' );";
//                getDatabaseService().callUpdateProcedure(cr_Procedure);
//
//                //debit entry
//                String dr_Procedure = "CALL z_account_advance_early_settle("
//                        + " '" + account + "', '" + dateFormat.format(date) + "',  '0.0' ,'" + amount + "'  , '" + transactionIndex + "' ,  '" + loan_index + "' );";
//                getDatabaseService().callUpdateProcedure(dr_Procedure);
//
//            }
//            if(loan_index>0){
//            
//                //debit entry
//                String advance_status_Procedure = "CALL z_advance_status_change( '" + loan_index + "' );";
//                getDatabaseService().callUpdateProcedure(advance_status_Procedure);
//            }



//        } catch (Exception e) {
//            e.printStackTrace();
//        }

}
