/*
 *  Rebit.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 12, 2015, 12:07:52 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.rebit_early_settle;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.renderer.table.CTableRenderer;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.loan.rebit.custom_object.RebitObject;
import com.mac.loan.zobject.Loan;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.AdvancedPayment;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 *
 * @author mohan
 */
public class Rebit extends CPanel {

    /**
     * Creates new form Rebit
     */
    public Rebit() {
        initComponents();

        initOthers();
    }

    @Action
    public void doNew() {
        setNewMode();
    }

    @Action
    public void doSave() {
        int q = mOptionPane.showConfirmDialog(null, "Do you sure want to save current rebit?", "Save", mOptionPane.YES_NO_OPTION);

        if (q == mOptionPane.YES_OPTION) {
            RebitObject object = getObject();
            try {
                serRebit.save(object);
//...
                setDefaultMode();

                mOptionPane.showMessageDialog(null, "Successfully saved !!!", "Save", mOptionPane.INFORMATION_MESSAGE);
            } catch (DatabaseException ex) {
                Logger.getLogger(Rebit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Action
    public void doDiscard() {
        int q = mOptionPane.showConfirmDialog(null, "Do you sure want to discard current rebit?", "Save", mOptionPane.YES_NO_OPTION);

        if (q == mOptionPane.YES_OPTION) {
            setNewMode();
        }

    }

    @Action
    public void doPrint() {
        this.recentButton.doClick();
    }

    @Action
    public void doClose() {
        TabFunctions.closeTab(this);
    }

    private void loanChanged() {
        initLoanInformation();
    }

    private void changeRebitAmount() {
        List<SettlementHistory> settlementHistorys = tableModel.getSettlementHistorys();

        double rebitAmount = 0.0;
        for (SettlementHistory settlementHistory : settlementHistorys) {
            rebitAmount += settlementHistory.getSettlementAmount();
        }
        txtRebitAmount.setCValue(rebitAmount);
        txtAfterBalance.setCValue(loanBalance - rebitAmount);
        txtFinalAmount.setCValue(loanBalance - rebitAmount);
        
        if (chIsAddPanalty.isSelected()) {
            txtFinalAmount.setCValue(txtFinalAmount.getCValue() + panelty);
        } else {
            txtFinalAmount.setCValue(loanBalance - rebitAmount);

        }

    }

    private void setRebitAmount() {
        double rebitBalance = txtRebitAmount.getCValue();
        double intBalance = txtInterestAmount.getCValue();
        double rebit = 0.0;
        List<SettlementHistory> settlementHistorys = tableModel.getSettlementHistorys();
        if (rebitBalance > intBalance) {
            mOptionPane.showMessageDialog(
                    null, "You cannot perform this operation ! \n maximum rebate amount exceed ",
                    "Error",
                    mOptionPane.WARNING_MESSAGE);
            txtRebitAmount.setCValue(intBalance);



        } else {
            for (SettlementHistory settlementHistory : settlementHistorys) {
                if (chIsAddPanalty.isSelected()) {
                    if (settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                    } else {

                        rebit = Math.min(settlementHistory.getSettlement().getBalanceAmount(), rebitBalance);
                    }
                } else {
                    rebit = Math.min(settlementHistory.getSettlement().getBalanceAmount(), rebitBalance);

                }
                settlementHistory.setSettlementAmount(rebit);
                rebitBalance -= rebit;
            }
        }
        tableModel.fireTableModelListeners();
    }

    private void initLoanInformation() {
        //settlements
        Loan loan = (Loan) cboAgreementNo.getCValue();

        loanBalance = 0.0;
        capitalAmount = 0.0;
        panelty = 0.0;
        double penaltyAmount = 0.0, interestAmount = 0.0, advancedAmount = 0.0, advancedinterest = 0.0;
        if (loan != null) {
            List<SettlementHistory> settlements = serRebit.getSettlements(loan);
            List<AdvancedPayment> AdvancedPayment = serRebit.getAdvanced(loan.getIndexNo());
            List<Settlement> settlementscp = serRebit.getCPAmount(loan.getIndexNo());

            for (SettlementHistory settlementHistory : settlements) {
                loanBalance += settlementHistory.getSettlement().getBalanceAmount();

//                if (settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
//                    penaltyAmount += settlementHistory.getSettlement().getBalanceAmount();
//                }

                if (settlementHistory.getSettlement().getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST)) {
                    interestAmount += settlementHistory.getSettlement().getBalanceAmount();
                }
            }

            for (AdvancedPayment listAdvancedPayment : AdvancedPayment) {
                advancedAmount += listAdvancedPayment.getBalanceAmount();

                if (listAdvancedPayment.getSettlementType().equals(SystemSettlement.LOAN_INTEREST)) {
                    advancedinterest += listAdvancedPayment.getBalanceAmount();
                }

            }
            for (Settlement settlementList : settlementscp) {
                if (settlementList.getSettlementType().getCode().equals(SystemSettlement.LOAN_PANALTY)) {
                    panelty += settlementList.getBalanceAmount();
                }
                if (settlementList.getSettlementType().getCode().equals(SystemSettlement.LOAN_CAPITAL)) {
                    capitalAmount += settlementList.getBalanceAmount();
                }
            }

            tableModel.setSettlementHistorys(settlements);
        }

        //other information
        txtClient.setCValue(loan.getClient().getName());
        txtBranch.setCValue(loan.getBranch());

        txtLoanBalance.setCValue(loanBalance);
        txtPenaltyBalance.setCValue(panelty);
        txtCapitalAmount.setCValue(capitalAmount);
        txtInterestAmount.setCValue((interestAmount + advancedinterest));
        txtAdvancedAmount.setCValue(advancedAmount);

        changeRebitAmount();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        serRebit = new SERRebit(this);

        //table model
        tableModel = new SettlementHistoryTableModel();
        tblSettlements.setModel(tableModel);
        tblSettlements.setDefaultRenderer(Double.class, new CTableRenderer());

        tblSettlements.getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                changeRebitAmount();
            }
        });

        //evevnts

        cboAgreementNo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                //change selected loan
                loanChanged();
            }
        });


        txtRebitAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
//                txtRebitPresentage.setCValue(0.0);
                setRebitAmount();
            }
        });

        txtRebitPresentage.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
//                txtRebitAmount.setCValue(0.0);
                double rebt = (txtRebitPresentage.getCValue() * txtInterestAmount.getCValue()) / 100;
                txtRebitAmount.setCValue(rebt);
                setRebitAmount();
            }
        });

        chIsAddPanalty.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                setRebitAmount();
                changeRebitAmount();
            }
        });
        //reference generator
        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.REBIT));

        //actions
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnNew, "doNew");
        actionUtil.setAction(btnSave, "doSave");
        actionUtil.setAction(btnDiscard, "doDiscard");
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnPrint, "doPrint");

        setDefaultMode();

        btnNew.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACCOUNTING_NEW_ICON, 16, 16));
        btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
        btnDiscard.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_DISCARD, 16, 16));
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        btnPrint.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_PRINT_ICON, 16, 16));

        cboAgreementNo.setExpressEditable(true);

        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.REBIT_TRANSACTION_CODE);
        this.recentButton.setDatabseService(getDatabaseService());

        CTableRenderer renderer = new CTableRenderer();
        tblSettlements.setDefaultRenderer(Double.class, renderer);
        tblSettlements.setDefaultRenderer(Integer.class, renderer);
        tblSettlements.setDefaultRenderer(Date.class, renderer);
    }

    private void setNewMode() {
        //change mode
        changeMode(NEW_MODE);

        //set reference numbers
        resetValues();

    }

    private void setDefaultMode() {
        changeMode(DEFAULT_MODE);
    }

    private void resetValues() {
        txtReferenceNo.resetValue();
        txtDocumentNo.resetValue();
        txtTransactionDate.resetValue();

        cboAgreementNo.resetValue();
        txtClient.resetValue();
        txtBranch.resetValue();
        txtLoanAmount.resetValue();
        txtLoanBalance.resetValue();
        txtRebitAmount.resetValue();
        txtAfterBalance.resetValue();
        txtFinalAmount.resetValue();
        txtAdvancedAmount.resetValue();
        txtInterestAmount.resetValue();
        txtCapitalAmount.resetValue();
    }

    private RebitObject getObject() {
        RebitObject object = new RebitObject();

        object.setReferenceNo(txtReferenceNo.getCValue());
        object.setDocumentNo(txtDocumentNo.getCValue());
        object.setTransactionDate(txtTransactionDate.getCValue());
        object.setLoan((Loan) cboAgreementNo.getCValue());
        object.setNote(txtNote.getCValue());

        object.setSettlementHistorys(tableModel.getSettlementHistorys());


        return object;
    }

    private void changeMode(int mode) {
        currentMode = mode;

        switch (mode) {
            case DEFAULT_MODE:
                txtReferenceNo.setValueEditable(false);
                txtDocumentNo.setValueEditable(false);
                txtTransactionDate.setValueEditable(false);

                cboAgreementNo.setValueEditable(false);
                txtClient.setValueEditable(false);
                txtBranch.setValueEditable(false);
                txtLoanAmount.setValueEditable(false);
                txtLoanBalance.setValueEditable(false);
                txtPenaltyBalance.setValueEditable(false);
                txtRebitAmount.setValueEditable(false);
                txtFinalAmount.setValueEditable(false);
                txtAfterBalance.setValueEditable(false);
                txtNote.setValueEditable(false);
                txtAdvancedAmount.setValueEditable(false);
                txtInterestAmount.setValueEditable(false);
                txtCapitalAmount.setValueEditable(false);

                btnNew.setVisible(true);
                btnSave.setVisible(false);
                btnDiscard.setVisible(false);
                btnClose.setVisible(true);

                break;
            case NEW_MODE:
                txtReferenceNo.setValueEditable(false);
                txtDocumentNo.setValueEditable(true);
                txtTransactionDate.setValueEditable(false);

                cboAgreementNo.setValueEditable(true);
                txtClient.setValueEditable(false);
                txtBranch.setValueEditable(false);
                txtLoanAmount.setValueEditable(false);
                txtLoanBalance.setValueEditable(false);
                txtPenaltyBalance.setValueEditable(false);
                txtRebitAmount.setValueEditable(true);
                txtAfterBalance.setValueEditable(false);
                txtFinalAmount.setValueEditable(false);
                txtNote.setValueEditable(true);
                txtAdvancedAmount.setValueEditable(false);
                txtInterestAmount.setValueEditable(false);
                txtCapitalAmount.setValueEditable(false);

                btnNew.setVisible(false);
                btnSave.setVisible(true);
                btnDiscard.setVisible(true);
                btnClose.setVisible(false);
                break;
            default:
                throw new AssertionError();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cboAgreementNo = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return serRebit.getLoans();
            }
        };
        jLabel2 = new javax.swing.JLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        jLabel3 = new javax.swing.JLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        jLabel4 = new javax.swing.JLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        jLabel5 = new javax.swing.JLabel();
        txtClient = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtBranch = new com.mac.af.component.derived.input.textfield.CIStringField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtLoanBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtRebitAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel10 = new javax.swing.JLabel();
        txtAfterBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel11 = new javax.swing.JLabel();
        txtNote = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnNew = new com.mac.af.component.derived.command.button.CCButton();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        btnDiscard = new com.mac.af.component.derived.command.button.CCButton();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnPrint = new com.mac.af.component.derived.command.button.CCButton();
        jLabel12 = new javax.swing.JLabel();
        txtPenaltyBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtRebitPresentage = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel13 = new javax.swing.JLabel();
        chIsAddPanalty = new javax.swing.JCheckBox();
        jLabel14 = new javax.swing.JLabel();
        txtAdvancedAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtInterestAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel15 = new javax.swing.JLabel();
        txtCapitalAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtFinalAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSettlements = new javax.swing.JTable();

        jSplitPane1.setDividerLocation(400);

        jLabel1.setText("Agreement No.:");

        jLabel2.setText("Reference No.:");

        jLabel3.setText("Document No.:");

        jLabel4.setText("Transaction Date :");

        jLabel5.setText("Client:");

        jLabel6.setText("Branch :");

        jLabel7.setText("Loan Amount :");

        txtLoanAmount.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        txtLoanBalance.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel8.setText("Future Int Balance :");

        jLabel9.setText("Rebate Amount :");

        txtRebitAmount.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtRebitAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRebitAmountKeyReleased(evt);
            }
        });

        jLabel10.setText("After Balance :");

        txtAfterBalance.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel11.setText("Note :");

        btnNew.setText("New");

        btnSave.setText("Save");

        btnDiscard.setText("Discard");

        btnClose.setText("Close");

        btnPrint.setText("Print");

        jLabel12.setText("Penalty Balance :");

        txtPenaltyBalance.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        txtRebitPresentage.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtRebitPresentage.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRebitPresentageKeyReleased(evt);
            }
        });

        jLabel13.setText("Rebate Percentage :");

        chIsAddPanalty.setText(" panalty");

        jLabel14.setText("Advanced Amount :");

        txtAdvancedAmount.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        txtInterestAmount.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel15.setText("Interest Amount :");

        txtCapitalAmount.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jLabel16.setText("Capital  Amount :");

        jLabel17.setText("Balance To be Settle :");

        txtFinalAmount.setForeground(new java.awt.Color(255, 0, 0));
        txtFinalAmount.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtFinalAmount.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(21, 21, 21))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel13))
                                .addGap(16, 16, 16)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFinalAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtAfterBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtLoanBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtLoanAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtBranch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtClient, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboAgreementNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtPenaltyBalance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(9, 9, 9)
                                        .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnDiscard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtRebitAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                                            .addComponent(txtRebitPresentage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(chIsAddPanalty, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16))
                        .addGap(21, 21, 21)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCapitalAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtInterestAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtAdvancedAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtClient, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtBranch, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtLoanBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtPenaltyBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtAdvancedAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtInterestAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtCapitalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtRebitAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chIsAddPanalty))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtRebitPresentage, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtAfterBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txtFinalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtNote, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDiscard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel1);

        tblSettlements.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblSettlements);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane1.setRightComponent(jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 867, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtRebitPresentageKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRebitPresentageKeyReleased
        // TODO add your handling code here:
        txtRebitAmount.setCValue(0.0);
    }//GEN-LAST:event_txtRebitPresentageKeyReleased

    private void txtRebitAmountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRebitAmountKeyReleased
        // TODO add your handling code here:
        txtRebitPresentage.setCValue(0.0);
    }//GEN-LAST:event_txtRebitAmountKeyReleased
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnDiscard;
    private com.mac.af.component.derived.command.button.CCButton btnNew;
    private com.mac.af.component.derived.command.button.CCButton btnPrint;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAgreementNo;
    private javax.swing.JCheckBox chIsAddPanalty;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTable tblSettlements;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAdvancedAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAfterBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtBranch;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtCapitalAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtClient;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtFinalAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtInterestAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNote;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPenaltyBalance;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtRebitAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtRebitPresentage;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables
    private SERRebit serRebit;
    private SettlementHistoryTableModel tableModel;
    private double loanBalance, capitalAmount, panelty;
    //modes
    private int currentMode = DEFAULT_MODE;
    private static final int DEFAULT_MODE = 0;
    private static final int NEW_MODE = 1;
    private RecentButton recentButton;
}
