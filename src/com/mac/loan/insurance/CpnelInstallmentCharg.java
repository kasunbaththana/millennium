package com.mac.loan.insurance;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.loan.LoanStatus;
import com.mac.loan.insurance.zobject.*;
import com.mac.loan.zutil.calculation.LoanPaymentInformation;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.InsuranceAccountInterface;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CpnelInstallmentCharg extends CPanel {

    public CpnelInstallmentCharg() {
        initComponents();
        initOthers();
        
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        txtInstallmentAmount.setEditable(false);
        cboLoan.setExpressEditable(true);
        
         btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
        btnDiscard.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_DISCARD, 16, 16));


        //set installment Amount
        txtInstallmentCount.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                Loan loan = (Loan) cboLoan.getCValue();
                List settlementDates = getSettlementDates(loan.getIndexNo());
                if (txtInstallmentCount.getCValue() <= settlementDates.size()) {
                    txtInstallmentAmount.setCValue(
                            calculateInstallmentAmunt(
                            txtChargAmount.getCValue(),
                            txtInstallmentCount.getCValue(),
                            txtInterestRate.getCValue()));
                    doShedules();
                } else {
                    mOptionPane.showMessageDialog(null, "You Don't Have Sufficient Due Dates ! ",
                            "Error Input", mOptionPane.WARNING_MESSAGE);
                    txtInstallmentCount.setCValue(0);
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                Loan loan = (Loan) cboLoan.getCValue();
                List settlementDates = getSettlementDates(loan.getIndexNo());
                if (txtInstallmentCount.getCValue() <= settlementDates.size()) {
                    txtInstallmentAmount.setCValue(
                            calculateInstallmentAmunt(
                            txtChargAmount.getCValue(),
                            txtInstallmentCount.getCValue(),
                            txtInterestRate.getCValue()));
                    doShedules();
                } else {
                    mOptionPane.showMessageDialog(null, "You Don't Have Sufficient Due Dates ! ",
                            "Error Input", mOptionPane.WARNING_MESSAGE);
                    txtInstallmentCount.setCValue(0);
                }
            }
        });
        txtInstallmentCount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Loan loan = (Loan) cboLoan.getCValue();
                List settlementDates = getSettlementDates(loan.getIndexNo());
                if (txtInstallmentCount.getCValue() <= settlementDates.size()) {
                    txtInstallmentAmount.setCValue(
                            calculateInstallmentAmunt(
                            txtChargAmount.getCValue(),
                            txtInstallmentCount.getCValue(),
                            txtInterestRate.getCValue()));
                    doShedules();
                } else {
                    mOptionPane.showMessageDialog(null, "You Don't Have Sufficient Due Dates ! ",
                            "Error Input", mOptionPane.WARNING_MESSAGE);
                    txtInstallmentCount.setCValue(0);
                }
            }
        });

        cboLoan.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Loan loan = (Loan) cboLoan.getCValue();
                List settlementDates = getSettlementDates(loan.getIndexNo());
                //JH1015/2015/12

                txtInstallmentCount.setCValue(settlementDates.size());
            }
        });

//        ActionUtil actionUtil=new ActionUtil(this);
//        actionUtil.setAction(txtSave, "doSave");



        tblPaymentSchedule.setCModel(
                new CTableModel(new CTableColumn[]{
            new CTableColumn("Date", "dueDate"),
            new CTableColumn("Transaction Type", "transactionType"),
            new CTableColumn("Amount", "amount")
//            new CTableColumn("Payment Amount", "paymentAmount")
        }));

    }

    public List getLoans() {
        List list;
        try {
            list = getDatabaseService().getCollection("FROM com.mac.loan.insurance.zobject.Loan WHERE status='" + LoanStatus.LOAN_START + "' AND agreementNo <> null");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(CpnelInstallmentCharg.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getSettlementDates(int loanIndexNo) {
        Date sysdate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        List list;
        try {
            list = getDatabaseService().getCollection("SELECT DISTINCT  dueDate FROM com.mac.loan.insurance.zobject.Settlement WHERE  loan='" + loanIndexNo + "' and dueDate > '" + sysdate + "'");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(CpnelInstallmentCharg.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

//    @Action
    private void doSave() {
        try {
            if (txtInstallmentAmount.getCValue() > 0 && txtInstallmentCount.getCValue() > 0 && txtChargAmount.getCValue() > 0) {




                int isOk = mOptionPane.showConfirmDialog(null, "Are you sure want to save changes ?",
                        "Save", mOptionPane.YES_NO_OPTION);
                if (isOk == 0) {


                    Loan loan = (Loan) cboLoan.getCValue();

                    List<LoanPaymentInformation> informations = getSettlementShedule(
                            txtChargAmount.getCValue(),
                            txtInstallmentCount.getCValue(),
                            txtInterestRate.getCValue());
                    SystemSettlement systemSettlement = SystemSettlement.getInstance();
                    systemSettlement.beginSettlementQueue();

                    Integer transaction = SystemTransactions.insertTransaction(
                            getDatabaseService(),
                            SystemTransactions.INSURANCE_TRANSACTION_CODE,
                            ReferenceGenerator.getInstance(ReferenceGenerator.INSURANCE).getValue(),
                            loan.getLoanDocumentNo(),
                            loan.getIndexNo(),
                            null,
                            loan.getClient(),
                            txtNote.getCValue());
                    double InsuranceTotal = 0;
                    for (LoanPaymentInformation loanPaymentInformation : informations) {

                        InsuranceTotal = InsuranceTotal + loanPaymentInformation.getPaymentAmount();
                        //save Settlement  
                        systemSettlement.addSettlementQueue(
                                loanPaymentInformation.getPaymentDate(),
                                loan.getClient(),
                                loan.getIndexNo(),
                                (loanPaymentInformation.getIndex() + 1),
                                SystemTransactions.INSURANCE_TRANSACTION_CODE,
                                transaction,
                                "Insurance Amount",
                                loanPaymentInformation.getCapitalAmount(),
                                SystemSettlement.INSURANCE);

                    }


                    //Save Account Entries
                    SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                    accountTransaction.beginAccountTransactionQueue();
                    accountTransaction.addAccountTransactionQueue(
                            InsuranceAccountInterface.INSURANCE_AMOUNT_DEBIT_CODE,
                            "Insurance Amount",
                            InsuranceTotal,
                            AccountTransactionType.AUTO);
                    accountTransaction.addAccountTransactionQueue(
                            InsuranceAccountInterface.INSURANCE_AMOUNT_CREDIT_CODE,
                            "Insurance Amount",
                            InsuranceTotal,
                            AccountTransactionType.AUTO);

                    accountTransaction.flushAccountTransactionQueue(
                            getDatabaseService(),
                            transaction,
                            SystemTransactions.INSURANCE_TRANSACTION_CODE);


                    systemSettlement.flushSettlementQueue(getDatabaseService());



                    mOptionPane.showMessageDialog(null, "Successfully Saved !",
                            "Data Saving....", mOptionPane.INFORMATION_MESSAGE);

                    resetFields();

                }
            } else {
                mOptionPane.showMessageDialog(null, "One or more Required fiels(s) not found, please enter all required data. ",
                        "Data Saving....", mOptionPane.INFORMATION_MESSAGE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doShedules() {
        try {
            List<LoanPaymentInformation> informations = getSettlementShedule(
                    txtChargAmount.getCValue(),
                    txtInstallmentCount.getCValue(),
                    txtInterestRate.getCValue());

            Loan loan = (Loan) cboLoan.getCValue();
            List settlementDates = getSettlementDates(loan.getIndexNo());

            for (LoanPaymentInformation loanPaymentInformation : informations) {

                List<Settlement> settlementList = new ArrayList<>();
                for (int i = 0; i < informations.size(); i++) {
                    Settlement settlement = new Settlement();
                    settlement.setDueDate((Date) settlementDates.get(i));
                    settlement.setAmount(loanPaymentInformation.getPaymentAmount());
                    settlement.setTransactionType(SystemTransactions.INSURANCE_TRANSACTION_CODE);

                    settlementList.add(settlement);
                }
                tblPaymentSchedule.setCValue(settlementList);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cboLoan = new com.mac.af.component.derived.input.combobox.CIComboBox()
        {
            @Override
            public List getComboData(){
                return getLoans();
            }
        }
        ;
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInstallmentCount = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        lblInfo1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInterestRate = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtChargAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInstallmentAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote = new com.mac.af.component.derived.input.textfield.CIStringField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPaymentSchedule = new com.mac.af.component.derived.display.table.CDTable();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        btnDiscard = new com.mac.af.component.derived.command.button.CCButton();

        cLabel6.setText("Installment Count :");

        cLabel7.setText("Interest Rate :");

        cDLabel2.setText("Document No :");

        cDLabel3.setText("Transaction Date :");

        cLabel5.setText("Charg Amount :");

        lblInfo1.setText("ANNUAL");

        cDLabel6.setText("Loan :");

        cLabel8.setText("Installment Amount :");

        cDLabel5.setText("Note :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboLoan, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cDLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtInterestRate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblInfo1, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtInstallmentCount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtChargAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtChargAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInterestRate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblInfo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInstallmentCount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInstallmentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNote, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        tblPaymentSchedule.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblPaymentSchedule);

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnDiscard.setText("Discard");
        btnDiscard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDiscardActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(127, 127, 127)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDiscard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDiscard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(37, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:

        doSave();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnDiscardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDiscardActionPerformed
        TabFunctions.closeTab(this);
    }//GEN-LAST:event_btnDiscardActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnDiscard;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoan;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblInfo1;
    private com.mac.af.component.derived.display.table.CDTable tblPaymentSchedule;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtChargAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtInstallmentAmount;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtInstallmentCount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtInterestRate;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNote;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables

    public double calculateInstallmentAmunt(double chargAmount, int installmentCount, double interestrate) {

        double YearinterstAmount = (chargAmount * interestrate) / 100;
        double curentInterstAmount = YearinterstAmount / 12;
        double payTotalInterst = curentInterstAmount * installmentCount;
        double payTotalCapital = chargAmount;
        double installmentAmount = (payTotalCapital + payTotalInterst) / installmentCount;

        return installmentAmount;
    }

    public List<LoanPaymentInformation> getSettlementShedule(double chargAmount, int installmentCount, double interestrate) {

        double YearinterstAmount = (chargAmount * interestrate) / 100;
        double curentInterstAmount = YearinterstAmount / 12;
        double payTotalInterst = curentInterstAmount * installmentCount;
        double payTotalCapital = chargAmount;

        double capitalAmount;
        double interestAmount;
        double totalAmount;

        double installmentAmount = (payTotalCapital + payTotalInterst) / installmentCount;
        List<LoanPaymentInformation> paymentSchedule = new ArrayList<>();

        //getDue date List
        Loan loan = (Loan) cboLoan.getCValue();
        List settlementDates = getSettlementDates(loan.getIndexNo());

        for (int i = 0; i < installmentCount; i++) {
            LoanPaymentInformation information = new LoanPaymentInformation();

            capitalAmount = payTotalCapital / installmentCount;
            interestAmount = payTotalCapital * interestrate / 100;
            totalAmount = capitalAmount + curentInterstAmount;

            information.setIndex(i);
            information.setCapitalAmount(totalAmount);
            information.setPaymentAmount(totalAmount);
            information.setPaymentDate((Date) settlementDates.get(i));

            paymentSchedule.add(information);
        }
//        System.out.println("YearinterstAmount=" + YearinterstAmount);
//        System.out.println("payTotalInterst=" + payTotalInterst);
//        System.out.println("payTotalCapital=" + payTotalCapital);
//        System.out.println("installmentAmount=" + installmentAmount);


        return paymentSchedule;
    }

    private void resetFields() {
        txtDocumentNo.setCValue("");
        txtChargAmount.setCValue(0.0);
        txtInterestRate.setCValue(0.0);
        txtInstallmentCount.setCValue(0);
        txtInstallmentAmount.setCValue(0.0);
        txtNote.setCValue("");
        cboLoan.resetValue();
        tblPaymentSchedule.resetValue();
    }
}
