/*
 *  PCLoanInit.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 10:12:15 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.opening_loan.object_creator;

import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.Arrays;
import java.util.List;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.loan.zobject.Loan;

/**
 *
 * @author user
 */
public class PCLoanInit extends DefaultObjectCreator {

    /**
     * Creates new form PCLoanInit
     */
    public PCLoanInit() {
        initComponents();
        initOthers();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.OPEN_LOAN));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();

        txtTransactionDate.setText("");

        cLabel2.setText("Transaction Date :");

        cLabel3.setText("Reference No :");

        cLabel1.setText("Document No :");

        txtDocumentNo.setText(" ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(txtDocumentNo);
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
                txtTransactionDate,
                txtReferenceNo);
    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtTransactionDate, "applicationTransactionDate"),
                new CInputComponentBinder(txtReferenceNo, "applicationReferenceNo"),
                new CInputComponentBinder(txtDocumentNo, "applicationDocumentNo"));
    }

    @Override
    protected Class getObjectClass() {
        return Loan.class;
    }
}
