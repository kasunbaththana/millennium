/*
 *  SEROpeningLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.opening_loan;

import com.mac.af.core.ApplicationException;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.concurrent.CExecutable;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.loan.LoanProfile;
import com.mac.loan.LoanSituation;
import com.mac.loan.LoanStatus;
import com.mac.loan.loan.header.LoanHeaderData;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zutil.default_charges.LoanDefaultChargesInformation;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanApplicationAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAvailableCapitalAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAvailableInterstAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAvailableOtherCharglAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAvailablePanaltyAccountInterface;
import com.mac.zsystem.transaction.settlement.SystemSettlement;

/**
 *
 * @author user
 */
public class SEROpeningLoanApplication extends AbstractService {

    public SEROpeningLoanApplication(Component component) {
        super(component);
    }

    public List getRegetRecentTransactions() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status1", LoanStatus.APPLICATION_PENDING);
            params.put("status2", LoanStatus.APPLICATION_SUSPEND);
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Loan where status=:status1 or status=:status2", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(Loan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getClients() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Client where active=true");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SEROpeningLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getLoanGroups() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanGroup");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SEROpeningLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List getLoanTypes() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.LoanType");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SEROpeningLoanApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public void executeSave(final Object o, final LoanHeaderData headerData, final LoanBalancePayment balancePayment) throws DatabaseException, ApplicationException {
      if(balancePayment.getLoanAmountx()>=0){
        String hql = "FROM com.mac.loan.zobject.Loan WHERE agreementNo=:AGREEMENT_NO";
        HashMap<String, Object> existLoanParams = new HashMap<>();
        existLoanParams.put("AGREEMENT_NO", headerData.getAgreementNo());
        List<Loan> existLoans = getDatabaseService().getCollection(hql, existLoanParams);
        
        System.out.println(headerData.getAgreementNo());
        if (!existLoans.isEmpty()) {
            throw new ApplicationException("The agreement number '" + headerData.getAgreementNo() + "' is already exists."
                    + "\nPlease enter a different agreement number.");
        }

        CExecutable<Void> executable = new CExecutable<Void>("Save Loan Application - " + ((Loan) o).getApplicationReferenceNo(), getCPanel()) {
            @Override
            public Void execute() throws Exception {
                Loan loan = (Loan) o;



                List<LoanDefaultChargesInformation> defaultChargesInformations = loan.getDefaultChargesInformations();

                loan.setProfile(LoanProfile.OPENING_LOAN);
                loan.setSituation(LoanSituation.LOAN_APPLICATION);
                loan.setStatus(LoanStatus.APPLICATION_PENDING);
                loan.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));

                loan = (Loan) getDatabaseService().save(loan);
                getDatabaseService().save(loan.getDocuments());
                getDatabaseService().save(loan.getClients());

                //save transaction info
                int transaction = SystemTransactions.insertTransaction(
                        getDatabaseService(),
                        SystemTransactions.OPNING_LOAN_TRANSACTION_CODE,
                        loan.getApplicationReferenceNo(),
                        loan.getApplicationDocumentNo(),
                        loan.getIndexNo(),
                        null,
                        loan.getClient().getCode(),
                        null);

                //SETTLEMENT
                SystemSettlement systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();

                System.out.println(balancePayment.getLoanCapital() + "----------------");

                systemSettlement.addSettlementQueue(
                        loan.getApplicationTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.OPNING_LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Capital Balance",
                        balancePayment.getLoanCapital(),
                        SystemSettlement.LOAN_CAPITAL);
                systemSettlement.addSettlementQueue(
                        loan.getApplicationTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.OPNING_LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Interest Balance",
                        balancePayment.getLoanInterest(),
                        SystemSettlement.LOAN_INTEREST);
                systemSettlement.addSettlementQueue(
                        loan.getApplicationTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.OPNING_LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Panalty Balance",
                        balancePayment.getLoanPanalty(),
                        SystemSettlement.LOAN_PANALTY);
                systemSettlement.addSettlementQueue(
                        loan.getApplicationTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.OPNING_LOAN_TRANSACTION_CODE,
                        transaction,
                        "Othercharge Balance",
                        balancePayment.getOtherCharge(),
                        SystemSettlement.OTHER_CHARGE);

                for (LoanDefaultChargesInformation loanDefaultChargesInformation : defaultChargesInformations) {
                    systemSettlement.addSettlementQueue(
                            loan.getApplicationTransactionDate(),
                            loan.getClient().getCode(),
                            loan.getIndexNo(),
                            null,
                            SystemTransactions.OPNING_LOAN_TRANSACTION_CODE,
                            transaction,
                            loanDefaultChargesInformation.getChargeScheme().getName(),
                            loanDefaultChargesInformation.getChargeAmount(),
                            SystemSettlement.APPLICATION_CHARGE);
                }
                systemSettlement.flushSettlementQueue(getDatabaseService());

                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();
                
                 accountTransaction.addAccountTransactionQueue(OpningLoanAccountInterface.OPNING_LOAN_AMOUNT_CREDIT_CODE,
                        "Loan Capital Amount",balancePayment.getLoanAmountx(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(OpningLoanAccountInterface.OPNING_LOAN_AMOUNT_DEBIT_CODE, 
                        "Loan Capital Amount", balancePayment.getLoanAmountx(), AccountTransactionType.AUTO);
               
                
                //ii  avaiable capital
                accountTransaction.addAccountTransactionQueue(OpningLoanAvailableCapitalAccountInterface.OPNING_LOAN_AVAILABLE_CAPITAL_CREDIT_CODE, 
                        "Opning Loan Available Amount", balancePayment.getLoanCapital(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(OpningLoanAvailableCapitalAccountInterface.OPNING_LOAN_AVAILABLE_CAPITAL_DEBIT_CODE,
                        "Opning Loan Available Amount", balancePayment.getLoanCapital(), AccountTransactionType.AUTO);
                
                //iii avaiable interst
                accountTransaction.addAccountTransactionQueue(OpningLoanAvailableInterstAccountInterface.OPNING_LOAN_AVAILABLE_INTERST_CREDIT_CODE,
                        "Opning Loan Available Interst Amount", balancePayment.getLoanInterest(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(OpningLoanAvailableInterstAccountInterface.OPNING_LOAN_AVAILABLE_INTERST_DEBIT_CODE,
                        "Opning Loan Available Interst Amount", balancePayment.getLoanInterest(), AccountTransactionType.AUTO);
                
                //iv  panalty
                accountTransaction.addAccountTransactionQueue(OpningLoanAvailablePanaltyAccountInterface.OPNING_LOAN_AVAILABLE_PANALTY_CREDIT_CODE,
                        "Opning Loan Available Panalty Amount", balancePayment.getLoanPanalty(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(OpningLoanAvailablePanaltyAccountInterface.OPNING_LOAN_AVAILABLE_PANALTY_DEBIT_CODE,
                        "Opning Loan Available Panalty Amount",  balancePayment.getLoanPanalty(), AccountTransactionType.AUTO);
                
                
                //v   other charg
                accountTransaction.addAccountTransactionQueue(OpningLoanAvailableOtherCharglAccountInterface.OPNING_LOAN_AVAILABLE_OTHER_CHARG_CREDIT_CODE,
                        "Opning Loan Available Other Charg Amount",  balancePayment.getOtherCharge(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(OpningLoanAvailableOtherCharglAccountInterface.OPNING_LOAN_AVAILABLE_OTHER_CHARG_DEBIT_CODE,
                        "Opning Loan Available Other Charg Amount", balancePayment.getOtherCharge(), AccountTransactionType.AUTO);
                //FLUSH
                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.OPNING_LOAN_TRANSACTION_CODE);


                //LOAN
                initLoanService();
                loan.setAvailableReceipt(true);
                serLoan.startLoan(loan, headerData, balancePayment.getAvailableInstallments());


                return null;
            }
        };
        CApplication.getExecutionManager().execute(executable);
        
        
        }else{
          mOptionPane.showMessageDialog(null,
                  "<HTML> Please Calculate Loan Amount \n Press <FONT color='red'> Calculate </FONT>  Button . </HTML>",
                  "Calculation..", mOptionPane.ERROR_MESSAGE);
        }
    }

    public void executeUpdate(final Object o, final LoanBalancePayment balancePayment) throws DatabaseException {
        CExecutable<Void> executable = new CExecutable<Void>("Save Loan Application - " + ((Loan) o).getApplicationReferenceNo(), getCPanel()) {
            @Override
            public Void execute() throws Exception {
                Loan loan = (Loan) o;

                loan.setNote(null);
                loan.setSituation(LoanSituation.LOAN_APPLICATION);
                loan.setStatus(LoanStatus.APPLICATION_PENDING);
                loan.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));

                loan = (Loan) getDatabaseService().save(loan);
                getDatabaseService().save(loan.getDocuments());
                getDatabaseService().save(loan.getClients());

                //save transaction history
                Integer transaction = SystemTransactions.getTransactionIndexNo(
                        getDatabaseService(),
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        loan.getApplicationReferenceNo());
                SystemTransactions.insertTransactionHistory(
                        getDatabaseService(),
                        transaction,
                        SystemTransactions.ACTION_EDIT,
                        null);

                //SETTLEMENT
                SystemSettlement systemSettlement = SystemSettlement.getInstance();
                systemSettlement.beginSettlementQueue();

                systemSettlement.addSettlementQueue(
                        loan.getApplicationTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        transaction,
                        "Loan Capital Balance",
                        balancePayment.getLoanCapital(),
                        SystemSettlement.LOAN_CAPITAL);
                systemSettlement.addSettlementQueue(
                        loan.getApplicationTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        transaction,
                        "Loan Interest Balance",
                        balancePayment.getLoanInterest(),
                        SystemSettlement.LOAN_CAPITAL);
                systemSettlement.addSettlementQueue(
                        loan.getApplicationTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        transaction,
                        "Loan Panalty Balance",
                        balancePayment.getLoanPanalty(),
                        SystemSettlement.LOAN_PANALTY);
                systemSettlement.addSettlementQueue(
                        loan.getApplicationTransactionDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        null,
                        SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                        transaction,
                        "Othercharge Balance",
                        balancePayment.getOtherCharge(),
                        SystemSettlement.OTHER_CHARGE);


                for (LoanDefaultChargesInformation loanDefaultChargesInformation : loan.getDefaultChargesInformations()) {
                    systemSettlement.addSettlementQueue(
                            loan.getApplicationTransactionDate(),
                            loan.getClient().getCode(),
                            loan.getIndexNo(),
                            null,
                            SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                            transaction,
                            loanDefaultChargesInformation.getChargeScheme().getName(),
                            loanDefaultChargesInformation.getChargeAmount(),
                            SystemSettlement.APPLICATION_CHARGE);
                }
                systemSettlement.flushSettlementQueue(getDatabaseService());

                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_CREDIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(LoanApplicationAccountInterface.APPLICATION_AMOUNT_DEBIT_CODE, "Loan Application Amount", loan.getLoanAmount(), AccountTransactionType.AUTO);
                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE);

                return null;
            }
        };
        CApplication.getExecutionManager().execute(executable);
    }

    public void executeDelete(Object o) throws DatabaseException {
        throw new UnsupportedOperationException("Delete not supported");
    }

    private void initLoanService() {
        if (serLoan == null) {
            serLoan = new SERLoan(getCPanel());
        }
    }
    private SERLoan serLoan;
}
