/*
 *  SERLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.loan.opening_loan;

import com.mac.af.core.ApplicationException;
import com.mac.loan.loan.header.LoanHeaderData;
import com.mac.loan.zutil.calculation.LoanCalculationException;
import com.mac.loan.zutil.calculation.LoanCalculationUtil;
import com.mac.loan.zutil.calculation.LoanPaymentInformation;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.loan.LoanSituation;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAccountInterface;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author user
 */
public class SERLoan extends AbstractService {

    public SERLoan(Component component) {
        super(component);
    }

    public boolean startLoan(Loan application, LoanHeaderData headerData) {
        try {
            Loan loan = getLoan(application, headerData);

            //CHECK AGREEMENT NO DUPLICATES
            String hql = "FROM com.mac.loan.zobject.Loan WHERE agreementNo=:AGREEMENT_NO";
            HashMap<String, Object> existLoanParams = new HashMap<>();
            existLoanParams.put("AGREEMENT_NO", loan.getAgreementNo());
            List<Loan> existLoans = getDatabaseService().getCollection(hql, existLoanParams);
            if (!existLoans.isEmpty()) {
                throw new ApplicationException("The agreement number '" + loan.getAgreementNo() + "' is already exists."
                        + "\nPlease enter a different agreement number.");
            }

            //SAVE LOAN
            getDatabaseService().beginLocalTransaction();
            loan = (Loan) getDatabaseService().save(loan);
            //update transaction info
            Integer transaction = SystemTransactions.insertTransaction(
                    getDatabaseService(),
                    SystemTransactions.LOAN_TRANSACTION_CODE,
                    loan.getLoanReferenceNo(),
                    loan.getLoanDocumentNo(),
                    loan.getIndexNo(),
                    null,
                    loan.getClient().getCode(),
                    null);

            getDatabaseService().commitLocalTransaction();

            //UPDATE SETTLEMENT
            List<LoanPaymentInformation> informations = getLoanPayments(application);
            
            
            SystemSettlement systemSettlement = SystemSettlement.getInstance();
            systemSettlement.beginSettlementQueue();
            for (LoanPaymentInformation loanPaymentInformation : informations) {
                //LOAN CAPITAL
                systemSettlement.addSettlementQueue(
                        loanPaymentInformation.getPaymentDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        (loanPaymentInformation.getIndex()+1),
                        SystemTransactions.LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Capital",
                        loanPaymentInformation.getCapitalAmount(),
                        SystemSettlement.LOAN_CAPITAL);

                //LOAN INTEREST
                systemSettlement.addSettlementQueue(
                        loanPaymentInformation.getPaymentDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        (loanPaymentInformation.getIndex()+1),
                        SystemTransactions.LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Interest",
                        loanPaymentInformation.getInterestAmount(),
                        SystemSettlement.LOAN_INTEREST);
            }
            systemSettlement.flushSettlementQueue(getDatabaseService());

            //VOUCHER AMOUNT SETTLEMENT
//            systemSettlement = SystemSettlement.getInstance();
//            systemSettlement.beginSettlementQueue();
//            systemSettlement.addSettlementQueue(
//                    loan.getLoanTransactionDate(),
//                    loan.getClient().getCode(),
//                    loan.getIndexNo(),
//                    SystemTransactions.LOAN_TRANSACTION_CODE,
//                    transaction,
//                    "Loan Amount",
//                    loan.getLoanAmount(),
//                    SystemSettlement.LOAN_AMOUNT);
//            systemSettlement.flushSettlementQueue(getDatabaseService());


            //ACCOUNT TRANSACTION
            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
            accountTransaction.beginAccountTransactionQueue();
            accountTransaction.addAccountTransactionQueue(LoanAccountInterface.LOAN_AMOUNT_CREDIT_CODE, "Loan Amount - " + loan.getAgreementNo(), loan.getLoanAmount(), AccountTransactionType.AUTO);
            accountTransaction.addAccountTransactionQueue(LoanAccountInterface.LOAN_AMOUNT_DEBIT_CODE, "Loan Amount - " + loan.getAgreementNo(), loan.getLoanAmount(), AccountTransactionType.AUTO);
            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_TRANSACTION_CODE);

            mOptionPane.showMessageDialog(null, "Loan started successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (DatabaseException ex) {
            Logger.getLogger(SERLoan.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to start the loan. \nERROR: " + ex.getMessage(), TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        } catch (LoanCalculationException ex) {
            Logger.getLogger(SERLoan.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, ex.getMessage(), SERLoan.TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        } catch (ApplicationException ex) {
            mOptionPane.showMessageDialog(null, ex.getMessage(), SERLoan.TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    public boolean startLoan(Loan application, LoanHeaderData headerData, int balanceInstallmentCount) {
        try {
            Loan loan = getLoan(application, headerData);

            //CHECK AGREEMENT NO DUPLICATES
            String hql = "FROM com.mac.loan.zobject.Loan WHERE agreementNo=:AGREEMENT_NO";
            HashMap<String, Object> existLoanParams = new HashMap<>();
            existLoanParams.put("AGREEMENT_NO", loan.getAgreementNo());
            List<Loan> existLoans = getDatabaseService().getCollection(hql, existLoanParams);
            if (!existLoans.isEmpty()) {
                throw new ApplicationException("The agreement number '" + loan.getAgreementNo() + "' is already exists."
                        + "\nPlease enter a different agreement number.");
            }

            //SAVE LOAN
            getDatabaseService().beginLocalTransaction();
            loan = (Loan) getDatabaseService().save(loan);
            //update transaction info
            Integer transaction = SystemTransactions.insertTransaction(
                    getDatabaseService(),
                    SystemTransactions.LOAN_TRANSACTION_CODE,
                    loan.getLoanReferenceNo(),
                    loan.getLoanDocumentNo(),
                    loan.getIndexNo(),
                    null,
                    loan.getClient().getCode(),
                    null);

            getDatabaseService().commitLocalTransaction();
            

            //UPDATE SETTLEMENT
            List<LoanPaymentInformation> informations = getLoanPayments(application);
            
            Collections.sort(informations, new Comparator<LoanPaymentInformation>() {
                @Override
                public int compare(LoanPaymentInformation o1, LoanPaymentInformation o2) {
                    return o1.getPaymentDate().compareTo(o2.getPaymentDate());
                }
            });
            List<LoanPaymentInformation> informations1 =
                    informations.subList((loan.getInstallmentCount() - balanceInstallmentCount ), informations.size());
            //old mohan code
//                    informations.subList(0, balanceInstallmentCount);

            double loanCapitalamountx=0;

            SystemSettlement systemSettlement = SystemSettlement.getInstance();
            systemSettlement.beginSettlementQueue();
            for (LoanPaymentInformation loanPaymentInformation : informations1) {
                //LOAN CAPITAL
                loanCapitalamountx=loanCapitalamountx+loanPaymentInformation.getCapitalAmount();
                systemSettlement.addSettlementQueue(
                        loanPaymentInformation.getPaymentDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        (loanPaymentInformation.getIndex()+1),
                        SystemTransactions.LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Capital",
                        loanPaymentInformation.getCapitalAmount(),
                        SystemSettlement.LOAN_CAPITAL);

                //LOAN INTEREST
                systemSettlement.addSettlementQueue(
                        loanPaymentInformation.getPaymentDate(),
                        loan.getClient().getCode(),
                        loan.getIndexNo(),
                        (loanPaymentInformation.getIndex()+1),
                        SystemTransactions.LOAN_TRANSACTION_CODE,
                        transaction,
                        "Loan Interest",
                        loanPaymentInformation.getInterestAmount(),
                        SystemSettlement.LOAN_INTEREST);
            }
            systemSettlement.flushSettlementQueue(getDatabaseService());

            
//             //i opening loan capital only
//            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
//                accountTransaction.beginAccountTransactionQueue();
//                
//                accountTransaction.addAccountTransactionQueue(OpningLoanAccountInterface.OPNING_LOAN_AMOUNT_CREDIT_CODE,
//                        "Loan Capital Amount",loanCapitalamountx, AccountTransactionType.AUTO);
//                accountTransaction.addAccountTransactionQueue(OpningLoanAccountInterface.OPNING_LOAN_AMOUNT_DEBIT_CODE, 
//                        "Loan Capital Amount", loanCapitalamountx, AccountTransactionType.AUTO);
//                
//                 accountTransaction.flushAccountTransactionQueue(getDatabaseService(),
//                         transaction, SystemTransactions.OPNING_LOAN_TRANSACTION_CODE);
            
            
            //VOUCHER AMOUNT SETTLEMENT
//            systemSettlement = SystemSettlement.getInstance();
//            systemSettlement.beginSettlementQueue();
//            systemSettlement.addSettlementQueue(
//                    loan.getLoanTransactionDate(),
//                    loan.getClient().getCode(),
//                    loan.getIndexNo(),
//                    SystemTransactions.LOAN_TRANSACTION_CODE,
//                    transaction,
//                    "Loan Amount",
//                    loan.getLoanAmount(),
//                    SystemSettlement.LOAN_AMOUNT);
//            systemSettlement.flushSettlementQueue(getDatabaseService());



            mOptionPane.showMessageDialog(null, "Loan started successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (DatabaseException ex) {
            Logger.getLogger(SERLoan.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to start the loan. \nERROR: " + ex.getMessage(), TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        } catch (LoanCalculationException ex) {
            Logger.getLogger(SERLoan.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, ex.getMessage(), SERLoan.TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        } catch (ApplicationException ex) {
            mOptionPane.showMessageDialog(null, ex.getMessage(), SERLoan.TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    public boolean rollbackLoan(Loan loan) {
        try {
            loan.setStatus(LoanStatus.APPLICATION_PENDING);

            Integer transaction = SystemTransactions.getTransactionIndexNo(
                    getDatabaseService(),
                    SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE,
                    loan.getApplicationReferenceNo());
            SystemTransactions.insertTransactionHistory(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.ACTION_ROLLBACK,
                    null);

            getDatabaseService().save(loan);

            return true;
        } catch (DatabaseException | ApplicationException e) {
            Logger.getLogger(SERLoan.class.getName()).log(Level.SEVERE, null, e);
        }

        return false;
    }

    private Loan getLoan(Loan application, LoanHeaderData headerData) throws LoanCalculationException, DatabaseException {
        //HEADER DATA
        application.setLoanReferenceNo(headerData.getReferenceNo());
        application.setLoanDocumentNo(headerData.getDocumentNo());
        application.setLoanTransactionDate(headerData.getTransactionDate());
        application.setAgreementNo(headerData.getAgreementNo());
        application.setLoanDate(headerData.getLoanDate());
        application.setEmployeeByLoanOfficer(headerData.getEmployeeByLoanOfficer());
        application.setEmployeeByRecoveryOfficer(headerData.getEmployeeByRecoveryOfficer());

        //STATUS
        application.setAvailableVoucher(false);
        application.setAvailableReceipt(true);
        application.setStatus(LoanStatus.LOAN_START);
        application.setSituation(LoanSituation.LOAN);

        return application;
    }

    private List<LoanPaymentInformation> getLoanPayments(Loan application) throws LoanCalculationException, DatabaseException {
        List<LoanPaymentInformation> paymentInformations = LoanCalculationUtil.getPaymentSchedule(
                getDatabaseService(),
                application.getLoanAmount(),
                application.getInterestRate(),
                application.getInstallmentCount(),
                application.getPaymentTerm(),
                application.getLoanType().getInterestMethod(),
                application.getLoanDate());
        return paymentInformations;
    }
    public static final String TITLE = "Loan";
}
