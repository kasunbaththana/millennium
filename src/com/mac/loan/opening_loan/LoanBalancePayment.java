/*
 *  LoanBalancePayment.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Mar 17, 2015, 5:42:35 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.opening_loan;

/**
 *
 * @author mohan
 */
public class LoanBalancePayment {

    private int availableInstallments;
    private double loanAmountx;
    private double loanCapital;
    private double loanInterest;
    private double loanPanalty;
    private double otherCharge;

    public double getLoanAmountx() {
        return loanAmountx;
    }

    public void setLoanAmountx(double loanAmountx) {
        this.loanAmountx = loanAmountx;
    }

    public double getLoanCapital() {
        return loanCapital;
    }

    public void setLoanCapital(double loanCapital) {
        this.loanCapital = loanCapital;
    }

    public double getLoanInterest() {
        return loanInterest;
    }

    public void setLoanInterest(double loanInterest) {
        this.loanInterest = loanInterest;
    }

    public double getLoanPanalty() {
        return loanPanalty;
    }

    public void setLoanPanalty(double loanPanalty) {
        this.loanPanalty = loanPanalty;
    }

    public double getOtherCharge() {
        return otherCharge;
    }

    public void setOtherCharge(double otherCharge) {
        this.otherCharge = otherCharge;
    }

    public int getAvailableInstallments() {
        return availableInstallments;
    }

    public void setAvailableInstallments(int availableInstallments) {
        this.availableInstallments = availableInstallments;
    }
}
