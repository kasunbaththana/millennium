package com.mac.loan.opening_loan;

/*
 *  TRAOpeningLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 8:32:10 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
//package com.mac.loan.opening_loan.*;
import com.mac.af.component.base.button.action.Action;
import com.mac.loan.opening_loan.object_creator.PCLoanInit;
import java.util.List;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.dialog.object_creator_dialog.ObjectCreatorDialog;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.transaction.AbstractTransactionPanel;
import com.mac.loan.loan.SERLoan;
import com.mac.loan.loan.header.LoanHeaderData;
import com.mac.loan.loan.header.PCLoanHeaderData;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.transaction.SystemTransactions;

/**
 *
 * @author user
 */
public class TRAOpeningLoanApplication extends AbstractTransactionPanel {

    private PCOpeningLoanApplication applicationX;

    public TRAOpeningLoanApplication() {
        super();
        initOthersX();
    }

    private synchronized void initOthersX() {
        if (!init) {
            init = true;
            pcLoanApplication = new PCOpeningLoanApplication();
            pcLoanApplicationInit = new PCLoanInit();
            serLoanApplication = new SEROpeningLoanApplication(this);

            //object creator dialog
            objectCreatorDialog = new ObjectCreatorDialog();
            PCLoanHeaderData pcLoanHeaderData = new PCLoanHeaderData() {
                @Override
                protected CPanel getCPanel() {
                    return TRAOpeningLoanApplication.this;
                }
            };
            objectCreatorDialog.setObjectCreator(pcLoanHeaderData);
            objectCreatorDialog.setTitle(SERLoan.TITLE);

            this.recentButton = new RecentButton();
            this.recentButton.setTransactionType(SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE);
            this.recentButton.setDatabseService(getDatabaseService());

        }
    }

    @Override
    protected synchronized AbstractObjectCreator getInitDialogObjectCreator() {
        initOthersX();
        return pcLoanApplicationInit;
    }

    @Override
    protected AbstractObjectCreator getInitDialogExternalObjectCreator() {
        initOthersX();
        return pcLoanApplicationInit;
    }

    @Override
    protected List getRecentTransactions() {
        initOthersX();
        return serLoanApplication.getRegetRecentTransactions();
    }

    @Override
    protected CTableModel getInitDialogTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Index No", "indexNo"),
            new CTableColumn("Reference No", "applicationReferenceNo"),
            new CTableColumn("Transaction Date", "applicationTransactionDate"),
            new CTableColumn("Client", new String[]{"client", "name"}),
            new CTableColumn("Loan Type", "loanType", "name"),
            new CTableColumn("Status", new String[]{"status"}),
            new CTableColumn("Note", new String[]{"note"})
        });
    }

    @Override
    protected void executeSave(Object o) throws Exception {
        initOthersX();
        LoanHeaderData headerData = objectCreatorDialog.getNewObject();

//        try {
        serLoanApplication.executeSave(o, headerData, applicationX.getLoanBalancePayment());
//        } catch (ApplicationException applicationException) {
//            mOptionPane.showMessageDialog(null, applicationException.getMessage(), "Save failed", mOptionPane.ERROR_MESSAGE);
//        }
    }

    @Override
    protected void executeUpdate(Object o) throws Exception {
        initOthersX();
//        serLoanApplication.executeUpdate(o);
        LoanHeaderData headerData = objectCreatorDialog.getNewObject();
        serLoanApplication.executeSave(o, headerData, pcLoanApplication.getLoanBalancePayment());
    }

    @Override
    protected void executeDelete(Object o) throws Exception {
        initOthersX();
        serLoanApplication.executeDelete(o);
    }

    @Override
    protected Object getObjectFromInitDataForNew(Object o) {
        return o;
    }

    @Override
    protected Object getObjectFromInitDataForEdit(Object o) {
        return o;
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        initOthersX();
        applicationX = pcLoanApplication;
        return pcLoanApplication;
    }

    @Override
    public int[] getDialogModes() {
        return new int[]{
            NEW_MOOD,
            EDIT_MOOD
        };
    }

    @Override
    protected String getTitle() {
        return "Loan Application";
    }

    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
    }
    private PCLoanInit pcLoanApplicationInit;
    private PCLoanInit pcLoanApplicationInitExternal;
    private PCOpeningLoanApplication pcLoanApplication;
    private SEROpeningLoanApplication serLoanApplication;
    private boolean init = false;
    private ObjectCreatorDialog<LoanHeaderData> objectCreatorDialog;
    private RecentButton recentButton;
}
