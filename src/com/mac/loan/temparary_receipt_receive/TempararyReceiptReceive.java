/*
 *  TempararyReceiptReceive.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 16, 2014, 8:33:00 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.temparary_receipt_receive;

import com.mac.af.component.derived.input.combobox.CIComboBox;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.loan.TemperaryReceiptStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.TemperaryReceipt;
import com.mac.loan.ztemplate.receipt.AbstractReceipt;
import com.mac.loan.ztemplate.receipt.PCReceipt;
import com.mac.loan.ztemplate.receipt.SERAbstractReceipt;
import com.mac.loan.ztemplate.receipt.custom_object.Receipt;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class TempararyReceiptReceive extends AbstractReceipt {

    private TemperaryReceipt temperaryReceipt;

    @Override
    public List getLoans() {
        String hql = "FROM com.mac.loan.zobject.TemperaryReceipt WHERE status=:STATUS";
        HashMap<String, Object> params = new HashMap<>();
        params.put("STATUS", TemperaryReceiptStatus.REALIZE);

        List<Loan> loans;
        try {
            loans = getDatabaseService().getCollection(hql, params);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }

    @Override
    protected String getReferenceGeneratorType() {
        return ReferenceGenerator.RECEIPT;
    }

    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.CHEQUE_RECEIPT_TRANSACTION_CODE;
    }

    @Override
    protected String getSettlementCreitOrDebit() {
        return SettlementCreditDebit.CREDIT;
    }

    @Override
    protected String getChequeType() {
        return ChequeType.CLIENT;
    }

    //CUSTOMIZED
    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCReceipt(getService()) {
            @Override
            protected String getReferenceGeneratorType() {
                return TempararyReceiptReceive.this.getReferenceGeneratorType();
            }

            @Override
            public boolean isPaymentAmountFixed() {
                return true;
            }

            @Override
            public Loan getSelectedLoan(CIComboBox comboBox) {
                temperaryReceipt = (TemperaryReceipt) comboBox.getCValue();

                return temperaryReceipt.getLoan();
            }

            @Override
            public Double getDefaultPaymentAmount(CIComboBox comboBox) {
                temperaryReceipt = (TemperaryReceipt) comboBox.getCValue();
                return temperaryReceipt.getAmount();
            }
        };
    }

    @Override
    protected int saveReceipt(String transactionTypeCode, Receipt receipt) throws DatabaseException {

        //SAVE TRANSACTION
        int transactionIndex = getService().saveTransaction(transactionTypeCode, receipt);

        //SAVE SETTLEMENT
        getService().saveSettlement(transactionTypeCode, receipt, transactionIndex);

        //SAVE OVER AMOUNT
        getService().saveOverAmount(receipt, transactionTypeCode, transactionIndex);

        //list amount
         getService().ListSettlement(transactionTypeCode, receipt, transactionIndex);
        
//        getService().ListSettlement(transactionTypeCode, receipt, transactionIndex);
        
        
        //ACCOUNT TRANSACTION
        getService().saveChequeReceiptIssueAccount(transactionIndex, transactionTypeCode, receipt);

      // save cheque datails

           

        //SAVE LOAN MODIFICATION
        getService().saveLoan(receipt.getLoan());

        //SAVE TEMPERARY RECEIPT
//        temperaryReceipt.setStatus(TemperaryReceiptStatus.SETTLED);
//        getDatabaseService().save(temperaryReceipt);
        
        getService().save_status(temperaryReceipt);
        

        return AbstractRegistrationForm.SAVE_SUCCESS;

    }

    @Override
    protected String getAccountSettingCode() {
        return null;
    }

    @Override
    protected String getTransactionName() {
        return "Temperary Receipt Receive";
    }

    @Override
    protected String getOverAmountSettlementType() {
        return SystemSettlement.OVER_PAY_RECEIPT;
    }

    @Override
    protected boolean isOverPayAvailable() {
        return true;
    }

    @Override
    protected Loan getModifiedLoan(Loan loan, Double afterBalance) {
        if (afterBalance.doubleValue() <= 0) {
            loan.setAvailableReceipt(false);
        } else {
            loan.setAvailableReceipt(true);
        }

        return loan;
    }
}
