/*
 *  LoanStatus.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jun 26, 2014, 4:00:00 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan;

/**
 *
 * @author mohan
 */
public class LoanStatus {

    //APPLICATION SITUATION
    public static final String APPLICATION_PENDING = "APPLICATION_PENDING";
    public static final String APPLICATION_PENDING_QL = "APPLICATION_PENDING_QL";
    public static final String APPLICATION_ACCEPT = "APPLICATION_ACCEPT";
    public static final String APPLICATION_REJECT = "APPLICATION_REJECT";
    public static final String APPLICATION_SUSPEND = "APPLICATION_SUSPEND";
    //LOAN STATUS
    public static final String LOAN_PENDING = "LOAN_PENDING";
    public static final String LOAN_REJECT = "LOAN_REJECT";
    
    public static final String LOAN_START = "LOAN_START";
    public static final String LOAN_DISBURSEMENT = "LOAN_DISBURSEMENT";
    public static final String SUSPEND = "SUSPEND";
    
    //LOAN SITUATION
    public static final String COLLECTED = "COLLECTED";
    public static final String COMPLETE = "COMPLETE";
    public static final String CANCEL = "CANCEL";
    
      //THIS CREATE FOR REPORT PURPOSE
    public static final String [] ALL_LOAN_STATUS= {
                    LOAN_START,
                    COLLECTED,
                    CANCEL,
                    LOAN_PENDING,
                    SUSPEND,
                    APPLICATION_PENDING,
                    APPLICATION_ACCEPT
                    };
}
