/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.loan_close;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.loan.LoanStatus;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.ReasonSetup;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanCloseAccountInterface;
import static com.mac.zsystem.transaction.account.account_setting.system_interface.LoanCloseAccountInterface.FUND_SETTLEMENT_CREDIT_CODE;
import com.mac.zsystem.transaction.account.object.Account;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author thilanga
 */
public class SERLoanClose extends AbstractService {

//    public static String LOAN_CLOSE = "LOAN CLOSE";
//    public static String LOAN_CANCEL = "LOAN CANCEL";
    public static String LOAN_CLOSE_NAME = "Loan Close";
    //
    private Loan loan;
    private REGLoanClose cPanel;
    Date systemdate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);

    public SERLoanClose(Component component) {
        super(component);
    }

    private void initCPanel() {
        if (cPanel == null) {
            cPanel = (REGLoanClose) getCPanel();
        }
    }

    public List<Settlement> getSettlements() throws DatabaseException {
        if (loan != null) {

            String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan=:LOANX AND settlementType<>'LOAN_AMOUNT' and status<>'CANCEL' ";

            HashMap<String, Object> params = new HashMap();
            params.put("LOANX", loan == null ? 0 : loan.getIndexNo());

            return getDatabaseService().getCollection(hql, params);
        } else {
            return Collections.emptyList();
        }
    }

    
    public List<Loan> getLoans() {
        List<Loan> loan;
        try {
            String hql = "FROM com.mac.loan.zobject.Loan WHERE status='LOAN_START' OR status='LOAN_SUSPEND' ";
            loan = getDatabaseService().getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }

    public List<ReasonSetup> getReason() {
        List<ReasonSetup> reasonSetup;
        try {
            String hql = "FROM com.mac.loan.zobject.ReasonSetup ";
            reasonSetup = getDatabaseService().getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            reasonSetup = new ArrayList();
        }
        return reasonSetup;
    }

    public List<Settlement> getSettlement(Loan loan) throws DatabaseException {
        if (loan != null) {

            String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan=:LOANX AND settlementType<>'LOAN_AMOUNT' and settlementType<>'OVER_PAY_RECEIPT' and status<>'CANCEL' and dueDate >=:DUEDATE ";

            HashMap<String, Object> params = new HashMap();
            params.put("LOANX", loan == null ? 0 : loan.getIndexNo());
            params.put("DUEDATE", systemdate);
            System.out.println("" + hql);
            return getDatabaseService().getCollection(hql, params);
        } else {
            return Collections.emptyList();
        }
    }

    public List<Account> getAccounts() {
        List<Account> loan;
        try {
            loan = getDatabaseService().getCollection(Account.class);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }

    /**
     * Consider following transactions Loan Application/ Loan/ Receipts/ Voucher
     */
    public void closeLoanErrorClosing(Loan loan) throws DatabaseException {
        SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE,
                loan.getLoanReferenceNo(),
                loan.getLoanDocumentNo(),
                loan.getIndexNo(),
                null,
                loan.getClient().getCode(),
                "Loan Close");

        String call = "CALL z_loan_cancel(" + loan.getIndexNo() + ")";
        getDatabaseService().callUpdateProcedure(call);
    }

    public void closeSettlementClosing(Loan loan) throws DatabaseException {
        int transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE,
                loan.getLoanReferenceNo(),
                loan.getLoanDocumentNo(),
                loan.getIndexNo(),
                null,
                loan.getClient().getCode(),
                "Loan Settlement Close");

        loan.setStatus(LoanStatus.COLLECTED);
        loan.setAvailableReceipt(false);
        loan.setAvailableVoucher(false);

        //settle settlements
        List<Settlement> settlements = getDatabaseService().initCriteria(Settlement.class)
                .add(Restrictions.eq("loan", loan.getIndexNo()))
                .add(Restrictions.eq("status", SettlementStatus.SETTLEMENT_PENDING))
                .list();


        List<Settlement> settlementssSET = getSettlement(loan);

        double creditSum = 0.0;
        double debitSum = 0.0, varCapital = 0.0, varInterest = 0.0, varInsuarance = 0.0, varTotalDue = 0.0;


        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (Settlement settlement : settlements) {
            if (settlement.getSettlementType().getCreditOrDebit().equals(SettlementCreditDebit.CREDIT)) {
                creditSum += settlement.getBalanceAmount();
                systemSettlement.addSettlementHistoryQueue(settlement, SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE, transaction, settlement.getBalanceAmount());
            }
            if (settlement.getSettlementType().getCreditOrDebit().equals(SettlementCreditDebit.DEBIT)) {
                debitSum += settlement.getBalanceAmount();
                systemSettlement.addSettlementHistoryQueue(settlement, SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE, transaction, settlement.getBalanceAmount());
            }
        }
        for (Settlement settlementLst : settlementssSET) {

            if (settlementLst.getSettlementType().getCode().equals(SystemSettlement.LOAN_CAPITAL)) {
                varCapital += settlementLst.getAmount();
            }
            if (settlementLst.getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST)) {
                varInterest += settlementLst.getAmount();
            }
            if (settlementLst.getSettlementType().getCode().equals(SystemSettlement.INSURANCE)) {
                varInsuarance += settlementLst.getAmount();
            }


        }
        varTotalDue += varCapital + varInterest + varInsuarance;
        if (mOptionPane.showConfirmDialog(null, "Are you sure want to close the loan ?\n Total balance  "+varTotalDue+" ", SERLoanClose.LOAN_CLOSE_NAME, mOptionPane.YES_NO_OPTION, mOptionPane.WARNING_MESSAGE) == mOptionPane.YES_OPTION) 
            {

        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());

        //accounts
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();

        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.CREDIT_SETTLEMENT_CREDIT_CODE, "Loan Settlement Close", creditSum, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.CREDIT_SETTLEMENT_DEBIT_CODE, "Loan Settlement Close", creditSum, AccountTransactionType.AUTO);

        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.DEBIT_SETTLEMENT_CREDIT_CODE, "Loan Settlement Close", debitSum, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.DEBIT_SETTLEMENT_DEBIT_CODE, "Loan Settlement Close", debitSum, AccountTransactionType.AUTO);

        //Due customer
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.SETTLEMENT_DUE_DEBIT_CODE, "Loan Settlement Close Due Amount", varTotalDue, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.CAPITAL_DUE_CREDIT_CODE, "Loan Settlement Close Due Amount", varCapital, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.INTEREST_DUE_CREDIT_CODE, "Loan Settlement Close Due Amount", varInterest, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.INSUARANCE_DUE_CREDIT_CODE, "Loan Settlement Close Due Amount", varInsuarance, AccountTransactionType.AUTO);

        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE);

        getDatabaseService().save(loan);
            }
        

    }
    public void closeSettlementClosingByFund(Loan loan) throws DatabaseException {
        int transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE,
                loan.getLoanReferenceNo(),
                loan.getLoanDocumentNo(),
                loan.getIndexNo(),
                null,
                loan.getClient().getCode(),
                "Loan Settlement By Fund");

        loan.setStatus(LoanStatus.COLLECTED);
        loan.setAvailableReceipt(false);
        loan.setAvailableVoucher(false);

        //settle settlements
        List<Settlement> settlements = getDatabaseService().initCriteria(Settlement.class)
                .add(Restrictions.eq("loan", loan.getIndexNo()))
                .add(Restrictions.eq("status", SettlementStatus.SETTLEMENT_PENDING))
                .list();


        List<Settlement> settlementssSET = getSettlement(loan);

        double creditSum = 0.0;
        double debitSum = 0.0, varCapital = 0.0, varInterest = 0.0, varInsuarance = 0.0, varTotalDue = 0.0;


        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (Settlement settlement : settlements) {
            if (settlement.getSettlementType().getCreditOrDebit().equals(SettlementCreditDebit.CREDIT)) {
                creditSum += settlement.getBalanceAmount();
                systemSettlement.addSettlementHistoryQueue(settlement, SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE, transaction, settlement.getBalanceAmount());
            }
            if (settlement.getSettlementType().getCreditOrDebit().equals(SettlementCreditDebit.DEBIT)) {
                debitSum += settlement.getBalanceAmount();
                systemSettlement.addSettlementHistoryQueue(settlement, SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE, transaction, settlement.getBalanceAmount());
            }
        }
        for (Settlement settlementLst : settlementssSET) {

            if (settlementLst.getSettlementType().getCode().equals(SystemSettlement.LOAN_CAPITAL)) {
                varCapital += settlementLst.getAmount();
            }
            if (settlementLst.getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST)) {
                varInterest += settlementLst.getAmount();
            }
            if (settlementLst.getSettlementType().getCode().equals(SystemSettlement.INSURANCE)) {
                varInsuarance += settlementLst.getAmount();
            }


        }
        varTotalDue += varCapital + varInterest + varInsuarance;
        if (mOptionPane.showConfirmDialog(null, "Are you sure want to close the loan ?\n Total balance  "+varTotalDue+" ", SERLoanClose.LOAN_CLOSE_NAME, mOptionPane.YES_NO_OPTION, mOptionPane.WARNING_MESSAGE) == mOptionPane.YES_OPTION) 
            {

        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());

        //accounts
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();

        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.CREDIT_SETTLEMENT_CREDIT_CODE, "Loan Settlement Close", creditSum, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.CREDIT_SETTLEMENT_DEBIT_CODE, "Loan Settlement Close", creditSum, AccountTransactionType.AUTO);

        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.DEBIT_SETTLEMENT_CREDIT_CODE, "Loan Settlement Close", debitSum, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.DEBIT_SETTLEMENT_DEBIT_CODE, "Loan Settlement Close", debitSum, AccountTransactionType.AUTO);

        //Due customer
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.SETTLEMENT_DUE_DEBIT_CODE, "Loan Settlement Close Due Amount", varTotalDue, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.CAPITAL_DUE_CREDIT_CODE, "Loan Settlement Close Due Amount", varCapital, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.INTEREST_DUE_CREDIT_CODE, "Loan Settlement Close Due Amount", varInterest, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.INSUARANCE_DUE_CREDIT_CODE, "Loan Settlement Close Due Amount", varInsuarance, AccountTransactionType.AUTO);
        
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.FUND_SETTLEMENT_CREDIT_CODE, "Loan Settlement Fund Amount", creditSum, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.FUND_SETTLEMENT_DEBIT_CODE, "Loan Settlement Fund Amount", creditSum, AccountTransactionType.AUTO);
//add banck account
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE);

        getDatabaseService().save(loan);
            }
        

    }

    public void ReasonClose(Loan loan, ReasonSetup note) {
        try {
//        getDatabaseService().beginLocalTransaction();
            int transaction = SystemTransactions.insertTransaction(
                    getDatabaseService(),
                    SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE,
                    loan.getLoanReferenceNo(),
                    loan.getLoanDocumentNo(),
                    loan.getIndexNo(),
                    null,
                    loan.getClient().getCode(),
                    "Loan Suspend Close");
            loan.setNote(note.getDescription());
            loan.setStatus(LoanStatus.SUSPEND);
            loan.setAvailableReceipt(false);
           
            getDatabaseService().save(loan);
//        getDatabaseService().commitLocalTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeResheduleClosing(Loan loan, Double totalBalance, String creditAccount, String debitAccount) throws DatabaseException {
        int transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE,
                loan.getLoanReferenceNo(),
                loan.getLoanDocumentNo(),
                loan.getIndexNo(),
                null,
                loan.getClient().getCode(),
                "Loan Settlement Close");

        loan.setStatus(LoanStatus.COLLECTED);
        loan.setAvailableReceipt(false);
        loan.setAvailableVoucher(false);

        //settle settlements
        List<Settlement> settlements = getDatabaseService().initCriteria(Settlement.class)
                .add(Restrictions.eq("loan", loan.getIndexNo()))
                .add(Restrictions.eq("status", SettlementStatus.SETTLEMENT_PENDING))
                .list();

        double creditSum = 0.0;
        double debitSum = 0.0;
        double capitalSum = 0.0;
        double capitaldueSum = 0.0;
        double interestSum = 0.0;
        double othercrgSum = 0.0;
        double totalSum = 0.0;

//        for (Settlement settlement : settlements) {
//        
//            if(settlement.getSettlementType().getCode().equals("LOAN_CAPITAL")) 
//               {
//                   
//                capitaldueSum += settlement.getBalanceAmount();
//                }      
//            
//        }


        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (Settlement settlement : settlements) {
            if (settlement.getSettlementType().getCreditOrDebit().equals(SettlementCreditDebit.CREDIT)) {


                if (settlement.getSettlementType().getCode().equals("LOAN_CAPITAL")) {
                    capitalSum += settlement.getBalanceAmount();
                } else if (settlement.getSettlementType().getCode().equals("LOAN_INTEREST")) {
                    interestSum += settlement.getBalanceAmount();
                } else if (settlement.getSettlementType().getCode().equals("OTHER_CHARGE"))//OTHER_CHARGE
                {
                    othercrgSum += settlement.getBalanceAmount();
                }
                totalSum += capitalSum + interestSum + othercrgSum;
                creditSum += settlement.getBalanceAmount();
                systemSettlement.addSettlementHistoryQueue(settlement, SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE, transaction, settlement.getBalanceAmount());
            }
            if (settlement.getSettlementType().getCreditOrDebit().equals(SettlementCreditDebit.DEBIT)) {
                debitSum += settlement.getBalanceAmount();
                systemSettlement.addSettlementHistoryQueue(settlement, SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE, transaction, settlement.getBalanceAmount());
            }
        }
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());

        //accounts
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();

        if (creditAccount != null && debitAccount != null) {
            accountTransaction.saveAccountTransaction(getDatabaseService(),
                    transaction,
                    SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE,
                    AccountTransactionType.AUTO,
                    AccountSettingCreditOrDebit.CREDIT,
                    capitalSum,
                    creditAccount,
                    "Loan reschedule balance",
                    loan.getIndexNo());

            accountTransaction.saveAccountTransaction(getDatabaseService(),
                    transaction,
                    SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE,
                    AccountTransactionType.AUTO,
                    AccountSettingCreditOrDebit.DEBIT,
                    capitalSum,
                    debitAccount,
                    "Loan reschedule balance",
                    loan.getIndexNo());
        }
//due accounts





//        accountTransaction.beginAccountTransactionQueue();
//        
//        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.CREDIT_SETTLEMENT_CREDIT_CODE, "Loan Settlement Close", creditSum, AccountTransactionType.AUTO);
//        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.CREDIT_SETTLEMENT_DEBIT_CODE, "Loan Settlement Close", creditSum, AccountTransactionType.AUTO);
//        
//        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.DEBIT_SETTLEMENT_CREDIT_CODE, "Loan Settlement Close", debitSum, AccountTransactionType.AUTO);
//        accountTransaction.addAccountTransactionQueue(LoanCloseAccountInterface.DEBIT_SETTLEMENT_DEBIT_CODE, "Loan Settlement Close", debitSum, AccountTransactionType.AUTO);
//        
//        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE);

        getDatabaseService().save(loan);
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;

//        initCPanel();
//        cPanel.refreshTable();
    }
}
