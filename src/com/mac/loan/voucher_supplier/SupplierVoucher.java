/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.voucher_supplier;

import com.mac.loan.zobject.Client;
import com.mac.loan.zobject.Loan;
import com.mac.loan.ztemplate.supplier_voucher.AbstractReceipt;
import com.mac.loan.ztemplate.supplier_voucher.SERAbstractReceipt;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SupplierPaymentAccountInterface;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NIMESH-PC
 */
public class SupplierVoucher extends AbstractReceipt {

    
    @Override
    public List<Client> getSupplier() {
        String hql = "FROM com.mac.loan.zobject.Client WHERE supplier=true";
        List<Client> clients;
        try {
            clients = getDatabaseService().getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            clients = new ArrayList<>();
        }

        return clients;
    }

    @Override
    protected String getReferenceGeneratorType() {
        return ReferenceGenerator.DELIVERYORDER;
    }

    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.SUPPLIER_PAYMENT;
    }

    @Override
    protected String getSettlementCreitOrDebit() {
        return SettlementCreditDebit.DEBIT;
    }

    @Override
    protected String getChequeType() {
        return ChequeType.COMPANY;
    }

    @Override
    protected String getAccountSettingCode() {
        return SupplierPaymentAccountInterface.SUPPLIER_PAYMENT_DEBIT_CODE;
    }

    @Override
    protected String getTransactionName() {
        return "Delivery Order";
    }

    @Override
    protected Loan getModifiedLoan(Loan loan, Double afterBalance) {
        if (afterBalance.doubleValue() <= 0.0) {
            loan.setAvailableVoucher(false);
        } else {
            loan.setAvailableVoucher(true);
        }

        return loan;
    }

    @Override
    protected String getOverAmountSettlementType() {
        return SystemSettlement.OVER_PAY_VOUCHER;
    }

    @Override
    protected boolean isOverPayAvailable() {
        return false;
    } 

    
}
