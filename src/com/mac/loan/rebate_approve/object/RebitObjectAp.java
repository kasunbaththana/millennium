/*
 *  RebitObject.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 15, 2015, 12:03:14 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.loan.rebate_approve.object;

import com.mac.loan.zobject.Loan;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mohan
 */
public class RebitObjectAp {

    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private Loan loan;
    private String note;
    private List<SettlementHistory> settlementHistorys;

    public RebitObjectAp() {
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<SettlementHistory> getSettlementHistorys() {
        return settlementHistorys;
    }

    public void setSettlementHistorys(List<SettlementHistory> settlementHistorys) {
        this.settlementHistorys = settlementHistorys;
    }
}
