/*
 *  PCLoan.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 13, 2014, 9:52:17 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.rebate_approve;

import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.zobject.Loan;
import java.util.List;

/**
 *
 * @author mohan
 */
public class PCLoanAp extends AbstractObjectCreator<Loan> {

    public PCLoanAp(SERRebitAp serRebit) {
        this.serRebit = serRebit;

        initComponents();
        initOthers();
    }

    public List getLoans() {
        return serRebit.getLoans();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        cboAgreementNo.setExpressEditable(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cboAgreementNo = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoans();
            }
        };

        cDLabel1.setText("Agreement No.:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboAgreementNo, javax.swing.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAgreementNo;
    // End of variables declaration//GEN-END:variables
    private Loan loan;
    private SERRebitAp serRebit;

    @Override
    public void setNewMood() {
        cboAgreementNo.setValueEditable(true);
    }

    @Override
    public void setEditMood() {
        cboAgreementNo.setValueEditable(true);
    }

    @Override
    public void setIdleMood() {
        cboAgreementNo.setValueEditable(false);
    }

    @Override
    public void resetFields() {
        cboAgreementNo.resetValue();
    }

    @Override
    protected void setValueAbstract(Loan value) throws ObjectCreatorException {
        this.loan = value;
    }

    @Override
    protected Loan getValueAbstract() throws ObjectCreatorException {
        return loan;
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        //NOTHING TO TO
    }

    @Override
    protected void initObject() throws ObjectCreatorException {
        this.loan = (Loan) cboAgreementNo.getCValue();
    }
}
