/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.write_off;


import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import java.util.Collection;

/**
 *
 * @author kasun
 */
public class REGWriteOff extends AbstractGridObject {

    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Due Date", "dueDate"),
            new CTableColumn("Description", "description"),
            new CTableColumn("Amount", "amount"),
            new CTableColumn("Balance Amount", "balanceAmount"),
            new CTableColumn("Status", "status")
        });
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        initService();

        return new PCWriteOff(this,serLoanClose);
    }

    @Override
    protected Collection getTableData() {
        initService();
//        try {
//            this.settlements = serLoanClose.getSettlements();
            return settlements;
//        } catch (DatabaseException ex) {
//            Logger.getLogger(REGLoanClose.class.getName()).log(Level.SEVERE, null, ex);
//            return Collections.emptyList();
//        }
    }

    private void initService() {
        if (serLoanClose == null) {
            serLoanClose = new SERWriteOff(this);
        }
    }

    public Collection getSettlements() {
        return settlements;
    }

    public void setSettlements(Collection settlements) {
        this.settlements = settlements;
    }
    
    
    private SERWriteOff serLoanClose;
    private Collection settlements;
}
