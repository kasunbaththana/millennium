/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.loan.write_off;


import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.zobject.Loan;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanCloseAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.WriteOffAccountInterface;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author kasun
 */
public class SERWriteOff extends AbstractService {

//    public static String LOAN_CLOSE = "LOAN CLOSE";
//    public static String LOAN_CANCEL = "LOAN CANCEL";
    public static String LOAN_CLOSE_NAME = "Loan Close";
    //
    private Loan loan;
    private REGWriteOff cPanel;
    Date systemdate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);

    public SERWriteOff(Component component) {
        super(component);
    }

    private void initCPanel() {
        if (cPanel == null) {
            cPanel = (REGWriteOff) getCPanel();
        }
    }

    public List<Settlement> getSettlements() throws DatabaseException {
        if (loan != null) {

            String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan=:LOANX AND settlementType<>'LOAN_AMOUNT' and status<>'CANCEL' ";

            HashMap<String, Object> params = new HashMap();
            params.put("LOANX", loan == null ? 0 : loan.getIndexNo());

            return getDatabaseService().getCollection(hql, params);
        } else {
            return Collections.emptyList();
        }
    }

    
    public List<Loan> getLoans() {
        List<Loan> loan;
        try {
            String hql = "FROM com.mac.loan.zobject.Loan WHERE writeOff='false' and status='LOAN_START' OR status='LOAN_SUSPEND'  ";
            loan = getDatabaseService().getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }



    public List<Settlement> getSettlement(Loan loan) throws DatabaseException {
        if (loan != null) {

            String hql = "FROM com.mac.zsystem.transaction.settlement.object.Settlement WHERE loan=:LOANX AND settlementType<>'LOAN_AMOUNT' and settlementType<>'OVER_PAY_RECEIPT' and status<>'CANCEL' and dueDate >=:DUEDATE ";

            HashMap<String, Object> params = new HashMap();
            params.put("LOANX", loan == null ? 0 : loan.getIndexNo());
            params.put("DUEDATE", systemdate);
            System.out.println("" + hql);
            return getDatabaseService().getCollection(hql, params);
        } else {
            return Collections.emptyList();
        }
    }



    public void ReasonClose(Loan loan, String note) {
        try {
//        getDatabaseService().beginLocalTransaction();
            int transaction = SystemTransactions.insertTransaction(
                    getDatabaseService(),
                    SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE,
                    loan.getLoanReferenceNo(),
                    loan.getLoanDocumentNo(),
                    loan.getIndexNo(),
                    null,
                    loan.getClient().getCode(),
                    note);
            loan.setNote(note);
            loan.setWriteOff(true);
           accountTransaction(transaction);
            getDatabaseService().save(loan);
//        getDatabaseService().commitLocalTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void accountTransaction(int transaction){
        
        try {
            double creditSum = 0.0,debitSum=0.0;
             //settle settlements
        List<Settlement> settlements = getDatabaseService().initCriteria(Settlement.class)
                .add(Restrictions.eq("loan", loan.getIndexNo()))
                .add(Restrictions.eq("status", SettlementStatus.SETTLEMENT_PENDING))
                .list();
           for (Settlement settlement : settlements) {
            if (settlement.getSettlementType().getCreditOrDebit().equals(SettlementCreditDebit.CREDIT)) {
                creditSum += settlement.getBalanceAmount();
               
            }
            
        }

        //accounts
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();

        accountTransaction.addAccountTransactionQueue(WriteOffAccountInterface.WRITE_OFF_CREDIT_CODE, "Loan Write Off", creditSum, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(WriteOffAccountInterface.WRITE_OFF_DEBIT_CODE, "Loan Write Off", creditSum, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(WriteOffAccountInterface.WRITE_OFF_PROVISION, "Loan Write Off", 0.0, AccountTransactionType.AUTO);

      
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.WRITE_OFF_TRANSACTION_CODE);

        } catch (Exception e) {
        }
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;

    }
}
