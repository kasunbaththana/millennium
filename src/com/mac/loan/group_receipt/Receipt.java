/*
 *  Receipt.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 12, 2014, 4:11:48 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.loan.group_receipt;

import com.mac.af.core.database.DatabaseException;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanGroup;
import com.mac.loan.ztemplate.group_receipt.AbstractReceipt;
import com.mac.loan.ztemplate.group_receipt.SERAbstractReceipt;
import com.mac.zsystem.transaction.account.account_setting.system_interface.GroupReceiptAccountInterface;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author mohan
 */
public class Receipt extends AbstractReceipt {

    @Override
    public List<LoanGroup> getLoanGroups() {
       String hql = "FROM com.mac.loan.zobject.LoanGroup";
        List<LoanGroup> loans;
        try {
            loans = getDatabaseService().getCollection(hql);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAbstractReceipt.class.getName()).log(Level.SEVERE, null, ex);
            loans = new ArrayList<>();
        }

        return loans;
    }

    @Override
    protected String getReferenceGeneratorType() {
        return ReferenceGenerator.GROUP_RECEIPT;
    }

    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.GROUP_RECEIPT_TRANSACTION_CODE;
    }

    @Override
    protected String getSettlementCreitOrDebit() {
        return SettlementCreditDebit.CREDIT;
    }

    @Override
    protected String getChequeType() {
        return ChequeType.CLIENT;
    }

    @Override
    protected String getAccountSettingCode() {
        return GroupReceiptAccountInterface.GROUP_RECEIPT_AMOUNT_CREDIT_CODE;
    }

    @Override
    protected String getTransactionName() {
        return "Group Receipt";
    }

    @Override
    protected String getOverAmountSettlementType() {
        return SystemSettlement.OVER_PAY_RECEIPT;
    }

    @Override
    protected boolean isOverPayAvailable() {
        return true;
    }

    @Override
    protected Loan getModifiedLoan(Loan loan, Double afterBalance) {
        if (afterBalance.doubleValue() <= 0) {
            loan.setAvailableReceipt(false);
        } else {
            loan.setAvailableReceipt(true);
        }
        
        return loan;
    }
}
