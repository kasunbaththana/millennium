/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.agrement_details;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.resources.ApplicationResources;
import com.mac.agrement_details.object.Loan;
import com.mac.agrement_details.object.LoanType;
import com.mac.agrement_details.table_model.LoanTypeTableModel;
import com.mac.zsystem.recent_button.RecentButton;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Udayanga
 */
public class PCAgrementDetailss extends CPanel {

    private SERAgrementDetails sERAgrementDetails;
    //
    private String status;
    private String loanType;
    //
    private TableRowSorter sorter;
    //
    private RecentButton recentButton;

    /**
     * Creates new form PCAgrementDetailss
     */
    public PCAgrementDetailss() {
        initComponents();
        initOthers();
    }

    @SuppressWarnings("unchecked")
    public void initOthers() {

        sERAgrementDetails = new SERAgrementDetails(this);

        CTableModel cTableModel = new CTableModel(new CTableColumn[]{
            new CTableColumn("Agrement No", new String[]{"agreementNo"}),
            new CTableColumn("Loan Date", new String[]{"loanDate"}),
            new CTableColumn("Client", "client", "name"),
            new CTableColumn("Loan Type", new String[]{"loanType"}),
            new CTableColumn("Loan Amount", new String[]{"loanAmount"}),
            new CTableColumn("Period", new String[]{"installmentCount"}),
            new CTableColumn("Installment Amount", new String[]{"installmentAmount"})
        });
        tblLoan.setCModel(cTableModel);

        //cboStatus.setTableModel(new StatusTableModel());
        cboLoanType.setTableModel(new LoanTypeTableModel());

        cboStatus.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setStatus(cboStatus.getCValue().toString());
            }
        });

        cboLoanType.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setLoanType(cboLoanType.getCValue().toString());
            }
        });

        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnSearch, "doSearch");
        actionUtil.setAction(btnRefresh, "doRefresh");
        actionUtil.setAction(btnPrint, "doPrintReport");
        actionUtil.setAction(btnFind, "doFind");

        //Table Search
        //   sorter = new TableRowSorter(cTableModel);
        //   tblLoan.setRowSorter(sorter);
//        btnFind.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                String searchText = txtTableSearch.getCValue();
//                if (searchText.length() == 0) {
//                    sorter.setRowFilter(null);
//                } else {
//                    sorter.setRowFilter(RowFilter.regexFilter(searchText));
//                }
//            }
//        });

        btnSearch.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_NEW_ICON));
        btnRefresh.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_REFRESH_ICON));
        btnPrint.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_PRINT_ICON));
        btnFind.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON));

    }

    public List<LoanType> getLoanTypeLIst() {
        try {
            return sERAgrementDetails.getLoanType();
        } catch (DatabaseException ex) {
            Logger.getLogger(PCAgrementDetailss.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<LoanType>();
        }
    }

    @Action
    public void doSearch() throws DatabaseException {
        double amount = 0.0;
        double instalmentAmount = 0.0;

        String[] loanType = getLoanType().split(" ");
        tblLoan.setCValue(sERAgrementDetails.getLoanTableData(getStatus(), loanType[0]));

        List<Loan> loans = (List<Loan>) tblLoan.getCValue();
        for (Loan loan : loans) {
            amount += loan.getLoanAmount();
            instalmentAmount += loan.getInstallmentAmount();
        }

        txtTotalAmount.setCValue(amount);
        txtTotalInstallmentAmount.setCValue(instalmentAmount);
    }

    @Action
    public void doRefresh() throws DatabaseException {
        tblLoan.setCValue(new ArrayList());
    }

    @Action
    public void doFind() throws DatabaseException {
        tblLoan.find(txtTableSearch.getCValue());
    }

    @Action
    public void doPrintReport() throws DatabaseException {
        try {
            byte[] reportByteArray;
            Connection connection = CConnectionProvider.getInstance().getConnection();
//          JasperDesign jasperDesign = JRXmlLoader.load("C:\\Users\\Udayanga\\Desktop\\Reports\\recipt_print.jrxml");
            InputStream stream = new ByteArrayInputStream(null);
            JasperDesign jasperDesign = JRXmlLoader.load(stream);
            HashMap parameter = new HashMap<>();
            String[] loanType = getLoanType().split(" ");
            parameter.put("LOAN_STATUS", cboStatus.getCValue().toString());
            parameter.put("LOAN_TYPE", loanType[0]);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, connection);
            JasperViewer.viewReport(jasperPrint, false);
        } catch (JRException | SQLException ex) {
            Logger.getLogger(PCAgrementDetailss.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        btnSearch = new com.mac.af.component.derived.command.button.CCButton();
        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cboStatus = new com.mac.af.component.derived.input.combobox.CIComboBox(SERAgrementDetails.LOAN_STATUS){
        };
        cboLoanType = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoanTypeLIst();
            }
        };
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblLoan = new com.mac.af.component.base.table.CTable();
        btnPrint = new com.mac.af.component.derived.command.button.CCButton();
        txtTableSearch = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnFind = new com.mac.af.component.derived.command.button.CCButton();
        btnRefresh = new com.mac.af.component.derived.command.button.CCButton();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalAmount = new com.mac.af.component.derived.display.textfield.CDDoubleField();
        txtTotalInstallmentAmount = new com.mac.af.component.derived.display.textfield.CDDoubleField();

        jSplitPane1.setDividerLocation(360);

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        cDLabel1.setText("Status:");

        cDLabel2.setText("Loan Type:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                            .addComponent(cboLoanType, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoanType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(219, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel3);

        tblLoan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblLoan);

        btnPrint.setText("Print");

        btnFind.setText("Find");

        btnRefresh.setText("Refresh");

        cDLabel3.setText("Total Amount:");

        cDLabel4.setText("Total Instalment Amount:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTableSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(313, 313, 313)
                        .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtTotalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtTotalInstallmentAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalInstallmentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTableSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jSplitPane1.setRightComponent(jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSearchActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnFind;
    private com.mac.af.component.derived.command.button.CCButton btnPrint;
    private com.mac.af.component.derived.command.button.CCButton btnRefresh;
    private com.mac.af.component.derived.command.button.CCButton btnSearch;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanType;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboStatus;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private com.mac.af.component.base.table.CTable tblLoan;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTableSearch;
    private com.mac.af.component.derived.display.textfield.CDDoubleField txtTotalAmount;
    private com.mac.af.component.derived.display.textfield.CDDoubleField txtTotalInstallmentAmount;
    // End of variables declaration//GEN-END:variables

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getStatus() {
        return status;
    }

    public String getLoanType() {
        return loanType;
    }
}
