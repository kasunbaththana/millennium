/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.agrement_details;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.LoanStatus;
import com.mac.agrement_details.object.Loan;
import com.mac.agrement_details.object.LoanType;
import java.awt.Component;
import java.util.List;

/**
 *
 * @author Udayanga
 */
public class SERAgrementDetails extends AbstractService {

    public static String LOAN_STATUS[] = {
        LoanStatus.APPLICATION_ACCEPT,
        LoanStatus.APPLICATION_PENDING,
        LoanStatus.APPLICATION_REJECT,
        LoanStatus.APPLICATION_SUSPEND,
        LoanStatus.CANCEL,
        LoanStatus.COLLECTED,
        LoanStatus.COMPLETE,
        LoanStatus.LOAN_PENDING,
        LoanStatus.LOAN_REJECT,
        LoanStatus.LOAN_START,};

    public SERAgrementDetails(Component component) {
        super(component);
    }

    public List<LoanType> getLoanType() throws DatabaseException {
        HibernateDatabaseService hibernateDatabaseService = new HibernateDatabaseService(CPanel.GLOBAL);
        List<LoanType> loanType = hibernateDatabaseService.initCriteria(LoanType.class).list();
        return loanType;
    }

    public List<Loan> getLoanTableData(String status, String loanType) throws DatabaseException {
        return getDatabaseService().getCollection("from com.mac.agrement_details.object.Loan where loanType='" + loanType + "' and status ='" + status + "'");
    }
}
