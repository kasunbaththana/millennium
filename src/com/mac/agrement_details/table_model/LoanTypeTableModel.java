/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.agrement_details.table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author Udayanga
 */
public class LoanTypeTableModel extends CTableModel<Object> {

    public LoanTypeTableModel() {
        super(new CTableColumn("Code", new String[]{"code"}),
                new CTableColumn("Name", new String[]{"name"}));
    }
}
