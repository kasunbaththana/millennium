/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.agrement_details.table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author Udayanga
 */
public class StatusTableModel extends CTableModel<Object> {

    public StatusTableModel() {
        super(new CTableColumn("Agrement No", new String[]{"agreementNo"}));
    }
}
