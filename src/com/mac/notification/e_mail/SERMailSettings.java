/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.notification.e_mail;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.notification.e_mail.object.EMailNotification;
import java.awt.Component;
import java.util.List;

/**
 *
 * @author Udayanga
 */
public class SERMailSettings extends AbstractService {

    public SERMailSettings(Component component) {
        super(component);
    }

    public List<EMailNotification> getNotificationList() throws DatabaseException {
        return getDatabaseService().getCollection("from com.mac.notification.e_mail.object.EMailNotification");
    }

    public void changeStatus(EMailNotification eMailNotification) throws DatabaseException {
        getDatabaseService().save(eMailNotification);
    }
}
