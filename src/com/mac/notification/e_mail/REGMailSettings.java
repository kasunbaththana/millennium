/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.notification.e_mail;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.notification.e_mail.object.EMailNotification;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Udayanga
 */
public class REGMailSettings extends AbstractGridObject {

    private SERMailSettings sERMailSettings;
    private List<EMailNotification> eMailNotifications;

    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Address", "address"),
            new CTableColumn("Message", "message"),
            new CTableColumn("Status", "status"),
            new CTableColumn("Active", new String[]{"active"}, true)
        });
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        return new PCEmailSender() {
            @Override
            public void doRefresh() {
                refreshTable();
            }

            @Override
            public List<EMailNotification> getEmailList() {
                return geteMailNotifications();
            }
        };
    }

    @Override
    protected Collection getTableData() {
        try {
            sERMailSettings = new SERMailSettings(this);
            eMailNotifications = sERMailSettings.getNotificationList();
            return eMailNotifications;
        } catch (DatabaseException ex) {
            Logger.getLogger(REGMailSettings.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList();
        }
    }

    public List<EMailNotification> geteMailNotifications() {
        return eMailNotifications;
    }

    public void refreshTableContent() {
        refreshTable();
    }
}
