package com.mac.notification.e_mail.object;
// Generated Oct 5, 2015 11:47:30 AM by Hibernate Tools 3.2.1.GA



/**
 * EMailNotification generated by hbm2java
 */
public class EMailNotification  implements java.io.Serializable {


     private int indexNo;
     private String address;
     private String message;
     private String fileUrl;
     private String status;
     private boolean active;

    public EMailNotification() {
    }

	
    public EMailNotification(int indexNo, String address, String message, String fileUrl) {
        this.indexNo = indexNo;
        this.address = address;
        this.message = message;
        this.fileUrl = fileUrl;
    }
    public EMailNotification(int indexNo, String address, String message, String fileUrl, String status, boolean active) {
       this.indexNo = indexNo;
       this.address = address;
       this.message = message;
       this.fileUrl = fileUrl;
       this.status = status;
       this.active = active;
    }
   
    public int getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(int indexNo) {
        this.indexNo = indexNo;
    }
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }
    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    public String getFileUrl() {
        return this.fileUrl;
    }
    
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    public boolean getActive() {
        return this.active;
    }
    
    public void setActive(boolean active) {
        this.active = active;
    }




}


