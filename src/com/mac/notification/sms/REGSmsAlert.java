/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.notification.sms;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.notification.sms.object.SmsAlert;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Udayanga
 */
public class REGSmsAlert extends AbstractGridObject<SmsAlert> {

    private SERSmsAlert sERSmsAlert;
    private List<SmsAlert> smsAlerts;

    @Override
    protected CTableModel<SmsAlert> getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Phone No", "phnNo"),
            new CTableColumn("Message", "message"),
            new CTableColumn("Status", "status"),
            new CTableColumn("Active",new String[]{"active"},true)
        });
    }

    @Override
    protected AbstractObjectCreator<SmsAlert> getObjectCreator() {
        return new PCSms() {
            @Override
            public List getSmsList() {
                return getSmsAlerts();
            }
        };
    }

    @Override
    protected Collection<SmsAlert> getTableData() {
        try {
            sERSmsAlert = new SERSmsAlert(this);
            smsAlerts = sERSmsAlert.getNotificationList();
            return smsAlerts;
        } catch (DatabaseException ex) {
            Logger.getLogger(REGSmsAlert.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    public List<SmsAlert> getSmsAlerts() {
        return smsAlerts;
    }
    
    public void refreshTableContent(){
        refreshTable();
    }
}
