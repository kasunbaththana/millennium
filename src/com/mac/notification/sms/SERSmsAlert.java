/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.notification.sms;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.notification.sms.object.SmsAlert;
import java.awt.Component;
import java.util.List;

/**
 *
 * @author Udayanga
 */
public class SERSmsAlert extends AbstractService {

    public SERSmsAlert(Component component) {
        super(component);
    }

    public List<SmsAlert> getNotificationList() throws DatabaseException {
        return getDatabaseService().getCollection("from com.mac.notification.sms.object.SmsAlert");
    }

    public void changeStatus(SmsAlert eMailNotification) throws DatabaseException {
        getDatabaseService().save(eMailNotification);
    }
}
