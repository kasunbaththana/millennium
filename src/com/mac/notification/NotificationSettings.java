/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.notification;

import com.mac.af.component.derived.input.textarea.CITextArea;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.notification.e_mail.REGMailSettings;
import com.mac.notification.e_mail.SERMailSettings;
import com.mac.notification.e_mail.object.EMailNotification;
import com.mac.notification.sms.REGSmsAlert;
import com.mac.notification.sms.SERSmsAlert;
import com.mac.notification.sms.object.SmsAlert;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

/**
 *
 * @author Udayanga
 */
public abstract class NotificationSettings extends CPanel {

    private REGSmsAlert rEGSmsAlert;
    private SERSmsAlert sERSmsAlert;
    private REGMailSettings rEGMailSettings;
    private SERMailSettings sERMailSettings;
    //
    private List<SmsAlert> smsAlerts;
    private int listSize = 0;
    private int res = 0;
    //
    private List<EMailNotification> eMailNotifications;
    private int tableSize = 0;
    private int result = 0;
    //
    public static final String SMS_STATUS = "SEND";
    public static final String MAIL_STATUS = "SEND";
    //

    public abstract JProgressBar jProgressBar();
    //
    public static String server;
    public static String user;
    public static String password;
    public static String phonenumber;
    public static String text;
    public static String data;
    public static String udh;
    public static String pid;
    public static String dcs;
    public static String sender;
    public static String validity;
    public static String servicetype;
    public static String smscroute;
    public static String receiptrequested;
    public static String sourceport;
    public static String destport;
    public static String delayuntil;
    public static String voicemail;
    public static String wapurl;
    public static String wapsl;
    public static String url_str;

    public static void init() {
        server = null;
        user = null;
        password = null;
        phonenumber = null;
        text = null;
        data = null;
        udh = null;
        pid = null;
        dcs = null;
        sender = null;
        validity = null;
        servicetype = null;
        smscroute = null;
        receiptrequested = null;
        sourceport = null;
        destport = null;
        delayuntil = null;
        voicemail = null;
        wapurl = null;
        wapsl = null;
    }

    private boolean checkConnection() {
        try {
            URL url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            if (connection.getContentLength() == -1) {
                System.out.println("Failed to verify connection");
            }
            System.out.println("Connect");
        } catch (IOException e) {
            System.out.println("Failed to open a connection");
        }
        return true;
    }

    public boolean sendEmailMessage(String toRecever, String messageText) {


        if (checkConnection() == true) {

            File file = new File("D:\\Programming Assignment Week 3.pdf");

            String to = toRecever;
            String from = "cmpnytester@gmail.com";

            Properties properties = new Properties();
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.socketFactory.port", "465");
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.port", "465");

            Session session = Session.getDefaultInstance(properties,
                    new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("cmpnytester@gmail.com", "!@#$1234");
                }
            });

            try {
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(from));
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                message.setSubject("From ");
                BodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setText("smtk" + messageText);
                Multipart multipart = new MimeMultipart();
                multipart.addBodyPart(messageBodyPart);
//                messageBodyPart = new MimeBodyPart();
//                DataSource source = new FileDataSource(file.getName());
//                messageBodyPart.setDataHandler(new DataHandler(source));
//                messageBodyPart.setFileName(file.getName());
//                multipart.addBodyPart(messageBodyPart);
                message.setContent(multipart);
                Transport.send(message);
                Thread.sleep(1000);
            } catch (MessagingException ex) {
                ex.printStackTrace();
            } catch (InterruptedException ex) {
                Logger.getLogger(NotificationSettings.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else {
            return false;
        }
    }

    public static void setvar(String argname, String argvalue) {
        if (argname != null) {
            if (argvalue != null) {
                url_str = url_str + "&" + argname + "=";
                try {
                    String encoded = URLEncoder.encode(argvalue, "UTF-8");
                    url_str = url_str + encoded;
                } catch (UnsupportedEncodingException e) {
                    url_str = url_str + argvalue;
                }
            }
        }
    }

    public static String sendSms() {
        String returnstring;
        returnstring = null;
        if (server == null) {
            System.out.println("sendsms.server value not set");
            return returnstring;
        }

        url_str = server + "?";
        setvar("user", user);
        setvar("password", password);
        setvar("phonenumber", phonenumber);
        setvar("text", text);
        setvar("data", data);
        setvar("udh", udh);
        setvar("pid", pid);
        setvar("dcs", dcs);
        setvar("sender", sender);
        setvar("validity", validity);
        setvar("servicetype", servicetype);
        setvar("smscroute", smscroute);
        setvar("receiptrequested", receiptrequested);
        setvar("sourceport", sourceport);
        setvar("destport", destport);
        setvar("delayuntil", delayuntil);
        setvar("voicemail", voicemail);
        setvar("wapurl", wapurl);
        setvar("wapsl", wapsl);

        try {
            URL url2 = new URL(url_str);
            HttpURLConnection connection = (HttpURLConnection) url2.openConnection();
            connection.setDoOutput(false);
            connection.setDoInput(true);
            String res = connection.getResponseMessage();
            System.out.println("Response Code ->" + res);
            int code = connection.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String str;
                while (null != ((str = in.readLine()))) {
                    if (str.startsWith("MessageID=")) {
                        returnstring = returnstring + str + "\r\n";
                        System.out.println(str);
                    }
                }
                System.out.println("send sms");
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(NotificationSettings.class.getName()).log(Level.SEVERE, null, ex);
                }
                connection.disconnect();
            }
        } catch (IOException e) {
            System.out.println("unable to create new url" + e.getMessage());
        }
        return returnstring;
    }

    public void setSmsData(String phoneNumber, String text) {
        NotificationSettings.init();
        NotificationSettings.server = "http://127.0.0.1:8800/";
        NotificationSettings.user = "serverUserName";
        NotificationSettings.password = "serverPassword";
        NotificationSettings.phonenumber = phoneNumber;
        NotificationSettings.text = text;
        NotificationSettings.sendSms();
    }

    public boolean doSmsInBackground(final JProgressBar jProgressBar, final CITextArea cDTextArea) {
        rEGSmsAlert = new REGSmsAlert();
        smsAlerts = rEGSmsAlert.getSmsAlerts();
        listSize = smsAlerts.size();
        res = (jProgressBar.getMaximum() + jProgressBar.getMinimum()) / listSize;

        sERSmsAlert = new SERSmsAlert(this);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for (SmsAlert object : smsAlerts) {

                    object.setStatus(NotificationSettings.SMS_STATUS);
                    setSmsData(object.getPhnNo(), object.getMessage());
                    try {
                        sERSmsAlert.changeStatus(object);
                        rEGSmsAlert.refreshTableContent();
                    } catch (DatabaseException ex) {
                        Logger.getLogger(NotificationSettings.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        try {
                            Runnable runnable1 = new Runnable() {
                                public void run() {
                                    jProgressBar.setValue(res);
                                    cDTextArea.setText(cDTextArea.getText()
                                            + String.format("Completed %d%% of task.\n", res));
                                }
                            };
                            SwingUtilities.invokeLater(runnable1);
                        } catch (Exception e) {
                        }
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(NotificationSettings.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    res = res + res;
                }
            }
        });
        t.start();
        return true;
    }

    public boolean doEmailInBackGround(final JProgressBar jProgressBar, final CITextArea cDTextArea) {
        rEGMailSettings = new REGMailSettings();
        eMailNotifications = rEGMailSettings.geteMailNotifications();
        tableSize = eMailNotifications.size();
        result = jProgressBar.getMaximum() + jProgressBar.getMaximum() / tableSize;

        sERMailSettings = new SERMailSettings(this);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (EMailNotification eMailNotification : eMailNotifications) {

                    sendEmailMessage(eMailNotification.getAddress(), eMailNotification.getMessage());
                    eMailNotification.setStatus(NotificationSettings.MAIL_STATUS);

                    try {
                        sERMailSettings.changeStatus(eMailNotification);
                        rEGMailSettings.refreshTableContent();
                        try {
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    jProgressBar.setValue(result);
                                    cDTextArea.setText(cDTextArea.getText()
                                            + String.format("Completed %d%% of task.\n", result));
                                }
                            };
                            SwingUtilities.invokeLater(runnable);
                            Thread.sleep(5000);
                            result = result + result;
                        } catch (InterruptedException ex) {
                            Logger.getLogger(NotificationSettings.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (DatabaseException ex) {
                        Logger.getLogger(NotificationSettings.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        thread.start();
        return false;
    }
}
