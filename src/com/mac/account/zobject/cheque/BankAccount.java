package com.mac.account.zobject.cheque;

/**
  *	@author Channa Mohan
  *	
  *	Created On Nov 4, 2014 4:06:51 PM 
  *	Mohan Hibernate Mapping Generator
  */


import java.util.HashSet;
import java.util.Set;

/**
 * BankAccount generated by hbm2java
 */
public class BankAccount  implements java.io.Serializable {


     private String code;
     private BankBranch bankBranch;
     private String owner;
     private String accountNumber;
     private String valueAccount;
     private String pendingAccount;
     private boolean active;
     private Set<Cheque> cheques = new HashSet<Cheque>(0);

    public BankAccount() {
    }

	
    public BankAccount(String code, BankBranch bankBranch, String owner, String accountNumber, String valueAccount, String pendingAccount, boolean active) {
        this.code = code;
        this.bankBranch = bankBranch;
        this.owner = owner;
        this.accountNumber = accountNumber;
        this.valueAccount = valueAccount;
        this.pendingAccount = pendingAccount;
        this.active = active;
    }
    public BankAccount(String code, BankBranch bankBranch, String owner, String accountNumber, String valueAccount, String pendingAccount, boolean active, Set<Cheque> cheques) {
       this.code = code;
       this.bankBranch = bankBranch;
       this.owner = owner;
       this.accountNumber = accountNumber;
       this.valueAccount = valueAccount;
       this.pendingAccount = pendingAccount;
       this.active = active;
       this.cheques = cheques;
    }
   
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    public BankBranch getBankBranch() {
        return this.bankBranch;
    }
    
    public void setBankBranch(BankBranch bankBranch) {
        this.bankBranch = bankBranch;
    }
    public String getOwner() {
        return this.owner;
    }
    
    public void setOwner(String owner) {
        this.owner = owner;
    }
    public String getAccountNumber() {
        return this.accountNumber;
    }
    
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public String getValueAccount() {
        return this.valueAccount;
    }
    
    public void setValueAccount(String valueAccount) {
        this.valueAccount = valueAccount;
    }
    public String getPendingAccount() {
        return this.pendingAccount;
    }
    
    public void setPendingAccount(String pendingAccount) {
        this.pendingAccount = pendingAccount;
    }
    public boolean isActive() {
        return this.active;
    }
    
    public void setActive(boolean active) {
        this.active = active;
    }
    public Set<Cheque> getCheques() {
        return this.cheques;
    }
    
    public void setCheques(Set<Cheque> cheques) {
        this.cheques = cheques;
    }






	@Override
	public boolean equals(Object other) {
        if ( (this == other ) ){
			return true;
		}
		
		if ( (other == null ) ){
			return false;
		}
		
		if ( !(other instanceof BankAccount) ){
			return false;
		}
		
		BankAccount castOther = ( BankAccount ) other; 

		if(this.code==null && castOther.code==null) {
			return false;
		}
		
		if(!java.util.Objects.equals(this.code, castOther.code)) {
            return false;
		}
        
		return true;
   }

    @Override
    public int hashCode() {
        int result = 17;
         
		result = result * 17 + java.util.Objects.hashCode(this.code);

        return result;
    }

    @Override
    public String toString() {
        return "["+code+"] "+accountNumber;
    }




}


