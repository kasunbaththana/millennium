package com.mac.account.zobject.cheque;

/**
  *	@author Channa Mohan
  *	
  *	Created On Nov 4, 2014 4:06:51 PM 
  *	Mohan Hibernate Mapping Generator
  */


import java.util.Date;

/**
 * Cheque generated by hbm2java
 */
public class Cheque  implements java.io.Serializable {


     private Integer indexNo;
     private BankBranch bankBranch;
     private Client client;
     private BankAccount bankAccount;
     private String branch;
     private Integer paymentObject;
     private String accountNo;
     private String chequeNo;
     private Double amount;
     private Date chequeDate;
     private Date depositDate;
     private Date realizeDate;
     private Date returnDate;
     private String status;
     private String type;

    public Cheque() {
    }

    public Cheque(BankBranch bankBranch, Client client, BankAccount bankAccount, String branch, Integer paymentObject, String accountNo, String chequeNo, Double amount, Date chequeDate, Date depositDate, Date realizeDate, Date returnDate, String status, String type) {
       this.bankBranch = bankBranch;
       this.client = client;
       this.bankAccount = bankAccount;
       this.branch = branch;
       this.paymentObject = paymentObject;
       this.accountNo = accountNo;
       this.chequeNo = chequeNo;
       this.amount = amount;
       this.chequeDate = chequeDate;
       this.depositDate = depositDate;
       this.realizeDate = realizeDate;
       this.returnDate = returnDate;
       this.status = status;
       this.type = type;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public BankBranch getBankBranch() {
        return this.bankBranch;
    }
    
    public void setBankBranch(BankBranch bankBranch) {
        this.bankBranch = bankBranch;
    }
    public Client getClient() {
        return this.client;
    }
    
    public void setClient(Client client) {
        this.client = client;
    }
    public BankAccount getBankAccount() {
        return this.bankAccount;
    }
    
    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
    public String getBranch() {
        return this.branch;
    }
    
    public void setBranch(String branch) {
        this.branch = branch;
    }
    public Integer getPaymentObject() {
        return this.paymentObject;
    }
    
    public void setPaymentObject(Integer paymentObject) {
        this.paymentObject = paymentObject;
    }
    public String getAccountNo() {
        return this.accountNo;
    }
    
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
    public String getChequeNo() {
        return this.chequeNo;
    }
    
    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }
    public Double getAmount() {
        return this.amount;
    }
    
    public void setAmount(Double amount) {
        this.amount = amount;
    }
    public Date getChequeDate() {
        return this.chequeDate;
    }
    
    public void setChequeDate(Date chequeDate) {
        this.chequeDate = chequeDate;
    }
    public Date getDepositDate() {
        return this.depositDate;
    }
    
    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }
    public Date getRealizeDate() {
        return this.realizeDate;
    }
    
    public void setRealizeDate(Date realizeDate) {
        this.realizeDate = realizeDate;
    }
    public Date getReturnDate() {
        return this.returnDate;
    }
    
    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }





	@Override
	public boolean equals(Object other) {
		if (this == other){
			return true;
		}
			 
		if (other == null){
			return false;
		}
		
		if ( !(other instanceof Cheque) ){
			return false;
		}
		
		Cheque castOther = ( Cheque ) other; 

		if(this.indexNo==null && castOther.indexNo==null) {
			return false;
		}
		
		if(!java.util.Objects.equals(this.indexNo, castOther.indexNo)) {
			return false;
		}
			 
		return true;
	}

    @Override
    public int hashCode() {
        int result = 17;
        
		result = result * 17 + java.util.Objects.hashCode(this.indexNo);

        return result;
    }





}


