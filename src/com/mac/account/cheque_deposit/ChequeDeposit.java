/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.cheque_deposit;

import com.mac.account.zobject.cheque.Cheque;
import com.mac.account.zobject.cheque.ChequeDetails;
import com.mac.account.ztemplate.cheque_transaction.AbstractChequeTransaction;
import com.mac.account.ztemplate.cheque_transaction.ChequeStatus;
import com.mac.account.ztemplate.cheque_transaction.custom_object.ChequeTransactionObject;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeDepositAccountInterface;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SMTK
 */
public class ChequeDeposit extends AbstractChequeTransaction {

//    @Override
//    public String executeText() {
//        return "Deposit";
//    }
//
//    @Override
//    public String getChequeNewStatus() {
//        return ChequeStatus.DEPOSIT;
//    }
//
//    @Override
//    public String getChequeOldStatus() {
//        return ChequeStatus.PENDING;
//    }
    @Override
    protected String getTitle() {
        return "Cheque Deposit";
    }

//    @Override
//    public String getChequeType() {
//        return ChequeType.CLIENT;
//    }
    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.CHEQUE_DEPOSIT_TRANSACTION_CODE;
    }

    @Override
    protected String getReferenceGeneratorKey() {
        return ReferenceGenerator.CHECQUE_DEPOSIT;
    }

//    @Override
//    protected String getCreditAccountSettingCode() {
//        return ChequeDepositAccountInterface.DEPOSIT_AMOUNT_CREDIT_CODE;
//    }
//
//    @Override
//    protected String getDebitAccountSettingCode() {
//        return ChequeDepositAccountInterface.DEPOSIT_AMOUNT_DEBIT_CODE;
//    }
    @Override
    protected List<Cheque> getCheques(ChequeTransactionObject transactionObject) {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("type", ChequeType.CLIENT);
            params.put("status", ChequeStatus.PENDING);
           
            list = getDatabaseService().getCollection("from com.mac.account.zobject.cheque.Cheque where "
                    + "status=:status and type=:type  ", params);
           
          
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(AbstractChequeTransaction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    protected void saveCheques(ChequeTransactionObject transactionObject, Collection<Cheque> cheques) throws DatabaseException {
//        TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                getTransactionTypeCode(),
                transactionObject.getReferenceNo(),
                transactionObject.getDocumentNo(),
                null,
                null,
                null,
                transactionObject.getNote());

        //CHEQUES
        for (Cheque cheque : cheques) {
            getDatabaseService().beginLocalTransaction();
            cheque.setStatus(ChequeStatus.DEPOSIT);
            cheque.setBankAccount(transactionObject.getBankAccount());
            cheque.setDepositDate(transactionObject.getAccountDate());

            getDatabaseService().save(cheque);
            getDatabaseService().save(cheque);
            getDatabaseService().commitLocalTransaction();
        }

          // save cheque datails
            ChequeDetails   ch_details=new ChequeDetails();
            for(Cheque cheque_detail : cheques)
            {
                ch_details.setChequeIndex(cheque_detail);
                ch_details.setTransaction(transactionIndex);
                ch_details.setDescription(SystemTransactions.CHEQUE_DEPOSIT_TRANSACTION_CODE);
                ch_details.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                ch_details.setAmount(cheque_detail.getAmount());
                ch_details.setStatus("ACTIVE");
                
                
                getDatabaseService().save(ch_details);
            getDatabaseService().commitLocalTransaction();
            }
        //ACCOUNT TRANSACTION
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        for (Cheque cheque : cheques) {
            
             accountTransaction.saveAccountTransactionChque(
                            getDatabaseService(),
                            transactionIndex,
                            SystemTransactions.CHEQUE_DEPOSIT_TRANSACTION_CODE,
                            AccountTransactionType.AUTO,
                            AccountSettingCreditOrDebit.DEBIT,
                            cheque.getAmount(),
                            transactionObject.getBankAccount().getPendingAccount(),
                            "Cheque Deposit From "+transactionObject.getBankAccount().getPendingAccount(),0,
                            cheque.getDepositDate());
             System.out.println("getDepositDate"+ cheque.getDepositDate());
            
            accountTransaction.addAccountTransactionQueue(
                    ChequeDepositAccountInterface.DEPOSIT_AMOUNT_CREDIT_CODE,
                    "Cheque Deposit Amount",
                    cheque.getAmount(),
                    AccountTransactionType.AUTO);

            
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, getTransactionTypeCode());
            
            
//            accountTransaction.addAccountTransactionQueue(
//                    ChequeDepositAccountInterface.DEPOSIT_AMOUNT_DEBIT_CODE,
//                    "Cheque Deposit Amount",
//                    cheque.getAmount(),
//                    AccountTransactionType.AUTO);

        }
    }

    @Override
    protected String getExecuteText() {
        return "Deposit";
    }
    private static final String TRANSACTION_NAME = "Cheque Deposit";
}
