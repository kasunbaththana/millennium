/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.ztemplate.account_entry;

import com.mac.account.ztemplate.account_entry.object.Entry;
import com.mac.account.ztemplate.account_entry.object.AccountEntryTransaction;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.BankDepositAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.PettyCashAccountInterface;
import com.mac.zsystem.transaction.account.object.GeneralVoucherSum;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SMTK
 */
public abstract class SERAccountEntry extends AbstractService {

    public SERAccountEntry(Component component) {
        super(component);
         try {
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException | DatabaseException ex) {
            Logger.getLogger(SERAccountEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected abstract String getTransactionTypeCode();

    protected abstract String getAccountTransactionTypeCode();

    public List getAccouns() {
        List list;
       try {  
            list = getDatabaseService().getCollection("from com.mac.zsystem.transaction.account.object.Account where active=true ");
    
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERAccountEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void save(AccountEntryTransaction transaction, Collection<Entry> journals) throws DatabaseException {
        //TRANSACTION
        int transactionIndex = saveTransaction(transaction);
        //ACCOUNT TRANSACTION
        
        if(getTransactionTypeCode().equals(SystemTransactions.BANK_ENTRY_TRANSACTION_CODE)){
            
        saveBankAccountEntries(transactionIndex, journals,transaction.getTransactionDate());
        }else{
            
        saveAccountEntries(transactionIndex, journals);
        }
    }
    
  
   
    
     public void saveAccountEntriesPettyCash(int transactionIndex, Collection<Entry> entries) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        double paymentAmount = 0;
        for (Entry entry : entries) {
            paymentAmount += entry.getDebitAmount();
        }

        accountTransaction.addAccountTransactionQueue(
                PettyCashAccountInterface.PETTY_CASH_CREDIT_CODE,
                "Petty Cash",
                paymentAmount,
                AccountTransactionType.AUTO);
        
         accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, getTransactionTypeCode());


    }
     


    public int saveTransaction(AccountEntryTransaction entryTransaction) throws DatabaseException {
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                getTransactionTypeCode(),
                entryTransaction.getReferenceNo(),
                entryTransaction.getDocumentNo(),
                null, //loan
                cashierSession.getIndexNo(), //cashier session
                null, //client
                entryTransaction.getNote());
        
        

        return transactionIndex;
    }
    public int saveTransactionVoucher(AccountEntryTransaction entryTransaction,Collection<Entry> entries) throws DatabaseException {
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                getTransactionTypeCode(),
                entryTransaction.getReferenceNo(),
                entryTransaction.getDocumentNo(),
                null, //loan
                cashierSession.getIndexNo(), //cashier session
                null, //client
                entryTransaction.getNote());
        String description="";
        for (Entry entry : entries) {
            
        description+=entry.getDescription()+" ";
        }
        
          GeneralVoucherSum generalVoucherSum=new GeneralVoucherSum();
            generalVoucherSum.setReferenceNo(entryTransaction.getReferenceNo());
            generalVoucherSum.setTransaction(transactionIndex);
            generalVoucherSum.setDescription(description);
            generalVoucherSum.setAmount(entryTransaction.getTotalCreditAmount()>0?entryTransaction.getTotalCreditAmount():entryTransaction.getTotalDebitAmount());
            generalVoucherSum.setStatus("PENDING");
            generalVoucherSum.setTdate(entryTransaction.getTransactionDate());
        
        getDatabaseService().save(generalVoucherSum);

        return transactionIndex;
    }
Date transeDate;
    public void saveAccountEntries(int transactionIndex, Collection<Entry> entries) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (Entry entry : entries) {
            accountTransaction.saveAccountTransaction(
                    getDatabaseService(),
                    transactionIndex,
                    getTransactionTypeCode(),
                    getAccountTransactionTypeCode(),
                    entry.getCreditOrDebit(),
                    entry.getCreditOrDebit().equals(AccountSettingCreditOrDebit.CREDIT) ? entry.getCreditAmount() : entry.getDebitAmount(),
                    entry.getAccount(),
                    entry.getDescription(),0);
            
        }
    }
    public void saveBankAccountEntries(int transactionIndex, Collection<Entry> entries,Date transDate) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (Entry entry : entries) {
            accountTransaction.saveAccountTransactionChque(
                    getDatabaseService(),
                    transactionIndex,
                    getTransactionTypeCode(),
                    getAccountTransactionTypeCode(),
                    entry.getCreditOrDebit(),
                    entry.getCreditOrDebit().equals(AccountSettingCreditOrDebit.CREDIT) ? entry.getCreditAmount() : entry.getDebitAmount(),
                    entry.getAccount(),
                    entry.getDescription(),0,transDate);
            
        }
    }
    public void saveAccountEntriesForCheque(int transactionIndex, Collection<Entry> entries,Date transDate) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (Entry entry : entries) {
            accountTransaction.saveAccountTransactionChque(
                    getDatabaseService(),
                    transactionIndex,
                    getTransactionTypeCode(),
                    getAccountTransactionTypeCode(),
                    entry.getCreditOrDebit(),
                    entry.getCreditOrDebit().equals(AccountSettingCreditOrDebit.CREDIT) ? entry.getCreditAmount() : entry.getDebitAmount(),
                    entry.getAccount(),
                    entry.getDescription(),0,transDate);
            
        }
    }
    public void saveAccountEntriesForVoucher(int transactionIndex, Collection<Entry> entries,Date transDate) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (Entry entry : entries) {
            accountTransaction.saveAccountTransactionVoucher(
                    getDatabaseService(),
                    transactionIndex,
                    getTransactionTypeCode(),
                    getAccountTransactionTypeCode(),
                    entry.getCreditOrDebit(),
                    entry.getCreditOrDebit().equals(AccountSettingCreditOrDebit.CREDIT) ? entry.getCreditAmount() : entry.getDebitAmount(),
                    entry.getAccount(),
                    entry.getDescription(),0,transDate);
            
        }
    }
      

    public boolean showPaymentDialog(String transactionTypeCode, AccountEntryTransaction receipt, String chequeType, String accountEntryType) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getTransactionDate(),
                (accountEntryType.equals(AccountEntry.ACCOUNT_ENTRY_CREDIT_ONLY)
                ? receipt.getTotalCreditAmount()
                : accountEntryType.equals(AccountEntry.ACCOUNT_ENTRY_DEBIT_ONLY)
                ? receipt.getTotalDebitAmount()
                : 0.0),
                chequeType);

        return isPaymentOk;
    }

    public void savePayment(int transactionIndex, AccountEntryTransaction receipt) throws DatabaseException, ApplicationException {
        systemPayment.savePayment(
                getDatabaseService(),
                SystemCashier.getCurrentCashierSession(getDatabaseService()),
                transactionIndex,
                null);
    }
    public void savePaymentVoucher(int transactionIndex, AccountEntryTransaction receipt,Date transDate,String description) throws DatabaseException, ApplicationException {
        systemPayment.savePaymentVoucher(
                getDatabaseService(),
                SystemCashier.getCurrentCashierSession(getDatabaseService()),
                transactionIndex,
                null,transDate,description);
    }
    private SystemPayment systemPayment;
    private CashierSession cashierSession;
}
