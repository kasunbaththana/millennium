/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.ztemplate.entry_payment;

import com.mac.account.ztemplate.account_entry.AccountEntry;
import com.mac.account.ztemplate.account_entry.object.AccountEntryTransaction;
import com.mac.account.ztemplate.account_entry.object.Entry;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import static com.mac.af.templates.registration.AbstractRegistrationForm.SAVE_SUCCESS;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.object.Account;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mohan
 */
public abstract class EntryPayment extends AccountEntry {

    public EntryPayment() {
        if (!getTransactionTypeCode().equals(SystemTransactions.GENERAL_VOUCHER_2_TRANSACTION_CODE)) {
            initCashierSession();
        }
        initOthers();
    }

    protected abstract String getChequeType();

    @Override
    public String getAccountEntryType() {
        return ACCOUNT_ENTRY_DEBIT_ONLY;
    }

    @Override
    protected String getAccountTransactionTypeCode() {
        return AccountTransactionType.AUTO;
    }

    @Override
    protected boolean isAccountCreditDebitEqualNeeded() {
        return false;
    }

    @Override
    public int save(AccountEntryTransaction entryTransaction, Collection<Entry> entries) {
        try {
            boolean isPaymentOK = getService().showPaymentDialog(
                    getTransactionTypeCode(),
                    entryTransaction,
                    getChequeType(),
                    getAccountEntryType());

            if (isPaymentOK) {
                if (getTransactionTypeCode().equals(SystemTransactions.GENERAL_VOUCHER_TRANSACTION_CODE)) {
                    //ITS JUST GOINT TO APPROVE
                    //SAVE TRANSACTION
                int vTransactionIndex = getService().saveTransactionVoucher(entryTransaction,entries); 
                   //SAVE ACCOUNT ENTRIES
                getService().saveAccountEntriesForVoucher(vTransactionIndex, entries,entryTransaction.getTransactionDate());
 
                String description = GetAccountDescription(entries);
                
                getService().savePaymentVoucher(vTransactionIndex, entryTransaction,entryTransaction.getTransactionDate(),description);
                    
                return SAVE_SUCCESS;
                }else{
                    //SAVE TRANSACTION
                int transactionIndex = getService().saveTransaction(entryTransaction);

                //SAVE ACCOUNT ENTRIES
                getService().saveAccountEntriesForCheque(transactionIndex, entries,entryTransaction.getTransactionDate());

                 //SAVE PAYMENTS
                if (getTransactionTypeCode().equals(SystemTransactions.GENERAL_VOUCHER_2_TRANSACTION_CODE)) {
                    getService().saveAccountEntriesPettyCash(transactionIndex, entries);
                } else {
                    getService().savePayment(transactionIndex, entryTransaction);
                }
                
                
                
                
                //REPORT
                Map<String, Object> params = new HashMap<>();
                params.put("TRANSACTION_NO", transactionIndex);
                TransactionUtil.PrintReport(getDatabaseService(), getTransactionTypeCode(), params);
                return SAVE_SUCCESS;
                }
            }
            return SAVE_FAILED;
        } catch (DatabaseException ex) {
            Logger.getLogger(EntryPayment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ApplicationException ex) {
            Logger.getLogger(EntryPayment.class.getName()).log(Level.SEVERE, null, ex);
        }
        return SAVE_FAILED;
    }

    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
    }
    
    public String GetAccountDescription(Collection<Entry> entries){
        String result="";
        for(Entry entriesEntrys:entries){
            Account a= (Account)getDatabaseService().initCriteria(Account.class)
                    .add(Restrictions.eq("code", entriesEntrys.getAccount()))
                    .uniqueResult();

            result+=a.getName()+" ";
        }
   
        return result;
    }

    private void initOthers() {
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(getTransactionTypeCode());
        this.recentButton.setDatabseService(getDatabaseService());
    }

    private void initCashierSession() {
        try {
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException ex) {
            Logger.getLogger(EntryPayment.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private RecentButton recentButton;
    private CashierSession cashierSession;
}
