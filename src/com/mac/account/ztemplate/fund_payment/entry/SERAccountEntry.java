/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.ztemplate.fund_payment.entry;

import com.mac.account.fund_registration.object.FundRegistration;
import com.mac.account.ztemplate.fund_payment.entry.object.Entry;
import com.mac.account.ztemplate.fund_payment.entry.object.AccountEntryTransaction;
import com.mac.account.ztemplate.fund_payment.entry.object.FundIssue;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.PettyCashAccountInterface;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kasun
 */
public abstract class SERAccountEntry extends AbstractService {

    public SERAccountEntry(Component component) {
        super(component);
         try {
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException | DatabaseException ex) {
            Logger.getLogger(SERAccountEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected abstract String getTransactionTypeCode();

    protected abstract String getAccountTransactionTypeCode();

    public List getAccouns() {
        List list;
       try {  
           if(getTransactionTypeCode().equals(SystemTransactions.FUND_ISSUE_VOUCHER_TRANSACTION_CODE)){
                list = getDatabaseService().getCollection("from com.mac.zsystem.transaction.account.object.Account where active=true AND accountGroup='FUND_ACCOUNT' ");
           }else{
               
            list = getDatabaseService().getCollection("from com.mac.zsystem.transaction.account.object.Account where active=true and code='32-1000' ");
           }
    
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERAccountEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List getMember() {
        List list;
       try {  
            list = getDatabaseService().getCollection("from com.mac.loan.zobject.Client where active=true ");
    
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERAccountEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void save(AccountEntryTransaction transaction, Collection<Entry> journals) throws DatabaseException {
        //TRANSACTION
        int transactionIndex = saveTransaction(transaction);
        //ACCOUNT TRANSACTION
        
        if(getTransactionTypeCode().equals(SystemTransactions.BANK_ENTRY_TRANSACTION_CODE)){
            
        saveBankAccountEntries(transactionIndex, journals,transaction.getTransactionDate());
        }else{
            
        saveAccountEntries(transactionIndex, journals);
        }
    }
    
  
   
    
     public void saveAccountEntriesPettyCash(int transactionIndex, Collection<Entry> entries) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        double paymentAmount = 0;
        for (Entry entry : entries) {
            paymentAmount += entry.getDebitAmount();
        }

        accountTransaction.addAccountTransactionQueue(
                PettyCashAccountInterface.PETTY_CASH_CREDIT_CODE,
                "Petty Cash",
                paymentAmount,
                AccountTransactionType.AUTO);
        
         accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, getTransactionTypeCode());


    }
     


    public int saveTransaction(AccountEntryTransaction entryTransaction) throws DatabaseException {
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                getTransactionTypeCode(),
                entryTransaction.getReferenceNo(),
                entryTransaction.getDocumentNo(),
                null, //loan
                cashierSession.getIndexNo(), //cashier session
                null, //client
                entryTransaction.getNote());

        return transactionIndex;
    }
Date transeDate;
    public void saveAccountEntries(int transactionIndex, Collection<Entry> entries) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (Entry entry : entries) {
            accountTransaction.saveAccountTransaction(
                    getDatabaseService(),
                    transactionIndex,
                    getTransactionTypeCode(),
                    getAccountTransactionTypeCode(),
                    entry.getCreditOrDebit(),
                    entry.getCreditOrDebit().equals(AccountSettingCreditOrDebit.CREDIT) ? entry.getCreditAmount() : entry.getDebitAmount(),
                    entry.getAccount(),
                    entry.getDescription(),0);
            
        }
    }
    public void saveBankAccountEntries(int transactionIndex, Collection<Entry> entries,Date transDate) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (Entry entry : entries) {
            accountTransaction.saveAccountTransactionChque(
                    getDatabaseService(),
                    transactionIndex,
                    getTransactionTypeCode(),
                    getAccountTransactionTypeCode(),
                    entry.getCreditOrDebit(),
                    entry.getCreditOrDebit().equals(AccountSettingCreditOrDebit.CREDIT) ? entry.getCreditAmount() : entry.getDebitAmount(),
                    entry.getAccount(),
                    entry.getDescription(),0,transDate);
            
        }
    }
    public void saveAccountEntriesForCheque(int transactionIndex, Collection<Entry> entries,AccountEntryTransaction accountEntryTransaction) throws DatabaseException {
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (Entry entry : entries) {
            accountTransaction.saveAccountTransactionChque(
                    getDatabaseService(),
                    transactionIndex,
                    getTransactionTypeCode(),
                    getAccountTransactionTypeCode(),
                    entry.getCreditOrDebit(),
                    entry.getCreditOrDebit().equals(AccountSettingCreditOrDebit.CREDIT) ? entry.getCreditAmount() : entry.getDebitAmount(),
                    entry.getAccount(),
                    entry.getDescription(),0,accountEntryTransaction.getTransactionDate());
            
        }
    }
      

    public boolean showPaymentDialog(String transactionTypeCode, AccountEntryTransaction receipt, String chequeType, String accountEntryType) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                receipt.getReferenceNo(),
                receipt.getDocumentNo(),
                receipt.getTransactionDate(),
                (accountEntryType.equals(AccountEntry.ACCOUNT_ENTRY_CREDIT_ONLY)
                ? receipt.getTotalCreditAmount()
                : accountEntryType.equals(AccountEntry.ACCOUNT_ENTRY_DEBIT_ONLY)
                ? receipt.getTotalDebitAmount()
                : 0.0),
                chequeType);

        return isPaymentOk;
    }

    public void savePayment(int transactionIndex, AccountEntryTransaction receipt) throws DatabaseException, ApplicationException {
        systemPayment.savePayment(
                getDatabaseService(),
                SystemCashier.getCurrentCashierSession(getDatabaseService()),
                transactionIndex,
                null);
    }
    
    public void saveFundIssue(int transaction,AccountEntryTransaction entryTransaction){
        
        try {
            
             FundIssue fundIssue =new FundIssue();
        
                fundIssue.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
                fundIssue.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                fundIssue.setTransaction(transaction);
                fundIssue.setClient(entryTransaction.getClient());
                fundIssue.setRefNo(entryTransaction.getReferenceNo());
                fundIssue.setAmount(entryTransaction.getTotalDebitAmount());
                fundIssue.setStatus("ACTIVE");
                
                getDatabaseService().save(fundIssue);
            
        } catch (Exception e) {
        }
        
        
    }
    public void saveFundRegistration(int transaction,AccountEntryTransaction entryTransaction){
        
        
        try {
            FundRegistration fundIssueRegistration =new FundRegistration();
            
            fundIssueRegistration.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
                fundIssueRegistration.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                fundIssueRegistration.setTransaction(transaction);
                fundIssueRegistration.setClient(entryTransaction.getClient());
                fundIssueRegistration.setRefNo(entryTransaction.getReferenceNo());
                fundIssueRegistration.setAmount(entryTransaction.getTotalCreditAmount());
                fundIssueRegistration.setStatus("ACTIVE");
                
                getDatabaseService().save(fundIssueRegistration);
            
            
        } catch (Exception e) {
            
        }
    }
            
            
    private SystemPayment systemPayment;
    private CashierSession cashierSession;
}
