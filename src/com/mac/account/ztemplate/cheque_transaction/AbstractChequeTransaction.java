/*
 *  AbstractChequeTransaction.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 13, 2014, 12:31:32 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.account.ztemplate.cheque_transaction;

import com.mac.account.zobject.cheque.Cheque;
import com.mac.account.ztemplate.cheque_transaction.custom_object.ChequeTransactionObject;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.templates.grid_selection.AbstractGridSelection;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public abstract class AbstractChequeTransaction extends AbstractGridSelection<Cheque> {

    public AbstractChequeTransaction() {
    }

//    public abstract String executeText();
//    public abstract String getChequeNewStatus();
//    public abstract String getChequeOldStatus();
//    public abstract String getChequeType();
    protected abstract String getTransactionTypeCode();

    protected abstract String getReferenceGeneratorKey();

//    protected abstract String getCreditAccountSettingCode();
//    protected abstract String getDebitAccountSettingCode();
    protected abstract List<Cheque> getCheques(ChequeTransactionObject transactionObject);

    protected abstract void saveCheques(ChequeTransactionObject transactionObject, Collection<Cheque> cheques) throws DatabaseException;

    @Override
    protected Collection<Cheque> getLeftData() {
        initService();

        return serChequeTransaction.getLeftData();
    }

    @Override
    protected CTableModel<Cheque> getLeftTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Index No", new String[]{"indexNo"}),
            new CTableColumn("Account No", new String[]{"accountNo"}),
            new CTableColumn("Cheque No", new String[]{"chequeNo"}),
            new CTableColumn("Amount", new String[]{"Amount"}),
            new CTableColumn("Cheque Date", new String[]{"chequeDate"}),
            new CTableColumn("Client", new String[]{"client", "name"})
        });
    }

    @Override
    protected CTableModel<Cheque> getRightTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Index No", new String[]{"indexNo"}),
            new CTableColumn("Cheque No", new String[]{"chequeNo"}),
            new CTableColumn("Amount", new String[]{"Amount"}),
            new CTableColumn("Cheque Date", new String[]{"chequeDate"}),
            new CTableColumn("Client", new String[]{"client", "name"})
        });
    }

    @Override
    protected void execute(Collection<Cheque> clctn) {
        try {
            initService();

            //        Collection<Cheque> chlist = new ArrayList<>();
            //        for (Cheque cheque : clctn) {
            //            cheque.setStatus(getChequeNewStatus());
            //            chlist.add(cheque);
            //        }
            //        serChequeTransaction.execute(chlist);

            saveCheques(serChequeTransaction.getChequeTransactionObject(), clctn);

            mOptionPane.showMessageDialog(this, getTitle() + " is successful.", getTitle(), mOptionPane.INFORMATION_MESSAGE);
        } catch (DatabaseException ex) {
            Logger.getLogger(AbstractChequeTransaction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initService() {
        if (serChequeTransaction == null) {
            serChequeTransaction = new SERChequeTransaction(this) {
                @Override
                protected String getTransactionName() {
                    return AbstractChequeTransaction.this.getTitle();
                }

                @Override
                protected String getReferenceGeneratorKey() {
                    return AbstractChequeTransaction.this.getReferenceGeneratorKey();
                }

                @Override
                protected List<Cheque> getCheques(ChequeTransactionObject transactionObject) {
                    System.out.println("transactionObject_____"+transactionObject);
                    if(transactionObject !=null){
                    return AbstractChequeTransaction.this.getCheques(transactionObject);
                    
                    }
                    
                    
                    return null;
                }

          
            };
        }
    }
    private SERChequeTransaction serChequeTransaction;
}
