/*
 *  ChequeStatus.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 2:06:11 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.account.ztemplate.cheque_transaction;

/**
 *
 * @author mohan
 */
public class ChequeStatus {

    public static final String PENDING = "PENDING";
    public static final String DEPOSIT = "DEPOSIT";
    public static final String REALIZE = "REALIZE";
    public static final String RETURN = "RETURN";
    public static final String ISSUE_RETURN = "ISSUE_RETURN";
    
    
    public static final String ISSUE = "ISSUE";
    
    public static final String CANCEL = "CANCEL";
    
    public static final String ALL[] = {
        PENDING,
        DEPOSIT,
        REALIZE,
        RETURN,
        ISSUE,
        CANCEL
    };
}
