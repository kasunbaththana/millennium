/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.ztemplate.cheque_transaction;

import com.mac.account.zobject.cheque.BankAccount;
import com.mac.account.zobject.cheque.Cheque;
import com.mac.account.ztemplate.cheque_transaction.custom_object.ChequeTransactionObject;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.panel.dialog.object_creator_dialog.ObjectCreatorDialog;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SMTK
 */
public abstract class SERChequeTransaction extends AbstractService {

    public SERChequeTransaction(Component component) {
        super(component);
    }

//    protected abstract String getTransactionTypeCode();
    protected abstract String getTransactionName();
  

    protected abstract String getReferenceGeneratorKey();

//    protected abstract String getCreditAccountSettingCode();
//    protected abstract String getDebitAccountSettingCode();
    protected abstract List<Cheque> getCheques(ChequeTransactionObject transactionObject);

    public List<BankAccount> getBankAccounts() {
        try {
            return getDatabaseService().getCollection("FROM com.mac.account.zobject.cheque.BankAccount");
        } catch (DatabaseException ex) {
            Logger.getLogger(SERChequeTransaction.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    
    
    }

    public Collection<Cheque> getLeftData() {
        initObjectCreatorDialog();
        chequeTransactionObject = objectCreatorDialog.getNewObject();

        if (chequeTransactionObject == null) {
            TabFunctions.closeTab(getCPanel());
        }

        return getCheques(chequeTransactionObject);

//        List list;
//        try {
//            HashMap params = new HashMap<>();
//            params.put("type", type);
//            params.put("status", status);
//            list = getDatabaseService().getCollection("from com.mac.account.zobject.cheque.Cheque where status=:status and type=:type", params);
//        } catch (DatabaseException ex) {
//            list = new ArrayList();
//            Logger.getLogger(AbstractChequeTransaction.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return list;
    }

//    public void execute(Collection<Cheque> clctn) {
//        try {
//            initObjectCreatorDialog();
//
////            Transaction transaction = (Transaction) objectCreatorDialog.getNewObject();
//
//            //TRANSACTION
////            int transactionIndex = SystemTransactions.insertTransaction(
////                    getDatabaseService(),
////                    getTransactionTypeCode(),
////                    transaction.getReferenceNo(),
////                    transaction.getDocumentNo(),
////                    null,
////                    cashierSession.getIndexNo(),
////                    null,
////                    transaction.getNote());
//
//            //CHEQUES
//            getDatabaseService().beginLocalTransaction();
//            for (Cheque cheque : clctn) {
//                getDatabaseService().save(cheque);
//            }
//            getDatabaseService().commitLocalTransaction();
//
//            //ACCOUNT TRANSACTION
////            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
////            accountTransaction.beginAccountTransactionQueue();
////            for (Cheque cheque : clctn) {
////                accountTransaction.addAccountTransactionQueue(getCreditAccountSettingCode(), getTransactionName() + " - " + cheque.getChequeNo(), cheque.getAmount(), AccountTransactionType.AUTO);
////                accountTransaction.addAccountTransactionQueue(getDebitAccountSettingCode(), getTransactionName() + " - " + cheque.getChequeNo(), cheque.getAmount(), AccountTransactionType.AUTO);
////            }
////            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, getTransactionTypeCode());
//
//
//
//
//        } catch ( //                ApplicationException | 
//                DatabaseException ex) {
//            Logger.getLogger(SERChequeTransaction.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    private void initObjectCreatorDialog() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (objectCreatorDialog == null) {
                    PCChequeTransactionInformation transactionInformation = new PCChequeTransactionInformation() {
                        @Override
                        protected String getReferenceGeneratorType() {
                            return SERChequeTransaction.this.getReferenceGeneratorKey();
                        }

                        @Override
                        protected List<BankAccount> getBankAccounts() {
                            return SERChequeTransaction.this.getBankAccounts();
                        }
                       
                    };

                    objectCreatorDialog = new ObjectCreatorDialog<>();
                    objectCreatorDialog.setObjectCreator(transactionInformation);
                    objectCreatorDialog.setTitle(getTransactionName());
                    
                }
            }
        };
        CApplication.invokeEventDispatch(runnable);
    }

    public ChequeTransactionObject getChequeTransactionObject() {
        return chequeTransactionObject;
    }
    private ObjectCreatorDialog<ChequeTransactionObject> objectCreatorDialog;
    private ChequeTransactionObject chequeTransactionObject;
}
