/*
 *  ChequeTransactionObject.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 4, 2014, 3:19:37 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.account.ztemplate.cheque_transaction.custom_object;

import com.mac.account.zobject.cheque.BankAccount;
import java.util.Date;

/**
 *
 * @author mohan
 */
public class ChequeTransactionObject {

    private BankAccount bankAccount;
    private Date accountDate;
    //TRANSACTION
    private Date transactionDate;
    private String referenceNo;
    private String documentNo;
    private String note;

    public ChequeTransactionObject() {
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Date getAccountDate() {
        return accountDate;
    }

    public void setAccountDate(Date accountDate) {
        this.accountDate = accountDate;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
