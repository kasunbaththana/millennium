/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.general_voucher_2;

import com.mac.account.ztemplate.entry_payment.EntryPayment;
import com.mac.af.component.base.button.action.Action;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;

/**
 *
 * @author mohan
 */
public class GeneralVoucher extends EntryPayment {

    public GeneralVoucher() {
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.GENERAL_VOUCHER_2_TRANSACTION_CODE);
        this.recentButton.setDatabseService(getDatabaseService());
    }

    @Override
    protected String getChequeType() {
        return ChequeType.COMPANY;
    }

    @Override
    protected String referenceGeneratorPrefix() {
        return ReferenceGenerator.GENERAL_VOUCHER;
    }

    @Override
    public String getTransactionName() {
        return "Petty Cash Voucher";
    }

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.GENERAL_VOUCHER_2_TRANSACTION_CODE;
    }

    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
    }
    private RecentButton recentButton;
}
