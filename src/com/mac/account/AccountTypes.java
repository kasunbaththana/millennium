/*
 *  AccountTypes.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 18, 2014, 3:30:19 PM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.account;

/**
 *
 * @author user
 */
public class AccountTypes {

    public static final String BALANCE_SHEET = "BALANCE_SHEET";
    public static final String PROFIT_AND_LOSS = "PROFIT_AND_LOSS";
    public static final String[] ACCOUNT_TYPES = {
        BALANCE_SHEET,
        PROFIT_AND_LOSS
    };
}
