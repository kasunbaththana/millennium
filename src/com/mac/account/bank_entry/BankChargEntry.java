/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.bank_entry;

import com.mac.account.ztemplate.account_entry.AccountEntry;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;

/**
 *
 * @author KASUN
 */
public class BankChargEntry extends AccountEntry {

    @Override
    protected String referenceGeneratorPrefix() {
        return REFERENCE_GENARATOR_TYPE;
    }

    @Override
    public String getTransactionName() {
        return TRANSACTION_TYPE;
    }

    @Override
    public String getTransactionTypeCode() {
        return TRANSACTION_TYPE_CODE;
    }
    private static final String REFERENCE_GENARATOR_TYPE = com.mac.zsystem.util.generator.ReferenceGenerator.BANK_ENTRY;
    private static final String TRANSACTION_TYPE = "Bank Entry";
    private static final String TRANSACTION_TYPE_CODE = SystemTransactions.BANK_ENTRY_TRANSACTION_CODE;

    @Override
    protected String getAccountTransactionTypeCode() {
        return AccountTransactionType.BANK_CHARG_ENTRY;
    }

    @Override
    protected boolean isAccountCreditDebitEqualNeeded() {
        return true;
    }
    
}
