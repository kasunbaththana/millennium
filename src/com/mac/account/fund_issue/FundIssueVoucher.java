/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.fund_issue;

import com.mac.account.ztemplate.fund_payment.EntryPayment;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;

/**
 *
 * @author kasun
 */
public class FundIssueVoucher extends EntryPayment {

    @Override
    protected String getChequeType() {
        return ChequeType.COMPANY;
    }

    @Override
    protected String referenceGeneratorPrefix() {
        return ReferenceGenerator.FUND_ISSUE_VOUCHER;
    }

    @Override
    public String getTransactionName() {
        return "Fund Issue Voucher";
    }

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.FUND_ISSUE_VOUCHER_TRANSACTION_CODE;
    }
    
    
    
    
}
