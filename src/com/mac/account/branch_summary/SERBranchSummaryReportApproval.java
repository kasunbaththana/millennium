/*
 *  SERLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.account.branch_summary;

import com.mac.loan.loan_approval.*;
import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author user
 */
public class SERBranchSummaryReportApproval extends AbstractService  implements SummaryApprovalServiceInterface {

    public SERBranchSummaryReportApproval(Component component) {
        super(component);
    }

    @Override
    public boolean acceptApplication(com.mac.account.branch_summary.object.BranchSummaryReport loan) {
       
        try {
            loan.setStatus("APPROVE");
            getDatabaseService().update(loan);
            
          
            mOptionPane.showMessageDialog(null, "Loan accepted successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(SERStartedLoanApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to accept loan application.", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

   
    @Override
        public List<com.mac.account.branch_summary.object.BranchSummaryReport> getLoans() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status", "PENDING");
            list = getDatabaseService().getCollection("from com.mac.account.branch_summary.object.BranchSummaryReport where status=:status", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(LoanApplicationApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public static final String TITLE = "Loan Approval";
}
