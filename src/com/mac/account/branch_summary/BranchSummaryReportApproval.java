
package com.mac.account.branch_summary;

import com.mac.af.component.base.button.action.Action;
import java.util.Collection;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.transaction.SystemTransactions;

/**
 *
 * @author kasun
 */
public class BranchSummaryReportApproval extends AbstractGridObject {
 
    public BranchSummaryReportApproval() {
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE);
        this.recentButton.setDatabseService(getDatabaseService());
        
        
    }

    protected int loanindex;
    
    
    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
        
    }   
    
    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Date", new String[]{"transactionDate"}),
            new CTableColumn("Opening Cash", new String[]{"openingCash"}),
            new CTableColumn("Total Received", new String[]{"totalReceived"}),
            new CTableColumn("Total Collection", new String[]{"totalCollection"}),
            new CTableColumn("Total Payment", new String[]{"totalPayment"}),
            new CTableColumn("Day End Total", new String[]{"dayEndCash"})
        });
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        return new PCSummaryApproval(getService());
    }

    @Override
    protected Collection getTableData() {
        return getService().getLoans();
        
    }

    private SummaryApprovalServiceInterface getService() {
        if (loanApproval == null) {
            loanApproval = initService();
        }

        return loanApproval;
    }

    public SummaryApprovalServiceInterface initService() {
        return new SERBranchSummaryReportApproval(this);
    }
    
    
    

    private RecentButton recentButton;    
    private SummaryApprovalServiceInterface loanApproval;
}
