/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.branch_summary;

import com.mac.account.branch_summary.object.BankDetails;
import com.mac.account.branch_summary.object.PaymentCash;
import com.mac.account.branch_summary.object.ReceivedCash;
import com.mac.account.branch_summary.object2.BranchSummaryBankDetails;
import com.mac.account.branch_summary.object2.BranchSummaryPaymentCash;
import com.mac.account.branch_summary.object2.BranchSummaryReceivedCash;
import com.mac.account.cash_denomination.*;
import com.mac.account.cashier_closing.*;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.zsystem.session_button.SessionButton;
import com.mac.zsystem.transaction.account.object.AccountTransaction;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author chathu
 */
public class BranchSummaryReport extends AbstractService {

    public BranchSummaryReport(Component component){
       super(component);
        initOthers();
    }
    
    private void initOthers() {
         this.sessionButton = new SessionButton();
        System.out.println("getTransactionTypeCode()____"+SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE);
        this.sessionButton.setTransactionType(SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE);
        try {
            this.sessionButton.setDatabseService(getDatabaseService());
        } catch (DatabaseException ex) {
            Logger.getLogger(CashDenonimation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    


    public Class getObjectClass() {
        return com.mac.zsystem.transaction.cashier.object.CashierClosing.class;
    }

   
    public AbstractObjectCreator getObjectCreator() {
        return new PCCashierClosing() {
            @Override
            protected CashierSession getCashierSession() {
                try {
                   
                        return SystemCashier.getCurrentCashierSession(getDatabaseService());
                
                } catch (DatabaseException ex) {
                    Logger.getLogger(CashierClosing.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                } catch (ApplicationException ex) {
                    Logger.getLogger(CashDenonimation.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        };
    }

 
    public CTableModel getTableModel() {
      CTableModel cTableModel = new CTableModel(new CTableColumn[]{
            new CTableColumn("Description","description"),
            new CTableColumn("Amount", "amount"),
        });
        return (cTableModel);
    }
    public CTableModel getBankTableModel() {
      CTableModel cTableModel = new CTableModel(new CTableColumn[]{
            new CTableColumn("Bank Name","bankName"),
            new CTableColumn("Opening Bal", "openingBal"),
            new CTableColumn("Direct Bank", "directBank"),
            new CTableColumn("Balance", "balance"),
        });
        return (cTableModel);
    }
    
    
 
    public List<ReceivedCash> getDetails(Date date){
       
        
        String sql="SELECT \n" +
                    "a.transaction_type AS description,SUM(a.debit_amount) AS amount,0 as sum_id\n" +
                    "\n" +
                    "FROM `account_transaction` AS a \n" +
                    "WHERE a.`account`='11-200' AND a.`status`='ACTIVE'  \n" +
                    "AND a.transaction_date = '"+date+"' AND a.`debit_amount` > 0\n" +
                    "AND a.`transaction_type` <> 'RECEIPT'\n" +
                    "AND a.`branch`='"+CApplication.getSessionVariable(CApplication.STORE_ID)+"' \n"+
                    "GROUP BY a.`transaction_type`\n" +
                    "\n" +
                    "UNION ALL\n" +
                    "\n" +
                    "SELECT 'DOCUMENT CHARGE' AS description,SUM(h.`settlement_amount`)AS debit_amount,0 as sum_id  \n" +
                    "FROM `transaction` t\n" +
                    "INNER JOIN `settlement_history` h   ON h.transaction = t.index_no\n" +
                    "INNER JOIN settlement st ON h.settlement=st.index_no   \n" +
                    "WHERE st.description LIKE '%DOCUMENT CHARGE%' AND t.transaction_type='RECEIPT' AND t.status<>'CANCEL'\n" +
                    "AND t.transaction_date = '"+date+"'\n" +
                    "AND t.`branch`='"+CApplication.getSessionVariable(CApplication.STORE_ID)+"' \n"+
                    "GROUP BY t.transaction_type\n" +
                    "\n" +
                    "UNION ALL\n" +
                    "\n" +
                    "SELECT 'RECEIPT COLLECTION' AS description,SUM(h.`settlement_amount`)AS debit_amount,0 as sum_id  \n" +
                    "FROM `transaction` t\n" +
                    "INNER JOIN `settlement_history` h   ON h.transaction = t.index_no\n" +
                    "INNER JOIN settlement st ON h.settlement=st.index_no   \n" +
                    "WHERE st.description NOT LIKE '%DOCUMENT CHARGE%' AND t.transaction_type='RECEIPT' AND t.status<>'CANCEL'\n" +
                    "AND t.transaction_date = '"+date+"' AND st.settlement_type NOT IN ('LOAN_AMOUNT')\n" +
                    "AND t.`branch`='"+CApplication.getSessionVariable(CApplication.STORE_ID)+"' \n"+
                    "GROUP BY t.transaction_type";
        try {
     
            HibernateSQLQuery hibernateSQLQuery =new HibernateSQLQuery(sql, ReceivedCash.class);
       
            return getDatabaseService().getCollectionSQLQuery(hibernateSQLQuery,null);
        } catch (DatabaseException ex) {
            Logger.getLogger(CashDenonimation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return new ArrayList<>();
    }
    public List<PaymentCash> getPaymentDetails(Date date){
        
        try {
            
            String sql="select \n" +
                        "account_transaction.transaction_type AS description,sum(account_transaction.credit_amount) as amount,0 as sum_id\n" +
                        "\n" +
                        "from account_transaction \n" +
                        "where account_transaction.account='11-200' and account_transaction.status='ACTIVE'  \n" +
                        "and account_transaction.transaction_date = '"+date+"' and account_transaction.credit_amount > 0\n" +
                        "and account_transaction.branch='"+CApplication.getSessionVariable(CApplication.STORE_ID)+"' and account_transaction.transaction_type<>'CASHIER_CLOSE'\n" +
                        "group by account_transaction.transaction_type";
         
            HibernateSQLQuery hibernateQuery =new HibernateSQLQuery(sql,PaymentCash.class);

       
           
            return getDatabaseService().getCollectionSQLQuery(hibernateQuery,null);
            
        } catch (Exception e) {
        
        e.printStackTrace();
        }
        
        return new ArrayList<>();
    }
     public List<BankDetails> getBankDetails(Date date){
        
        try {
            
            String sql="SELECT  a.`name` as bank_name,SUM(t.`debit_amount`)-SUM(t.`credit_amount`) AS direct_bank,IFNULL(o.ope_amount,0.0) AS opening_bal,\n" +
                        "(SUM(t.`debit_amount`)-SUM(t.`credit_amount`))+IFNULL(o.ope_amount,0.0) AS balance,0.0 as return_charges,0.0 as transfer,0 as sum_id\n" +
                        "FROM `bank_account` b \n" +
                        "INNER JOIN `account` a ON b.`value_account`=a.`code`\n" +
                        "INNER JOIN `account_transaction` t ON t.`account` = a.`code`\n" +
                        "LEFT JOIN (SELECT SUM(t1.`debit_amount`)-SUM(t1.`credit_amount`) AS ope_amount,t1.account \n" +
                        "		FROM `account_transaction` t1 WHERE t1.`transaction_date`<'"+date+"' AND t1.`status`<>'CANCEL' \n" +
                        "		AND t1.branch='"+CApplication.getSessionVariable(CApplication.STORE_ID)+"' GROUP BY t1.account )\n" +
                        "		AS o ON o.account = a.code\n" +
                        "WHERE t.`status`<>'CANCEL' AND t.`transaction_date` = '"+date+"'  AND t.branch='"+CApplication.getSessionVariable(CApplication.STORE_ID)+"'\n" +
                        "GROUP BY t.`account`";
         
            HibernateSQLQuery hibernateQuery =new HibernateSQLQuery(sql,BankDetails.class);

       
           
            return getDatabaseService().getCollectionSQLQuery(hibernateQuery,null);
            
        } catch (Exception e) {
        
        e.printStackTrace();
        }
        
        return new ArrayList<>();
    }
    
    public double getOpeningCash(Date date){
        try {
            
            double cr = (double) (getDatabaseService().initCriteria(AccountTransaction.class)
                    .add(Restrictions.eq("account.code", "11-210"))
                    .add(Restrictions.eq("status", "ACTIVE"))
                    .add(Restrictions.lt("transactionDate", date))
                    .setProjection(Projections.sum("creditAmount")).uniqueResult());
            
            double dr = (double) (getDatabaseService().initCriteria(AccountTransaction.class)
                    .add(Restrictions.eq("account.code", "11-210"))
                    .add(Restrictions.eq("status", "ACTIVE"))
                    .add(Restrictions.lt("transactionDate", date))
                    .setProjection(Projections.sum("debitAmount")).uniqueResult());
      
         return (dr-cr);   
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0.0;
    }

        public String checkIsAvailable(Date date){
            
            try {
                String cr = (String) (getDatabaseService().initCriteria(com.mac.account.branch_summary.object.BranchSummaryReport.class)
                    .add(Restrictions.eq("transactionDate", date))
                    .setProjection(Projections.property("status")).uniqueResult());
               return cr; 
            } catch (Exception e) {
            }
            return "";
        } 


    protected void save(List<PaymentCash> pcs,List<ReceivedCash> cashs,List<BankDetails> bds,com.mac.account.branch_summary.object.BranchSummaryReport bsr)   {
       
        try {
           
            com.mac.account.branch_summary.object.BranchSummaryReport index =  (com.mac.account.branch_summary.object.BranchSummaryReport) getDatabaseService().save(bsr);
            
            for(PaymentCash cash:pcs){
                BranchSummaryPaymentCash cash1=new BranchSummaryPaymentCash();
                cash1.setDescription(cash.getDescription());
                cash1.setAmount(cash.getAmount());
                cash1.setSumId(index.getIndexNo());
                getDatabaseService().save(cash1);
            }
            for(ReceivedCash rc:cashs){
                BranchSummaryReceivedCash rc1=new BranchSummaryReceivedCash();
                rc1.setDescription(rc.getDescription());
                rc1.setAmount(rc.getAmount());
                rc1.setSumId(index.getIndexNo());
                getDatabaseService().save(rc1);
            }
            for(BankDetails bd:bds){
                BranchSummaryBankDetails bd1=new BranchSummaryBankDetails();
                bd1.setBankName(bd.getBankName());
                bd1.setOpeningBal(bd.getOpeningBal());
                bd1.setSumId(index.getIndexNo());
                bd1.setDirectBank(bd.getDirectBank());
                bd1.setReturnCharges(bd.getReturnCharges());
                bd1.setBalance(bd.getBalance());
                bd1.setTransfer(bd.getTransfer());
                getDatabaseService().save(bd1);
            }
            
            
          mOptionPane.showMessageDialog(null, "Process Success !", "Process", mOptionPane.INFORMATION_MESSAGE);   
        } catch (Exception e) {
            e.printStackTrace();
            mOptionPane.showMessageDialog(null, e.getMessage(), "Error", mOptionPane.ERROR_MESSAGE); 
        }
        
        

        
    }


    @Action
    public void doPrint() {
        this.sessionButton.doClick();
    }
    
     private SessionButton sessionButton;
    
    
}
