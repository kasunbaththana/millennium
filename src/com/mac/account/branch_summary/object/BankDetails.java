package com.mac.account.branch_summary.object;
// Generated Jul 27, 2021 12:07:57 PM by Hibernate Tools 3.2.1.GA



/**
 * BankDetails generated by hbm2java
 */
public class BankDetails  implements java.io.Serializable {


     private String bankName;
     private Double openingBal;
     private Double directBank;
     private Double returnCharges;
     private Double transfer;
     private Double balance;
     private Integer sumId;

    public BankDetails() {
    }

	
    public BankDetails(String bankName) {
        this.bankName = bankName;
    }
    public BankDetails(String bankName, Double openingBal, Double directBank, Double returnCharges, Double transfer, Double balance, Integer sumId) {
       this.bankName = bankName;
       this.openingBal = openingBal;
       this.directBank = directBank;
       this.returnCharges = returnCharges;
       this.transfer = transfer;
       this.balance = balance;
       this.sumId = sumId;
    }
   
    public String getBankName() {
        return this.bankName;
    }
    
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    public Double getOpeningBal() {
        return this.openingBal;
    }
    
    public void setOpeningBal(Double openingBal) {
        this.openingBal = openingBal;
    }
    public Double getDirectBank() {
        return this.directBank;
    }
    
    public void setDirectBank(Double directBank) {
        this.directBank = directBank;
    }
    public Double getReturnCharges() {
        return this.returnCharges;
    }
    
    public void setReturnCharges(Double returnCharges) {
        this.returnCharges = returnCharges;
    }
    public Double getTransfer() {
        return this.transfer;
    }
    
    public void setTransfer(Double transfer) {
        this.transfer = transfer;
    }
    public Double getBalance() {
        return this.balance;
    }
    
    public void setBalance(Double balance) {
        this.balance = balance;
    }
    public Integer getSumId() {
        return this.sumId;
    }
    
    public void setSumId(Integer sumId) {
        this.sumId = sumId;
    }




}


