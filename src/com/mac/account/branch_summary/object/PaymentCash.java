package com.mac.account.branch_summary.object;
// Generated Jul 27, 2021 12:07:57 PM by Hibernate Tools 3.2.1.GA



/**
 * PaymentCash generated by hbm2java
 */
public class PaymentCash  implements java.io.Serializable {


     private String description;
     private Double amount;
     private Integer sumId;

    public PaymentCash() {
    }

	
    public PaymentCash(String description) {
        this.description = description;
    }
    public PaymentCash(String description, Double amount, Integer sumId) {
       this.description = description;
       this.amount = amount;
       this.sumId = sumId;
    }
   
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public Double getAmount() {
        return this.amount;
    }
    
    public void setAmount(Double amount) {
        this.amount = amount;
    }
    public Integer getSumId() {
        return this.sumId;
    }
    
    public void setSumId(Integer sumId) {
        this.sumId = sumId;
    }




}


