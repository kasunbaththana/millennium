/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.branch_summary;

import com.mac.account.branch_summary.object.BankDetails;
import com.mac.account.branch_summary.object.PaymentCash;
import com.mac.account.branch_summary.object.ReceivedCash;
import com.mac.account.cash_denomination.*;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kasun
 */
public class BranchSummaryPanel extends CPanel {

    /**
     * Creates new form ClientInformationPanel
     */
    public BranchSummaryPanel() {
       
        
        initComponents();
        initOthers();
    }



   

    @SuppressWarnings("unchecked")
    private void initOthers() {
        
        cashierSession = getCashierSession();
        branchSummaryReport = new BranchSummaryReport(this);
        
        txtCashReceived.setEnabled(false);
        txtPrvCashInHand.setEnabled(false);
        txttotalCollection.setEnabled(false);
        txttotalPayment.setEnabled(false);
        txtbranch.setEnabled(false);
        txtEndCashInHand.setEnabled(false);
        
       txtbranch.setCValue(CApplication.getSessionVariable(CApplication.STORE_ID).toString());
   
        tbl_cash_received.setCModel(branchSummaryReport.getTableModel());
        tbl_cah_payment.setCModel(branchSummaryReport.getTableModel());
        tbl_bank_details.setCModel(branchSummaryReport.getBankTableModel());
 
        tbl_cash_received.setCValue(branchSummaryReport.getDetails(txtDate.getCValue()));
        tbl_cah_payment.setCValue(branchSummaryReport.getPaymentDetails(txtDate.getCValue()));
        txtPrvCashInHand.setCValue(branchSummaryReport.getOpeningCash(txtDate.getCValue()));
        tbl_bank_details.setCValue(branchSummaryReport.getBankDetails(txtDate.getCValue()));
//        
//        txtDate.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//            tbl_cash_received.setCValue(branchSummaryReport.getDetails(txtDate.getCValue()));  
//            tbl_cah_payment.setCValue(branchSummaryReport.getPaymentDetails(txtDate.getCValue()));
//            }
//        });
        jButton1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                txtPrvCashInHand.setCValue(branchSummaryReport.getOpeningCash(txtDate.getCValue()));
               tbl_cash_received.setCValue(branchSummaryReport.getDetails(txtDate.getCValue()));  
            tbl_cah_payment.setCValue(branchSummaryReport.getPaymentDetails(txtDate.getCValue()));
            tbl_bank_details.setCValue(branchSummaryReport.getBankDetails(txtDate.getCValue()));
            setTotal();
            }
        });
        
        btn_process.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                
               saveTransaction();
            }
        });
    
        
    }
    
   
   
private CashierSession cashierSession;

public CashierSession getCashierSession(){
        try {
          return  SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException ex) {
            Logger.getLogger(CashDenominationPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}

public void setTotal(){
    
    List<ReceivedCash> cashs = (List<ReceivedCash>) tbl_cash_received.getCValue();
    double received_cash = 0.0;
    double prv_cash_in_hand= txtPrvCashInHand.getCValue();
    for(ReceivedCash cash:cashs){
        received_cash += cash.getAmount();
    }
    txtCashReceived.setCValue(received_cash);
    txttotalCollection.setCValue(received_cash+prv_cash_in_hand);
    
    // cash payment
    
    List<PaymentCash> pcs=(List<PaymentCash>) tbl_cah_payment.getCValue();
    double payment_cahs=0.0;
    for(PaymentCash cash:pcs){
        payment_cahs+=cash.getAmount();
    }
    txttotalPayment.setCValue(payment_cahs);
    
    txtEndCashInHand.setCValue((received_cash+prv_cash_in_hand)-payment_cahs);
    
    
     if(branchSummaryReport.checkIsAvailable(txtDate.getCValue()).equals("APPROVE")){
                    
        mOptionPane.showMessageDialog(null, "APPROVED !", "", mOptionPane.YES_OPTION);   
        btn_process.setEnabled(false);
       }else if(branchSummaryReport.checkIsAvailable(txtDate.getCValue()).equals("PENDING")){
        btn_process.setEnabled(false);            
        mOptionPane.showMessageDialog(null, "APPROVE NOT YET!", "", mOptionPane.INFORMATION_MESSAGE);             
      }
    
    
}
public void saveTransaction(){
    double prv_cash_in_hand= txtPrvCashInHand.getCValue();
    double day_cash_in_hand= txtEndCashInHand.getCValue();
    double txt_cash_received= txtCashReceived.getCValue();
    double txt_total_collection= txttotalCollection.getCValue();
    double txt_total_payment= txttotalPayment.getCValue();
    List<PaymentCash> pcs=(List<PaymentCash>) tbl_cah_payment.getCValue();
    List<ReceivedCash> cashs = (List<ReceivedCash>) tbl_cash_received.getCValue();
    List<BankDetails> bds = (List<BankDetails>) tbl_bank_details.getCValue();
    
    com.mac.account.branch_summary.object.BranchSummaryReport bsr=new com.mac.account.branch_summary.object.BranchSummaryReport();
    bsr.setBranch(CApplication.getSessionVariable(CApplication.STORE_ID).toString());
    bsr.setTransactionDate(txtDate.getCValue());
    bsr.setOpeningCash(prv_cash_in_hand);
    bsr.setDayEndCash(day_cash_in_hand);
    bsr.setTotalReceived(txt_cash_received);
    bsr.setTotalCollection(txt_total_collection);
    bsr.setTotalPayment(txt_total_payment);
    bsr.setStatus("PENDING");
    
    branchSummaryReport.save(pcs,cashs,bds,bsr);
}
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtbranch = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel21 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel1 = new com.mac.af.component.base.label.CLabel();
        cLabel22 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cLabel23 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPrvCashInHand = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jPanel2 = new javax.swing.JPanel();
        cLabel28 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCashReceived = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel29 = new com.mac.af.component.derived.display.label.CDLabel();
        txttotalCollection = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_cash_received = new com.mac.af.component.derived.input.table.CITable();
        jPanel3 = new javax.swing.JPanel();
        cLabel30 = new com.mac.af.component.derived.display.label.CDLabel();
        txttotalPayment = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_cah_payment = new com.mac.af.component.derived.input.table.CITable();
        jButton1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_bank_details = new com.mac.af.component.derived.input.table.CITable();
        cLabel24 = new com.mac.af.component.derived.display.label.CDLabel();
        txtEndCashInHand = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        btn_process = new com.mac.af.component.base.button.button.CButton();

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Branch Daily Summary");

        cLabel21.setText("Branch");

        cLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cLabel1.setText("Millennium Micro Credit (Private) Limited\t\t\t\t\t\t ");
        cLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N

        cLabel22.setText("Date");

        cLabel23.setText("Cash In Hand\t ");
        cLabel23.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        txtPrvCashInHand.setForeground(new java.awt.Color(0, 153, 153));
        txtPrvCashInHand.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cash Received", 0, 0, new java.awt.Font("Agency FB", 1, 14))); // NOI18N
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));

        cLabel28.setText("Total Received");
        cLabel28.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        txtCashReceived.setText("0.0");
        txtCashReceived.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        cLabel29.setText("Total Collection");
        cLabel29.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        txttotalCollection.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        tbl_cash_received.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tbl_cash_received);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txttotalCollection, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                            .addComponent(txtCashReceived, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCashReceived, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttotalCollection, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cash Payment", 0, 0, new java.awt.Font("Agency FB", 1, 14))); // NOI18N

        cLabel30.setText("Total Payment");
        cLabel30.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        txttotalPayment.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        tbl_cah_payment.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tbl_cah_payment);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(cLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotalPayment, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttotalPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.setText("Refresh");

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Bank Details"));

        tbl_bank_details.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tbl_bank_details);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 163, Short.MAX_VALUE)
                .addContainerGap())
        );

        cLabel24.setText("Day End Cash In Hand");
        cLabel24.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        txtEndCashInHand.setForeground(new java.awt.Color(0, 153, 0));
        txtEndCashInHand.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        btn_process.setText("Process");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1028, Short.MAX_VALUE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtbranch, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                                            .addComponent(txtDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(cLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtPrvCashInHand, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jButton1))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(346, 346, 346)
                                        .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtEndCashInHand, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(btn_process, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtbranch, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPrvCashInHand, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEndCashInHand, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_process, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.base.button.button.CButton btn_process;
    private com.mac.af.component.base.label.CLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel21;
    private com.mac.af.component.derived.display.label.CDLabel cLabel22;
    private com.mac.af.component.derived.display.label.CDLabel cLabel23;
    private com.mac.af.component.derived.display.label.CDLabel cLabel24;
    private com.mac.af.component.derived.display.label.CDLabel cLabel28;
    private com.mac.af.component.derived.display.label.CDLabel cLabel29;
    private com.mac.af.component.derived.display.label.CDLabel cLabel30;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private com.mac.af.component.derived.input.table.CITable tbl_bank_details;
    private com.mac.af.component.derived.input.table.CITable tbl_cah_payment;
    private com.mac.af.component.derived.input.table.CITable tbl_cash_received;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtCashReceived;
    private com.mac.af.component.derived.input.textfield.CIDateField txtDate;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtEndCashInHand;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPrvCashInHand;
    private com.mac.af.component.derived.input.textfield.CIStringField txtbranch;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txttotalCollection;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txttotalPayment;
    // End of variables declaration//GEN-END:variables

    private BranchSummaryReport branchSummaryReport;
}
