/*
 *  LoanApproval.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 3, 2014, 8:43:23 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.account.branch_summary;


import java.util.List;

/**
 *
 * @author mohan
 */
public interface SummaryApprovalServiceInterface {

    public boolean acceptApplication(com.mac.account.branch_summary.object.BranchSummaryReport loan);


    public List<com.mac.account.branch_summary.object.BranchSummaryReport> getLoans();
}
