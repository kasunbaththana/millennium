/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.fund_registration;

import com.mac.account.ztemplate.fund_payment.EntryPayment;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;

/**
 *
 * @author kasun
 */
public class FundRgistration extends EntryPayment {

    @Override
    protected String getChequeType() {
        return ChequeType.CLIENT;
    }

    @Override
    protected String referenceGeneratorPrefix() {
        return ReferenceGenerator.FUND_REGISTRATION;
    }

    @Override
    public String getTransactionName() {
        return "Fund Registration";
    }

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.FUND_REGISTRATION_TRANSACTION_CODE;
    }

    @Override
    public String getAccountEntryType() {
        return ACCOUNT_ENTRY_CREDIT_ONLY;
    }
}
