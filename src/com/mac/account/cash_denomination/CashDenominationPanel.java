/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.cash_denomination;

import com.mac.account.cash_denomination.object.Denomination;
import static com.mac.account.general_voucher.SERGeneralVoucherApproval.TITLE;
import com.mac.af.core.ApplicationException;
import com.mac.dash_board.client.*;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.dash_board.client.object2.Client;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.cashier.CashierSessionStatus;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierCloseDetails;
import com.mac.zsystem.transaction.cashier.object.CashierClosing;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kasun
 */
public class CashDenominationPanel extends CPanel {

    /**
     * Creates new form ClientInformationPanel
     */
    public CashDenominationPanel() {
       
        
        initComponents();
        initOthers();
    }



   

    @SuppressWarnings("unchecked")
    private void initOthers() {
        unEditableComponent();initCashierSessionInfo();
        cashierSession = getCashierSession();
        cashDenonimation = new CashDenonimation(this);
       
        tblDetails.setCModel(cashDenonimation.getTableModel());
 
        ReferenceGenerator generator=new ReferenceGenerator(ReferenceGenerator.CASHIER_CLOSE);
        txtReferenceNo.setCValue(generator.getValue());
        
        txtEmployee.setCValue(cashierSession.getEmployee().getName());
        btnsave.setEnabled(false);
        txtReferenceNo.setEditable(false);
        txtEmployee.setEditable(false);
        btnRefresh.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                resetForm();
               tblDetails.setCValue(cashDenonimation.getDetails(txtTransactionDate.getCValue()));
               setDataTotal(txtTransactionDate.getCValue());
            }
        });
    
       btncalculate.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                calculateExcess();
            }
        });
       
        btnsave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              save();
            }
        });
        
        MouseAdapter adapter=new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
                
                Denomination d=(Denomination) tblDetails.getSelectedValue();
                viewSelectedDetails(d.getCenter(),txtTransactionDate.getCValue());
            }
        };   
        tblDetails.addMouseListener(adapter);
    }
    
    public void save(){
        
        double ext =txtExcessShort.getCValue();
        
        if((int)ext==0){
         
        CashierClosing cashierClosing = new CashierClosing();
        cashierClosing.setReferenceNo(txtReferenceNo.getCValue());
        cashierClosing.setReferenceNo(txtDocumentNo.getCValue());
        cashierClosing.setTransactionDate(txtTransactionDate.getCValue());
        cashierClosing.setCashierPoint(cashierSession.getCashierPoint());
        cashierClosing.setEmployee(cashierSession.getEmployee());
        cashierClosing.setCashierSession(cashierSession);
        cashierClosing.setNote5000(txtTotalNote5000.getCValue());
        cashierClosing.setNote2000(txtTotalNote2000.getCValue());
        cashierClosing.setNote1000(txtTotalNote1000.getCValue());
        cashierClosing.setNote500(txtTotalNote500.getCValue());
        cashierClosing.setNote100(txtTotalNote100.getCValue());
        cashierClosing.setNote50(txtTotalNote50.getCValue());
        cashierClosing.setNote20(txtTotalNote20.getCValue());
        cashierClosing.setNote10(txtTotalNote10.getCValue());
        cashierClosing.setTotalNote(txtTotalNote.getCValue());
        cashierClosing.setOther(txtTotalOther.getCValue());
        cashierClosing.setTotalCash(txtTotal.getCValue());
        
        cashierClosing.setTotalCollection(txtTotalCollection.getCValue());
        cashierClosing.setAlLoan(txtAlLoan.getCValue());
        cashierClosing.setQlLoan(txtQlLoan1.getCValue());
        cashierClosing.setCashInHand(txtCashInHand.getCValue());
        cashierClosing.setExcessShort(txtExcessShort.getCValue());
        cashierClosing.setRepayment(txtRepayment.getCValue());
        cashierClosing.setDocumentCharge(txtDocumentCharge.getCValue());
        cashierClosing.setAdvanceRental(txtAdvanceRental.getCValue());
        cashierClosing.setDefaultCharges(txtDefaultCharge.getCValue());
        cashierClosing.setLatePayment(txtLatePayment.getCValue());
        try {
            Integer transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE,
                txtReferenceNo.getCValue(),
                txtDocumentNo.getCValue(),
                null,
                cashierSession.getIndexNo(),//cashier session ID
                null,
                "DENOMINATION");
            cashierClosing = (CashierClosing) getDatabaseService().save(cashierClosing);
       
        
                List<Denomination> denomination = (List<Denomination>) tblDetails.getCValue();

                for(Denomination d:denomination){
                    CashierCloseDetails d1=new CashierCloseDetails();
                    d1.setCenter(d.getCenter());
                    d1.setCashierClose(cashierClosing.getIndexNo());
                    d1.setRepaymentCollection(d.getRepaymentCollection());
                    d1.setDocumentCharge(d.getDocumentCharge());
                    d1.setAdvanceRental(d.getAdvanceRental());
                    d1.setDefaultCharge(d.getDefaultCharge());
                    d1.setLatePayment(d.getLatePayment());
                    d1.setStatus("ACTIVE");
                    getDatabaseService().save(d1);
                }
                
                
                getDatabaseService().save(cashierClosing);
                
                 SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.saveAccountTransaction(
                        getDatabaseService(),
                        transaction,
                        SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE,
                        AccountTransactionType.AUTO,
                        AccountSettingCreditOrDebit.CREDIT,
                        cashierClosing.getCashInHand(),
                        cashierClosing.getCashierPoint().getAccountByCashierAccount().getCode(),
                        "Cashier closing - " + cashierClosing.getCashierPoint().getName(),0);
                accountTransaction.saveAccountTransaction(
                        getDatabaseService(),
                        transaction,
                        SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE,
                        AccountTransactionType.AUTO,
                        AccountSettingCreditOrDebit.DEBIT,
                        cashierClosing.getCashInHand(),
                        cashierClosing.getCashierPoint().getAccountByMainAccount().getCode(),
                        "Cashier closing - " + cashierClosing.getCashierPoint().getName(),0);
                
                
                getDatabaseService().save(cashierClosing);
                
                
               CashierSession cashierSessions = cashierClosing.getCashierSession();
                cashierSessions.setStatus(CashierSessionStatus.CLOSED);
                
                getDatabaseService().update(cashierSessions);
                
                mOptionPane.showMessageDialog(null, "Save successfully.",
                   TITLE, mOptionPane.INFORMATION_MESSAGE);
        
         } catch (DatabaseException ex) {
            Logger.getLogger(CashDenominationPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        }else{  
           mOptionPane.showMessageDialog(null, "Amount is Shorted !",
                   TITLE, mOptionPane.ERROR_MESSAGE);
        }
        
    }
   
private CashierSession cashierSession;

public CashierSession getCashierSession(){
        try {
          return  SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException ex) {
            Logger.getLogger(CashDenominationPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}

private void initCashierSessionInfo() {
        if (cashierSession != null) {
            txtEmployee.setCValue(cashierSession.getEmployee().getName());
            txtCashierPoint.setCValue(cashierSession.getCashierPoint().getName());
        } else {
            txtEmployee.setCValue(null);
            txtCashierPoint.setCValue(null);
        }
    }
    private List<Client> getVehicleClient(String vehicleNo) {

        List<Client> clientx = new ArrayList<>();
        String sql = "SELECT DISTINCT client.*\n"
                + "                FROM loan_vehicle\n"
                + "                left join loan on loan_vehicle.loan = loan.index_no\n"
                + "                left join vehicle on loan_vehicle.vehicle_index=vehicle.index_no\n"
                + "                left join client on loan.client=client.code\n"
                + "                where vehicle.vehicle_no LIKE'%" + vehicleNo + "%'";
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(sql, Client.class);
        try {
            clientx = CPanel.GLOBAL.getDatabaseService().getCollection(hibernateSQLQuery, null);
        } catch (DatabaseException ex) {
            Logger.getLogger(ClientInformationPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clientx;
    }
    
    public void setDataTotal(Date date){
        
        try {
            double repayment=0.0;
            double document=0.0;
            double advance=0.0;
            double def=0.0;
            double late=0.0;
            double total=0.0;
           
            
            
            List<Denomination> denominations=cashDenonimation.getDetails(date);
            
            
            for(Denomination d:denominations){
               repayment += d.getRepaymentCollection();
               document += d.getDocumentCharge();
               advance += d.getAdvanceRental();
               def += d.getDefaultCharge();
               late += d.getLatePayment();
               total += d.getTotal();
              
            }
            txtRepayment.setCValue(repayment);
            txtDocumentCharge.setCValue(document);
            txtAdvanceRental.setCValue(advance);
            txtDefaultCharge.setCValue(def);
            txtLatePayment.setCValue(late);
            txtTotalCollection.setCValue(total);
            
            double al=0.0;
            for(Denomination ae:cashDenonimation.getAdvanceLoan(date)){
                
               al += ae.getTotal();
                
            }
            double ql=0.0;
            for(Denomination ae:cashDenonimation.getQlLoan(date)){
                
               ql += ae.getTotal();
                
            }
            txtAlLoan.setCValue(al);
            txtQlLoan1.setCValue(ql);
            txtCashInHand.setCValue(total-(al+ql));
            
        } catch (Exception e) {
        e.printStackTrace();
        }
        
        
    }
    public void calculateExcess(){
        
        double totalFloat = txtTotal.getCValue();
        double cashInHand = txtCashInHand.getCValue();
        
        txtExcessShort.setCValue(totalFloat-cashInHand);
        btnsave.setEnabled(true);
       
    }

    public void unEditableComponent(){
        
            txtRepayment.setEditable(false);
            txtDocumentCharge.setEditable(false);
            txtAdvanceRental.setEditable(false);
            txtDefaultCharge.setEditable(false);
            txtLatePayment.setEditable(false);
            
            txtTotalNote5000.setEditable(false);
            txtTotalNote2000.setEditable(false);
            txtTotalNote1000.setEditable(false);
            txtTotalNote500.setEditable(false);
            txtTotalNote100.setEditable(false);
            txtTotalNote50.setEditable(false);
            txtTotalNote20.setEditable(false);
            txtTotalNote10.setEditable(false);
            txtTotalNote.setEditable(false);
            txtTotal.setEditable(false);
            txtTotalCollection.setEditable(false);
            txtAlLoan.setEditable(false);
            txtQlLoan1.setEditable(false);
            txtCashInHand.setEditable(false);
            txtExcessShort.setEditable(false);
    }
    public void resetForm(){
        
            txtTotalNote5000.resetValue();
            txtTotalNote2000.resetValue();
            txtTotalNote1000.resetValue();
            txtTotalNote500.resetValue();
            txtTotalNote100.resetValue();
            txtTotalNote50.resetValue();
            txtTotalNote20.resetValue();
            txtTotalNote10.resetValue();
            txtTotalNote.resetValue();
            txtTotal.resetValue();
            txtTotalCollection.resetValue();
            txtAlLoan.resetValue();
            txtQlLoan1.resetValue();
            txtCashInHand.resetValue();
            txtExcessShort.resetValue();
            
            btnsave.setEnabled(false);
            
            txtNote5000.resetValue();
            txtNote2000.resetValue();
            txtNote1000.resetValue();
            txtNote500.resetValue();
            txtNote100.resetValue();
            txtNote50.resetValue();
            txtNote20.resetValue();
            txtNote10.resetValue();
            txtTotalOther.resetValue();
        
    }
    
    public void viewSelectedDetails(String center,Date date){
        
        try {
            
          List<com.mac.account.cash_denomination.object2.Denomination> denominations= cashDenonimation.getDetailsByCenter(center,date);
            
          ViewLoanDetail details=new ViewLoanDetail(denominations);
          details.setSize(800, 400);
          details.setAlwaysOnTop(true);
         
          details.setVisible(true);
            
            System.out.println("SHOW .. .. ");
            
            
        } catch (Exception e) {
        e.printStackTrace();
        }
        
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cLabel19 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        txtCashierPoint = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        jPanel2 = new javax.swing.JPanel();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote5000 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote2000 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote1000 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote500 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote100 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote50 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        txtNote20 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote10 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalNote500 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote2000 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote1000 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote5000 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote100 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote50 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote20 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote10 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalNote = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel21 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalOther = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel17 = new com.mac.af.component.derived.display.label.CDLabel();
        txtEmployee = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel18 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel20 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotal = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDetails = new com.mac.af.component.derived.input.table.CITable();
        jPanel3 = new javax.swing.JPanel();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtRepayment = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentCharge = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAdvanceRental = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDefaultCharge = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel22 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLatePayment = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel24 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalCollection = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel25 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAlLoan = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel26 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCashInHand = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel27 = new com.mac.af.component.derived.display.label.CDLabel();
        txtExcessShort = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        btnRefresh = new javax.swing.JButton();
        btncalculate = new javax.swing.JButton();
        btnsave = new javax.swing.JButton();
        cLabel28 = new com.mac.af.component.derived.display.label.CDLabel();
        txtQlLoan1 = new com.mac.af.component.derived.input.textfield.CIDoubleField();

        jLabel1.setFont(new java.awt.Font("Tempus Sans ITC", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText(" cash denomination");

        cLabel19.setText("Employee :");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Note"));

        cLabel2.setText("Rs.5000 :");

        txtNote5000.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote5000FocusLost(evt);
            }
        });

        cLabel6.setText("Rs.2000 :");

        txtNote2000.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote2000FocusLost(evt);
            }
        });

        cLabel7.setText("Rs.1000 :");

        txtNote1000.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote1000FocusLost(evt);
            }
        });

        cLabel8.setText("Rs.500:");

        txtNote500.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote500FocusLost(evt);
            }
        });

        cLabel9.setText("Rs.100 :");

        txtNote100.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote100FocusLost(evt);
            }
        });

        cLabel10.setText("Rs.50 :");

        txtNote50.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote50FocusLost(evt);
            }
        });

        txtNote20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote20FocusLost(evt);
            }
        });

        cLabel11.setText("Rs.20 :");

        txtNote10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote10FocusLost(evt);
            }
        });

        cLabel12.setText("Rs.10 :");

        txtTotalNote500.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote2000.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote1000.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote5000.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote100.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote50.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote20.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel16.setText("Total Note :");

        txtTotalNote.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotalNote.setText("0.0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNote20, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                            .addComponent(txtNote100, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote2000, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote500, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote1000, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote5000, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotalNote5000, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                            .addComponent(txtTotalNote2000, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote1000, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote500, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote100, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(txtTotalNote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNote5000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNote2000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNote1000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNote500, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtTotalNote5000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalNote2000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalNote1000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalNote500, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNote100, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote100, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNote50, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote50, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNote20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNote10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cLabel14.setText("Other :");

        cLabel21.setText("Reference No :");

        txtTotalOther.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotalOther.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtTotalOtherFocusLost(evt);
            }
        });

        cLabel17.setText("Transaction Date :");

        cLabel18.setText("Cashier Point :");

        cLabel20.setText("Document No :");

        cLabel13.setText("Total :");

        txtTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        tblDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblDetails);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Total"));

        cLabel3.setText("Repayment:");

        txtRepayment.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel4.setText("Document Charges:");

        txtDocumentCharge.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel5.setText("Advance Rental:");

        txtAdvanceRental.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel15.setText("Default Charges:");

        txtDefaultCharge.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel22.setText("Late Payment:");

        txtLatePayment.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtRepayment, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDocumentCharge, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAdvanceRental, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDefaultCharge, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtLatePayment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRepayment, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentCharge, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAdvanceRental, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDefaultCharge, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLatePayment, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cLabel24.setText("Total Collection:");

        txtTotalCollection.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel25.setText("AL Loan:");

        txtAlLoan.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel26.setText("Cash In Hand:");
        cLabel26.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        txtCashInHand.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCashInHand.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        cLabel27.setText("Excess/Short:");
        cLabel27.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        txtExcessShort.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtExcessShort.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        btnRefresh.setText("Refresh");

        btncalculate.setText("Calculate");

        btnsave.setText("Save");

        cLabel28.setText("QL Loan:");

        txtQlLoan1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtEmployee, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                            .addComponent(txtCashierPoint, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(32, 32, 32)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtTotalOther, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                                    .addComponent(txtTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 112, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnRefresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtExcessShort, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtTotalCollection, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(cLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtAlLoan, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(btncalculate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnsave, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCashInHand, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtQlLoan1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCashierPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTotalOther, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(23, 23, 23))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(192, 192, 192)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTotalCollection, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAlLoan, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQlLoan1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCashInHand, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtExcessShort, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btncalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsave, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtNote5000FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote5000FocusLost
        int count = Integer.parseInt(txtNote5000.getValue().toString());
        txtTotalNote5000.setValue(count * 5000);
        setTotal();
    }//GEN-LAST:event_txtNote5000FocusLost

    private void txtNote2000FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote2000FocusLost
        int count = Integer.parseInt(txtNote2000.getValue().toString());
        txtTotalNote2000.setValue(count * 2000);
        setTotal();
    }//GEN-LAST:event_txtNote2000FocusLost

    private void txtNote1000FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote1000FocusLost
        int count = Integer.parseInt(txtNote1000.getValue().toString());
        txtTotalNote1000.setValue(count * 1000);
        setTotal();
    }//GEN-LAST:event_txtNote1000FocusLost

    private void txtNote500FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote500FocusLost
        int count = Integer.parseInt(txtNote500.getValue().toString());
        txtTotalNote500.setValue(count * 500);
        setTotal();
    }//GEN-LAST:event_txtNote500FocusLost

    private void txtNote100FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote100FocusLost
        int count = Integer.parseInt(txtNote100.getValue().toString());
        txtTotalNote100.setValue(count * 100);
        setTotal();
    }//GEN-LAST:event_txtNote100FocusLost

    private void txtNote50FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote50FocusLost
        int count = Integer.parseInt(txtNote50.getValue().toString());
        txtTotalNote50.setValue(count * 50);
        setTotal();
    }//GEN-LAST:event_txtNote50FocusLost

    private void txtNote20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote20FocusLost
        int count = Integer.parseInt(txtNote20.getValue().toString());
        txtTotalNote20.setValue(count * 20);
        setTotal();
    }//GEN-LAST:event_txtNote20FocusLost

    private void txtNote10FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote10FocusLost
        int count = Integer.parseInt(txtNote10.getValue().toString());
        txtTotalNote10.setValue(count * 10);
        setTotal();
    }//GEN-LAST:event_txtNote10FocusLost

    private void txtTotalOtherFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtTotalOtherFocusLost
        setTotal();
    }//GEN-LAST:event_txtTotalOtherFocusLost
 private void setTotal() {
   
        double n5000 = Double.parseDouble(txtTotalNote5000.getValue().toString());
        double n2000 = Double.parseDouble(txtTotalNote2000.getValue().toString());
        double n1000 = Double.parseDouble(txtTotalNote1000.getValue().toString());
        double n500 = Double.parseDouble(txtTotalNote500.getValue().toString());
        double n100 = Double.parseDouble(txtTotalNote100.getValue().toString());
        double n50 = Double.parseDouble(txtTotalNote50.getValue().toString());
        double n20 = Double.parseDouble(txtTotalNote20.getValue().toString());
        double n10 = Double.parseDouble(txtTotalNote10.getValue().toString());

        double totalNote = n5000 + n2000 + n1000 + n500 + n100 + n50 + n20 + n10 ;

        double other = Double.parseDouble(txtTotalOther.getValue().toString());
        double total =   totalNote + other;

  
        txtTotalNote.setValue(totalNote);
        txtTotal.setValue(total);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btncalculate;
    private javax.swing.JButton btnsave;
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cLabel17;
    private com.mac.af.component.derived.display.label.CDLabel cLabel18;
    private com.mac.af.component.derived.display.label.CDLabel cLabel19;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel20;
    private com.mac.af.component.derived.display.label.CDLabel cLabel21;
    private com.mac.af.component.derived.display.label.CDLabel cLabel22;
    private com.mac.af.component.derived.display.label.CDLabel cLabel24;
    private com.mac.af.component.derived.display.label.CDLabel cLabel25;
    private com.mac.af.component.derived.display.label.CDLabel cLabel26;
    private com.mac.af.component.derived.display.label.CDLabel cLabel27;
    private com.mac.af.component.derived.display.label.CDLabel cLabel28;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cLabel9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private com.mac.af.component.derived.input.table.CITable tblDetails;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAdvanceRental;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAlLoan;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtCashInHand;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCashierPoint;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtDefaultCharge;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtDocumentCharge;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtEmployee;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtExcessShort;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLatePayment;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote10;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote100;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote1000;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote20;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote2000;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote50;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote500;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote5000;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtQlLoan1;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtRepayment;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotal;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalCollection;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote10;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote100;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote1000;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote20;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote2000;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote50;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote500;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote5000;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalOther;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables

    private CashDenonimation cashDenonimation;
}
