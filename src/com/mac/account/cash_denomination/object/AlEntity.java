/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.cash_denomination.object;

/**
 *
 * @author Administrator
 */
public class AlEntity {
    private double loan_amount;

    public AlEntity(double loan_amount) {
        this.loan_amount = loan_amount;
    }

    
    public double getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(double loan_amount) {
        this.loan_amount = loan_amount;
    }
    
    
}
