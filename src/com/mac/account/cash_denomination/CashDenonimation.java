/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.cash_denomination;

import com.mac.account.cash_denomination.object.Denomination;
import com.mac.account.cashier_closing.*;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.zsystem.session_button.SessionButton;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.cashier.CashierSessionStatus;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chathu
 */
public class CashDenonimation extends AbstractService {

    public CashDenonimation(Component component){
       super(component);
        initOthers();
    }
    
    private void initOthers() {
         this.sessionButton = new SessionButton();
        System.out.println("getTransactionTypeCode()____"+SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE);
        this.sessionButton.setTransactionType(SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE);
        try {
            this.sessionButton.setDatabseService(getDatabaseService());
        } catch (DatabaseException ex) {
            Logger.getLogger(CashDenonimation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    


    public Class getObjectClass() {
        return com.mac.zsystem.transaction.cashier.object.CashierClosing.class;
    }

   
    public AbstractObjectCreator getObjectCreator() {
        return new PCCashierClosing() {
            @Override
            protected CashierSession getCashierSession() {
                try {
                   
                        return SystemCashier.getCurrentCashierSession(getDatabaseService());
                
                } catch (DatabaseException ex) {
                    Logger.getLogger(CashierClosing.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                } catch (ApplicationException ex) {
                    Logger.getLogger(CashDenonimation.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        };
    }

 
    public CTableModel getTableModel() {
      CTableModel cTableModel = new CTableModel(new CTableColumn[]{
            new CTableColumn("Center","center"),
            new CTableColumn("Center Name", "centerName"),
            new CTableColumn("Repayment", "repaymentCollection"),
            new CTableColumn("Document/Fund", "documentCharge"),
            new CTableColumn("Advance", "advanceRental"),
            new CTableColumn("Default", "defaultCharge"),
            new CTableColumn("Total", "total"),
        });
        return (cTableModel);
    }
    public List<Denomination> getDetails(Date date){
       
        
        String sql="SELECT\n" +
"        t.`index_no` as `index_no`,\n" +
"        IFNULL(s.`code`,IFNULL(l.`loan_type`,t.`transaction_type`)) AS center,\n" +
"        ifnull(s.`name`,'Collection')   AS centerName,\n" +
"        IFNULL(sum(h.repaymentCollection),0.0) AS repaymentCollection,\n" +
"            \n" +
"        IFNULL(sum(d.`documentCharge`),0.0)  AS documentCharge,\n" +
"        \n" +
"        IFNULL(sum(p.`advanceRental`),0.0) AS advanceRental,\n" +
"        \n" +
"        ifnull(sum(df.defaultCharge),0.0) AS defaultCharge,\n" +
"        0.0 AS latePayment,\n" +
"        IFNULL(sum(h.repaymentCollection),0.0)+IFNULL(sum(d.`documentCharge`),0.0)+IFNULL(sum(p.`advanceRental`),0.0) AS total  \n" +
"   " +
"    FROM\n" +
"        `transaction` t \n" +
"    LEFT JOIN `loan` l ON t.`loan`=l.`index_no` \n" +
"    LEFT JOIN `client` c ON c.`code` = l.`client` \n" +
"    LEFT JOIN `loan_center` s ON s.`code` = c.`center` \n" +
"    LEFT JOIN `transaction_history` th ON t.`index_no` = th.transaction   \n" +
"    left join (SELECT SUM(h.`settlement_amount`) as repaymentCollection ,h.transaction \n" +
"        FROM `settlement_history` h     \n" +
"        INNER JOIN settlement st ON h.settlement=st.index_no    \n" +
"        WHERE st.settlement_type not IN ('LOAN_AMOUNT','APPLICATION_CHARGE') group by h.transaction ) as h on h.transaction = t.`index_no`\n" +
"    left join (SELECT SUM(h.`settlement_amount`)as documentCharge  ,h.transaction\n" +
"        FROM `settlement_history` h     \n" +
"        INNER JOIN settlement st ON h.settlement=st.index_no    \n" +
"        WHERE st.settlement_type  IN ('APPLICATION_CHARGE') GROUP BY h.transaction )as d on  d.transaction = t.`index_no`\n" +
"    left join (SELECT (p.`amount`) as advanceRental,p.transaction\n" +
"        FROM  `payment_information` p \n" +
"        WHERE  p.`payment_setting` = 'ADVANCE_RECEIPT_CASH' GROUP BY p.transaction )as p on p.transaction = t.`index_no` \n" +
"    left join (SELECT SUM(h.`settlement_amount`) as  defaultCharge, h.transaction\n" +
"        FROM `settlement_history` h     \n" +
"        INNER JOIN settlement st ON h.settlement=st.index_no    \n" +
"        WHERE st.settlement_type = ('LOAN_PANALTY') GROUP BY h.transaction)as df on df.transaction = t.`index_no`\n" +
"        \n" +
"    WHERE\n" +
"        t.`status`<>'CANCEL' AND t.`transaction_type` IN('RECEIPT','ADVANCE_RECEIPT')  \n" +
"        AND t.`transaction_date`= '"+date+"'  AND th.`employee`= '"+CApplication.getSessionVariable(CApplication.USER_ID)+"'  \n" +
"    GROUP BY ifnull(s.`code`,IFNULL(l.`loan_type`,t.`transaction_type`))";
      //  System.out.println(sql);
        try {
//            HashMap params =new HashMap<>();
//            params.put("TDATE", (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
//            params.put("OFFICER",CApplication.getSessionVariable(CApplication.USER_ID));
            
        
            HibernateSQLQuery hibernateSQLQuery =new HibernateSQLQuery(sql, Denomination.class);
       
            return getDatabaseService().getCollectionSQLQuery(hibernateSQLQuery,null);
        } catch (DatabaseException ex) {
            Logger.getLogger(CashDenonimation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return new ArrayList<>();
    }
    
    public List<com.mac.account.cash_denomination.object2.Denomination> getDetailsByCenter(String center,Date date){
       
        
        String sqls="SELECT\n" +
"        t.`index_no` as `index_no`,\n" +
"        ifnull(s.`code`,l.`loan_type`) AS center,\n" +
"        concat(l.agreement_no,' - ',c.name)  AS centerName,\n" +
"        IFNULL(sum(h.repaymentCollection),0.0) AS repaymentCollection,\n" +
"            \n" +
"        IFNULL(sum(d.`documentCharge`),0.0)  AS documentCharge,\n" +
"        \n" +
"        IFNULL(sum(p.`advanceRental`),0.0) AS advanceRental,\n" +
"        \n" +
"        ifnull(sum(df.defaultCharge),0.0) AS defaultCharge,\n" +
"        0.0 AS latePayment,\n" +
"        IFNULL(sum(h.repaymentCollection),0.0)+IFNULL(sum(d.`documentCharge`),0.0)+IFNULL(sum(p.`advanceRental`),0.0) AS total  \n" +
"   " +
"    FROM\n" +
"        `transaction` t \n" +
"    LEFT JOIN `loan` l ON t.`loan`=l.`index_no` \n" +
"    LEFT JOIN `client` c ON c.`code` = l.`client` \n" +
"    LEFT JOIN `loan_center` s ON s.`code` = c.`center` \n" +
"    LEFT JOIN `transaction_history` th ON t.`index_no` = th.transaction   \n" +
"    left join (SELECT SUM(h.`settlement_amount`) as repaymentCollection ,h.transaction \n" +
"        FROM `settlement_history` h     \n" +
"        INNER JOIN settlement st ON h.settlement=st.index_no    \n" +
"        WHERE st.settlement_type not IN ('LOAN_AMOUNT','APPLICATION_CHARGE') group by h.transaction ) as h on h.transaction = t.`index_no`\n" +
"    left join (SELECT SUM(h.`settlement_amount`)as documentCharge  ,h.transaction\n" +
"        FROM `settlement_history` h     \n" +
"        INNER JOIN settlement st ON h.settlement=st.index_no    \n" +
"        WHERE st.settlement_type  IN ('APPLICATION_CHARGE') GROUP BY h.transaction )as d on  d.transaction = t.`index_no`\n" +
"    left join (SELECT (p.`amount`) as advanceRental,p.transaction\n" +
"        FROM  `payment_information` p \n" +
"        WHERE  p.`payment_setting` = 'ADVANCE_RECEIPT_CASH' GROUP BY p.transaction )as p on p.transaction = t.`index_no` \n" +
"    left join (SELECT SUM(h.`settlement_amount`) as  defaultCharge, h.transaction\n" +
"        FROM `settlement_history` h     \n" +
"        INNER JOIN settlement st ON h.settlement=st.index_no    \n" +
"        WHERE st.settlement_type = ('LOAN_PANALTY') GROUP BY h.transaction)as df on df.transaction = t.`index_no`\n" +
"        \n" +
"    WHERE\n" +
"        t.`status`<>'CANCEL' AND t.`transaction_type` IN('BANK_RECEIPT','CHEQUE_RECEIPT','RECEIPT','ADVANCE_RECEIPT')  \n" +
"        AND t.`transaction_date`= '"+date+"'  AND th.`employee`= '"+CApplication.getSessionVariable(CApplication.USER_ID)+"'  \n" +
"        AND  (s.`code`='"+center+"' OR l.`loan_type`='"+center+"') \n" +
"    GROUP BY l.`index_no`";
        //'"+CApplication.getSessionVariable(CApplication.USER_ID)+"'
        System.out.println(sqls);
        try {
//            HashMap params =new HashMap<>();
//            params.put("TDATE", (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
//            params.put("OFFICER",CApplication.getSessionVariable(CApplication.USER_ID));
            
        
            HibernateSQLQuery hibernateSQLQuery =new HibernateSQLQuery(sqls, com.mac.account.cash_denomination.object2.Denomination.class);
       
            return getDatabaseService().getCollectionSQLQuery(hibernateSQLQuery,null);
        } catch (DatabaseException ex) {
            Logger.getLogger(CashDenonimation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return new ArrayList<>();
    }
    
   public List<Denomination> getAdvanceLoan(Date date){
     
       try {
               String sql2 = "SELECT\n" +
                        "        t.`index_no` as `index_no`,\n" +
                        "        '' AS center,\n" +
                        "        ''  AS centerName,\n" +
                        "        0.0 AS repaymentCollection,\n" +
                        "            \n" +
                        "        0.0  AS documentCharge,\n" +
                        "        \n" +
                        "        0.0 AS advanceRental,\n" +
                        "        \n" +
                        "        0.0 AS defaultCharge,\n" +
                        "        0.0 AS latePayment,\n" +
                            "       ifnull(sum(l.`loan_amount`),0.0) as total\n" +
                            "    FROM\n" +
                            "        `transaction` t \n" +
                            "    LEFT JOIN `loan` l ON t.`loan`=l.`index_no` \n" +
                            "    LEFT JOIN `transaction_history` th ON t.`index_no` = th.transaction   \n" +
                            "\n" +
                            "    WHERE\n" +
                            "        t.`status`<>'CANCEL' AND t.`transaction_type` = 'ADVANCE_LOAN' AND l.loan_type='AL'  \n" +
                            "        AND t.`transaction_date`= '"+date+"'  AND l.`recovery_officer`= '"+CApplication.getSessionVariable(CApplication.USER_ID)+"' group by th.`employee` ";
           HibernateSQLQuery hibernateSQLQuery =new HibernateSQLQuery(sql2, Denomination.class);
       
           return getDatabaseService().getCollectionSQLQuery(hibernateSQLQuery,null);
          
       
       } catch (Exception e) {
           e.printStackTrace();
       }
       return new ArrayList<>();
   }

   public List<Denomination> getQlLoan(Date date){
     
       try {
               String sql2 = "SELECT\n" +
                        "        t.`index_no` as `index_no`,\n" +
                        "        '' AS center,\n" +
                        "        ''  AS centerName,\n" +
                        "        0.0 AS repaymentCollection,\n" +
                        "            \n" +
                        "        0.0  AS documentCharge,\n" +
                        "        \n" +
                        "        0.0 AS advanceRental,\n" +
                        "        \n" +
                        "        0.0 AS defaultCharge,\n" +
                        "        0.0 AS latePayment,\n" +
                            "       ifnull(sum(l.`loan_amount`),0.0) as total\n" +
                            "    FROM\n" +
                            "        `transaction` t \n" +
                            "    LEFT JOIN `loan` l ON t.`loan`=l.`index_no` \n" +
                            "    LEFT JOIN `transaction_history` th ON t.`index_no` = th.transaction   \n" +
                            "\n" +
                            "    WHERE\n" +
                            "        t.`status`<>'CANCEL' AND t.`transaction_type` = 'ADVANCE_LOAN' AND l.loan_type='QL'  \n" +
                            "        AND t.`transaction_date`= '"+date+"'  AND l.`recovery_officer`= '"+CApplication.getSessionVariable(CApplication.USER_ID)+"' group by th.`employee` ";
           HibernateSQLQuery hibernateSQLQuery =new HibernateSQLQuery(sql2, Denomination.class);
       
           return getDatabaseService().getCollectionSQLQuery(hibernateSQLQuery,null);
          
       
       } catch (Exception e) {
           e.printStackTrace();
       }
       return new ArrayList<>();
   }




    protected int save(Object object) throws DatabaseException {
        com.mac.zsystem.transaction.cashier.object.CashierClosing cashierClosing = (com.mac.zsystem.transaction.cashier.object.CashierClosing) object;

        CashierSession cashierSession = cashierClosing.getCashierSession();
        cashierSession.setStatus(CashierSessionStatus.CLOSED);

        Integer transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE,
                cashierClosing.getReferenceNo(),
                cashierClosing.getDocumentNo(),
                null,
                cashierSession.getIndexNo(),//cashier session ID
                null,
                null);

        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.saveAccountTransaction(
                getDatabaseService(),
                transaction,
                SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE,
                AccountTransactionType.AUTO,
                AccountSettingCreditOrDebit.CREDIT,
                cashierClosing.getTotalCash(),
                cashierClosing.getCashierPoint().getAccountByCashierAccount().getCode(),
                "Cashier closing - " + cashierClosing.getCashierPoint().getName(),0);
        accountTransaction.saveAccountTransaction(
                getDatabaseService(),
                transaction,
                SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE,
                AccountTransactionType.AUTO,
                AccountSettingCreditOrDebit.DEBIT,
                cashierClosing.getTotalCash(),
                cashierClosing.getCashierPoint().getAccountByMainAccount().getCode(),
                "Cashier closing - " + cashierClosing.getCashierPoint().getName(),0);
        getDatabaseService().save(cashierClosing);
        getDatabaseService().save(cashierSession);


        return 1;
    }


    @Action
    public void doPrint() {
        this.sessionButton.doClick();
    }
    
     private SessionButton sessionButton;
    
    
}
