/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.budget;

import com.mac.account.AccountGroup;
import com.mac.account.AccountTypes;
import com.mac.account.budget.object.Account;
import com.mac.account.budget.object.BudgetAccount;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.util.compare.comparison_chain.ComparisonChain;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mohan
 */
public class SERBudget extends AbstractService {

    public SERBudget(Component component) {
        super(component);
    }

    public List getBudgetAccounts() {
        try {
            Criteria criteria = getDatabaseService().initCriteria(Account.class)
                    .add(Restrictions.eq("active", true))
                    .add(Restrictions.or(Restrictions.eq("accountGroup", AccountGroup.EXPENSE_ACCOUNT), Restrictions.eq("accountGroup", AccountGroup.INCOME_ACCOUNT)));
            List<Account> accounts = criteria.list();
            List<BudgetAccount> budgetAccounts = new ArrayList<>();
            for (Account account : accounts) {
                BudgetAccount budgetAccount = new BudgetAccount();
                budgetAccount.setAmount(0.0);
                budgetAccount.setAccount(account);

                budgetAccounts.add(budgetAccount);
            }

            Collections.sort(budgetAccounts, new Comparator<BudgetAccount>() {
                @Override
                public int compare(BudgetAccount o1, BudgetAccount o2) {
                    return ComparisonChain
                            .start()
                            .compare(o1.getAccount().getAccountGroup(), o2.getAccount().getAccountGroup())
                            .result();
                }
            });


            return budgetAccounts;
        } catch (DatabaseException | HibernateException hibernateException) {
            return new ArrayList();
        }

    }
}
