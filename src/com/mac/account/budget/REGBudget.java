/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.budget;

import com.mac.account.AccountGroup;
import com.mac.account.budget.object.Budget;
import com.mac.account.budget.object.BudgetAccount;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 *
 * @author mohan
 */
public class REGBudget extends AbstractRegistrationForm<Budget> {

    public REGBudget() {
        tblMain.getCTableModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                double income = 0.0;
                double expence = 0.0;

                Set<BudgetAccount> budgetAccounts = new HashSet<>(tblMain.getCValue());

                for (BudgetAccount budgetAccount : budgetAccounts) {
                    if (budgetAccount.getAccount().getAccountGroup().equals(AccountGroup.EXPENSE_ACCOUNT)) {
                        expence += budgetAccount.getAmount();
                    }
                    if (budgetAccount.getAccount().getAccountGroup().equals(AccountGroup.INCOME_ACCOUNT)) {
                        income += budgetAccount.getAmount();
                    }
                }

                budget.setIncomeTotal(income);
                budget.setExpenseTotal(expence);

            }
        });
    }

    @Override
    public AbstractObjectCreator<Budget> getObjectCreator() {
        budget = new PCBudget();
        return budget;
    }

    @Override
    public Class<? extends Budget> getObjectClass() {
        return Budget.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Account Code", "account", "code"),
                new CTableColumn("Account Name", "account", "name"),
                new CTableColumn("Account Group", "account", "accountGroup"),
                new CTableColumn("Budget", new String[]{"amount"}, true));
    }

    @Override
    protected List<Budget> getTableData() throws DatabaseException {
        initService();
        return serBudget.getBudgetAccounts();


    }

    private void initService() {
        if (serBudget == null) {
            serBudget = new SERBudget(this);
        }
    }

    @Override
    protected Budget getTableValueForObjectCreator(Object tableValue) {
        return null;
    }

    @Override
    protected int save(Budget object) throws DatabaseException {
        Set<BudgetAccount> budgetAccounts = new HashSet<>(tblMain.getCValue());
        Set<BudgetAccount> selectedAccounts = new HashSet<>();

        for (BudgetAccount budgetAccount : budgetAccounts) {
            if (budgetAccount.getAmount() > 0) {
                budgetAccount.setBudget(object);
                selectedAccounts.add(budgetAccount);
            }
        }


        object.setBudgetAccounts(selectedAccounts);

        int trasaction = SystemTransactions.insertTransaction(getDatabaseService(),
                SystemTransactions.ACCOUNT_BUDGET,
                object.getReferenceNo(),
                object.getDocumentNo(),
                null,
                null,
                null,
                null);

        object.setTransaction(trasaction);

        getDatabaseService().save(object);
        return SAVE_SUCCESS;
    }
    private SERBudget serBudget;
    private PCBudget budget;
}
