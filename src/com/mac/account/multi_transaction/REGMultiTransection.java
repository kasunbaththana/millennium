/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.multi_transaction;

import java.util.List;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author Thilanga-pc
 */
public abstract class REGMultiTransection extends AbstractRegistrationForm {

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCMultiTransaction();

    }

    @Override
    public Class getObjectClass() {
        return com.mac.account.multi_transaction.object.MultiTransaction.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Referance No", "referenceNo"),
            new CTableColumn("Document No", "documentNo"),
            new CTableColumn("Transaction No", "transactionDate"),
            new CTableColumn("Credit No", "creditAccount"),
            new CTableColumn("Debit No", "debitAccount"),
            new CTableColumn("Amount", "amount")
        });
    }

    protected abstract List getCreditAccounts();

    protected abstract List getDebitAccounts();

    protected abstract boolean isPayable();

    protected abstract String getAccountType();
    public static final String ACCOUNT_TYPE_CREDIT = "CREDIT";
    public static final String ACCOUNT_TYPE_D = "DEBIT";
    public static final String ACCOUNT_TYPE_D_AND_C = "CRIDIT AND DEBIT";
}
