/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.cheque_issue;

import com.mac.account.zobject.cheque.Cheque;
import com.mac.account.zobject.cheque.ChequeDetails;
import com.mac.account.ztemplate.cheque_transaction.AbstractChequeTransaction;
import com.mac.account.ztemplate.cheque_transaction.ChequeStatus;
import com.mac.account.ztemplate.cheque_transaction.custom_object.ChequeTransactionObject;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeIssueAccountInterface;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SMTK
 */
public class ChequeIssue extends AbstractChequeTransaction {

//    @Override
//    public String executeText() {
//        return "Issue";
//    }
//
//    @Override
//    public String getChequeNewStatus() {
//        return ChequeStatus.ISSUE;
//    }
//
//    @Override
//    public String getChequeOldStatus() {
//        return ChequeStatus.PENDING;
//    }
    @Override
    protected String getTitle() {
        return "Checque Issue";
    }

//    @Override
//    public String getChequeType() {
//        return ChequeType.COMPANY;
//    }
    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.CHEQUE_ISSUE_TRANSACTION_CODE;
    }

    @Override
    protected String getReferenceGeneratorKey() {
        return ReferenceGenerator.CHECQUE_ISSUE;
    }

//    @Override
//    protected String getCreditAccountSettingCode() {
//        return ChequeIssueAccountInterface.ISSUE_AMOUNT_CREDIT_CODE;
//    }
//
//    @Override
//    protected String getDebitAccountSettingCode() {
//        return ChequeIssueAccountInterface.ISSUE_AMOUNT_DEBIT_CODE;
//    }
    @Override
    protected List<Cheque> getCheques(ChequeTransactionObject transactionObject) {
        List list;
        System.out.println("Bak  " + transactionObject.getBankAccount().getAccountNumber());
        try {
            HashMap params = new HashMap<>();
            params.put("type", ChequeType.COMPANY);
            params.put("status", ChequeStatus.PENDING);
            params.put("BankAccount", transactionObject.getBankAccount().getAccountNumber());
            params.put("codeaccount", transactionObject.getBankAccount().getCode());
            list = getDatabaseService().getCollection("from com.mac.account.zobject.cheque.Cheque where "
                    + "status=:status and type=:type and (accountNo=:BankAccount or accountNo=:codeaccount )  ", params);

        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(AbstractChequeTransaction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    protected void saveCheques(ChequeTransactionObject transactionObject, Collection<Cheque> cheques) throws DatabaseException {
        //        TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                getTransactionTypeCode(),
                transactionObject.getReferenceNo(),
                transactionObject.getDocumentNo(),
                null,
                null,
                null,
                transactionObject.getNote());

        //CHEQUES
        for (Cheque cheque : cheques) {
            getDatabaseService().beginLocalTransaction();
            cheque.setStatus(ChequeStatus.ISSUE);
            cheque.setBankAccount(transactionObject.getBankAccount());
            cheque.setDepositDate(transactionObject.getAccountDate());

            getDatabaseService().save(cheque);
            getDatabaseService().commitLocalTransaction();
        }

        // save cheque datails
        ChequeDetails ch_details = new ChequeDetails();
        for (Cheque cheque_detail : cheques) {
            ch_details.setChequeIndex(cheque_detail);
            ch_details.setTransaction(transactionIndex);
            ch_details.setDescription(SystemTransactions.CHEQUE_ISSUE_TRANSACTION_CODE);
            ch_details.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
            ch_details.setAmount(cheque_detail.getAmount());
            ch_details.setStatus("ACTIVE");


            getDatabaseService().save(ch_details);
            getDatabaseService().commitLocalTransaction();
        }


        //ACCOUNT TRANSACTION
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        for (Cheque cheque : cheques) {

            accountTransaction.saveAccountTransactionChque(
                    getDatabaseService(),
                    transactionIndex,
                    SystemTransactions.CHEQUE_ISSUE_TRANSACTION_CODE,
                    AccountTransactionType.AUTO,
                    AccountSettingCreditOrDebit.CREDIT,
                    cheque.getAmount(),
                    transactionObject.getBankAccount().getValueAccount(),
                    "Cheque Issue From " + transactionObject.getBankAccount().getCode(), 0,
                    transactionObject.getAccountDate());
            accountTransaction.addAccountTransactionQueueChq(
                    ChequeIssueAccountInterface.ISSUE_AMOUNT_DEBIT_CODE,
                    "Cheque Issue Amount",
                    cheque.getAmount(),
                    AccountTransactionType.AUTO,
                    transactionObject.getAccountDate());

            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, getTransactionTypeCode());

        }

    }

    @Override
    protected String getExecuteText() {
        return "Issue";
    }
}
