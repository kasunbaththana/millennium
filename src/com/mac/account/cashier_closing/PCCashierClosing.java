/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.cashier_closing;

import java.util.Arrays;
import java.util.List;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.util.generator.ReferenceGenerator;

/**
 *
 * @author chathu
 */
public abstract class PCCashierClosing extends DefaultObjectCreator {

    /**
     * Creates new form PCCashierClose
     */
    public PCCashierClosing() {
        initComponents();
        initOthers();
    }

    private void setTotal() {
   
        double n5000 = Double.parseDouble(txtTotalNote5000.getValue().toString());
        double n2000 = Double.parseDouble(txtTotalNote2000.getValue().toString());
        double n1000 = Double.parseDouble(txtTotalNote1000.getValue().toString());
        double n500 = Double.parseDouble(txtTotalNote500.getValue().toString());
        double n100 = Double.parseDouble(txtTotalNote100.getValue().toString());
        double n50 = Double.parseDouble(txtTotalNote50.getValue().toString());
        double n20 = Double.parseDouble(txtTotalNote20.getValue().toString());
        double n10 = Double.parseDouble(txtTotalNote10.getValue().toString());

        double totalNote = n5000 + n2000 + n1000 + n500 + n100 + n50 + n20 + n10 ;

        double other = Double.parseDouble(txtTotalOther.getValue().toString());
        double total =   totalNote + other;

  
        txtTotalNote.setValue(totalNote);
        txtTotal.setValue(total);
    }

    private void initCashierSessionInfo() {
        if (cashierSession != null) {
            txtEmployee.setCValue(cashierSession.getEmployee().getName());
            txtCashierPoint.setCValue(cashierSession.getCashierPoint().getName());
        } else {
            txtEmployee.setCValue(null);
            txtCashierPoint.setCValue(null);
        }
    }

    protected abstract CashierSession getCashierSession();

    @SuppressWarnings("unchecked")
    private void initOthers() {
        cashierSession = getCashierSession();

   

        txtTransactionDate.setValueEditable(false);
        txtCashierPoint.setValueEditable(false);
        txtEmployee.setValueEditable(false);

        txtTotalNote5000.setValueEditable(false);
        txtTotalNote2000.setValueEditable(false);
        txtTotalNote1000.setValueEditable(false);
        txtTotalNote500.setValueEditable(false);
        txtTotalNote100.setValueEditable(false);
        txtTotalNote50.setValueEditable(false);
        txtTotalNote20.setValueEditable(false);
        txtTotalNote10.setValueEditable(false);
        txtTotalNote.setValueEditable(false);
        txtTotal.setValueEditable(false);
//        txtTotalcent.setValueEditable(false);
        
        txtReferenceNo.setGenerator(new ReferenceGenerator(ReferenceGenerator.CASHIER_CLOSE));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote5000 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote2000 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote1000 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote500 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote100 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote50 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        txtNote20 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNote10 = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalNote500 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote2000 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote1000 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote5000 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote100 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote50 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote20 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtTotalNote10 = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalNote = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotal = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalOther = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel17 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel18 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel19 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        txtCashierPoint = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtEmployee = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel20 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel21 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();

        setMinimumSize(new java.awt.Dimension(35, 425));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Note"));

        cLabel2.setText("Rs.5000 :");

        txtNote5000.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote5000FocusLost(evt);
            }
        });

        cLabel6.setText("Rs.2000 :");

        txtNote2000.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote2000FocusLost(evt);
            }
        });

        cLabel7.setText("Rs.1000 :");

        txtNote1000.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote1000FocusLost(evt);
            }
        });

        cLabel8.setText("Rs.500:");

        txtNote500.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote500FocusLost(evt);
            }
        });

        cLabel9.setText("Rs.100 :");

        txtNote100.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote100FocusLost(evt);
            }
        });

        cLabel10.setText("Rs.50 :");

        txtNote50.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote50FocusLost(evt);
            }
        });

        txtNote20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote20FocusLost(evt);
            }
        });

        cLabel11.setText("Rs.20 :");

        txtNote10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNote10FocusLost(evt);
            }
        });

        cLabel12.setText("Rs.10 :");

        txtTotalNote500.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote2000.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote1000.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote5000.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote100.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote50.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote20.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        txtTotalNote10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel16.setText("Total Note :");

        txtTotalNote.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNote20, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                            .addComponent(txtNote100, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote2000, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote500, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote1000, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNote5000, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotalNote5000, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                            .addComponent(txtTotalNote2000, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote1000, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote500, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote100, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalNote10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(txtTotalNote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNote5000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNote2000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNote1000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNote500, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtTotalNote5000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalNote2000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalNote1000, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalNote500, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNote100, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote100, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNote50, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote50, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNote20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNote10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalNote, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cLabel13.setText("Total :");

        txtTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        cLabel14.setText("Other :");

        txtTotalOther.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotalOther.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtTotalOtherFocusLost(evt);
            }
        });

        cLabel17.setText("Transaction Date :");

        cLabel18.setText("Cashier Point :");

        cLabel19.setText("Employee :");

        cLabel20.setText("Document No :");

        cLabel21.setText("Reference No :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCashierPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtEmployee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalOther, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(285, 285, 285))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCashierPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalOther, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtNote5000FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote5000FocusLost
        int count = Integer.parseInt(txtNote5000.getValue().toString());
        txtTotalNote5000.setValue(count * 5000);
        setTotal();
    }//GEN-LAST:event_txtNote5000FocusLost

    private void txtNote2000FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote2000FocusLost
        int count = Integer.parseInt(txtNote2000.getValue().toString());
        txtTotalNote2000.setValue(count * 2000);
        setTotal();
    }//GEN-LAST:event_txtNote2000FocusLost

    private void txtNote1000FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote1000FocusLost
        int count = Integer.parseInt(txtNote1000.getValue().toString());
        txtTotalNote1000.setValue(count * 1000);
        setTotal();
    }//GEN-LAST:event_txtNote1000FocusLost

    private void txtNote500FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote500FocusLost
        int count = Integer.parseInt(txtNote500.getValue().toString());
        txtTotalNote500.setValue(count * 500);
        setTotal();
    }//GEN-LAST:event_txtNote500FocusLost

    private void txtNote100FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote100FocusLost
        int count = Integer.parseInt(txtNote100.getValue().toString());
        txtTotalNote100.setValue(count * 100);
        setTotal();
    }//GEN-LAST:event_txtNote100FocusLost

    private void txtNote50FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote50FocusLost
        int count = Integer.parseInt(txtNote50.getValue().toString());
        txtTotalNote50.setValue(count * 50);
        setTotal();
    }//GEN-LAST:event_txtNote50FocusLost

    private void txtNote20FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote20FocusLost
        int count = Integer.parseInt(txtNote20.getValue().toString());
        txtTotalNote20.setValue(count * 20);
        setTotal();
    }//GEN-LAST:event_txtNote20FocusLost

    private void txtNote10FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNote10FocusLost
        int count = Integer.parseInt(txtNote10.getValue().toString());
        txtTotalNote10.setValue(count * 10);
        setTotal();
    }//GEN-LAST:event_txtNote10FocusLost

    private void txtTotalOtherFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtTotalOtherFocusLost
        setTotal();
    }//GEN-LAST:event_txtTotalOtherFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cLabel17;
    private com.mac.af.component.derived.display.label.CDLabel cLabel18;
    private com.mac.af.component.derived.display.label.CDLabel cLabel19;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel20;
    private com.mac.af.component.derived.display.label.CDLabel cLabel21;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cLabel9;
    private javax.swing.JPanel jPanel2;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCashierPoint;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtEmployee;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote10;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote100;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote1000;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote20;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote2000;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote50;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote500;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNote5000;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotal;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote10;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote100;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote1000;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote20;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote2000;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote50;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote500;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalNote5000;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalOther;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables
    private CashierSession cashierSession;

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(
                txtReferenceNo,
                txtDocumentNo,
                txtNote5000,
                txtNote2000,
                txtNote1000,
                txtNote500,
                txtNote100,
                txtNote50,
                txtNote20,
                txtNote10,
                txtTotalOther);
    }
    

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtTransactionDate, "transactionDate"),
                new CInputComponentBinder(txtReferenceNo, "referenceNo"),
                new CInputComponentBinder(txtDocumentNo, "documentNo"),

                new CInputComponentBinder(txtTotalNote5000, "note5000"),
                new CInputComponentBinder(txtTotalNote2000, "note2000"),
                new CInputComponentBinder(txtTotalNote1000, "note1000"),
                new CInputComponentBinder(txtTotalNote500, "note500"),
                new CInputComponentBinder(txtTotalNote100, "note100"),
                new CInputComponentBinder(txtTotalNote50, "note50"),
                new CInputComponentBinder(txtTotalNote20, "note20"),
                new CInputComponentBinder(txtTotalNote10, "note10"),
                new CInputComponentBinder(txtTotalNote, "totalNote"),
                new CInputComponentBinder(txtTotalOther, "other"),
                new CInputComponentBinder(txtTotal, "totalCash"));
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(txtEmployee, txtCashierPoint, txtTransactionDate, txtReferenceNo);
    }

    @Override
    public void resetFields() {
        super.resetFields();
        initCashierSessionInfo();
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        super.initInterface();

        initCashierSessionInfo();
    }

    @Override
    protected void initObject() throws ObjectCreatorException {
        super.initObject();

        com.mac.zsystem.transaction.cashier.object.CashierClosing cashierClosing = (com.mac.zsystem.transaction.cashier.object.CashierClosing) getValueAbstract();
        cashierClosing.setCashierPoint(cashierSession.getCashierPoint());
        cashierClosing.setEmployee(cashierSession.getEmployee());
        cashierClosing.setCashierSession(cashierSession);
    }

    @Override
    protected Class getObjectClass() {
        return com.mac.zsystem.transaction.cashier.object.CashierClosing.class;
    }
}
