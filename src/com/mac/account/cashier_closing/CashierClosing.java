/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.cashier_closing;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zsystem.session_button.SessionButton;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.cashier.CashierSessionStatus;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chathu
 */
public class CashierClosing extends AbstractRegistrationForm {

    public CashierClosing() {
        splitPane.setDividerLocation(680);
        initOthers();
    }
    
    private void initOthers() {
         this.sessionButton = new SessionButton();
        System.out.println("getTransactionTypeCode()____"+SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE);
        this.sessionButton.setTransactionType(SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE);
        this.sessionButton.setDatabseService(getDatabaseService());
    }
    

    @Override
    public Class getObjectClass() {
        return com.mac.zsystem.transaction.cashier.object.CashierClosing.class;
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCCashierClosing() {
            @Override
            protected CashierSession getCashierSession() {
                try {
                    return SystemCashier.getCurrentCashierSession(getDatabaseService());
                } catch (ApplicationException ex) {
                    Logger.getLogger(CashierClosing.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            }
        };
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Index No", new String[]{"indexNo"}),
//            new CTableColumn("Cashier Session", new String[]{"CashierSession","toString" }),
            new CTableColumn("Transaction Date", new String[]{"transactionDate"}),
            new CTableColumn("Cashier Point", new String[]{"cashierPoint", "name"}),
            new CTableColumn("Employee", new String[]{"employee"}),
            new CTableColumn("Total Coins", new String[]{"totalCoin"}),
            new CTableColumn("Total Note", new String[]{"totalNote"}),
            new CTableColumn("Other", new String[]{"other"}),
            new CTableColumn("Total Cash", new String[]{"totalCash"}),});
    }

    @Override
    protected int getRegistrationType() {
        return NEW_ONLY_REGISTRATION_TYPE;
    }

    @Override
    protected int save(Object object) throws DatabaseException {
        com.mac.zsystem.transaction.cashier.object.CashierClosing cashierClosing = (com.mac.zsystem.transaction.cashier.object.CashierClosing) object;

        CashierSession cashierSession = cashierClosing.getCashierSession();
        cashierSession.setStatus(CashierSessionStatus.CLOSED);

        Integer transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE,
                cashierClosing.getReferenceNo(),
                cashierClosing.getDocumentNo(),
                null,
                cashierSession.getIndexNo(),//cashier session ID
                null,
                null);

        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.saveAccountTransaction(
                getDatabaseService(),
                transaction,
                SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE,
                AccountTransactionType.AUTO,
                AccountSettingCreditOrDebit.CREDIT,
                cashierClosing.getTotalCash(),
                cashierClosing.getCashierPoint().getAccountByCashierAccount().getCode(),
                "Cashier closing - " + cashierClosing.getCashierPoint().getName(),0);
        accountTransaction.saveAccountTransaction(
                getDatabaseService(),
                transaction,
                SystemTransactions.CASHIER_CLOSE_TRANSACTION_CODE,
                AccountTransactionType.AUTO,
                AccountSettingCreditOrDebit.DEBIT,
                cashierClosing.getTotalCash(),
                cashierClosing.getCashierPoint().getAccountByMainAccount().getCode(),
                "Cashier closing - " + cashierClosing.getCashierPoint().getName(),0);
        getDatabaseService().save(cashierClosing);
        getDatabaseService().save(cashierSession);


        return SAVE_SUCCESS;
    }

    @Override
    @Action
    public void doPrint() {
        this.sessionButton.doClick();
    }
    
     private SessionButton sessionButton;
    
    
}
