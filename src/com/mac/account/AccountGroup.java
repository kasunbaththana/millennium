/*
 *  AccountGroup.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 19, 2014, 11:29:57 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.account;

/**
 *
 * @author user
 */
public class AccountGroup {

    public static final String NONE = "NONE";
    public static final String BANK_ACCOUNT = "BANK_ACCOUNT";
    public static final String CASH_ACCOUNT = "CASH_ACCOUNT";
    public static final String EXPENSE_ACCOUNT = "EXPENSE_ACCOUNT";
    public static final String INCOME_ACCOUNT = "INCOME_ACCOUNT";
    public static final String FUND_ACCOUNT = "FUND_ACCOUNT";
    //all
    public static final String ACCOUNT_GROUPS[] = {
        NONE,
        CASH_ACCOUNT,
        INCOME_ACCOUNT,
        EXPENSE_ACCOUNT,
        BANK_ACCOUNT,
        FUND_ACCOUNT,
    };
}
