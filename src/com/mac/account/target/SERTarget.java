/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.target;

import com.mac.account.AccountGroup;
import com.mac.account.target.object.Employee;
import com.mac.account.target.object.EmployeeTarget;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mohan
 */
public class SERTarget extends AbstractService {

    public SERTarget(Component component) {
        super(component);
    }

    public List getBudgetAccounts() {
        try {
            Criteria criteria = getDatabaseService().initCriteria(Employee.class)
                    .add(Restrictions.eq("active", true));
            List<Employee> accounts = criteria.list();
            List<EmployeeTarget> budgetAccounts = new ArrayList<>();
            for (Employee account : accounts) {
                EmployeeTarget budgetAccount = new EmployeeTarget();
                budgetAccount.setAmount(0.0);
                budgetAccount.setEmployee(account);

                budgetAccounts.add(budgetAccount);
            }

            return budgetAccounts;
        } catch (DatabaseException | HibernateException hibernateException) {
            return new ArrayList();
        }

    }
}
