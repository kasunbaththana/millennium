/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.target;

import com.mac.account.AccountGroup;
import com.mac.account.target.object.EmployeeTarget;
import com.mac.account.target.object.Target;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 *
 * @author mohan
 */
public class REGTarget extends AbstractRegistrationForm<Target> {

    public REGTarget() {
        tblMain.getCTableModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                double totalTarget = 0.0;

                Set<EmployeeTarget> budgetAccounts = new HashSet<>(tblMain.getCValue());

                for (EmployeeTarget budgetAccount : budgetAccounts) {
                    totalTarget += budgetAccount.getAmount();
                }

                budget.setTotalTarget(totalTarget);

            }
        });
    }

    @Override
    public AbstractObjectCreator<Target> getObjectCreator() {
        budget = new PCTarget();
        return budget;
    }

    @Override
    public Class<? extends Target> getObjectClass() {
        return Target.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Employee Code", "employee", "code"),
                new CTableColumn("Account Name", "employee", "name"),
                new CTableColumn("Target", new String[]{"amount"}, true));
    }

    @Override
    protected List<Target> getTableData() throws DatabaseException {
        initService();
        return serBudget.getBudgetAccounts();


    }

    private void initService() {
        if (serBudget == null) {
            serBudget = new SERTarget(this);
        }
    }

    @Override
    protected Target getTableValueForObjectCreator(Object tableValue) {
        return null;
    }

    @Override
    protected int save(Target object) throws DatabaseException {
        Set<EmployeeTarget> budgetAccounts = new HashSet<>(tblMain.getCValue());
        Set<EmployeeTarget> selectedAccounts = new HashSet<>();

        for (EmployeeTarget budgetAccount : budgetAccounts) {
            if (budgetAccount.getAmount() > 0) {
                budgetAccount.setTarget(object);
                selectedAccounts.add(budgetAccount);
            }
        }


        object.setEmployeeTargets(selectedAccounts);

        int trasaction = SystemTransactions.insertTransaction(getDatabaseService(),
                SystemTransactions.EMPLOYEE_TARGET,
                object.getReferenceNo(),
                object.getDocumentNo(),
                null,
                null,
                null,
                null);

        object.setTransaction(trasaction);

        getDatabaseService().save(object);
        return SAVE_SUCCESS;
    }
    private SERTarget serBudget;
    private PCTarget budget;
}
