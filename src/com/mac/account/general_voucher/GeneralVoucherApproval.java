
package com.mac.account.general_voucher;


import com.mac.af.component.base.button.action.Action;
import java.util.Collection;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.transaction.SystemTransactions;

/**
 *
 * @author kasun
 */
public class GeneralVoucherApproval extends AbstractGridObject {
 
    public GeneralVoucherApproval() {
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.GENERAL_VOUCHER_TRANSACTION_APPROVE_CODE);
        this.recentButton.setDatabseService(getDatabaseService());
        
        
    }

    protected int loanindex;
    
    
    @Override
    @Action
    public void doPrint() {
        this.recentButton.doClick();
        
    }   
    
    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Index No", new String[]{"indexNo"}),
            new CTableColumn("Reference No", new String[]{"referenceNo"}),
            new CTableColumn("Date", new String[]{"tdate"}),
            new CTableColumn("Description", new String[]{"description"}),
            new CTableColumn("Total Amount", new String[]{"amount"})
        });
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        return new PCGeneralVoucherApprove(getService());
    }

    @Override
    protected Collection getTableData() {
        return getService().getGeneralVoucher();
        
    }

    private GeneralVoucherServiceInterface getService() {
        if (loanApproval == null) {
            loanApproval = initService();
        }

        return loanApproval;
    }

    public GeneralVoucherServiceInterface initService() {
        return new SERGeneralVoucherApproval(this);
    }
    
    
    

    private RecentButton recentButton;    
    private GeneralVoucherServiceInterface loanApproval;
}
