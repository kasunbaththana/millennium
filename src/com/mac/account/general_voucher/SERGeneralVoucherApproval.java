/*
 *  SERLoanApplication.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 20, 2014, 11:55:07 AM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.account.general_voucher;


import com.mac.account.ztemplate.cheque_transaction.ChequeStatus;
import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.object.GeneralVoucherDet;
import com.mac.zsystem.transaction.account.object.GeneralVoucherSum;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.payment.object.PaymentInformation;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction.object.Transaction;
import com.mac.zsystem.transaction.transaction_cancel.object.AccountTransaction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author user
 */
public class SERGeneralVoucherApproval extends AbstractService implements GeneralVoucherServiceInterface {

    public SERGeneralVoucherApproval(Component component) {
        super(component);
        
    }

    @Override
    public boolean acceptApplication(GeneralVoucherSum voucher) {
 

        try {
            //this is used to find who is approve
           SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.GENERAL_VOUCHER_TRANSACTION_APPROVE_CODE,
                voucher.getReferenceNo(),
                "",
                null, //loan
                SystemCashier.getCurrentCashierSession(getDatabaseService()).getIndexNo(), //cashier session
                null, //client
                "");
           
            Transaction T =(Transaction) getDatabaseService().initCriteria(Transaction.class)
                    .add(Restrictions.eq("indexNo", voucher.getTransaction()))
                    .uniqueResult();
            
            if(T.getIndexNo()!=null){
              String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction.object.Transaction "
                + "SET status=:STATUS "
                + "WHERE indexNo=:TRANSACTION";

            HashMap<String, Object> hashMapsettelment = new HashMap<>();
            hashMapsettelment.put("STATUS", "ACTIVE");
            hashMapsettelment.put("TRANSACTION", T.getIndexNo());
            getDatabaseService().executeUpdate(hql, hashMapsettelment);
            }
            //transaction begin
            Collection<GeneralVoucherDet> generalVoucherDet = getDatabaseService().getCollection("from com.mac.zsystem.transaction.account.object.GeneralVoucherDet where status='ACTIVE' and transaction='"+voucher.getTransaction()+"'");
            
            for(GeneralVoucherDet dets :generalVoucherDet){
                AccountTransaction accountTransaction=new AccountTransaction();
                GeneralVoucherDet detsDet=dets;
                
                if(dets.getCreditAmount()>0 || dets.getDebitAmount()>0){
                    
                accountTransaction.setAccount(dets.getAccount());
                accountTransaction.setAccountSetting(dets.getAccountSetting());
                accountTransaction.setBranch(dets.getBranch());
                accountTransaction.setCreditAmount(dets.getCreditAmount());
                accountTransaction.setDebitAmount(dets.getDebitAmount());
                accountTransaction.setDescription(dets.getDescription());
                accountTransaction.setStatus("ACTIVE");
                accountTransaction.setTransaction(dets.getTransaction());
                accountTransaction.setTransactionDate(dets.getTransactionDate());
                accountTransaction.setTransactionType(dets.getTransactionType());
                accountTransaction.setType(dets.getType());
                
                getDatabaseService().save(accountTransaction);
                }
                detsDet.setStatus("SENT");
                getDatabaseService().update(detsDet);
            }
            
            
            PaymentInformation c =(PaymentInformation) getDatabaseService().initCriteria(PaymentInformation.class)
                    .add(Restrictions.eq("paymentSetting.code", "GENERAL_VOUCHER_CHEQUE"))
                    .add(Restrictions.eq("transaction", voucher.getTransaction()))
                    .uniqueResult();
            
            if(c.getIndexNo()!=null){
              String hql = "UPDATE "
                + "com.mac.account.zobject.cheque.Cheque "
                + "SET status=:STATUS "
                + "WHERE paymentObject=:TRANSACTION";

            HashMap<String, Object> hashMapsettelment = new HashMap<>();
            hashMapsettelment.put("STATUS", ChequeStatus.PENDING);
            hashMapsettelment.put("TRANSACTION", c.getIndexNo());
            getDatabaseService().executeUpdate(hql, hashMapsettelment);
            }
            voucher.setStatus("SENT");
            getDatabaseService().save(voucher);
        
            
            mOptionPane.showMessageDialog(null, "General Voucher Approve successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
           
//                Map<String, Object> params = new HashMap<>();
//                params.put("TRANSACTION_NO", voucher.getTransaction());
//                TransactionUtil.PrintReport(getDatabaseService(), SystemTransactions.GENERAL_VOUCHER_TRANSACTION_CODE, params);
//     
            return true;
        } catch (Exception ex) {
            Logger.getLogger(SERGeneralVoucherApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to Approve General Voucher.", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

   

    @Override
    public boolean rejectApplication(GeneralVoucherSum voucher) {
        String note = mOptionPane.showInputDialog(null, "Please enter a note :", TITLE, mOptionPane.QUESTION_MESSAGE);
        //loan.setNote(note);
     

        try {
            //this is used to find who is approve
           SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.GENERAL_VOUCHER_TRANSACTION_APPROVE_CODE,
                voucher.getReferenceNo(),
                "",
                null, //loan
                SystemCashier.getCurrentCashierSession(getDatabaseService()).getIndexNo(), //cashier session
                null, //client
                note);
           
             Transaction T =(Transaction) getDatabaseService().initCriteria(Transaction.class)
                    .add(Restrictions.eq("indexNo", voucher.getTransaction()))
                    .uniqueResult();
            
            if(T.getIndexNo()!=null){
              String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction.object.Transaction "
                + "SET status=:STATUS "
                + "WHERE indexNo=:TRANSACTION";

            HashMap<String, Object> hashMapsettelment = new HashMap<>();
            hashMapsettelment.put("STATUS", "CANCEL");
            hashMapsettelment.put("TRANSACTION", T.getIndexNo());
            getDatabaseService().executeUpdate(hql, hashMapsettelment);
            }
            
               Collection<GeneralVoucherDet> generalVoucherDet = getDatabaseService().getCollection("from com.mac.zsystem.transaction.account.object.GeneralVoucherDet where status='ACTIVE' and transaction='"+voucher.getTransaction()+"'");
            
            for(GeneralVoucherDet dets :generalVoucherDet){
                
                GeneralVoucherDet detsDet=dets;
                
                detsDet.setStatus("CANCEL");
                getDatabaseService().update(detsDet);
            }
            
            
            PaymentInformation c =(PaymentInformation) getDatabaseService().initCriteria(PaymentInformation.class)
                    .add(Restrictions.eq("paymentSetting.code", "GENERAL_VOUCHER_CHEQUE"))
                    .add(Restrictions.eq("transaction", voucher.getTransaction()))
                    .uniqueResult();
            
       
            
            if(c.getIndexNo()!=null){
              String hql = "UPDATE "
                + "com.mac.account.zobject.cheque.Cheque "
                + "SET status=:STATUS "
                + "WHERE paymentObject=:TRANSACTION";

            HashMap<String, Object> hashMapsettelment = new HashMap<>();
            hashMapsettelment.put("STATUS", "C");
            hashMapsettelment.put("TRANSACTION", c.getIndexNo());
            getDatabaseService().executeUpdate(hql, hashMapsettelment);
            }
           
            voucher.setStatus("CANCEL");
            getDatabaseService().save(voucher);
            
           
            mOptionPane.showMessageDialog(null, "General Voucher rejected", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(SERGeneralVoucherApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to reject General Voucher", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    @Override
    public List<GeneralVoucherSum> getGeneralVoucher() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("status", "PENDING");
            list = getDatabaseService().getCollection("from com.mac.zsystem.transaction.account.object.GeneralVoucherSum where status=:status", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERGeneralVoucherApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    private void save(Object object) throws Exception {
        Thread.sleep(1000000);
        getDatabaseService().save(object);
    }
    public static final String TITLE = "Loan Approval";
    

}
