/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.general_voucher;

import com.mac.account.AccountTransactionCancel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.transaction.SystemTransactionStatus;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction_cancel.SERTransactionCancel;
import com.mac.zsystem.transaction.transaction_cancel.object.Transaction;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author KASUN
 */
public class GenaralVoucherCancel extends AccountTransactionCancel{

    SERTransactionCancel sERTransactionCancel;
    @Override
    protected String getTransactionType() {
        return SystemTransactions.GENERAL_VOUCHER_TRANSACTION_CODE;
    }

    @Override
    protected Date getTransactionDate() {
        return (Date)CApplication.getSessionVariable(CApplication.WORKING_DATE);
    }
    
    @Override
    protected void cancelAdditional(Transaction transaction) throws DatabaseException {
        SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.TRANSACTION_CANCEL_TRANSACTION_CODE,
                transaction.getReferenceNo(),
                transaction.getDocumentNo(),
                null,
                null,
                null,
                "General Voucher Cancel");

      
        //transaction cancel
        cancelTransaction(transaction.getIndexNo());
        //cancel account transaction
        cancelAccountTransaction(transaction.getIndexNo());
        //cancel payment payment
        cancelPaymentTransaction(transaction.getIndexNo());
        
       
        
    }
        public void cancelAccountTransaction(int transaction) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.AccountTransaction "
                + "SET status=:STATUS "
                + "WHERE transaction=:TRANSACTION";

        HashMap<String, Object> hashMapsettelment = new HashMap<>();
        hashMapsettelment.put("STATUS", SettlementStatus.SETTLEMENT_CANCEL);
        hashMapsettelment.put("TRANSACTION", transaction);
        getDatabaseService().executeUpdate(hql, hashMapsettelment);

    }
          public void cancelTransaction(int indexNo) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.Transaction "
                + "SET status=:STATUS "
                + "WHERE indexNo=:INDEX_NO";

        HashMap<String, Object> params = new HashMap<>();
        params.put("INDEX_NO", indexNo);
        params.put("STATUS", SystemTransactionStatus.CANCEL);

        getDatabaseService().executeUpdate(hql, params);
    }
           public void cancelPaymentTransaction(int transaction) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.Payment "
                + "SET status=:STATUS "
                + "WHERE transaction=:TRANSACTION";

        HashMap<String, Object> hashMapsettelment = new HashMap<>();
        hashMapsettelment.put("STATUS", SettlementStatus.SETTLEMENT_CANCEL);
        hashMapsettelment.put("TRANSACTION", transaction);

        getDatabaseService().executeUpdate(hql, hashMapsettelment);
    }
          
    
    
}
