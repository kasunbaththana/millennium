
package com.mac.account.general_voucher;



import com.mac.zsystem.transaction.account.object.GeneralVoucherSum;
import java.util.List;

/**
 *
 * @author kasun
 */
public interface GeneralVoucherServiceInterface {

    public boolean acceptApplication(GeneralVoucherSum voucher);

    public boolean rejectApplication(GeneralVoucherSum voucher);

    public List<GeneralVoucherSum> getGeneralVoucher();
}
