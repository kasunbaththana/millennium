package com.mac.account.cheque_realize;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mac.account.zobject.cheque.Cheque;
import com.mac.account.zobject.cheque.ChequeDetails;
import com.mac.account.ztemplate.cheque_transaction.AbstractChequeTransaction;
import com.mac.account.ztemplate.cheque_transaction.ChequeStatus;
import com.mac.account.ztemplate.cheque_transaction.custom_object.ChequeTransactionObject;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.loan.TemperaryReceiptStatus;
import com.mac.loan.zobject.TemperaryReceipt;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author SMTK
 */
public class ChequeRealize extends AbstractChequeTransaction {

//    @Override
//    public String executeText() {
//        return "Realize";
//    }
//
//    @Override
//    public String getChequeNewStatus() {
//        return ChequeStatus.REALIZE;
//    }
//
//    @Override
//    public String getChequeOldStatus() {
//        return ChequeStatus.DEPOSIT;
//    }
    @Override
    protected String getTitle() {
        return "Cheque Realize";
    }

//    @Override
//    public String getChequeType() {
//        return ChequeType.CLIENT;
//    }
    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.CHEQUE_REALIZE_TRANSACTION_CODE;
    }

    @Override
    protected String getReferenceGeneratorKey() {
        return ReferenceGenerator.CHECQUE_REALIZE;
    }

//    @Override
//    protected String getCreditAccountSettingCode() {
//        return ChequeRealizeAccountInterface.REALIZE_AMOUNT_CREDIT_CODE;
//    }
//
//    @Override
//    protected String getDebitAccountSettingCode() {
//        return ChequeRealizeAccountInterface.REALIZE_AMOUNT_DEBIT_CODE;
//    }
    @Override
    protected List<Cheque> getCheques(ChequeTransactionObject transactionObject) {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("type", ChequeType.CLIENT);
            params.put("status", ChequeStatus.DEPOSIT);
            params.put("bank_account", transactionObject.getBankAccount());
            list = getDatabaseService().getCollection("from com.mac.account.zobject.cheque.Cheque where status=:status and type=:type and bankAccount=:bank_account", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(AbstractChequeTransaction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }

    private int getTransNo(int paymentObj) {
        int transactionNo = 0;
        try {
            String sql = "SELECT payment_information.`transaction` FROM \n"
                    + "cheque\n"
                    + "LEFT JOIN `payment_information` ON \n"
                    + "`cheque`.`payment_object`=`payment_information`.`index_no`\n"
                    + "WHERE `cheque`.`payment_object`='" + paymentObj + "'";
            ResultSet executeQuery = getConnection().createStatement().executeQuery(sql);
            if (executeQuery.next()) {
                transactionNo = executeQuery.getInt(1);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return transactionNo;
    }

    @Override
    protected void saveCheques(ChequeTransactionObject transactionObject, Collection<Cheque> cheques) throws DatabaseException {
//        TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                getTransactionTypeCode(),
                transactionObject.getReferenceNo(),
                transactionObject.getDocumentNo(),
                null,
                null,
                null,
                transactionObject.getNote());

        //CHEQUES
//        getDatabaseService().beginLocalTransaction();
        for (Cheque cheque : cheques) {
            cheque.setStatus(ChequeStatus.REALIZE);
            cheque.setBankAccount(transactionObject.getBankAccount());
            cheque.setRealizeDate(transactionObject.getAccountDate());

           
            
            //SAVE TEMPARARY RECEIPT
            int transNo = getTransNo(cheque.getPaymentObject());
            System.out.println("transNo__"+transNo);
            TemperaryReceipt uniqueResult = (TemperaryReceipt) getDatabaseService()
                    .initCriteria(TemperaryReceipt.class)
                    .add(Restrictions.eq("transaction", transNo)).uniqueResult();
            if(uniqueResult!=null)
            {
            TemperaryReceipt temperaryReceipt = uniqueResult;
            temperaryReceipt.setStatus(TemperaryReceiptStatus.REALIZE);
            getDatabaseService().update(temperaryReceipt); 
            }
             getDatabaseService().save(cheque);
           
        }
// save cheque datails
            ChequeDetails   ch_details=new ChequeDetails();
            for(Cheque cheque_detail : cheques)
            {
                ch_details.setChequeIndex(cheque_detail);
                ch_details.setTransaction(transactionIndex);
                ch_details.setDescription(SystemTransactions.CHEQUE_REALIZE_TRANSACTION_CODE);
                ch_details.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                ch_details.setAmount(cheque_detail.getAmount());
                ch_details.setStatus("ACTIVE");
                
                
                getDatabaseService().save(ch_details);
            getDatabaseService().commitLocalTransaction();
            }




//        getDatabaseService().commitLocalTransaction();

        //ACCOUNT TRANSACTION
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        for (Cheque cheque : cheques) {
            
            
            accountTransaction.saveAccountTransactionChque(
                            getDatabaseService(),
                            transactionIndex,
                           getTransactionTypeCode(),
                            AccountTransactionType.AUTO,
                            AccountSettingCreditOrDebit.CREDIT,
                            cheque.getAmount(),
                            transactionObject.getBankAccount().getPendingAccount(),
                            "Cheque Realize From "+transactionObject.getBankAccount().getPendingAccount(),0,
                            transactionObject.getAccountDate());
            
            accountTransaction.saveAccountTransactionChque(
                            getDatabaseService(),
                            transactionIndex,
                            getTransactionTypeCode(),
                            AccountTransactionType.AUTO,
                            AccountSettingCreditOrDebit.DEBIT,
                            cheque.getAmount(),
                            transactionObject.getBankAccount().getValueAccount(),
                            "Cheque Realize From "+transactionObject.getBankAccount().getValueAccount(),0,
                            transactionObject.getAccountDate());
            
            
//            
//            accountTransaction.addAccountTransactionQueue(
//                    ChequeRealizeAccountInterface.REALIZE_AMOUNT_CREDIT_CODE,
//                    "Cheque Realize Amount",
//                    cheque.getAmount(),
//                    AccountTransactionType.AUTO);
//
//            accountTransaction.addAccountTransactionQueue(
//                    ChequeRealizeAccountInterface.REALIZE_AMOUNT_DEBIT_CODE,
//                    "Cheque Realize Amount",
//                    cheque.getAmount(),
//                    AccountTransactionType.AUTO);
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex, getTransactionTypeCode());
        }
    }

    @Override
    protected String getExecuteText() {
        return "Realize";
    }
}
