/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.general_receipt;

import com.mac.account.ztemplate.entry_payment.EntryPayment;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;

/**
 *
 * @author mohan
 */
public class GeneralReceipt extends EntryPayment {

    @Override
    protected String getChequeType() {
        return ChequeType.CLIENT;
    }

    @Override
    protected String referenceGeneratorPrefix() {
        return ReferenceGenerator.GENERAL_RECEIPT;
    }

    @Override
    public String getTransactionName() {
        return "General Receipt";
    }

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.GENERAL_RECEIPT_TRANSACTION_CODE;
    }

    @Override
    public String getAccountEntryType() {
        return ACCOUNT_ENTRY_CREDIT_ONLY;
    }
}
