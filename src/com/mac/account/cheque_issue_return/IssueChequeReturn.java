/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.account.cheque_issue_return;

import com.mac.account.zobject.cheque.Cheque;
import com.mac.account.ztemplate.cheque_transaction.AbstractChequeTransaction;
import com.mac.account.ztemplate.cheque_transaction.ChequeStatus;
import com.mac.account.ztemplate.cheque_transaction.custom_object.ChequeTransactionObject;
import com.mac.af.core.database.DatabaseException;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeIssueReturnAccountInterface;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SMTK
 */
public class IssueChequeReturn extends AbstractChequeTransaction {

//    @Override
//    public String executeText() {
//        return "Return";
//    }
//
//    @Override
//    public String getChequeNewStatus() {
//        return ChequeStatus.RETURN;
//    }
//
//    @Override
//    public String getChequeOldStatus() {
//        return ChequeStatus.DEPOSIT;
//    }
    @Override
    protected String getTitle() {
        return "Issue Cheque Return";
    }

//    @Override
//    public String getChequeType() {
//        return ChequeType.CLIENT;
//    }
    @Override
    protected String getTransactionTypeCode() {
        return SystemTransactions.CHEQUE_ISSUE_RETURN_TRANSACTION_CODE;
    }

    @Override
    protected String getReferenceGeneratorKey() {
        return ReferenceGenerator.CHECQUE_RETURN;
    }

//    @Override
//    protected String getCreditAccountSettingCode() {
//        return ChequeReturnAccountInterface.RETURN_AMOUNT_CREDIT_CODE;
//    }
//
//    @Override
//    protected String getDebitAccountSettingCode() {
//        return ChequeReturnAccountInterface.RETURN_AMOUNT_DEBIT_CODE;
//    }
    @Override
    protected List<Cheque> getCheques(ChequeTransactionObject transactionObject) {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("type", ChequeType.COMPANY);
             params.put("BankAccount", transactionObject.getBankAccount().getAccountNumber());
             params.put("codeaccount", transactionObject.getBankAccount().getCode());
             params.put("status", ChequeStatus.ISSUE);
            list = getDatabaseService().getCollection("from com.mac.account.zobject.cheque.Cheque where "
                    + "status=:status and type=:type and (accountNo=:BankAccount or accountNo=:codeaccount ) ", params);
            
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(AbstractChequeTransaction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    protected void saveCheques(ChequeTransactionObject transactionObject, Collection<Cheque> cheques) throws DatabaseException {
//        TRANSACTION
        int transactionIndex = SystemTransactions.insertTransaction(
                getDatabaseService(),
                getTransactionTypeCode(),
                transactionObject.getReferenceNo(),
                transactionObject.getDocumentNo(),
                null,
                null,
                null,
                transactionObject.getNote());
        
      //CHEQUES
     
        getDatabaseService().beginLocalTransaction();
        for (Cheque cheque : cheques) {
            cheque.setStatus(ChequeStatus.RETURN);
            cheque.setBankAccount(transactionObject.getBankAccount());
            cheque.setReturnDate(transactionObject.getAccountDate());

//            String call = "CALL z_update_temp_receipt(" + cheque.getIndexNo() + ")";
//             getDatabaseService().callUpdateProcedure(call);

            getDatabaseService().save(cheque);
        }
        getDatabaseService().commitLocalTransaction();
   //TEMPORY RECEIPT
         
        
        
        
        
        //ACCOUNT TRANSACTION
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        for (Cheque cheque : cheques) {
           
            
//accountTransaction.addAccountTransactionQueue(
//                   ChequeIssueReturnAccountInterface.ISSUE_RETURN_CREDIT_CODE,
//                    "Issue Checque Return ",
//                    cheque.getAmount(),
//                    AccountTransactionType.AUTO);
            

//transactionObject.getBankAccount().getValueAccount()
//            accountTransaction.addAccountTransactionQueue(
//                    ChequeIssueReturnAccountInterface.ISSUE_RETURN_DEBIT_CODE,
//                     "Issue Checque Return ",
//                    cheque.getAmount(),
//                    AccountTransactionType.AUTO);
           
            
             accountTransaction.saveAccountTransaction(
                            getDatabaseService(),
                            transactionIndex,
                            SystemTransactions.CHEQUE_ISSUE_RETURN_TRANSACTION_CODE,
                            AccountTransactionType.AUTO,
                            AccountSettingCreditOrDebit.DEBIT,
                           cheque.getAmount(),
                            transactionObject.getBankAccount().getValueAccount(),
                            "Return Cheque Issue",
                            0 );
             
               accountTransaction.addAccountTransactionQueue(
                   ChequeIssueReturnAccountInterface.ISSUE_RETURN_CREDIT_CODE,
                    "Return Cheque Issue",
                    cheque.getAmount(),
                    AccountTransactionType.AUTO);
        
        
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transactionIndex,SystemTransactions.CHEQUE_ISSUE_RETURN_TRANSACTION_CODE);
        }
      
    }

    @Override
    protected String getExecuteText() {
        return "Return";
    }
}
