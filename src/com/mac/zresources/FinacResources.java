/*
 *  FinacResources.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 20, 2014, 11:50:55 AM
 *  Copyrights Channa Mohan, All rights reserved.
 *  
 */
package com.mac.zresources;

import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author mohan
 */
public class FinacResources {

    public static URL getImageIconURL(String path) {
        return FinacResources.class.getResource(path);
    }

    public static ImageIcon getImageIcon(String path) {
        ImageIcon icon;
        try {
            icon = new ImageIcon(FinacResources.class.getResource(path));
        } catch (Exception e) {
            icon = null;
        }

        return icon;
    }

    public static ImageIcon getImageIcon(String path, int height, int width) {
        ImageIcon icon;
        try {
            icon = new ImageIcon(new ImageIcon(FinacResources.class.getResource(path)).getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
        } catch (Exception e) {
            icon = null;
        }

        return icon;
    }
    //REGISTRATION
    public static final String REGISTRATION_PEOPLE = "icons/registration/people.png";
    public static final String REGISTRATION_EMPLOYEE = "icons/registration/employee.png";
    public static final String REGISTRATION_SUPLIER = "icons/registration/suplier.png";
    public static final String REGISTRATION_CLIENT = "icons/registration/client.png";
    public static final String REGISTRATION_COMPANY = "icons/registration/company.png";
    public static final String REGISTRATION_BRANCH = "icons/registration/branch.png";
    public static final String REGISTRATION_LEAVE_DAY = "icons/registration/leave_day.png";
    public static final String REGISTRATION_LOAN = "icons/registration/loan.png";
    public static final String REGISTRATION_LOAN_GROUP = "icons/registration/loan_group.png";
    public static final String REGISTRATION_LOAN_TYPE = "icons/registration/loan_type.png";
    public static final String REGISTRATION_ROUTE = "icons/registration/route.png";
    public static final String REGISTRATION_ACCOUNT = "icons/registration/account.png";
    public static final String REGISTRATION_ACCOUNT_CATEGORY = "icons/registration/account_category.png";
    public static final String REGISTRATION_BANK = "icons/registration/bank.png";
    public static final String REGISTRATION_BANK_BRANCH = "icons/registration/bank_branch.png";
    public static final String REGISTRATION_HP_ITEM = "icons/registration/hp_item.png";
    public static final String REGISTRATION_HP_ITEM_CATEGORY = "icons/registration/hp_item_category.png";
    public static final String REGISTRATION_HP_ITEM_DEPARTMENT = "icons/registration/hp_item_department.png";
    public static final String REGISTRATION_CASHIER_POINT = "icons/registration/cashier_point.png";
    //LOAN
    public static final String LOAN = "icons/loan/loan.png";
    public static final String CANCEL = "icons/loan/cancel.png";
    public static final String AUTHORIZATION = "icons/loan/Authorization.png";
    public static final String OPEN_LOAN = "icons/loan/loan_open.png";
    public static final String LOAN_TYPE = "icons/loan/loan_type.png";
    public static final String LOAN_APPLICATION = "icons/loan/loan_application.png";
    public static final String LOAN_APPLICATION2 = "icons/loan/loan_application2.png";
    public static final String OPENING_LOAN2 = "icons/loan/opening_loan2.png";
    public static final String LOAN_APPROVAL = "icons/loan/loan_approval.png";
    public static final String LOAN_CLOSE = "icons/loan/loan_close.png";
    public static final String LOAN_OPENING = "icons/loan/opening_loan.png";
    public static final String LOAN_VOUCHER = "icons/loan/voucher.png";
    public static final String LOAN_RECEIPT = "icons/loan/receipt.png";
    public static final String LOAN_TEMPERERY_RECEIPT = "icons/loan/temperery_receipt.png";
    public static final String LOAN_OTHER_CHARGES = "icons/loan/other_charges.png";
    public static final String TRANSACTION_s = "icons/loan/transaction.png";
    public static final String SARVING = "icons/loan/saving.png";
    public static final String SARVING_TYPE = "icons/loan/serving_type.png";
    public static final String SARVING_SCHEMA = "icons/loan/saving_schema.png";
    public static final String SARVING_ACCOUNT = "icons/loan/saving_account.png";
    public static final String SARVING_DEPOSIT = "icons/loan/saving_deposit.png";
    public static final String SARVING_WITHDRAW = "icons/loan/saving_withdraw.png";
    public static final String OTHER_CHARGES_CANCEL="icons/loan/other_charges.png";
    public static final String LOAN_OPENING_BALANCE = "icons/loan/opening_balance.png";
    public static final String LOAN_REBIT = "icons/loan/rebit.png";
    public static final String LOAN_JOURNAL = "icons/loan/journal.png";
    public static final String LOAN_MULTI_TRANSACTION = "icons/loan/multi_transaction.png";
    public static final String REBIT_CANCEL = "icons/loan/multi_transaction.png";
    public static final String VEHICLE_VALUATION = "icons/loan/vehicle_valuation.png";
    //ACTION
    public static final String ACTION_ACCEPT = "icons/action/accept.png";
    public static final String ACTION_SUSPAND = "icons/action/suspand.png";
    public static final String ACTION_REJECT = "icons/action/reject.png";
    public static final String ACTION_START = "icons/action/start.png";
    public static final String ACTION_NEXT_DATE = "icons/action/next_date.png";
    //APPLICATION MENU
    public static final String APPLICATION_MENU_THEME = "icons/application_menu/theme.png";
    public static final String APPLICATION_MENU_CONFIG = "icons/application_menu/config.png";
    public static final String APPLICATION_MENU_CACHE_MANAGER = "icons/application_menu/cache_manager.png";
    public static final String APPLICATION_MENU_TASK_MANAGER = "icons/application_menu/task_manager.png";
    //ACCOUNT
    public static final String ACCOUNT_CHEQUE_ISSUE = "icons/account/cheque_issue.png";
    public static final String ACCOUNT_CHEQUE_REALIZE = "icons/account/cheque_realize.png";
    //SETTINGS
    public static final String TRANSACTION = "icons/system/settings/transaction.png";
    public static final String PAYMENT_SETTING = "icons/system/settings/payment_setting.png";
    public static final String ACCOUNTT_SETTING = "icons/system/settings/account_setting.png";
    
    public static final String PAYMENT_SETTING1 = "icons/system/settings/payment_setting1.png";
    public static final String ACCOUNTT_SETTING1 = "icons/system/settings/account_setting1.png";
    public static final String TRANSACTION_SETTING = "icons/system/settings/transaction_setting.png";
    public static final String SETTINGS_FOLDER = "icons/system/settings/settings _folder.png";
    public static final String SETTING = "icons/system/settings/setting.png";
    //CASHIER
    public static final String CASHIER_CLOSE = "icons/cashier/cashier_close.png";
    public static final String CASHIER = "icons/cashier/cashier.png";
    //REPORT
    public static final String REPORT = "icons/report/report.png";
    //BANK
    public static final String BANK_DEPOSIT = "icons/bank/bank_deposit.png";
    
    public static final String BLACK_LIST = "icons/system/black_list.png";
    
    public static final String ROUTE = "icons/system/route.png";
    public static final String RECEIPT = "icons/system/receipt.png";
    
    public static final String SOUND_ALERT = "sound/alert_x.wav";
    
    public static final String MICRO_CENTER = "icons/micro_finance/center.png";
    public static final String MICRO_GROUP = "icons/micro_finance/group.png";
    public static final String MICRO_FUND = "icons/micro_finance/fund.png";
    
    public static final String ADVANCE_SCHEME = "icons/micro_finance/advance.png";
    
    
    
}
