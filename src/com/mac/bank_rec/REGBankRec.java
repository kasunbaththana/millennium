/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.bank_rec;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.bank_rec.object.AccountTransaction;
import com.mac.bank_rec.object.BankReconcilationInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Udayanga
 */
public class REGBankRec extends AbstractGridObject {

    private SERBankRec sERBankRec;
    private SERBankRec sERBankRec2;
    //
    private List<AccountTransaction> data;
    private List<AccountTransaction> accountTransactions = null;
    // 
    private int recallCheck = 0;
    private int recallCheckSec = 0;
    //
    private int tableFill = 0;
    private int tableFillSec = 0;
    //
    private String accountCodeNo;
    private Date date;
    //
    double resultBeforeReconcil = 0.0;
    double reconcilAmount = 0.0;

    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Date", "transactionDate"),
            new CTableColumn("Description", "transactionData"),
            new CTableColumn("Check No", "checkNo"),
            new CTableColumn("Debit Amount", "debitAmount"),
            new CTableColumn("Credit Amount", "creditAmount"),
            new CTableColumn("Reconcil", new String[]{"transactionStatus"}, true)
        });
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        return new PCBank_Rec() {
            @Override
            public List tblData() {
                return getDataTable();
            }

            @Override
            public void getReCallData() {
                refreshTable();
                recallCheck = 1;
                recallCheckSec = 1;
            }

            @Override
            public void fillTable(String accountCode, Date dates) {
                accountCodeNo = accountCode;
                date = dates;
                refreshTable();
                tableFill = 1;
                tableFillSec = 1;
                calcBalanceBeforeReconcil();
            }

            @Override
            public double executer() {
                return resultBeforeReconcil;
            }

            @Override
            public void saveBankReconcilTableData() {
                saveBankReconcilData();
            }

            @Override
            public double getReconcilAmount() {
                return reconcilAmount;
            }

            @Override
            public void reFreshVariable() {
                resultBeforeReconcil = 0.0;
                reconcilAmount = 0.0;
            }
        };
    }

    @Override
    protected Collection getTableData() {
        if (tableFill == 1) {
            if (recallCheck == 1) {
                try {
                    sERBankRec = new SERBankRec(this);
                    accountTransactions = sERBankRec.getAllData();
                    for (AccountTransaction accountTransaction : accountTransactions) {
                        if (accountTransaction.getReconcil() == Byte.parseByte("1")) {
                            accountTransaction.setTransactionStatus(true);
                        } else {
                            accountTransaction.setTransactionStatus(false);
                        }
                    }
                    recallCheck = 0;
                    return accountTransactions;
                } catch (DatabaseException ex) {
                    Logger.getLogger(REGBankRec.class.getName()).log(Level.SEVERE, null, ex);
                    return new ArrayList();
                }
            } else {
                try {
                    sERBankRec2 = new SERBankRec(this);
                    data = sERBankRec2.getData(accountCodeNo, date);
                    accountCodeNo = "";
                    date = null;

                    return data;

                } catch (DatabaseException ex) {
                    Logger.getLogger(REGBankRec.class.getName()).log(Level.SEVERE, null, ex);
                    return new ArrayList();
                }
            }
        }
        tableFill = 0;
        return new ArrayList();
    }

    public List getDataTable() {
        return data;
    }

    public void calcBalanceBeforeReconcil() {
        try {
            double balanceBeforeReconcil = 0.0;
            sERBankRec = new SERBankRec(this);
            List<AccountTransaction> transactions = sERBankRec.getBalanceBeforeReconcilData();
            for (AccountTransaction accountTransaction : transactions) {
                balanceBeforeReconcil += (accountTransaction.getDebitAmount() - accountTransaction.getCreditAmount());
            }
            resultBeforeReconcil = balanceBeforeReconcil;
        } catch (DatabaseException ex) {
            Logger.getLogger(REGBankRec.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveBankReconcilData() {
        if (tableFillSec == 1 && recallCheckSec == 1) {
            List<AccountTransaction> accountTransactionsRecall = accountTransactions;
            for (AccountTransaction accountTransaction : accountTransactionsRecall) {
                try {
                    BankReconcilationInfo bankReconcilationInfo = new BankReconcilationInfo();
                    //bankReconcilationInfo.setTransaction(accountTransaction.getTransaction());
                    bankReconcilationInfo.setAccountTransaction(accountTransaction);
                    bankReconcilationInfo.setAccount(accountTransaction.getAccount());
                    bankReconcilationInfo.setStatus(Byte.parseByte("1"));

                    accountTransaction.setReconcil(Byte.parseByte("2"));

                    sERBankRec.save(bankReconcilationInfo);
                    sERBankRec.saveBankReconcilation(accountTransaction);

                } catch (DatabaseException ex) {
                    Logger.getLogger(REGBankRec.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            tableFillSec = 0;
            recallCheckSec = 0;
        } else {

            double reconcilAmountSum = 0.0;

            List<AccountTransaction> accountTransactionsLoadData = data;
            System.out.println("oooooooooooooooooooooooo___"+accountTransactionsLoadData);
            for (AccountTransaction accountTransaction : accountTransactionsLoadData) {
                if (accountTransaction.isTransactionStatus() == true) {
                    try {
                        BankReconcilationInfo bankReconcilationInfo = new BankReconcilationInfo();
                        //bankReconcilationInfo.setTransaction(accountTransaction.getTransaction());
                        bankReconcilationInfo.setAccountTransaction(accountTransaction);
                        bankReconcilationInfo.setAccount(accountTransaction.getAccount());
                        bankReconcilationInfo.setStatus(Byte.parseByte("1"));
                        accountTransaction.setReconcil(Byte.parseByte("2"));

                        reconcilAmountSum += (accountTransaction.getDebitAmount() - accountTransaction.getCreditAmount());
                        reconcilAmount = reconcilAmountSum;
                        //
                        sERBankRec.save(bankReconcilationInfo);
                        sERBankRec.saveBankReconcilation(accountTransaction);
                    } catch (DatabaseException ex) {
                        Logger.getLogger(REGBankRec.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            reconcilAmount =0.0;
            resultBeforeReconcil=0.0;
        }
    }
}
