/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.bank_rec;

import com.mac.bank_rec.object.AccountTransaction;
import com.mac.bank_rec.object.Transaction;

/**
 *
 * @author Udayanga
 */
public class BankRecCustomObj {
    
    private Transaction transaction;
    private AccountTransaction accountTransactions ;

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public AccountTransaction getAccountTransactions() {
        return accountTransactions;
    }

    public void setAccountTransactions(AccountTransaction accountTransactions) {
        this.accountTransactions = accountTransactions;
    }

   
}
