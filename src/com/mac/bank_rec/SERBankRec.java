/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.bank_rec;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.bank_rec.object.AccountTransaction;
import com.mac.bank_rec.object.BankAccount;
import com.mac.bank_rec.object.BankReconcilationInfo;
import java.awt.Component;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Udayanga
 */
public class SERBankRec extends AbstractService {

    public SERBankRec(Component component) {
        super(component);
    }

    public List<BankAccount> getBankAccountList() throws DatabaseException {
        List<BankAccount> bankAccounts = getDatabaseService().getCollection("from com.mac.bank_rec.object.Account where account_group='BANK_ACCOUNT'");
        return bankAccounts;
    }

    public List<AccountTransaction> getData(String Code,Date date) throws DatabaseException {
        System.out.println("mmmmmmmmmmmm"+date);
        return getDatabaseService().getCollection("from com.mac.bank_rec.object.AccountTransaction where account=" + Code + " and transaction_date <='"+date+"' and reconcil='0' ");
    }

    public List<AccountTransaction> getAllData() throws DatabaseException {
        //return getDatabaseService().getCollection("from com.mac.bank_rec.object.AccountTransaction where reconcil='0' OR reconcil='1'");
        return getDatabaseService().getCollection("from com.mac.bank_rec.object.AccountTransaction where reconcil='1'");
    }

    public void saveBankReconcilation(AccountTransaction accountTransaction) throws DatabaseException {
        getDatabaseService().update(accountTransaction);
    }

    public List<AccountTransaction> getBalanceBeforeReconcilData()throws DatabaseException{
        return getDatabaseService().getCollection("from com.mac.bank_rec.object.AccountTransaction where reconcil='2'");
    }
    
    public void save(BankReconcilationInfo bankReconcilationInfo) throws DatabaseException {
        getDatabaseService().save(bankReconcilationInfo);
    }
}
