/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.bank_rec;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.resources.ApplicationResources;
import com.mac.bank_rec.object.Account;
import com.mac.bank_rec.object.AccountTransaction;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Udayanga
 */
public abstract class PCBank_Rec extends DefaultObjectCreator {

    private SERBankRec sERBankRec;

    public abstract List tblData();

    public abstract void getReCallData();

    public abstract void fillTable(String accountCode, Date date);

    public abstract double executer();

    public abstract void saveBankReconcilTableData();

    public abstract double getReconcilAmount();

    public abstract void reFreshVariable();

    /**
     * Creates new form PCBank_Rec
     */
    
    public PCBank_Rec() {
        initComponents();
        initOthers();
    }

    @SuppressWarnings("unchecked")
    public void initOthers() {
        sERBankRec = new SERBankRec(this);

        txtRefferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.BANK_RECONCILATION));
        txtRefferenceNo.resetValue();
        txtDate.resetValue();

        ActionUtil actionUtil = new ActionUtil(this);

        actionUtil.setAction(btnNew, "doNew");
        actionUtil.setAction(btnDelete, "doDelete");
        actionUtil.setAction(btnSave, "doSave");
        actionUtil.setAction(btnLoadData, "doLoadData");
        actionUtil.setAction(btnHold, "doHold");
        actionUtil.setAction(btnRecall, "doRecall");

        btnNew.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_NEW_ICON));
        btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_NEW_ICON));
        btnDelete.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_DELETE_ICON));
        btnLoadData.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON));
        btnHold.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_DISCARD_ICON));
        btnRecall.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_REFRESH_ICON));

    }

    @Action
    public void doNew() {
        this.setNewMood();
    }

    @Action
    public void doSave() {
        this.saveBankReconcilTableData();
        
    }

    @Action
    public void doRecall() {
        getReCallData();
//        double balanceBeforeReconcil = 0.0;
//        double balanceAfterReconcil = 0.0;
//        double Result = 0.0;
//
//        Collection<AccountTransaction> accountTransactions = test();
//        for (AccountTransaction accountTransaction : accountTransactions) {
//            if (accountTransaction.isTransactionStatus() == true) {
//                balanceBeforeReconcil += (accountTransaction.getDebitAmount() - accountTransaction.getCreditAmount());
//            }
//            balanceAfterReconcil += (accountTransaction.getDebitAmount() - accountTransaction.getCreditAmount());
//
//        }
//        Result = (balanceBeforeReconcil - balanceAfterReconcil);
//        txtBalanceBeforeReconcil.setCValue(balanceBeforeReconcil);
//        txtBalanceAfterReconcil.setCValue(Result);
    }

    @Action
    public void doLoadData() {
        Account account = (Account) cboBankAccount.getCValue();
        fillTable(account.getCode(), txtAsAtDate.getCValue());
        txtBalanceBeforeReconcil.setCValue(executer());
        txtReconcilAmount.setCValue(getReconcilAmount());
        txtBalanceAfterReconcil.setCValue(executer() - getReconcilAmount());
//        reFreshVariable();
    }

    @Action
    public void doHold() {

        Collection<AccountTransaction> accountTransactions = tblData();

        for (AccountTransaction accountTransaction : accountTransactions) {
            if (accountTransaction.isTransactionStatus() == true) {
                try {
                    accountTransaction.setReconcil(Byte.parseByte("1"));
                    sERBankRec.saveBankReconcilation(accountTransaction);
                    sERBankRec.saveBankReconcilation(accountTransaction);
                } catch (DatabaseException ex) {
                    Logger.getLogger(PCBank_Rec.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public List getBankAccountList() {
        try {
            return sERBankRec.getBankAccountList();
        } catch (DatabaseException ex) {
            Logger.getLogger(PCBank_Rec.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cCButton1 = new com.mac.af.component.derived.command.button.CCButton();
        cCButton2 = new com.mac.af.component.derived.command.button.CCButton();
        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtRefferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cboBankAccount = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getBankAccountList();
            }
        };
        txtAsAtDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBalanceBeforeReconcil = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtReconcilAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBalanceAfterReconcil = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        btnLoadData = new com.mac.af.component.derived.command.button.CCButton();
        btnRecall = new com.mac.af.component.derived.command.button.CCButton();
        btnHold = new com.mac.af.component.derived.command.button.CCButton();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        btnDelete = new com.mac.af.component.derived.command.button.CCButton();
        btnNew = new com.mac.af.component.derived.command.button.CCButton();
        txtDate = new com.mac.af.component.derived.input.textfield.CIDateField();

        cCButton1.setText("cCButton1");

        cCButton2.setText("cCButton2");

        cDLabel1.setText("Refferance No:");

        cDLabel2.setText("Document No:");

        txtDocumentNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDocumentNoActionPerformed(evt);
            }
        });

        cDLabel3.setText("Date:");

        cDLabel4.setText("Date As At:");

        cDLabel5.setText("Bank Account:");

        cDLabel6.setText("Balance Before Reconcil:");

        txtBalanceBeforeReconcil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBalanceBeforeReconcilActionPerformed(evt);
            }
        });

        txtReconcilAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReconcilAmountActionPerformed(evt);
            }
        });

        cDLabel7.setText(" Reconcil Amount:");

        cDLabel8.setText("Balance After Reconcil:");

        txtBalanceAfterReconcil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBalanceAfterReconcilActionPerformed(evt);
            }
        });

        btnLoadData.setText("Load Data");

        btnRecall.setText("Recall");

        btnHold.setText("Hold");

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");

        btnNew.setText("New");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtRefferenceNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAsAtDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboBankAccount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnHold, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRecall, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtReconcilAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtBalanceBeforeReconcil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtBalanceAfterReconcil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLoadData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(txtRefferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAsAtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboBankAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLoadData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBalanceBeforeReconcil, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReconcilAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBalanceAfterReconcil, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(btnHold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRecall, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtDocumentNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDocumentNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDocumentNoActionPerformed

    private void txtBalanceBeforeReconcilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBalanceBeforeReconcilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBalanceBeforeReconcilActionPerformed

    private void txtReconcilAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReconcilAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReconcilAmountActionPerformed

    private void txtBalanceAfterReconcilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBalanceAfterReconcilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBalanceAfterReconcilActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSaveActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnDelete;
    private com.mac.af.component.derived.command.button.CCButton btnHold;
    private com.mac.af.component.derived.command.button.CCButton btnLoadData;
    private com.mac.af.component.derived.command.button.CCButton btnNew;
    private com.mac.af.component.derived.command.button.CCButton btnRecall;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.command.button.CCButton cCButton1;
    private com.mac.af.component.derived.command.button.CCButton cCButton2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboBankAccount;
    private com.mac.af.component.derived.input.textfield.CIDateField txtAsAtDate;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBalanceAfterReconcil;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBalanceBeforeReconcil;
    private com.mac.af.component.derived.input.textfield.CIDateField txtDate;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtReconcilAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtRefferenceNo;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList(txtRefferenceNo,
                txtDocumentNo,
                txtDate,
                txtAsAtDate,
                txtBalanceBeforeReconcil,
                txtReconcilAmount,
                txtBalanceAfterReconcil);
    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtRefferenceNo, "transaction", "referenceNo"),
                new CInputComponentBinder(txtDocumentNo, "transaction", "documentNo"),
                new CInputComponentBinder(txtDate, "transaction", "transactionDate"));
    }

    @Override
    protected Class getObjectClass() {
        return BankRecCustomObj.class;
    }

    @Override
    public void setNewMood() {
        txtRefferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(true);
        txtDate.setValueEditable(false);
        btnSave.setVisible(true);
        btnNew.setVisible(false);

    }

    @Override
    public void setIdleMood() {
        txtRefferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(false);
        txtDate.setValueEditable(false);
        btnSave.setVisible(false);
        //
        txtBalanceBeforeReconcil.setValueEditable(false);
        txtReconcilAmount.setValueEditable(false);
        txtBalanceAfterReconcil.setValueEditable(false);
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(txtDate,
                txtRefferenceNo);
    }
}
