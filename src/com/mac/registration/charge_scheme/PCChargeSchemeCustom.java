/*
 *  PCChargeSchemeCustom.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 17, 2015, 12:54:33 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.registration.charge_scheme;

import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.registration.charge_scheme.object.ChargeSchemeCustom;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class PCChargeSchemeCustom extends DefaultObjectCreator<ChargeSchemeCustom> {

    /**
     * Creates new form PCChargeSchemeCustom
     */
    public PCChargeSchemeCustom() {
        initComponents();
        initOthers();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        radioButtonGroup = new CRadioButtonGroup();
        radioButtonGroup.setDefaultRadioButton(radochrgAmont);
        radioButtonGroup.addRadioButton(radochrgAmont);
        radioButtonGroup.addRadioButton(radoChargRate);

        radochrgAmont.setValue(ChargeSchemeType.CUSTOM_AMOUNT);
        radoChargRate.setValue(ChargeSchemeType.CUSTOM_RATE);

        txtChargeRate.setValueEditable(false);
        radoChargRate.addEnabilityComponent(txtChargeRate);
        radochrgAmont.addEnabilityComponent(txtChargeAmount);

        radioButtonGroup.setCValue(ChargeSchemeType.CUSTOM_AMOUNT);
        radioButtonGroup.setDefaultRadioButton(radochrgAmont);

        radochrgAmont.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtChargeRate.setCValue(0.00);

            }
        });
        radoChargRate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtChargeAmount.setCValue(0.00);
                 txtChargeRate.setValueEditable(true);

            }
        });
        txtChargeRate.setValueEditable(false);
//        System.out.println("xxxxxxxxxxxxx_" + radioButtonGroup.getCValue());
//        switch (radioButtonGroup.getCValue().toString()) {
//            case ChargeSchemeType.CUSTOM_CHARG:
//                txtChargeRate.setValueEditable(false);
//                txtChargeAmount.setValueEditable(true);
//                break;
//            case ChargeSchemeType.CUSTOM_RATE:
//                txtChargeAmount.setValueEditable(false);
//                txtChargeRate.setValueEditable(true);
//
//                break;
//            default:
//                throw new AssertionError();
//        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtFromAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtToAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtChargeAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtChargeRate = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        radochrgAmont = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        radoChargRate = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtRent = new com.mac.af.component.derived.input.textfield.CIIntegerField();

        cDLabel1.setText("From Amount:");

        cDLabel2.setText("To Amount:");

        txtChargeAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtChargeAmountActionPerformed(evt);
            }
        });

        txtChargeRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtChargeRateActionPerformed(evt);
            }
        });

        radochrgAmont.setText("Charg Amount :");

        radoChargRate.setText("Charg Rate :");
        radoChargRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radoChargRateActionPerformed(evt);
            }
        });

        cDLabel3.setText("OR Rental:");

        cDLabel4.setForeground(new java.awt.Color(255, 0, 0));
        cDLabel4.setText("Worninig! Do not use From ,\nTo Amount and Rent Both Its not work");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtToAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtFromAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(radochrgAmont, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(radoChargRate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(4, 4, 4)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtChargeRate, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(txtChargeAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtRent, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFromAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtToAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRent, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radochrgAmont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtChargeAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radoChargRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtChargeRate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void radoChargRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radoChargRateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radoChargRateActionPerformed

    private void txtChargeAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtChargeAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtChargeAmountActionPerformed

    private void txtChargeRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtChargeRateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtChargeRateActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton radoChargRate;
    private com.mac.af.component.derived.input.radiobutton.CIRadioButton radochrgAmont;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtChargeAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtChargeRate;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtFromAmount;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtRent;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtToAmount;
    // End of variables declaration//GEN-END:variables
    private CRadioButtonGroup radioButtonGroup;

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList(
                txtFromAmount,
                txtToAmount,
                txtChargeAmount,
                txtRent,
                txtChargeRate);
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList();
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtFromAmount, "fromAmount"),
                new CInputComponentBinder(txtToAmount, "toAmount"),
                new CInputComponentBinder(txtRent, "rent"),
                new CInputComponentBinder(radioButtonGroup, "type"));
    }

    @Override
    protected Class<? extends ChargeSchemeCustom> getObjectClass() {
        return ChargeSchemeCustom.class;
    }

    @Override
    protected void afterInitObject(ChargeSchemeCustom object) throws ObjectCreatorException {
        switch (radioButtonGroup.getCValue().toString()) {
            case ChargeSchemeType.CUSTOM_AMOUNT:
                object.setAmount(txtChargeAmount.getCValue());
                break;
            case ChargeSchemeType.CUSTOM_RATE:
                object.setAmount(txtChargeRate.getCValue());
                break;
            default:
                throw new AssertionError();
        }
    }

    @Override
    protected List getUneditableComponents() {
      return Arrays.asList(
                txtChargeRate);
        
    }
    
    
}
