/*
 *  REGChargeScheme.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 17, 2015, 12:18:26 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.registration.charge_scheme;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.registration.charge_scheme.object.ChargeScheme;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class REGChargeScheme extends AbstractRegistrationForm<ChargeScheme> {

    @Override
    public AbstractObjectCreator<ChargeScheme> getObjectCreator() {
        return new PCChargeScheme();
    }

    @Override
    public Class<? extends ChargeScheme> getObjectClass() {
        return ChargeScheme.class;
    }

    @Override
    public CTableModel<ChargeScheme> getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Code", "code"),
            new CTableColumn("Name", "name"),
            new CTableColumn("Type", "type")
        });
    }
    
       public List getAccouns() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.registration.charge_scheme.object.Account  where active=true");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(REGChargeScheme.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
