/*
 *  ChargeSchemeType.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 17, 2015, 11:50:01 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.registration.charge_scheme;

/**
 *
 * @author mohan
 */
public class ChargeSchemeType {

    public static final String VALUE = "VALUE";
    public static final String PERCENT = "PERCENT";
    public static final String CUSTOM = "CUSTOM";
    
    public static final String CUSTOM_AMOUNT = "CUSTOM_AMOUNT";
    public static final String CUSTOM_RATE = "CUSTOM_RATE";
    
}
