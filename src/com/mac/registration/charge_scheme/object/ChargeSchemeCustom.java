package com.mac.registration.charge_scheme.object;

/**
  *	@author Channa Mohan
  *	
  *	Created On Sep 21, 2015 3:35:35 PM 
  *	Mohan Hibernate Mapping Generator
  */



/**
 * ChargeSchemeCustom generated by hbm2java
 */
public class ChargeSchemeCustom  implements java.io.Serializable {


     private Integer indexNo;
     private ChargeScheme chargeScheme;
     private double fromAmount;
     private double toAmount;
     private String type;
     private double amount;
     private int rent;

    public ChargeSchemeCustom() {
    }

    public ChargeSchemeCustom(ChargeScheme chargeScheme, double fromAmount, double toAmount, String type, double amount) {
       this.chargeScheme = chargeScheme;
       this.fromAmount = fromAmount;
       this.toAmount = toAmount;
       this.type = type;
       this.amount = amount;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public ChargeScheme getChargeScheme() {
        return this.chargeScheme;
    }
    
    public void setChargeScheme(ChargeScheme chargeScheme) {
        this.chargeScheme = chargeScheme;
    }
    public double getFromAmount() {
        return this.fromAmount;
    }
    
    public void setFromAmount(double fromAmount) {
        this.fromAmount = fromAmount;
    }
    public double getToAmount() {
        return this.toAmount;
    }
    
    public void setToAmount(double toAmount) {
        this.toAmount = toAmount;
    }
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    public double getAmount() {
        return this.amount;
    }
    
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }
    



@Override
public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ChargeSchemeCustom) ) return false;
		 ChargeSchemeCustom castOther = ( ChargeSchemeCustom ) other; 

	if(!java.util.Objects.equals(this.chargeScheme, castOther.chargeScheme)) {
            return false;
     }
	if(!java.util.Objects.equals(this.fromAmount, castOther.fromAmount)) {
            return false;
     }
	if(!java.util.Objects.equals(this.toAmount, castOther.toAmount)) {
            return false;
     }
	if(!java.util.Objects.equals(this.type, castOther.type)) {
            return false;
     }
	if(!java.util.Objects.equals(this.amount, castOther.amount)) {
            return false;
     }
         
		 return true;
   }

   @Override
   public int hashCode() {
         int result = 17;
         
	result = result * 17 + java.util.Objects.hashCode(this.chargeScheme);
	result = result * 17 + java.util.Objects.hashCode(this.fromAmount);
	result = result * 17 + java.util.Objects.hashCode(this.toAmount);
	result = result * 17 + java.util.Objects.hashCode(this.type);
	result = result * 17 + java.util.Objects.hashCode(this.amount);

         return result;
   }   

}


