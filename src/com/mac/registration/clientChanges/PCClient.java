
package com.mac.registration.clientChanges;

import com.mac.registration.client.*;
import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.registration.client.object.Client;
import com.mac.registration.client.object.LoanGroup;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author kasun
 */
public class PCClient extends DefaultObjectCreator {

    private boolean isClient;
    private boolean isSupplier;
    //

    private String SET_MOOD="";

    private String type;
    /**
     * Creates new form PCClient
     */
    public PCClient(boolean isClient, boolean isSupplier,String TYPE) {
        initComponents();
        initOthers();
        this.isClient = isClient;
        this.isSupplier = isSupplier;
        this.type = TYPE;
    }

    

    private List<LoanGroup> getGroups() {
        List<LoanGroup> routes;

        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            routes = databaseService.getCollection("from com.mac.registration.client.object.LoanGroup");
        } catch (Exception e) {
            routes = new ArrayList<>();
            Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, e);
        }
        return routes;
    }


  
    @SuppressWarnings("unchecked")
    private void initOthers() {

   
        cboGroup.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LoanGroup group =(LoanGroup) cboGroup.getCValue();
                setAccCode(group);
                
            }
        });
     
        
        FocusAdapter adapter=new FocusAdapter() {
                
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e); 
                txtCode_Ref.setCValue(txtCode.getCValue());
                
            }
                        
            };
        
        
        txtCode.addFocusListener(adapter);
        
        
    }

 

    public void setAccCode(LoanGroup group){
        
        try {
            if(SET_MOOD.equals("EDIT")){
            if(group!=null){
              
            String group_code = group.getCode();
            String center_code = group.getCenter();
            int max_client = 0;
//             LoanCenter centers =  (LoanCenter) (CPanel.GLOBAL.getDatabaseService().initCriteria(LoanCenter.class))
//                     .add(Restrictions.eq("code", group.getCenter()))
//                     .uniqueResult();
//             
            if(group.getCode()!=null){
                
                
                   Criteria  c =  (CPanel.GLOBAL.getDatabaseService().initCriteria(Client.class)).
                    add(Restrictions.eq("center", group.getCenter())).
                    setProjection(Projections.count("code"));
                 max_client =(int) c.uniqueResult();
         
//          
                  
             
            }else{
                
                max_client = 1;
            }
       
            String max_code = String.format("%03d", max_client+1);
                
            String client_code = String.format("%s/%s",center_code,max_code);
            
           
            txtCode_Ref.setCValue(client_code);
           
            }

            }
        } catch (Exception e) {
        e.printStackTrace();
        }
        
    }
 

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        lblImage = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        cboGroup = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getGroups();
            }
        };
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNicNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtName = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtCode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtCode_Ref = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel27 = new com.mac.af.component.derived.display.label.CDLabel();

        cLabel2.setText("NIC No :");

        lblImage.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        cLabel1.setText("Code :");

        cLabel16.setText("Group :");

        cLabel4.setText("Name :");

        cLabel27.setText("Ref Code :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboGroup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtNicNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(112, 112, 112))
                            .addComponent(txtCode_Ref, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCode_Ref, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNicNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(365, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Basic Details", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel27;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboGroup;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblImage;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCode;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCode_Ref;
    private com.mac.af.component.derived.input.textfield.CIStringField txtName;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNicNo;
    // End of variables declaration//GEN-END:variables
public CRadioButtonGroup radioButtonGroup2;
    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList(
                (Component) txtCode);
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList(
                (Component) txtNicNo,
                (Component) txtCode_Ref,
                (Component) txtName//(Component) chkActive
                );
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                (Component) cboGroup
                );
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(txtNicNo,txtName,txtCode);
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtCode, "code"),
                new CInputComponentBinder(txtNicNo, "nicNo"),
                new CInputComponentBinder(txtName, "name"),
                new CInputComponentBinder(cboGroup, "groupCode"),
                new CInputComponentBinder(txtCode_Ref, "refCode")
                );
    }

    @Override
    protected Class getObjectClass() {
        return com.mac.registration.client.object.Client.class;
    }

    @Override
    public void resetFields() {
        super.resetFields();
//        jSlider1.setValue(0);
        lblImage.setCValue(null);
        txtNicNo.setEditable(true);
        txtNicNo.resetValue();
    }

    @Override
    public void setIdleMood() {
        super.setIdleMood();
       SET_MOOD = "IDLE";
    }

    @Override
    public void setNewMood() {
//        super.setNewMood();
//  
//       
//        lblImage.setIcon(null);
//         SET_MOOD ="NEW";
//         
//         System.out.println("type  "+type);
//        if(type!=null){
//        if(type.equals("MICRO")){
//            cboGroup.setVisible(true);
//        }else{
//            
//            cboGroup.setVisible(false);
//        }
//        
//        }
       
        
    }

    @Override
    public void setEditMood() {
        super.setEditMood();
        System.out.println("setEditMood ");
        cboGroup.setEnabled(true);
        cboGroup.setExpressEditable(true);
       SET_MOOD ="EDIT";
    }

    @Override
    protected void afterNewObject(Object object) {
        ((com.mac.registration.client.object.Client) object).setActive(true);
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        super.initInterface();


    }

    @Override
    protected void afterInitObject(Object object) throws ObjectCreatorException {
        super.afterInitObject(object);
        Client client = (Client) object;
        
        LoanGroup group= (LoanGroup) cboGroup.getCValue();
        
        client.setActive(false);
         client.setStatus(ClientStatus.ACTIVE);
         
         if(group!=null){
             client.setCenter(group.getCenter());
         }else{
             client.setCenter(null);
         }

    }
    
    
    private SessionFactory sessionFactory;
}
