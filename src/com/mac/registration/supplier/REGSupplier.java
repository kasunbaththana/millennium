/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.supplier;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import java.util.List;

/**
 *
 * @author Nimesh
 */
public class REGSupplier extends  AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.supplier.object.Client.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Mobile", new String[]{"mobile"}),
            new CTableColumn("Telephone 1", new String[]{"telephone1"}),
            new CTableColumn("Telephone 2", new String[]{"telephone2"}),
            new CTableColumn("Fax", new String[]{"fax"}),
            new CTableColumn("E mail", new String[]{"email"}),
            new CTableColumn("Active", new String[]{"active"})
        });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCSupplier(isClient(), isSuplier());
    }

    @Override
    protected List getTableData() throws DatabaseException {
        return getDatabaseService().getCollection("FROM com.mac.registration.supplier.object.Client WHERE supplier=true");
    }

    public Boolean isClient() {
        return false;
    }

    public Boolean isSuplier() {
        return true;
    }
}
