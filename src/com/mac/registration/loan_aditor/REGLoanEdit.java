/*
 *  REGClient.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.registration.loan_aditor;


import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import java.util.List;

/**
 *  
 * @author mohan
 */
public class REGLoanEdit extends AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.loan_aditor.object.Loan.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("AgreementNo", new String[]{"agreementNo"}),
            new CTableColumn("Client", new String[]{"client"}),
            new CTableColumn("LoanGroup", new String[]{"loanGroup"}),
            new CTableColumn("LoanType", new String[]{"loanType"}),
            new CTableColumn("LoanAmount", new String[]{"loanAmount"}),
            new CTableColumn("LoanDate", new String[]{"loanDate"}),
            new CTableColumn("PaymentTerm", new String[]{"paymentTerm"}),
            new CTableColumn("Status", new String[]{"status"})
        });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCLoanEdit();
    }

    @Override
    protected List getTableData() throws DatabaseException {
        return getDatabaseService().getCollection("FROM com.mac.registration.loan_aditor.object.Loan WHERE status='LOAN_START'");
    }

    @Override
    protected int save(Object object) throws DatabaseException {
        return super.save(object); //To change body of generated methods, choose Tools | Templates.
   
    
    }

   
}
