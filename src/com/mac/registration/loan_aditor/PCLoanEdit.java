/*
 *  PCClient.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.registration.loan_aditor;


import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.present_receipt.zobject.Loan;
import com.mac.registration.Salutation;
import com.mac.registration.client.object.Route;
import com.mac.registration.loan_aditor.object.Client;
import com.mac.registration.loan_aditor.object.Employee;
import com.mac.registration.loan_aditor.object.LoanGroup;
import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;

/**
 *
 * @author mohan
 */
public class PCLoanEdit extends DefaultObjectCreator {

    /**
     * Creates new form PCClient
     */
    public PCLoanEdit() {
        initComponents();
        initOthers();
    }

    private List<Client> getClient() {
        List<Client> client;

        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            client = databaseService.getCollection("from com.mac.registration.loan_aditor.object.Client");
        } catch (Exception e) {
            client = new ArrayList<>();
            Logger.getLogger(PCLoanEdit.class.getName()).log(Level.SEVERE, null, e);
        }
        return client;
    }
    private List<Client> getSupplier() {
        List<Client> client;

        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            client = databaseService.getCollection("from com.mac.registration.loan_aditor.object.Client where supplier=1 ");
        } catch (Exception e) {
            client = new ArrayList<>();
            Logger.getLogger(PCLoanEdit.class.getName()).log(Level.SEVERE, null, e);
        }
        return client;
    }
    private List<Employee> getEmployee() {
        List<Employee> employee;

        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            employee = databaseService.getCollection("from com.mac.registration.loan_aditor.object.Employee");
        } catch (Exception e) {
            employee = new ArrayList<>();
            Logger.getLogger(PCLoanEdit.class.getName()).log(Level.SEVERE, null, e);
        }
        return employee;
    }
    private List<LoanGroup> getLoanGroup() {
        List<LoanGroup> loanGroup;

        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            loanGroup = databaseService.getCollection("from com.mac.registration.loan_aditor.object.LoanGroup");
        } catch (Exception e) {
            loanGroup = new ArrayList<>();
            Logger.getLogger(PCLoanEdit.class.getName()).log(Level.SEVERE, null, e);
        }
        return loanGroup;
    }
public void saveloan()
{
    try {
            Loan loan=new Loan();
            
            loan.setIndexNo(loan.getIndexNo());
            loan.setAgreementNo(txtagreement.getCValue());
            loan.setClient(cboClient.getCValue().toString());
            loan.setGraceDays(txtGraceDay.getCValue());
            loan.setLoanGroup(cboloanGroup.getCValue().toString());
            loan.setLoanOfficer(cboLoanOfficer.getCValue().toString());
            loan.setRecoveryOfficer(cboRecoveryOfficer.getCValue().toString());
            loan.setSupplier(cbosupplier.getCValue().toString());
            
        
         HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
        databaseService.save(loan);
        
    } catch (Exception e) {
        e.printStackTrace();
    }
}
   

    @SuppressWarnings("unchecked")
    private void initOthers() {


//        cboRoute.setExpressEditable(true);
//        cboloanGroup.setExpressEditable(true);

//        cboClient.setTableModel(new RouteTableModel());
//        cboClient.setRegistration("Client Registration", com.mac.registration.route.REGRoute.class);

    
        //fourcing
        txtLoanType.addFocusListener(new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {
              
                cboLoanOfficer.requestFocus();
            }
            
        });
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtagreement = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtLoanType = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtPayTerm = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtLoanStatus = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        txtInstallmentamount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel17 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel18 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txFirstDueDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cboloanGroup = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoanGroup();
            }
        };
        cboLoanOfficer = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getEmployee();
            }
        };
        cboRecoveryOfficer = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getEmployee();
            }
        };
        txtGraceDay = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        txtinscount = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        txtinsrate = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtPanalty = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cboClient = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        cbosupplier = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getSupplier();
            }
        };

        txtLoanStatus.setPreferredSize(new java.awt.Dimension(79, 16));

        cLabel1.setText("Agreement No:");

        cLabel2.setText("Client :");

        cLabel3.setText("Loan Group:");

        cLabel4.setText("Loan Type:");

        cLabel5.setText("Loan Amount:");

        cLabel7.setText("Installment Amount:");

        cLabel8.setText("First Due Date:");

        cLabel9.setText("Paymen term:");

        cLabel10.setText("Panalty:");

        cLabel11.setText("Loan Officer:");

        cLabel12.setText("Recovery Officer:");

        cLabel13.setText("Loan Status:");

        cLabel16.setText("Interest Rate:");

        cLabel17.setText("Ins Count:");

        cLabel18.setText("Grace Days:");

        cLabel14.setText("Supplier:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtLoanAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cboClient, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboRecoveryOfficer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtPayTerm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtLoanStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtagreement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtLoanType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txFirstDueDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtPanalty, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtGraceDay, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(cboloanGroup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboLoanOfficer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(128, 128, 128)
                                                .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(txtInstallmentamount, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtinscount, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                                        .addComponent(txtinsrate, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(cbosupplier, javax.swing.GroupLayout.DEFAULT_SIZE, 415, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtagreement, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboClient, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboloanGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLoanType, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtinscount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtInstallmentamount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtinsrate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txFirstDueDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPayTerm, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtGraceDay, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPanalty, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoanOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboRecoveryOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLoanStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbosupplier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(68, 68, 68))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cLabel17;
    private com.mac.af.component.derived.display.label.CDLabel cLabel18;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cLabel9;
    private com.mac.af.component.derived.input.textfield.CIStringField cboClient;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanOfficer;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboRecoveryOfficer;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboloanGroup;
    private com.mac.af.component.derived.input.combobox.CIComboBox cbosupplier;
    private com.mac.af.component.derived.input.textfield.CIDateField txFirstDueDate;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtGraceDay;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtInstallmentamount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtLoanStatus;
    private com.mac.af.component.derived.input.textfield.CIStringField txtLoanType;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPanalty;
    private com.mac.af.component.derived.input.textfield.CIStringField txtPayTerm;
    private com.mac.af.component.derived.input.textfield.CIStringField txtagreement;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtinscount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtinsrate;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList(
                (Component) txtagreement);
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList(
                
                );
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                (Component) cboClient,
                (Component) cboloanGroup,
                (Component) txtLoanType,
                (Component) txtPanalty,
                (Component) txtLoanAmount,
                (Component) txtInstallmentamount,
                (Component) txtinscount,
                (Component) txtinsrate,
                (Component) txFirstDueDate,
                (Component) txtPayTerm,
                (Component) txtPanalty,
                (Component) txtGraceDay,
                (Component) cboLoanOfficer,
                (Component) cboRecoveryOfficer,
                (Component) txtLoanStatus,
                (Component) cbosupplier
//                (Component) jSlider1
                );
    }

//    @Override
//    protected List getUneditableComponents() {
//        return Arrays.asList(txtDateOfBirth);
//    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtagreement, "agreementNo"),
                new CInputComponentBinder(cboClient, "client"),
                new CInputComponentBinder(cboloanGroup, "loanGroup"),
                new CInputComponentBinder(txtLoanType, "loanType"),
                new CInputComponentBinder(txtLoanAmount, "loanAmount"),
                new CInputComponentBinder(txtInstallmentamount, "installmentAmount"),
                new CInputComponentBinder(txtinscount, "installmentCount"),
                new CInputComponentBinder(txtinsrate, "interestRate"),
                new CInputComponentBinder(txFirstDueDate, "loanDate"),
                new CInputComponentBinder(txtPayTerm, "paymentTerm"),
                new CInputComponentBinder(txtPanalty, "panaltyRate"),
                new CInputComponentBinder(txtGraceDay, "graceDays"),
                new CInputComponentBinder(cboLoanOfficer, "employeeByLoanOfficer"),
                new CInputComponentBinder(cboRecoveryOfficer, "employeeByRecoveryOfficer"),
                new CInputComponentBinder(txtLoanStatus, "status"),
                new CInputComponentBinder(cbosupplier, "supplier")
                );
    }

    @Override
    protected Class getObjectClass() {
        return com.mac.registration.loan_aditor.object.Loan.class;
    }

    @Override
    public void resetFields() {
        super.resetFields();
//        jSlider1.setValue(0);
        
    }

    @Override
    public void setIdleMood() {
        super.setIdleMood();
        cboClient.setValueEditable(false);
        txtGraceDay.setValueEditable(false);
        txtInstallmentamount.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanStatus.setValueEditable(false);
        txFirstDueDate.setValueEditable(false);
        txtLoanType.setValueEditable(false);
        txtPanalty.setValueEditable(false);
        txtPayTerm.setValueEditable(false);
        txtagreement.setValueEditable(false);
        txtinscount.setValueEditable(false);
        txtinsrate.setValueEditable(false);
       cboLoanOfficer.setValueEditable(false);
       cboloanGroup.setValueEditable(false);
       cboRecoveryOfficer.setValueEditable(false);
       cbosupplier.setValueEditable(false);
    }

    

    @Override
    public void setEditMood() {
        super.setEditMood();
        cboClient.setValueEditable(false);
        txtGraceDay.setValueEditable(true);
        txtInstallmentamount.setValueEditable(false);
        txtLoanAmount.setValueEditable(false);
        txtLoanStatus.setValueEditable(false);
        txtLoanType.setValueEditable(false);
        txtPanalty.setValueEditable(false);
        txtPayTerm.setValueEditable(false);
        txtagreement.setValueEditable(true);
        txtinscount.setValueEditable(false);
        txtinsrate.setValueEditable(false);
       cboLoanOfficer.setValueEditable(true);
        txFirstDueDate.setValueEditable(false);
       cboloanGroup.setValueEditable(true);
       cboRecoveryOfficer.setValueEditable(true);
       cbosupplier.setValueEditable(true);
    }

    @Override
    public void setNewMood() {
         //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void afterNewObject(Object object) {
//        ((com.mac.registration.client.object.Client) object).setActive(true);
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        super.initInterface();

//        Client client = (Client) getValueAbstract();
//        if (client != null) {
//            lblStarMarks.setCValue(client.getStarMarks() + " Stars");
////            try {
////                ImageIcon icon = imageWriter.getImage(client.getCode());
////                if (icon != null) {
////                    lblImage.setIcon(getScaledImage(icon.getImage(), 70, 75));
////                }
////            } catch (SocketException ex) {
////                Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, ex);
////            } catch (IOException ex) {
////                Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, ex);
////            }
//        } else {
//            lblStarMarks.setCValue("0 Stars");
//        }

    }

    @Override
    protected void afterInitObject(Object object) throws ObjectCreatorException {
        super.afterInitObject(object);
//        Client client = (Client) object;
//        client.setClient(isClient);
//        client.setSupplier(isSupplier);
//        client.setStarMarks(0);
//        client.setActive(false);
//         client.setStatus(ClientStatus.ACTIVE);
//        try {
//            imageWriter.saveImage(file.getAbsolutePath(), client.getCode());
//        } catch (Exception e) {
//            Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, e);
//        }
    }
    
    
    private SessionFactory sessionFactory;
}
