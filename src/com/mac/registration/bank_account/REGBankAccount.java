/*
 *  REGBankAccount.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 4, 2014, 2:12:37 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.registration.bank_account;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.registration.bank_account.object.BankAccount;

/**
 *
 * @author mohan
 */
public class REGBankAccount extends AbstractRegistrationForm<BankAccount>{

    @Override
    public AbstractObjectCreator<BankAccount> getObjectCreator() {
        return new PCBankAccount();
    }

    @Override
    public Class<? extends BankAccount> getObjectClass() {
        return BankAccount.class;
    }

    @Override
    public CTableModel<BankAccount> getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Code","code"),
                new CTableColumn("Owner","owner"),
                new CTableColumn("Account Number","accountNumber"),
                new CTableColumn("Active","active")
                );
    }
    
}
