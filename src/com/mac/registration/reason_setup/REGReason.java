/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.reason_setup;


import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author soft-master
 */
public class REGReason extends AbstractRegistrationForm {
    
    @Override
    public  AbstractObjectCreator getObjectCreator(){
            return new PCReasonSetup();
    }

    @Override
    public Class getObjectClass() {
        return com.mac.registration.reason_setup.object.ReasonSetup.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
                    new CTableColumn("Code",new String[]{"code"}),
                    new CTableColumn("Name", new String[]{"description"})
                });
                
                
    }







}


