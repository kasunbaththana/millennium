/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.vehicle;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author NIMESH-PC
 */
public class REGVehicle extends AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.vehicle.object.Vehicle.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Vehicle No", new String[]{"vehicleNo"}),
            new CTableColumn("Model", new String[]{"model"}),
            new CTableColumn("Brand", new String[]{"brand"}),
            new CTableColumn("Engine Capacity", new String[]{"engineCapacity"}),
            new CTableColumn("Color", new String[]{"color"})
        });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCVehicle();
    }

}
