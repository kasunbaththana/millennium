/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.customer_chargs;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.registration.customer_chargs.object.CusChargs;

/**
 *
 * @author NIMESH-PC
 */
public class REGCustomerChargs extends AbstractRegistrationForm<CusChargs>{

   @Override
    public Class getObjectClass() {
        return CusChargs.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Amount", new String[]{"amount"}),
            new CTableColumn("Name", new String[]{"name"})});
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCCustomerChargs();
    }

}
