/*
 *  REGClient.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.registration.company;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zutil.system_settings.RegistrationsTypesStatus;
import com.mac.zutil.system_settings.SystemStatusCheck;

/**
 *
 * @author mohan
 */
public class REGCompany extends AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.company.object.Company.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"})
        });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCCompany();
    }

    @Override
    protected int getRegistrationType() {
        return new SystemStatusCheck().getRegistrationTypeFromDB(getDatabaseService()
                ,RegistrationsTypesStatus.REGCompany);
    }
}
