/*
 *  REGEmployee.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.registration.employee;

import com.mac.registration.employee.object.Employee;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author mohan
 */
public class REGEmployee extends AbstractRegistrationForm<Employee> {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.employee.object.Employee.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("NIC No", new String[]{"nicNo"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Prefix", new String[]{"prefix"}),
            new CTableColumn("Mobile", new String[]{"mobile"}),
            new CTableColumn("Telephone 1", new String[]{"telephone1"}),
            new CTableColumn("Telephone 2", new String[]{"telephone2"}),
            new CTableColumn("Fax", new String[]{"fax"}),
            new CTableColumn("Branch", new String[]{"branch"}),
            new CTableColumn("Login Access", new String[]{"loginAccess"}),
            new CTableColumn("Active", new String[]{"active"})});
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCEmployee();
    }

    @Override
    protected int save(Employee object) throws DatabaseException {
        if (object.getPhoto() != null) {
            getDatabaseService().save(object.getPhoto());
        }
        getDatabaseService().save(object);
        
        return SAVE_SUCCESS;
    }
}
