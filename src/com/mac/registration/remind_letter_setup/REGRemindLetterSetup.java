/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.remind_letter_setup;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author NIMESH-PC
 */
public class REGRemindLetterSetup extends AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.remind_letter_setup.object.RemindLetterSetup.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Min Level", new String[]{"minLevel"}),
            new CTableColumn("Max Level", new String[]{"maxLevel"})
        });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCRemindLetterSetup();
    }

    @Override
    protected int getRegistrationType() {
        return EDIT_ONLY_REGISTRATION_TYPE;
    }
}
