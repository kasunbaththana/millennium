/*
 *  REGBanckBranch.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 24, 2014, 4:04:18 PM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.registration.bank_branch;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author user
 */
public class REGBanckBranch extends AbstractRegistrationForm{

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCBankBranch();
    }

    @Override
    public Class getObjectClass() {
        return com.mac.registration.bank_branch.object.BankBranch.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Bank", new String[]{"bank"})
        });
    }
    
}
