/*
 *  REGLoanType.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.registration.loan_type;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.registration.loan_type.object.LoanDefaultCharge;
import com.mac.registration.loan_type.object.LoanType;

/**
 *
 * @author mohan
 */
public class REGLoanType extends AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.loan_type.object.LoanType.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Prefix", new String[]{"prefix"}),
            new CTableColumn("Minimum Allocation", new String[]{"minimumAllocation"}),
            new CTableColumn("Maximum Allocation", new String[]{"maximumAllocation"}),
            new CTableColumn("Minimum InterestRate", new String[]{"minimumInterestRate"}),
            new CTableColumn("Maximum InterestRate", new String[]{"maximumInterestRate"}),
            new CTableColumn("Panalty", new String[]{"panaltyAvailable"}),
            new CTableColumn("Active", new String[]{"active"}),
            new CTableColumn("Hp", new String[]{"ishp"}),
                });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCLoanType();
    }

    @Override
    protected int save(Object object) throws DatabaseException {
        LoanType loanType = (LoanType) object;
        for (LoanDefaultCharge loanDefaultCharge : loanType.getLoanDefaultCharges()) {
            loanDefaultCharge.setLoanType(loanType);
        }
        loanType.setAgrementNoWidth(5);
       // loanType.setLastAgrementNo(0);
        return super.save(object);
    }
}
//SystemSettlement settlement = SystemSettlement.getInstance();
//        settlement.beginSettlementQueue();
//        settlement.beginSettlementHistoryQueue();
//settlement.flushSettlementHistoryQueue(getDatabaseService());