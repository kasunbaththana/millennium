/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.loan_type;

import com.mac.registration.loan_type.object.Account;
import com.mac.registration.loan_type.object.LoanType;

/**
 *
 * @author thilanga
 */
public class LoanDefaultCharges {

    private String name;
    private double betweenFrom;
    private String betweenTo;
    private double amountRate;
    private double amountAmount;
    private LoanType loanType;
    //private LoanDefaultCharges loanDefaultCharges;
    private Account accountByDebitAccount;
    private boolean active;

    public LoanDefaultCharges() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBetweenFrom() {
        return betweenFrom;
    }

    public void setBetweenFrom(double betweenFrom) {
        this.betweenFrom = betweenFrom;
    }

    public String getBetweenTo() {
        return betweenTo;
    }

    public void setBetweenTo(String betweenTo) {
        this.betweenTo = betweenTo;
    }

    public LoanType getLoanType() {
        return loanType;
    }

    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }

//    public LoanDefaultCharges getLoanDefaultCharges() {
//        return loanDefaultCharges;
//    }
//
//    public void setLoanDefaultCharges(LoanDefaultCharges loanDefaultCharges) {
//        this.loanDefaultCharges = loanDefaultCharges;
//    }
//
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getChargeRate() {
        return amountRate;
    }

    public void setChargeRate(double amountRate) {
        this.amountRate = amountRate;
    }

    public double getChargeAmount() {
        return amountAmount;
    }

    public void setChargeAmount(double amountAmount) {
        this.amountAmount = amountAmount;
    }

    public Account getAccountByDebitAccount() {
        return accountByDebitAccount;
    }

    public void setAccountByDebitAccount(Account accountByDebitAccount) {
        this.accountByDebitAccount = accountByDebitAccount;
    }
    
}
