package com.mac.registration.loan_type.object;
// Generated Feb 24, 2021 10:47:51 PM by Hibernate Tools 3.2.1.GA



/**
 * AdvanceScheme generated by hbm2java
 */
public class AdvanceScheme  implements java.io.Serializable {


     private String code;
     private String name;
     private double amount;
     private String type;
     private String account;

    public AdvanceScheme() {
    }

	
    public AdvanceScheme(String code, String name, double amount, String account) {
        this.code = code;
        this.name = name;
        this.amount = amount;
        this.account = account;
    }
    public AdvanceScheme(String code, String name, double amount, String type, String account) {
       this.code = code;
       this.name = name;
       this.amount = amount;
       this.type = type;
       this.account = account;
    }
   
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public double getAmount() {
        return this.amount;
    }
    
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    public String getAccount() {
        return this.account;
    }
    
    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return   code + "-" + name;
    }

    



}


