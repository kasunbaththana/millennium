package com.mac.registration.loan_type.object;

/**
  *	@author Channa Mohan
  *	
  *	Created On Jun 18, 2015 4:09:12 PM 
  *	Mohan Hibernate Mapping Generator
  */



/**
 * LoanDefaultCharge generated by hbm2java
 */
public class LoanDefaultCharge  implements java.io.Serializable {


     private Integer indexNo;
     private Account accountByCreditAccount;
     private Account accountByDebitAccount;
     private LoanType loanType;
     private String name;
     private double betweenFrom;
     private double betweenTo;
     private String amountType;
     private double amountRate;
     private double amountValue;
     private boolean active;

    public LoanDefaultCharge() {
    }

	
    public LoanDefaultCharge(Account accountByCreditAccount, Account accountByDebitAccount, String name, double betweenFrom, double betweenTo, String amountType, double amountRate, double amountValue, boolean active) {
        this.accountByCreditAccount = accountByCreditAccount;
        this.accountByDebitAccount = accountByDebitAccount;
        this.name = name;
        this.betweenFrom = betweenFrom;
        this.betweenTo = betweenTo;
        this.amountType = amountType;
        this.amountRate = amountRate;
        this.amountValue = amountValue;
        this.active = active;
    }
    public LoanDefaultCharge(Account accountByCreditAccount, Account accountByDebitAccount, LoanType loanType, String name, double betweenFrom, double betweenTo, String amountType, double amountRate, double amountValue, boolean active) {
       this.accountByCreditAccount = accountByCreditAccount;
       this.accountByDebitAccount = accountByDebitAccount;
       this.loanType = loanType;
       this.name = name;
       this.betweenFrom = betweenFrom;
       this.betweenTo = betweenTo;
       this.amountType = amountType;
       this.amountRate = amountRate;
       this.amountValue = amountValue;
       this.active = active;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public Account getAccountByCreditAccount() {
        return this.accountByCreditAccount;
    }
    
    public void setAccountByCreditAccount(Account accountByCreditAccount) {
        this.accountByCreditAccount = accountByCreditAccount;
    }
    public Account getAccountByDebitAccount() {
        return this.accountByDebitAccount;
    }
    
    public void setAccountByDebitAccount(Account accountByDebitAccount) {
        this.accountByDebitAccount = accountByDebitAccount;
    }
    public LoanType getLoanType() {
        return this.loanType;
    }
    
    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public double getBetweenFrom() {
        return this.betweenFrom;
    }
    
    public void setBetweenFrom(double betweenFrom) {
        this.betweenFrom = betweenFrom;
    }
    public double getBetweenTo() {
        return this.betweenTo;
    }
    
    public void setBetweenTo(double betweenTo) {
        this.betweenTo = betweenTo;
    }
    public String getAmountType() {
        return this.amountType;
    }
    
    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }
    public double getAmountRate() {
        return this.amountRate;
    }
    
    public void setAmountRate(double amountRate) {
        this.amountRate = amountRate;
    }
    public double getAmountValue() {
        return this.amountValue;
    }
    
    public void setAmountValue(double amountValue) {
        this.amountValue = amountValue;
    }
    public boolean isActive() {
        return this.active;
    }
    
    public void setActive(boolean active) {
        this.active = active;
    }





	@Override
	public boolean equals(Object other) {
		if (this == other){
			return true;
		}
			 
		if (other == null){
			return false;
		}
		
		if ( !(other instanceof LoanDefaultCharge) ){
			return false;
		}
		
		LoanDefaultCharge castOther = ( LoanDefaultCharge ) other; 

		if(this.indexNo==null && castOther.indexNo==null) {
			return false;
		}
		
		if(!java.util.Objects.equals(this.indexNo, castOther.indexNo)) {
			return false;
		}
			 
		return true;
	}

    @Override
    public int hashCode() {
        int result = 17;
        
		result = result * 17 + java.util.Objects.hashCode(this.indexNo);

        return result;
    }





}


