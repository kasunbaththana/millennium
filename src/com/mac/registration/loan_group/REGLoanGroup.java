
package com.mac.registration.loan_group;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.loan.zobject.Loan;
import com.mac.registration.loan_group.object.LoanGroup;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author kasun
 */
public  class REGLoanGroup extends AbstractRegistrationForm {

   
    @Override
    public Class getObjectClass() {
        return com.mac.registration.loan_group.object.LoanGroup.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
                    new CTableColumn("Center", new String[]{"loanCenter"}),
                    new CTableColumn("Index No", new String[]{"code"}),
                    new CTableColumn("Group Code", new String[]{"name"})
                }
        );
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCLoanGroup();
    }

    @Override
    protected int save(Object object) throws DatabaseException {
   
        LoanGroup loanGroup = (LoanGroup) object;
        
        System.out.println(loanGroup.getLoanCenter());
        
        
       
        
       // return 1;
        return super.save(object); //To change body of generated methods, choose Tools | Templates.
        
        
    }
    
    
    
    
    
    
}
