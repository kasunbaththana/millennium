/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.remind_letter;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author Thilanga-pc
 */
public class REGTemplateRegistration extends AbstractRegistrationForm {

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCTemplateRegistration();
    }

    @Override
    public Class getObjectClass() {
        return com.mac.registration.remind_letter.object.LetterTemplate.class;

    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", "code"),
            new CTableColumn("Name", "name"),
            new CTableColumn("Text", "text")
        });

    }
}
