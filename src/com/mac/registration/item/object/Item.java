package com.mac.registration.item.object;

/**
  *	@author Channa Mohan
  *	
  *	Created On Nov 10, 2014 12:17:50 PM 
  *	Mohan Hibernate Mapping Generator
  */



/**
 * Item generated by hbm2java
 */
public class Item  implements java.io.Serializable {


     private String code;
     private Client client;
     private ItemCategory itemCategory;
     private String name;
     private Double costPrice;
     private Double salesPrice;

    public Item() {
    }

	
    public Item(String code) {
        this.code = code;
    }
    public Item(String code, Client client, ItemCategory itemCategory, String name, Double costPrice, Double salesPrice) {
       this.code = code;
       this.client = client;
       this.itemCategory = itemCategory;
       this.name = name;
       this.costPrice = costPrice;
       this.salesPrice = salesPrice;
    }
   
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    public Client getClient() {
        return this.client;
    }
    
    public void setClient(Client client) {
        this.client = client;
    }
    public ItemCategory getItemCategory() {
        return this.itemCategory;
    }
    
    public void setItemCategory(ItemCategory itemCategory) {
        this.itemCategory = itemCategory;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public Double getCostPrice() {
        return this.costPrice;
    }
    
    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }
    public Double getSalesPrice() {
        return this.salesPrice;
    }
    
    public void setSalesPrice(Double salesPrice) {
        this.salesPrice = salesPrice;
    }


	@Override
     public String toString() {
		return code + "-" + name;
     }




	@Override
	public boolean equals(Object other) {
        if ( (this == other ) ){
			return true;
		}
		
		if ( (other == null ) ){
			return false;
		}
		
		if ( !(other instanceof Item) ){
			return false;
		}
		
		Item castOther = ( Item ) other; 

		if(this.code==null && castOther.code==null) {
			return false;
		}
		
		if(!java.util.Objects.equals(this.code, castOther.code)) {
            return false;
		}
        
		return true;
   }

    @Override
    public int hashCode() {
        int result = 17;
         
		result = result * 17 + java.util.Objects.hashCode(this.code);

        return result;
    }




}


