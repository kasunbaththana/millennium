/*
 *  REGClient.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.registration.route;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author mohan
 */
public class REGRoute extends AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.route.object.Route.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"})
        });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCRoute();
    }
}
