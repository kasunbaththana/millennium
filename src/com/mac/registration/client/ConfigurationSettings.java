/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author Udayanga
 */
public class ConfigurationSettings {

    //ftp server
    private String server;
    private int portNo;
    private String userName;
    private String pasWord;
    private FTPClient fTPClient;
    //sms getway
    private String smsUrl;
    private String smsUserName;
    private String smsPassword;
    //email server
    private String emailAddress;
    private String emailPassword;

    public String getSmsUrl() {
        return smsUrl;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public String getServer() {
        return server;
    }

    public int getPortNo() {
        return portNo;
    }

    public String getUserName() {
        return userName;
    }

    public String getPasWord() {
        return pasWord;
    }

    public FTPClient getfTPClient() {
        return fTPClient;
    }

    public String getSmsUserName() {
        return smsUserName;
    }

    public String getSmsPassword() {
        return smsPassword;
    }

    public void readConfigurationSettings() {
        Properties properties = new Properties();
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(ImageFileStatus.FILE_NAME);
            properties.load(inputStream);
        } catch (IOException ex) {
            Logger.getLogger(ConfigurationSettings.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Logger.getLogger(ConfigurationSettings.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
        //ftp server
        server = properties.getProperty(ImageFileStatus.IP_ADDRESS);
        portNo = Integer.parseInt(properties.getProperty(ImageFileStatus.PORT_NO));
        userName = properties.getProperty(ImageFileStatus.USER_NAME);
        pasWord = properties.getProperty(ImageFileStatus.PASSWORD);
        //sms getwaty
        smsUrl = properties.getProperty(ImageFileStatus.SMS_URL);
        smsUserName = properties.getProperty(ImageFileStatus.SMS_GETWAY_USERNAME);
        smsPassword = properties.getProperty(ImageFileStatus.SMS_GETWAY_PASSWORD);
        //email server
        emailAddress = properties.getProperty(ImageFileStatus.EMAIL_ADDRESS);
        emailPassword = properties.getProperty(ImageFileStatus.EMAIL_PASSWORD);
    }

    public void writeConfigurationSettings() {
        Properties properties = new Properties();
        OutputStream output = null;
        File file = new File(ImageFileStatus.FILE_NAME);
        try {
            if (!file.exists()) {
                output = new FileOutputStream(file.getName());
                //ftp server
                properties.setProperty(ImageFileStatus.IP_ADDRESS, "");
                properties.setProperty(ImageFileStatus.PORT_NO, "");
                properties.setProperty(ImageFileStatus.USER_NAME, "");
                properties.setProperty(ImageFileStatus.PASSWORD, "");
                //sms getway
                properties.setProperty(ImageFileStatus.SMS_URL, "");
                properties.setProperty(ImageFileStatus.SMS_GETWAY_USERNAME, "");
                properties.setProperty(ImageFileStatus.SMS_GETWAY_PASSWORD, "");
                //email server
                properties.setProperty(ImageFileStatus.EMAIL_ADDRESS, "");
                properties.setProperty(ImageFileStatus.EMAIL_PASSWORD, "");
                properties.store(output, null);
            }
        } catch (IOException io) {
            Logger.getLogger(ConfigurationSettings.class.getName()).log(Level.SEVERE, null, io);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    Logger.getLogger(ConfigurationSettings.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
}
