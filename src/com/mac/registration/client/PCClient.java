
package com.mac.registration.client;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.derived.input.radiobutton.CRadioButtonGroup;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.af.resources.ApplicationResources;
import com.mac.registration.Salutation;
import com.mac.registration.client.object.Client;
import com.mac.registration.client.object.LoanGroup;
import com.mac.registration.client.object.Route;
import com.mac.zsystem.model.table_model.RouteTableModel;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author kasun
 */
public class PCClient extends DefaultObjectCreator {

    private boolean isClient;
    private boolean isSupplier;
    //
    private JFileChooser fileChooser;
    private BufferedImage image;
    private byte[] imageBytes;
    private ImageWriter imageWriter;
    private File file;
    private String SET_MOOD="";

    private String type;
    /**
     * Creates new form PCClient
     */
    public PCClient(boolean isClient, boolean isSupplier,String TYPE) {
        initComponents();
        initOthers();
        this.isClient = isClient;
        this.isSupplier = isSupplier;
        this.type = TYPE;
    }

    
    private List<Route> getRoutes() {
        List<Route> routes;

        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            routes = databaseService.getCollection("from com.mac.registration.client.object.Route");
        } catch (Exception e) {
            routes = new ArrayList<>();
            Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, e);
        }
        return routes;
    }
    private Boolean getIsExistClient(String nic) {
        List<Client> routes;

        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            routes = databaseService.getCollection("from com.mac.registration.client.object.Client where nicNo='"+nic+"'");
        } catch (Exception e) {
            routes = new ArrayList<>();
            Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, e);
        }
        if(routes.isEmpty()){
            return false;
        }
        return true;
    }
    private List<LoanGroup> getGroups() {
        List<LoanGroup> routes;

        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            routes = databaseService.getCollection("from com.mac.registration.client.object.LoanGroup");
        } catch (Exception e) {
            routes = new ArrayList<>();
            Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, e);
        }
        return routes;
    }

    private void resetBirthday() {
        if (!txtNicNo.getCValue().isEmpty() && txtNicNo.getCValue().length() == 10) {

            
            String nic = txtNicNo.getCValue().trim();
            int year = Integer.parseInt(nic.substring(0, 2));
            year = 1900 + year;
            int dayOfYear = Integer.parseInt(nic.substring(2, 5));
            if (dayOfYear > 500) {
                dayOfYear = dayOfYear - 500;
            }

            if (year % 4 != 0 && dayOfYear > 59) {
                dayOfYear = dayOfYear - 1;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
            calendar.set(Calendar.YEAR, year);

            txtDateOfBirth.setCValue(calendar.getTime());
        }else if (!txtNicNo.getCValue().isEmpty() && txtNicNo.getCValue().length() == 12) {
            String nic = txtNicNo.getCValue().trim();
            int year = Integer.parseInt(nic.substring(2, 4));
            year = 1900 + year;
            int dayOfYear = Integer.parseInt(nic.substring(4, 7));
            if (dayOfYear > 500) {  
                dayOfYear = dayOfYear - 500;
            }

            if (year % 4 != 0 && dayOfYear > 59) {
                dayOfYear = dayOfYear - 1;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
            calendar.set(Calendar.YEAR, year);

            txtDateOfBirth.setCValue(calendar.getTime());
            
            
        } else {
            txtDateOfBirth.setCValue(null);
        }
    }

    public List getReligon(){
        
        
        
        List<String> nliList =new ArrayList<>();
        
        nliList.add("Buddhism");
        nliList.add("Christianity");
        nliList.add("Hinduism");
        nliList.add("Islam");
        nliList.add("Judaism");
        nliList.add("Myth");
      
     
        return nliList;

    }
    @SuppressWarnings("unchecked")
    private void initOthers() {
        radioButtonGroup2 = new CRadioButtonGroup();
        imageWriter = new ImageWriter();

        cboRoute.setExpressEditable(true);
        cboSalutation.setExpressEditable(true);

        cboRoute.setTableModel(new RouteTableModel());
        cboRoute.setRegistration("Route Registration", com.mac.registration.route.REGRoute.class);

        txtDateOfBirth.setCValue(null);
        txtDateOfBirth.setEnabled(false);
//        txtMaxAllocation.
        chkConsiderMaximumAllocation.addComponent(txtMaximumAllocation);
        
        rdoFemale.setValue("FEMALE");
        rdoMale.setValue("MALE");
        
        radioButtonGroup2.addRadioButton(rdoFemale);
        radioButtonGroup2.addRadioButton(rdoMale);

        //set date of birth
        txtNicNo.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetBirthday();
                 System.out.println("SET_MOOD "+SET_MOOD);
                    if(getIsExistClient(txtNicNo.getCValue()) && SET_MOOD != "IDLE"){
                        
                        txtNicNo.resetValue();
                         mOptionPane.showMessageDialog(null, "This Customer is Already Exist", "Error " , mOptionPane.ERROR_MESSAGE);
                    }else{
                     txtNicNo.setEditable(true);   
                    }
            }
        });

     
                
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnBrowse, "doBrowse");
        btnBrowse.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_GENERATE, 16, 16));

        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }

                String extension = getExtension(f);
                if (extension != null) {
                    if (extension.equals("jpeg")
                            || extension.equals("jpg")
                            || extension.equals("png")) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return false;
            }

            @Override
            public String getDescription() {
                return "Image Files";
            }
        });

        
        //fourcing
        txtName.addFocusListener(new FocusAdapter() {

            @Override
            public void focusLost(FocusEvent e) {
              
                txtLongName.requestFocus();
            }
            
        });
        
        cboGroup.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LoanGroup group =(LoanGroup) cboGroup.getCValue();
                setAccCode(group);
                
            }
        });
        System.out.println("type  "+type);
        if(type!=null){
        if(type.equals("MICRO")){
            cboGroup.setEnabled(true);
        }else{
            
            cboGroup.setEnabled(false);
        }
        
            
        
    }
        
        FocusAdapter adapter=new FocusAdapter() {
                
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e); 
                txtCode_Ref.setCValue(txtCode.getCValue());
                txtCode_Ref.setEnabled(false);
            }
                        
            };
        
        
        txtCode.addFocusListener(adapter);
        
        
    }

    @Action
    public void doBrowse() throws IOException {
        fileChooser.showOpenDialog(CApplication.getInstance().getMainFrame());
        file = fileChooser.getSelectedFile();

        if (file != null) {
            image = ImageIO.read(file);

            imageBytes = new byte[(int) file.length()];
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                fileInputStream.read(imageBytes);
            }
        } else {
            image = null;
            imageBytes = null;
        }
        setThumbnail();
    }

    public void setAccCode(LoanGroup group){
        
        try {
            if(SET_MOOD.equals("NEW")){
            if(group!=null){
              
            String group_code = group.getCode();
            String center_code = group.getCenter();
            int max_client = 0;
//             LoanCenter centers =  (LoanCenter) (CPanel.GLOBAL.getDatabaseService().initCriteria(LoanCenter.class))
//                     .add(Restrictions.eq("code", group.getCenter()))
//                     .uniqueResult();
//             
            if(group.getCode()!=null){
                
                
                   Criteria  c =  (CPanel.GLOBAL.getDatabaseService().initCriteria(Client.class)).
                    add(Restrictions.eq("center", group.getCenter())).
                    setProjection(Projections.count("code"));
                 max_client =(int) c.uniqueResult();
         
//          
                  
             
            }else{
                
                max_client = 1;
            }
       
            String max_code = String.format("%03d", max_client+1);
                
            String client_code = String.format("%s/%s",center_code,max_code);
            
            txtCode.setCValue(client_code);
            txtCode_Ref.setCValue(client_code);
           
            }

            }
        } catch (Exception e) {
        e.printStackTrace();
        }
        
    }
    private void setThumbnail() {
        if (image != null) {
            lblImage.setIcon(getScaledImage(image, 70, 75));
        } else {
            lblImage.setIcon(null);
        }
    }

    private ImageIcon getScaledImage(Image image, int w, int h) {
        ImageIcon loadedImageIcon = new ImageIcon(image.getScaledInstance(w, h, Image.SCALE_DEFAULT));
        return loadedImageIcon;
    }

    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        lblImage = new com.mac.af.component.derived.display.label.CDLabel();
        cboSalutation = new com.mac.af.component.derived.input.combobox.CIComboBox(
            Salutation.ALL
        )
        ;
        btnBrowse = new com.mac.af.component.derived.command.button.CCButton();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtMaximumAllocation = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtEmail = new com.mac.af.component.derived.input.textfield.CIStringField();
        chkConsiderMaximumAllocation = new com.mac.af.component.derived.input.checkbox.CIEnabilityCheckBox();
        txtNote = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTelephone2 = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtFax = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtDateOfBirth = new com.mac.af.component.derived.input.textfield.CIDateField();
        txtMobile = new com.mac.af.component.derived.input.textfield.CIStringField();
        cboGroup = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getGroups();
            }
        };
        txtTelephone1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        cboRoute = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getRoutes();
            }
        };
        txtAddressLine3 = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        txtNicNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtLongName = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        txtName = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtAddressLine2 = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtAddressLine1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtCode_Ref = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel27 = new com.mac.af.component.derived.display.label.CDLabel();
        jPanel2 = new javax.swing.JPanel();
        cLabel17 = new com.mac.af.component.derived.display.label.CDLabel();
        txteduqlf = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel18 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel19 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDepends = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel20 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel21 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCivilStatus = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel22 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel23 = new com.mac.af.component.derived.display.label.CDLabel();
        txtProvince = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel24 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCity = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel25 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDistrict = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel26 = new com.mac.af.component.derived.display.label.CDLabel();
        txtGSDivision = new com.mac.af.component.derived.input.textfield.CIStringField();
        rdoMale = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        rdoFemale = new com.mac.af.component.derived.input.radiobutton.CIRadioButton();
        cboReligion = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getReligon();
            }
        };
        txtNoOFMem = new com.mac.af.component.derived.input.textfield.CIIntegerField();
        chkGuarantor = new com.mac.af.component.derived.input.checkbox.CICheckBox();

        cLabel2.setText("NIC No :");

        lblImage.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        btnBrowse.setText("Browse");

        cLabel1.setText("Code :");

        txtEmail.setPreferredSize(new java.awt.Dimension(79, 16));

        chkConsiderMaximumAllocation.setText("Max. Allocation");

        txtNote.setPreferredSize(new java.awt.Dimension(79, 16));

        cLabel16.setText("Group :");

        txtTelephone2.setPreferredSize(new java.awt.Dimension(79, 16));

        txtFax.setPreferredSize(new java.awt.Dimension(79, 16));

        cLabel4.setText("Name :");

        cLabel3.setText("Salutation :");

        cLabel5.setText("Full Name :");

        cLabel6.setText("Route :");

        cLabel7.setText("Address Line 1 :");

        cLabel8.setText("Address Line 2 :");

        cLabel9.setText("Address Line 3 :");

        cLabel10.setText("Mobile :");

        cLabel11.setText("Telephone 1 :");

        cLabel12.setText("Telephone 2 :");

        cLabel13.setText("Fax :");

        cLabel15.setText("Note :");

        cLabel14.setText("E mail :");

        cLabel27.setText("Ref Code :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkConsiderMaximumAllocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboGroup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAddressLine1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLongName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtMobile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTelephone1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAddressLine2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAddressLine3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboRoute, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(txtNote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTelephone2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFax, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtMaximumAllocation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboSalutation, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                            .addComponent(txtCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtNicNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDateOfBirth, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtCode_Ref, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(btnBrowse, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                            .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCode_Ref, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNicNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDateOfBirth, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboSalutation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBrowse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLongName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboRoute, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddressLine1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddressLine2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAddressLine3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMobile, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTelephone1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTelephone2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFax, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNote, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMaximumAllocation, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkConsiderMaximumAllocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Basic Details", jPanel1);

        cLabel17.setText("Edu qualification :");

        cLabel18.setText("No OF family Member:");

        cLabel19.setText("Depends:");

        cLabel20.setText("Gender:");

        cLabel21.setText("Civil Status:");

        cLabel22.setText("Religion:");

        cLabel23.setText("Province:");

        cLabel24.setText("City:");

        cLabel25.setText("District:");

        cLabel26.setText("Gs Division:");

        rdoMale.setText("Male");

        rdoFemale.setText("Female");

        chkGuarantor.setText("Is Guarantor");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txteduqlf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNoOFMem, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(33, 33, 33))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(cLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(4, 4, 4)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCivilStatus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtProvince, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCity, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDistrict, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDepends, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(rdoMale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(37, 37, 37)
                                        .addComponent(rdoFemale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(cboReligion, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(chkGuarantor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtGSDivision, javax.swing.GroupLayout.DEFAULT_SIZE, 292, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txteduqlf, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNoOFMem, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDepends, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdoFemale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdoMale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCivilStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboReligion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtProvince, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDistrict, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtGSDivision, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(chkGuarantor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(193, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("More Information", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnBrowse;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cLabel17;
    private com.mac.af.component.derived.display.label.CDLabel cLabel18;
    private com.mac.af.component.derived.display.label.CDLabel cLabel19;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel20;
    private com.mac.af.component.derived.display.label.CDLabel cLabel21;
    private com.mac.af.component.derived.display.label.CDLabel cLabel22;
    private com.mac.af.component.derived.display.label.CDLabel cLabel23;
    private com.mac.af.component.derived.display.label.CDLabel cLabel24;
    private com.mac.af.component.derived.display.label.CDLabel cLabel25;
    private com.mac.af.component.derived.display.label.CDLabel cLabel26;
    private com.mac.af.component.derived.display.label.CDLabel cLabel27;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboGroup;
    public com.mac.af.component.derived.input.combobox.CIComboBox cboReligion;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboRoute;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboSalutation;
    private com.mac.af.component.derived.input.checkbox.CIEnabilityCheckBox chkConsiderMaximumAllocation;
    private com.mac.af.component.derived.input.checkbox.CICheckBox chkGuarantor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblImage;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoFemale;
    public com.mac.af.component.derived.input.radiobutton.CIRadioButton rdoMale;
    private com.mac.af.component.derived.input.textfield.CIStringField txtAddressLine1;
    private com.mac.af.component.derived.input.textfield.CIStringField txtAddressLine2;
    private com.mac.af.component.derived.input.textfield.CIStringField txtAddressLine3;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCity;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCivilStatus;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCode;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCode_Ref;
    private com.mac.af.component.derived.input.textfield.CIDateField txtDateOfBirth;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDepends;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDistrict;
    private com.mac.af.component.derived.input.textfield.CIStringField txtEmail;
    private com.mac.af.component.derived.input.textfield.CIStringField txtFax;
    private com.mac.af.component.derived.input.textfield.CIStringField txtGSDivision;
    private com.mac.af.component.derived.input.textfield.CIStringField txtLongName;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtMaximumAllocation;
    private com.mac.af.component.derived.input.textfield.CIStringField txtMobile;
    private com.mac.af.component.derived.input.textfield.CIStringField txtName;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNicNo;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtNoOFMem;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNote;
    private com.mac.af.component.derived.input.textfield.CIStringField txtProvince;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTelephone1;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTelephone2;
    private com.mac.af.component.derived.input.textfield.CIStringField txteduqlf;
    // End of variables declaration//GEN-END:variables
public CRadioButtonGroup radioButtonGroup2;
    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList(
                (Component) txtCode);
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList(
                (Component) txtNicNo,
                (Component) txtCode_Ref,
                (Component) cboSalutation,
                (Component) txtName,
                (Component) txtLongName,
                (Component) cboRoute,
                (Component) chkConsiderMaximumAllocation //(Component) chkActive
                );
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                (Component) txtAddressLine1,
                (Component) txtAddressLine2,
                (Component) txtAddressLine3,
                (Component) txtMobile,
                (Component) txtTelephone1,
                (Component) txtTelephone2,
                (Component) txtFax,
                (Component) txtEmail,
                (Component) txtNote,
                (Component) cboGroup,
                
                (Component) txteduqlf,
                (Component) txtNoOFMem,
                (Component) txtDepends,
                (Component) txtCivilStatus,
                (Component) cboReligion,
                (Component) txtProvince,
                (Component) txtCity,
                (Component) txtDistrict,
                (Component) txtGSDivision
                
//                (Component) jSlider1
                );
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(txtDateOfBirth);
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtCode, "code"),
                new CInputComponentBinder(txtNicNo, "nicNo"),
                new CInputComponentBinder(cboSalutation, "salutation"),
                new CInputComponentBinder(txtName, "name"),
                new CInputComponentBinder(txtLongName, "longName"),
                new CInputComponentBinder(cboRoute, "route"),
                new CInputComponentBinder(cboGroup, "groupCode"),
                new CInputComponentBinder(txtAddressLine1, "addressLine1"),
                new CInputComponentBinder(txtAddressLine2, "addressLine2"),
                new CInputComponentBinder(txtAddressLine3, "addressLine3"),
                new CInputComponentBinder(txtMobile, "mobile"),
                new CInputComponentBinder(txtTelephone1, "telephone1"),
                new CInputComponentBinder(txtTelephone2, "telephone2"),
                new CInputComponentBinder(txtFax, "fax"),
                new CInputComponentBinder(txtEmail, "email"),
                new CInputComponentBinder(txtNote, "note"),
                new CInputComponentBinder(txtMaximumAllocation, "maximumAllocation"),
                new CInputComponentBinder(chkConsiderMaximumAllocation, "considerMaximumAllocation"),
                new CInputComponentBinder(chkGuarantor, "isGuarantor"),
                new CInputComponentBinder(txtCode_Ref, "refCode"),
                
                new CInputComponentBinder(txteduqlf, "eduQl"),
                new CInputComponentBinder(txtNoOFMem, "noOfFamilyMem"),
                new CInputComponentBinder(txtDepends, "depends"),
                new CInputComponentBinder(txtCivilStatus, "civil"),
                new CInputComponentBinder(cboReligion, "religon"),
                new CInputComponentBinder(txtProvince, "province"),
                new CInputComponentBinder(txtCity, "city"),
                new CInputComponentBinder(txtDistrict, "district"),
                new CInputComponentBinder(txtGSDivision, "gsDivision")
                
                );
    }

    @Override
    protected Class getObjectClass() {
        return com.mac.registration.client.object.Client.class;
    }

    @Override
    public void resetFields() {
        super.resetFields();
//        jSlider1.setValue(0);
        lblImage.setCValue(null);
        txtNicNo.setEditable(true);
        txtNicNo.resetValue();
    }

    @Override
    public void setIdleMood() {
        super.setIdleMood();
        btnBrowse.setEnabled(false);
       SET_MOOD = "IDLE";
    }

    @Override
    public void setNewMood() {
        super.setNewMood();
        btnBrowse.setEnabled(false);
       
        lblImage.setIcon(null);
         SET_MOOD ="NEW";
         
         System.out.println("type  "+type);
        if(type!=null){
        if(type.equals("MICRO")){
            cboGroup.setVisible(true);
        }else{
            
            cboGroup.setVisible(false);
        }
        
        }
       
        
    }

    @Override
    public void setEditMood() {
        super.setEditMood();
        System.out.println("setEditMood ");
        cboGroup.setEnabled(false);
        cboGroup.setExpressEditable(false);
       
       cboGroup.setValueEditable(false);
       SET_MOOD ="EDIT";
        btnBrowse.setEnabled(false);
        txtCode_Ref.setEnabled(false);
    }

    @Override
    protected void afterNewObject(Object object) {
        ((com.mac.registration.client.object.Client) object).setActive(true);
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        super.initInterface();
        resetBirthday();

        
//        Client client = (Client) getValueAbstract();
//        if (client != null) {
//            lblStarMarks.setCValue(client.getStarMarks() + " Stars");
////            try {
////                ImageIcon icon = imageWriter.getImage(client.getCode());
////                if (icon != null) {
////                    lblImage.setIcon(getScaledImage(icon.getImage(), 70, 75));
////                }
////            } catch (SocketException ex) {
////                Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, ex);
////            } catch (IOException ex) {
////                Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, ex);
////            }
//        } else {
//            lblStarMarks.setCValue("0 Stars");
//        }

    }

    @Override
    protected void afterInitObject(Object object) throws ObjectCreatorException {
        super.afterInitObject(object);
        Client client = (Client) object;
        
        LoanGroup group= (LoanGroup) cboGroup.getCValue();
        
        client.setClient(isClient);
        client.setSupplier(isSupplier);
        client.setStarMarks(0);
        client.setActive(false);
         client.setStatus(ClientStatus.ACTIVE);
         client.setGender((String) radioButtonGroup2.getCValue());
         if(group!=null){
             client.setCenter(group.getCenter());
         }else{
             client.setCenter(null);
         }
         
//        try {
//            imageWriter.saveImage(file.getAbsolutePath(), client.getCode());
//        } catch (Exception e) {
//            Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, e);
//        }
    }
    
    
    private SessionFactory sessionFactory;
}
