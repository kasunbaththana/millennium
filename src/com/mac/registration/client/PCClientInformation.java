/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.client;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.af.resources.ApplicationResources;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.registration.client.object.Client;
import com.mac.registration.client.object.ClientInformation;
import com.mac.registration.client.object.ClientReviewHistory;
import com.mac.zresources.FinacResources;
import com.mac.zsystem.model.table_model.ClientInformationTableModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author chathu
 */
public class PCClientInformation extends DefaultObjectCreator<ClientInformation> {

    /**
     * Creates new form PCClientInformation
     */
    public PCClientInformation() {
        initComponents();
        initOthers();


    }

    @Action
    public void doBlackList() {
        getCPanel().getDatabaseService().beginLocalTransaction();
        Client client = (Client) cboClient.getCValue();
        client.setStatus(ClientStatus.BLACK_LIST);
        try {
            getCPanel().getDatabaseService().save(client);
            getCPanel().getDatabaseService().commitLocalTransaction();
//            getCPanel().getDatabaseService().save(client);
//            getCPanel().getDatabaseService().commitLocalTransaction();
        } catch (DatabaseException ex) {
            Logger.getLogger(PCClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
        mOptionPane.showMessageDialog(null, "Client Black List Successfuly ..", "Client Information", mOptionPane.INFORMATION_MESSAGE);
        btnBlackList.setVisible(false);
        btnActive.setVisible(true);
        setNewMood();
        try {
            setValue(new ClientInformation());
        } catch (ObjectCreatorException ex) {
            Logger.getLogger(PCClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doActivate() {
        getCPanel().getDatabaseService().beginLocalTransaction();
        Client client = (Client) cboClient.getCValue();
        client.setStatus(ClientStatus.ACTIVE);
        try {
            getCPanel().getDatabaseService().save(client);
            getCPanel().getDatabaseService().commitLocalTransaction();
//            getCPanel().getDatabaseService().save(client);
//            getCPanel().getDatabaseService().commitLocalTransaction();
        } catch (DatabaseException ex) {
            Logger.getLogger(PCClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
        mOptionPane.showMessageDialog(null, "Client Reactivate Successfuly ..", "Client Information", mOptionPane.INFORMATION_MESSAGE);
        btnBlackList.setVisible(true);
        btnActive.setVisible(false);
        setNewMood();
        try {
            setValue(new ClientInformation());
        } catch (ObjectCreatorException ex) {
            Logger.getLogger(PCClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doReview() {
        saveClientReview((Client) cboClient.getCValue());
        setNewMood();
        txtReviewDescription.resetValue();
        sliderReviewPoint.setValue(5);
        setNewMood();

        lblClientReview.setText(" ");
        lblClientReviewOld.setText(" ");
        try {
            setValue(new ClientInformation());
        } catch (ObjectCreatorException ex) {
            Logger.getLogger(PCClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void saveClientReview(Client client) {
        getCPanel().getDatabaseService().beginLocalTransaction();
        ClientReviewHistory reviewHistory = new ClientReviewHistory();
        reviewHistory.setClient(client);
        reviewHistory.setPoint(sliderReviewPoint.getValue());
        reviewHistory.setDiscription(txtReviewDescription.getCValue());
        try {
            getCPanel().getDatabaseService().save(reviewHistory);
            getCPanel().getDatabaseService().commitLocalTransaction();
        } catch (DatabaseException ex) {
            Logger.getLogger(PCClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
        //LOAD CLIENT REVIEW
//        getClientReviewList();
        mOptionPane.showMessageDialog(null, "Client Review Save Successfuly ..", "Client Information", mOptionPane.INFORMATION_MESSAGE);

    }

    @Action
    public void doClose() {
        TabFunctions.closeTab(getCPanel());
    }

    @Action(asynchronous = true)
    public void doSave() {
        ClientInformation clientInformation;
        Client client;

        try {
            if ((clientInformation = getValue()) != null) {
                client = (Client) cboClient.getCValue();
                if (mOptionPane.showConfirmDialog(null, "Are you sure want to save the client information ?", SERClientInformation.TITLE, mOptionPane.YES_NO_OPTION, mOptionPane.WARNING_MESSAGE) == mOptionPane.YES_OPTION) {
                    clientInformation.setClient(client.getCode());
                    clientInformation.setClient_1(client);

                    if (serClientInformation.save(clientInformation)) {
                        resetAll();
                    }
                }
            }
        } catch (ObjectCreatorException ex) {
            Logger.getLogger(PCClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Action
    public void doEdit() {
        try {
            setValue(getClientInformation((Client) cboClient.getCValue()));
        } catch (ObjectCreatorException ex) {
            Logger.getLogger(PCClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
        setEditMood();
    }

    @Action
    public void doDiscard() {
        int q = mOptionPane.showConfirmDialog(this, "Do you sure want to discard changes ?", SERClientInformation.TITLE, mOptionPane.YES_NO_OPTION);
        if (q == mOptionPane.YES_OPTION) {
            resetAll();
        }
    }

    private void resetAll() {
        ((AbstractGridObject) getCPanel()).refreshTable();
        setIdleMood();
    }

    private ClientInformation getClientInformation(Client client) {
        ClientInformation clientInformation;

        if (client.getClientInformations().isEmpty()) {
            clientInformation = new ClientInformation();
        } else {
            clientInformation = client.getClientInformations().iterator().next();
        }

        return clientInformation;
    }

    private void resetMode() {
        switch (CURRENT_MODE) {
            case IDLE_MODE:
                btnEdit.setVisible(true);
                btnSave.setVisible(false);
                btnDiscard.setVisible(false);
                btnClose.setVisible(true);
                break;
            case EDIT_MODE:
                btnEdit.setVisible(false);
                btnSave.setVisible(true);
                btnDiscard.setVisible(true);
                btnClose.setVisible(false);
                break;
            default:
                throw new AssertionError();
        }
    }

    @Override
    public void setEditMood() {
        super.setEditMood();

        CURRENT_MODE = EDIT_MODE;
        resetMode();
    }

    @Override
    public void setIdleMood() {
        super.setIdleMood();

        CURRENT_MODE = IDLE_MODE;
        resetMode();
    }

    private void showHideBlackList() {
        Client client = (Client) cboClient.getCValue();
        if(client!=null){
        switch (client.getStatus()) {
            case ClientStatus.BLACK_LIST:
                btnBlackList.setVisible(false);
                btnActive.setVisible(true);
                break;
            case ClientStatus.ACTIVE:
                btnActive.setVisible(false);
                btnBlackList.setVisible(true);
                break;
            default:

                btnActive.setVisible(false);
                btnBlackList.setVisible(false);
                break;
        }
        }
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {

//lblClientReview.setText(new ReviewCalculation().getReviewCalculation(WIDTH, sliderReviewPoint.getValue(), PROPERTIES));


        sliderSettings(sliderReviewPoint);
        serClientInformation = new SERClientInformation(this);
        cboClient.setExpressEditable(true);
        cboClient.setTableModel(new ClientInformationTableModel());

        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnSave, "doSave");
        actionUtil.setAction(btnEdit, "doEdit");
        actionUtil.setAction(btnDiscard, "doDiscard");
        //CLIENT REVIEW PANEL BUTTONS
        actionUtil.setAction(btnBlackList, "doBlackList");
        actionUtil.setAction(btnActive, "doActivate");
        actionUtil.setAction(btnReview, "doReview");
        cboClient.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                //SHOW AND HIDE BLACK LIST BUTTON SET
                showHideBlackList();
                System.out.println(e.getItem());
                try {
                    setValue(getClientInformation((Client) e.getItem()));
                    //LOAD CLIENT REVIEW
//                    getClientReviewList();
                } catch (ObjectCreatorException ex) {
                    Logger.getLogger(PCClientInformation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        sliderReviewPoint.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                Client client = (Client) cboClient.getCValue();
                String reviewCalculation = new ReviewCalculation().getReviewCalculation(client, sliderReviewPoint.getValue());
                lblClientReview.setCValue("<html> New Review Rate :"+" <FONT COLOR=RED>" + reviewCalculation +" </FONT></html> ");

                String reviewOld = new ReviewCalculation().getOldReview(client);
                lblClientReviewOld.setCValue("<html> Old Review Rate : "+" <B>" + reviewOld +"</B> </html> ");


            }
        });
        btnEdit.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_EDIT, 16, 16));
        btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
        btnDiscard.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_DISCARD, 16, 16));
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        //CLIENT REVIEW BTTON SET
        btnActive.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_LOOKUP, 16, 16));
        btnBlackList.setIcon(FinacResources.getImageIcon(FinacResources.BLACK_LIST, 16, 16));
        btnReview.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_RESTORE, 16, 16));

//        tblClientReview.setCModel(new CTableModel(
//                new CTableColumn("Point", "point"),
//                new CTableColumn("Description", "discription")));

//        tblClientReview.getColumnModel().getColumn(0).setPreferredWidth(10);
//        tblClientReview.getColumnModel().getColumn(1).setPreferredWidth(50);


    }

//    public void getClientReviewList() {
//        Client client = (Client) cboClient.getCValue();
//        tblClientReview.setCValue(serClientInformation.getClientReviewList(client));
//    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cboClient = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return serClientInformation.getClients();
            }
        };
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        btnDiscard = new com.mac.af.component.derived.command.button.CCButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtWorkPlace1 = new com.mac.af.component.derived.input.textarea.CITextArea();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtWorkPlace2 = new com.mac.af.component.derived.input.textarea.CITextArea();
        jPanel2 = new javax.swing.JPanel();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtAsset1 = new com.mac.af.component.derived.input.textarea.CITextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtAsset2 = new com.mac.af.component.derived.input.textarea.CITextArea();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtAsset3 = new com.mac.af.component.derived.input.textarea.CITextArea();
        jPanel3 = new javax.swing.JPanel();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtInstallmentPlace1 = new com.mac.af.component.derived.input.textarea.CITextArea();
        jScrollPane7 = new javax.swing.JScrollPane();
        txtInstallmentPlace2 = new com.mac.af.component.derived.input.textarea.CITextArea();
        jScrollPane8 = new javax.swing.JScrollPane();
        txtInstallmentPlace3 = new com.mac.af.component.derived.input.textarea.CITextArea();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        txtReviewDescription = new com.mac.af.component.derived.input.textarea.CITextArea();
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        jPanel5 = new javax.swing.JPanel();
        sliderReviewPoint = new javax.swing.JSlider();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        jLabel1 = new javax.swing.JLabel(){
            @Override
            public void paint(Graphics g) {
                super.paint(g); //To change body of generated methods, choose Tools | Templates.
                int rr=0;
                int gg=0;
                int w=0;
                for (int i = 0; i < 10; i++) {
                    rr+=2*8;
                    g.setColor(new Color(255, rr, rr));
                    g.fillRect(25+w, 0, this.getWidth(), this.getHeight());
                    // r+=10;
                    w+=25;
                    gg+=1;

                }
            }
        };
        jLabel2 = new javax.swing.JLabel(){
            @Override
            public void paint(Graphics g) {
                super.paint(g); //To change body of generated methods, choose Tools | Templates.
                int rr=255;
                int gg=0;
                int w=0;
                for (int i = 0; i < 10; i++) {
                    rr-=2*8;
                    g.setColor(new Color(rr, 255, rr+2));
                    g.fillRect(25+w, 0, this.getWidth(), this.getHeight());
                    // r+=10;
                    w+=25;
                    gg+=1;

                }
            }
        };
        btnBlackList = new com.mac.af.component.derived.command.button.CCButton();
        btnReview = new com.mac.af.component.derived.command.button.CCButton();
        btnActive = new com.mac.af.component.derived.command.button.CCButton();
        lblClientReview = new com.mac.af.component.derived.display.label.CDLabel();
        lblClientReviewOld = new com.mac.af.component.derived.display.label.CDLabel();
        btnEdit = new com.mac.af.component.derived.command.button.CCButton();

        cDLabel1.setText("Client :");

        btnClose.setText("Close");

        btnSave.setText("Save");

        btnDiscard.setText("Discard");

        cDLabel2.setText("Work Place 1 :");

        txtWorkPlace1.setColumns(20);
        txtWorkPlace1.setRows(5);
        jScrollPane1.setViewportView(txtWorkPlace1);

        cDLabel3.setText("Work Place 2 :");

        txtWorkPlace2.setColumns(20);
        txtWorkPlace2.setRows(5);
        jScrollPane2.setViewportView(txtWorkPlace2);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(231, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Work Place", jPanel1);

        cDLabel4.setText("Asset 1 :");

        cDLabel5.setText("Asset 2 :");

        cDLabel6.setText("Asset 3 :");

        txtAsset1.setColumns(20);
        txtAsset1.setRows(5);
        jScrollPane3.setViewportView(txtAsset1);

        txtAsset2.setColumns(20);
        txtAsset2.setRows(5);
        jScrollPane4.setViewportView(txtAsset2);

        txtAsset3.setColumns(20);
        txtAsset3.setRows(5);
        jScrollPane5.setViewportView(txtAsset3);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(141, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Assets", jPanel2);

        cDLabel8.setText("Instalment Place 2 :");

        cDLabel7.setText("Instalment Place 1 :");

        cDLabel9.setText("Instalment Place 3 :");

        txtInstallmentPlace1.setColumns(20);
        txtInstallmentPlace1.setRows(5);
        jScrollPane6.setViewportView(txtInstallmentPlace1);

        txtInstallmentPlace2.setColumns(20);
        txtInstallmentPlace2.setRows(5);
        jScrollPane7.setViewportView(txtInstallmentPlace2);

        txtInstallmentPlace3.setColumns(20);
        txtInstallmentPlace3.setRows(5);
        jScrollPane8.setViewportView(txtInstallmentPlace3);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(141, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Other Installment Places", jPanel3);

        txtReviewDescription.setColumns(20);
        txtReviewDescription.setRows(5);
        jScrollPane9.setViewportView(txtReviewDescription);

        cDLabel11.setText("Description :");

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cDLabel10.setText("Point :");

        jLabel1.setText(" ");

        jLabel2.setText(" ");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sliderReviewPoint, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(8, 8, 8)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sliderReviewPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnBlackList.setText("Black List ");

        btnReview.setText("Review");

        btnActive.setText("Active");

        lblClientReview.setForeground(new java.awt.Color(0, 51, 51));
        lblClientReview.setText(" ");
        lblClientReview.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N

        lblClientReviewOld.setText(" ");
        lblClientReviewOld.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(btnBlackList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnActive, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnReview, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lblClientReview, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblClientReviewOld, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnActive, btnBlackList, btnReview});

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblClientReview, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblClientReviewOld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBlackList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReview, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(174, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Client Review", jPanel4);

        btnEdit.setText("Edit");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDiscard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTabbedPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cboClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDiscard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnActive;
    private com.mac.af.component.derived.command.button.CCButton btnBlackList;
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnDiscard;
    private com.mac.af.component.derived.command.button.CCButton btnEdit;
    private com.mac.af.component.derived.command.button.CCButton btnReview;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboClient;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblClientReview;
    private com.mac.af.component.derived.display.label.CDLabel lblClientReviewOld;
    private javax.swing.JSlider sliderReviewPoint;
    private com.mac.af.component.derived.input.textarea.CITextArea txtAsset1;
    private com.mac.af.component.derived.input.textarea.CITextArea txtAsset2;
    private com.mac.af.component.derived.input.textarea.CITextArea txtAsset3;
    private com.mac.af.component.derived.input.textarea.CITextArea txtInstallmentPlace1;
    private com.mac.af.component.derived.input.textarea.CITextArea txtInstallmentPlace2;
    private com.mac.af.component.derived.input.textarea.CITextArea txtInstallmentPlace3;
    private com.mac.af.component.derived.input.textarea.CITextArea txtReviewDescription;
    private com.mac.af.component.derived.input.textarea.CITextArea txtWorkPlace1;
    private com.mac.af.component.derived.input.textarea.CITextArea txtWorkPlace2;
    // End of variables declaration//GEN-END:variables
    private SERClientInformation serClientInformation;

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList();
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                (Component) txtWorkPlace1,
                (Component) txtWorkPlace2,
                (Component) txtAsset1,
                (Component) txtAsset2,
                (Component) txtAsset3,
                (Component) txtInstallmentPlace1,
                (Component) txtInstallmentPlace2,
                (Component) txtInstallmentPlace3);
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtWorkPlace1, "workPlace1"),
                new CInputComponentBinder(txtWorkPlace2, "workPlace2"),
                new CInputComponentBinder(txtAsset1, "asset1"),
                new CInputComponentBinder(txtAsset2, "asset2"),
                new CInputComponentBinder(txtAsset3, "asset3"),
                new CInputComponentBinder(txtInstallmentPlace1, "installmentPlace1"),
                new CInputComponentBinder(txtInstallmentPlace2, "installmentPlace2"),
                new CInputComponentBinder(txtInstallmentPlace3, "installmentPlace3"));
    }

    @Override
    protected Class<? extends ClientInformation> getObjectClass() {
        return com.mac.registration.client.object.ClientInformation.class;
    }
    //MODE
    private int CURRENT_MODE = 0;
    private static final int IDLE_MODE = 0;
    private static final int EDIT_MODE = 1;

    private void sliderSettings(JSlider jSlider) {
        jSlider.setMinimum(ClientStatus.MIN_VAL);
        jSlider.setMaximum(ClientStatus.MAX_VAL);
        jSlider.setValue(ClientStatus.INI_VAL);
        jSlider.setMinorTickSpacing(1);
        jSlider.setMajorTickSpacing(5);
        jSlider.setPaintTrack(true);
        jSlider.setPaintLabels(true);
    }
}
