/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.client;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author Udayanga
 */
public class ImageWriter {

    private String server;
    private int portNo;
    private String userName;
    private String pasWord;
    private FTPClient fTPClient;

    public void saveImage(String path, String code) {
        if (path != null) {


            this.readConfigurationSettings();
            try {
                fTPClient = new FTPClient();
                fTPClient.connect(server, portNo);

                if (fTPClient.login(userName, pasWord)) {

                    fTPClient.setFileType(FTP.BINARY_FILE_TYPE);
                    File LocalFile = new File(path);
                    String firstRemoteFile = code + ImageFileStatus.FILE_EXT;
                    InputStream inputStream = new FileInputStream(LocalFile);

                    fTPClient.storeFile(firstRemoteFile, inputStream);
                    inputStream.close();

                } else {
                    System.out.println("ftp server:login failed");
                }
            } catch (SocketException ex) {
                Logger.getLogger(ImageWriter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ImageWriter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ImageIcon getImage(String code) throws SocketException, IOException {

        this.readConfigurationSettings();

        fTPClient = new FTPClient();
        fTPClient.connect(server, portNo);

        ImageIcon nicon = null;
        String cmpCode = code + ImageFileStatus.FILE_EXT;
        BufferedImage image = null;

        if (fTPClient.login(userName, pasWord)) {
            String[] files2 = fTPClient.listNames();
            for (String string : files2) {
                if (cmpCode.equals(string)) {
                    File downloadFile = new File(ImageFileStatus.FILE_DESTINATION);
                    OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
                    fTPClient.retrieveFile("/" + string, outputStream);
                    image = ImageIO.read(downloadFile);
                    nicon = new ImageIcon(image);
                    outputStream.close();
                    downloadFile.delete();
                }
            }
        } else {
            System.out.println("ftp server:login failed");
        }
        return nicon;
    }

    public void readConfigurationSettings() {
        Properties properties = new Properties();
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(ImageFileStatus.FILE_NAME);
            properties.load(inputStream);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        server = properties.getProperty(ImageFileStatus.IP_ADDRESS);
        portNo = Integer.parseInt(
                properties.getProperty(ImageFileStatus.PORT_NO).isEmpty()
                ? "14147"
                : properties.getProperty(ImageFileStatus.PORT_NO));
        userName = properties.getProperty(ImageFileStatus.USER_NAME);
        pasWord = properties.getProperty(ImageFileStatus.PASSWORD);
    }

    public void writeConfigurationSettings() {
        Properties prop = new Properties();
        OutputStream output = null;
        File file = new File(ImageFileStatus.FILE_NAME);
        try {
            if (!file.exists()) {
                output = new FileOutputStream(file.getName());
                prop.setProperty(ImageFileStatus.IP_ADDRESS, "");
                prop.setProperty(ImageFileStatus.PORT_NO, "");
                prop.setProperty(ImageFileStatus.USER_NAME, "");
                prop.setProperty(ImageFileStatus.PASSWORD, "");
                prop.store(output, null);
            }
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
