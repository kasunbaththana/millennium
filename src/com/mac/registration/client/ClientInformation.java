/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.client;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chathu
 */
public class ClientInformation extends AbstractGridObject{


    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
                    new CTableColumn("Client", "client_1", "name"),
                    new CTableColumn("Work Place 1", "workPlace1"),
                    new CTableColumn("Work Place 2", "workPlace2"),
                    new CTableColumn("Asset 1", "asset1"),
                    new CTableColumn("Asset 2", "asset2"),
                    new CTableColumn("Asset 3", "asset3"),
                    new CTableColumn("Instalment Place 1", "installmentPlace1"),
                    new CTableColumn("Instalment Place 2", "installmentPlace2"),
                    new CTableColumn("Instalment Place 3", "installmentPlace3"),
                    new CTableColumn("Status","client_1" , "status")
                });
    }

    @Override
    protected AbstractObjectCreator getObjectCreator() {
        
        return new PCClientInformation();
    }
    @Override
    protected Collection getTableData() {
         List list;
        try {
            HashMap params = new HashMap<String, Object>();
            list = getDatabaseService().getCollection("from com.mac.registration.client.object.ClientInformation", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(ClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
}
