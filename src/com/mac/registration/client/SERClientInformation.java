/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.client;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.af.core.message.mOptionPane;
import com.mac.loan.loan_approval.SERLoanApplicationApproval;
import com.mac.registration.client.object.Client;
import com.mac.registration.client.object.ClientInformation;
import com.mac.registration.client.object.ClientReviewHistory;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author chathu
 */
public class SERClientInformation extends AbstractService {

    public SERClientInformation(Component component) {
        super(component);
    }

    boolean save(ClientInformation clientInformation) {
        try {
            getDatabaseService().save(clientInformation);

            mOptionPane.showMessageDialog(null, "Client information saved successfully.", TITLE, mOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (DatabaseException ex) {
            Logger.getLogger(SERLoanApplicationApproval.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Unable to save client information.", TITLE, mOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    public List getClients() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.registration.client.object.Client");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public Collection getClientReviewList(Client client) {
        Collection collection;
        try {
            collection = getDatabaseService().initCriteria(ClientReviewHistory.class).add(Restrictions.eq("client", client)).list();
        } catch (DatabaseException ex) {
            collection = new ArrayList();
            Logger.getLogger(SERClientInformation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return collection;
    }
    public static final String TITLE = "Client Information";
}