package com.mac.registration.client.object;
// Generated Feb 11, 2021 1:58:49 PM by Hibernate Tools 3.2.1.GA



/**
 * LoanGroup generated by hbm2java
 */
public class LoanGroup  implements java.io.Serializable {


     private String code;
     private String name;
     private String center;

    public LoanGroup() {
    }

	
    public LoanGroup(String code, String center) {
        this.code = code;
        this.center = center;
    }
    public LoanGroup(String code, String name, String center) {
       this.code = code;
       this.name = name;
       this.center = center;
    }
   
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getCenter() {
        return this.center;
    }
    
    public void setCenter(String center) {
        this.center = center;
    }

    @Override
    public String toString() {
        return  center + "-" + name ;
    }




}


