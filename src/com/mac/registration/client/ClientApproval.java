/*
 *  ClientApproval.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 3, 2014, 7:57:46 AM 
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.registration.client;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.registration.client.object.Client;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class ClientApproval extends AbstractGridObject<Client> {

    @Override
    protected CTableModel<Client> getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"refCode"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Mobile", new String[]{"mobile"}),
            new CTableColumn("Telephone 1", new String[]{"telephone1"}),
            new CTableColumn("Telephone 2", new String[]{"telephone2"}),
            new CTableColumn("Fax", new String[]{"fax"}),
            new CTableColumn("E mail", new String[]{"email"}),
            new CTableColumn("Active", new String[]{"active"})});
    }

    @Override
    protected AbstractObjectCreator<Client> getObjectCreator() {
        return new PCClientApprove();
    }

    @Override
    protected Collection<Client> getTableData() {
        try {
            return getDatabaseService().getCollection("FROM com.mac.registration.client.object.Client WHERE active=false and group_code=null");
        } catch (DatabaseException ex) {
            Logger.getLogger(ClientApproval.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }
}
