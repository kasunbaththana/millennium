/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.client;

/**
 *
 * @author Udayanga
 */
public class ImageFileStatus {

    public static final String FILE_EXT = ".jpg";
    public static final String FILE_DESTINATION = "/client.jpg";
    public static final String FILE_NAME = "config.properties";
    //ftp server
    public static final String IP_ADDRESS = "ftpServerIpAddress";
    public static final String PORT_NO = "ftpServerPortNo";
    public static final String USER_NAME = "ftpServerUserName";
    public static final String PASSWORD = "ftpServerPassword";
    //sms getway
    public static final String SMS_URL = "smsServerUrl";
    public static final String SMS_GETWAY_USERNAME = "smsServerUserName";
    public static final String SMS_GETWAY_PASSWORD = "smsServerPassword";
    //email server
    //public static final String EMAIL_SERVER_URL="";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String EMAIL_PASSWORD = "emailPassword";
}
