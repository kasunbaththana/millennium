/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.client;

import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.registration.client.object.Client;
import com.mac.registration.client.object.ClientReviewHistory;
import java.text.DecimalFormat;
import java.util.List;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 * @author NIMESH-PC
 */
public class ReviewCalculation {

    private String reviewConvert(double value) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return decimalFormat.format(value);
    }

    public String getReviewCalculation(Client client, int newReviewValue) {

        double reviewValue = 0;

        double reviewCount = ((Number) CPanel.GLOBAL.getDatabaseService().initCriteria(ClientReviewHistory.class)
                .add(Restrictions.eq("client", client))
                .setProjection(Projections.count("indexNo")).uniqueResult()).doubleValue();

        List<ClientReviewHistory> clientReviewList = CPanel.GLOBAL.getDatabaseService().initCriteria(ClientReviewHistory.class)
                .add(Restrictions.eq("client", client)).list();

        for (ClientReviewHistory clientReviewHistory : clientReviewList) {
            reviewValue += clientReviewHistory.getPoint();
        }






        double reviewSummary = (reviewValue + newReviewValue) / (reviewCount + 1);


        if (reviewSummary == 0) {
            return reviewConvert(reviewSummary);
        }
        return reviewConvert(reviewSummary);
    }

    public String getOldReview(Client client) {

        double reviewValue = 0;

        double reviewCount = ((Number) CPanel.GLOBAL.getDatabaseService().initCriteria(ClientReviewHistory.class)
                .add(Restrictions.eq("client", client))
                .setProjection(Projections.count("indexNo")).uniqueResult()).doubleValue();

        List<ClientReviewHistory> clientReviewList = CPanel.GLOBAL.getDatabaseService().initCriteria(ClientReviewHistory.class)
                .add(Restrictions.eq("client", client)).list();

        for (ClientReviewHistory clientReviewHistory : clientReviewList) {
            reviewValue += clientReviewHistory.getPoint();
        }
        double reviewSummary = reviewValue / reviewCount;


        if (reviewSummary == 0) {
            return reviewConvert(reviewSummary);
        }
        return reviewConvert(reviewSummary);
    }
}
