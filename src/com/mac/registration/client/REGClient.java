/*
 *  REGClient.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.registration.client;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import java.util.List;

/**
 *  
 * @author mohan
 */
public class REGClient extends AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.client.object.Client.class;
    }
    
   

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"refCode"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Mobile", new String[]{"mobile"}),
            new CTableColumn("Telephone 1", new String[]{"telephone1"}),
            new CTableColumn("Telephone 2", new String[]{"telephone2"}),
            new CTableColumn("Fax", new String[]{"fax"}),
            new CTableColumn("E mail", new String[]{"email"}),
            new CTableColumn("Active", new String[]{"active"})
        });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCClient(isClient(), isSuplier(),"NONE");
    }

    @Override
    protected List getTableData() throws DatabaseException {
        return getDatabaseService().getCollection("FROM com.mac.registration.client.object.Client WHERE  client=true and groupCode=null");
    }

    public Boolean isClient() {
        return true;
    }

    public Boolean isSuplier() {
        return false;
    }
    
   
}
