
package com.mac.registration.fund_type;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zsystem.transaction.account.object.Account;

/**
 *
 * @author kasun
 */
public class REGFundType extends AbstractRegistrationForm {

    @Override
    public Class getObjectClass() {
        return com.mac.registration.fund_type.object.FundType.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"indexNo"}),
            new CTableColumn("Name", new String[]{"name"})
        });
    }

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCFundType();
    }

    @Override
    public void doSave() {
        super.doSave(); 
        
      
    }
    
    

    
}
