
package com.mac.registration.fund_type;

import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.panel.object.CInputComponentBinder;
import java.awt.Component;
import java.util.Arrays;
import java.util.List;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.registration.fund_type.object.FundType;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Projections;

/**
 *
 * @author kasun
 */
public class PCFundType extends DefaultObjectCreator {

    /**
     * Creates new form PCClient
     */
    public PCFundType() {
        initComponents();
        initOthers();
    }

   
    @SuppressWarnings("unchecked")
    private void initOthers() {
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtName = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtMemo = new com.mac.af.component.derived.input.textarea.CITextArea();
        txtCode = new com.mac.af.component.derived.input.textfield.CIIntegerField();

        cLabel1.setText("Code :");

        cLabel2.setText("Name :");

        cLabel3.setText("Memo:");

        txtMemo.setColumns(20);
        txtMemo.setRows(5);
        jScrollPane2.setViewportView(txtMemo);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                    .addComponent(txtCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private com.mac.af.component.derived.input.textfield.CIIntegerField txtCode;
    private com.mac.af.component.derived.input.textarea.CITextArea txtMemo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtName;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList(
                (Component) txtCode);
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList(
                (Component) txtName);
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                (Component) txtMemo);
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtCode, "indexNo"),
                new CInputComponentBinder(txtName, "name"),
                new CInputComponentBinder(txtMemo, "memo"));
    }

    @Override
    protected Class getObjectClass() {
        return com.mac.registration.fund_type.object.FundType.class;
    }

    @Override
    protected void afterNewObject(Object object) {
        
        try {
            int centers =  ((Integer) CPanel.GLOBAL.getDatabaseService().initCriteria(FundType.class).
                    setProjection(Projections.count("indexNo")).uniqueResult()).intValue()+1;
       
            
              ((com.mac.registration.fund_type.object.FundType) object).setIndexNo(centers);
        } catch (Exception ex) {
            Logger.getLogger(PCFundType.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected List getUneditableComponents() {
         return Arrays.asList(
                (Component) txtCode);
    }
    
    
    
}
