/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.registration.cashier_point;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.Account;
import com.mac.zsystem.transaction.cashier.object.CashierPoint;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thilanga-pc
 */
public class REGCashier extends AbstractRegistrationForm {

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCCashier() {
            @Override
            protected String getTerminal() {
                initTerminal();

                return terminal;
            }

            @Override
            protected List getAccounts() {
                try {
                    return getDatabaseService().getCollection(Account.class);
                } catch (DatabaseException ex) {
                    Logger.getLogger(REGCashier.class.getName()).log(Level.SEVERE, null, ex);
                    return new ArrayList();
                }
            }
        };
    }

    @Override
    public Class getObjectClass() {
        return CashierPoint.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Name", "name"),
            new CTableColumn("Cashier Account", "accountByCashierAccount", "name"),
            new CTableColumn("Main Cash Account", "accountByMainAccount", "name"),
            new CTableColumn("Terminal", "terminal")
        });
    }

    private void initTerminal() {
        try {
            terminal = SystemCashier.getTerminalIdentifier(REGCashier.this);
        } catch (ApplicationException ex) {
            Logger.getLogger(REGCashier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private String terminal;
}
