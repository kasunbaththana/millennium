/*
 *  REGCompanyLeaveDay.java
 *  
 *  @author Soft Master Technologies (pvt) Ltd.
 *     easy@softmastergroup.com
 *  
 *  Created on Jun 24, 2014, 12:41:31 PM
 *  Copyrights Soft master technologies, All rights reserved.
 *  
 */
package com.mac.registration.company_leave_day;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author user
 */
public class REGCompanyLeaveDay extends AbstractRegistrationForm {

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCCompanyLeaveDate();
    }

    @Override
    public Class getObjectClass() {
        return com.mac.registration.company_leave_day.object.CompanyLeaveDay.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Leave Date", new String[]{"leaveDate"})
        });
    }
}
