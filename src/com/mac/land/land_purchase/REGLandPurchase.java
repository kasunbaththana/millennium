/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.land_purchase;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.land.zobjects.Land;
import com.mac.land.zutil.SystemLandMovement;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
//import java.util.Date;

/**
 *
 * @author thilanga
 */
public class REGLandPurchase extends AbstractRegistrationForm<LandTransaction> {

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCLandPurchase();
    }

    @Override
    public Class getObjectClass() {
        return Land.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn("Description", "description"),
                new CTableColumn("Location", "location"),
                new CTableColumn("Value", "value"));
    }

    @Override
    protected int save(LandTransaction object) throws DatabaseException {
        //SAVE TRANSACTION
        int transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LAND_PURCHASE_TRANSACTION_CODE,
                object.getReferenceNo(),
                object.getDocumentNo(),
                null,
                null,
                object.getLand().getClient().getCode(),
                null);

        //SAVE LAND
        Land land = object.getLand();
        land.setTransaction(transaction);
        land = (Land) getDatabaseService().save(land);
        
        //SAVE LAND MOVEMENT
        SystemLandMovement.insertLandMovement(
                getDatabaseService(), 
                transaction, 
                SystemTransactions.LAND_PURCHASE_TRANSACTION_CODE,
                land.getIndexNo(),
                null,
                null, 
                land.getOriginalSize(),
                0.0,
                land.getValue(),
                0.0);

        //SAVE SETTLEMENT
        SystemSettlement settlement = SystemSettlement.getInstance();

        settlement.beginSettlementQueue();
        settlement.addSettlementQueue(
                object.getTransactionDate(),
                object.getLand().getClient().getCode(),
                null,
                null,
                SystemTransactions.LAND_PURCHASE_TRANSACTION_CODE,
                transaction,
                "Supplier Payment for Land",
                land.getValue(),
                SystemSettlement.LOAN_AMOUNT);
        settlement.flushSettlementQueue(getDatabaseService());

        return SAVE_SUCCESS;
    }

    @Override
    protected int getRegistrationType() {
        return NEW_ONLY_REGISTRATION_TYPE;
    }
}
