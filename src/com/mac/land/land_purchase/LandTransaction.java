/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.land_purchase;

import com.mac.land.zobjects.Land;
import java.util.Date;

/**
 *
 * @author thilanga
 */
public class LandTransaction {

    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private Land land = new Land();

    public LandTransaction() {
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }
}
