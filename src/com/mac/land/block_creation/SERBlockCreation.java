/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.block_creation;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.item.zobject.Item;
import com.mac.land.block_creation.object_creator.ProjectBlock;
import com.mac.land.zobjects.LandProject;
import com.mac.land.zutil.SystemLandMovement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;

/**
 *
 * @author thilanga
 */
public class SERBlockCreation extends AbstractService {

    public SERBlockCreation(Component component) {
        super(component);
    }

    public void saveBlockCreation(BlockTransaction blockTransAction) throws DatabaseException {
        int transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LAND_BLOCK_TRANSACTION_CODE,
                blockTransAction.getReferenceNo(),
                blockTransAction.getDocumentNo(),
                null,
                null,
                null,
                null);

        LandProject landProject = blockTransAction.getLandProject();
        getDatabaseService().save(landProject);

        int i = 0;
        for (ProjectBlock object : blockTransAction.getProjectBlocks()) {
            ++i;

            Item item = new Item();
            item.setCode(landProject.getProjectCode() + "-" + i);
            item.setName(landProject.getDescription() + ", Block " + i);
            item.setCostPrice(object.getCost());
            item.setSalesPrice(object.getSalesPrice());

            getDatabaseService().save(item);

            SystemLandMovement.insertLandMovement(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.LAND_BLOCK_TRANSACTION_CODE,
                    null,
                    object.getLandProject().getIndexNo(),
                    null,
                    0.0,
                    object.getSize(),
                    0.0,
                    0.0);
        }

    }
}
