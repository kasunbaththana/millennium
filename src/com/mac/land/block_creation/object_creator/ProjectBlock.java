/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.block_creation.object_creator;

//import com.mac.land.block_creation.PCBlockCreation;
import com.mac.land.LandSize;
import com.mac.land.zobjects.LandProject;
import com.mac.land.zutil.LandCalculationUtil;

/**
 *
 * @author thilanga
 */
public class ProjectBlock {

    private LandProject landProject;
    private double size;
    private double costPrice;
    private double salesPrice;

    public ProjectBlock() {
    }

    public LandProject getLandProject() {
        return landProject;
    }

    public void setLandProject(LandProject landProject) {
        this.landProject = landProject;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getCost() {
        return costPrice;
    }

    public void setCost(double cost) {
        this.costPrice = cost;
    }

    public double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(double costPrice) {
        this.costPrice = costPrice;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    //CUSTOM
    public String getBlockSize() {
        LandSize landSize = LandCalculationUtil.perchesToLandSize(size);
        return "A:" + landSize.getAcres() + ", R:" + landSize.getRoods() + ", P:" + landSize.getPerches();
    }

    public double getProfit() {
        return salesPrice - costPrice;
    }
}
