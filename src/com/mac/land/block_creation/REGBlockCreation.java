/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.block_creation;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author thilanga
 */
public class REGBlockCreation extends AbstractRegistrationForm<BlockTransaction> {

    @Override
    public AbstractObjectCreator<BlockTransaction> getObjectCreator() {
        return new PCBlockCreation();
    }

    @Override
    public Class<? extends BlockTransaction> getObjectClass() {
        return com.mac.land.block_creation.BlockTransaction.class;
    }

    @Override
    public CTableModel<BlockTransaction> getTableModel() {
        return new CTableModel(
                new CTableColumn("Reference No", "referenceNo"),
                new CTableColumn("Document  No", "documentNo"),
                new CTableColumn("Transaction Date", "transactionDate"));
    }

    @Override
    protected int save(BlockTransaction object) throws DatabaseException {
        initService();
        serBlockCreation.saveBlockCreation(object);

        return SAVE_SUCCESS;
    }

    private void initService() {
        if (serBlockCreation == null) {
            serBlockCreation = new SERBlockCreation(this);
        }
    }
    private SERBlockCreation serBlockCreation;
}
