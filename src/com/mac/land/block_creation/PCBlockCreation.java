/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.block_creation;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.land.LandSize;
import com.mac.land.block_creation.object_creator.PCBlock;
import com.mac.land.block_creation.object_creator.ProjectBlock;
import com.mac.land.zobjects.LandProject;
import com.mac.land.zutil.LandCalculationUtil;
import com.mac.zsystem.model.table_model.land_table_model.BlockCreationTableModel;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author thilanga
 */
public class PCBlockCreation extends DefaultObjectCreator<BlockTransaction> {

    private PCBlock pcBlock;
    private LandProject landProject;

//    private static ProjectBlock projectBlock;
//    public static String projectName;
//    public static double avilableSize;
//    public double total;
    /**
     * Creates new form PCBlockCreation
     */
    public PCBlockCreation() {
        initComponents();
        initOthers();
    }

    private List<LandProject> getLandProjects() {
        List<LandProject> landProjects;
        try {
            String hql = "FROM com.mac.land.zobjects.LandProject";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            landProjects = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            landProjects = new ArrayList();
        }
        return landProjects;
    }

    private void landProjectChanged() {
        landProject = (LandProject) cboLandProject.getCValue();
        pcBlock.setLandProject(landProject);

        resetProjectSize();
    }

    private void resetProjectSize() {
        double projectSize = landProject.getProjectSize();
        LandSize projectSizeLS = LandCalculationUtil.perchesToLandSize(projectSize);
        txtSizeAcres.setCValue(projectSizeLS.getAcres());
        txtSizeRoods.setCValue(projectSizeLS.getRoods());
        txtSizePerches.setCValue(projectSizeLS.getPerches());

        double availableSize = landProject.getAvailableSize();
        LandSize availableSizeLS = LandCalculationUtil.perchesToLandSize(availableSize);
        txtAvailableAcres.setCValue(availableSizeLS.getAcres());
        txtAvailableRoods.setCValue(availableSizeLS.getRoods());
        txtAvailablePerchese.setCValue(availableSizeLS.getPerches());
    }

    private void newBolockCreated() {
        double totalSize = 0.;
        double totalCost = 0.0;

        for (ProjectBlock block : (Collection<ProjectBlock>) tblAddingTable.getCValue()) {
            totalSize += block.getSize();
            totalCost += block.getCost();
        }

        double availableSize = landProject.getProjectSize() - totalSize;
        landProject.setAvailableSize(availableSize);

        txtTotalBlockCost.setCValue(totalCost);
        resetProjectSize();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        cboLandProject.setTableModel(new BlockCreationTableModel());
        pcBlock = new PCBlock();
        tblAddingTable.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Size", "BlockSize"),
            new CTableColumn("Sales Price", "salesPrice"),
            new CTableColumn("Cost Price", "costPrice")
        }));
        tblAddingTable.setObjectCreator(pcBlock);
        tblAddingTable.setDescrption("Block Creation");
        txtReferanceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.LAND_BLOCK_CREATION));

        cboLandProject.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                landProjectChanged();
//                setProjectName();
            }
        });

        tblAddingTable.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                newBolockCreated();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cIStringField1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferanceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cboLandProject = new com.mac.af.component.derived.input.combobox.CIComboBox(){

            @Override
            public List getComboData(){
                return getLandProjects();
            }
        };
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        tblAddingTable = new com.mac.af.component.derived.input.table.CIAddingTable();
        txtSizeAcres = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtSizeRoods = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtSizePerches = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalBlockCost = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtAvailableAcres = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtAvailableRoods = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtAvailablePerchese = new com.mac.af.component.derived.input.textfield.CIDoubleField();

        cIStringField1.setText("cIStringField1");

        cDLabel1.setText("Reference No:");

        cDLabel2.setText("Document No:");

        cDLabel3.setText("Transaction Date:");

        cDLabel4.setText("Land Project:");

        cDLabel5.setText("Acres :");

        cDLabel6.setText("Roods :");

        cDLabel7.setText("Perches :");

        cDLabel8.setText("Total Block Cost :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(cDLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cDLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cDLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cDLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboLandProject, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtReferanceNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSizeAcres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAvailableAcres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSizePerches, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAvailablePerchese, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSizeRoods, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAvailableRoods, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addComponent(tblAddingTable, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(txtTotalBlockCost, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(12, 12, 12))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferanceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLandProject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSizeAcres, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAvailableAcres, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSizeRoods, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAvailableRoods, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSizePerches, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAvailablePerchese, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tblAddingTable, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalBlockCost, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.input.textfield.CIStringField cIStringField1;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLandProject;
    private com.mac.af.component.derived.input.table.CIAddingTable tblAddingTable;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAvailableAcres;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAvailablePerchese;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAvailableRoods;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferanceNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtSizeAcres;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtSizePerches;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtSizeRoods;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalBlockCost;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList();
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                (Component) txtReferanceNo,
                (Component) txtDocumentNo,
                (Component) txtTransactionDate,
                (Component) cboLandProject,
                (Component) tblAddingTable);
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtReferanceNo, "referenceNo"),
                new CInputComponentBinder(txtDocumentNo, "documentNo"),
                new CInputComponentBinder(txtTransactionDate, "transactionDate"),
                new CInputComponentBinder(cboLandProject, "landProject"),
                new CInputComponentBinder(tblAddingTable, "projectBlocks"));
    }

    @Override
    protected List<Component> getUneditableComponents() {
        return Arrays.asList(
                (Component) txtReferanceNo,
                (Component) txtTransactionDate,
                (Component) txtSizeAcres,
                (Component) txtSizeRoods,
                (Component) txtSizePerches,
                (Component) txtAvailableAcres,
                (Component) txtAvailableRoods,
                (Component) txtAvailablePerchese,
                (Component) txtTotalBlockCost);
    }

    @Override
    protected Class<? extends BlockTransaction> getObjectClass() {
        return com.mac.land.block_creation.BlockTransaction.class;
    }
//    private void setProjectName() {
//        landProject = (LandProject) cboLandProject.getCValue();
//        projectName = landProject.getDescription();
//        avilableSize = total;
//    }
}
