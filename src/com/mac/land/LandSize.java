/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land;

/**
 *
 * @author thilanga
 */
public class LandSize {

    private double acres;
    private double roods;
    private double perches;

    public LandSize() {
    }

    public LandSize(double acres, double roods, double perches) {
        this.acres = acres;
        this.roods = roods;
        this.perches = perches;
    }

    public double getAcres() {
        return acres;
    }

    public void setAcres(double acres) {
        this.acres = acres;
    }

    public double getRoods() {
        return roods;
    }

    public void setRoods(double roods) {
        this.roods = roods;
    }

    public double getPerches() {
        return perches;
    }

    public void setPerches(double perches) {
        this.perches = perches;
    }
}
