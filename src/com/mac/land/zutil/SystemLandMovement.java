/*
 *  SystemLandMovement.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 10, 2014, 7:41:29 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.land.zutil;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.CApplication;
import com.mac.land.zobjects.LandMovement;
import java.util.Date;

/**
 *
 * @author mohan
 */
public class SystemLandMovement {

    public static void insertLandMovement(
            HibernateDatabaseService databaseService,
            int transaction,
            String transactionType,
            Integer land,
            Integer project,
            Integer block,
            double land_in,
            double land_out,
            double credit,
            double debit) throws DatabaseException {
        
        LandMovement landMovement = new LandMovement();
        landMovement.setTransaction(transaction);
        landMovement.setTransactionType(transactionType);
        landMovement.setTransactionDate((Date)CApplication.getSessionVariable(CApplication.WORKING_DATE));
        landMovement.setBranch((String)CApplication.getSessionVariable(CApplication.USER_ID));
        landMovement.setLand(land);
        landMovement.setProject(project);
        landMovement.setBlock(block);
        landMovement.setLandIn(land_in);
        landMovement.setLandOut(land_out);
        landMovement.setCreditAmount(credit);
        landMovement.setDebitAmount(debit);
        
        databaseService.save(landMovement);
    }
}
