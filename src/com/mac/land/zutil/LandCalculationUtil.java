/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.zutil;

import com.mac.land.LandSize;

/**
 *
 * @author thilanga
 */
public class LandCalculationUtil {

    private static final double PERCHES_PER_ACRES = 160.0;
    private static final double PERCHES_PER_ROOD = 40.0;

    public static double landSizeToPerches(double acres, double roods, double perches) {
        return ((acres * PERCHES_PER_ACRES) + (roods * PERCHES_PER_ROOD) + perches);
    }

    public static LandSize perchesToLandSize(double perchesAmount) {
        LandSize landSize = new LandSize();

        int acres = (int) (perchesAmount / PERCHES_PER_ACRES);
        perchesAmount -= (acres * PERCHES_PER_ACRES);
        landSize.setAcres(acres);

        int roods = (int) (perchesAmount / PERCHES_PER_ROOD);
        perchesAmount -= (roods * PERCHES_PER_ROOD);
        landSize.setRoods(roods);

        double perches = perchesAmount;
        landSize.setPerches(perches);

        return landSize;
    }
}
