/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.land_project;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import static com.mac.af.templates.registration.AbstractRegistrationForm.SAVE_SUCCESS;
import com.mac.land.zobjects.Land;
import com.mac.land.zobjects.LandProject;
import com.mac.land.zutil.SystemLandMovement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;

/**
 *
 * @author thilanga
 */
public class REGProject extends AbstractRegistrationForm<LandProjectTransaction> {

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCProject();
    }

    @Override
    public Class getObjectClass() {
        return com.mac.land.zobjects.LandProject.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn("Description", "description"),
                new CTableColumn("Land", "land", "description"),
                new CTableColumn("Location", "land", "location"));
    }

    @Override
    protected int save(LandProjectTransaction object) throws DatabaseException {
        //SAVE TRANSACTION
        int transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LAND_PROJECT_TRANSACTION_CODE,
                object.getReferenceNo(),
                object.getDocumentNo(),
                null,
                null,
                null,
                null);

        //SAVE PROJECT
        LandProject landProject = object.getLandProject();
        landProject.setTransaction(transaction);
        landProject = (LandProject) getDatabaseService().save(landProject);

        //SAVE LAND
        Land land = landProject.getLand();
        land.setAvailableSize(land.getAvailableSize() - landProject.getAvailableSize());
        getDatabaseService().save(land);

        //SAVE LAND MOVEMNENT
        SystemLandMovement.insertLandMovement(
                getDatabaseService(),
                transaction,
                SystemTransactions.LAND_PROJECT_TRANSACTION_CODE,
                null,
                landProject.getIndexNo(),
                null,
                landProject.getProjectSize(),
                0.0,
                landProject.getProjectValue(),
                0.0);

        SystemLandMovement.insertLandMovement(
                getDatabaseService(),
                transaction,
                SystemTransactions.LAND_PROJECT_TRANSACTION_CODE,
                landProject.getLand().getIndexNo(),
                null,
                null,
                0.0,
                landProject.getProjectSize(),
                0.0,
                0.0);

        return SAVE_SUCCESS;
    }

    public double getProjectCost(Land land, double size) {
        double cost = (land.getOriginalSize() / land.getValue()) * size;

        return cost;
    }

    @Override
    protected int getRegistrationType() {
        return NEW_ONLY_REGISTRATION_TYPE;
    }
}
