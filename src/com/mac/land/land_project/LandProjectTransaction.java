/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.land_project;

import com.mac.land.zobjects.LandProject;
import java.util.Date;

/**
 *
 * @author thilanga
 */
public class LandProjectTransaction {

    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private LandProject landProject = new LandProject();

    public LandProjectTransaction() {
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public LandProject getLandProject() {
        return landProject;
    }

    public void setLandProject(LandProject landProject) {
        this.landProject = landProject;
    }
}
