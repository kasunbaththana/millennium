/*
 *  SERCapitalExpenditure.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 10, 2014, 11:24:15 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.land.capital_expenditure;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.land.capital_expenditure.object_creator.Expenditure;
import com.mac.land.zobjects.LandExpenditure;
import com.mac.land.zutil.SystemLandMovement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;

/**
 *
 * @author mohan
 */
public class SERCapitalExpenditure extends AbstractService {

    public SERCapitalExpenditure(Component component) {
        super(component);
    }

    public void save(ExpenditureTransaction object) throws DatabaseException {
        int transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LAND_EXPENDITURE_TRANSACTION_CODE,
                object.getReferenceNo(),
                object.getDocumentNo(),
                null,
                null,
                null,
                null);

        for (Expenditure expenditure : object.getExpenditures()) {
            SystemLandMovement.insertLandMovement(
                    getDatabaseService(),
                    transaction,
                    SystemTransactions.LAND_EXPENDITURE_TRANSACTION_CODE,
                    null,
                    object.getLandProject().getIndexNo(),
                    null,
                    0.0,
                    0.0,
                    expenditure.getExpenditureValue(),
                    0.0);

            LandExpenditure landExpenditure = new LandExpenditure();
            landExpenditure.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
            landExpenditure.setProject(object.getLandProject().getIndexNo());
            landExpenditure.setDescription(expenditure.getExpenditureName());
            landExpenditure.setValue(expenditure.getExpenditureValue());
            landExpenditure.setTransaction(transaction);
            landExpenditure.setTransactionDate(object.getTransactionDate());

            getDatabaseService().save(landExpenditure);
        }
    }
}
