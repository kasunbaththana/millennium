/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.capital_expenditure;

import com.mac.land.capital_expenditure.object_creator.Expenditure;
import com.mac.land.zobjects.LandProject;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author thilanga
 */
public class ExpenditureTransaction {

    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private LandProject landProject;
    private Collection<Expenditure> expenditures;

    public ExpenditureTransaction() {
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public LandProject getLandProject() {
        return landProject;
    }

    public void setLandProject(LandProject landProject) {
        this.landProject = landProject;
    }

    public Collection<Expenditure> getExpenditures() {
        return expenditures;
    }

    public void setExpenditures(Collection<Expenditure> expenditures) {
        this.expenditures = expenditures;
    }
}
