/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.capital_expenditure.object_creator;

/**
 *
 * @author thilanga
 */
public class Expenditure {

    private String expenditureName;
    private Double expenditureValue;

    public Expenditure() {
    }

    public String getExpenditureName() {
        return expenditureName;
    }

    public void setExpenditureName(String expenditureName) {
        this.expenditureName = expenditureName;
    }

    public Double getExpenditureValue() {
        return expenditureValue;
    }

    public void setExpenditureValue(Double expenditureValue) {
        this.expenditureValue = expenditureValue;
    }
}
