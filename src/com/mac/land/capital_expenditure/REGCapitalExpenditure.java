/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.land.capital_expenditure;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;

/**
 *
 * @author thilanga
 */
public class REGCapitalExpenditure extends AbstractRegistrationForm<ExpenditureTransaction> {

    @Override
    public AbstractObjectCreator<ExpenditureTransaction> getObjectCreator() {
        return new PCCapitalExpenditure();
    }

    @Override
    public Class<? extends ExpenditureTransaction> getObjectClass() {
        return com.mac.land.capital_expenditure.ExpenditureTransaction.class;
    }

    @Override
    public CTableModel<ExpenditureTransaction> getTableModel() {
        return new CTableModel(
                new CTableColumn("Reference No", "referenceNo"),
                new CTableColumn("Document  No", "documentNo"),
                new CTableColumn("Transaction Date", "transactionDate"));
    }

    @Override
    protected int save(ExpenditureTransaction object) throws DatabaseException {
        initService();
        serCapitalExpenditure.save(object);
        return SAVE_SUCCESS;
    }

    private void initService() {
        if (serCapitalExpenditure == null) {
            serCapitalExpenditure = new SERCapitalExpenditure(this);
        }
    }
    private SERCapitalExpenditure serCapitalExpenditure;
}
