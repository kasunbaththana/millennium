/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zutil.remind_letter;

/**
 *
 * @author NIMESH-PC
 */
public class LetterStatus {

    public static final String LETTER_NO = "NO";
    
    public static final String FIRST_LETTER = "FIRST_LETTER";
    public static final String SECOND_LETTER = "SECOND_LETTER";
    public static final String THIRD_LETTER = "THIRD_LETTER";
    
    public static final String PENDING_STATUS = "0";
    public static final String DEACTIVATE_STATUS = "1";
}
