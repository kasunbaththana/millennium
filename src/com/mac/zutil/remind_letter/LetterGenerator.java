/*
 *  LetterGenerator.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 1, 2014, 2:07:07 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zutil.remind_letter;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.registration.remind_letter_setup.object.RemindLetterSetup;
import com.mac.zreport.ReportBuilder;
import com.mac.zutil.remind_letter.object.RemindLetterHistory;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.swing.JRViewer;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Nimesh
 */
public class LetterGenerator extends CPanel {

    /**
     * Creates new form LetterGenerator
     */
    public LetterGenerator() {
        initComponents();

        initOthers();
    }

    @Action(asynchronous = true)
    public void doGenerate() {
        int level = cboRemindLetterLevel.getSelectedIndex() + 1;

        String path = "remind_letter_" + level + ".jrxml";

        try {
            
            
            JasperDesign jasperDesign = JRXmlLoader.load(path);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

            HashMap<String, Object> params = new HashMap<>();
            ReportBuilder.initDefaultParameters();
            params.putAll(ReportBuilder.DEFAULT_PARAMETERS);
            List<RemindLetterHistory> remindLetterHistorys = new ArrayList<>();
//            List<Loan> loans = new ArrayList<>();
            StringBuilder stringBuilder = new StringBuilder();
            RemindLetterHistory letterHistory;
            for (Object object : tblReminds.getCValue()) {
                letterHistory = (RemindLetterHistory) object;

                if (letterHistory.isSelected()) {
                    stringBuilder.append(letterHistory.getLoan().getIndexNo()).append(",");
                    //                    loanIndexes.add(letterHistory.getLoan().getIndexNo());
                    Date sDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);

                    params.put("LOAN_INDEX_SET", letterHistory.getLoan().getIndexNo().toString());
                    System.out.println("letterHistory.getLoan().getIndexNo()"+letterHistory.getLoan().getIndexNo().toString());
                    params.put("H_DATE", CApplication.getSessionVariable(CApplication.WORKING_DATE));
                    params.put("H_BRANCH", CApplication.getSessionVariable(CApplication.STORE_ID));
                    
                     
//                    TransactionUtil.PrintRemindLetter(getDatabaseService(), "", params);.

                    final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, getConnection());



                    //get Client Name
                    String client_name = "";
                    ResultSet rs = getConnection()
                            .createStatement()
                            .executeQuery("SELECT name FROM client WHERE code='" + letterHistory.getLoan().getClient() + "'");
                    if (rs.next()) {
                        client_name = rs.getString("name");
                    }


                    //Create File Location and name
                    String folderName = "Remind_letter_" + level;
                    String FileName =
                            CApplication.getSessionVariable(CApplication.WORKING_DATE)
                            + "__" + letterHistory.getLoan().getAgreementNo().toString().replaceAll("/", "_")
                            + "__" + client_name;
//                            + "" + letterHistory.getLoan().getIndexNo();

                    File destFile = new File("C:/REMIND_LETTERS/" + folderName + "/" + FileName + ".docx");
                    
                    
                    //Export To Word Document
                    JRDocxExporter exporter = new JRDocxExporter();
                    exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(destFile));
                    exporter.exportReport();

                    //save remind
                    RemindLetterHistory reminLetter = new RemindLetterHistory();

                    reminLetter.setBranch(CApplication.getSessionVariable(CApplication.STORE_ID).toString());
                    reminLetter.setTransactionDate(sDate);
                    reminLetter.setLoan(letterHistory.getLoan());
                    reminLetter.setLoanBalance(letterHistory.getLoanBalance());
//                    reminLetter.setLevel(letterHistory.getLevel());
                    reminLetter.setLetterStatus("REMIND_LETTER_"+letterHistory.getLevel());
//                    Loan loan = letterHistory.getLoan();

//                    switch (letterHistory.getLetterStatus()) {
//                        case "NO":
//                            reminLetter.setLetterStatus(LetterStatus.FIRST_LETTER);
//                            break;
//                        case "FIRST_LETTER":
//                            reminLetter.setLetterStatus(LetterStatus.SECOND_LETTER);
//                            break;
//                        case "SECOND_LETTER":
//                            reminLetter.setLetterStatus(LetterStatus.THIRD_LETTER);
//                            break;
//                        default:
//                            throw new AssertionError();
//                    }


//                    loans.add(loan);

                    remindLetterHistorys.add(reminLetter);

                }
            }
            if (stringBuilder.length() != 0) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }

            letterGenerator.saveRemindLetterHistory(remindLetterHistorys);

            // print Report
            params.put("LOAN_INDEX_SET", stringBuilder.toString());
            System.out.println("LOAN_INDEX_SET"+stringBuilder.toString());
            params.put("H_DATE", CApplication.getSessionVariable(CApplication.WORKING_DATE));
            params.put("H_BRANCH", CApplication.getSessionVariable(CApplication.STORE_ID));

            System.out.println(stringBuilder.toString());

            final JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, getConnection());



            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    JRViewer viewer = new JRViewer(jasperPrint);

                    pnlRight.removeAll();
                    pnlRight.add(viewer);
                }
            };

            CApplication.invokeEventDispatch(runnable);


        } catch (JRException | SQLException e) {
            mOptionPane.showMessageDialog(null, "Unable to generate remind letters.", "Error", mOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        setRemindLevel();
    }

    private void setRemindLevel() {
        try {
            int level = cboRemindLetterLevel.getSelectedIndex() + 1;

            List<RemindLetterHistory> letterHistorys;
            
            //remind letter setup get level
         RemindLetterSetup  letter1=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_1")).uniqueResult();
         RemindLetterSetup  letter2=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_2")).uniqueResult();
         RemindLetterSetup  letter3=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_3")).uniqueResult();
         RemindLetterSetup  letter4=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_4")).uniqueResult();
        
//First Remind Letter, Second Remind Letter, Third Remind Letter
            System.out.println("level__"+level);
            switch (level) {
                case 0:
                case 1:
                    //                samarasiri
    //                letterHistorys = letterGenerator.getReminLetter(45, 59);
                    //badulla
//                    letterHistorys = letterGenerator.getReminLetterByVolume(2, 2.9);
                    letterHistorys = letterGenerator
                            .getReminLetterByVolume(letter1.getMinLevel(), letter1.getMaxLevel());
                    break;
                case 2:
                    //                samarasiri
    //                letterHistorys = letterGenerator.getReminLetter(60, 73);
                    //badulla
//                    letterHistorys = letterGenerator.getReminLetterByVolume(3, 3.9);
                    letterHistorys = letterGenerator
                            .getReminLetterByVolume(letter2.getMinLevel(), letter2.getMaxLevel());
                    break;
                case 3:
                    //                samarasiri
    //                letterHistorys = letterGenerator.getReminLetter(74, 100000);
                    //badulla
//                    letterHistorys = letterGenerator.getReminLetterByVolume(4.0, 1000);
                    letterHistorys = letterGenerator
                            .getReminLetterByVolume(letter3.getMinLevel(), letter3.getMaxLevel());
                    break;
                    case 4:
                    //                samarasiri
    //                letterHistorys = letterGenerator.getReminLetter(74, 100000);
                    //badulla
//                    letterHistorys = letterGenerator.getReminLetterByVolume(4.0, 1000);
                    letterHistorys = letterGenerator
                            .getReminLetterByVolume(letter4.getMinLevel(), letter4.getMaxLevel());
                    break;
                default:
                    throw new AssertionError();
            }
            
            
          
            
            tblReminds.setCValue(letterHistorys);
        } catch (Exception ex) {
            Logger.getLogger(LetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();

        return connection;
    }

    @Action
    public void doClose() {
        tblReminds.setCValue(null);
        TabFunctions.closeTab(this);
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        //SERVICE
        letterGenerator = new SERLetterGenerator(this);

        //Create Folder If not Exist
        createFolders();



        //ACTIONS
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnGenerate, "doGenerate");
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnFind, "doFind");

        //
        btnGenerate.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_GENERATE, 16, 16));
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        btnFind.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON));
        
        CTableModel tableModel = new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Selected", new String[]{"selected"}, true),
            new CTableColumn("Agreement No", "loan", "agreementNo"),
            new CTableColumn("loan", "loan", "indexNo"),
            new CTableColumn("Balance", "loanBalance"),
//            new CTableColumn("Volume", "remindLevel")
            new CTableColumn("Volume", "level")
        });
        tblReminds.setCModel(tableModel);
        
        List<com.mac.zutil.remind_letter.object.RemindLetterSetup> ls =letterGenerator.setlettertitle();
        for(com.mac.zutil.remind_letter.object.RemindLetterSetup remindLetterSetup:ls)
        {
         cboRemindLetterLevel.addItem(remindLetterSetup.getName());
        
        }
       
        
        setRemindLevel();

        cboRemindLetterLevel.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setRemindLevel();
            }
        });
            

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        pnlLeft = new javax.swing.JPanel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cboRemindLetterLevel = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReminds = new com.mac.af.component.derived.input.table.CITable();
        jPanel1 = new javax.swing.JPanel();
        btnGenerate = new com.mac.af.component.derived.command.button.CCButton();
        txtTableFind = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnFind = new com.mac.af.component.derived.command.button.CCButton();
        pnlRight = new javax.swing.JPanel();

        jSplitPane1.setDividerLocation(300);

        cDLabel2.setText("Remind Letter Level :");

        cboRemindLetterLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Remind_letter_1", "Remind_letter_2", "Remind_letter_3" }));

        tblReminds.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblReminds);

        btnGenerate.setText("Generate");
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        btnClose.setText("Close");

        btnFind.setText("Find");
        btnFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(txtTableFind, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTableFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlLeftLayout = new javax.swing.GroupLayout(pnlLeft);
        pnlLeft.setLayout(pnlLeftLayout);
        pnlLeftLayout.setHorizontalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboRemindLetterLevel, 0, 173, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlLeftLayout.setVerticalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboRemindLetterLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jSplitPane1.setLeftComponent(pnlLeft);

        pnlRight.setLayout(new javax.swing.BoxLayout(pnlRight, javax.swing.BoxLayout.LINE_AXIS));
        jSplitPane1.setRightComponent(pnlRight);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnFindActionPerformed

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGenerateActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnFind;
    private com.mac.af.component.derived.command.button.CCButton btnGenerate;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private javax.swing.JComboBox cboRemindLetterLevel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlRight;
    private com.mac.af.component.derived.input.table.CITable tblReminds;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTableFind;
    // End of variables declaration//GEN-END:variables
    private SERLetterGenerator letterGenerator;

    private void createFolders() {
        File remindLetterFolder = new File("C:/REMIND_LETTERS");
        File remindLetter1Folder = new File("C:/REMIND_LETTERS/Remind_letter_1");
        File remindLetter2Folder = new File("C:/REMIND_LETTERS/Remind_letter_2");
        File remindLetter3Folder = new File("C:/REMIND_LETTERS/Remind_letter_3");

        if (!remindLetterFolder.exists()) {
            remindLetterFolder.mkdir();
        }
        if (!remindLetter1Folder.exists()) {
            remindLetter1Folder.mkdir();
        }
        if (!remindLetter2Folder.exists()) {
            remindLetter2Folder.mkdir();
        }
        if (!remindLetter3Folder.exists()) {
            remindLetter3Folder.mkdir();
        }
    }
    @Action
    public void doFind() throws DatabaseException {
         
           tblReminds.find(txtTableFind.getCValue());
           
    }
}
