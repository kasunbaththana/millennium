///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
package com.mac.zutil.remind_letter;
//
//import com.mac.af.core.database.DatabaseException;
//import com.mac.af.core.database.hibernate.HibernateSQLQuery;
//import com.mac.af.core.environment.CApplication;
//import com.mac.af.core.environment.service.AbstractService;
//import com.mac.zutil.remind_letter.object.LetterTemplate;
//import com.mac.zutil.remind_letter.object.RemindLetterHistory;
//import java.awt.Component;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author NIMESH-PC
// */
public class Old {
// 
//    /*
// *  SERLetterGenerator.java
// *  
// *  @author Channa Mohan
// *     hjchanna@gmail.com
// *  
// *  Created on Dec 2, 2014, 10:21:22 AM
// *  Copyrights channa mohan, All rights reserved.
// *  
// */
//package com.mac.zutil.remind_letter;
//
//import com.mac.af.core.database.DatabaseException;
//import com.mac.af.core.database.hibernate.HibernateSQLQuery;
//import com.mac.af.core.environment.CApplication;
//import com.mac.af.core.environment.service.AbstractService;
//import com.mac.zutil.remind_letter.object.LetterTemplate;
//import com.mac.zutil.remind_letter.object.RemindLetterHistory;
//import java.awt.Component;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author mohan
// */
//public class SERLetterGenerator extends AbstractService {
//
//    public SERLetterGenerator(Component component) {
//        super(component);
//    }
//
////    public List<RemindLetterHistory> getRecentLetters(int level) {
////        String hql = "FROM com.mac.zutil.remind_letter.object.RemindLetterHistory WHERE level=:LEVEL";
////
////
////        HashMap<String, Object> params = new HashMap<>();
////        params.put("LEVEL", level);
////
////        List<RemindLetterHistory> remindLetterHistories;
////        try {
////            remindLetterHistories = getDatabaseService().getCollection(hql, params);
////        } catch (DatabaseException ex) {
////            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
////            remindLetterHistories = new ArrayList<>();
////        }
////
////        return remindLetterHistories;
////    }
//    public List<RemindLetterHistory> getRecentLetters(int startLevel, int endLevel) {
//        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
//        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
//
//
//
//        String fDate = dateFormat.format(transactionDate);
//
//        String sql =
//                "select\n"
//                + "	settlement.loan as index_no,\n"
//                + "	settlement.loan as loan,\n"
//                + "	settlement.branch as branch,\n"
//                + "	min(settlement.due_date) as transaction_date,\n"
//                + "	(\n"
//                + "	select sum(st1.balance_amount) \n"
//                + "	from settlement st1 left join settlement_type on settlement_type.code = st1.settlement_type\n"
//                + "	where \n"
//                + "		settlement_type.credit_or_debit = 'CREDIT' \n"
//                + "		and st1.loan = settlement.loan \n"
//                + "		and st1.due_date < '" + fDate + "'\n"
//                + "		and st1.status <> 'CANCEL'\n"
//                + "	) as loan_balance,\n"
//                + "	datediff('" + fDate + "',settlement.due_date) as remind_level\n"
//                + "from\n"
//                + "	settlement\n"
//                + "where\n"
//                + "	settlement.due_date < '" + fDate + "'\n"
//                + "	and settlement.settlement_type = 'LOAN_CAPITAL'\n"
//                + "	and settlement.status = 'PENDING'\n"
//                + "     and settlement.branch = '" + branch + "'"
//                + "group by\n"
//                + "     settlement.loan\n"
//                + "having \n"
//                + "loan_balance > 0.1\n"
//                + "and remind_level between " + startLevel + " and " + endLevel + "";
//
////        System.out.println("zzz_"+sql);
//
//
//        /* List<RemindLetterHistory> remindLetterHistorys = null;
//         try {
//         Connection connection = CConnectionProvider.getInstance().getConnection();
//         Statement statement = connection.createStatement();
//         ResultSet resultSet = statement.executeQuery(sql);
//            
//         remindLetterHistorys = new ArrayList<>();
//         while (resultSet.next()) {
//         RemindLetterHistory history = new RemindLetterHistory();
//                
//         history.setLoan((Loan)getDatabaseService().getObject(Loan.class, resultSet.getInt("loan")));
//                
//         history.setBranch(resultSet.getString("branch"));
//         history.setTransactionDate(resultSet.getDate("transaction_date"));
//         history.setLoanBalance(resultSet.getDouble("loan_balance"));
//         history.setRemindLevel(resultSet.getInt("remind_level"));
//                
//         remindLetterHistorys.add(history);
//         }
//         } catch (SQLException | DatabaseException e) {
//         e.printStackTrace();
//         return new ArrayList<>();
//         }*/
//
//
//        System.out.println(sql);
//
//        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(sql, RemindLetterHistory.class);
//
//        List<RemindLetterHistory> remindLetterHistorys;
//        try {
//            remindLetterHistorys = getDatabaseService().getCollection(hibernateSQLQuery, null);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(com.mac.zutil.remind_letter.SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
//            remindLetterHistorys = new ArrayList<>();
//        }
//
//        System.out.println("----------------------------------------");
//        System.out.println("----------------------------------------");
//        System.out.println("----------------------------------------");
//        System.out.println(remindLetterHistorys.size());
//
//        return remindLetterHistorys;
//    }
//
//    public List<RemindLetterHistory> getFirstLetter() {
//        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
//        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
//
//        String fDate = dateFormat.format(transactionDate);
//
//
//        String FirstSql = "select\n"
//                + "        settlement.index_no,"
//                + "        settlement.branch as branch,\n"
//                + "        min(settlement.due_date) as transaction_date,\n"
//                + "        settlement.loan as loan,\n"
//                + "        (  select\n"
//                + "            sum(st1.balance_amount)   \n"
//                + "        from\n"
//                + "            settlement st1 \n"
//                + "        left join\n"
//                + "            settlement_type \n"
//                + "                on settlement_type.code = st1.settlement_type  \n"
//                + "        where\n"
//                + "            settlement_type.credit_or_debit = 'CREDIT'    \n"
//                + "            and st1.loan = settlement.loan    \n"
//                + "            and st1.due_date < '" + fDate + "'  \n"
//                + "            and st1.status <> 'CANCEL'  ) as loan_balance,\n"
//                + "        datediff( '" + fDate + "' ,\n"
//                + "        settlement.due_date) as remind_level,\n"
//                + "        (  select\n"
//                + "            sum(st1.balance_amount)            \n"
//                + "        from\n"
//                + "            settlement st1          \n"
//                + "        left join\n"
//                + "            settlement_type                  \n"
//                + "                on settlement_type.code = st1.settlement_type           \n"
//                + "        where\n"
//                + "            settlement_type.credit_or_debit = 'CREDIT'                 \n"
//                + "            and st1.loan = settlement.loan                 \n"
//                + "            and st1.due_date < '" + fDate + "'              \n"
//                + "            and st1.status <> 'CANCEL'  ) / loan.installment_amount as volume ,\n"
//                + "           '" + fDate + "'   as sent_date,\n"
//                + "         'j' as letter_status,\n"
//                + "        '0' as status  \n"
//                + "    from\n"
//                + "        settlement \n"
//                + "    left join\n"
//                + "        loan \n"
//                + "            on settlement.loan=loan.index_no \n"
//                + "    where\n"
//                + "        settlement.due_date < '" + fDate + "'  \n"
//                + "        and settlement.settlement_type = 'LOAN_CAPITAL'  \n"
//                + "        and settlement.status = 'PENDING'      \n"
//                + "        and settlement.branch = '" + branch + "'  \n"
//                + "    group by\n"
//                + "        settlement.loan \n"
//                + "    having\n"
//                + "        volume >= 2";
//
//
//        System.out.println("FirstSql_____" + FirstSql);
//
//        List<RemindLetterHistory> newFirstLetter=new ArrayList<>();
//        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);
//
////        List<RemindLetterHistory> existingFirstLetter;
////        List<RemindLetterHistory> remindLetterHistorys = new ArrayList<>();
//
//
//        try {
//            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);
//
////            existingFirstLetter = getDatabaseService().initCriteria(RemindLetterHistory.class)
////                    .add(Restrictions.eq("letterStatus", LetterStatus.FIRST_LETTER))
////                    .add(Restrictions.eq("status", LetterStatus.ACTIVE_STATUS)).list();
////
////            for (RemindLetterHistory newRemindLetter1 : newFirstLetter) {
////                        remindLetterHistorys.add(newRemindLetter1);
////
////                for (RemindLetterHistory existRemindLetter2 : existingFirstLetter) {
////
////                    if (newRemindLetter1.getLoan().equals(existRemindLetter2.getLoan())
////                            && newRemindLetter1.getLoanBalance() == existRemindLetter2.getLoanBalance()) {
////                        
////                        remindLetterHistorys.remove(newRemindLetter1);
////                    }
////                }
////            }
//            
////            System.out.println(newFirstLetter.toString());
//
//
//
//
//        } catch (DatabaseException ex) {
//            Logger.getLogger(com.mac.zutil.remind_letter.SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
//            newFirstLetter = new ArrayList<>();
//        }
//
//        return newFirstLetter;
//    }
//
//    public List<RemindLetterHistory> getSecondtLetter() {
//        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
//        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
//
//        String secondSQL = ""
//                + "SELECT \n"
//                + "		index_no,\n"
//                + "		branch,\n"
//                + "		transaction_date,\n"
//                + "		loan,\n"
//                + "		loan_balance,\n"
//                + "		DATEDIFF('" + transactionDate + "', remind_letter_history.sent_date) as remind_level,\n"
//                + "		volume,\n"
//                + "		sent_date,\n"
//                + "		letter_status,\n"
//                + "		status\n"
//                + "FROM  remind_letter_history\n"
//                + "WHERE  remind_letter_history.letter_status='" + LetterStatus.FIRST_LETTER + "' "
//                + "AND  remind_letter_history.`status`='" + LetterStatus.ACTIVE_STATUS + "' "
//                + "AND branch='" + branch + "'\n"
//                + "HAVING remind_level > 15";
//        System.out.println("SECOND_____" + secondSQL);
//
//        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(secondSQL, RemindLetterHistory.class);
//
//        List<RemindLetterHistory> remindLetterHistorys;
//
//
//        try {
//            remindLetterHistorys = getDatabaseService().getCollection(hibernateSQLQuery, null);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(com.mac.zutil.remind_letter.SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
//            remindLetterHistorys = new ArrayList<>();
//        }
//        return remindLetterHistorys;
//    }
//
//    public List<RemindLetterHistory> getThirdLetter() {
//        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
//        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
//
//        String secondSQL = ""
//                + "SELECT \n"
//                + "		index_no,\n"
//                + "		branch,\n"
//                + "		transaction_date,\n"
//                + "		loan,\n"
//                + "		loan_balance,\n"
//                + "		DATEDIFF('" + transactionDate + "', remind_letter_history.sent_date) as remind_level,\n"
//                + "		volume,\n"
//                + "		sent_date,\n"
//                + "		letter_status,\n"
//                + "		status\n"
//                + "FROM  remind_letter_history\n"
//                + "WHERE  remind_letter_history.letter_status='" + LetterStatus.SECOND_LETTER + "' "
//                + "AND  remind_letter_history.`status`='" + LetterStatus.ACTIVE_STATUS + "' "
//                + "AND branch='" + branch + "'\n"
//                + "HAVING remind_level > 20";
//        System.out.println("THIRD______" + secondSQL);
//
//        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(secondSQL, RemindLetterHistory.class);
//
//        List<RemindLetterHistory> remindLetterHistorys;
//
//
//        try {
//            remindLetterHistorys = getDatabaseService().getCollection(hibernateSQLQuery, null);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(com.mac.zutil.remind_letter.SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
//            remindLetterHistorys = new ArrayList<>();
//        }
//        return remindLetterHistorys;
//    }
//
//    public List<RemindLetterHistory> getNewLetters(LetterTemplate letterTemplate) {
//        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
//        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
//        Integer level = letterTemplate.getLevel();
//
////        String sql =
////                "SELECT "
////                + "null AS index_no,"
////                + "'" + branch + "' AS branch,"
////                + "'" + dateFormat.format(transactionDate) + "' AS transaction_date,"
////                + "settlement.loan AS loan,"
////                + "SUM(settlement.balance_amount) AS loan_balance,"
////                + "" + level + " AS level "
////                + "FROM "
////                + "settlement "
////                + "WHERE "
////                + "settlement.due_date < '" + dateFormat.format(transactionDate) + "' "
////                + "GROUP BY "
////                + "settlement.loan "
////                + "HAVING "
////                + "loan_balance > 0.1";
//
//        String fDate = dateFormat.format(transactionDate);
//        String sql =
//                "select\n"
//                + "	settlement.loan as index_no,\n"
//                + "	settlement.loan as loan,\n"
//                + "	settlement.branch as branch,\n"
//                + "	min(settlement.due_date) as transaction_date,\n"
//                + "	(\n"
//                + "	select sum(st1.balance_amount) \n"
//                + "	from settlement st1 left join settlement_type on settlement_type.code = st1.settlement_type\n"
//                + "	where \n"
//                + "		settlement_type.credit_or_debit = 'CREDIT' \n"
//                + "		and st1.loan = settlement.loan \n"
//                + "		and st1.due_date < '" + fDate + "'\n"
//                + "		and st1.status <> 'CANCEL'\n"
//                + "	) as loan_balance,\n"
//                + "	datediff('" + fDate + "',settlement.due_date) as level\n"
//                + "from\n"
//                + "	settlement\n"
//                + "where\n"
//                + "	settlement.due_date < '" + fDate + "'\n"
//                + "	and settlement.settlement_type = 'LOAN_CAPITAL'\n"
//                + "	and settlement.status = 'PENDING'\n"
//                + "     and settlement.branch = '" + branch + "'"
//                + "group by\n"
//                + "	settlement.loan\n"
//                + "having \n"
//                + "	balance > 0.1";
//
//        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(sql, RemindLetterHistory.class);
//
//        List<RemindLetterHistory> remindLetterHistorys;
//        try {
//            remindLetterHistorys = getDatabaseService().getCollection(hibernateSQLQuery, null);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(com.mac.zutil.remind_letter.SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
//            remindLetterHistorys = new ArrayList<>();
//        }
//
//        for (RemindLetterHistory remindLetterHistory : remindLetterHistorys) {
//            System.out.println(remindLetterHistory);
//        }
//
//        return remindLetterHistorys;
//    }
//
//    public List<LetterTemplate> getLetterTemplates() {
//        try {
//            return getDatabaseService().getCollection(LetterTemplate.class);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(com.mac.zutil.remind_letter.SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
//            return new ArrayList<>();
//        }
//    }
//
//    public void saveRemindLetterHistory(List<RemindLetterHistory> remindLetterHistories) {
//        try {
////            getDatabaseService().beginLocalTransaction();
//
//            for (RemindLetterHistory remindLetterHistory : remindLetterHistories) {
//                switch (remindLetterHistory.getLetterStatus()) {
//                    case "NO":
//                        remindLetterHistory.setLetterStatus("FIRST_LETTER");
//                        break;
//                    case "FIRST_LETTER":
//                        remindLetterHistory.setLetterStatus("FIRST_LETTER");
//                        break;
//                    case "SECOND_LETTER":
//                        remindLetterHistory.setLetterStatus("THIRD_LETTER");
//                        break;
//                }
//                getDatabaseService().save(remindLetterHistory);
//            }
//
////            getDatabaseService().commitLocalTransaction();
//        } catch (DatabaseException databaseException) {
//            Logger.getLogger(com.mac.zutil.remind_letter.SERLetterGenerator.class.getName()).log(Level.SEVERE, null, databaseException);
//        }
//    }
//    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//}
//
}
