package com.mac.zutil.remind_letter;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zutil.remind_letter.object.RemindLetterHistory;
import com.mac.zutil.remind_letter.object.RemindLetterSetup;
//import com.mac.zutil.remind_letter.object.RemindLetterSetup;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nimesh Duminda
 */
public class SERLetterGenerator extends AbstractService {

    public SERLetterGenerator(Component component) {
        super(component);
    }

    public List<RemindLetterHistory> getReminLetter(int min, int max) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = "select\n"
                + "                	settlement.loan as index_no,\n"
                + "                	settlement.loan as loan,\n"
                + "                	settlement.branch as branch,\n"
                + "                	(SELECT MAX(settlement_history.transaction_date)\n"
                + "FROM settlement_history Where\n"
                + "settlement_history.settlement=settlement.index_no \n"
                + "and settlement_history.`status`<>'CANCEL' \n"
                + "and settlement_history.transaction_type='RECEIPT' ) as transaction_date,\n"
                + "                	(\n"
                + "                	select sum(st1.balance_amount) \n"
                + "                	from settlement st1 left join settlement_type on settlement_type.code = st1.settlement_type\n"
                + "                	where \n"
                + "                		settlement_type.credit_or_debit = 'CREDIT' \n"
                + "                		and st1.loan = settlement.loan \n"
                + "                		and st1.due_date <  '" + fDate + "'\n"
                + "                		and settlement.status <> 'CANCEL'\n"
                + "                	) as loan_balance,\n"
                + "                	datediff('" + fDate + "' ,transaction_date) as remind_level,"
                + "  0 as volume  \n"
                + "                from\n"
                + "                	settlement   left join loan on settlement.loan=loan.index_no   \n"
                + "                where\n"
                + "                	settlement.due_date < '" + fDate + "' \n"
                + "                	and settlement.status = 'PENDING'\n"
                + "                     and settlement.branch = '" + branch + "'\n"
                + "                     and loan.loan_group IS NULL   \n"
                + "                group by\n"
                + "                	settlement.loan\n"
                + "                having \n"
                + "                loan_balance > 0.9 AND \n"
                + "                	 remind_level between  '" + min + "'  and  '" + max + "' "
                + "order by settlement.loan asc ;";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);



        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
        }

        return newFirstLetter;
    }

    //Badulla
    public List<RemindLetterHistory> getReminLetterByVolume(double min, double max) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = "select\n"
                + "        settlement.index_no,"
                + "        settlement.branch as branch,\n"
                + "        min(settlement.due_date) as transaction_date,\n"
                + "        settlement.loan as loan,\n"
                + "        (  select\n"
                + "            sum(stxx.balance_amount)   \n"
                + "        from\n"
                + "            settlement stxx \n"
                + "        left join\n"
                + "            settlement_type \n"
                + "                on settlement_type.code = stxx.settlement_type  \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'    \n"
                + "            and stxx.loan = settlement.loan    \n"
                + "            and stxx.due_date <= '" + fDate + "'  \n"
                + "            and (stxx.settlement_type = 'LOAN_CAPITAL' "
                + "OR stxx.settlement_type =' LOAN_INTEREST' "
                + "OR stxx.settlement_type ='OTHER_CHARGE' )  \n"
                + "            and stxx.status <> 'CANCEL'  ) as loan_balance,\n"
                + "        datediff( '" + fDate + "' ,\n"
                + "        settlement.due_date) as remind_level,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)            \n"
                + "        from\n"
                + "            settlement st1          \n"
                + "        left join\n"
                + "            settlement_type                  \n"
                + "                on settlement_type.code = st1.settlement_type           \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                 \n"
                + "            and st1.loan = settlement.loan                 \n"
                + "            and st1.due_date < '" + fDate + "'              \n"
//                + "            and st1.status <> 'CANCEL'  ) / loan.installment_amount as volume ,\n"
                + "            and st1.status <> 'CANCEL'  ) / loan.installment_amount as level ,\n"
                + "           '" + fDate + "'   as sent_date,\n"
                + "         'Letter' as letter_status,\n"
                + "        '0' as status  \n"
                + "    from\n"
                + "        settlement \n"
                + "    left join\n"
                + "        loan \n"
                + "            on settlement.loan=loan.index_no \n"
                + "    where\n"
                + "        settlement.due_date <= '" + fDate + "'  \n"
                + "        and (settlement.settlement_type = 'LOAN_CAPITAL' OR settlement.settlement_type =' LOAN_INTEREST' OR settlement.settlement_type ='OTHER_CHARGE' ) \n"
                + "        and loan.status = 'LOAN_START'      \n"
                + "        and settlement.status = 'PENDING'      \n"
                + "        and settlement.branch = '" + branch + "'  \n"
                + "    group by\n"
                + "        settlement.loan \n"
                + "    having\n"
                + "        level between " + min + " and " + max + "";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);



        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
        }

        return newFirstLetter;
    }
    public List<RemindLetterSetup> setlettertitle()
    {
        List<RemindLetterSetup> newFirstLetter = new ArrayList<>();
        try {
          newFirstLetter= getDatabaseService().getCollection("from com.mac.zutil.remind_letter.object.RemindLetterSetup ");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newFirstLetter;
    }

    public void saveRemindLetterHistory(List<RemindLetterHistory> remindLetterHistories) {
        try {

            for (RemindLetterHistory remindLetterHistory : remindLetterHistories) {
                getDatabaseService().save(remindLetterHistory);
            }

        } catch (DatabaseException databaseException) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, databaseException);
        }
    }
//    public void updateLoans(List<Loan> loans) {
//        try {
//            for (Loan loan : loans) {
//                getDatabaseService().update(loan);
//            }
//        } catch (DatabaseException databaseException) {
//            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, databaseException);
//        }
//    }
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
}
