/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zutil.system_settings;

import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.message.mOptionPane;
import com.mac.zutil.system_settings.object.Settings;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author NIMESH-PC
 */
public class SystemStatusCheck {

    public boolean isEnableThisSettings(HibernateDatabaseService databaseService, final String settingsCode) {
        try {
            Settings setting = (Settings) databaseService.initCriteria(Settings.class)
                    .add(Restrictions.eq("code", settingsCode)).uniqueResult();

            if (setting.getStatus().equals(SystemSettingsStatus.ACTIVE)) {
                return true;
            }
            return false;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public int getRegistrationTypeFromDB(HibernateDatabaseService databaseService, String classpath) {
        Settings editiableModules = (Settings) databaseService.initCriteria(Settings.class)
                .add(Restrictions.eq("name", classpath)).uniqueResult();

        if (editiableModules == null) {
            mOptionPane.showMessageDialog(null, "Please Contact System Admin", "Registration Type DB Error !!", mOptionPane.ERROR_MESSAGE);
            return 555;
        } else {
            switch (editiableModules.getStatus()) {
                case RegistrationsTypesStatus.ALL:
                    return 0;
                case RegistrationsTypesStatus.NEW:
                    return 1;
                case RegistrationsTypesStatus.EDIT:
                    return 2;
                default:
                    return 555;
            }

        }
    }
    
      public String checkFinacType(HibernateDatabaseService databaseService, final String settingsCode) {
//          System.out.println("dbbbbbbbb_"+databaseService);
        try {
            Settings setting = (Settings) databaseService.initCriteria(Settings.class)
                    .add(Restrictions.eq("code", settingsCode)).uniqueResult();

            if (setting.getStatus().equals(SystemSettingsStatus.HP)) {
                return SystemSettingsStatus.HP;
            }else if(setting.getStatus().equals(SystemSettingsStatus.LOAN)){
            return SystemSettingsStatus.LOAN;
            }else{
            return SystemSettingsStatus.LOAN;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return SystemSettingsStatus.LOAN;
        }

    }
      
      public String clientLoadForLoan(HibernateDatabaseService databaseService, final String settingsCode) {
     // System.out.println("dbbbbbbbb_"+databaseService);
        try {
            Settings setting = (Settings) databaseService.initCriteria(Settings.class)
                    .add(Restrictions.eq("code", settingsCode)).uniqueResult();

            if (setting.getStatus().equals(SystemSettingsStatus.STATUS_LOAN_APPICATION_CLIENT_NIC)) {
                return SystemSettingsStatus.STATUS_LOAN_APPICATION_CLIENT_NIC;
            }else if(setting.getStatus().equals(SystemSettingsStatus.STATUS_LOAN_APPICATION_CLIENT_NAME)){
            return SystemSettingsStatus.STATUS_LOAN_APPICATION_CLIENT_NAME;
            }else{
            return "NULL";
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "NULL";
        }

    }
    
}
