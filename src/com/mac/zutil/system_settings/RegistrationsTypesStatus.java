/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zutil.system_settings;

/**
 *
 * @author NIMESH-PC
 */
public class RegistrationsTypesStatus {
    // REGISTRATION TYPES FOR DB

//    new_and_edit_registration_type
    public static final String ALL = "ALL";
//    new_only_registration_type
    public static final String NEW = "NEW";
//    edit_only_registration_type
    public static final String EDIT = "EDIT";
    
    
    //Editable Modules Class Names List
    public static final String REGCompany = "com.mac.registration.company.REGCompany.class";
    public static final String REGBranch = "com.mac.registration.branch.REGBranch.class";
}
