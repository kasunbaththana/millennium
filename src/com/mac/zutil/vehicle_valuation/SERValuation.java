package com.mac.zutil.vehicle_valuation;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.LoanStatus;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanValuationAccountInterface;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.settlement.SystemSettlement;
import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zutil.vehicle_valuation.object.Loan;
import com.mac.zutil.vehicle_valuation.object.Vehicle;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Kasun
 */
public class SERValuation extends AbstractService {

    public SERValuation(Component component) {
        super(component);
    }

      public List getAgreementNo(int index) {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.zutil.vehicle_valuation.object.Loan where note='SEIZED' and  status<>'LOAN_START' and index_no="+index+" ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERValuation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public List getLoanVehicle() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.zutil.vehicle_valuation.object.LoanVehicle ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERValuation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
     public List getAgreementNo2() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.zutil.vehicle_valuation.object.Loan where note='SEIZED' and  status='SUSPEND'  ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERValuation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
     
     
     public boolean save(
             Loan loan,
             Vehicle vehicle,
             double loanamount,
             double capital,
             double interest,
             double charge,
             double totamount,
             double othercharge,
             double finaclamount
             
             )
     {
        
          try { 
//            getDatabaseService().beginLocalTransaction();
//            VehicleValuation valuation=new VehicleValuation();
//            valuation.setIndexNo(loan.getIndexNo());
//            valuation.setLoan(loan);
//            valuation.setVehicle(vehicle);
//            valuation.setLoanAmount(loanamount);
//            valuation.setCapital(capital);
//            valuation.setInterest(interest);
//            valuation.setCharge(charge);
//            valuation.setVamount(totamount);
//            valuation.setOthercharge(othercharge);
//            valuation.setFinalamount(finaclamount);
             
//            getDatabaseService().save(loan);
//            getDatabaseService().save(vehicle);
//            getDatabaseService().save(valuation);
//            
//            getDatabaseService().commitLocalTransaction();
              
              getDatabaseService().callUpdateProcedure
                      (
                      "call z_savevaluation ("
                      + ""+loan.getIndexNo()+","
                      + ""+loan.getIndexNo()+","
                      + ""+vehicle.getIndexNo()+","
                      + ""+loanamount+","
                      + ""+capital+","
                      + ""+interest+","
                      + ""+charge+","
                      + ""+totamount+","
                      + ""+othercharge+","
                      + ""+finaclamount+" "
                      + ")");
              
              
              loanCloase(loan, totamount,finaclamount,capital,interest,charge);
              
            return true;
            
        } catch (DatabaseException ex) {
            Logger.getLogger(SERValuation.class.getName()).log(Level.SEVERE, null, ex);
            
            
            return false;
        }
            
            
         
     }
     private void loanCloase(Loan loan,double totalBalance,double valuation,double capital_amount ,double interest_amount ,double other_amount)
     {
         try {
             int transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.LOAN_VALUATION_TRANSACTION_CODE,
                loan.getLoanReferenceNo(),
                loan.getLoanDocumentNo(),
                loan.getIndexNo(),
                null,
                loan.getClient(),
                "Loan Valuation Close");

        loan.setStatus(LoanStatus.COLLECTED);
        loan.setAvailableReceipt(false);
        loan.setAvailableVoucher(false);

        //settle settlements
        List<Settlement> settlements = getDatabaseService().initCriteria(Settlement.class)
                .add(Restrictions.eq("loan", loan.getIndexNo()))
                .add(Restrictions.eq("status", SettlementStatus.SETTLEMENT_PENDING))
                .list();

        double creditSum = 0.0;
        double debitSum = 0.0;
        double lost_or_profit = 0.0;
        double totalamount = 0.0;

        SystemSettlement systemSettlement = SystemSettlement.getInstance();
        systemSettlement.beginSettlementHistoryQueue();
        for (Settlement settlement : settlements) {
            if (settlement.getSettlementType().getCreditOrDebit().equals(SettlementCreditDebit.CREDIT)) {
                creditSum += settlement.getBalanceAmount();
                systemSettlement.addSettlementHistoryQueue(
                        settlement, 
                        SystemTransactions.LOAN_VALUATION_TRANSACTION_CODE, 
                        transaction, 
                        settlement.getBalanceAmount());
            }
            if (settlement.getSettlementType().getCreditOrDebit().equals(SettlementCreditDebit.DEBIT)) {
                debitSum += settlement.getBalanceAmount();
                systemSettlement.addSettlementHistoryQueue(
                        settlement, 
                        SystemTransactions.LOAN_VALUATION_TRANSACTION_CODE, 
                        transaction, 
                        settlement.getBalanceAmount());
            }
            // calculate interest capital other charge
            
//            if(settlement.getSettlementType().getCode().equals(SystemSettlement.LOAN_CAPITAL))
//            {
//                 capital += settlement.getBalanceAmount();
//            }
//            if(settlement.getSettlementType().getCode().equals(SystemSettlement.LOAN_INTEREST))
//            {
//                 interest += settlement.getBalanceAmount();
//            }
//            if(settlement.getSettlementType().getCode().equals(SystemSettlement.OTHER_CHARGE))
//            {
//                 othercharge += settlement.getBalanceAmount();
//            }
//            
            
            
        }
        totalamount = capital_amount + interest_amount + other_amount;
        systemSettlement.flushSettlementHistoryQueue(getDatabaseService());
        // first step accounts
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        if(totalBalance!=0)
        {
        accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.DEBIT_SEZED_CLOSE, "Loan sezed Close", totalamount, AccountTransactionType.AUTO);
        
        accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.DEBIT_SEZED_CLOSE_CAPITAL, "Loan sezed Close", capital_amount, AccountTransactionType.AUTO);
        
        accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.DEBIT_SEZED_CLOSE_INTEREST, "Loan sezed Close", interest_amount, AccountTransactionType.AUTO);
       
        accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.DEBIT_SEZED_CLOSE_OTHER_CHARGE, "Loan sezed Close", other_amount, AccountTransactionType.AUTO);

        }
        //accounts boss and debitor
        accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.CREDIT_SETTLEMENT_CREDIT_CODE, "Loan Valuation Close", totalBalance, AccountTransactionType.AUTO);
        accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.CREDIT_SETTLEMENT_DEBIT_CODE, "Loan Valuation Close", valuation, AccountTransactionType.AUTO);

        // frofit and lost 
        lost_or_profit = valuation - totalBalance ;
        lost_or_profit = Math.abs(lost_or_profit);
        if(totalBalance < valuation)
        {
            accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.CREDIT_SETTLEMENT_PROFIT_CREDIT, "Loan Valuation Close", lost_or_profit, AccountTransactionType.AUTO);
            accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.CREDIT_SETTLEMENT_LOSS_DEBIT, "Loan Valuation Close", 0.0, AccountTransactionType.AUTO);
        }
        else if(totalBalance > valuation)
        {
            accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.CREDIT_SETTLEMENT_PROFIT_CREDIT, "Loan Valuation Close", 0.0, AccountTransactionType.AUTO);
            accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.CREDIT_SETTLEMENT_LOSS_DEBIT, "Loan Valuation Close", lost_or_profit, AccountTransactionType.AUTO);
            
        }
        else
        {
           accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.CREDIT_SETTLEMENT_PROFIT_CREDIT, "Loan Valuation Close", 0.0, AccountTransactionType.AUTO);
           accountTransaction.addAccountTransactionQueue(LoanValuationAccountInterface.CREDIT_SETTLEMENT_LOSS_DEBIT, "Loan Valuation Close", 0.0, AccountTransactionType.AUTO);
              
            
        }
        
        accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.LOAN_VALUATION_TRANSACTION_CODE);
        
        getDatabaseService().save(loan);
        
         } catch (Exception e) {
             e.printStackTrace();
         }
  
     }
    
    }

