/*
 *  LetterGenerator.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 1, 2014, 2:07:07 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zutil.vehicle_valuation;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zutil.vehicle_valuation.object.Loan;
import com.mac.zutil.vehicle_valuation.object.LoanVehicle;
import com.mac.zutil.vehicle_valuation.object.Settlement;
import com.mac.zutil.vehicle_valuation.object.Vehicle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.List;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Kasun
 */
public class Valuation extends CPanel {
SERValuation SERValuation;
Vehicle lVehicle;
    /**
     * Creates new form LetterGenerator
     */
 
//  TableRowSorter<CTableModel> rowSorter;
    public Valuation() {
        initComponents();
//        this.rowSorter = new TableRowSorter<>(tblReminds.getCTableModel());
        initOthers();
       
    }
   
         


    @Action
    public void doClose() {
        
        TabFunctions.closeTab(this);
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        //SERVICE
        SERValuation = new SERValuation(this);

        //ACTIONS
        ActionUtil actionUtil = new ActionUtil(this);
//        actionUtil.setAction(btnGenerate, "doGenerate");
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnSave, "doSave");
   
        //
//        btnGenerate.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_GENERATE, 16, 16));
     
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
       
//        cboloanvehicle.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//               
//
//                if (cboloanvehicle.getCValue() != null ) {
//                   getLoanDetails();
//                   
//                }
//            }
//        });
        
        cboAgreementNo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               

                if (cboAgreementNo.getCValue() != null ) {
                   getloan();
                    txtadd_charge.setCValue(00.0);
                }
            }
        });
        
         FocusAdapter focusAdapter = new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                try {
                    getloan();
                    addOthercharge();
                } catch (Exception ex) {
                }
            }
        };
        
       cboAgreementNo.setExpressEditable(true);
//       cboloanvehicle.setExpressEditable(true);
       txtadd_charge.addFocusListener(focusAdapter);
       
       txtadd_charge.setCValue(00.0);
        idleMode();
        
        

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnClose1 = new com.mac.af.component.derived.command.button.CCButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        pnlLeft = new javax.swing.JPanel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cboAgreementNo = new com.mac.af.component.derived.input.combobox.CIComboBox()
        {
            @Override
            public List getComboData(){
                return SERValuation.getAgreementNo2();
            }
        }

        ;
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtcapital = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        txtinterest = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        txtcharge = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtadd_charge = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        txtfinal_amount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel17 = new com.mac.af.component.derived.display.label.CDLabel();
        txtvehicle = new com.mac.af.component.derived.input.textfield.CIStringField();
        pnlRight = new javax.swing.JPanel();

        btnClose1.setText("Close");

        jSplitPane1.setDividerLocation(300);

        cDLabel2.setText("Agreement No :");

        btnClose.setText("Close");

        btnSave.setText("Save");

        cDLabel10.setText("Vehicle No :");

        cDLabel11.setText("Loan Amount :");

        cDLabel12.setText("Capital :");

        cDLabel13.setText("Interest :");

        cDLabel14.setText("Charge :");

        cDLabel15.setText("valuation Amount:");

        cDLabel16.setText("Add Charge:");

        cDLabel17.setText("Final Amount:");

        javax.swing.GroupLayout pnlLeftLayout = new javax.swing.GroupLayout(pnlLeft);
        pnlLeft.setLayout(pnlLeftLayout);
        pnlLeftLayout.setHorizontalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(cDLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cDLabel16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cDLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtfinal_amount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtadd_charge, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)))
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cDLabel14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cDLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cDLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtcharge, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtinterest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtcapital, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtLoanAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(txtvehicle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnlLeftLayout.setVerticalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtvehicle, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcapital, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtinterest, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcharge, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtadd_charge, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtfinal_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );

        jSplitPane1.setLeftComponent(pnlLeft);

        pnlRight.setLayout(new javax.swing.BoxLayout(pnlRight, javax.swing.BoxLayout.LINE_AXIS));
        jSplitPane1.setRightComponent(pnlRight);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnClose1;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel17;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAgreementNo;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlRight;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtLoanAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtadd_charge;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtcapital;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtcharge;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtfinal_amount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtinterest;
    private com.mac.af.component.derived.input.textfield.CIStringField txtvehicle;
    // End of variables declaration//GEN-END:variables
  
    private void idleMode()
    {
        cboAgreementNo.setEnabled(true);
//        cboloanvehicle.setEnabled(true);
        txtvehicle.setEditable(false);
//        txtAmount.setEditable(false);
        txtLoanAmount.setEditable(false);
        txtcapital.setEditable(false);
        txtcharge.setEditable(false);
        txtinterest.setEditable(false);
        
    }
    private void clear()
    {
        cboAgreementNo.setCValue(null);
//        cboloanvehicle.setEnabled(true);
        txtvehicle.setCValue("");
        txtAmount.setCValue(00.0);
        txtLoanAmount.setCValue(00.0);
        txtcapital.setCValue(00.0);
        txtcharge.setCValue(00.0);
        txtinterest.setCValue(00.0);
        txtadd_charge.setCValue(00.0);
        txtfinal_amount.setCValue(00.0);
    }
    
    
    private void addOthercharge()
    {
        
     
        txtfinal_amount.setCValue(txtAmount.getCValue()+txtadd_charge.getCValue());
        
    }
    
private void getLoanDetails()
{
    try {

//        LoanVehicle vehicle=(LoanVehicle) cboloanvehicle.getCValue();
       
        
        
         
    } catch (Exception e) {
        e.printStackTrace();
    }
    
}
private void getloan()
{
    double capital=0.0,interest=0.0,othercharge=0.0,valueamount=0.0;
    try {
         Loan loancbo =(Loan) cboAgreementNo.getCValue();
         
            List<com.mac.zsystem.transaction.settlement.object.Settlement> settlements = getDatabaseService().initCriteria(com.mac.zsystem.transaction.settlement.object.Settlement.class)
                .add(Restrictions.eq("loan", loancbo.getIndexNo()))
                .add(Restrictions.eq("status", SettlementStatus.SETTLEMENT_PENDING))
                .list();
        
        txtLoanAmount.setCValue(loancbo.getLoanAmount());
//        txtagrement.setCValue(loancbo.getAgreementNo());
        for(com.mac.zsystem.transaction.settlement.object.Settlement settlement: settlements)
        {
            System.out.println("LOAN_"+settlement.getSettlementType().getCode());
            if(settlement.getSettlementType().getCode().equals("LOAN_CAPITAL"))
            {
           capital += settlement.getBalanceAmount();
            }
            else if(settlement.getSettlementType().getCode().equals("LOAN_INTEREST"))
            {
            interest += settlement.getBalanceAmount();    
            }
            else if(settlement.getSettlementType().getCode().equals("OTHER_CHARGE"))//OTHER_CHARGE
            {
            othercharge += settlement.getBalanceAmount();    
            }
            
        }
        for(LoanVehicle vehicle :loancbo.getLoanVehicles() )
        {
            
           txtvehicle.setCValue(vehicle.getVehicleIndex().getVehicleNo());
           lVehicle = vehicle.getVehicleIndex();
        }
        
        
        valueamount = capital + interest + othercharge;
       txtcapital.setCValue(capital);
       txtinterest.setCValue(interest);
       txtcharge.setCValue(othercharge);
       txtAmount.setCValue(valueamount);
       txtfinal_amount.setCValue(valueamount);
     
        
    } catch (Exception e) {
        e.printStackTrace();
    }
    
    
}

@Action
    public void doSave() {
    if(txtvehicle.getCValue().isEmpty())
    {
        mOptionPane.showMessageDialog(this, "Action Faild Please Assign Vehicle !", "WARNING_MESSAGE", mOptionPane.WARNING_MESSAGE);
    }
    else
    {
    boolean issave =   SERValuation.save(
              (Loan) cboAgreementNo.getCValue(),
               lVehicle,
               txtLoanAmount.getCValue(),
               txtcapital.getCValue(),
               txtinterest.getCValue(),
               txtcharge.getCValue(),
               txtAmount.getCValue(),
               txtadd_charge.getCValue(),
               txtfinal_amount.getCValue()
               );
    
    if(issave==true)
    {
        mOptionPane.showMessageDialog(this, "Save Success !", "Save", mOptionPane.INFORMATION_MESSAGE);
    clear();
    }
    else
    {
         mOptionPane.showMessageDialog(this, "Save Faild !", "Faild", mOptionPane.ERROR_MESSAGE);
    }
    }
}
   
   
     
    
}
