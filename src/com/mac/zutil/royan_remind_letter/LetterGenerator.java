/*
 *  LetterGenerator.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 1, 2014, 2:07:07 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zutil.royan_remind_letter;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.registration.remind_letter_setup.object.RemindLetterSetup;
import com.mac.zreport.ReportBuilder;
import com.mac.zutil.royan_remind_letter.object.RemindLetterHistory;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.swing.JRViewer;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Nimesh
 */
public class LetterGenerator extends CPanel {
SERLetterGenerator SERLetterGenerator;
    /**
     * Creates new form LetterGenerator
     */
 
//  TableRowSorter<CTableModel> rowSorter;
    public LetterGenerator() {
        initComponents();
//        this.rowSorter = new TableRowSorter<>(tblReminds.getCTableModel());
        initOthers();
        
//         tblReminds.setRowSorter(rowSorter);
         
         
//          txtTableFind.getDocument().addDocumentListener(new DocumentListener(){
//
//            @Override
//            public void insertUpdate(DocumentEvent e) {
//                String text = txtTableFind.getText();
//
//                if (text.trim().length() == 0) {
//                    rowSorter.setRowFilter(null);
//                } else {
//                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
//                }
//            }
//
//            @Override
//            public void removeUpdate(DocumentEvent e) {
//                String text = txtTableFind.getText();
//
//                if (text.trim().length() == 0) {
//                    rowSorter.setRowFilter(null);
//                } else {
//                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
//                }
//            }
//
//            @Override
//            public void changedUpdate(DocumentEvent e) {
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//            }
//
//        });
    }

    @Action(asynchronous = true)
    public void doGenerate() {
        int level = cboRemindLetterLevel.getSelectedIndex() + 1;

        String path1 = "";
        String letterStatus = "";
        File destFile = null;
        if (level == 1) {
            path1 = "C:/REMIND_LETTERS/remind_letter_1_customer.jrxml";
            letterStatus = RYNLetterStatus.R_FIRST_LETTER;
        } else if (level == 2) {
            path1 = "C:/REMIND_LETTERS/remind_letter_2_customer.jrxml";
            letterStatus = RYNLetterStatus.R_SECOND_LETTER;
        } else if (level == 3) {
            path1 = "C:/REMIND_LETTERS/remind_letter_end_finance.jrxml";
            letterStatus = RYNLetterStatus.R_FINANCE_END;
        } else if (level == 4) {
            path1 = "C:/REMIND_LETTERS/remind_letter_Lease.jrxml";
            letterStatus = RYNLetterStatus.R_VEHICAL_SEASE;
        }
         else if (level == 5) {
            path1 = "C:/REMIND_LETTERS/remind_letter_Deletion.jrxml";
            letterStatus = RYNLetterStatus.R_DELETION_LETTER;
        }
         else if (level == 6) {
            path1 = "C:/REMIND_LETTERS/remind_letter_leagal_action.jrxml";
            letterStatus = RYNLetterStatus.R_LEAGAL_ACTION;
        }

        try {
            JasperDesign jasperDesign1 = JRXmlLoader.load(path1);
            JasperReport jasperReport1 = JasperCompileManager.compileReport(jasperDesign1);

            String FileName ;
            HashMap<String, Object> params = new HashMap<>();
            ReportBuilder.initDefaultParameters();
            params.putAll(ReportBuilder.DEFAULT_PARAMETERS);
            List<RemindLetterHistory> remindLetterHistorys = new ArrayList<>();
            StringBuilder stringBuilder = new StringBuilder();
            RemindLetterHistory letterHistory;
            
            for (Object object : tblReminds.getCValue()) {
                letterHistory = (RemindLetterHistory) object;

                if (letterHistory.isSelected()) {
                    stringBuilder.append(letterHistory.getLoan().getIndexNo()).append(",");
                    Date sDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);

                    params.put("LOAN_INDEX_SET", letterHistory.getLoan().getIndexNo().toString());
                    params.put("H_DATE", CApplication.getSessionVariable(CApplication.WORKING_DATE));
                    params.put("H_BRANCH", CApplication.getSessionVariable(CApplication.STORE_ID));
                   // final JasperPrint jasperPrint1 = JasperFillManager.fillReport(jasperReport1, params, getConnection());
                    //get Client Name
                    String client_name = "";
                    ResultSet rs = getConnection()
                            .createStatement()
                            .executeQuery("SELECT name FROM client WHERE code='" + letterHistory.getLoan().getClient() + "'");
                    if (rs.next()) {
                        client_name = rs.getString("name");
                    }

                    //Create File Location and name
                     FileName =
                            CApplication.getSessionVariable(CApplication.WORKING_DATE)
                            + "__" + letterHistory.getLoan().getAgreementNo().toString().replaceAll("/", "_")
                            + "__" + client_name;

                     destFile = new File("C:/REMIND_LETTERS/" + cboRemindLetterLevel.getSelectedItem().toString()
                            .replaceAll(" ", "_") + "/" + FileName + ".docx");

                    System.out.println("destFile"+destFile);
                    //Export To Word Document
                    
//                    JRDocxExporter exporter1 = new JRDocxExporter();
//                   
//                    exporter1.setExporterInput(new SimpleExporterInput(jasperPrint1));
//                    exporter1.setExporterOutput(new SimpleOutputStreamExporterOutput(destFile));
//                    exporter1.exportReport();

                    //save remind
                    RemindLetterHistory reminLetter = new RemindLetterHistory();

                    reminLetter.setBranch(CApplication.getSessionVariable(CApplication.STORE_ID).toString());
                    reminLetter.setTransactionDate(sDate);
                    reminLetter.setLoan(letterHistory.getLoan());
                    reminLetter.setLoanBalance(letterHistory.getLoanBalance());
                    reminLetter.setLevel(letterHistory.getLevel());
                    reminLetter.setLetterStatus(letterStatus);
                    reminLetter.setletterSend(true);
                    reminLetter.setStatus("ACTIVE");

                    remindLetterHistorys.add(reminLetter);
                    
                    //save 
                    
                }
            }
            if (stringBuilder.length() != 0) {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }

            if (level == 1 || level == 2 || level == 3 || level == 4 || level == 5 || level == 6) {
                letterGenerator.saveRemindLetterHistory(remindLetterHistorys);
            }

            // print Report
            params.put("LOAN_INDEX_SET", stringBuilder.toString());
            params.put("H_SYSTEM_DATE", CApplication.getSessionVariable(CApplication.WORKING_DATE));
            params.put("H_BRANCH", CApplication.getSessionVariable(CApplication.STORE_ID));

            System.out.println(stringBuilder.toString());

            final JasperPrint jasperPrint1 = JasperFillManager.fillReport(jasperReport1, params, getConnection());
                    Exporter exporter = new JRDocxExporter();
                    exporter.setExporterInput(new SimpleExporterInput(jasperPrint1));
                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(destFile));

                    exporter.exportReport();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    JRViewer viewer1 = new JRViewer(jasperPrint1);
                    pnlRight.removeAll();
                    pnlRight.add(viewer1);
                }
            };

            CApplication.invokeEventDispatch(runnable);

        } catch (Exception e) {
            mOptionPane.showMessageDialog(null, "Unable to generate remind letters.", "Error", mOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        tblReminds.resetValue();
        setRemindLevel();
    }
     
                 
         

    private void setRemindLevel() {
        int level = cboRemindLetterLevel.getSelectedIndex() + 1;
//       tblReminds.removeAll();
//       tblReminds.resetValue();
        List<RemindLetterHistory> letterHistorys;
        RemindLetterSetup  letter1=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_1")).uniqueResult();
         RemindLetterSetup  letter2=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_2")).uniqueResult();
         RemindLetterSetup  letter6=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_6")).uniqueResult();

        switch (level) {
            case 0:
            case 1:
                letterHistorys = letterGenerator.getReminLetterByVolume(letter1.getMinLevel(), letter1.getMaxLevel(),RYNLetterStatus.R_FIRST_LETTER);
                break;
            case 2:
                letterHistorys = letterGenerator.getReminLetterByVolume(letter2.getMinLevel(), letter2.getMaxLevel(),RYNLetterStatus.R_SECOND_LETTER);
                break;
            case 3:
                letterHistorys = letterGenerator.getReminLetterByEndLoan(RYNLetterStatus.R_FINANCE_END);
                break;
            case 4:
                letterHistorys = letterGenerator.getReminLetterForSease(RYNLetterStatus.R_VEHICAL_SEASE);
                break;
            case 5:
                letterHistorys = letterGenerator.getReminLetterForDeletion(RYNLetterStatus.R_DELETION_LETTER);
                break;
            case 6:
                letterHistorys = letterGenerator.getReminLetterByVolume(letter6.getMinLevel(), letter6.getMaxLevel(),RYNLetterStatus.R_LEAGAL_ACTION);
                break;
            default:
                throw new AssertionError();
                
        }
        tblReminds.setCValue(letterHistorys);
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();

        return connection;
    }

    @Action
    public void doClose() {
        tblReminds.setCValue(null);
        TabFunctions.closeTab(this);
    }
    
    

    @SuppressWarnings("unchecked")
    private void initOthers() {
        //SERVICE
        letterGenerator = new SERLetterGenerator(this);

        //Create Folder If not Exist
        createFolders();

        //ACTIONS
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnGenerate, "doGenerate");
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnFind, "doFind");
   
        //
        btnGenerate.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_GENERATE, 16, 16));
     
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        btnFind.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON));
        CTableModel tableModel = new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Selected", new String[]{"selected"}, true),
            new CTableColumn("Agreement No", "loan", "agreementNo"),
            new CTableColumn("loan", "loan", "indexNo"),
            new CTableColumn("Balance", "loanBalance"),
            //            new CTableColumn("Volume", "remindLevel"),
            new CTableColumn("Volume", "level")
        });
        
        tblReminds.setCModel(tableModel);
 
        setCombo_data();

        setRemindLevel();
          
        cboRemindLetterLevel.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setRemindLevel();
                txtTableFind.setCValue("");
            }
        });

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        pnlLeft = new javax.swing.JPanel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnGenerate = new com.mac.af.component.derived.command.button.CCButton();
        cboRemindLetterLevel = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReminds = new com.mac.af.component.derived.input.table.CITable();
        txtTableFind = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnFind = new com.mac.af.component.derived.command.button.CCButton();
        pnlRight = new javax.swing.JPanel();

        jSplitPane1.setDividerLocation(300);

        cDLabel2.setText("Remind Letter Level :");

        btnClose.setText("Close");

        btnGenerate.setText("Generate");
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        cboRemindLetterLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "FIRST_REMIND_CUSTOMER", "SECOND_REMIND_CUSTOMER", "AGREEMENT_EXPIRATION", "VEHICAL_SEASE_REMIND", "DELETION_LETTER", "LEGAL ACTION" }));
        cboRemindLetterLevel.setToolTipText("");
        cboRemindLetterLevel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboRemindLetterLevelActionPerformed(evt);
            }
        });

        tblReminds.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblReminds);

        btnFind.setText("Find");

        javax.swing.GroupLayout pnlLeftLayout = new javax.swing.GroupLayout(pnlLeft);
        pnlLeft.setLayout(pnlLeftLayout);
        pnlLeftLayout.setHorizontalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboRemindLetterLevel, 0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                        .addComponent(txtTableFind, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlLeftLayout.setVerticalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboRemindLetterLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTableFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(pnlLeft);

        pnlRight.setLayout(new javax.swing.BoxLayout(pnlRight, javax.swing.BoxLayout.LINE_AXIS));
        jSplitPane1.setRightComponent(pnlRight);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cboRemindLetterLevelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboRemindLetterLevelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboRemindLetterLevelActionPerformed

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGenerateActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnFind;
    private com.mac.af.component.derived.command.button.CCButton btnGenerate;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    public javax.swing.JComboBox cboRemindLetterLevel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlRight;
    private com.mac.af.component.derived.input.table.CITable tblReminds;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTableFind;
    // End of variables declaration//GEN-END:variables
    private SERLetterGenerator letterGenerator;

    private void setCombo_data()
    {
//        List<com.mac.zutil.remind_letter.object.RemindLetterSetup> ls =letterGenerator.setlettertitle();
//        for(com.mac.zutil.remind_letter.object.RemindLetterSetup remindLetterSetup:ls)
//        {
//         cboRemindLetterLevel.addItem(remindLetterSetup.getName().toString());
//        
//        }
        
//         List<com.mac.zutil.remind_letter.object.RemindLetterSetup> ls =letterGenerator.setlettertitle();
//        
//          cboRemindLetterLevel.setModel(new DefaultComboBoxModel(ls.toArray()));
//        
//        
        
       
    }
    private void createFolders() {
        File remindLetterFolder = new File("D:/REMIND_LETTERS");
      

        File remind1 = new File("C:/REMIND_LETTERS/FIRST_REMIND_CUSTOMER");
        File remind2 = new File("C:/REMIND_LETTERS/SECOND_REMIND_CUSTOMER");
        
        File remind3 = new File("C:/REMIND_LETTERS/THIRED_REMIND_CUSTOMER");
        File remind4 = new File("C:/REMIND_LETTERS/VEHICAL_SEASE_REMIND");
        File remind5 = new File("C:/REMIND_LETTERS/DELETION_LETTER");
        File remind6 = new File("C:/REMIND_LETTERS/LEGAL_ACTION");
        if (!remindLetterFolder.exists()) {
            remindLetterFolder.mkdir();
        }
        if (!remind1.exists()) {
            remind1.mkdir();
        }
        if (!remind2.exists()) {
            remind2.mkdir();
        }
        if (!remind3.exists()) {
            remind3.mkdir();
        }
        if (!remind4.exists()) {
            remind4.mkdir();
        }
       
        if (!remind5.exists()) {
            remind5.mkdir();
        }
        if (!remind6.exists()) {
            remind6.mkdir();
        }
        
    }
     @Action
    public void doFind() throws DatabaseException, Exception {
         
         CTableModel tablemodel= tblReminds.getCTableModel();
//           tblReminds.find(txtTableFind.getCValue());
         tablemodel.removeAll();
         setdata_find();
    }
     
     
     public void setdata_find() throws Exception
     {
         int level = cboRemindLetterLevel.getSelectedIndex() + 1;
//       tblReminds.removeAll();
       tblReminds.resetValue();
        List<RemindLetterHistory> letterHistorys;
          RemindLetterSetup  letter1=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_1")).uniqueResult();
         RemindLetterSetup  letter2=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_2")).uniqueResult();
             RemindLetterSetup  letter6=   (RemindLetterSetup) getDatabaseService().initCriteria(RemindLetterSetup.class)
                   .add(Restrictions.eq("code", "letter_6")).uniqueResult();

        switch (level) {
            case 1:
                letterHistorys = letterGenerator.getReminLetterByVolume_Agreement(letter1.getMinLevel(), letter1.getMaxLevel(),txtTableFind.getCValue());
            case 2:
                letterHistorys = letterGenerator.getReminLetterByVolume_Agreement(letter2.getMinLevel(), letter2.getMaxLevel(),txtTableFind.getCValue());
                break;
            case 3:
                letterHistorys = letterGenerator.getReminLetterByEndLoan(RYNLetterStatus.R_FINANCE_END);
            case 4:
                letterHistorys = letterGenerator.getReminLetterForSease(RYNLetterStatus.R_VEHICAL_SEASE);
                break;
            case 5:
                letterHistorys = letterGenerator.getReminLetterForDeletion(RYNLetterStatus.R_DELETION_LETTER);
                break;
            case 6:
                letterHistorys = letterGenerator.getReminLetterByVolume_Agreement(letter6.getMinLevel(), letter6.getMaxLevel(),txtTableFind.getCValue());
                break;
           
            
            default:
                throw new AssertionError();
        }
        tblReminds.setCValue(letterHistorys);
     }
}
