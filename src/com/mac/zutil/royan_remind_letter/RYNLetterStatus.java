/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zutil.royan_remind_letter;

/**
 *
 * @author NIMESH-PC
 */
public class RYNLetterStatus {
    public static final String LETTER_NO="NO";
    
    public static final String R_FIRST_LETTER="FIRST_LETTER";
    public static final String R_SECOND_LETTER="SECOND_LETTER";
    public static final String R_VEHICAL_SEASE="VEHICAL_SEASE";
    public static final String R_SETTLEMENT_BOARD="SETTLEMENT_BOARD";
    public static final String R_FINANCE_END="FINANCE_END";
    public static final String R_DELETION_LETTER="DELETION_LETTER";
    public static final String R_LEAGAL_ACTION="LEAGAL_ACTION";
    
    public static final String PENDING_STATUS="0";
    public static final String DEACTIVATE_STATUS="1";
    
}
