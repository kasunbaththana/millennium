package com.mac.zutil.royan_remind_letter;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zutil.remind_letter.object.Loan;

//import com.mac.zutil.remind_letter.object.RemindLetterSetup;
import com.mac.zutil.royan_remind_letter.object.RemindLetterHistory;
import com.mac.zutil.remind_letter.object.RemindLetterSetup;

import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Nimesh Duminda
 */
public class SERLetterGenerator extends AbstractService {

    public SERLetterGenerator(Component component) {
        super(component);
    }

    public List<RemindLetterHistory> getReminLetter(int min, int max) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);

        String FirstSql = "select\n"
                + "                	settlement.loan as index_no,\n"
                + "                	settlement.loan as loan,\n"
                + "                	settlement.branch as branch,\n"
                + "                	(SELECT MAX(settlement_history.transaction_date)\n"
                + "FROM settlement_history Where\n"
                + "settlement_history.settlement=settlement.index_no \n"
                + "and settlement_history.`status`<>'CANCEL' \n"
                + "and settlement_history.transaction_type='RECEIPT' ) as transaction_date,\n"
                + "                	(\n"
                + "                	select sum(st1.balance_amount) \n"
                + "                	from settlement st1 left join settlement_type on settlement_type.code = st1.settlement_type\n"
                + "                	where \n"
                + "                		settlement_type.credit_or_debit = 'CREDIT' \n"
                + "                		and st1.loan = settlement.loan \n"
                + "                		and st1.due_date <  '" + fDate + "'\n"
                + "                		and settlement.status <> 'CANCEL'\n"
                + "                	) as loan_balance,\n"
                + "                	datediff('" + fDate + "' ,transaction_date) as remind_level,"
                + "  0 as volume  \n"
                + "                from\n"
                + "                	settlement   left join loan on settlement.loan=loan.index_no   \n"
                + "                where\n"
                + "                	settlement.due_date < '" + fDate + "' \n"
                + "                	and settlement.status = 'PENDING'\n"
              //  + "                     and settlement.branch = '" + branch + "'\n"
                + "                     and loan.loan_group IS NULL   \n"
                + "                group by\n"
                + "                	settlement.loan\n"
                + "                having \n"
                + "                loan_balance > 0.9 AND \n"
                + "                	 remind_level between  '" + min + "'  and  '" + max + "' "
                + "order by settlement.loan asc ;";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);
        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);

        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
        }
        return newFirstLetter;
    }

    public List<RemindLetterHistory> getReminLetterByVolume(double min, double max, String report) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = "select\n"
                + "        settlement.index_no,"
                + "        settlement.branch as branch,\n"
                + "        min(settlement.due_date) as transaction_date,\n"
                + "        settlement.loan as loan,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)   \n"
                + "        from\n"
                + "            settlement st1 \n"
                + "        left join\n"
                + "            settlement_type \n"
                + "                on settlement_type.code = st1.settlement_type  \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'    \n"
                + "            and st1.loan = settlement.loan    \n"
                + "            and st1.due_date < '" + fDate + "'  \n"
                + "            and st1.status <> 'CANCEL'  ) as loan_balance,\n"
//                + "        datediff( '" + fDate + "' ,\n"
//                + "        settlement.due_date) as remind_level,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)            \n"
                + "        from\n"
                + "            settlement st1          \n"
                + "        left join\n"
                + "            settlement_type                  \n"
                + "                on settlement_type.code = st1.settlement_type           \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                 \n"
                + "            and st1.loan = settlement.loan                 \n"
                + "            and st1.due_date < '" + fDate + "'              \n"
                + "            and st1.status <> 'CANCEL'  ) / loan.installment_amount as level ,\n"
                + "           '" + fDate + "'   as sent_date,\n"
                + "         'Letter' as letter_status,\n"
                + "        'ACTIVE' as status,  \n"
                + "        false as letter_send  \n"
                + "    from\n"
                + "        settlement \n"
                + "    left join\n"
                + "        loan \n"
                + "            on settlement.loan=loan.index_no \n"
                + "    where\n"
                + "        settlement.due_date < '" + fDate + "'  \n"
                + "        and (settlement.settlement_type = 'LOAN_CAPITAL' OR settlement.settlement_type =' LOAN_INTEREST' OR settlement.settlement_type ='OTHER_CHARGE')  \n"
                + "        and settlement.status = 'PENDING'      \n"
                + "        and loan.`status` ='LOAN_START'  \n"
                + "         AND loan.index_no NOT IN(SELECT h.loan FROM `remind_letter_history` h"
                + " WHERE h.status<>'PAY' and letter_send=1 and h.`letter_status`='"+report+"') "
                + "    group by\n"
                + "        settlement.loan \n"
                + "    having\n"
                + "        level between " + min + " and " + max + "";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery ;



        try {
             getDatabaseService().beginLocalTransaction(); 
        hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);
          
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);
            getDatabaseService().commitLocalTransaction();

        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
        }

        return newFirstLetter;
    }

    public List<RemindLetterHistory> getReminLetterByEndLoan(String report) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = " select\n"
                + "        settlement.index_no,\n"
                + "        settlement.branch as branch,\n"
                + "       ( SELECT DATE_ADD(lnx.loan_date, INTERVAL loan.installment_count-1 MONTH)\n"
                + "		 FROM \n"
                + "		 loan lnx where lnx.index_no=loan.index_no\n"
                + "		  )as transaction_date,     \n"
                + "        settlement.loan as loan,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)            \n"
                + "        from\n"
                + "            settlement st1          \n"
                + "        left join\n"
                + "            settlement_type                  \n"
                + "                on settlement_type.code = st1.settlement_type           \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                 \n"
                + "            and st1.loan = settlement.loan                 \n"
                + "            and st1.due_date < '" + fDate + "'              \n"
                + "            and st1.status <> 'CANCEL'  ) as loan_balance,\n"
                + "        datediff( '" + fDate + "' ,\n"
                + "        settlement.due_date) as remind_level,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)                     \n"
                + "        from\n"
                + "            settlement st1                   \n"
                + "        left join\n"
                + "            settlement_type                                   \n"
                + "                on settlement_type.code = st1.settlement_type                    \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                              \n"
                + "            and st1.loan = settlement.loan                              \n"
                + "            and st1.due_date < '" + fDate + "'                           \n"
                + "            and st1.status <> 'CANCEL'  ) / loan.installment_amount as level ,\n"
                + "        '" + fDate + "'   as sent_date,\n"
                + "        loan.`status` as letter_status,\n"
                
                + "        'ACTIVE' as status,  \n"
                + "        false as letter_send  \n"
                + "    from\n"
                + "        settlement      \n"
                + "    left join\n"
                + "        loan              \n"
                + "            on settlement.loan=loan.index_no      \n"
                + "    where\n"
                + "        settlement.due_date < '" + fDate + "'           \n"
                + "        and (\n"
                + "            settlement.settlement_type = 'LOAN_CAPITAL' \n"
                + "            OR settlement.settlement_type =' LOAN_INTEREST'\n"
                
                + "       OR settlement.settlement_type ='OTHER_CHARGE'  ) \n"
                + "                  \n"
                + "        and settlement.status = 'PENDING'               \n"
                + "         AND loan.index_no NOT IN(SELECT h.loan FROM `remind_letter_history` h "
                + "WHERE   letter_send=1 and h.`letter_status`='"+report+"' ) "
             //   + "        and settlement.branch = '" + branch + "'       \n"
                 + "        AND loan.`status` ='LOAN_START'  \n"
                + "    group by\n"
                + "        settlement.loan      \n"
                + "    having\n"
                + "       transaction_date < '" + fDate + "'  AND loan_balance > 1  \n"
                + "    AND  loan.`status` ='LOAN_START'      \n"
                + "       ";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);



        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
        }

        return newFirstLetter;
    }
    public List<RemindLetterHistory> getReminLettergetLoan() {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = " select\n"
                + "        settlement.index_no,\n"
                + "        settlement.branch as branch,\n"
                + "       ( SELECT DATE_ADD(lnx.loan_date, INTERVAL loan.installment_count-1 MONTH)\n"
                + "		 FROM \n"
                + "		 loan lnx where lnx.index_no=loan.index_no\n"
                + "		  )as transaction_date,     \n"
                + "        settlement.loan as loan,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)            \n"
                + "        from\n"
                + "            settlement st1          \n"
                + "        left join\n"
                + "            settlement_type                  \n"
                + "                on settlement_type.code = st1.settlement_type           \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                 \n"
                + "            and st1.loan = settlement.loan                 \n"
                + "            and st1.due_date < '" + fDate + "'              \n"
                + "            and st1.status <> 'CANCEL'  ) as loan_balance,\n"
                + "        datediff( '" + fDate + "' ,\n"
                + "        settlement.due_date) as remind_level,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)                     \n"
                + "        from\n"
                + "            settlement st1                   \n"
                + "        left join\n"
                + "            settlement_type                                   \n"
                + "                on settlement_type.code = st1.settlement_type                    \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                              \n"
                + "            and st1.loan = settlement.loan                              \n"
                + "            and st1.due_date < '" + fDate + "'                           \n"
                + "            and st1.status <> 'CANCEL'  ) / loan.installment_amount as level ,\n"
                + "        '" + fDate + "'   as sent_date,\n"
                + "        'Letter' as letter_status,\n"
                + "        '0' as status       \n"
                + "    from\n"
                + "        settlement      \n"
                + "    left join\n"
                + "        loan              \n"
                + "            on settlement.loan=loan.index_no      \n"
                + "    where\n"
                + "        settlement.due_date < '" + fDate + "'           \n"
                + "        and (\n"
                + "            settlement.settlement_type = 'LOAN_CAPITAL' \n"
                + "            OR settlement.settlement_type =' LOAN_INTEREST'\n"
                + "        ) \n"
                + "        OR settlement.settlement_type ='OTHER_CHARGE'           \n"
                + "        and settlement.status = 'PENDING'               \n"
             //   + "        and settlement.branch = '" + branch + "'       \n"
                + "    group by\n"
                + "        settlement.loan      \n"
                + "    having\n"
                + "       transaction_date < '" + fDate + "'  AND loan_balance > 1 \n"
                + "       ";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);



        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
        }

        return newFirstLetter;
    }
//   public List<RemindLetterSetup> setlettertitle()
//    {
//        List<RemindLetterSetup> newFirstLetter = new ArrayList<>();
//        try {
//          newFirstLetter= getDatabaseService().getCollection("from com.mac.zutil.remind_letter.object.RemindLetterSetup ");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return newFirstLetter;
//    }
   public List<RemindLetterSetup> setlettertitle() {
        List<RemindLetterSetup> loan = new ArrayList<>();
        try {
            String hql = "FROM com.mac.zutil.remind_letter.object.RemindLetterSetup";
            loan = getDatabaseService().getCollection(hql);
                    
                    } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loan = new ArrayList();
        }
        return loan;
    }
  public List<Loan> getloan(String loan)
    {
        
        List<Loan> newFirstLetter = new ArrayList<>();
        try {
          newFirstLetter= getDatabaseService().getCollection("from com.mac.zutil.remind_letter.object.Loan where agreementNo='"+loan+"'");
        
          
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newFirstLetter;
    }
    public List<RemindLetterHistory> getReminLetterForSease(String report) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = 
        "SELECT\n" +
"                        settlement.index_no,\n" +
"                        settlement.branch AS branch,\n" +
"                        MIN(settlement.due_date) AS transaction_date,     \n" +
"                        settlement.loan AS loan,\n" +
"                        (  SELECT\n" +
"                            SUM(st1.balance_amount)            \n" +
"                        FROM\n" +
"                            settlement st1          \n" +
"                        LEFT JOIN\n" +
"                            settlement_type                  \n" +
"                                ON settlement_type.code = st1.settlement_type           \n" +
"                        WHERE\n" +
"                            settlement_type.credit_or_debit = 'CREDIT'                 \n" +
"                            AND st1.loan = settlement.loan                 \n" +
"                            AND st1.due_date < '" + fDate + "'               \n" +
"                            AND st1.status <> 'CANCEL'  ) AS loan_balance,\n" +
"                            \n" +
"                        DATEDIFF( '" + fDate + "' ,   settlement.due_date) AS remind_level,\n" +
"                        \n" +
"                        0 AS LEVEL ,\n" +
"                            \n" +
"                       '" + fDate + "'    AS sent_date,\n" +
"                        remind_letter_history.letter_status AS letter_status,\n" +
  "        'ACTIVE' as status,  \n"+
   "        false as letter_send  \n"+
"                    FROM\n" +
"                        settlement      \n" +
"                    LEFT JOIN remind_letter_history ON settlement.loan=remind_letter_history.loan  \n" +
"                	 LEFT JOIN `loan` l ON    settlement.loan = l.`index_no`  \n" +
"                    WHERE\n" +
"                    	l.`status`='SUSPEND' AND\n" +
"                    	l.`note`='SEIZED' AND \n" +
"                        settlement.due_date < '" + fDate + "'           \n" +
"                        AND (\n" +
"                            settlement.settlement_type = 'LOAN_CAPITAL' \n" +
"                            OR settlement.settlement_type =' LOAN_INTEREST'\n" +
"                         \n" +
"                        OR settlement.settlement_type ='OTHER_CHARGE' )           \n" +
"                        AND settlement.status = 'PENDING'               \n" +
               "         AND loan.index_no NOT IN(SELECT h.loan FROM `remind_letter_history` h "
                + "WHERE h.status<>'PAY' and letter_send=1 and h.`letter_status`='"+report+"' ) "+
//"                        AND settlement.branch = '" + branch + "'        \n" +
"                    GROUP BY\n" +
"                        settlement.loan   ";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);



        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
        }

        return newFirstLetter;
    }
    public List<RemindLetterHistory> getReminLetterForDeletion(String report) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = 
        " SELECT \n" +
"                        settlement.index_no,\n" +
"                        settlement.branch AS branch,\n" +
"                        MIN(settlement.due_date) AS transaction_date,     \n" +
"                        settlement.loan AS loan,\n" +
"                        0 AS loan_balance,\n" +
"                            \n" +
"                        0 AS remind_level,\n" +
"                        \n" +
"                        0 AS LEVEL ,\n" +
"                            \n" +
"                       '"+fDate+"'    AS sent_date,\n" +
"                        remind_letter_history.letter_status AS letter_status,\n" +
 "        'ACTIVE' as status,  \n"+
   "        false as letter_send  \n"+
"                    FROM\n" +
"                        settlement      \n" +
"                    LEFT JOIN remind_letter_history ON settlement.loan=remind_letter_history.loan  \n" +
"                	 LEFT JOIN `loan` l ON    settlement.loan = l.`index_no`  \n" +
"                    WHERE\n" +
"                    	l.`status`='COLLECTED' \n" +
"                        AND l.index_no NOT IN(SELECT h.loan FROM `remind_letter_history` h \n" +
"                    	WHERE h.`letter_send`=1 AND h.`letter_status`='"+report+"' )    \n" +
"                    GROUP BY\n" +
"                        settlement.loan  ";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);



        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
        }

        return newFirstLetter;
    }
    
    public List<RemindLetterHistory> getReminLetterForSettlementBoard() {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = " select\n"
                + "        settlement.index_no,\n"
                + "        settlement.branch as branch,\n"
                + "        min(settlement.due_date) as transaction_date,     \n"
                + "        settlement.loan as loan,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)            \n"
                + "        from\n"
                + "            settlement st1          \n"
                + "        left join\n"
                + "            settlement_type                  \n"
                + "                on settlement_type.code = st1.settlement_type           \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                 \n"
                + "            and st1.loan = settlement.loan                 \n"
                + "            and st1.due_date < '" + fDate + "'               \n"
                + "            and st1.status <> 'CANCEL'  ) as loan_balance,\n"
                + "            \n"
                + "        datediff( '" + fDate + "' ,   settlement.due_date) as remind_level,\n"
                + "        \n"
                + "        0 as level ,\n"
                + "            \n"
                + "        '" + fDate + "'   as sent_date,\n"
                + "        remind_letter_history.letter_status as letter_status,\n"
                + "        '0' as status       \n"
                + "    from\n"
                + "        settlement      \n"
                + "    right join remind_letter_history on settlement.loan=remind_letter_history.loan  \n"
                + "	     \n"
                + "    where\n"
                + "    	remind_letter_history.letter_status='VEHICAL_SEASE' AND\n"
                + "        settlement.due_date < '" + fDate + "'           \n"
                + "        and (\n"
                + "            settlement.settlement_type = 'LOAN_CAPITAL' \n"
                + "            OR settlement.settlement_type =' LOAN_INTEREST'\n"
                + "         \n"
                + "        OR settlement.settlement_type ='OTHER_CHARGE' )          \n"
                + "        and settlement.status = 'PENDING'               \n"
            //    + "        and settlement.branch = '" + branch + "'       \n"
                + "    group by\n"
                + "        settlement.loan      \n"
                + "    having\n"
                + "       transaction_date < '" + fDate + "'  AND loan_balance>1\n"
                + "       ";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);



        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
        }

        return newFirstLetter;
    }

    public void saveRemindLetterHistory(List<RemindLetterHistory> remindLetterHistories) {
        try {

            for (RemindLetterHistory remindLetterHistory : remindLetterHistories) {
                getDatabaseService().save(remindLetterHistory);
            }

        } catch (DatabaseException databaseException) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, databaseException);
        }
    }
//    public void updateLoans(List<Loan> loans) {
//        try {
//            for (Loan loan : loans) {
//                getDatabaseService().update(loan);
//            }
//        } catch (DatabaseException databaseException) {
//            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, databaseException);
//        }
//    }
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      public List<RemindLetterHistory> getReminLetterByVolume_Agreement(double min, double max,String aGreement) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = "select\n"
                + "        settlement.index_no,"
                + "        settlement.branch as branch,\n"
                + "        min(settlement.due_date) as transaction_date,\n"
                + "        settlement.loan as loan,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)   \n"
                + "        from\n"
                + "            settlement st1 \n"
                + "        left join\n"
                + "            settlement_type \n"
                + "                on settlement_type.code = st1.settlement_type  \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'    \n"
                + "            and st1.loan = settlement.loan    \n"
                + "            and st1.due_date < '" + fDate + "'  \n"
                + "            and st1.status <> 'CANCEL'  ) as loan_balance,\n"
//                + "        datediff( '" + fDate + "' ,\n"
//                + "        settlement.due_date) as remind_level,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)            \n"
                + "        from\n"
                + "            settlement st1          \n"
                + "        left join\n"
                + "            settlement_type                  \n"
                + "                on settlement_type.code = st1.settlement_type           \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                 \n"
                + "            and st1.loan = settlement.loan                 \n"
                + "            and st1.due_date < '" + fDate + "'              \n"
                //                + "            and st1.status <> 'CANCEL'  ) / loan.installment_amount as volume ,\n"
                + "            and st1.status <> 'CANCEL'  ) / loan.installment_amount as level ,\n"
                + "           '" + fDate + "'   as sent_date,\n"
                + "         'Letter' as letter_status,\n"
                + "        '0' as status  \n"
                + "    from\n"
                + "        settlement \n"
                + "    left join\n"
                + "        loan \n"
                + "            on settlement.loan=loan.index_no \n"
                + "    where\n"
                + "        settlement.due_date < '" + fDate + "'  \n"
                + "        and (settlement.settlement_type = 'LOAN_CAPITAL' OR settlement.settlement_type =' LOAN_INTEREST' OR settlement.settlement_type ='OTHER_CHARGE')  \n"
                + "        and settlement.status = 'PENDING'      \n"
              //  + "        and settlement.branch = '" + branch + "'  \n"
                + "        and loan.`status` ='LOAN_START' \n"
                + "        AND loan.`agreement_no` LIKE '"+aGreement+"%'     \n"
                + "    group by\n"
                + "        settlement.loan \n"
                + "    having\n"
                + "        level between " + min + " and " + max + "";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery ;



        try {
             getDatabaseService().beginLocalTransaction(); 
        hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);
          
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);
            getDatabaseService().commitLocalTransaction();

        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
             JOptionPane.showMessageDialog(null, "Agreement no not Found !");
        }

        return newFirstLetter;
    }
      public List<RemindLetterHistory> getReminLetterForSease_Agreement(String aGreement) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = 
        "SELECT\n" +
"                        settlement.index_no,\n" +
"                        settlement.branch AS branch,\n" +
"                        MIN(settlement.due_date) AS transaction_date,     \n" +
"                        settlement.loan AS loan,\n" +
"                        (  SELECT\n" +
"                            SUM(st1.balance_amount)            \n" +
"                        FROM\n" +
"                            settlement st1          \n" +
"                        LEFT JOIN\n" +
"                            settlement_type                  \n" +
"                                ON settlement_type.code = st1.settlement_type           \n" +
"                        WHERE\n" +
"                            settlement_type.credit_or_debit = 'CREDIT'                 \n" +
"                            AND st1.loan = settlement.loan                 \n" +
"                            AND st1.due_date < '" + fDate + "'               \n" +
"                            AND st1.status <> 'CANCEL'  ) AS loan_balance,\n" +
"                            \n" +
"                        DATEDIFF( '" + fDate + "' ,   settlement.due_date) AS remind_level,\n" +
"                        \n" +
"                        0 AS LEVEL ,\n" +
"                            \n" +
"                       '" + fDate + "'    AS sent_date,\n" +
"                        remind_letter_history.letter_status AS letter_status,\n" +
"                        '0' AS STATUS       \n" +
"                    FROM\n" +
"                        settlement      \n" +
"                    LEFT JOIN remind_letter_history ON settlement.loan=remind_letter_history.loan  \n" +
"                	 LEFT JOIN `loan` l ON    settlement.loan = l.`index_no`  \n" +
"                    WHERE\n" +
"                    	l.`status`='SUSPEND' AND\n" +
"                    	l.`note`='SEIZED' AND \n" +
"                        settlement.due_date < '" + fDate + "'           \n" +
"                        AND (\n" +
"                            settlement.settlement_type = 'LOAN_CAPITAL' \n" +
"                            OR settlement.settlement_type =' LOAN_INTEREST'\n" +
"                         \n" +
"                        OR settlement.settlement_type ='OTHER_CHARGE' )           \n" +
"                        AND settlement.status = 'PENDING'               \n" +
//"                        AND settlement.branch = '" + branch + "'        \n "+
"                        AND l.`agreement_no` LIKE '"+aGreement+"%'    \n" +
"                    GROUP BY\n" +
"                        settlement.loan   ";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);



        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
             JOptionPane.showMessageDialog(null, "Agreement no not Found !");
        }

        return newFirstLetter;
    }
      public List<RemindLetterHistory> getReminLetterByEndLoan_Agreement(String aGreement) {
        Date transactionDate = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
        String branch = (String) CApplication.getSessionVariable(CApplication.STORE_ID);

        String fDate = dateFormat.format(transactionDate);


        String FirstSql = " select\n"
                + "        settlement.index_no,\n"
                + "        settlement.branch as branch,\n"
                + "       ( SELECT DATE_ADD(lnx.loan_date, INTERVAL loan.installment_count-1 MONTH)\n"
                + "		 FROM \n"
                + "		 loan lnx where lnx.index_no=loan.index_no\n"
                + "		  )as transaction_date,     \n"
                + "        settlement.loan as loan,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)            \n"
                + "        from\n"
                + "            settlement st1          \n"
                + "        left join\n"
                + "            settlement_type                  \n"
                + "                on settlement_type.code = st1.settlement_type           \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                 \n"
                + "            and st1.loan = settlement.loan                 \n"
                + "            and st1.due_date < '" + fDate + "'              \n"
                + "            and st1.status <> 'CANCEL'  ) as loan_balance,\n"
                + "        datediff( '" + fDate + "' ,\n"
                + "        settlement.due_date) as remind_level,\n"
                + "        (  select\n"
                + "            sum(st1.balance_amount)                     \n"
                + "        from\n"
                + "            settlement st1                   \n"
                + "        left join\n"
                + "            settlement_type                                   \n"
                + "                on settlement_type.code = st1.settlement_type                    \n"
                + "        where\n"
                + "            settlement_type.credit_or_debit = 'CREDIT'                              \n"
                + "            and st1.loan = settlement.loan                              \n"
                + "            and st1.due_date < '" + fDate + "'                           \n"
                + "            and st1.status <> 'CANCEL'  ) / loan.installment_amount as level ,\n"
                + "        '" + fDate + "'   as sent_date,\n"
                + "        loan.`status` as letter_status,\n"
                + "         loan.`agreement_no` as status       \n"
                + "    from\n"
                + "        settlement      \n"
                + "    left join\n"
                + "        loan              \n"
                + "            on settlement.loan=loan.index_no      \n"
                + "    where\n"
                + "        settlement.due_date < '" + fDate + "'           \n"
                + "        and (\n"
                + "            settlement.settlement_type = 'LOAN_CAPITAL' \n"
                + "            OR settlement.settlement_type =' LOAN_INTEREST'\n"
                + "        ) \n"
                + "        OR settlement.settlement_type ='OTHER_CHARGE'           \n"
                + "        and settlement.status = 'PENDING'               \n"
                + "    group by\n"
                + "        settlement.loan      \n"
                + "    having\n"
                + "       transaction_date < '" + fDate + "'  AND loan_balance > 100 \n"
                + "    AND  loan.`status` ='LOAN_START'      \n"
                + "    AND loan.`agreement_no` LIKE '"+aGreement+"%'    \n" 
                + "       ";


        System.out.println("FirstSql_____" + FirstSql);

        List<RemindLetterHistory> newFirstLetter = new ArrayList<>();
        HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(FirstSql, RemindLetterHistory.class);



        try {
            newFirstLetter = getDatabaseService().getCollection(hibernateSQLQuery, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(SERLetterGenerator.class.getName()).log(Level.SEVERE, null, ex);
            newFirstLetter = new ArrayList<>();
            JOptionPane.showMessageDialog(null, "Agreement no not Found !");
        }

        return newFirstLetter;
    }
}
