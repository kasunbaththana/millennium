/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.micro_finance.center;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public  class REGCenter extends AbstractRegistrationForm {

   
   
    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCCenter();
    }

    @Override
    public Class getObjectClass() {
       
        return com.mac.micro_finance.center.object.LoanCenter.class;
    }

    @Override
    public CTableModel getTableModel() {
       
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("City", new String[]{"city"}),
            new CTableColumn("Location", new String[]{"location"}),
            new CTableColumn("Start", new String[]{"startTime"}),
            new CTableColumn("End", new String[]{"endTime"}),
        });
    }
    
}
