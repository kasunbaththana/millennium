/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.item.sales_item;

import com.mac.item.sales_item.default_chargers.DefChargers;
import com.mac.item.zobject.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class SalesInvoiceCustomObj {

    private SalesInvoice salesInvoice = new SalesInvoice();
    private SalseInvoiceCharges salseInvoiceCharges;
    private ChargeScheme chargeScheme;
    private List<DefChargers> chargersWithAmounts = new ArrayList<>();

    public List<DefChargers> getChargersWithAmounts() {
        return chargersWithAmounts;
    }

    public void setChargersWithAmounts(List<DefChargers> chargersWithAmounts) {
        this.chargersWithAmounts = chargersWithAmounts;
    }

    public SalesInvoice getSalesInvoice() {
        return salesInvoice;
    }

    public void setSalesInvoice(SalesInvoice salesInvoice) {
        this.salesInvoice = salesInvoice;
    }

    public SalseInvoiceCharges getSalseInvoiceCharges() {
        return salseInvoiceCharges;
    }

    public void setSalseInvoiceCharges(SalseInvoiceCharges salseInvoiceCharges) {
        this.salseInvoiceCharges = salseInvoiceCharges;
    }

    public ChargeScheme getChargeScheme() {
        return chargeScheme;
    }

    public void setChargeScheme(ChargeScheme chargeScheme) {
        this.chargeScheme = chargeScheme;
    }
}
