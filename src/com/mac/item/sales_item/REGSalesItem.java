/*
 *  REGSalesItem.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 5, 2014, 12:18:46 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.item.sales_item;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.item.SalesInvoiceStatus;
import com.mac.item.sales_item.default_chargers.DefChargers;
import com.mac.item.zobject.SalesInvoice;
import com.mac.item.zobject.SalesItem;
import com.mac.item.zobject.SalseInvoiceCharges;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SalesInvoiceAccountInterface;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mohan
 */
public class REGSalesItem extends AbstractRegistrationForm<SalesInvoiceCustomObj> {

    private RecentButton recentButton;

    public REGSalesItem() {
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.SALES_ITEM_TRANSACTION_CODE);
        this.recentButton.setDatabseService(getDatabaseService());
        initCashierSession();
    }

    private void initCashierSession() {
        try {
            SystemCashier.getCurrentCashierSession(getDatabaseService());
        } catch (ApplicationException ex) {
            Logger.getLogger(REGSalesItem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public AbstractObjectCreator<SalesInvoiceCustomObj> getObjectCreator() {
        return new PCSalesInvoice() {
            @Override
            protected CPanel getCPanel() {
                return REGSalesItem.this;
            }
        };
    }

    @Override
    public Class<? extends SalesInvoiceCustomObj> getObjectClass() {
        return SalesInvoiceCustomObj.class;
    }

    @Override
    public CTableModel<SalesInvoiceCustomObj> getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Reference No.", new String[]{"salesInvoice", "referenceNo"}),
                new CTableColumn("Document No.", new String[]{"salesInvoice", "documentNo"}),
                new CTableColumn("Date", new String[]{"salesInvoice", "transactionDate"}),
                new CTableColumn("Sales Value", new String[]{"salesInvoice", "salesValue"}),
                new CTableColumn("Down Payment", new String[]{"salesInvoice", "downPayment"}),
                new CTableColumn("Credit Amount", new String[]{"salesInvoice", "creditAmount"}));
    }

    @Override
    protected int save(SalesInvoiceCustomObj salesInvoiceCustom) throws DatabaseException {
        //payment
        SystemPayment systemPayment = SystemPayment.getInstance(SystemTransactions.SALES_ITEM_TRANSACTION_CODE);
        boolean isPaymentOk = systemPayment.showPaymentDialog(getDatabaseService(), salesInvoiceCustom.getSalesInvoice().getReferenceNo(), salesInvoiceCustom.getSalesInvoice().getDocumentNo(), salesInvoiceCustom.getSalesInvoice().getTransactionDate(), salesInvoiceCustom.getSalesInvoice().getDownPayment(), ChequeType.CLIENT);


        if (isPaymentOk) {
            try {
                //start cashier session
                CashierSession cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());

                SalesInvoice salesInvoice = salesInvoiceCustom.getSalesInvoice();

                String referneceNo = salesInvoice.getReferenceNo();
                String documentNo = salesInvoice.getDocumentNo();
                String client = salesInvoice.getClient().getCode();
                
               

                //insert transaction
                HibernateDatabaseService databaseService = getDatabaseService().createChildDatabaseService();
                int transaction = SystemTransactions.insertTransaction(
                        databaseService,
                        SystemTransactions.SALES_ITEM_TRANSACTION_CODE,
                        referneceNo,
                        documentNo,
                        null,//LOAN
                        cashierSession.getIndexNo(),//CASHIER SESSION
                        client,
                        TOOL_TIP_TEXT_KEY);


                //modifications of sales invoice
                salesInvoiceCustom.getSalesInvoice().setTransaction(transaction);
                salesInvoiceCustom.getSalesInvoice().setLoanCreated(false);
                salesInvoiceCustom.getSalesInvoice().setStatus(SalesInvoiceStatus.ACTIVE);

                //modify sales items
                for (SalesItem salesItem : salesInvoiceCustom.getSalesInvoice().getSalesItems()) {
                    salesItem.setTransaction(transaction);
                    salesItem.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                    salesItem.setSalesInvoice(salesInvoice);
                }

                 //SALES INVOICE
                getDatabaseService().save(salesInvoiceCustom.getSalesInvoice());

                //PAYMENT
                systemPayment.savePayment(getDatabaseService(), cashierSession, transaction, salesInvoiceCustom.getSalesInvoice().getClient().getCode());

                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
                accountTransaction.beginAccountTransactionQueue();

                //sales total amount
                accountTransaction.addAccountTransactionQueue(SalesInvoiceAccountInterface.SALES_AMOUNT_CREDIT, "Sales Amount - " + salesInvoiceCustom.getSalesInvoice().getReferenceNo(), salesInvoiceCustom.getSalesInvoice().getSalesValue(), AccountTransactionType.AUTO);
                accountTransaction.addAccountTransactionQueue(SalesInvoiceAccountInterface.SALES_AMOUNT_DEBIT, "Sales Amount - " + salesInvoiceCustom.getSalesInvoice().getReferenceNo(), salesInvoiceCustom.getSalesInvoice().getSalesValue(), AccountTransactionType.AUTO);
                //down payment
                accountTransaction.addAccountTransactionQueue(SalesInvoiceAccountInterface.DOWN_AMOUNT_DEBIT, "Down Payment - " + salesInvoiceCustom.getSalesInvoice().getReferenceNo(), salesInvoiceCustom.getSalesInvoice().getDownPayment(), AccountTransactionType.AUTO);
                //charges
                accountTransaction.addAccountTransactionQueue(SalesInvoiceAccountInterface.CHARGES_AMOUNT_DEBIT, "Default Charg - " + salesInvoiceCustom.getSalesInvoice().getReferenceNo(), salesInvoiceCustom.getSalesInvoice().getDefaultChargers(), AccountTransactionType.AUTO);

                //save accounts
                accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction, SystemTransactions.SALES_ITEM_TRANSACTION_CODE);

                List<DefChargers> chargersWithAmounts = salesInvoiceCustom.getChargersWithAmounts();
                for (DefChargers defChargers : chargersWithAmounts) {
                    accountTransaction.saveAccountTransaction(
                            getDatabaseService(),
                            transaction,
                            SystemTransactions.SALES_ITEM_TRANSACTION_CODE,
                            AccountTransactionType.AUTO,
                            AccountSettingCreditOrDebit.CREDIT,
                            defChargers.getAmount(),
                            defChargers.getChargeScheme().getAccount(),
                            "Default Charge-" + salesInvoiceCustom.getSalesInvoice().getReferenceNo(),0);
                }


                List<DefChargers> chargersWithAmounts1 = salesInvoiceCustom.getChargersWithAmounts();
                for (DefChargers defChargers : chargersWithAmounts1) {
                    SalseInvoiceCharges salseInvoiceCharges = new SalseInvoiceCharges();
                    salseInvoiceCharges.setChargeScheme(defChargers.getChargeScheme());
                    salseInvoiceCharges.setValue(defChargers.getAmount());
                    salseInvoiceCharges.setSalesInvoice(salesInvoice);
                    getDatabaseService().save(salseInvoiceCharges);
                }
                
                

                //report
                Map<String, Object> params = new HashMap<>();
                params.put("TRANSACTION_NO", transaction);
                TransactionUtil.PrintReport(getDatabaseService(), SystemTransactions.SALES_ITEM_TRANSACTION_CODE, params);

                return SAVE_SUCCESS;
            } catch (ApplicationException ex) {
                Logger.getLogger(REGSalesItem.class.getName()).log(Level.SEVERE, null, ex);
                return SAVE_FAILED;
            }
        } else {
            return SAVE_FAILED;
        }
    }

    @Override
    protected List getTableData() throws DatabaseException {

        List<SalesInvoice> list = getDatabaseService().initCriteria(SalesInvoice.class)
                .add(Restrictions.eq("status", SalesInvoiceStatus.ACTIVE))
                .add(Restrictions.eq("loanCreated", false))
                .list();
        List<SalesInvoiceCustomObj> invoiceCustomObjectList = new ArrayList<>();
        for (SalesInvoice salesInvoice : list) {
            SalesInvoiceCustomObj invoiceCustomObject = new SalesInvoiceCustomObj();
            invoiceCustomObject.setSalesInvoice(salesInvoice);
            invoiceCustomObjectList.add(invoiceCustomObject);
        }

        return invoiceCustomObjectList;
    }

    @Override
    @Action
    public void doPrint() {
        recentButton.doClick();
    }

    @Override
    @Action
    public void doDelete() {
        try {
            int q = mOptionPane.showConfirmDialog(null, "Do you sure want to delete?", "Delete", mOptionPane.YES_NO_OPTION);

            if (q == mOptionPane.YES_OPTION) {
                SalesInvoiceCustomObj customObj = pojoCreator.getValue();
                SalesInvoice invoice = customObj.getSalesInvoice();
                invoice.setStatus(SalesInvoiceStatus.CANCEL);
                getDatabaseService().save(invoice);
                getDatabaseService().save(invoice);

                refreshTable();

                mOptionPane.showMessageDialog(null, "Successfully deleted !!!", "Delete", mOptionPane.INFORMATION_MESSAGE);
            }
        } catch (ObjectCreatorException | DatabaseException ex) {
            Logger.getLogger(REGSalesItem.class.getName()).log(Level.SEVERE, null, ex);
            mOptionPane.showMessageDialog(null, "Failed to delete !!!", "Delete", mOptionPane.ERROR_MESSAGE);
        }
    }
}
