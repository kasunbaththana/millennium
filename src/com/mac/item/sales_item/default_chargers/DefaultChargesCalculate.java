/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.item.sales_item.default_chargers;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.registration.charge_scheme.ChargeSchemeType;
import com.mac.item.zobject.ChargeScheme;
import com.mac.item.zobject.ChargeSchemeCustom;

/**
 *
 * +
 *
 * @author NIMESH-PC
 */
public class DefaultChargesCalculate {

    public double getCustomAmount(HibernateDatabaseService databaseService, double SalseValue, ChargeScheme chargeSchemes) throws DatabaseException {
        double amount = 0;
        switch (chargeSchemes.getType()) {
            case ChargeSchemeType.VALUE:
                amount = chargeSchemes.getAmount();
                break;
            case ChargeSchemeType.PERCENT:
                amount = SalseValue * (chargeSchemes.getAmount() / 100);
                break;
            case ChargeSchemeType.CUSTOM:
                for (ChargeSchemeCustom chargeSchemeCustom : chargeSchemes.getChargeSchemeCustoms()) {
                    if (chargeSchemeCustom.getFromAmount() <= SalseValue && SalseValue <= chargeSchemeCustom.getToAmount()) {
                        switch (chargeSchemeCustom.getType()) {
                            case ChargeSchemeType.CUSTOM_AMOUNT:
                                amount = chargeSchemeCustom.getAmount();
                                break;
                            case ChargeSchemeType.CUSTOM_RATE:
                                amount = SalseValue * (chargeSchemeCustom.getAmount() / 100);
                                break;
                            default:
                                throw new AssertionError();
                        }


                    }
                }
                break;
            default:
                throw new AssertionError();
        }
        
        return amount;
    }
}
