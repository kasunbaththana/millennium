/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.item.sales_item.default_chargers;

//import com.mac.item.sales_item.default_chargers.object.ChargeScheme;
import com.mac.item.zobject.ChargeScheme;


/**
 *
 * @author NIMESH-PC
 */
public class DefChargers {
    
    private ChargeScheme chargeScheme;
    private double amount;

    public ChargeScheme getChargeScheme() {
        return chargeScheme;
    }

    public void setChargeScheme(ChargeScheme chargeScheme) {
        this.chargeScheme = chargeScheme;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    
    
}
