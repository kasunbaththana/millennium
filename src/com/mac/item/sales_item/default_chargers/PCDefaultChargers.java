/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.item.sales_item.default_chargers;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.item.sales_item.PCSalesInvoice;
import com.mac.item.zobject.ChargeScheme;
import com.mac.registration.charge_scheme.REGChargeScheme;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NIMESH-PC
 */
public abstract class PCDefaultChargers extends DefaultObjectCreator<DefChargers> {

    /**
     * Creates new form DefaultChargers
     */
  
    public PCDefaultChargers() {
        initComponents();
        initOthers();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        cmboDefaultCharg.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                try {
                    txtAmount.setCValue(new DefaultChargesCalculate().getCustomAmount(getCPanel().getDatabaseService(), SalesAmount.getSalseAmount, (ChargeScheme) cmboDefaultCharg.getCValue()));
                } catch (DatabaseException ex) {
                    Logger.getLogger(PCDefaultChargers.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
        
        cmboDefaultCharg.setTableModel(new CTableModel(
                new CTableColumn("Code", "code"),
                new CTableColumn("Name", "names"),
                new CTableColumn("Type", "type")
                ));
        
        cmboDefaultCharg.setRegistration("New Charg Schemes", REGChargeScheme.class);
        
        
    }

    private List<ChargeScheme> getChrgesScheme() {
        try {
            return getCPanel().getDatabaseService().getCollection(ChargeScheme.class);
        } catch (DatabaseException ex) {
            Logger.getLogger(PCSalesInvoice.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cmboDefaultCharg = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getChrgesScheme();
            }
        };
        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();

        cDLabel1.setText("Default Chargers :");

        cDLabel2.setText("Amount :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmboDefaultCharg, javax.swing.GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE)
                    .addComponent(txtAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmboDefaultCharg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.input.combobox.CIComboBox cmboDefaultCharg;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAmount;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getEssentialComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getOtherFieldComponents() {
        return Arrays.asList();
    }

    @Override
    protected List getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(cmboDefaultCharg, "chargeScheme"),
                new CInputComponentBinder(txtAmount, "amount"));
    }

    @Override
    protected Class<? extends DefChargers> getObjectClass() {
        return DefChargers.class;
    }
}
