/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.item.sales_item;

import com.mac.item.sales_item.default_chargers.PCDefaultChargers;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.item.sales_item.default_chargers.DefChargers;
import com.mac.item.sales_item.default_chargers.SalesAmount;
import com.mac.item.zobject.Client;
import com.mac.item.zobject.SalesItem;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author thilanga
 */
public class PCSalesInvoice extends DefaultObjectCreator<SalesInvoiceCustomObj> {

    private PCSalesItem pcSalesItem;
    private PCDefaultChargers pcdefaultChargers;
 

    /**
     * Creates new form PCSalesInvoice
     */
    public PCSalesInvoice() {
        initComponents();
        initOthers();
    }

    public List<Client> getClients() {
        try {
            return getCPanel().getDatabaseService().getCollection(Client.class);
        } catch (DatabaseException ex) {
            Logger.getLogger(PCSalesInvoice.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    private void resetSalesValues() {
        Collection<SalesItem> salesItems = tblAddingTable.getCValue();

        Double salesValue = 0.0;
        Double downPayment = txtDownPayment.getCValue();
        Double totalAmount = txtTotalAmount.getCValue();
        Double creditAmount=0.0;

        for (SalesItem salesItem : salesItems) {
            salesValue += salesItem.getSalesPrice();
        }

        creditAmount = totalAmount - downPayment;

        txtSalesValue.setCValue(salesValue);
        txtCreditAmount.setCValue(creditAmount);
        resetAll();

        SalesAmount.getSalseAmount = salesValue;
    }
    private void xxx() {
        Collection<SalesItem> salesItems = tblAddingTable.getCValue();

        Double salesValue = 0.0;
        Double downPayment = txtDownPayment.getCValue();
        Double totalAmount = txtTotalAmount.getCValue();
        Double creditAmount=0.0;

        for (SalesItem salesItem : salesItems) {
            salesValue += salesItem.getSalesPrice();
        }

        creditAmount = totalAmount - downPayment;

        txtSalesValue.setCValue(salesValue);
        txtCreditAmount.setCValue(creditAmount);
//        resetAll();

        SalesAmount.getSalseAmount = salesValue;
    }

    private void resertDefaultChargers() {
        Collection<DefChargers> defaultChrg = tblDefaultChargers.getCValue();

        double defaultAmount = 0.0;

        for (DefChargers chargeScheme : defaultChrg) {
            defaultAmount += chargeScheme.getAmount();
        }
        txtDefaultChargers.setCValue(defaultAmount);
        txtTotalAmount.setCValue(defaultAmount);
        resetAll();
    }

    private void resetAll() {
        Double salesAmount = txtSalesValue.getCValue();
        Double DefaultAmout = txtDefaultChargers.getCValue();
        Double sum = salesAmount + DefaultAmout;
        txtTotalAmount.setCValue(sum);
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {

          

        pcSalesItem = new PCSalesItem() {
            @Override
            protected CPanel getCPanel() {
                return PCSalesInvoice.this.getCPanel();
            }
        };
        pcdefaultChargers = new PCDefaultChargers() {
            @Override
            protected CPanel getCPanel() {
                return PCSalesInvoice.this.getCPanel();
            }
        };

        tblAddingTable.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Item", "item", "name"),
            new CTableColumn("Serial No. 1", "serialNo1"),
            new CTableColumn("Serial No. 2", "serialNo2"),
            new CTableColumn("Sales Price", "salesPrice")
        }));

        tblAddingTable.setCValue(new ArrayList());
        tblAddingTable.setObjectCreator(pcSalesItem);
        tblAddingTable.setDescrption("Add Item");
        txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.SALES_INVOICE));

        tblAddingTable.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                resetSalesValues();
                resertDefaultChargers();
                xxx();
            }
        });

        //DefaultChargers Table BEGIN
        tblDefaultChargers.setCModel(new CTableModel(new CTableColumn[]{
            new CTableColumn("Code", new String[]{"ChargeScheme", "code"}),
            new CTableColumn("Name", new String[]{"ChargeScheme", "name"}),
            new CTableColumn("Amont", "amount")
        }));

        tblDefaultChargers.setObjectCreator(pcdefaultChargers);
        tblDefaultChargers.setDescrption("Default Chargers");
        tblDefaultChargers.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                resertDefaultChargers();
            }
        });
        

        //DefaultChargers Table END
        txtDownPayment.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetSalesValues();
            }
        });
        
//          cboClient.addItemListener(new ItemListener() {
//            @Override
//            public void itemStateChanged(ItemEvent e) {
//                Client client = (Client) cboClient.getCValue();
//                if (client != null && client.getStatus() != null) {
//                    if (client.getStatus().equals(ClientStatus.BLACK_LIST)) {
//                        mOptionPane.showMessageDialog(cboClient, "<HTML> <FONT COLOR='green'> ( " + client.getName() + " ) </FONT> Client is <FONT COLOR='red'>  BLACKLISTED ,  </FONT> From The System !.</HTML>", "Blacklist Message", mOptionPane.INFORMATION_MESSAGE);
//                        cboClient.setCValue(null);
//                    }
//                }
//            }
//        });
        
        
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        tblAddingTable = new com.mac.af.component.derived.input.table.CIAddingTable();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtSalesValue = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtDownPayment = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCreditAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cboClient = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getClients();
            }
        };
        txtDefaultChargers = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        tblDefaultChargers = new com.mac.af.component.derived.input.table.CIAddingTable();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();

        cDLabel1.setText("Reference No:");

        cDLabel2.setText("Document No:");

        cDLabel3.setText("Transaction Date:");

        cDLabel4.setText("Sales Value Total:");

        cDLabel5.setText("Down Payment :");

        cDLabel6.setText("Credit Amount :");

        cDLabel7.setText("Client :");

        cDLabel8.setText("Default Charge Total :");

        cDLabel9.setText("Total Amount :");

        cDLabel10.setText("Item Details :");

        cDLabel11.setText("Default Charges :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cDLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cDLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cDLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tblAddingTable, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(txtTotalAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tblDefaultChargers, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                            .addComponent(txtSalesValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDownPayment, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCreditAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDefaultChargers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tblAddingTable, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSalesValue, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tblDefaultChargers, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDefaultChargers, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDownPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCreditAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboClient;
    private com.mac.af.component.derived.input.table.CIAddingTable tblAddingTable;
    private com.mac.af.component.derived.input.table.CIAddingTable tblDefaultChargers;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtCreditAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtDefaultChargers;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtDownPayment;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtSalesValue;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalAmount;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList((Component) cboClient);
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                (Component) txtDocumentNo,
                (Component) tblAddingTable,
                (Component) tblDefaultChargers,
                (Component) txtDownPayment);
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtReferenceNo, new String[]{"salesInvoice", "referenceNo"}),
                new CInputComponentBinder(txtDocumentNo, new String[]{"salesInvoice", "documentNo"}),
                new CInputComponentBinder(txtTransactionDate, new String[]{"salesInvoice", "transactionDate"}),
                new CInputComponentBinder(cboClient, new String[]{"salesInvoice", "client"}),
                new CInputComponentBinder(txtSalesValue, new String[]{"salesInvoice", "salesValue"}),
                new CInputComponentBinder(txtDownPayment, new String[]{"salesInvoice", "downPayment"}),
                new CInputComponentBinder(txtCreditAmount, new String[]{"salesInvoice", "creditAmount"}),
                new CInputComponentBinder(txtCreditAmount, new String[]{"salesInvoice", "creditAmount"}),
                new CInputComponentBinder(txtDefaultChargers, new String[]{"salesInvoice", "defaultChargers"}),
                new CInputComponentBinder(tblAddingTable, new String[]{"salesInvoice", "salesItems"}),
                new CInputComponentBinder(tblDefaultChargers, "chargersWithAmounts"));
    }

    @Override
    protected List<Component> getUneditableComponents() {
        return Arrays.asList(
                (Component) txtReferenceNo,
                (Component) txtTransactionDate,
                (Component) txtSalesValue,
                (Component) txtCreditAmount,
                (Component) txtDefaultChargers,
                (Component) txtTotalAmount);
    }

    @Override
    protected Class<? extends SalesInvoiceCustomObj> getObjectClass() {
        return SalesInvoiceCustomObj.class;
    }

//    @Override
//    protected void afterInitObject(SalesInvoiceCustomObj object) throws ObjectCreatorException {
//        super.afterInitObject(object); 
//        
//        if(object.getSalesInvoice()==null){
//        object.setSalesInvoice(new SalesInvoice());
//        }else{
//        object.setSalesInvoice(object.getSalesInvoice());
//        }
//    }
//
//    
//    
//    @Override
//    protected void initInterface() throws ObjectCreatorException {
//        super.initInterface(); 
//    }
    

}
