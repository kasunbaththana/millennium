/*
 *  SalesInvoiceStatus.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 5, 2014, 2:12:24 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.item;

/**
 *
 * @author mohan
 */
public class SalesInvoiceStatus {
    public static final String ACTIVE = "ACTIVE";
    public static final String CANCEL = "CANCEL";
}
