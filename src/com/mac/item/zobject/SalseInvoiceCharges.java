package com.mac.item.zobject;

/**
  *	@author Channa Mohan
  *	
  *	Created On Sep 21, 2015 2:58:12 PM 
  *	Mohan Hibernate Mapping Generator
  */



/**
 * SalseInvoiceCharges generated by hbm2java
 */
public class SalseInvoiceCharges  implements java.io.Serializable {


     private Integer indexNo;
     private SalesInvoice salesInvoice;
     private ChargeScheme chargeScheme;
     private double value;

    public SalseInvoiceCharges() {
    }

    public SalseInvoiceCharges(SalesInvoice salesInvoice, ChargeScheme chargeScheme, double value) {
       this.salesInvoice = salesInvoice;
       this.chargeScheme = chargeScheme;
       this.value = value;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public SalesInvoice getSalesInvoice() {
        return this.salesInvoice;
    }
    
    public void setSalesInvoice(SalesInvoice salesInvoice) {
        this.salesInvoice = salesInvoice;
    }
    public ChargeScheme getChargeScheme() {
        return this.chargeScheme;
    }
    
    public void setChargeScheme(ChargeScheme chargeScheme) {
        this.chargeScheme = chargeScheme;
    }
    public double getValue() {
        return this.value;
    }
    
    public void setValue(double value) {
        this.value = value;
    }



@Override
public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof SalseInvoiceCharges) ) return false;
		 SalseInvoiceCharges castOther = ( SalseInvoiceCharges ) other; 

	if(!java.util.Objects.equals(this.salesInvoice, castOther.salesInvoice)) {
            return false;
     }
	if(!java.util.Objects.equals(this.chargeScheme, castOther.chargeScheme)) {
            return false;
     }
	if(!java.util.Objects.equals(this.value, castOther.value)) {
            return false;
     }
         
		 return true;
   }

   @Override
   public int hashCode() {
         int result = 17;
         
	result = result * 17 + java.util.Objects.hashCode(this.salesInvoice);
	result = result * 17 + java.util.Objects.hashCode(this.chargeScheme);
	result = result * 17 + java.util.Objects.hashCode(this.value);

         return result;
   }   

}


