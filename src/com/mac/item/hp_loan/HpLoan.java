/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.item.hp_loan;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.item.zobject.SalesInvoice;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thilanga
 */
public class HpLoan extends AbstractGridObject<SalesInvoice> {

    @Override
    protected CTableModel<SalesInvoice> getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Reference No.", "referenceNo"),
                new CTableColumn("Document No.", "documentNo"),
                new CTableColumn("Date", "transactionDate"),
                new CTableColumn("Sales Value", "salesValue"),
                new CTableColumn("Down Payment", "downPayment"),
                new CTableColumn("Credit Amount", "creditAmount"));
    }

    @Override
    protected AbstractObjectCreator<SalesInvoice> getObjectCreator() {
        return new PCHpLoan();
    }

    @Override
    protected Collection<SalesInvoice> getTableData() {
        try {
            return getDatabaseService().getCollection(SalesInvoice.class);
        } catch (DatabaseException ex) {
            Logger.getLogger(HpLoan.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }
}
