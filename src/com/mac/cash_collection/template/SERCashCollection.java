/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.cash_collection.template;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class SERCashCollection extends AbstractService {

    public SERCashCollection(Component component) {
        super(component);
    }
    
    
    public List getMobileList() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.cash_collection.template.zobject.AndroidMobile WHERE status='ACTIVE' ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERCashCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List getOfficeList() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.cash_collection.template.zobject.Employee");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERCashCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    
    
//       public int saveTransaction(String transactionTypeCode, Loan loan) throws DatabaseException {
//        //SAVE TRANSACTION
//        int transactionIndex = SystemTransactions.insertTransaction(
//                getDatabaseService(),
//                transactionTypeCode,
//                receipt.getReferenceNo(),
//                receipt.getDocumentNo(),
//                receipt.getLoan().getIndexNo(),
//                cashierSession.getIndexNo(),
//                receipt.getLoan().getClient().getCode(),
//                receipt.getNote());
//
//        return transactionIndex;
//    }
    
}
