/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.cash_collection.template;

import com.finac.loan.Finac;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.cash_collection.template.zobject.AndroidMobile;
import com.mac.cash_collection.template.zobject.AndroidReceipt;
import com.mac.cash_collection.template.zobject.Employee;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mac.cash_collection.template.zobject.*;

/**
 *
 * @author Administrator
 */
public class CashCollection extends CPanel {

    private SERCashCollection serCashCollection;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyMMddHHmmss");
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Creates new form CashCollection
     */
    public CashCollection() {
        initComponents();
        initOthers();
    }

    protected String getTransactionTypeCode() {
        return SystemTransactions.RECEIPT_TRANSACTION_CODE;
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
         btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
         btnLoadDetails.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON, 16, 16));
         
         
        txtTransactionDate.setCValue((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        serCashCollection = new SERCashCollection(this);
        txtReferanceNo.setCValue(getRefernceenarate());
        txtReferanceNo.setValueEditable(false);
        txtTotalApproved.setValueEditable(false);
        txtTotalCollection.setValueEditable(false);
        txtclientName.setValueEditable(false);
        txtclientCode.setValueEditable(false);

        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnLoadDetails, "doLoad");
        actionUtil.setAction(btnSave, "doSave");


        CTableModel tableModel = new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Receipt No", "receiptNo"),
            new CTableColumn("Agreement No", "agreementNo"),
            new CTableColumn("Due Amount", "amount"),
            new CTableColumn("Paid Amount", "paidAmount"),
            new CTableColumn("payType", "payType"),
            new CTableColumn("Date", "date"),
            new CTableColumn("Time", "time"),
            new CTableColumn("Selected", new String[]{"selected"}, true)
        });
        tblCollection.setCModel(tableModel);


        tblCollection.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                Collection<AndroidReceipt> r = tblCollection.getCValue();
                double approve = 0.0;
                double collect = 0.0;
                for (AndroidReceipt x : r) {
                    if (x.isSelected()) {
                        approve += Double.parseDouble("" + x.getPaidAmount());
                    }
                        collect += Double.parseDouble("" + x.getPaidAmount());
                }
                txtTotalApproved.setCValue(approve);
                txtTotalCollection.setCValue(collect);
                getclientDetails(
                        tblCollection.getValueAt(
                       (int) tblCollection.getSelectedRow(), 
                       1).toString());
            }
        });


    }

    private String getRefernceenarate() {
        return "REC" + (String) CApplication.getSessionVariable(Finac.USER_PREFIX_KEY) + DATE_FORMAT.format(new Date());
    }

    @Action
    public void doSave() {
        int q = mOptionPane.showConfirmDialog(null, "Do you sure want to save ?", "Save", mOptionPane.YES_NO_OPTION);
        if (q == mOptionPane.YES_OPTION) {
            try {

                Collection<AndroidReceipt> receipts = tblCollection.getCValue();
                Employee officer = (Employee) cboCollectionOfficer.getCValue();


                for (AndroidReceipt receipt : receipts) {
                    if (receipt.isSelected()) {


                        String sqlget = "SELECT loan.`index_no` AS loan_index , loan.`client` AS clientx FROM loan WHERE loan.`agreement_no`='" + receipt.getAgreementNo() + "'";
                        ResultSet loanSet = getConnection().createStatement().executeQuery(sqlget);
                        if (loanSet.next()) {

                            int transactionIndex = SystemTransactions.insertTransactionWithOfficer(
                                    getDatabaseService(), getTransactionTypeCode(),
                                    txtReferanceNo.getCValue(), txtDocumentNo.getCValue(),
                                    loanSet.getInt("loan_index"), null, loanSet.getString("clientx"), officer.getCode(),
                                    "note",txtLoadingDate.getCValue());


                            String mobileReceipt = "CALL z_mobile_receipt("
                                    + "" + transactionIndex + ","
                                    + "'" + getFormattedDate(txtTransactionDate.getCValue()) + "',"
                                    + "'" + getFormattedDate(txtLoadingDate.getCValue()) + "',"
                                    + "'" + receipt.getAgreementNo() + "',"
                                    + "'" + receipt.getAutoNo() + "',"
                                    + "" + receipt.getPaidAmount() + ");";

                            getDatabaseService().callUpdateProcedure(mobileReceipt);

                        }
                    }

                }

                mOptionPane.showMessageDialog(null, "Successfully saved !!!", "Save", mOptionPane.INFORMATION_MESSAGE);
                resetValues();
            } catch (Exception e) {
                e.printStackTrace();
                mOptionPane.showMessageDialog(this, "Sace Faild", "Data saving ... :( \n"+e.getMessage(), mOptionPane.ERROR_MESSAGE);
            }
        }

    }

    @Action
    public void doLoad() {
        List<AndroidReceipt> cashList;
        Date datex = txtLoadingDate.getCValue();
        AndroidMobile mobile = (AndroidMobile) cboMobile.getCValue();


        try {
            String hql = "FROM com.mac.cash_collection.template.zobject.AndroidReceipt "
                    + "WHERE status='0' AND date='" + dateFormat.format(datex) + "'AND androidMobile='" + mobile.getMacAddress() + "' ";
            cashList = getDatabaseService().getCollection(hql, null);


        } catch (DatabaseException ex) {
            Logger.getLogger(CashCollection.class.getName()).log(Level.SEVERE, null, ex);
            cashList = new ArrayList<>();
        }


        tblCollection.setCValue(cashList);

        if (cashList.size() < 1) {
            mOptionPane.showMessageDialog(this, txtLoadingDate.getCValue().toString() + " AND " + mobile.getMobileNo() + " No Details Found For this Parameters...", "Data Loading ... :(", mOptionPane.WARNING_MESSAGE);
        }

    }
    
      public void getclientDetails(String androidReceipt) {
        List<Loan> cashList;
        
        try {
            String hql = "FROM com.mac.cash_collection.template.zobject.Loan  where  agreementNo='"+androidReceipt+"' ";
            cashList = getDatabaseService().getCollection(hql, null);

            getDatabaseService().commitLocalTransaction();
        } catch (DatabaseException ex) {
            Logger.getLogger(CashCollection.class.getName()).log(Level.SEVERE, null, ex);
            cashList = new ArrayList<>();
        }
        txtclientName.resetValue();
        txtclientCode.resetValue();
        
        for(Loan list:cashList){
        txtclientName.setCValue(list.getClient().getLongName());
         txtclientCode.setCValue(list.getClient().getCode());
        }
        
    }
    
    
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        tblCollection = new com.mac.af.component.derived.input.table.CITable();
        jPanel3 = new javax.swing.JPanel();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        cboMobile = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return serCashCollection.getMobileList();
            }
        };
        cboCollectionOfficer = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return serCashCollection.getOfficeList();
            }
        };
        btnLoadDetails = new com.mac.af.component.derived.command.button.CCButton();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferanceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoadingDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        txtTotalApproved = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTotalCollection = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtclientName = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        txtclientCode = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();

        tblCollection.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblCollection);

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnLoadDetails.setText("Load Details");

        cDLabel4.setText("Transaction Date :");

        cDLabel3.setText("Document No.:");

        cDLabel2.setText("Reference No.:");

        cDLabel5.setText("Mobile :");

        cDLabel6.setText("Collection Officer :");

        cDLabel7.setText("Loading Date :");

        cDLabel8.setText("Total Approved Amount :");

        cDLabel9.setText("Total Collection  Amount :");

        cDLabel10.setText("Customer Full Name :");

        cDLabel11.setText("Code :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
                            .addComponent(txtReferanceNo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtLoadingDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(cDLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboCollectionOfficer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboMobile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnLoadDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalCollection, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtclientCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtclientName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalApproved, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferanceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoadingDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboCollectionOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotalCollection, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotalApproved, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLoadDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(89, 89, 89)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtclientName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtclientCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(66, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 699, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 43, Short.MAX_VALUE))
            .addComponent(jScrollPane2)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSaveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnLoadDetails;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboCollectionOfficer;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboMobile;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private com.mac.af.component.derived.input.table.CITable tblCollection;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtLoadingDate;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferanceNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalApproved;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtTotalCollection;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    private com.mac.af.component.derived.input.textfield.CIStringField txtclientCode;
    private com.mac.af.component.derived.input.textfield.CIStringField txtclientName;
    // End of variables declaration//GEN-END:variables

    public String getFormattedDate(Date date) {
        return dateFormat.format(date);
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }

    private void resetValues() {
        txtLoadingDate.setCValue((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        tblCollection.setCValue(null);
        txtDocumentNo.setCValue(null);
        cboMobile.resetValue();
        txtReferanceNo.setCValue(getRefernceenarate());
        cboCollectionOfficer.resetValue();

    }
}
