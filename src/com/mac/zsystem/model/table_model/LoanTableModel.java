/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class LoanTableModel extends CTableModel<Object> {

    public LoanTableModel() {
        super(new CTableColumn("Agreement No", new String[]{"agreementno"}),
                new CTableColumn("Client", new String[]{"client"}));
    }
}
