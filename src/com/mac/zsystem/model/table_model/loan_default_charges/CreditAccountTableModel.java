/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.loan_default_charges;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class CreditAccountTableModel extends CTableModel {

    public CreditAccountTableModel() {
        super(new CTableColumn("Account Code", new String[]{"code"}),
                new CTableColumn("Account Name", new String[]{"name"}));
    }
}
