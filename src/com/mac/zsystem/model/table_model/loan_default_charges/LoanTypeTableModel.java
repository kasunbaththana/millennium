/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.loan_default_charges;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class LoanTypeTableModel extends CTableModel {

    public LoanTypeTableModel() {
          super(new CTableColumn("Loan type code", new String[]{"code"}),
                new CTableColumn("Loan type name", new String[]{"name"}));
    }
}
