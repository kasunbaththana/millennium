/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class BranchTableModel extends CTableModel {

    public BranchTableModel() {
        super(new CTableColumn("Company Code", new String[]{"code"}),
                new CTableColumn("Company Name", new String[]{"name"}));
    }
}
