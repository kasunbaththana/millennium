/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.recept_table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class ReceiptTableModel extends CTableModel {

    public ReceiptTableModel() {
        super(new CTableColumn("NIC", new String[]{"client","nicNo"}),
                new CTableColumn("Full Name", new String[]{"client","longName"}),
                new CTableColumn("Address", new String[]{"client","addressLine1"}),
                new CTableColumn("Mobile", new String[]{"client","mobile"}),
                new CTableColumn("Agreement No", new String[]{"agreementNo"}),
                new CTableColumn("Vehicle", new String[]{"Vehicle"})
                
                );
    }
}
