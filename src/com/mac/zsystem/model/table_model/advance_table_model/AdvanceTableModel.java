/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.advance_table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class AdvanceTableModel extends CTableModel {

    public AdvanceTableModel() {
        super(new CTableColumn("NIC", new String[]{"loan.client","nicNo"}),
                new CTableColumn("Full Name", new String[]{"loan.client","longName"}),
                new CTableColumn("Address", new String[]{"loan.client","addressLine1"}),
                new CTableColumn("Mobile", new String[]{"loan.client","mobile"}),
                new CTableColumn("Agreement No", new String[]{"loan.agreementNo"})
                
                );
    }
}
