/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.hp_loan_application;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga 
 */
public class SalesInvoiceTableModel extends CTableModel {

    public SalesInvoiceTableModel() {
           super(new CTableColumn("Reference No", new String[]{"referenceNo"}),
                new CTableColumn("Document No", new String[]{"documentNo"}));
    }
    
}
