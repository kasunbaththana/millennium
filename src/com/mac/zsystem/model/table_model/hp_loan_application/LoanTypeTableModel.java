/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.hp_loan_application;

//import com.mac.zsystem.model.table_model.loan_application.*;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class LoanTypeTableModel extends CTableModel {

    public LoanTypeTableModel() {
        super(new CTableColumn("Code", new String[]{"code"}),
                new CTableColumn("Name", new String[]{"name"}));

    }
}
