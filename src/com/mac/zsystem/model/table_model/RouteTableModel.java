/*
 *  RouteTableModel.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 21, 2014, 12:57:20 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.model.table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author mohan
 */
public class RouteTableModel extends CTableModel {

    public RouteTableModel() {
        super(new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"})
        });
    }
}
