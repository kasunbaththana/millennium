/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.land_table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class LandPurchaseTableModel extends CTableModel<Object> {

    public LandPurchaseTableModel() {
        super(new CTableColumn("Supplier Code", new String[]{"code"}),
                new CTableColumn("Supplier Name", new String[]{"name"}));
    }
}
