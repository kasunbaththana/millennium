/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.land_table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class LandProjectTableModel extends CTableModel {

    public LandProjectTableModel() {
        super(new CTableColumn("Land Code", new String[]{"landCode"}),
                new CTableColumn("Description", new String[]{"description"}));
    }
}
