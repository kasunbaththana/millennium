/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.land_table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class BlockCreationTableModel extends CTableModel<Object> {

    public BlockCreationTableModel() {
        super(new CTableColumn("Project Code", new String[]{"projectCode"}),
                new CTableColumn("Description", new String[]{"description"}));
    }
}
