/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.loan_application;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class LoanGroupTableModel extends CTableModel {

    public LoanGroupTableModel() {
        super(new CTableColumn("Loan group code", new String[]{"code"}),
                new CTableColumn("Loan group name", new String[]{"name"}));
    }
}
