/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model.loan_application;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class ClientTableModel extends CTableModel {

    public ClientTableModel() {
        super(new CTableColumn("Client Code", new String[]{"code"}),
                new CTableColumn("Client Name", new String[]{"name"}));
    }
}
