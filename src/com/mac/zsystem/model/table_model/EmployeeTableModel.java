/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.model.table_model;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author thilanga
 */
public class EmployeeTableModel extends CTableModel<Object> {

    public EmployeeTableModel() {
        super(new CTableColumn("Branch Code", new String[]{"code"}),
                new CTableColumn("Branch Name", new String[]{"name"}));
    }
}
