package com.mac.zsystem.settings.transaction_settings.object;

/**
  *	@author Channa Mohan
  *	
  *	Created On Jun 19, 2015 2:07:43 PM 
  *	Mohan Hibernate Mapping Generator
  */


import java.util.HashSet;
import java.util.Set;

/**
 * Report generated by hbm2java
 */
public class Report  implements java.io.Serializable {


     private String code;
     private String name;
     private String category;
     private byte[] report;
     private Set<TransactionType> transactionTypes = new HashSet<TransactionType>(0);

    public Report() {
    }

	
    public Report(String code, String name, String category, byte[] report) {
        this.code = code;
        this.name = name;
        this.category = category;
        this.report = report;
    }
    public Report(String code, String name, String category, byte[] report, Set<TransactionType> transactionTypes) {
       this.code = code;
       this.name = name;
       this.category = category;
       this.report = report;
       this.transactionTypes = transactionTypes;
    }
   
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getCategory() {
        return this.category;
    }
    
    public void setCategory(String category) {
        this.category = category;
    }
    public byte[] getReport() {
        return this.report;
    }
    
    public void setReport(byte[] report) {
        this.report = report;
    }
    public Set<TransactionType> getTransactionTypes() {
        return this.transactionTypes;
    }
    
    public void setTransactionTypes(Set<TransactionType> transactionTypes) {
        this.transactionTypes = transactionTypes;
    }


	@Override
     public String toString() {
		return code + "-" + name;
     }




	@Override
	public boolean equals(Object other) {
        if ( (this == other ) ){
			return true;
		}
		
		if ( (other == null ) ){
			return false;
		}
		
		if ( !(other instanceof Report) ){
			return false;
		}
		
		Report castOther = ( Report ) other; 

		if(this.code==null && castOther.code==null) {
			return false;
		}
		
		if(!java.util.Objects.equals(this.code, castOther.code)) {
            return false;
		}
        
		return true;
   }

    @Override
    public int hashCode() {
        int result = 17;
         
		result = result * 17 + java.util.Objects.hashCode(this.code);

        return result;
    }




}


