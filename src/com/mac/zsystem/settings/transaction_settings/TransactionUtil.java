/*
 *  TransactionUtil.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 16, 2015, 9:17:43 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zsystem.settings.transaction_settings;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.templates.report.ReportBuilder;
import com.mac.af.templates.report.ReportDialog;
import com.mac.zreport.zobject.Report;
import com.mac.zsystem.settings.transaction_settings.object.TransactionType;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class TransactionUtil {

    public static void PrintReport(final HibernateDatabaseService databaseService, final String transactionType, final Map params) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    TransactionType type = getTransactionType(databaseService, transactionType);

                    if (type.isPrintReport()) {


                        int q = mOptionPane.showConfirmDialog(null, "Do you want to pring report ?", "Print Report", mOptionPane.YES_NO_OPTION, mOptionPane.QUESTION_MESSAGE);

                        if (q == mOptionPane.YES_OPTION) {


                            if (type.getReport() != null) {
                                InputStream inputStream = new ByteArrayInputStream(type.getReport().getReport());

                                com.mac.zreport.ReportBuilder.initDefaultParameters();
                                Map<String, Object> defaultParams = com.mac.zreport.ReportBuilder.DEFAULT_PARAMETERS;

                                ReportDialog reportDialog = new ReportDialog(inputStream,
                                        defaultParams,
                                        params);
                                reportDialog.setTitle(ReportBuilder.getFormattedString(type.getName()));
                                reportDialog.setVisible(true);
                            }
                        }
                    }
                } catch (DatabaseException ex) {
                    Logger.getLogger(TransactionUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        CApplication.invokeEventDispatch(runnable);
    }
    public static void PrintRemindLetter(final HibernateDatabaseService databaseService, final String reportCode, final Map params) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Report type = (Report) databaseService.getObject(Report.class, reportCode);

//                    if (type.isPrintReport()) {


                        int q = mOptionPane.showConfirmDialog(null, "Do you want to pring report ?", "Print Report", mOptionPane.YES_NO_OPTION, mOptionPane.QUESTION_MESSAGE);

                        if (q == mOptionPane.YES_OPTION) {


                            if (type.getReport() != null) {
                                InputStream inputStream = new ByteArrayInputStream(type.getReport());

                                com.mac.zreport.ReportBuilder.initDefaultParameters();
                                Map<String, Object> defaultParams = com.mac.zreport.ReportBuilder.DEFAULT_PARAMETERS;

                                ReportDialog reportDialog = new ReportDialog(inputStream,
                                        defaultParams,
                                        params);
                                reportDialog.setTitle(ReportBuilder.getFormattedString(type.getName()));
                                reportDialog.setVisible(true);
                            }
                        }
//                    }
                } catch (DatabaseException ex) {
                    Logger.getLogger(TransactionUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        CApplication.invokeEventDispatch(runnable);
    }

    public static boolean isSendToApprove(HibernateDatabaseService databaseService, String transactionTypeCode) throws DatabaseException {
        TransactionType transactionType = getTransactionType(databaseService, transactionTypeCode);

        return transactionType.isApprove();


    }

    private static TransactionType getTransactionType(HibernateDatabaseService databaseService, String code) throws DatabaseException {
        return (TransactionType) databaseService.getObject(TransactionType.class, code);
    }
}
