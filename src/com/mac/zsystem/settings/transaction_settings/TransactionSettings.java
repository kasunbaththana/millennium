/*
 *  TransactionSettings.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 15, 2015, 4:42:19 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zsystem.settings.transaction_settings;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zsystem.settings.transaction_settings.object.TransactionType;

/**
 *
 * @author mohan
 */
public class TransactionSettings extends AbstractRegistrationForm<TransactionType> {

    @Override
    public CTableModel<TransactionType> getTableModel() {
        return new CTableModel(
                new CTableColumn("Code", "code"),
                new CTableColumn("Name", "name"));
    }

    @Override
    public AbstractObjectCreator<TransactionType> getObjectCreator() {
        return new PCTransaction();
    }

//    @Override
//    public  Collection<TransactionType> getTableData() {
//        List<TransactionType> transactions;
//        
//        try {
//            transactions = getDatabaseService().getCollection(TransactionType.class);
//        } catch (DatabaseException ex) {
//            Logger.getLogger(TransactionSettings.class.getName()).log(Level.SEVERE, null, ex);
//            
//            transactions = new ArrayList<>();
//        }
//        
//        return transactions;
//
//    }
    @Override
    public Class<? extends TransactionType> getObjectClass() {
        return TransactionType.class;
    }

    @Override
    protected int getRegistrationType() {
        return EDIT_ONLY_REGISTRATION_TYPE;
    }
}
