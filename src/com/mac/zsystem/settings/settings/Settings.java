/*
 *  Settings.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 24, 2014, 4:15:33 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.settings.settings;

import com.mac.af.component.renderer.tree.CTreeRenderer;

/**
 *
 * @author mohan
 */
public class Settings extends com.mac.af.core.setting.gui.Settings {

    @Override
    protected CTreeRenderer getTreeRenderer() {
        return new SettingsTreeRenderer();
    }
}
