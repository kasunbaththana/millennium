/*
 *  SettingsTreeRenderer.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jun 30, 2014, 5:46:26 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.settings.settings;

import javax.swing.Icon;
import javax.swing.JTree;
import com.mac.af.component.renderer.tree.CTreeRenderer;
import com.mac.af.core.setting.SettingObject;
import com.mac.zresources.FinacResources;

/**
 *
 * @author mohan
 */
public class SettingsTreeRenderer
        extends CTreeRenderer {

    @Override
    public String getText(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        if (!leaf) {
            return "<HTML><B><H4><FONT FACE='Verdana'>" + ((SettingObject) value).getName() + "</FONT></H4></HTML></B>";
        } else {
            return ((SettingObject) value).getName();
        }
    }

    @Override
    public Icon getIcon(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        return SETTING_ICON;
    }
    private static final Icon SETTING_ICON = FinacResources.getImageIcon(FinacResources.SETTING, 24, 24);
}
