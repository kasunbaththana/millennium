/*
 *  REGUserRolePermission.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 22, 2015, 9:41:25 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zsystem.permission.user_role_permission;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zsystem.permission.object.UserRole;

/**
 *
 * @author mohan
 */
public class REGUserRolePermission extends AbstractRegistrationForm<UserRole> {

    @Override
    public AbstractObjectCreator<UserRole> getObjectCreator() {
        return new PCUserRolePermission();
    }

    @Override
    public Class<? extends UserRole> getObjectClass() {
        return UserRole.class;
    }

    @Override
    public CTableModel<UserRole> getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Code", "code"),
                new CTableColumn("User Role", "name"));
    }
}
