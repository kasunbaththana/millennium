/*
 *  REGPermission.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 22, 2015, 9:03:25 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zsystem.permission.permission;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zsystem.permission.object.Permission;

/**
 *
 * @author mohan
 */
public class REGPermission extends AbstractRegistrationForm<Permission> {

    @Override
    public AbstractObjectCreator<Permission> getObjectCreator() {
        return new PCPermission();
    }

    @Override
    public Class<? extends Permission> getObjectClass() {
        return Permission.class;
    }

    @Override
    public CTableModel<Permission> getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Name", "name"),
                new CTableColumn("Permission", "class_")
                );
    }
}
