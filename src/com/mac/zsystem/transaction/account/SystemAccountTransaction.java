/*
 *  SystemAccountTransaction.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:56:08 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account;

import com.mac.account.zobject.cheque.BankAccount;
import com.mac.account.ztemplate.cheque_transaction.PCChequeTransactionInformation;
import com.mac.account.ztemplate.cheque_transaction.custom_object.ChequeTransactionObject;
import com.mac.account.ztemplate.cheque_transaction.custom_object.SERTranseDate;
import com.mac.af.core.ApplicationRuntimeException;
import com.mac.af.core.concurrent.CExecutable;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.message.mOptionPane;
import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionStatus;
import com.mac.zsystem.transaction.account.account_setting.system_interface.AdvanceAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.AdvanceLoanAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.AdvanceRefundInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.AdvanceSettleAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.BankDepositReceiptAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeDepositAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeIssueAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeIssueReturnAccountInterface;
//import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeRealizeAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeReceiptAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ChequeReturnAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.DayEndAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.DisbursementAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.EarlySettleLoanAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SupplierPaymentAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.GroupReceiptAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.InsuranceAccountInterface;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.account.object.AccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanApplicationAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanApprovalAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanCloseAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.LoanValuationAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAvailableCapitalAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAvailableInterstAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAvailableOtherCharglAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OpningLoanAvailablePanaltyAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.OtherChargeAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.PettyCashAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.RebitLoanAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.ReceiptAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SDepositAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SSavingOpeningStartAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SSavingStartAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SWithdrawAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SalesInvoiceAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.TemperaryReceiptAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.VoucherAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.WriteOffAccountInterface;
import com.mac.zsystem.transaction.account.object.Account;
import com.mac.zsystem.transaction.account.object.GeneralVoucherDet;
import com.mac.zsystem.transaction.account.object.GeneralVoucherSum;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author mohan
 */
public class SystemAccountTransaction {

    private static final AccountInterface LOAN_ACCOUNT_INTERFACE = new LoanAccountInterface();
    private static final AccountInterface LOAN_APPLICATION_ACCOUNT_INTERFACE = new LoanApplicationAccountInterface();
    private static final AccountInterface LOAN_APPROVAL_ACCOUNT_INTERFACE = new LoanApprovalAccountInterface();
    private static final AccountInterface VOUCHER_ACCOUNT_INTERFACE = new VoucherAccountInterface();
    private static final AccountInterface RECEIPT_ACCOUNT_INTERFACE = new ReceiptAccountInterface();
    private static final AccountInterface GROUP_RECEIPT_ACCOUNT_INTERFACE = new GroupReceiptAccountInterface();
    private static final AccountInterface BANK_DEPOSIT_RECEIPT_ACCOUNT_INTERFACE = new BankDepositReceiptAccountInterface();
    private static final AccountInterface TEMP_RECEIPT_ACCOUNT_INTERFACE = new TemperaryReceiptAccountInterface();
    private static final AccountInterface CHEQUE_DEPOSIT_ACCOUNT_INTERFACE = new ChequeDepositAccountInterface();
    private static final AccountInterface ADVANCE_LOAN_ACCOUNT_INTERFACE = new AdvanceLoanAccountInterface();
//    private static final AccountInterface CHEQUE_REALIZE_ACCOUNT_INTERFACE = new ChequeRealizeAccountInterface();
    private static final AccountInterface CHEQUE_RETURN_ACCOUNT_INTERFACE = new ChequeReturnAccountInterface();
    private static final AccountInterface CHEQUE_ISSUE_ACCOUNT_INTERFACE = new ChequeIssueAccountInterface();
    private static final AccountInterface CHEQUE_ISSUE_RETURN_ACCOUNT_INTERFACE = new ChequeIssueReturnAccountInterface();
    private static final AccountInterface DAY_END_ACCOUNT_INTERFACE = new DayEndAccountInterface();
    private static final AccountInterface OTHER_CHARGE_ACCOUNT_INTERFACE = new OtherChargeAccountInterface();
    private static final AccountInterface INSURANCE_ACCOUNT_INTERFACE = new InsuranceAccountInterface();
    private static final AccountInterface LOAN_CLOSE_ACCOUNT_INTERFACE = new LoanCloseAccountInterface();
    private static final AccountInterface SALES_INVOICE_ACCOUNT_INTERFACE = new SalesInvoiceAccountInterface();
    private static final AccountInterface SUPPLIER_PAYMENT_ACCOUNT_INTERFACE = new SupplierPaymentAccountInterface();
    private static final AccountInterface DISBURSEMENT_PAYMENT_ACCOUNT_INTERFACE = new DisbursementAccountInterface();
    //REBIT REBIT
    private static final AccountInterface REBIT_ACCOUNT_INTERFACE = new RebitLoanAccountInterface();
    private static final AccountInterface ADVANCE_ACCOUNT_INTERFACE = new AdvanceAccountInterface();
    private static final AccountInterface ADVANCE_SETTLE_INTERFACE = new AdvanceSettleAccountInterface();
    private static final AccountInterface ADVANCE_REFUND_INTERFACE = new AdvanceRefundInterface();
    private static final AccountInterface EARLY_SETTLE_REBIT_ACCOUNT_INTERFACE = new EarlySettleLoanAccountInterface();
//    private static final AccountInterface CUSTOMER_PAYMENT_INTERFACE = new CustomerPaymentAccountInterface();
    private static final AccountInterface CHEQUE_RECEIPT_INTERFACE = new ChequeReceiptAccountInterface();
    //OPNING LOAN
    private static final AccountInterface OPNING_LOAN_AMOUNT_INTERFACE = new OpningLoanAccountInterface();
    private static final AccountInterface OPNING_LOAN_AVAILABLE_CAPITAL_INTERFACE = new OpningLoanAvailableCapitalAccountInterface();
    private static final AccountInterface OPNING_LOAN_AVAILABLE_INTERST_INTERFACE = new OpningLoanAvailableInterstAccountInterface();
    private static final AccountInterface OPNING_LOAN_AVAILABLE_PANALTY_INTERFACE = new OpningLoanAvailableOtherCharglAccountInterface();
    private static final AccountInterface OPNING_LOAN_AVAILABLE_OTHER_CHARG_INTERFACE = new OpningLoanAvailablePanaltyAccountInterface();
    
    private static final AccountInterface WRITE_OFF_INTERFACE = new WriteOffAccountInterface();
   //saving
    private static final AccountInterface SAVING = new SSavingStartAccountInterface();
    private static final AccountInterface OPNING_SAVING = new SSavingOpeningStartAccountInterface();
    private static final AccountInterface OPNING_WITHDROW = new SWithdrawAccountInterface();
    private static final AccountInterface SAVING_DEPOSIT = new SDepositAccountInterface();
   // private static final AccountInterface OPNING_LOAN_AVAILABLE_OTHER_CHARG_INTERFACE = new OpningLoanAvailablePanaltyAccountInterface();
    
    private static final AccountInterface LOAN_VALUVATION_INTERFACE = new LoanValuationAccountInterface();
    
     private static final AccountInterface PETTY_CASH_ACCOUNT_INTERFACE = new PettyCashAccountInterface();

    //    private static final AccountInterface LOAN_CHARGE_ACCOUNT_INTERFACE = new LoanChargeAccountInterface();
    public static List<AccountInterface> getAccountInterfaces() {
        return Arrays.asList(
                (AccountInterface) LOAN_ACCOUNT_INTERFACE,
                (AccountInterface) LOAN_APPLICATION_ACCOUNT_INTERFACE,
                (AccountInterface) LOAN_APPROVAL_ACCOUNT_INTERFACE,
                (AccountInterface) VOUCHER_ACCOUNT_INTERFACE,
                (AccountInterface) SUPPLIER_PAYMENT_ACCOUNT_INTERFACE,
                (AccountInterface) RECEIPT_ACCOUNT_INTERFACE,
                (AccountInterface) GROUP_RECEIPT_ACCOUNT_INTERFACE,
                (AccountInterface) BANK_DEPOSIT_RECEIPT_ACCOUNT_INTERFACE,
                (AccountInterface) TEMP_RECEIPT_ACCOUNT_INTERFACE,
                (AccountInterface) CHEQUE_DEPOSIT_ACCOUNT_INTERFACE,
//                (AccountInterface) CHEQUE_REALIZE_ACCOUNT_INTERFACE,
                (AccountInterface) CHEQUE_RETURN_ACCOUNT_INTERFACE,
                (AccountInterface) CHEQUE_ISSUE_ACCOUNT_INTERFACE,
                (AccountInterface) CHEQUE_ISSUE_RETURN_ACCOUNT_INTERFACE,
                (AccountInterface) DAY_END_ACCOUNT_INTERFACE,
                (AccountInterface) OTHER_CHARGE_ACCOUNT_INTERFACE,
                (AccountInterface) INSURANCE_ACCOUNT_INTERFACE,
                (AccountInterface) LOAN_CLOSE_ACCOUNT_INTERFACE,
                (AccountInterface) SALES_INVOICE_ACCOUNT_INTERFACE,
                (AccountInterface) EARLY_SETTLE_REBIT_ACCOUNT_INTERFACE,
                (AccountInterface) DISBURSEMENT_PAYMENT_ACCOUNT_INTERFACE,
                (AccountInterface) ADVANCE_LOAN_ACCOUNT_INTERFACE,
                //(AccountInterface) CUSTOMER_PAYMENT_INTERFACE,
                //REBAIT
                (AccountInterface) REBIT_ACCOUNT_INTERFACE,
                (AccountInterface) LOAN_VALUVATION_INTERFACE,
                (AccountInterface) PETTY_CASH_ACCOUNT_INTERFACE,
                (AccountInterface) ADVANCE_ACCOUNT_INTERFACE,
                (AccountInterface) ADVANCE_SETTLE_INTERFACE,
                (AccountInterface) ADVANCE_REFUND_INTERFACE,
                //OPNING LOAN
                (AccountInterface) OPNING_LOAN_AMOUNT_INTERFACE,
                (AccountInterface) OPNING_LOAN_AVAILABLE_CAPITAL_INTERFACE,
                (AccountInterface) OPNING_LOAN_AVAILABLE_INTERST_INTERFACE,
                (AccountInterface) OPNING_LOAN_AVAILABLE_PANALTY_INTERFACE,
                (AccountInterface) CHEQUE_RECEIPT_INTERFACE,
                (AccountInterface) OPNING_LOAN_AVAILABLE_OTHER_CHARG_INTERFACE,
                //saving
                (AccountInterface) OPNING_SAVING,
                (AccountInterface) SAVING,
                (AccountInterface) OPNING_WITHDROW,
                (AccountInterface) SAVING_DEPOSIT,
                (AccountInterface) WRITE_OFF_INTERFACE
                
                
                );
    }
private ChequeTransactionObject chequeTransactionObject;
    public static SystemAccountTransaction getInstance() {
        return new SystemAccountTransaction();
    }

    public void beginAccountTransactionQueue() {
        if (accountTransactionQueues != null) {
            throw new ApplicationRuntimeException("Account Transaction Queue is already begun");
        }

        accountTransactionQueues = new ArrayList<>();
    }

    public void addAccountTransactionQueue(
            String accountSettingCode,
            String description,
            Double amount,
            String type) {
        checkAccountTransactionQueue();
//        System.out.println("type______"+type);
//        System.out.println("accountSettingCode"+accountSettingCode);
//        System.out.println("description"+description);
//        System.out.println("amount"+amount);
        
        AccountTransactionQueue queue = new AccountTransactionQueue();
        queue.setAccountSettingCode(accountSettingCode);
        queue.setDescription(description);
        queue.setAmount(amount);
        queue.setType(type);

        accountTransactionQueues.add(queue);
    }
        public void addAccountTransactionQueueChq(
            String accountSettingCode,
            String description,
            Double amount,
            String type,
            Date dateTrans) {
        checkAccountTransactionQueue();
         sERTranseDate = new  SERTranseDate();
             
            sERTranseDate.setTransaction_date(dateTrans);
        
//        System.out.println("type______"+type);
//        System.out.println("accountSettingCode"+accountSettingCode);
//        System.out.println("description"+description);
//        System.out.println("amount"+amount);
        
        AccountTransactionQueue queue = new AccountTransactionQueue();
        queue.setAccountSettingCode(accountSettingCode);
        queue.setDescription(description);
        queue.setAmount(amount);
        queue.setType(type);

        accountTransactionQueues.add(queue);
    }

    public void flushAccountTransactionQueue(
            final HibernateDatabaseService parentDatabaseService,
            final int transaction,
            final String transactionType) throws DatabaseException {

//            System.out.println("parentDatabaseService.getCPanel().getTabTitle()___"+parentDatabaseService.getCPanel().getTabTitle());
        checkAccountTransactionQueue();

        CExecutable<Void> executable = new CExecutable<Void>(parentDatabaseService.getCPanel().getTabTitle() + " Update accounts", parentDatabaseService.getCPanel()) {
            @Override
            public Void execute() throws Exception {

                HibernateDatabaseService databaseService = parentDatabaseService.createChildDatabaseService();
                HashMap<AccountSetting, AccountTransactionQueue> accountQueues = new HashMap<>();
                List< AccountSetting> accountSettings = getAccountSettings(databaseService, transactionType);
                //CHECK SETTINGS
                boolean isActive = false;
                for (AccountSetting accountSetting : accountSettings) {
                    isActive = isActive || (accountSetting.isActive() ? accountSetting.getAccount() == null : false);
                    if (isActive) {
                        mOptionPane.showMessageDialog(
                                databaseService.getCPanel(),
                                "Error occured in Account Transaction. Please check Account Settings related to the transaction " + accountSetting.getTransactionType().getName() + ".",
                                "Account Transaction",
                                mOptionPane.WARNING_MESSAGE);
                    }
                }

                //MATCH ACCOUNT SETTINGS AND QUEUE
                for (AccountSetting accountSetting : accountSettings) {
                    for (AccountTransactionQueue accountTransactionQueue : accountTransactionQueues) {
                        if (accountSetting.getCode().equals(accountTransactionQueue.getAccountSettingCode())) {
                            accountQueues.put(accountSetting, accountTransactionQueue);
                        }
                    }
                }

                databaseService.beginLocalTransaction();

                AccountTransactionQueue transactionQueue;
                for (AccountSetting accountSetting : accountSettings) {
                    if (accountSetting.isActive()) {
                        transactionQueue = accountQueues.get(accountSetting);
//                    
                        AccountTransaction accountTransaction =
                                createAccountTransaction(
                                transaction,
                                transactionType,
                                "AUTO",
                                accountSetting.getCreditOrDebit(),
                                transactionQueue.getAmount(),
                                accountSetting.getAccount(),
                                transactionQueue.getDescription(),
                                transactionQueue.getAccountSettingCode());

                        databaseService.save(accountTransaction);
                    }
                }

                databaseService.commitLocalTransaction();

                return null;

            }
        };

        CApplication.getExecutionManager().execute(executable);
    }
    

     
    private AccountTransaction createAccountTransaction(
            int transaction,
            String transactionType,
            String type,
            String creditOrDebit,
            double amount,
            Account account,
            String description,
            String accountSetting) {
        AccountTransaction accountTransaction = new AccountTransaction();
        try {
            accountTransaction.setTransactionDate(
                sERTranseDate.getTransaction_date());
        } catch (NullPointerException e) {
            accountTransaction.setTransactionDate(
                (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        }
//        accountTransaction.setTransactionDate(
//                sERTranseDate.getTransaction_date());
        
        accountTransaction.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
        accountTransaction.setAccountSetting(accountSetting);
        accountTransaction.setDescription(description);
        accountTransaction.setAccount(account);
        

        if (creditOrDebit.equals(AccountSettingCreditOrDebit.CREDIT)) {
            accountTransaction.setCreditAmount(amount);
            accountTransaction.setDebitAmount(0.0);
        }
        if (creditOrDebit.equals(AccountSettingCreditOrDebit.DEBIT)) {
            accountTransaction.setCreditAmount(0.0);
            accountTransaction.setDebitAmount(amount);
        }
        accountTransaction.setTransaction(transaction);
        accountTransaction.setTransactionType(transactionType);
        accountTransaction.setType(type);
      
        accountTransaction.setStatus(AccountTransactionStatus.ACTIVE);
   
        

        return accountTransaction;
    }
    private GeneralVoucherDet createAccountTransactionVoucher(
            int transaction,
            String transactionType,
            String type,
            String creditOrDebit,
            double amount,
            Account account,
            String description,
            String accountSetting) {
        GeneralVoucherDet accountTransaction = new GeneralVoucherDet();
        try {
            accountTransaction.setTransactionDate(
                sERTranseDate.getTransaction_date());
        } catch (NullPointerException e) {
            accountTransaction.setTransactionDate(
                (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        }
//        accountTransaction.setTransactionDate(
//                sERTranseDate.getTransaction_date());
        
        accountTransaction.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
        accountTransaction.setAccountSetting(accountSetting);
        accountTransaction.setDescription(description);
        accountTransaction.setAccount(account.getCode());
        

        if (creditOrDebit.equals(AccountSettingCreditOrDebit.CREDIT)) {
            accountTransaction.setCreditAmount(amount);
            accountTransaction.setDebitAmount(0.0);
        }
        if (creditOrDebit.equals(AccountSettingCreditOrDebit.DEBIT)) {
            accountTransaction.setCreditAmount(0.0);
            accountTransaction.setDebitAmount(amount);
        }
        accountTransaction.setTransaction(transaction);
        accountTransaction.setTransactionType(transactionType);
        accountTransaction.setType(type);
      
        accountTransaction.setStatus(AccountTransactionStatus.ACTIVE);
   
        

        return accountTransaction;
    }

    private void checkAccountTransactionQueue() {
        if (accountTransactionQueues == null) {
            throw new ApplicationRuntimeException("Account Transaction Queue is not begun");
        }
    }

    public void saveAccountTransaction(
            HibernateDatabaseService databaseService,
            int transaction,
            String transactionType,
            String type,
            String creditOrDebit,
            double amount,
            String accountCode,
            String description,
            int loan) throws DatabaseException {


        Account account = (Account) databaseService.getObject(Account.class, accountCode);

        AccountTransaction accountTransaction;
        accountTransaction = createAccountTransaction(
                transaction,
                transactionType,
                type,
                creditOrDebit,
                amount,
                account,
                description,
                null);

        databaseService.save(accountTransaction);
    }
    public void saveAccountTransactionChque(
            HibernateDatabaseService databaseService,
            int transaction,
            String transactionType,
            String type,
            String creditOrDebit,
            double amount,
            String accountCode,
            String description,
            int loan,
            Date accountDate) throws DatabaseException {
             sERTranseDate = new  SERTranseDate();
             
            sERTranseDate.setTransaction_date(accountDate);
        Account account = (Account) databaseService.getObject(Account.class, accountCode);

        AccountTransaction accountTransaction;
        accountTransaction = createAccountTransaction(
                transaction,
                transactionType,
                type,
                creditOrDebit,
                amount,
                account,
                description,
                null);

        databaseService.save(accountTransaction);
    }
    public void saveAccountTransactionVoucher(
            HibernateDatabaseService databaseService,
            int transaction,
            String transactionType,
            String type,
            String creditOrDebit,
            double amount,
            String accountCode,
            String description,
            int loan,
            Date accountDate) throws DatabaseException {
             sERTranseDate = new  SERTranseDate();
             
            sERTranseDate.setTransaction_date(accountDate);
        Account account = (Account) databaseService.getObject(Account.class, accountCode);

        GeneralVoucherDet accountTransaction;
        accountTransaction = createAccountTransactionVoucher(
                transaction,
                transactionType,
                type,
                creditOrDebit,
                amount,
                account,
                description,
                null);

        databaseService.save(accountTransaction);
        
      
        
        
        
    }

    private List<AccountSetting> getAccountSettings(HibernateDatabaseService databaseService, String transactionType) throws DatabaseException {
        String hql = "FROM com.mac.zsystem.transaction.account.object.AccountSetting WHERE transactionType.code=:TRANSACTION_TYPE";
        HashMap<String, Object> params = new HashMap<>();
        params.put("TRANSACTION_TYPE", transactionType);

        List<AccountSetting> accountSettings = databaseService.getCollection(hql, params);

        if (accountSettings.isEmpty()) {
            throw new ApplicationRuntimeException("Account Interface not found for transaction type " + transactionType);
        }

        return accountSettings;
    }
    private List<AccountTransactionQueue> accountTransactionQueues;
    private SERTranseDate sERTranseDate;
}
