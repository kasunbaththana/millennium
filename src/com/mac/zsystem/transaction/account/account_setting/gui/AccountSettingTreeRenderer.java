/*
 *  AccountSettingTreeRenderer.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 3:59:26 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.gui;

import com.mac.af.component.renderer.tree.CTreeRenderer;
import com.mac.zresources.FinacResources;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.account.object.TransactionType;
import javax.swing.Icon;
import javax.swing.JTree;

/**
 *
 * @author mohan
 */
public class AccountSettingTreeRenderer extends CTreeRenderer {

    @Override
    public String getText(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        if (value instanceof TransactionType) {
            return "<HTML><B><H4><FONT FACE='Verdana'>"+ ((TransactionType) value).getName() + "</FONT></H4<></HTML></B>";
        } else if (value instanceof AccountSetting) {
            return ((AccountSetting) value).getName();
        } else {
            return value != null ? value.toString() : null;
        }
    }

    @Override
    public Icon getIcon(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        if (value instanceof TransactionType) {
            return TRANSACTION_ICON;
        } else {
            return ACCOUNT_SETTING_ICON;
        }
    }
    private static final Icon TRANSACTION_ICON = FinacResources.getImageIcon(FinacResources.TRANSACTION, 24, 24);
    private static final Icon ACCOUNT_SETTING_ICON = FinacResources.getImageIcon(FinacResources.ACCOUNTT_SETTING, 24, 24);
}
