/*
 *  SystemAccountSettings.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 3:37:13 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.gui;

import com.mac.af.component.model.tree.CTreeModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.tree_registration.AbstractTreeRegistration;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.account.object.TransactionType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.tree.TreeCellRenderer;

/**
 *
 * @author mohan
 */
public class SystemAccountSettings extends AbstractTreeRegistration {

    @Override
    protected Map<Class, AbstractObjectCreator> getObjectCreatorMap() {
        initService();

        Map<Class, AbstractObjectCreator> objectCreators = new HashMap<>();

        objectCreators.put(AccountSetting.class, new PCAccountSetting(serAccountSetting));
        objectCreators.put(TransactionType.class, new PCTransactionType());

        return objectCreators;
    }

    @Override
    protected CTreeModel getTreeModel() {
        return new AccountSettingTreeModel();
    }

    @Override
    protected TreeCellRenderer getTreeRenderer() {
        return new AccountSettingTreeRenderer();
    }

    @Override
    protected List getTreeData() {
        initService();

        return serAccountSetting.getTransactionTypes();
    }

    @Override
    protected Class getChildClass(Class cls) {
        if (cls.equals(TransactionType.class)) {
            return AccountSetting.class;
        } else {
            return null;
        }
    }

    @Override
    protected Class getParentClass(Class cls) {
        if (cls.equals(AccountSetting.class)) {
            return TransactionType.class;
        } else {
            return null;
        }
    }

    @Override
    protected boolean isAllowNew(Class cls) {
        return false;
    }

    @Override
    protected void save(Object currentObject, Object saveObject) throws DatabaseException {
        if (currentObject instanceof AccountSetting) {
            serAccountSetting.saveAccountSetting((AccountSetting) currentObject);
        }
    }

    private void initService() {
        if (serAccountSetting == null) {
            serAccountSetting = new SERAccountSetting(this);
        }
    }
    private SERAccountSetting serAccountSetting;
}
