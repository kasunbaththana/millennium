/*
 *  SERAccountSetting.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 4:04:22 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.gui;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zsystem.transaction.account.object.Account;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.account.object.TransactionType;
import java.awt.Component;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class SERAccountSetting extends AbstractService {

    public SERAccountSetting(Component component) {
        super(component);
    }

    public List<TransactionType> getTransactionTypes() {
        try {
            return getDatabaseService().getCollection(TransactionType.class);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAccountSetting.class.getName()).log(Level.SEVERE, null, ex);
            return Arrays.asList();
        }
    }

    public List<Account> getAccounts() {
        try {
            return getDatabaseService().getCollection(Account.class);
        } catch (DatabaseException ex) {
            Logger.getLogger(SERAccountSetting.class.getName()).log(Level.SEVERE, null, ex);
            return Arrays.asList();
        }
    }
    
    public void saveAccountSetting(AccountSetting accountSetting) throws DatabaseException {
        getDatabaseService().save(accountSetting);
    }

}
