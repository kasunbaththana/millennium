/*
 *  AccountTransactionStatus.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 12:24:03 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting;

/**
 *
 * @author mohan
 */
public class AccountTransactionStatus {
    public static final String ACTIVE = "ACTIVE";
    public static final String CANCEL = "CANCEL";
}
