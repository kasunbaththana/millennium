/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class ChequeIssueReturnAccountInterface
        extends AccountInterface {

    public static final String ISSUE_RETURN_CREDIT_CODE = "ISSUE_RETURN_CREDIT";
   

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.CHEQUE_ISSUE_RETURN_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(ISSUE_RETURN_CREDIT_CODE, "Cheque Issue Return Credit", AccountSettingCreditOrDebit.CREDIT));
    }
}
