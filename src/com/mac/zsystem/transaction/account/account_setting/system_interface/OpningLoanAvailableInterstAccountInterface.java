/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class OpningLoanAvailableInterstAccountInterface
        extends AccountInterface {

    public static final String OPNING_LOAN_AVAILABLE_INTERST_CREDIT_CODE = "OP_LN_AV_INTERST_AMOUNT_CREDIT";
    public static final String OPNING_LOAN_AVAILABLE_INTERST_DEBIT_CODE = "OP_LN_AV_INTERST_AMOUNT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.OPNING_LOAN_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(OPNING_LOAN_AVAILABLE_INTERST_CREDIT_CODE, "Opning Loan Available Interst Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(OPNING_LOAN_AVAILABLE_INTERST_DEBIT_CODE, "Opning Loan Available Interst Amount Debit", AccountSettingCreditOrDebit.DEBIT));
    }
}