/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class LoanApprovalAccountInterface
        extends AccountInterface {

    //APPROVE
    public static final String APPROVAL_AMOUNT_CREDIT_CODE = "APPROVAL_AMOUNT_CREDIT";
    public static final String APPROVAL_AMOUNT_DEBIT_CODE = "APPROVAL_AMOUNT_DEBIT";
    //SUSPEND
    public static final String SUSPEND_AMOUNT_CREDIT_CODE = "SUSPEND_AMOUNT_CREDIT";
    public static final String SUSPEND_AMOUNT_DEBIT_CODE = "SUSPEND_AMOUNT_DEBIT";
    //REJECT
    public static final String REJECT_AMOUNT_CREDIT_CODE = "REJECT_AMOUNT_CREDIT";
    public static final String REJECT_AMOUNT_DEBIT_CODE = "REJECT_AMOUNT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.LOAN_APPROVAL_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                //APPROVAL
                newAccountSetting(APPROVAL_AMOUNT_CREDIT_CODE, "Approval Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(APPROVAL_AMOUNT_DEBIT_CODE, "Approval Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                //SUSPEND
                newAccountSetting(SUSPEND_AMOUNT_CREDIT_CODE, "Suspend Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(SUSPEND_AMOUNT_DEBIT_CODE, "Suspend Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                //REJECT
                newAccountSetting(REJECT_AMOUNT_CREDIT_CODE, "Reject Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(REJECT_AMOUNT_DEBIT_CODE, "Reject Amount Debit", AccountSettingCreditOrDebit.DEBIT));
    }
}
