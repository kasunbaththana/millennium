/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class InsuranceAccountInterface   
extends AccountInterface {

    public static final String INSURANCE_AMOUNT_DEBIT_CODE = "INSURANCE_AMOUNT_DEBIT";
    public static final String INSURANCE_AMOUNT_CREDIT_CODE = "INSURANCE_AMOUNT_CREDIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.INSURANCE_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(INSURANCE_AMOUNT_DEBIT_CODE, "Insurance Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(INSURANCE_AMOUNT_CREDIT_CODE, "Insurance Amount Credit", AccountSettingCreditOrDebit.CREDIT)
                );
    }
}
