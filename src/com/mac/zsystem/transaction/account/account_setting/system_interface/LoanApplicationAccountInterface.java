/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class LoanApplicationAccountInterface 
extends AccountInterface 
{

    public static final String APPLICATION_AMOUNT_CREDIT_CODE = "APPLICATION_AMOUNT_CREDIT";
    public static final String APPLICATION_AMOUNT_DEBIT_CODE = "APPLICATION_AMOUNT_DEBIT";
    public static final String DEFAULT_CHARG_DEBIT = "DEFAULT_CHARG_DEBIT";
    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.LOAN_APPLICATION_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(APPLICATION_AMOUNT_CREDIT_CODE, "Loan Application Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(APPLICATION_AMOUNT_DEBIT_CODE, "Loan Application Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(DEFAULT_CHARG_DEBIT, "Default Charg Amount Debit", AccountSettingCreditOrDebit.DEBIT)
                );
    }
}
