
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author kasun
 */
public class WriteOffAccountInterface
        extends AccountInterface {

    public static final String WRITE_OFF_CREDIT_CODE = "WRITE_OFF_CREDIT";
    public static final String WRITE_OFF_DEBIT_CODE = "WRITE_OFF_DEBIT";
    public static final String WRITE_OFF_PROVISION = "WRITE_OFF_PROVISION";


    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.WRITE_OFF_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(WRITE_OFF_CREDIT_CODE, "Write credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(WRITE_OFF_DEBIT_CODE, "Write debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(WRITE_OFF_PROVISION, "Write Provision credit", AccountSettingCreditOrDebit.CREDIT));
    }
}
