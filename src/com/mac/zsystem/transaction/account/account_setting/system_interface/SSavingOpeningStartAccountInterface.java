/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class SSavingOpeningStartAccountInterface extends AccountInterface {



    public static final String SAVING_OP_CAPITAL_CREDIT_CODE = "SAVING_OP_CAPITAL_CREDIT";
    public static final String SAVING_OP_INTEREST_CREDIT_CODE = "SAVING_OP_INTEREST_CREDIT";
    public static final String SAVING_OP_CA_IN_DEBIT_CODE = "SAVING_OP_CA_IN_DEBIT";
    
    public static final String SAVING_OP_AMOUNT_CREDIT_CODE = "SAVING_OP_AMOUNT_CREDIT";
    public static final String SAVING_OP_AMOUNT_DEBIT_CODE = "SAVING_OP_AMOUNT_DEBIT";
   

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.OP_SAVING_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(SAVING_OP_CAPITAL_CREDIT_CODE, "Opening Saving Start Capital Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(SAVING_OP_INTEREST_CREDIT_CODE, "Opening Saving Start Interest Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(SAVING_OP_CA_IN_DEBIT_CODE, "Opening Saving Start Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(SAVING_OP_AMOUNT_CREDIT_CODE, "Opening Saving Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(SAVING_OP_AMOUNT_DEBIT_CODE, "Opening Saving Amount Credit", AccountSettingCreditOrDebit.CREDIT)
                );
    }
}