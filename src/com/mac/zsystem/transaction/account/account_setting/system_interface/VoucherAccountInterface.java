/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class VoucherAccountInterface
        extends AccountInterface {

    public static final String VOUCHER_AMOUNT_DEBIT_CODE = "VOUCHER_AMOUNT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.VOUCHER_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(VOUCHER_AMOUNT_DEBIT_CODE, "Voucher Amount Debit", AccountSettingCreditOrDebit.DEBIT));
    }
}
