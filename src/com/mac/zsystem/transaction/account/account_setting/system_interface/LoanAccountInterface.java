/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class LoanAccountInterface 
extends AccountInterface 
{

    public static final String LOAN_AMOUNT_CREDIT_CODE = "LOAN_AMOUNT_CREDIT";
    public static final String LOAN_INTEREST_CREDIT_CODE = "LOAN_INEREST_CREDIT";
    public static final String LOAN_AMOUNT_DO_CREDIT_CODE = "LOAN_AMOUNT_DO_CREDIT";
    public static final String LOAN_AMOUNT_DEBIT_CODE = "LOAN_AMOUNT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.LOAN_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(LOAN_AMOUNT_CREDIT_CODE, "Loan Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(LOAN_INTEREST_CREDIT_CODE, "Loan Interest Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(LOAN_AMOUNT_DO_CREDIT_CODE, "Do Loan Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(LOAN_AMOUNT_DEBIT_CODE, "Loan Amount Debit", AccountSettingCreditOrDebit.DEBIT)
                );
    }
}
