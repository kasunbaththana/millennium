
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class LoanValuationAccountInterface
        extends AccountInterface {

    public static final String CREDIT_SETTLEMENT_CREDIT_CODE = "VALUATION_SETTLE_CREDIT";
    
    public static final String CREDIT_SETTLEMENT_DEBIT_CODE = "VALUATION_SETTLE_DEBIT";
    
    
    public static final String CREDIT_SETTLEMENT_PROFIT_CREDIT = "VALUATION_SETTLE_PROFIT_CREDIT";
    public static final String CREDIT_SETTLEMENT_LOSS_DEBIT = "VALUATION_SETTLE_LOSS_DEBIT";
    
    
    
    public static final String DEBIT_SEZED_CLOSE = "DEBIT_SEZED_CLOSE";
    public static final String DEBIT_SEZED_CLOSE_CAPITAL = "DEBIT_SEZED_CLOSE_CAPITAL";
    public static final String DEBIT_SEZED_CLOSE_INTEREST = "DEBIT_SEZED_CLOSE_INTEREST";
    public static final String DEBIT_SEZED_CLOSE_OTHER_CHARGE = "DEBIT_SEZED_CLOSE_OTHER_CHARGE";
    
    
   
    
    

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.LOAN_VALUATION_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(CREDIT_SETTLEMENT_CREDIT_CODE, "Credit Valuation credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(CREDIT_SETTLEMENT_DEBIT_CODE, "Credit Valuation debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(CREDIT_SETTLEMENT_PROFIT_CREDIT, "Profit Valuation", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(CREDIT_SETTLEMENT_LOSS_DEBIT, "Loss Valuation", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(DEBIT_SEZED_CLOSE, "Sezed Close", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(DEBIT_SEZED_CLOSE_CAPITAL, "Sezed Close capital", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(DEBIT_SEZED_CLOSE_INTEREST, "Sezed Close Interest", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(DEBIT_SEZED_CLOSE_OTHER_CHARGE, "Sezed Close OtherCharg", AccountSettingCreditOrDebit.CREDIT)
                
                );
    }
}
