/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class SupplierPaymentAccountInterface 
   extends AccountInterface {

    public static final String SUPPLIER_PAYMENT_DEBIT_CODE = "SUPPLIER_PAYMENT_DEBIT";
    public static final String SUPPLIER_DOWNPAYMENT_DEBIT_CODE = "DOWNPAYMENT_PAYMENT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.SUPPLIER_PAYMENT;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(SUPPLIER_PAYMENT_DEBIT_CODE, "Supplier Payment Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(SUPPLIER_DOWNPAYMENT_DEBIT_CODE, "DownPayment  Amount Debit", AccountSettingCreditOrDebit.DEBIT));
    }
}
