/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class SeizedChargeAccountInterface
        extends AccountInterface {

    public static final String SEIZED_CHARGE_AMOUNT_CREDIT_CODE = "SEIZED_CHARGE_CREDIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.SEIZED_CHARGE_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(SEIZED_CHARGE_AMOUNT_CREDIT_CODE, "Seized charge Amount Credit", AccountSettingCreditOrDebit.CREDIT)
                );
    }
}
