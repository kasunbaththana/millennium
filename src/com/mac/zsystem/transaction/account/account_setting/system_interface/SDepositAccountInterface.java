/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class SDepositAccountInterface extends AccountInterface {



//    public static final String SAVING_DEPOSIT_AMOUNT_DEBIT_CODE = "SAVING_DEPOSIT_AMOUNT_DEBIT";
    public static final String SAVING_DEPOSIT_AMOUNT_CREDIT_CODE = "SAVING_DEPOSIT_AMOUNT_CREDIT";
    
    public static final String SAVING_DEPOSIT_DUE_DEBIT_CODE = "SAVING_DEPOSIT_DUE_DEBIT";
    public static final String SAVING_DEPOSIT_DUE_CREDIT_CODE = "SAVING_DEPOSIT_DUE_CREDIT";
   

    @Override
    public String getTransactionTypeCode() {
//        return SystemTransactions.REBIT_TRANSACTION_CODE;
        return SystemTransactions.SAVING_DEPOSIT_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
//                newAccountSetting(SAVING_DEPOSIT_AMOUNT_DEBIT_CODE, "Creditor Deposit Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(SAVING_DEPOSIT_AMOUNT_CREDIT_CODE, "Creditor Deposit Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(SAVING_DEPOSIT_DUE_DEBIT_CODE, "Creditor Deposit Due Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(SAVING_DEPOSIT_DUE_CREDIT_CODE, "Creditor Deposit Due Credit", AccountSettingCreditOrDebit.CREDIT)
                
                );
    }
}