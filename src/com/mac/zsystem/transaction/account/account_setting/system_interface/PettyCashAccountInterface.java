/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author KASUN
 */
public class PettyCashAccountInterface  extends AccountInterface {

    public static final String PETTY_CASH_CREDIT_CODE = "PETTY_CASH_CREDIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.GENERAL_VOUCHER_2_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(PETTY_CASH_CREDIT_CODE, "Petty Cash Amount Credit", AccountSettingCreditOrDebit.CREDIT));
    }
}
