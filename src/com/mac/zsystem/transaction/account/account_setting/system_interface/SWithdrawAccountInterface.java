/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class SWithdrawAccountInterface extends AccountInterface {



    public static final String SAVING_WITHDRAW_AMOUNT_DEBIT_CODE = "SAVING_WITHDRAW_AMOUNT_DEBIT";
//    public static final String SAVING_WITHDRAW_AMOUNT_CREDIT_CODE = "SAVING_WITHDRAW_AMOUNT_CREDIT";
    
    public static final String SAVING_WITHDRAW_INTEREST_CREDIT_CODE = "SAVING_WITHDRAW_INTEREST_CREDIT";
    public static final String SAVING_WITHDRAW_INTEREST_DEBIT_CODE = "SAVING_WITHDRAW_INTEREST_DEBIT";
//    public static final String SAVING_WITHDRAW_CAPITAL_CREDIT_CODE = "SAVING_WITHDRAW_CAPITAL_CREDIT";
//    public static final String SAVING_WITHDRAW_CAPITAL_DEBIT_CODE = "SAVING_WITHDRAW_CAPITAL_DEBIT";
   

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.SAVING_WITHDRAW_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(SAVING_WITHDRAW_AMOUNT_DEBIT_CODE, "Saving Withdraw Amount Debit", AccountSettingCreditOrDebit.DEBIT),
//                newAccountSetting(SAVING_WITHDRAW_AMOUNT_CREDIT_CODE, "Saving Withdraw Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(SAVING_WITHDRAW_INTEREST_CREDIT_CODE, "Saving Withdraw Interest Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(SAVING_WITHDRAW_INTEREST_DEBIT_CODE, "Saving Withdraw Interest Debit", AccountSettingCreditOrDebit.DEBIT)
//                newAccountSetting(SAVING_WITHDRAW_CAPITAL_DEBIT_CODE, "Saving Withdraw Capital Debit", AccountSettingCreditOrDebit.DEBIT),
//                newAccountSetting(SAVING_WITHDRAW_CAPITAL_CREDIT_CODE, "Saving Withdraw Capital Credit", AccountSettingCreditOrDebit.CREDIT)
                );
    }
}