/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class ChequeDepositAccountInterface
        extends AccountInterface {

    public static final String DEPOSIT_AMOUNT_CREDIT_CODE = "DEPOSIT_AMOUNT_CREDIT";
//    public static final String DEPOSIT_AMOUNT_DEBIT_CODE = "DEPOSIT_AMOUNT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.CHEQUE_DEPOSIT_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(DEPOSIT_AMOUNT_CREDIT_CODE, "Cheque Deposit Amount Credit", AccountSettingCreditOrDebit.CREDIT));
//                newAccountSetting(DEPOSIT_AMOUNT_DEBIT_CODE, "Cheque Deposit Amount Debit", AccountSettingCreditOrDebit.DEBIT));
    }
}
