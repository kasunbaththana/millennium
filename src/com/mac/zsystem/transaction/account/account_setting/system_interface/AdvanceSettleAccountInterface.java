/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class AdvanceSettleAccountInterface
        extends AccountInterface {

    public static final String ADVANCE_SETTLE_AMOUNT_CREDIT_CODE = "ADVANCE_SETTLE_AMOUNT_CREDIT";
    public static final String ADVANCE_SETTLE_AMOUNT_DEBIT_CODE = "ADVANCE_SETTLE_AMOUNT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.ADVANCE_SETTLEMENT_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(ADVANCE_SETTLE_AMOUNT_CREDIT_CODE, "Advance Settle Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(ADVANCE_SETTLE_AMOUNT_DEBIT_CODE, "Advance Settle Amount Debit", AccountSettingCreditOrDebit.DEBIT)
               );
    }
}
