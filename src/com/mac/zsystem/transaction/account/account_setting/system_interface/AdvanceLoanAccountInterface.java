/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class AdvanceLoanAccountInterface 
extends AccountInterface 
{

    public static final String LOAN_AMOUNT_CREDIT_CODE = "ADVANCE_LOAN_AMOUNT_CREDIT";
    public static final String INTEREST_AMOUNT_CREDIT_CODE = "ADVANCE_INTEREST_AMOUNT_CREDIT";
    public static final String LOAN_AMOUNT_DEBIT_CODE = "ADVANCE_LOAN_AMOUNT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.ADVANCE_LOAN_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(LOAN_AMOUNT_CREDIT_CODE, "Loan Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(INTEREST_AMOUNT_CREDIT_CODE, "Loan Interest Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(LOAN_AMOUNT_DEBIT_CODE, "Loan Amount Debit", AccountSettingCreditOrDebit.DEBIT)
                );
    }
}
