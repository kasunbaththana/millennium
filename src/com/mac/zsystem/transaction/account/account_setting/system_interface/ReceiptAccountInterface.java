/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class ReceiptAccountInterface
        extends AccountInterface {

    public static final String RECEIPT_AMOUNT_CREDIT_CODE = "RECEIPT_AMOUNT_CREDIT";
    //public static final String RECEIPT_INTREST_CREDIT_CODE = "RECEIPT_INTREST_CREDIT";
    //public static final String RECEIPT_ADVANCED_CREDIT_CODE = "RECEIPT_ADVANCED_CREDIT";
    public static final String RECEIPT_PANELTY_CREDIT_CODE = "RECEIPT_PANELTY_CREDIT";
    public static final String RECEIPT_INT_PNL_DEBIT_CODE = "RECEIPT_INT_PNL_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.RECEIPT_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(RECEIPT_AMOUNT_CREDIT_CODE, "Receipt Amount Credit", AccountSettingCreditOrDebit.CREDIT),
              //  newAccountSetting(RECEIPT_INTREST_CREDIT_CODE, "DayEnd Interst Credit", AccountSettingCreditOrDebit.CREDIT),
              //  newAccountSetting(RECEIPT_ADVANCED_CREDIT_CODE, "Receipt Advanced Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(RECEIPT_PANELTY_CREDIT_CODE, "DayEnd Panelty Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(RECEIPT_INT_PNL_DEBIT_CODE, "DayEnd int_pnl Credit", AccountSettingCreditOrDebit.DEBIT)
                );
    }
}
