/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class SalesInvoiceAccountInterface extends AccountInterface {

    public static final String SALES_AMOUNT_CREDIT = "SALES_AMOUNT_CREDIT";
    public static final String SALES_AMOUNT_DEBIT = "SALES_AMOUNT_DEBIT";
    
    public static final String CHARGES_AMOUNT_DEBIT = "CHARGES_AMOUNT_DEBIT";
    public static final String DOWN_AMOUNT_DEBIT = "DOWN_AMOUNT_CREDIT";

    @Override
    public String getTransactionTypeCode() {

        return SystemTransactions.SALES_ITEM_TRANSACTION_CODE;
    }

    @Override
    protected List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(SALES_AMOUNT_CREDIT, "Sales amount credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(SALES_AMOUNT_DEBIT, "Sales amount debit", AccountSettingCreditOrDebit.DEBIT),
                
                newAccountSetting(CHARGES_AMOUNT_DEBIT, "Total charges debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(DOWN_AMOUNT_DEBIT, "Down payment debit", AccountSettingCreditOrDebit.DEBIT));
    }
}
