/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class OpningLoanAvailablePanaltyAccountInterface 
       extends AccountInterface {

    public static final String OPNING_LOAN_AVAILABLE_PANALTY_CREDIT_CODE = "OP_LN_AV_PANALTY_AMOUNT_CREDIT";
    public static final String OPNING_LOAN_AVAILABLE_PANALTY_DEBIT_CODE = "OP_LN_AV_PANALTY_AMOUNT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.OPNING_LOAN_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(OPNING_LOAN_AVAILABLE_PANALTY_CREDIT_CODE, "Opning Loan Available Panalty Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(OPNING_LOAN_AVAILABLE_PANALTY_DEBIT_CODE, "Opning Loan Available Panalty Amount Debit", AccountSettingCreditOrDebit.DEBIT));
    }
}