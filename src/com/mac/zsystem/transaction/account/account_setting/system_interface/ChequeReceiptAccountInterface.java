/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class ChequeReceiptAccountInterface 
    extends AccountInterface {

    public static final String CHEQUE_RECEIPT_AMOUNT_CREDIT_CODE = "CHEQUE_RECEIPT_CREDIT";
    public static final String CHEQUE_RECEIPT_AMOUNT_DEBIT_CODE = "CHEQUE_RECEIPT_DEBIT";
   // public static final String RECEIPT_INTREST_CREDIT_CODE = "CHEQUE_INTREST_CREDIT";
    public static final String RECEIPT_PANELTY_CREDIT_CODE = "CHEQUE_PANELTY_CREDIT";
    public static final String RECEIPT_INT_PNL_DEBIT_CODE = "CHEQUE_INT_PNL_DEBIT";
    public static final String RECEIPT_ADVANCED_CREDIT_CODE = "CHEQUE_ADVANCED_CREDIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.CHEQUE_RECEIPT_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(CHEQUE_RECEIPT_AMOUNT_CREDIT_CODE, "Cheque Receipt Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(CHEQUE_RECEIPT_AMOUNT_DEBIT_CODE, "Cheque Receipt Amount Debit", AccountSettingCreditOrDebit.DEBIT),
             //   newAccountSetting(RECEIPT_INTREST_CREDIT_CODE, "Cheque Interest Amount Debit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(RECEIPT_PANELTY_CREDIT_CODE, "Cheque Panelty Amount Debit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(RECEIPT_INT_PNL_DEBIT_CODE, "Cheque Int Pnl Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(RECEIPT_ADVANCED_CREDIT_CODE, "Cheque Receipt Advanced Amount Credit", AccountSettingCreditOrDebit.CREDIT)
                
                );
    }
}