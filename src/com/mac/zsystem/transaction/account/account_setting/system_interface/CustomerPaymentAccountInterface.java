/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class CustomerPaymentAccountInterface 
        extends AccountInterface {

    public static final String CUSTOMER_PAYMENT_AMOUNT_CREDIT_CODE = "CUS_PYMT_AMOUNT_CREDIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.CUSTOMER_PAYMENT;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(CUSTOMER_PAYMENT_AMOUNT_CREDIT_CODE, "Customer payment Amount Credit", AccountSettingCreditOrDebit.CREDIT));
    }
}
