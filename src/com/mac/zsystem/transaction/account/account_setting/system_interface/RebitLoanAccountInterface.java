/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class RebitLoanAccountInterface extends AccountInterface {

    public static final String REBIT_AMOUNT_CREDIT_CODE = "REBIT_AMOUNT_CREDIT";
    //
    public static final String REBIT_PANALTY_AMOUNT_DEBIT_CODE = "REBIT_PANALTY_DEBIT";
    public static final String REBIT_OTHER_CHARGE_AMOUNT_DEBIT_CODE = "REBIT_OTHRCHG_DEBIT";
    public static final String REBIT_INTEREST_AMOUNT_DEBIT_CODE = "REBIT_INTEREST_DEBIT";
    public static final String REBIT_INSURANCE_AMOUNT_DEBIT_CODE = "REBIT_INSURANCE_DEBIT";
    public static final String LOAN_CAPITAL_AMOUNT_DEBIT_CODE = "REBIT_CAPITAL_DEBIT";

    @Override
    public String getTransactionTypeCode() {
//        return SystemTransactions.REBIT_TRANSACTION_CODE;
        return SystemTransactions.REBIT_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(REBIT_AMOUNT_CREDIT_CODE, "Rebit Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(REBIT_PANALTY_AMOUNT_DEBIT_CODE, "Panalty Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(REBIT_OTHER_CHARGE_AMOUNT_DEBIT_CODE, "OtherCharg Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(REBIT_INTEREST_AMOUNT_DEBIT_CODE, "Interst Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(REBIT_INSURANCE_AMOUNT_DEBIT_CODE, "insurance Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(LOAN_CAPITAL_AMOUNT_DEBIT_CODE, "Loan Capital Amount Debit", AccountSettingCreditOrDebit.DEBIT)
                );
    }
}