/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class LoanCloseAccountInterface
        extends AccountInterface {

    public static final String CREDIT_SETTLEMENT_CREDIT_CODE = "CREDIT_SETTLE_CREDIT";
    public static final String CREDIT_SETTLEMENT_DEBIT_CODE = "CREDIT_SETTLE_DEBIT";
    public static final String DEBIT_SETTLEMENT_CREDIT_CODE = "DEBIT_SETTLE_CREDIT";
    public static final String DEBIT_SETTLEMENT_DEBIT_CODE = "DEBIT_SETTLE_DEBIT";
    
    public static final String SETTLEMENT_DUE_DEBIT_CODE = "SETTLEMENT_DUE_DEBIT";
    public static final String INTEREST_DUE_CREDIT_CODE = "INTEREST_DUE_CREDIT";
    public static final String CAPITAL_DUE_CREDIT_CODE = "CAPITAL_DUE_CREDIT";
    public static final String INSUARANCE_DUE_CREDIT_CODE = "INSUARANCE_DUE_CREDIT";
    
    public static final String FUND_SETTLEMENT_CREDIT_CODE = "FUND_SETTLEMENT_CREDIT";
    public static final String FUND_SETTLEMENT_DEBIT_CODE = "FUND_SETTLEMENT_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.LOAN_CLOSE_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(CREDIT_SETTLEMENT_CREDIT_CODE, "Credit settlement credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(CREDIT_SETTLEMENT_DEBIT_CODE, "Credit settlement debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(DEBIT_SETTLEMENT_CREDIT_CODE, "Debit settlement credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(DEBIT_SETTLEMENT_DEBIT_CODE, "Debit settlement debit", AccountSettingCreditOrDebit.DEBIT),
               
                newAccountSetting(SETTLEMENT_DUE_DEBIT_CODE, "Settlement debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(INTEREST_DUE_CREDIT_CODE, "Settlement Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(CAPITAL_DUE_CREDIT_CODE, "Settlement Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(INSUARANCE_DUE_CREDIT_CODE, "Settlement Credit", AccountSettingCreditOrDebit.CREDIT),
                
                newAccountSetting(FUND_SETTLEMENT_CREDIT_CODE, "Fund Settlement Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(FUND_SETTLEMENT_DEBIT_CODE, "Fund Settlement Credit", AccountSettingCreditOrDebit.DEBIT));
    }
}
