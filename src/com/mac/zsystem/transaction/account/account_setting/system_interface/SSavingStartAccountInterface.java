/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class SSavingStartAccountInterface extends AccountInterface {



    public static final String SAVING_CAPITAL_CREDIT_CODE = "SAVING_CAPITAL_CREDIT";
//    public static final String SAVING_INTEREST_CREDIT_CODE = "SAVING_INTEREST_CREDIT";
    public static final String SAVING_CAPITAL_DEBIT_CODE = "SAVING_CAPITAL_DEBIT";
    
    public static final String SAVING_AMOUNT_CREDIT_CODE = "SAVING_AMOUNT_CREDIT";
   

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.SAVING_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(SAVING_CAPITAL_CREDIT_CODE, "Creditor Start Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(SAVING_CAPITAL_DEBIT_CODE, "Creditor Start Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(SAVING_AMOUNT_CREDIT_CODE, "Creditor Amount Credit", AccountSettingCreditOrDebit.CREDIT)
                );
    }
}