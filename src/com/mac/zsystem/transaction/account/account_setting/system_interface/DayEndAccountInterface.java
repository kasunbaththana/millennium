/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class DayEndAccountInterface
        extends AccountInterface {

    public static final String PANALTY_AMOUNT_CREDIT_CODE = "PANALTY_AMOUNT_CREDIT";
    public static final String PANALTY_AMOUNT_DEBIT_CODE = "PANALTY_AMOUNT_DEBIT";
    //
    public static final String LOAN_CAPITAL_CREDIT_CODE = "LOAN_CAPITAL_CREDIT";
    public static final String LOAN_CAPITAL_DEBIT_CODE = "LOAN_CAPITAL_DEBIT";
    //
    public static final String LOAN_INTEREST_CREDIT_CODE = "LOAN_INTEREST_CREDIT";
    public static final String LOAN_INTEREST_DEBIT_CODE = "LOAN_INTEREST_DEBIT";
    //
    public static final String LOAN_INSURANCE_CREDIT_CODE = "LOAN_INSURANCE_CREDIT";
    public static final String LOAN_INSURANCE_DEBIT_CODE = "LOAN_INSURANCE_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.DAY_END_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(PANALTY_AMOUNT_CREDIT_CODE, "Panalty Amount Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(PANALTY_AMOUNT_DEBIT_CODE, "Panalty Amount Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(LOAN_CAPITAL_CREDIT_CODE, "Loan Capital Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(LOAN_CAPITAL_DEBIT_CODE, "Loan Capital Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(LOAN_INTEREST_CREDIT_CODE, "Loan Interest Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(LOAN_INTEREST_DEBIT_CODE, "Loan Interest Debit", AccountSettingCreditOrDebit.DEBIT),
                newAccountSetting(LOAN_INSURANCE_CREDIT_CODE, "Loan Insurance Credit", AccountSettingCreditOrDebit.CREDIT),
                newAccountSetting(LOAN_INSURANCE_DEBIT_CODE, "Loan Insurance Debit", AccountSettingCreditOrDebit.DEBIT)
                
                );
    }
}
