/*
 *  AccountInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:24:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.account.account_setting.system_interface;

import com.mac.zsystem.transaction.account.account_setting.AccountInterface;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.object.AccountSetting;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class OtherChargeAccountInterface
        extends AccountInterface {

    public static final String OTHER_CHARGE_AMOUNT_DEBIT_CODE = "OTHER_CHARGE_DEBIT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.OTHER_CHARGE_TRANSACTION_CODE;
    }

    @Override
    public List<AccountSetting> getNewAccountSettings() {
        return Arrays.asList(
                newAccountSetting(OTHER_CHARGE_AMOUNT_DEBIT_CODE, "Othercharge Amount Debit", AccountSettingCreditOrDebit.DEBIT)
                );
    }
}
