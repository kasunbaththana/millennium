package com.mac.zsystem.transaction.transaction_cancel.object;

/**
  *	@author Channa Mohan
  *	
  *	Created On Jan 5, 2018 8:38:43 AM 
  *	Mohan Hibernate Mapping Generator
  */


import java.util.Date;

/**
 * AdvancedPayment generated by hbm2java
 */
public class AdvancedPayment  implements java.io.Serializable {


     private Integer indexNo;
     private Date transactionDate;
     private String branch;
     private String client;
     private Integer loan;
     private Integer installmentNo;
     private Integer transaction;
     private String transactionType;
     private String description;
     private Double amount;
     private Double balanceAmount;
     private Date dueDate;
     private String settlementType;
     private String account;
     private Boolean send;
     private String status;

    public AdvancedPayment() {
    }

	
    public AdvancedPayment(Date transactionDate, String branch, String status) {
        this.transactionDate = transactionDate;
        this.branch = branch;
        this.status = status;
    }
    public AdvancedPayment(Date transactionDate, String branch, String client, Integer loan, Integer installmentNo, Integer transaction, String transactionType, String description, Double amount, Double balanceAmount, Date dueDate, String settlementType, String account, Boolean send, String status) {
       this.transactionDate = transactionDate;
       this.branch = branch;
       this.client = client;
       this.loan = loan;
       this.installmentNo = installmentNo;
       this.transaction = transaction;
       this.transactionType = transactionType;
       this.description = description;
       this.amount = amount;
       this.balanceAmount = balanceAmount;
       this.dueDate = dueDate;
       this.settlementType = settlementType;
       this.account = account;
       this.send = send;
       this.status = status;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public Date getTransactionDate() {
        return this.transactionDate;
    }
    
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
    public String getBranch() {
        return this.branch;
    }
    
    public void setBranch(String branch) {
        this.branch = branch;
    }
    public String getClient() {
        return this.client;
    }
    
    public void setClient(String client) {
        this.client = client;
    }
    public Integer getLoan() {
        return this.loan;
    }
    
    public void setLoan(Integer loan) {
        this.loan = loan;
    }
    public Integer getInstallmentNo() {
        return this.installmentNo;
    }
    
    public void setInstallmentNo(Integer installmentNo) {
        this.installmentNo = installmentNo;
    }
    public Integer getTransaction() {
        return this.transaction;
    }
    
    public void setTransaction(Integer transaction) {
        this.transaction = transaction;
    }
    public String getTransactionType() {
        return this.transactionType;
    }
    
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public Double getAmount() {
        return this.amount;
    }
    
    public void setAmount(Double amount) {
        this.amount = amount;
    }
    public Double getBalanceAmount() {
        return this.balanceAmount;
    }
    
    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
    public Date getDueDate() {
        return this.dueDate;
    }
    
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
    public String getSettlementType() {
        return this.settlementType;
    }
    
    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }
    public String getAccount() {
        return this.account;
    }
    
    public void setAccount(String account) {
        this.account = account;
    }
    public Boolean getSend() {
        return this.send;
    }
    
    public void setSend(Boolean send) {
        this.send = send;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }



@Override
public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof AdvancedPayment) ) return false;
		 AdvancedPayment castOther = ( AdvancedPayment ) other; 

	if(!java.util.Objects.equals(this.transactionDate, castOther.transactionDate)) {
            return false;
     }
	if(!java.util.Objects.equals(this.branch, castOther.branch)) {
            return false;
     }
	if(!java.util.Objects.equals(this.status, castOther.status)) {
            return false;
     }
         
		 return true;
   }

   @Override
   public int hashCode() {
         int result = 17;
         
	result = result * 17 + java.util.Objects.hashCode(this.transactionDate);
	result = result * 17 + java.util.Objects.hashCode(this.branch);
	result = result * 17 + java.util.Objects.hashCode(this.status);

         return result;
   }   

}


