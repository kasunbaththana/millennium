/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.transaction_cancel;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.loan.temperary_receipt.object.PaymentInformation;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.transaction.SystemTransactionStatus;
import com.mac.zsystem.transaction.transaction_cancel.object.Settlement;
import com.mac.zsystem.transaction.transaction_cancel.object.SettlementHistory;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Thilanga-pc
 */
public class SERTransactionCancel extends AbstractService {

    public SERTransactionCancel(Component component) {
        super(component);
    }
List<PaymentInformation> PaymentInformation;
    public void cancel(int indexNo) throws DatabaseException {
        //TRANSACTION
        cancelTransaction(indexNo);

        //SETTLEMENT
        cancelSettlement(indexNo);

        //SETTLEMENT HISTORY
        cancelSettlementHistory(indexNo);

        //ACCOUNT TRANSACTION
        cancelAccountTransaction(indexNo);

        //PAYMENT
        cancelPaymentTransaction(indexNo);
        
        //ADVANCED PAYMENT
        cancelAdvancedPament(indexNo);
    }
//kasun
    public void cancel_cheque_status(int indexNo) throws DatabaseException 
    {
        change_status(indexNo);
    }
    public void change_status(int indexNo) throws DatabaseException 
    {
       
       int transaction = getTransaction(indexNo);
        String hql = "UPDATE "
                + "com.mac.loan.temperary_receipt.object.Cheque "
                + "SET status=:STATUS "
                + "WHERE payment_object=:INDEX_NO";

        HashMap<String, Object> params = new HashMap<>();
        params.put("INDEX_NO", transaction);
        params.put("STATUS", SystemTransactionStatus.CANCEL);
        
        String hql2 = "UPDATE "
                + "com.mac.loan.temperary_receipt.object.TemperaryReceipt "
                + "SET status=:STATUS "
                + "WHERE transaction=:INDEX_NO";

        HashMap<String, Object> params2 = new HashMap<>();
        params2.put("INDEX_NO", indexNo);
        params2.put("STATUS", SystemTransactionStatus.CANCEL);

        getDatabaseService().executeUpdate(hql2, params2);
        getDatabaseService().executeUpdate(hql, params); 
        
    }
    public void change_status_chq(int indexNo) throws DatabaseException 
    {
       
       int transaction = getTransaction(indexNo);
        String hql = "UPDATE "
                + "com.mac.loan.temperary_receipt.object.Cheque "
                + "SET status=:STATUS "
                + "WHERE payment_object=:INDEX_NO";

        HashMap<String, Object> params = new HashMap<>();
        params.put("INDEX_NO", transaction);
        params.put("STATUS", SystemTransactionStatus.CANCEL);
        
       
        getDatabaseService().executeUpdate(hql, params); 
        
    }
    
    public int getTransaction(int indexNo)
    {
        int transaction_index = 0;
        List<PaymentInformation> list= getPaymentInformation(indexNo);
        
        for(PaymentInformation liastPaymentInformation:list)
        {
           transaction_index= liastPaymentInformation.getIndexNo();
        }
        
        return transaction_index;
    }
    
    
     public List<PaymentInformation> getPaymentInformation(int indexNo) {
        String hql = "FROM com.mac.loan.temperary_receipt.object.PaymentInformation WHERE transaction=:TRANSACTION";
        HashMap<String, Object> params = new HashMap<>();
            params.put("TRANSACTION", indexNo);

        
        try {
            PaymentInformation = getDatabaseService().getCollection(hql, params);
        } catch (DatabaseException ex) {
           PaymentInformation = new ArrayList<>();
        }

        return PaymentInformation;
    }
    
    public void cancelTransaction(int indexNo) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.Transaction "
                + "SET status=:STATUS "
                + "WHERE indexNo=:INDEX_NO";

        HashMap<String, Object> params = new HashMap<>();
        params.put("INDEX_NO", indexNo);
        params.put("STATUS", SystemTransactionStatus.CANCEL);

        getDatabaseService().executeUpdate(hql, params);
    }

//    Settelement Cancel
    public void cancelSettlement(int transaction) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.Settlement "
                + "SET status=:STATUS "
                + "WHERE transaction=:TRANSACTION";

        HashMap<String, Object> hashMapsettelment = new HashMap<>();
        hashMapsettelment.put("STATUS", SettlementStatus.SETTLEMENT_CANCEL);
        hashMapsettelment.put("TRANSACTION", transaction);

        getDatabaseService().executeUpdate(hql, hashMapsettelment);
    }
     public void cancelAdvancedPament(int transaction) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.AdvancedPayment "
                + "SET status=:STATUS "
                + "WHERE transaction=:TRANSACTION";
         System.out.println("transaction= "+transaction);
        HashMap<String, Object> hashMapAdvancedPayment = new HashMap<>();
        hashMapAdvancedPayment.put("STATUS", SettlementStatus.SETTLEMENT_CANCEL);
        hashMapAdvancedPayment.put("TRANSACTION", transaction);

        getDatabaseService().executeUpdate(hql, hashMapAdvancedPayment);
    }
   

//      cancel settelement history
    public void cancelSettlementHistory(int transaction) throws DatabaseException {
        String selectHQL = "FROM com.mac.zsystem.transaction.transaction_cancel.object.SettlementHistory WHERE transaction=:TRANSACTION";
        HashMap<String, Object> hashMapsettelment = new HashMap<>();
        hashMapsettelment.put("TRANSACTION", transaction);

        Settlement settlement;
        List<SettlementHistory> settlementHistories = getDatabaseService().getCollection(selectHQL, hashMapsettelment);
        for (SettlementHistory settlementHistory : settlementHistories) {
            settlementHistory.setStatus(SettlementStatus.SETTLEMENT_HISTORY_CANCEL);
            settlement = settlementHistory.getSettlement();

            settlement.setBalanceAmount(settlement.getBalanceAmount() + settlementHistory.getSettlementAmount());
            settlement.setStatus(SettlementStatus.SETTLEMENT_PENDING);
            
            getDatabaseService().save(settlementHistory);
            getDatabaseService().save(settlement);
        }
        
    }

//    cancel account transaction
    public void cancelAccountTransaction(int transaction) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.AccountTransaction "
                + "SET status=:STATUS "
                + "WHERE transaction=:TRANSACTION";

        HashMap<String, Object> hashMapsettelment = new HashMap<>();
        hashMapsettelment.put("STATUS", SettlementStatus.SETTLEMENT_CANCEL);
        hashMapsettelment.put("TRANSACTION", transaction);
        getDatabaseService().executeUpdate(hql, hashMapsettelment);

    }

    public void cancelPaymentTransaction(int transaction) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.Payment "
                + "SET status=:STATUS "
                + "WHERE transaction=:TRANSACTION";

        HashMap<String, Object> hashMapsettelment = new HashMap<>();
        hashMapsettelment.put("STATUS", SettlementStatus.SETTLEMENT_CANCEL);
        hashMapsettelment.put("TRANSACTION", transaction);

        getDatabaseService().executeUpdate(hql, hashMapsettelment);
    }
}