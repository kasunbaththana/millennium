/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.transaction_cancel;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.transaction_cancel.AbstractTransactionCancel;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.transaction.transaction.SystemTransactionStatus;
import com.mac.zsystem.transaction.transaction_cancel.object.Transaction;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Thilanga-pc
 */
public abstract class TransactionCancel extends AbstractTransactionCancel<Transaction> {

    protected abstract String getTransactionType();

    protected abstract void cancelAdditional(Transaction transaction) throws DatabaseException;

    
    @Override
    @Action
    public void doPrint() {
//        super.doPrint(); 
         initOthers();
         this.recentButton.doClick();
    }

    
    
    @Override
    protected AbstractObjectCreator getInternalObjectCreator() {
        return new PCTransactionCancel();
    }

    @Override
    public void cancelTransaction(Transaction t) throws DatabaseException {
        initService();
        
        cancelAdditional(t);

        serTransactionCancel.cancel(t.getIndexNo());
        
        if(getTransactionType().equals("TEMP_RECEIPT"))
        {
            serTransactionCancel.cancel_cheque_status(t.getIndexNo());
        }
        
    }

    @Override
    protected CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn("Index No", "indexNo"),
//                new CTableColumn("Agreement No.", "loan","agreementNo"),
                new CTableColumn("Agreement No", "agreementNo"),
                new CTableColumn("ReferenceNo", "referenceNo"),
                new CTableColumn("Document No", "documentNo"),
                new CTableColumn("Transaction Date", "transactionDate"),
                new CTableColumn("Transaction Type", "transactionType")
                
                );
    }

    @Override
    protected Collection getTableData() {
        List list;
        try {
            HashMap params = new HashMap<>();
            params.put("STATUS", SystemTransactionStatus.ACTIVE);
            params.put("TRANSACTION_TYPE", getTransactionType());
            list = getDatabaseService().getCollection("from com.mac.zsystem.transaction.transaction_cancel.object.Transaction where status=:STATUS AND transactionType=:TRANSACTION_TYPE", params);
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(TransactionCancel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    private void initService() {
        if (serTransactionCancel == null) {
            serTransactionCancel = new SERTransactionCancel(this);
        }
       
    }
    private SERTransactionCancel serTransactionCancel;
      private RecentButton recentButton;
    private void initOthers() {
        this.recentButton = new RecentButton();
        System.out.println("getTransactionType__"+getTransactionType());
        this.recentButton.setTransactionType(getTransactionType());
        this.recentButton.setDatabseService(getDatabaseService());

        
    }
}
