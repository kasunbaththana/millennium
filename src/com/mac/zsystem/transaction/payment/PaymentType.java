/*
 *  PaymentType.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 3:49:31 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment;

/**
 *
 * @author mohan
 */
public class PaymentType {

    public static final String CASH = "CASH";
    public static final String CHEQUE = "CHEQUE";
    public static final String BANK = "BANK";
    public static final String ACCOUNT = "ACCOUNT";
}
