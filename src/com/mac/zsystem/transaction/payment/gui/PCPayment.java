/*
 *  PCPayment.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 2:49:50 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment.gui;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.zsystem.transaction.account.account_setting.AccountTableModel;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.Account;
import com.mac.zsystem.transaction.payment.object.BankDeposit;
import com.mac.zsystem.transaction.payment.object.Cheque;
import com.mac.zsystem.transaction.payment.object.Payment;
import com.mac.zsystem.transaction.payment.object.PaymentInformation;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.object.TransactionType;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author mohan
 */
public class PCPayment extends AbstractObjectCreator<Payment> {

    public PCPayment(TransactionType transactionType) {
        this.transactionType = transactionType;

        initComponents();

        initOthers();
    }

    private void initInformation() {
        txtReferenceNo.setCValue(referenceNo);
        txtDocumentNo.setCValue(documentNo);
        txtTransactionDate.setCValue(transactionDate);
        txtPayableAmount.setCValue(payableAmount);
    }

    private List getAccount() {
        try {
            return getCPanel().getDatabaseService().getCollection(Account.class);
        } catch (DatabaseException ex) {
            Logger.getLogger(PCPayment.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList();
        }
    }

    private void resetCalculations() {
        cashAmount = txtCashAmount.getCValue();
        chequeAmount = 0.0;
        bankAmount = 0.0;
        accountAmount = txtAccountAmount.getCValue();

        Collection<Cheque> cheques = tblCheques.getCValue();
        for (Cheque cheque : cheques) {
            chequeAmount += cheque.getAmount();
        }

        Collection<BankDeposit> bankDeposits = tblBankDeposit.getCValue();
        for (BankDeposit bankDeposit : bankDeposits) {
            bankAmount += bankDeposit.getAmount();
        }

        paidAmount = chequeAmount + cashAmount + bankAmount + accountAmount;


        txtCashAmount.setCValue(cashAmount);
        txtBankAmount.setCValue(bankAmount);
        txtChequeAmount.setCValue(chequeAmount);
        txtAccountAmount.setCValue(accountAmount);
        txtPaidAmount.setCValue(paidAmount);
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public void setPayableAmount(Double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public void setChequeType(String chequeType) {
        this.chequeType = chequeType;
    }

    private void initPaymentSettings() {
        for (PaymentSetting paymentSetting : transactionType.getPaymentSettings()) {
            
            if (paymentSetting.getType().equals(PaymentType.CASH)) {
                cashPaymentSetting = paymentSetting;
            }
            if (paymentSetting.getType().equals(PaymentType.CHEQUE)) {
                chequePaymentSetting = paymentSetting;
            }
            if (paymentSetting.getType().equals(PaymentType.BANK)) {
                bankPaymentSetting = paymentSetting;
            }
            if (paymentSetting.getType().equals(PaymentType.ACCOUNT)) {
                accountPaymentSetting = paymentSetting;
            }
//            System.out.println("cashPaymentSetting____________________XXX"+cashPaymentSetting);
        }

        cashPaymentActive = (cashPaymentSetting != null ? cashPaymentSetting.isActive() : false);
        chequePaymentActive = (chequePaymentSetting != null ? chequePaymentSetting.isActive() : false);
        bankPaymentActive = (bankPaymentSetting != null ? bankPaymentSetting.isActive() : false);
        accountPaymentActive = (accountPaymentSetting != null ? accountPaymentSetting.isActive() : false);
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        cboAccountAmount.setExpressEditable(true);
        cboAccountAmount.setTableModel(new AccountTableModel());
        if(transactionType.getCode().equals(SystemTransactions.VOUCHER_TRANSACTION_CODE)
                ||transactionType.getCode().equals(SystemTransactions.GENERAL_VOUCHER_TRANSACTION_CODE) 
                ||transactionType.getCode().equals(SystemTransactions.SAVING_WITHDRAW_CODE)){
            tblCheques.setObjectCreator(new PCChequeDataVoucher(){
            @Override
            protected CPanel getCPanel() {
                return PCPayment.this.getCPanel();
            }
        });
        }else{
        tblCheques.setObjectCreator(new PCChequeData() {
            @Override
            protected CPanel getCPanel() {
                return PCPayment.this.getCPanel();
            }
        });
        }
        tblCheques.setDescrption("Cheque");

        CTableModel<Cheque> tableModel = new CTableModel<>(new HashSet<Cheque>(), new CTableColumn[]{
            new CTableColumn("Bank Branch", new String[]{"bankBranch", "name"}),
            new CTableColumn("Account No.", new String[]{"accountNo"}),
            new CTableColumn("Cheque No.", new String[]{"chequeNo"}),
            new CTableColumn("Cheque Date", new String[]{"chequeDate"}),
            new CTableColumn("Amount", new String[]{"amount"})
        });
        tblCheques.setCModel(tableModel);


        tblBankDeposit.setObjectCreator(new PCBankDepositData() {
            @Override
            protected CPanel getCPanel() {
                return PCPayment.this.getCPanel();
            }
        });
        tblBankDeposit.setDescrption("Bank Deposit");
        CTableModel<BankDeposit> bankDepositTableModel = new CTableModel<>(new HashSet<BankDeposit>(), new CTableColumn[]{
            new CTableColumn("Bank Account", "bankAccount", "accountNumber"),
            new CTableColumn("Amount", "amount")
        });
        tblBankDeposit.setCModel(bankDepositTableModel);


        //VALUE CALCULATION LISTENERS
        tblCheques.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                resetCalculations();
            }
        });
        tblBankDeposit.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                resetCalculations();
            }
        });
        txtCashAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetCalculations();
            }
        });
        txtBankAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetCalculations();
            }
        });
        txtAccountAmount.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                resetCalculations();
            }
        });

        initPaymentSettings();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        txtDocumentNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        txtTransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPayableAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jPanel1 = new javax.swing.JPanel();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCashAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPaidAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jPanel3 = new javax.swing.JPanel();
        tblBankDeposit = new com.mac.af.component.derived.input.table.CIAddingTable();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBankAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jPanel2 = new javax.swing.JPanel();
        tblCheques = new com.mac.af.component.derived.input.table.CIAddingTable();
        txtChequeAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        jPanel4 = new javax.swing.JPanel();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAccountAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cboAccountAmount = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getAccount();
            }
        };

        cDLabel1.setText("Reference No.:");

        cDLabel2.setText("Document No.:");

        cDLabel3.setText("Transaction Date :");

        cDLabel4.setText("Payable Amount :");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        cDLabel5.setText("Cash Amount :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCashAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCashAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cDLabel7.setText("Paid Amount :");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Bank Deposits"));

        cDLabel8.setText("Bank Amount :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tblBankDeposit, javax.swing.GroupLayout.DEFAULT_SIZE, 348, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBankAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBankAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tblBankDeposit, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Cheques"));

        cDLabel6.setText("Cheque Amount :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tblCheques, javax.swing.GroupLayout.DEFAULT_SIZE, 348, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtChequeAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtChequeAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tblCheques, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        cDLabel9.setText("Account Amount :");

        cDLabel10.setText("Account  :");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboAccountAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAccountAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAccountAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAccountAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDocumentNo, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                            .addComponent(txtTransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtPayableAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtPaidAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDocumentNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPayableAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPaidAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAccountAmount;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private com.mac.af.component.derived.input.table.CIAddingTable tblBankDeposit;
    private com.mac.af.component.derived.input.table.CIAddingTable tblCheques;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAccountAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBankAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtCashAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtChequeAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtDocumentNo;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPaidAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtPayableAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtTransactionDate;
    // End of variables declaration//GEN-END:variables
    private Payment payment;
    private TransactionType transactionType;
    private PaymentSetting cashPaymentSetting;
    private PaymentSetting chequePaymentSetting;
    private PaymentSetting bankPaymentSetting;
    private PaymentSetting accountPaymentSetting;
    private boolean cashPaymentActive;
    private boolean chequePaymentActive;
    private boolean accountPaymentActive;
    private boolean bankPaymentActive;
    //parameters
    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private Double payableAmount;
    private String chequeType;
    //calcutation
    private Double cashAmount;
    private Double accountAmount;
    private Double bankAmount;
    private Double chequeAmount;
    private Double paidAmount;

    @Override
    public void setNewMood() {
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(false);
        txtTransactionDate.setValueEditable(false);
        txtPayableAmount.setValueEditable(false);
        txtPaidAmount.setValueEditable(false);
        txtCashAmount.setValueEditable(cashPaymentActive);
        txtBankAmount.setValueEditable(false);
        txtChequeAmount.setValueEditable(false);
        tblCheques.setValueEditable(chequePaymentActive);
        tblBankDeposit.setValueEditable(bankPaymentActive);


        jPanel1.setVisible(cashPaymentActive);
        jPanel2.setVisible(chequePaymentActive);
        jPanel3.setVisible(bankPaymentActive);
        jPanel4.setVisible(accountPaymentActive);

        payment = new Payment();
    }

    @Override
    public void setEditMood() {
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(false);
        txtTransactionDate.setValueEditable(false);
        txtPayableAmount.setValueEditable(false);
        txtPaidAmount.setValueEditable(false);
        txtCashAmount.setValueEditable(cashPaymentActive);
        txtBankAmount.setValueEditable(false);
        txtChequeAmount.setValueEditable(false);
        tblCheques.setValueEditable(chequePaymentActive);
        tblBankDeposit.setValueEditable(bankPaymentActive);
    }

    @Override
    public void setIdleMood() {
        txtReferenceNo.setValueEditable(false);
        txtDocumentNo.setValueEditable(false);
        txtTransactionDate.setValueEditable(false);
        txtPayableAmount.setValueEditable(false);
        txtPaidAmount.setValueEditable(false);
        txtCashAmount.setValueEditable(false);
        txtBankAmount.setValueEditable(false);
        txtChequeAmount.setValueEditable(false);
        tblCheques.setValueEditable(false);
        tblBankDeposit.setValueEditable(false);
    }

    @Override
    public void resetFields() {
        txtReferenceNo.resetValue();
        txtDocumentNo.resetValue();
        txtTransactionDate.resetValue();
        txtPayableAmount.resetValue();
        txtPaidAmount.resetValue();
        txtCashAmount.resetValue();
        txtBankAmount.resetValue();
        txtChequeAmount.resetValue();

        initInformation();
        resetCalculations();
    }

    @Override
    protected void setValueAbstract(Payment value) throws ObjectCreatorException {
        this.payment = value;
    }

    @Override
    protected Payment getValueAbstract() throws ObjectCreatorException {
        return payment;
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
    }

    @Override
    protected void initObject() throws ObjectCreatorException {
        if (paidAmount.doubleValue() != payableAmount.doubleValue()) {
            throw new ObjectCreatorException("Paid amount not match to the Payable amount."
                    + "\n Paid amount : " + paidAmount
                    + "\n Payable amount : " + payableAmount);
        }

        payment.setAmount(paidAmount);

        //CASH PAYMENT
        if (cashPaymentActive) {
            PaymentInformation cashPayment = new PaymentInformation();
            cashPayment.setPayment(payment);
            cashPayment.setPaymentSetting(cashPaymentSetting);
            cashPayment.setAmount(cashAmount);

            payment.getPaymentInformations().add(cashPayment);
        }

        //BANK PAYMENT
        System.out.println(bankPaymentActive);
        if (bankPaymentActive) {
            PaymentInformation bankDeposi = new PaymentInformation();
            bankDeposi.setPayment(payment);
            bankDeposi.setPaymentSetting(bankPaymentSetting);
            bankDeposi.setAmount(bankAmount);

            Set<BankDeposit> bankDeposits = new HashSet<>(tblBankDeposit.getCValue());
            for (BankDeposit bankDeposit : bankDeposits) {
                bankDeposit.setPaymentInformation(bankDeposi);
                bankDeposit.setType(chequeType);
            }
            bankDeposi.setBankDeposits(bankDeposits);

            payment.getPaymentInformations().add(bankDeposi);
        }

        //CHEQUE PAYMENT
        if (chequePaymentActive) {
            PaymentInformation chequePayment = new PaymentInformation();
            chequePayment.setPayment(payment);
            chequePayment.setPaymentSetting(chequePaymentSetting);
            chequePayment.setAmount(chequeAmount);

            Set<Cheque> cheques = new HashSet<>(tblCheques.getCValue());
            for (Cheque cheque : cheques) {
                cheque.setPaymentInformation(chequePayment);
                cheque.setType(chequeType);
            }
            chequePayment.setCheques(cheques);

            payment.getPaymentInformations().add(chequePayment);
        }

        if (accountPaymentActive) {
            PaymentInformation accountPayment = new PaymentInformation();
            accountPayment.setPayment(payment);
            accountPayment.setPaymentSetting(accountPaymentSetting);
            accountPayment.setAmount(accountAmount);
            accountPayment.setAccount(((Account) cboAccountAmount.getCValue()).getCode());

            payment.getPaymentInformations().add(accountPayment);
        }
    }
}
