/*
 *  PaymentDialog.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 3:23:50 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment.gui;

import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.panel.dialog.object_creator_dialog.ObjectCreatorDialog;
import com.mac.zsystem.transaction.payment.object.Payment;
import com.mac.zsystem.transaction.payment.object.TransactionType;
import java.util.Date;

/**
 *
 * @author mohan
 */
public abstract class PaymentDialog extends ObjectCreatorDialog<Payment> {

    public PaymentDialog(final TransactionType transactionType) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                pcPayment = new PCPayment(transactionType) {
                    @Override
                    protected CPanel getCPanel() {
                        return getOwnCPanel();
                    }
                };

//                pcPayment.setChequeType(chequeType);
                setObjectCreator(pcPayment);
                setTitle(transactionType.getName());
                pack();
            }
        };
        CApplication.invokeEventDispatch(runnable);
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
        pcPayment.setReferenceNo(referenceNo);
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
        pcPayment.setDocumentNo(documentNo);
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
        pcPayment.setTransactionDate(transactionDate);
    }

    public void setPayableAmount(Double payableAmount) {
        this.payableAmount = payableAmount;
        pcPayment.setPayableAmount(payableAmount);
    }

    public void setChequeType(String chequeType) {
        this.chequeType = chequeType;
        pcPayment.setChequeType(chequeType);
    }
    

    //
    protected abstract CPanel getOwnCPanel();
    private PCPayment pcPayment;
    //parameters
    private String referenceNo;
    private String documentNo;
    private Date transactionDate;
    private Double payableAmount;
    private String chequeType;
}
