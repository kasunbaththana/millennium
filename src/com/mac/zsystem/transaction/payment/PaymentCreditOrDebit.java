/*
 *  PaymentCreditOrDebit.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 3:50:59 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment;

/**
 *
 * @author mohan
 */
public class PaymentCreditOrDebit {

    public static final String CREDIT = "CREDIT";
    public static final String DEBIT = "DEBIT";
    //
    public static final String[] ALL = {
        CREDIT,
        DEBIT
    };
}
