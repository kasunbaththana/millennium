/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.payment.payment_setting.system_interface;

/**
 *
 * @author NIMESH-PC
 */

import com.mac.zsystem.transaction.payment.PaymentCreditOrDebit;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import static com.mac.zsystem.transaction.payment.payment_setting.system_interface.SalesInvoicePaymentInterface.CASH_PAYMENT;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

public class SalesInvoicePaymentInterface extends PaymentInterface {

    public static final String CASH_PAYMENT = "SALES_INVOICE_CASH";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.SALES_ITEM_TRANSACTION_CODE;
    }

    @Override
    protected List<PaymentSetting> getNewPaymentSettings() {
        return Arrays.asList(
                newPaymentSetting(CASH_PAYMENT, "Sales Invoice Cash", PaymentType.CASH, PaymentCreditOrDebit.CREDIT));
    }
}
