/*
 *  VoucherPaymentInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 3:46:59 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment.payment_setting.system_interface;

import com.mac.zsystem.transaction.payment.PaymentCreditOrDebit;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class BankDepositPaymentInterface extends PaymentInterface {

    public static final String CASH_PAYMENT = "BANK_DEPOSIT_CASH";
    
    public static final String CHEQUE_PAYMENT = "BANK_DEPOSIT_CHEQUE";
    public static final String ACCOUNT_PAYMENT = "BANK_DEPOSIT_ACCOUNT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.BANK_DEPOSIT_TRANSACTION_CODE;
    }

    @Override
    protected List<PaymentSetting> getNewPaymentSettings() {
        return Arrays.asList(
                newPaymentSetting(CASH_PAYMENT, "Bank Deposit Cash", PaymentType.CASH, PaymentCreditOrDebit.CREDIT),
                newPaymentSetting(CHEQUE_PAYMENT, "Bank Deposit Cheque Payment", PaymentType.CHEQUE, PaymentCreditOrDebit.CREDIT),
                newPaymentSetting(ACCOUNT_PAYMENT, "Bank Deposit Account", PaymentType.ACCOUNT, PaymentCreditOrDebit.CREDIT)
                );
    }
}
