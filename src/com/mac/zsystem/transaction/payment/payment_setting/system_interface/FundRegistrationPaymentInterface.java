
package com.mac.zsystem.transaction.payment.payment_setting.system_interface;

import com.mac.zsystem.transaction.payment.PaymentCreditOrDebit;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author kasun
 */
public class FundRegistrationPaymentInterface extends PaymentInterface {

    public static final String CASH_PAYMENT = "FUND_REG_CASH";
    public static final String CHEQUE_PAYMENT = "FUND_REG_CHEQUE";
    public static final String ACCOUNT_PAYMENT = "FUND_REG_ACCOUNT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.FUND_REGISTRATION_TRANSACTION_CODE;
    }

    @Override
    protected List<PaymentSetting> getNewPaymentSettings() {
        return Arrays.asList(
                newPaymentSetting(CASH_PAYMENT, "Fund Registration Cash Payment", PaymentType.CASH, PaymentCreditOrDebit.DEBIT),
                newPaymentSetting(ACCOUNT_PAYMENT, "Fund Registration Account Payment", PaymentType.ACCOUNT, PaymentCreditOrDebit.DEBIT),
                newPaymentSetting(CHEQUE_PAYMENT, "Fund Registration Cheque Payment", PaymentType.CHEQUE, PaymentCreditOrDebit.DEBIT));
    }
}
