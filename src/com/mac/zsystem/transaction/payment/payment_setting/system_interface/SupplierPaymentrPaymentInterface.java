/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.payment.payment_setting.system_interface;

import com.mac.zsystem.transaction.payment.PaymentCreditOrDebit;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class SupplierPaymentrPaymentInterface extends PaymentInterface {

    public static final String CASH_PAYMENT = "SUPPLIER_PAYMENT_CASH";
    public static final String CHEQUE_PAYMENT = "SUPPLIER_PAYMENT_CHEQUE";
    public static final String ACCOUNT_PAYMENT = "SUPPLIER_PAYMENT_ACCOUNT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.SUPPLIER_PAYMENT;
    }

    @Override
    protected List<PaymentSetting> getNewPaymentSettings() {
        return Arrays.asList(
                newPaymentSetting(CASH_PAYMENT, "Supplier Payment Cash Payment", PaymentType.CASH, PaymentCreditOrDebit.CREDIT),
                newPaymentSetting(CHEQUE_PAYMENT, "Supplier Payment Cheque Payment", PaymentType.CHEQUE, PaymentCreditOrDebit.CREDIT),
                newPaymentSetting(ACCOUNT_PAYMENT, "Supplier Payment Account Payment", PaymentType.ACCOUNT, PaymentCreditOrDebit.CREDIT)
                
                );
    }
}
