/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.payment.payment_setting.system_interface;

import com.mac.zsystem.transaction.payment.PaymentCreditOrDebit;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author KASUN
 */
public class SavingStartPaymentInterface  extends PaymentInterface {

    public static final String CASH_PAYMENT = "SAVING_START_CASH";
    public static final String CHEQUE_PAYMENT = "SAVING_START_CHEQUE";
    public static final String ACCOUNT_PAYMENT = "SAVING_START_ACCOUNT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.SAVING_CODE;
    }

    @Override
    protected List<PaymentSetting> getNewPaymentSettings() {
        return Arrays.asList(
                newPaymentSetting(CASH_PAYMENT, "Creditor Cash Payment", PaymentType.CASH, PaymentCreditOrDebit.DEBIT),
                newPaymentSetting(CHEQUE_PAYMENT, "Creditor Cheque Payment", PaymentType.CHEQUE, PaymentCreditOrDebit.DEBIT),
                newPaymentSetting(ACCOUNT_PAYMENT, "Creditor Account Payment", PaymentType.ACCOUNT, PaymentCreditOrDebit.DEBIT)
                
                );
    }
}