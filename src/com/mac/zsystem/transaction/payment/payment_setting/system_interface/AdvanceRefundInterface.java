
package com.mac.zsystem.transaction.payment.payment_setting.system_interface;

import com.mac.zsystem.transaction.payment.PaymentCreditOrDebit;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author kasun
 */
public class AdvanceRefundInterface extends PaymentInterface {

    public static final String CASH_PAYMENT = "ADVANCE_REFUND_CASH";
    public static final String CHEQUE_PAYMENT = "ADVANCE_REFUND_CHEQUE";
    public static final String ACCOUNT_PAYMENT_A = "ADVANCE_REFUND_ACCOUNT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.ADVACNE_REF_TRANSACTION_CODE;
    }

    @Override
    protected List<PaymentSetting> getNewPaymentSettings() {
        return Arrays.asList(
                newPaymentSetting(CASH_PAYMENT, "Advance Refund Cash ", PaymentType.CASH, PaymentCreditOrDebit.CREDIT),
                newPaymentSetting(CHEQUE_PAYMENT, "Advance Refund Cheque ", PaymentType.CHEQUE, PaymentCreditOrDebit.CREDIT),
                newPaymentSetting(ACCOUNT_PAYMENT_A, "Advance Refund Account", PaymentType.ACCOUNT, PaymentCreditOrDebit.CREDIT)
                );
    }
}
