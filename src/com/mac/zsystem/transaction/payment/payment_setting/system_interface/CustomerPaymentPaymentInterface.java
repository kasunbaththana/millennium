/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.payment.payment_setting.system_interface;

import com.mac.zsystem.transaction.payment.PaymentCreditOrDebit;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author NIMESH-PC
 */
public class CustomerPaymentPaymentInterface extends PaymentInterface  {
     public static final String CASH_PAYMENT = "CUSTOMER_PAYMENT_CASH";
    public static final String CHEQUE_PAYMENT = "CUSTOMER_PAYMENT_CHEQUE";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.CUSTOMER_PAYMENT;
    }

    @Override
    protected List<PaymentSetting> getNewPaymentSettings() {
        return Arrays.asList(
                newPaymentSetting(CASH_PAYMENT, "Customer Payment Cash Payment", PaymentType.CASH, PaymentCreditOrDebit.DEBIT),
                newPaymentSetting(CHEQUE_PAYMENT, "Customer Payment Cheque Payment", PaymentType.CHEQUE, PaymentCreditOrDebit.DEBIT));
    }
}
