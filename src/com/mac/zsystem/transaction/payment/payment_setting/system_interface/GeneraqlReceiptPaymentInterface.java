/*
 *  VoucherPaymentInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 3:46:59 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment.payment_setting.system_interface;

import com.mac.zsystem.transaction.payment.PaymentCreditOrDebit;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class GeneraqlReceiptPaymentInterface extends PaymentInterface {

    public static final String CASH_PAYMENT = "GENERAL_RECEIPT_CASH";
    public static final String CHEQUE_PAYMENT = "GENERAL_RECEIPT_CHEQUE";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.GENERAL_RECEIPT_TRANSACTION_CODE;
    }

    @Override
    protected List<PaymentSetting> getNewPaymentSettings() {
        return Arrays.asList(
                newPaymentSetting(CASH_PAYMENT, "General Receipt Cash Payment", PaymentType.CASH, PaymentCreditOrDebit.DEBIT),
                newPaymentSetting(CHEQUE_PAYMENT, "General Receipt Cheque Payment", PaymentType.CHEQUE, PaymentCreditOrDebit.DEBIT));
    }
}
