/*
 *  VoucherPaymentInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 3:46:59 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment.payment_setting.system_interface;

import com.mac.zsystem.transaction.payment.PaymentCreditOrDebit;
import com.mac.zsystem.transaction.payment.PaymentType;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mohan
 */
public class DisbursementPaymentInterface extends PaymentInterface {

    public static final String CASH_PAYMENT = "DSB_CASH";
    public static final String CHEQUE_PAYMENT = "DSB_CHEQUE";
    public static final String ACCOUNT_PAYMENT = "DSB_ACCOUNT";

    @Override
    public String getTransactionTypeCode() {
        return SystemTransactions.DISBURSEMENT_TRANSACTION_CODE;
    }

    @Override
    protected List<PaymentSetting> getNewPaymentSettings() {
        return Arrays.asList(
                newPaymentSetting(CASH_PAYMENT, "Disbursement Cash Payment", PaymentType.CASH, PaymentCreditOrDebit.CREDIT),
                newPaymentSetting(CHEQUE_PAYMENT, "Disbursement Cheque Payment", PaymentType.CHEQUE, PaymentCreditOrDebit.CREDIT),
                newPaymentSetting(ACCOUNT_PAYMENT, "Disbursement Account Payment", PaymentType.ACCOUNT, PaymentCreditOrDebit.CREDIT)
                
                );
    }
}
