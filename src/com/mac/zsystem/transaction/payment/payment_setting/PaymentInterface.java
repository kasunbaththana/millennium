/*
 *  PaymentInterface.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 1:18:09 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment.payment_setting;

import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import java.util.List;

/**
 *
 * @author mohan
 */
public abstract class PaymentInterface {

    public abstract String getTransactionTypeCode();

    protected abstract List<PaymentSetting> getNewPaymentSettings();

    public List<PaymentSetting> getPaymentSettings() {
        initAccountSettings();
        return paymentSettings;
    }

    protected PaymentSetting newPaymentSetting(
            String code,
            String name,
            String type,
            String creditOrDebit) {

        PaymentSetting paymentSetting = new PaymentSetting();

        paymentSetting.setCode(code);
        paymentSetting.setName(name);
        paymentSetting.setTransactionType(null);
        paymentSetting.setCreditOrDebit(creditOrDebit);
        paymentSetting.setType(type);
        paymentSetting.setAccount(null);
        paymentSetting.setSortIndex(0);
        paymentSetting.setActive(false);

        return paymentSetting;
    }

    private void initAccountSettings() {
        if (paymentSettings == null) {
            paymentSettings = getNewPaymentSettings();
        }
    }
    private List<PaymentSetting> paymentSettings;
}
