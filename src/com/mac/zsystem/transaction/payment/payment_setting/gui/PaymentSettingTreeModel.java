/*
 *  PaymentSettingTreeModel.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 3:53:13 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment.payment_setting.gui;

import com.mac.af.component.model.tree.CTreeModel;
import com.mac.zsystem.transaction.payment.object.TransactionType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author mohan
 */
public class PaymentSettingTreeModel extends CTreeModel {

    @Override
    public Object getChild(Object parent, int index) {
        if (parent instanceof TransactionType) {
            Iterator iterator = ((TransactionType) parent).getPaymentSettings().iterator();
            for (int i = 0; i < index; i++) {
                iterator.next();
            }

            return iterator.next();
        } else if (parent instanceof List) {
            return ((List) parent).get(index);
        } else {
            return null;
        }
    }

    @Override
    public int getChildCount(Object parent) {
        if (parent instanceof TransactionType) {
            return ((TransactionType) parent).getPaymentSettings().size();
        } else if (parent instanceof Collection) {
            return ((Collection) parent).size();
        }

        return 0;
    }

    @Override
    public boolean isLeaf(Object node) {
        boolean b;
        if (node instanceof Collection) {
            b = false;
        } else if (node instanceof TransactionType) {
            b = false;
        } else {
            b = true;
        }

        return b;
    }
    
        @Override
    public void setRoot(Object root) {
        if (root instanceof Collection) {
            Collection<TransactionType> rootCollection = (Collection) root;
            List<TransactionType> transactionTypes = new ArrayList<>();
            for (TransactionType transactionType : rootCollection) {
                if (!transactionType.getPaymentSettings().isEmpty()) {
                    transactionTypes.add(transactionType);
                }
            }
            
            super.setRoot(transactionTypes);
        }
    }
}
