/*
 *  SystemPaymentSettings.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 14, 2014, 3:37:13 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment.payment_setting.gui;

import com.mac.af.component.model.tree.CTreeModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.tree_registration.AbstractTreeRegistration;
import com.mac.zsystem.transaction.payment.object.PaymentSetting;
import com.mac.zsystem.transaction.payment.object.TransactionType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.tree.TreeCellRenderer;

/**
 *
 * @author mohan
 */
public class SystemPaymentSettings extends AbstractTreeRegistration {

    @Override
    protected Map<Class, AbstractObjectCreator> getObjectCreatorMap() {
        initService();

        Map<Class, AbstractObjectCreator> objectCreators = new HashMap<>();

        objectCreators.put(PaymentSetting.class, new PCPaymentSetting(serAccountSetting));
        objectCreators.put(TransactionType.class, new PCTransactionType());

        return objectCreators;
    }

    @Override
    protected CTreeModel getTreeModel() {
        return new PaymentSettingTreeModel();
    }

    @Override
    protected TreeCellRenderer getTreeRenderer() {
        return new PaymentSettingTreeRenderer();
    }

    @Override
    protected List getTreeData() {
        initService();

        return serAccountSetting.getTransactionTypes();
    }

    @Override
    protected Class getChildClass(Class cls) {
        if (cls.equals(TransactionType.class)) {
            return PaymentSetting.class;
        } else {
            return null;
        }
    }

    @Override
    protected Class getParentClass(Class cls) {
        if (cls.equals(PaymentSetting.class)) {
            return TransactionType.class;
        } else {
            return null;
        }
    }

    @Override
    protected boolean isAllowNew(Class cls) {
        return false;
    }

    @Override
    protected void save(Object currentObject, Object saveObject) throws DatabaseException {
        if (currentObject instanceof PaymentSetting) {
            serAccountSetting.saveAccountSetting((PaymentSetting) currentObject);
        }
    }

    private void initService() {
        if (serAccountSetting == null) {
            serAccountSetting = new SERPaymentSetting(this);
        }
    }
    private SERPaymentSetting serAccountSetting;
}
