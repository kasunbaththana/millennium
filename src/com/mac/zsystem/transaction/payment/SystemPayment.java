/*
 *  SystemPayment.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 3:34:27 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.payment;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.gui.PaymentDialog;
import com.mac.zsystem.transaction.payment.object.BankDeposit;
import com.mac.zsystem.transaction.payment.object.Cheque;
import com.mac.zsystem.transaction.payment.object.Payment;
import com.mac.zsystem.transaction.payment.object.PaymentInformation;
import com.mac.zsystem.transaction.payment.object.TransactionType;
import com.mac.zsystem.transaction.payment.payment_setting.PaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.AdvanceRefundInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.BankDepositPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.CustomerPaymentPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.DisbursementPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.FundRegistrationPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.FundVoucherPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.SupplierPaymentrPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.GeneralVoucherPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.GeneralVoucherPaymentInterface2;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.GeneraqlReceiptPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.GroupReceiptPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.ReceiptBankDepositPaymentInterface;
//import com.mac.zsystem.transaction.payment.payment_setting.system_interface.ReceiptBankDepositPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.ReceiptPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.SalesInvoicePaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.SavingDepositPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.SavingStartPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.SavingWithdrawPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.TemperaryReceiptPaymentInterface;
import com.mac.zsystem.transaction.payment.payment_setting.system_interface.VoucherPaymentInterface;
import com.mac.zsystem.transaction.settlement.SettlementCreditDebit;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mohan
 */
public class SystemPayment {

    private SystemPayment(String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }
    private static final PaymentInterface VOUCHER_PAYMENT_INTERFACE = new VoucherPaymentInterface();
    private static final PaymentInterface SUPPLIER_PAYMENT_INTERFACE = new SupplierPaymentrPaymentInterface();
    private static final PaymentInterface RECEIPT_PAYMENT_INTERFACE = new ReceiptPaymentInterface();
    private static final PaymentInterface GROUP_RECEIPT_PAYMENT_INTERFACE = new GroupReceiptPaymentInterface();
    private static final PaymentInterface BANK_RECEIPT_PAYMENT_INTERFACE = new ReceiptBankDepositPaymentInterface();
    private static final PaymentInterface TEMPERARY_RECEIPT_PAYMENT_INTERFACE = new TemperaryReceiptPaymentInterface();
    private static final PaymentInterface BANK_DEPOSIT_INTERFACE = new BankDepositPaymentInterface();
    private static final PaymentInterface GENERAL_RECEIPT_INTERFACE = new GeneraqlReceiptPaymentInterface();
    private static final PaymentInterface GENERAL_VOUCHER_INTERFACE = new GeneralVoucherPaymentInterface();
    private static final PaymentInterface GENERAL_VOUCHER_INTERFACE_2 = new GeneralVoucherPaymentInterface2();
    private static final PaymentInterface SALES_INVOICE_INTERFACE = new SalesInvoicePaymentInterface();
     private static final PaymentInterface CUSTOMER_PAYMENT_INTERFACE = new CustomerPaymentPaymentInterface();
     private static final PaymentInterface SAVING_PAYMENT_INTERFACE = new SavingWithdrawPaymentInterface();
     private static final PaymentInterface SAVING_START_INTERFACE = new SavingStartPaymentInterface();
     private static final PaymentInterface SAVING_DEPOSIT_INTERFACE = new SavingDepositPaymentInterface();
     private static final PaymentInterface DISBURSEMENT_INTERFACE = new DisbursementPaymentInterface();
     private static final PaymentInterface FUND_VOUCHER_INTERFACE = new FundVoucherPaymentInterface();
     private static final PaymentInterface FUND_REGISTRATION_INTERFACE = new FundRegistrationPaymentInterface();
     private static final PaymentInterface ADVANCE_REFUND_INTERFACE = new AdvanceRefundInterface();

    public static List<PaymentInterface> getPaymentInterfaces() {
        return Arrays.asList(
                (PaymentInterface) VOUCHER_PAYMENT_INTERFACE,
                (PaymentInterface) SUPPLIER_PAYMENT_INTERFACE,
                (PaymentInterface) RECEIPT_PAYMENT_INTERFACE,
                (PaymentInterface) GROUP_RECEIPT_PAYMENT_INTERFACE,
                (PaymentInterface) BANK_RECEIPT_PAYMENT_INTERFACE,
                (PaymentInterface) TEMPERARY_RECEIPT_PAYMENT_INTERFACE,
                (PaymentInterface) BANK_DEPOSIT_INTERFACE,
                (PaymentInterface) GENERAL_RECEIPT_INTERFACE,
                (PaymentInterface) GENERAL_VOUCHER_INTERFACE,
                (PaymentInterface) GENERAL_VOUCHER_INTERFACE_2,
                (PaymentInterface) CUSTOMER_PAYMENT_INTERFACE,
                (PaymentInterface) SAVING_PAYMENT_INTERFACE,
                (PaymentInterface) SAVING_START_INTERFACE,
                (PaymentInterface) SAVING_DEPOSIT_INTERFACE,
                (PaymentInterface) DISBURSEMENT_INTERFACE,
                (PaymentInterface) FUND_VOUCHER_INTERFACE,
                (PaymentInterface) FUND_REGISTRATION_INTERFACE,
                (PaymentInterface) ADVANCE_REFUND_INTERFACE,
                (PaymentInterface) SALES_INVOICE_INTERFACE);
    }

    public static SystemPayment getInstance(String transactionCode) {
        return new SystemPayment(transactionCode);
    }

    public boolean showPaymentDialog(
            final HibernateDatabaseService databaseService,
            String referenceNo,
            String documentNo,
            Date transactionDate,
            Double payableAmount,
            String chequeType) throws DatabaseException {
        final TransactionType transactionType = (TransactionType) databaseService.getObject(TransactionType.class, transactionTypeCode);

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                paymentDialog = new PaymentDialog(transactionType) {
                    @Override
                    protected CPanel getOwnCPanel() {
                        return databaseService.getCPanel();
                    }
                };
            }
        };

        CApplication.invokeEventDispatch(runnable);

        paymentDialog.setReferenceNo(referenceNo);
        paymentDialog.setDocumentNo(documentNo);
        paymentDialog.setTransactionDate(transactionDate);
        paymentDialog.setPayableAmount(payableAmount);
        paymentDialog.setChequeType(chequeType);
        paymentDialog.pack();

        payment = paymentDialog.getNewObject();


        return payment != null;
    }
    public boolean isNotCashPayment(){
        
         for (PaymentInformation paymentInformation : payment.getPaymentInformations()) {
             
             if (paymentInformation.getPaymentSetting().getType().equals(PaymentType.CASH)) {
                 if(paymentInformation.getAmount()>0){
                     
                     return false;
                 }
                
             }
             
         }
        
        
        return true;
        
    }

    public void savePayment(HibernateDatabaseService databaseService, CashierSession cashierSession, int transaction, String client) throws DatabaseException {

        System.out.println(payment.getPaymentInformations().size());

        for (PaymentInformation paymentInformation : payment.getPaymentInformations()) {
            paymentInformation.setTransaction(transaction);
            paymentInformation.setTransactionType(transactionTypeCode);

            for (Cheque cheque : paymentInformation.getCheques()) {
                cheque.setClient(client);
            }

            for (BankDeposit bankDeposit : paymentInformation.getBankDeposits()) {
                bankDeposit.setClient(client);
                bankDeposit.setBankBranch(bankDeposit.getBankAccount().getBankBranch());
            }
        }


        payment.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        payment.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
        payment.setClient(client);
        payment.setCashierSession(cashierSession != null ? cashierSession.getIndexNo() : null);
        payment.setTransaction(transaction);
        payment.setTransactionType(transactionTypeCode);
        payment.setStatus(PaymentStatus.ACTIVE);

        databaseService.save(payment);

        //ACCOUNT TRANSACTION
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (PaymentInformation paymentInformation : payment.getPaymentInformations()) {
            if (paymentInformation.getPaymentSetting().getType().equals(PaymentType.CHEQUE)) {
                for (Cheque cheque : paymentInformation.getCheques()) {
                    accountTransaction.saveAccountTransaction(
                            databaseService,
                            transaction,
                            transactionTypeCode,
                            AccountTransactionType.AUTO,
                            paymentInformation.getPaymentSetting().getCreditOrDebit(),
                            cheque.getAmount(),
                            paymentInformation.getPaymentSetting().getAccount().getCode(),
                            paymentInformation.getPaymentSetting().getName(),0);
                }
            } else if (paymentInformation.getPaymentSetting().getType().equals(PaymentType.BANK)) {
                for (BankDeposit cheque : paymentInformation.getBankDeposits()) {
                   
//                    accountTransaction.saveAccountTransaction(
//                            databaseService,
//                            transaction,
//                            transactionTypeCode,
//                            AccountTransactionType.AUTO,
//                            paymentInformation.getPaymentSetting().getCreditOrDebit(),
//                            cheque.getAmount(),
//                            paymentInformation.getPaymentSetting().getAccount().getCode(),
//                            paymentInformation.getPaymentSetting().getName(),0);
                    
                    
////                    // DR for bank value account
                    accountTransaction.saveAccountTransaction(
                            databaseService,
                            transaction,
                            transactionTypeCode,
                            AccountTransactionType.AUTO,
                            SettlementCreditDebit.DEBIT,
                            cheque.getAmount(),
                            cheque.getBankAccount().getAccountByValueAccount().getCode(),
                            paymentInformation.getPaymentSetting().getName(),0);
                    
                    
                    
                    
                }
            } else if (paymentInformation.getPaymentSetting().getType().equals(PaymentType.CASH)) {
//                    System.out.println("paymentInformation.getPaymentSetting().getAccount().getCode()_"+paymentInformation.getPaymentSetting().getAccount().getCode());
                accountTransaction.saveAccountTransaction(
                        databaseService,
                        transaction,
                        transactionTypeCode,
                        AccountTransactionType.AUTO,
                        paymentInformation.getPaymentSetting().getCreditOrDebit(),
                        paymentInformation.getAmount(),
                        cashierSession.getCashierPoint().getAccountByCashierAccount().getCode(),
                        paymentInformation.getPaymentSetting().getName(),0);
                
            } else if (paymentInformation.getPaymentSetting().getType().equals(PaymentType.ACCOUNT)) {
                accountTransaction.saveAccountTransaction(
                        databaseService,
                        transaction,
                        transactionTypeCode,
                        AccountTransactionType.AUTO,
                        paymentInformation.getPaymentSetting().getCreditOrDebit(),
                        paymentInformation.getAmount(),
                        paymentInformation.getAccount(),
                        paymentInformation.getPaymentSetting().getName(),0);
            }
        }
    }
    public void savePaymentVoucher(HibernateDatabaseService databaseService, CashierSession cashierSession, int transaction, String client,Date transDate,String description) throws DatabaseException {

        System.out.println(payment.getPaymentInformations().size());

        for (PaymentInformation paymentInformation : payment.getPaymentInformations()) {
            paymentInformation.setTransaction(transaction);
            paymentInformation.setTransactionType(transactionTypeCode);

            for (Cheque cheque : paymentInformation.getCheques()) {
                cheque.setClient(client);
                cheque.setStatus("V");
            }

            for (BankDeposit bankDeposit : paymentInformation.getBankDeposits()) {
                bankDeposit.setClient(client);
                bankDeposit.setBankBranch(bankDeposit.getBankAccount().getBankBranch());
            }
        }


        payment.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        payment.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
        payment.setClient(client);
        payment.setCashierSession(cashierSession != null ? cashierSession.getIndexNo() : null);
        payment.setTransaction(transaction);
        payment.setTransactionType(transactionTypeCode);
        payment.setStatus(PaymentStatus.ACTIVE);

        databaseService.save(payment);

        //ACCOUNT TRANSACTION
        SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        for (PaymentInformation paymentInformation : payment.getPaymentInformations()) {
            if (paymentInformation.getPaymentSetting().getType().equals(PaymentType.CHEQUE)) {
                for (Cheque cheque : paymentInformation.getCheques()) {
                    accountTransaction.saveAccountTransactionVoucher(
                            databaseService,
                            transaction,
                            transactionTypeCode,
                            AccountTransactionType.AUTO,
                            paymentInformation.getPaymentSetting().getCreditOrDebit(),
                            cheque.getAmount(),
                            paymentInformation.getPaymentSetting().getAccount().getCode(),
                            paymentInformation.getPaymentSetting().getName()+" "+description,0,transDate);
                }
            } else if (paymentInformation.getPaymentSetting().getType().equals(PaymentType.BANK)) {
                for (BankDeposit cheque : paymentInformation.getBankDeposits()) {
                   
//                    accountTransaction.saveAccountTransaction(
//                            databaseService,
//                            transaction,
//                            transactionTypeCode,
//                            AccountTransactionType.AUTO,
//                            paymentInformation.getPaymentSetting().getCreditOrDebit(),
//                            cheque.getAmount(),
//                            paymentInformation.getPaymentSetting().getAccount().getCode(),
//                            paymentInformation.getPaymentSetting().getName(),0);
                    
                    
////                    // DR for bank value account
                    accountTransaction.saveAccountTransactionVoucher(
                            databaseService,
                            transaction,
                            transactionTypeCode,
                            AccountTransactionType.AUTO,
                            SettlementCreditDebit.DEBIT,
                            cheque.getAmount(),
                            cheque.getBankAccount().getAccountByValueAccount().getCode(),
                            paymentInformation.getPaymentSetting().getName()+" "+description,0,transDate);
                    
                    
                    
                    
                }
            } else if (paymentInformation.getPaymentSetting().getType().equals(PaymentType.CASH)) {
//                    System.out.println("paymentInformation.getPaymentSetting().getAccount().getCode()_"+paymentInformation.getPaymentSetting().getAccount().getCode());
                accountTransaction.saveAccountTransactionVoucher(
                        databaseService,
                        transaction,
                        transactionTypeCode,
                        AccountTransactionType.AUTO,
                        paymentInformation.getPaymentSetting().getCreditOrDebit(),
                        paymentInformation.getAmount(),
                        cashierSession.getCashierPoint().getAccountByCashierAccount().getCode(),
                        paymentInformation.getPaymentSetting().getName()+" "+description,0,transDate);
                
            } else if (paymentInformation.getPaymentSetting().getType().equals(PaymentType.ACCOUNT)) {
                accountTransaction.saveAccountTransactionVoucher(
                        databaseService,
                        transaction,
                        transactionTypeCode,
                        AccountTransactionType.AUTO,
                        paymentInformation.getPaymentSetting().getCreditOrDebit(),
                        paymentInformation.getAmount(),
                        paymentInformation.getAccount(),
                        paymentInformation.getPaymentSetting().getName()+" "+description,0,transDate);
            }
        }
    }
    private String transactionTypeCode;
    private PaymentDialog paymentDialog;
    private Payment payment;
}
