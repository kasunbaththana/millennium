package com.mac.zsystem.transaction.cashier.object;

/**
  *	@author Channa Mohan
  *	
  *	Created On Jan 30, 2015 2:51:32 PM 
  *	Mohan Hibernate Mapping Generator
  */


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * CashierClosing generated by hbm2java
 */
public class CashierClosing  implements java.io.Serializable {


     private Integer indexNo;
     private Employee employee;
     private CashierSession cashierSession;
     private CashierPoint cashierPoint;
     private String referenceNo;
     private String documentNo;
     private Date transactionDate;
     private Double coin10;
     private Double coin5;
     private Double coin2;
     private Double coin1;
     private Double totalCoin;
     private Double totalCent;
     private Double note5000;
     private Double note2000;
     private Double note1000;
     private Double note500;
     private Double note100;
     private Double note50;
     private Double note20;
     private Double note10;
     private Double totalNote;
     private Double other;
     private Double totalCash;
     private Double totalCheque;
     private String status;
     private Set<CashierClosingCheque> cashierClosingCheques = new HashSet<CashierClosingCheque>(0);
     private Double totalCollection;
     private Double alLoan;
     private Double qlLoan;
     private Double cashInHand;
     private Double excessShort;
     private Double repayment;
     private Double documentCharge;
     private Double advanceRental;
     private Double defaultCharges;
     private Double latePayment;

    public CashierClosing() {
    }

    public CashierClosing(Employee employee, CashierSession cashierSession, CashierPoint cashierPoint, String referenceNo, String documentNo, Date transactionDate, Double coin10, Double coin5, Double coin2, Double coin1, Double totalCoin, Double note5000, Double note2000, Double note1000, Double note500, Double note100, Double note50, Double note20, Double note10, Double totalNote, Double other, Double totalCash, Double totalCheque,Double totalCent, String status, Set<CashierClosingCheque> cashierClosingCheques) {
       this.employee = employee;
       this.cashierSession = cashierSession;
       this.cashierPoint = cashierPoint;
       this.referenceNo = referenceNo;
       this.documentNo = documentNo;
       this.transactionDate = transactionDate;
       this.coin10 = coin10;
       this.coin5 = coin5;
       this.coin2 = coin2;
       this.coin1 = coin1;
       this.totalCoin = totalCoin;
       this.totalCent = totalCent;
       this.note5000 = note5000;
       this.note2000 = note2000;
       this.note1000 = note1000;
       this.note500 = note500;
       this.note100 = note100;
       this.note50 = note50;
       this.note20 = note20;
       this.note10 = note10;
       this.totalNote = totalNote;
       this.other = other;
       this.totalCash = totalCash;
       this.totalCheque = totalCheque;
       this.status = status;
       this.cashierClosingCheques = cashierClosingCheques;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public Employee getEmployee() {
        return this.employee;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public CashierSession getCashierSession() {
        return this.cashierSession;
    }
    
    public void setCashierSession(CashierSession cashierSession) {
        this.cashierSession = cashierSession;
    }
    public CashierPoint getCashierPoint() {
        return this.cashierPoint;
    }
    
    public void setCashierPoint(CashierPoint cashierPoint) {
        this.cashierPoint = cashierPoint;
    }
    public String getReferenceNo() {
        return this.referenceNo;
    }
    
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
    public String getDocumentNo() {
        return this.documentNo;
    }
    
    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }
    public Date getTransactionDate() {
        return this.transactionDate;
    }
    
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
    public Double getCoin10() {
        return this.coin10;
    }
    
    public void setCoin10(Double coin10) {
        this.coin10 = coin10;
    }
    public Double getCoin5() {
        return this.coin5;
    }
    
    public void setCoin5(Double coin5) {
        this.coin5 = coin5;
    }
    public Double getCoin2() {
        return this.coin2;
    }
    
    public void setCoin2(Double coin2) {
        this.coin2 = coin2;
    }
    public Double getCoin1() {
        return this.coin1;
    }
    
    public void setCoin1(Double coin1) {
        this.coin1 = coin1;
    }
    public Double getTotalCoin() {
        return this.totalCoin;
    }
    
    public void setTotalCoin(Double totalCoin) {
        this.totalCoin = totalCoin;
    }
    public Double getNote5000() {
        return this.note5000;
    }
    
    public void setNote5000(Double note5000) {
        this.note5000 = note5000;
    }
    public Double getNote2000() {
        return this.note2000;
    }
    
    public void setNote2000(Double note2000) {
        this.note2000 = note2000;
    }
    public Double getNote1000() {
        return this.note1000;
    }
    
    public void setNote1000(Double note1000) {
        this.note1000 = note1000;
    }
    public Double getNote500() {
        return this.note500;
    }
    
    public void setNote500(Double note500) {
        this.note500 = note500;
    }
    public Double getNote100() {
        return this.note100;
    }
    
    public void setNote100(Double note100) {
        this.note100 = note100;
    }
    public Double getNote50() {
        return this.note50;
    }
    
    public void setNote50(Double note50) {
        this.note50 = note50;
    }
    public Double getNote20() {
        return this.note20;
    }
    
    public void setNote20(Double note20) {
        this.note20 = note20;
    }
    public Double getNote10() {
        return this.note10;
    }
    
    public void setNote10(Double note10) {
        this.note10 = note10;
    }
    public Double getTotalNote() {
        return this.totalNote;
    }
    
    public void setTotalNote(Double totalNote) {
        this.totalNote = totalNote;
    }
    public Double getOther() {
        return this.other;
    }
    
    public void setOther(Double other) {
        this.other = other;
    }
    public Double getTotalCash() {
        return this.totalCash;
    }
    
    public void setTotalCash(Double totalCash) {
        this.totalCash = totalCash;
    }
    public Double getTotalCheque() {
        return this.totalCheque;
    }
    
    public void setTotalCheque(Double totalCheque) {
        this.totalCheque = totalCheque;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    public Set<CashierClosingCheque> getCashierClosingCheques() {
        return this.cashierClosingCheques;
    }
    
    public void setCashierClosingCheques(Set<CashierClosingCheque> cashierClosingCheques) {
        this.cashierClosingCheques = cashierClosingCheques;
    }

    public Double getTotalCent() {
        return totalCent;
    }

    public void setTotalCent(Double totalCent) {
        this.totalCent = totalCent;
    }

    public Double getTotalCollection() {
        return totalCollection;
    }

    public void setTotalCollection(Double totalCollection) {
        this.totalCollection = totalCollection;
    }

    public Double getAlLoan() {
        return alLoan;
    }

    public void setAlLoan(Double alLoan) {
        this.alLoan = alLoan;
    }

    public Double getCashInHand() {
        return cashInHand;
    }

    public void setCashInHand(Double cashInHand) {
        this.cashInHand = cashInHand;
    }

    public Double getExcessShort() {
        return excessShort;
    }

    public void setExcessShort(Double excessShort) {
        this.excessShort = excessShort;
    }

    public Double getRepayment() {
        return repayment;
    }

    public void setRepayment(Double repayment) {
        this.repayment = repayment;
    }

    public Double getDocumentCharge() {
        return documentCharge;
    }

    public void setDocumentCharge(Double documentCharge) {
        this.documentCharge = documentCharge;
    }

    public Double getAdvanceRental() {
        return advanceRental;
    }

    public void setAdvanceRental(Double advanceRental) {
        this.advanceRental = advanceRental;
    }

    public Double getDefaultCharges() {
        return defaultCharges;
    }

    public void setDefaultCharges(Double defaultCharges) {
        this.defaultCharges = defaultCharges;
    }

    public Double getLatePayment() {
        return latePayment;
    }

    public void setLatePayment(Double latePayment) {
        this.latePayment = latePayment;
    }

    public Double getQlLoan() {
        return qlLoan;
    }

    public void setQlLoan(Double qlLoan) {
        this.qlLoan = qlLoan;
    }

    




	@Override
	public boolean equals(Object other) {
		if (this == other){
			return true;
		}
			 
		if (other == null){
			return false;
		}
		
		if ( !(other instanceof CashierClosing) ){
			return false;
		}
		
		CashierClosing castOther = ( CashierClosing ) other; 

		if(this.indexNo==null && castOther.indexNo==null) {
			return false;
		}
		
		if(!java.util.Objects.equals(this.indexNo, castOther.indexNo)) {
			return false;
		}
			 
		return true;
	}

    @Override
    public int hashCode() {
        int result = 17;
        
		result = result * 17 + java.util.Objects.hashCode(this.indexNo);

        return result;
    }





}


