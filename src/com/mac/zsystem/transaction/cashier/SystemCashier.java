/*
 *  SystemCashier.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 9:46:11 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.cashier;

import com.mac.af.core.ApplicationException;
import com.mac.af.core.concurrent.CExecutable;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.registration.cashier_point.PCCashier;
import com.mac.zsystem.transaction.cashier.object.CashierPoint;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.cashier.object.Employee;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class SystemCashier {

    public static CashierSession getCurrentCashierSession(final HibernateDatabaseService databaseService) throws ApplicationException {
        try {
            CashierSession cashierSession = getCurrentCashierSessionInternal(databaseService);

            return cashierSession;

        } catch (final ApplicationException applicationException) {
            CExecutable<Void> executable = new CExecutable<Void>("CASHIER SESSION READING", databaseService.getCPanel()) {
                @Override
                public Void execute() throws Exception {
                    Thread.sleep(100);

                    mOptionPane.showMessageDialog(null, applicationException.getMessage(), "Cashier Transaction", mOptionPane.ERROR_MESSAGE);
                    TabFunctions.closeTab(databaseService.getCPanel());

                    return null;
                }
            };

            CApplication.getExecutionManager().execute(executable);

            return null;
        }
    }

    private static CashierSession getCurrentCashierSessionInternal(HibernateDatabaseService databaseService) throws ApplicationException {
        String terminal = getTerminalIdentifierInternal();

        CashierSession cashierSession;

        String hql = "FROM com.mac.zsystem.transaction.cashier.object.CashierSession WHERE cashierPoint.terminal=:TERMINAL AND status=:STATUS";
        HashMap<String, Object> params = new HashMap<>();
        params.put("TERMINAL", terminal);
        params.put("STATUS", CashierSessionStatus.RUNING);


        try {
            List<CashierSession> cashierSessions = databaseService.getCollection(hql, params);
            if (cashierSessions.isEmpty()) {
                Employee employee = getCurrentEmployee(databaseService);

                HashMap<String, Object> cashierPointParams = new HashMap<>();
                cashierPointParams.put("TERMINAL", terminal);
                CashierPoint cashierPoint;
                List<CashierPoint> cashierPoints = databaseService.getCollection("FROM com.mac.zsystem.transaction.cashier.object.CashierPoint WHERE terminal=:TERMINAL", cashierPointParams);
                if (cashierPoints.isEmpty()) {
                    throw new ApplicationException("This cashier point is not registered. Please register cashier point before transactions.");
                } else {
                    cashierPoint = cashierPoints.get(0);
                }

                cashierSession = new CashierSession();
                cashierSession.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                cashierSession.setCashierPoint(cashierPoint);
                cashierSession.setEmployee(employee);
                cashierSession.setStatus(CashierSessionStatus.RUNING);
                cashierSession = (CashierSession) databaseService.save(cashierSession);
            } else {
                cashierSession = cashierSessions.get(0);

                if (!cashierSession.getEmployee().getCode().equals((String) CApplication.getSessionVariable(CApplication.USER_ID))) {
                    throw new ApplicationException("Another cashier session by employee '" + cashierSession.getEmployee().getName() + "' is already running."
                            + "\nYou cannot start a cashier session until close existing cashier session.");
                }
            }

            return cashierSession;

        } catch (DatabaseException e) {
            Logger.getLogger(SystemCashier.class.getName()).log(Level.SEVERE, null, e);
            throw new ApplicationException("Unable to get cashier session information");
        }

    }

    public static String getTerminalIdentifier(final CPanel panel) throws ApplicationException {
        try {
            String terminal = getTerminalIdentifierInternal();

            return terminal;

        } catch (final ApplicationException applicationException) {
            CExecutable<Void> executable = new CExecutable<Void>("CASHIER SESSION READING", panel) {
                @Override
                public Void execute() throws Exception {
                    Thread.sleep(100);

                    mOptionPane.showMessageDialog(null, applicationException.getMessage(), "Cashier Transaction", mOptionPane.ERROR_MESSAGE);
                    TabFunctions.closeTab(panel);

                    return null;
                }
            };

            CApplication.getExecutionManager().execute(executable);

            return null;
        }
    }

    private static String getTerminalIdentifierInternal() throws ApplicationException {
        try {
            InetAddress address;
            address = InetAddress.getLocalHost();
            NetworkInterface interface1 = NetworkInterface.getByInetAddress(address);
            byte[] mac = interface1.getHardwareAddress();

            StringBuilder builder = new StringBuilder();
            for (byte b : mac) {
                builder.append(String.format("%02X", b));
            }

            return builder.toString();
        } catch (UnknownHostException | SocketException ex) {
            Logger.getLogger(PCCashier.class.getName()).log(Level.SEVERE, null, ex);

            throw new ApplicationException("Unable to identify teminal");
        }
    }

    private static Employee getCurrentEmployee(HibernateDatabaseService databaseService) throws DatabaseException {
        return (Employee) databaseService.getObject(Employee.class, (String) CApplication.getSessionVariable(CApplication.USER_ID));
    }
}
