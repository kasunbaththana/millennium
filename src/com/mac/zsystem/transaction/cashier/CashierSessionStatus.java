/*
 *  CashierSessionStatus.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 10:16:44 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.cashier;

/**
 *
 * @author mohan
 */
public class CashierSessionStatus {

    public static final String RUNING = "RUNING";
    public static final String CLOSED = "CLOSED";
}
