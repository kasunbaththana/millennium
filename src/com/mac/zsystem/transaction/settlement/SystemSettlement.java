/*
 *  SystemSettlement.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 29, 2014, 8:00:07 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.settlement;

import com.mac.zsystem.transaction.settlement.object.Settlement;
import com.mac.zsystem.transaction.settlement.object.SettlementHistory;
import com.mac.zsystem.transaction.settlement.object.SettlementType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import com.mac.af.core.ApplicationRuntimeException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.concurrent.CExecutable;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.cpanel.CPanel;

import com.mac.zsystem.transaction.settlement.object.SettlementHistoryRpt;
import com.mac.zsystem.transaction.transaction.SystemTransactions;

/**
 *
 * @author mohan
 */
public class SystemSettlement {
    //settlement codes

    //CREDIT
    public static final String LOAN_CAPITAL = "LOAN_CAPITAL";
    public static final String LOAN_INTEREST = "LOAN_INTEREST";
    
    public static final String INSURANCE = "INSURANCE";
    public static final String CREDIT = "CREDIT";
    
    public static final String LOAN_PANALTY = "LOAN_PANALTY";
    
    public static final String OTHER_CHARGE = "OTHER_CHARGE";
    public static final String REBIT = "REBIT";
    public static final String OVER_PAY_VOUCHER = "OVER_PAY_VOUCHER";
    public static final String OVER_PAY_RECEIPT = "OVER_PAY_RECEIPT";
    public static final String APPLICATION_CHARGE = "APPLICATION_CHARGE";
    //DEBIT
    public static final String LOAN_AMOUNT = "LOAN_AMOUNT";
    
    //settlement types
    private static final SettlementType LOAN_CAPITAL_SETTLEMENT_TYPE = new SettlementType(LOAN_CAPITAL, 4, "Loan Capital Amount", SettlementCreditDebit.CREDIT, new HashSet<Settlement>(0));
    private static final SettlementType LOAN_INTEREST_SETTLEMENT_TYPE = new SettlementType(LOAN_INTEREST, 3, "Loan Interest Amount", SettlementCreditDebit.CREDIT, new HashSet<Settlement>(0));
  
    private static final SettlementType INSURANCE_AMOUNT_SETTLEMENT_TYPE = new SettlementType(INSURANCE, 4, "Insurance Amount", SettlementCreditDebit.CREDIT, new HashSet<Settlement>(0));
    
    private static final SettlementType LOAN_PANALTY_SETTLEMENT_TYPE = new SettlementType(LOAN_PANALTY, 1, "Loan Panalty Amount", SettlementCreditDebit.CREDIT, new HashSet<Settlement>(0));
    private static final SettlementType OTHER_CHARGE_SETTLEMENT_TYPE = new SettlementType(OTHER_CHARGE, 2, "Other Charge", SettlementCreditDebit.CREDIT, new HashSet<Settlement>(0));
    private static final SettlementType REBIT_SETTLEMENT_TYPE = new SettlementType(REBIT, 0, "Rebit", SettlementCreditDebit.DEBIT, new HashSet<Settlement>(0));
    private static final SettlementType LOAN_AMOUNT_SETTLEMENT_TYPE = new SettlementType(LOAN_AMOUNT, 0, "Loan Amount", SettlementCreditDebit.DEBIT, new HashSet<Settlement>(0));
    private static final SettlementType OVER_PAY_VOUCHER_SETTLEMENT_TYPE = new SettlementType(OVER_PAY_VOUCHER, 0, "Voucher Overpaid Amount", SettlementCreditDebit.CREDIT, new HashSet<Settlement>(0));
    private static final SettlementType OVER_PAY_RECEIPT_SETTLEMENT_TYPE = new SettlementType(OVER_PAY_RECEIPT, 0, "Receipt Overpaid Amount", SettlementCreditDebit.CREDIT, new HashSet<Settlement>(0));
    private static final SettlementType APPLICATION_CHARGE_SETTLEMENT_TYPE = new SettlementType(APPLICATION_CHARGE, 0, "Application Charge", SettlementCreditDebit.CREDIT, new HashSet<Settlement>(0));
    //

    public static List<SettlementType> getSystemSettlementTypes() {
        return Arrays.asList(
                LOAN_CAPITAL_SETTLEMENT_TYPE,
                LOAN_INTEREST_SETTLEMENT_TYPE,
                LOAN_PANALTY_SETTLEMENT_TYPE,
                OTHER_CHARGE_SETTLEMENT_TYPE,
                REBIT_SETTLEMENT_TYPE,
                LOAN_AMOUNT_SETTLEMENT_TYPE,
                OVER_PAY_RECEIPT_SETTLEMENT_TYPE,
                OVER_PAY_VOUCHER_SETTLEMENT_TYPE,
                INSURANCE_AMOUNT_SETTLEMENT_TYPE,
                APPLICATION_CHARGE_SETTLEMENT_TYPE);
    }

    /*
     * Operations
     */
    public static SystemSettlement getInstance() {
        return new SystemSettlement();
    }

    public void beginSettlementQueue() {
        if (this.settlementQueues != null) {
            throw new ApplicationRuntimeException("Settlement Queue is already begun. Please perform this afetr current settlement queue.");
        }

        this.settlementQueues = new ArrayList<>();
    }

    public void addSettlementQueue(
            Date dueDate,
            String client,
            Integer loan,
            Integer installmentNo,
            String transactionType,
            Integer transaction,
            String description,
            Double amount,
            String settlementTypeCode) {

        if (this.settlementQueues == null) {
            throw new ApplicationRuntimeException("Settlement Queue is not begun");
        }

        if (amount > 0) {

            SettlementQueue settlement = new SettlementQueue();
            settlement.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
            settlement.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
            settlement.setClient(client);
            settlement.setLoan(loan);
            //installment no
            settlement.setInstallmentNo(installmentNo);
            settlement.setTransaction(transaction);
            settlement.setTransactionType(transactionType);
            settlement.setDescription(description);
            settlement.setAmount(amount);
            settlement.setBalanceAmount(amount);
            settlement.setDueDate(dueDate);
            settlement.setStatus(SettlementStatus.SETTLEMENT_PENDING);
            //settlement type
            settlement.setSettlementTypeCode(settlementTypeCode);

            this.settlementQueues.add(settlement);
        }
    }
     public void addSettlementQueueForCharge(
            Date dueDate,
            String client,
            Integer loan,
            Integer installmentNo,
            String transactionType,
            Integer transaction,
            String description,
            Double amount,
            String settlementTypeCode,
            String account) {

        if (this.settlementQueues == null) {
            throw new ApplicationRuntimeException("Settlement Queue is not begun");
        }

        if (amount > 0) {

            SettlementQueue settlement = new SettlementQueue();
            settlement.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
            settlement.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
            settlement.setClient(client);
            settlement.setLoan(loan);
            //installment no
            settlement.setInstallmentNo(installmentNo);
            settlement.setTransaction(transaction);
            settlement.setTransactionType(transactionType);
            settlement.setDescription(description);
            settlement.setAmount(amount);
            settlement.setBalanceAmount(amount);
            settlement.setDueDate(dueDate);
            settlement.setAccount(account);
            settlement.setStatus(SettlementStatus.SETTLEMENT_PENDING);
            //settlement type
            settlement.setSettlementTypeCode(settlementTypeCode);

            this.settlementQueues.add(settlement);
        }
    }
   

    public void flushSettlementQueue(final HibernateDatabaseService databaseService) throws DatabaseException {
        if (settlementQueues == null) {
            throw new ApplicationRuntimeException("Settlement queue is not begun.");
        }


        CExecutable<Void> executable = new CExecutable<Void>(databaseService.getCPanel().getTabTitle() + " Update Settlements", databaseService.getCPanel()) {
            @Override
            public Void execute() throws Exception {
                HibernateDatabaseService childDatabaseService = databaseService.createChildDatabaseService();

                childDatabaseService.beginLocalTransaction();

                SettlementType settlementType;
                for (SettlementQueue settlementQueue : settlementQueues) {
                    settlementType = getSettlementType(settlementQueue.getSettlementTypeCode(), childDatabaseService);
                    settlementQueue.setSettlementType(settlementType);

                    childDatabaseService.save(settlementQueue.getSettlement());
                }

                childDatabaseService.commitLocalTransaction();

                childDatabaseService.close();

                settlementQueues = null;

                return null;
            }
        };

        CApplication.getExecutionManager().execute(executable);
    }

    public void beginSettlementHistoryQueue() {
        if (this.settlementHistorysQueues != null) {
            throw new ApplicationRuntimeException("Settlement history queue is already started");
        }

        this.settlementHistorysQueues = new ArrayList<>();
    }

    public void addSettlementHistoryQueue(
            Settlement settlement,
            String transactionType,
            Integer transactionIndexNo,
            Double settlementAmount) throws DatabaseException {

        if (this.settlementHistorysQueues == null) {
            throw new ApplicationRuntimeException("Settlement history queue is not begun");
        }

        if (settlementAmount > 0) { //No need to save empty settlement history
            //history
            SettlementHistoryQueue history = new SettlementHistoryQueue();

            history.setSettlement(settlement);
            history.setTransaction(transactionIndexNo);
            history.setTransactionType(transactionType);
            history.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
            history.setSettlementAmount(settlementAmount);
            history.setBeforeBalance(settlement.getAmount());
            history.setStatus(SettlementStatus.SETTLEMENT_HISTORY_ACTIVE);
            if(transactionType.equals(SystemTransactions.REBIT_APPROVE_TRANSACTION_CODE))
            {
            history.setStatus(SettlementStatus.REBATE_APPROVE);
                
            }
            
            
            this.settlementHistorysQueues.add(history);
        }
    }
    public int getinstallment_count(
            int IndexNo
            ) throws DatabaseException {
        HibernateDatabaseService databaseService =new HibernateDatabaseService(CPanel.GLOBAL);
        int balance=0;
         System.out.println("IndexNo__"+IndexNo);
        String sql=" SELECT count(*) FROM com.mac.zsystem.transaction.settlement.object.Settlement    WHERE loan = '"+IndexNo+"' AND balance_amount > 0  AND status <> 'CANCEL' AND settlement_type= 'LOAN_CAPITAL'";
                                            
       balance = Integer.parseInt(databaseService.getUniqueResultHQL(sql, null).toString());
        
        
        
        
        
        return balance;
        
    }
    public void addSettlementHistoryQueuerpt(
             HibernateDatabaseService databaseService,
            Settlement settlement,
            String transactionType,
            Integer transactionIndexNo,
            Double settlementAmount,
            Double arreares) throws DatabaseException {

        if (this.settlementHistorysQueues == null) {
            throw new ApplicationRuntimeException("Settlement history queue is not begun");
        }
        if(arreares==0 || arreares==null)
        {
            arreares=0.0;
        }
        if (settlementAmount > 0) { //No need to save empty settlement history
            SettlementHistoryRpt  sttlementHistoryRpt =new SettlementHistoryRpt();
            
            sttlementHistoryRpt.setDescription(transactionType);
            sttlementHistoryRpt.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
//            sttlementHistoryRpt.sSetTransaction(transactionIndexNo);
            sttlementHistoryRpt.setCustomer("");
            sttlementHistoryRpt.setSettlement(settlement);
            sttlementHistoryRpt.setCreditAmount(arreares);
            sttlementHistoryRpt.setDebitAmount(0);
            sttlementHistoryRpt.setStatus("ACTIVE");
            
            databaseService.save(sttlementHistoryRpt);
            
        }
    }


    public int flushSettlementHistoryQueue(final HibernateDatabaseService databaseService) throws DatabaseException {
        if (settlementHistorysQueues == null) {
            throw new ApplicationRuntimeException("Settlement history queue is not begun.");
        }


//        CExecutable<Void> executable = new CExecutable<Void>(databaseService.getCPanel().getTabTitle() + " Update Settlement History", databaseService.getCPanel()) {
//            @Override
//            public Void execute() throws Exception {
                HibernateDatabaseService childDatabaseService = databaseService.createChildDatabaseService();

                childDatabaseService.beginLocalTransaction();

                Settlement settlement;
                SettlementHistory history;
//                int count = 0;
                for (SettlementHistoryQueue settlementQueue : settlementHistorysQueues) {
                    //BATCH
//                    if (count!=0 && ++count % BATCH_SIZE ==count) {
//                        childDatabaseService.commitLocalTransaction();
//                        childDatabaseService.beginLocalTransaction();
//                    }
                    
                    settlement = settlementQueue.getSettlementHistory().getSettlement();
                    history = settlementQueue.getSettlementHistory();

                    if(history.getTransactionType() .equals(SystemTransactions.REBIT_APPROVE_TRANSACTION_CODE))
                    {
                        settlement.setBalanceAmount(settlement.getBalanceAmount());
                        settlement.setRebateAmount(history.getSettlementAmount());
                        
                        System.out.println("REBATE AMOUNT ...  "+history.getSettlementAmount());
                    }else
                    {
                    //MODIFICATIONS OF SETTLEMENT
                   // settlement.setBalanceAmount(settlement.getBalanceAmount() - history.getSettlementAmount());
                    if(history.getTransactionType() .equals(SystemTransactions.REBIT_TRANSACTION_CODE))
                    {
                        settlement.setRebateAmount(0.00);
                    
                    }
                        
                        
                        
                    settlement.setBalanceAmount(settlement.getBalanceAmount() - history.getSettlementAmount());
                    System.out.println("settle AMOUNT ...  "+history.getSettlementAmount());
                    }
                    
                    if (settlement.getBalanceAmount() <= 0.0) {
                        settlement.setStatus(SettlementStatus.SETTLEMENT_SETTLED);
                    }

                    //SAVE
                    databaseService.save(settlement);
                    databaseService.save(history);
                }

                childDatabaseService.commitLocalTransaction();

                childDatabaseService.close();

                settlementQueues = null;

                return 1;
//            }
//        };

//        CApplication.getExecutionManager().execute(executable);
    }

    private SettlementType getSettlementType(String code, HibernateDatabaseService databaseService) throws DatabaseException {
        initSettlementTypes(databaseService);
        for (SettlementType settlementType : settlementTypes) {
            if (settlementType.getCode().equals(code)) {
                return settlementType;
            }
        }
        return null;
    }

    private void initSettlementTypes(HibernateDatabaseService databaseService) throws DatabaseException {
        if (settlementTypes == null || oldAccessedService != databaseService) {
            settlementTypes = databaseService.getCollection(SettlementType.class);
        }
    }
    private List<SettlementType> settlementTypes;
    private HibernateDatabaseService oldAccessedService;
    private List<SettlementQueue> settlementQueues;
    private List<SettlementHistoryQueue> settlementHistorysQueues;
    private static final int BATCH_SIZE = 20;

    //SETTLEMENT QUEUE OBJECT
    private class SettlementQueue {

        private Settlement settlement;
        private String settlementTypeCode;

        public SettlementQueue() {
            settlement = new Settlement();
        }

        public void setSettlementTypeCode(String settlementTypeCode) {
            this.settlementTypeCode = settlementTypeCode;
        }

        public String getSettlementTypeCode() {
            return settlementTypeCode;
        }

        public Settlement getSettlement() {
            return settlement;
        }

        //settlement
        public void setIndexNo(Integer indexNo) {
            settlement.setIndexNo(indexNo);
        }

        public void setSettlementType(SettlementType settlementType) {
            settlement.setSettlementType(settlementType);
        }

        public void setTransactionDate(Date transactionDate) {
            settlement.setTransactionDate(transactionDate);
        }

        public void setBranch(String branch) {
            settlement.setBranch(branch);
        }

        public void setClient(String client) {
            settlement.setClient(client);
        }

        public void setLoan(Integer loan) {
            settlement.setLoan(loan);
        }
        
        public void setInstallmentNo(Integer installmentNo) {
            settlement.setInstallmentNo(installmentNo);
        }
        

        public void setTransaction(int transaction) {
            settlement.setTransaction(transaction);
        }

        public void setTransactionType(String transactionType) {
            settlement.setTransactionType(transactionType);
        }

        public void setDescription(String description) {
            settlement.setDescription(description);
        }

        public void setAmount(Double amount) {
            settlement.setAmount(amount);
        }

        public void setBalanceAmount(Double balanceAmount) {
            settlement.setBalanceAmount(balanceAmount);
        }

        public void setDueDate(Date dueDate) {
            settlement.setDueDate(dueDate);
        }

        public void setAccount(String account) {
            settlement.setAccount(account);
        }
        public void setStatus(String status) {
            settlement.setStatus(status);
        }
        
    }

    //SETTLEMENT HISTORY QUEUE OBJECT
    private class SettlementHistoryQueue {

        private SettlementHistory settlementHistory;
//        private Integer settlementIndexNo;

        public SettlementHistoryQueue() {
            this.settlementHistory = new SettlementHistory();
        }

        public SettlementHistory getSettlementHistory() {
            return settlementHistory;
        }

        public void setIndexNo(Integer indexNo) {
            this.settlementHistory.setIndexNo(indexNo);
        }

        public void setSettlement(Settlement settlement) {
            this.settlementHistory.setSettlement(settlement);
        }

        public void setTransaction(int transaction) {
            this.settlementHistory.setTransaction(transaction);
        }

        public void setTransactionType(String transactionType) {
            this.settlementHistory.setTransactionType(transactionType);
        }

        public void setTransactionDate(Date transactionDate) {
            this.settlementHistory.setTransactionDate(transactionDate);
        }

        public void setSettlementAmount(double settlementAmount) {
            this.settlementHistory.setSettlementAmount(settlementAmount);
        }

        public void setBeforeBalance(double beforeBalance) {
            this.settlementHistory.setBeforeBalance(beforeBalance);
        }

        public void setStatus(String status) {
            this.settlementHistory.setStatus(status);
        }
    }
}
