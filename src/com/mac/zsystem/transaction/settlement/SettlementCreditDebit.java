/*
 *  SettlementCreditDebit.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 30, 2014, 8:15:11 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.settlement;

/**
 *
 * @author mohan
 */
public class SettlementCreditDebit {
    public static final String CREDIT = "CREDIT";
    public static final String DEBIT = "DEBIT";
}
