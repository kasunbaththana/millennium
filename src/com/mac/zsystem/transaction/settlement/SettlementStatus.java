/*
 *  SettlementStatus.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 29, 2014, 8:23:17 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.settlement;

/**
 *
 * @author mohan
 */
public class SettlementStatus {

    public static final String SETTLEMENT_PENDING = "PENDING";
    public static final String SETTLEMENT_SETTLED = "SETTLED";
    public static final String SETTLEMENT_REBIT = "REBIT";
    public static final String SETTLEMENT_CANCEL = "CANCEL";
    public static final String REBATE_APPROVE = "RAPPROVE";
    //
    public static final String SETTLEMENT_HISTORY_ACTIVE = "ACTIVE";
    public static final String SETTLEMENT_HISTORY_CANCEL = "CANCEL";
}
