/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.transaction.day_end;

import com.mac.af.core.database.DatabaseException;
import com.mac.zsystem.transaction.day_end.object.Branch;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction_cancel.TransactionCancel;
import com.mac.zsystem.transaction.transaction_cancel.object.Transaction;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Mohan
 */
public class DayEndRollback extends TransactionCancel {

    @Override
    protected String getTransactionType() {
        return SystemTransactions.DAY_END_TRANSACTION_CODE;
    }

    @Override
    protected void cancelAdditional(Transaction transaction) throws DatabaseException {
        String branchCode = transaction.getBranch();

        Branch branch = (Branch) getDatabaseService().getObject(Branch.class, branchCode);

        Date currentDate = branch.getWorkingDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date nextDate = calendar.getTime();

        branch.setCode(branchCode);
        branch.setWorkingDate(nextDate);
        getDatabaseService().save(branch);
    }
}
