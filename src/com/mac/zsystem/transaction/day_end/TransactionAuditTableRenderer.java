/*
 *  TransactionAuditTableRenderer.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 7, 2014, 4:25:41 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.day_end;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import com.mac.af.core.environment.tab.TabFunctions;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultTableCellRenderer;

/**
 *
 * @author mohan
 */
public class TransactionAuditTableRenderer extends SubstanceDefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, final Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        SubstanceDefaultTableCellRenderer renderer = (SubstanceDefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (value instanceof TransactionAudit) {
            renderer.setText(((TransactionAudit) value).getText());
            renderer.setIcon(((TransactionAudit) value).getIcon());
            renderer.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    TabFunctions.AddTab(((TransactionAudit) value).getPanel(), ((TransactionAudit) value).getText(), ((TransactionAudit) value).getIcon());
                }
            });
        }

        return renderer;
    }
}
