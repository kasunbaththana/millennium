/*
 *  SERDayEnd.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 29, 2014, 3:53:35 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.day_end;

import com.mac.zsystem.transaction.day_end.object.Branch;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zsystem.transaction.day_end.object.Employee;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author mohan
 */
public class SERDayEnd extends AbstractService {

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public SERDayEnd(Component component) {
        super(component);
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }

    public void nextDate(Branch branch) throws DatabaseException {
        //get current date and next date
        Date currentDate = branch.getWorkingDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date nextDate = calendar.getTime();
        getDatabaseService().beginLocalTransaction();
        //INSERT TRANSACTION
        Integer transaction = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.DAY_END_TRANSACTION_CODE, //TRANSACTION CODE
                ReferenceGenerator.getInstance(ReferenceGenerator.DAY_END).getValue(), //REFERENCE NO
                null,//DOCUMENT NO
                null,//LOAN
                null,//CASHIER SESSION
                null,//CLIENT
                null);//NOTE

        //update branch
        System.out.println("nextDate" + nextDate);
        branch.setWorkingDate(nextDate);
        getDatabaseService().save(branch);

        //date increment system
        CApplication.addSessionVariable(CApplication.WORKING_DATE, nextDate);

     //   try {

//            String sqlget="SELECT \n" +
//"                loan.agreement_no,\n" +
//"                s.loan,\n" +
//"                loan.branch,\n" +
//"                loan.client,\n" +
//"                MAX(s.due_date),\n" +
//"                TIMESTAMPDIFF(MONTH, MAX(s.due_date), '"+getFormattedDate(nextDate)+"'),\n" +
//"                DATE_ADD(MAX(s.due_date),INTERVAL 1 MONTH) AS next_date\n" +
//"                \n" +
//"                FROM loan \n" +
//"                LEFT JOIN settlement s ON loan.index_no= s.loan\n" +
//"                WHERE \n" +
//"                s.settlement_type IN ('LOAN_CAPITAL','LOAN_ENDING') AND\n" +
//"                loan.status='LOAN_START' AND s.`status`<>'CANCEL'\n" +
//"                GROUP BY loan.index_no\n" +
//"                HAVING MAX(s.due_date) < '"+getFormattedDate(nextDate)+"' AND next_date = '"+getFormattedDate(nextDate)+"'\n" +
//"                ORDER BY MAX(s.due_date) ASC";
//            String sqlget = "SELECT \n"
//                    + "			    loan.agreement_no,\n"
//                    + "                s.loan,\n"
//                    + "                loan.branch,\n"
//                    + "                loan.client,\n"
//                    + "			    MAX(s.`due_date`),\n"
//                    + "			    IF(\n"
//                    + "			      loan.`payment_term` = 'DAILY',\n"
//                    + "			      'DAILY',\n"
//                    + "			      'MONTHLY'\n"
//                    + "			    ) AS TERM,\n"
//                    + "			    IF(\n"
//                    + "			      loan.`payment_term` = 'DAILY',\n"
//                    + "			      TIMESTAMPDIFF(DAY, MAX(s.`due_date`), '" + getFormattedDate(nextDate) + "'),\n"
//                    + "			      TIMESTAMPDIFF(\n"
//                    + "				MONTH,\n"
//                    + "				MAX(s.`due_date`),\n"
//                    + "				'" + getFormattedDate(nextDate) + "'\n"
//                    + "			      )\n"
//                    + "			    ) AS s,\n"
//                    + "			    IF(\n"
//                    + "			      loan.`payment_term` = 'DAILY',\n"
//                    + "			      DATE_ADD(MAX(s.due_date), INTERVAL 1 DAY),\n"
//                    + "			      DATE_ADD(MAX(s.due_date), INTERVAL 1 MONTH)\n"
//                    + "			    ) AS next_date \n"
//                    + "			  FROM\n"
//                    + "			    loan \n"
//                    + "			    LEFT JOIN `settlement` s \n"
//                    + "			      ON loan.`index_no` = s.`loan` \n"
//                    + "			  WHERE s.settlement_type IN ('LOAN_CAPITAL', 'LOAN_ENDING') \n"
//                    + "			    AND s.`status` <> 'CANCEL' \n"
//                    + "			    AND loan.`status` = 'LOAN_START' \n"
//                    + "			     GROUP BY loan.index_no \n"
//                    + "			  HAVING MAX(s.`due_date`) < '" + getFormattedDate(nextDate) + "' \n"
//                    + "			    AND next_date = '" + getFormattedDate(nextDate) + "' \n"
//                    + "			  ORDER BY loan.index_no ASC ";
//
//            System.out.println(" sq " + sqlget);
//            ResultSet loanSet = getConnection().createStatement()
//                    .executeQuery(sqlget);
//            while (loanSet.next()) {
//
//                String panaltyProcedure2 = "CALL `z_increase_loan_shedules`('" + getFormattedDate(nextDate) + "', '" + loanSet.getString(2) + "')";
//                getDatabaseService().callUpdateProcedure(panaltyProcedure2);
//                System.out.println("increese...  " + panaltyProcedure2);
//
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
             String panaltyProcedure = "CALL z_panalty_update("
                + "'" + getFormattedDate(nextDate) + "',"
                + "'" + branch.getCode() + "',"
                + "" + transaction + ");";
        getDatabaseService().callUpdateProcedure(panaltyProcedure);
        
        //        //loan capital and interest
        String accountProcedure = "CALL z_account_update_day_end("
                + "'" + getFormattedDate(nextDate) + "',"
                + "'" + branch.getCode() + "',"
                + "" + transaction + ");";
        getDatabaseService().callUpdateProcedure(accountProcedure);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
       

//        String panaltyProcedureEndLoan = "CALL z_panalty_update_end_loan("
//                + "'" + getFormattedDate(nextDate) + "',"
//                + "'" + branch.getCode() + "',"
//                + "" + transaction + ");";
//        getDatabaseService().callUpdateProcedure(panaltyProcedureEndLoan);



//        try {
//            String advancedProcedure = "CALL z_account_advanced_update_day_end("
//                    + "'" + getFormattedDate(nextDate) + "',"
//                    + "'" + branch.getCode() + "',"
//                    + "" + transaction + ");";
//
//            getDatabaseService().callUpdateProcedure(advancedProcedure);
//            System.out.println("advancedProcedure.." + advancedProcedure);
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//
//        }
        //update saving
//        String savingProcedure = "CALL z_saving_update("
//                + "'" + getFormattedDate(nextDate) + "',"
//                + "'" + branch.getCode() + "',"
//                + "" + transaction + ");";
//        getDatabaseService().callUpdateProcedure(savingProcedure);
//        //update saving
//        String saving2Procedure = "CALL z_saving_update_month("
//                + "'" + getFormattedDate(nextDate) + "',"
//                + "'" + branch.getCode() + "',"
//                + "" + transaction + ");";
//        getDatabaseService().callUpdateProcedure(saving2Procedure);
//
//        //update saving
//        String receiptBlockProcedure = "CALL z_UpdateBlockReceipt();";
//
//        getDatabaseService().callUpdateProcedure(receiptBlockProcedure);

        //

        // for end leasing 


//
//        //REMIND LETTER PROCEDURE
//        String remindLetterProcedure = "CALL z_remind_letter_update("
//                + "'" + branch.getCode() + "',"
//                + "0.1);";
//        getDatabaseService().callUpdateProcedure(remindLetterProcedure);
        getDatabaseService().commitLocalTransaction();

    }

    public String getFormattedDate(Date date) {
        return dateFormat.format(date);
    }

    public Branch getCurrentBranch() throws DatabaseException {
        return (Branch) getDatabaseService()
                .getObject(
                Branch.class,
                (String) CApplication.getSessionVariable(CApplication.STORE_ID));
    }

    public Employee getCurrentEmployee() throws DatabaseException {
        return (Employee) getDatabaseService()
                .getObject(
                Employee.class,
                (String) CApplication.getSessionVariable(CApplication.USER_ID));
    }
}
