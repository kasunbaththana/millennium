/*
 *  SystemDayEnd.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 7, 2014, 9:04:08 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.day_end;

import com.finac.loan.killProcess;
import com.mac.zresources.FinacResources;
import com.mac.zsystem.transaction.day_end.object.Branch;
import com.mac.zsystem.transaction.day_end.object.Employee;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import com.mac.af.core.environment.CApplication;
import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.core.concurrent.CExecutable;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.resources.ApplicationResources;
import com.mac.af.core.message.mOptionPane;

/**
 *
 * @author mohan
 */
public class SystemDayEnd extends CPanel {

    /**
     * Creates new form SystemDayEnd
     */
    public SystemDayEnd() {
        initComponents();

        initOthers();
    }

    @Action
    public void doNextDate() {
        btnNextDate.setEnabled(false);
        btnClose.setEnabled(false);
        CExecutable<Void> executable = new CExecutable<Void>("Day End - " + serDayEnd.getFormattedDate(currentDate), this) {
            @Override
            public Void execute() throws Exception {
                prgStatus.setIndeterminate(true);
                if (isTransactionsChecked()) {
                    try {
                        serDayEnd.nextDate(branch);

                        resetTransactionsAudits();

                        int q = mOptionPane.showOptionDialog(null, "Day-End transactions completed, system need to be restarted...", "Day End", mOptionPane.YES_NO_OPTION, mOptionPane.INFORMATION_MESSAGE, null, new String[]{"Shutdown", "Restart"}, "Shutdown");
                        if (q == mOptionPane.YES_OPTION) {
                            CApplication.getInstance().Shutdown();
                        } else {
                            CApplication.getInstance().Restart();
                        }

                    } catch (DatabaseException ex) {
                        Logger.getLogger(SystemDayEnd.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else {
                    mOptionPane.showMessageDialog(null, "Please check all transaction audits !", "Day End", mOptionPane.WARNING_MESSAGE);
              
                }
                prgStatus.setIndeterminate(false);

                return null;
            }
        };

        CApplication.getExecutionManager().execute(executable);
        btnNextDate.setEnabled(true);
        btnClean.setEnabled(false);
        btnClose.setEnabled(true);
    }

    @Action
    public void doClose() {
        TabFunctions.closeTab(this);
    }
    @Action
    public void doClean() {
         killProcess  klpro=new killProcess();
        klpro.process_of_query_kill();
        
        
        btnNextDate.setEnabled(true);
        btnClean.setEnabled(false);
        
    }

    private void initTable() {
        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return TransactionAudit.class;
                    case 1:
                        return Boolean.class;
                    default:
                        throw new AssertionError();
                }
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 1;
            }
        };

        //trasaction audits
        tableModel.addRow(new Object[]{LOAN_APPLICATION_CHECK, LOAN_APPLICATION_CHECK.isChecked()});
        tableModel.addRow(new Object[]{LOAN_CHECK, LOAN_CHECK.isChecked()});
        tableModel.addRow(new Object[]{LOAN_APPROVAL, LOAN_APPROVAL.isChecked()});
        tableModel.addRow(new Object[]{VOUCHER_CHECK, VOUCHER_CHECK.isChecked()});
        tableModel.addRow(new Object[]{RECEIPT_CHECK, RECEIPT_CHECK.isChecked()});
        tableModel.addRow(new Object[]{SAVING_DAYEND, SAVING_DAYEND.isChecked()});
//        tableModel.addRow(new Object[]{OTHER_CHARGE_CHECK, OTHER_CHARGE_CHECK.isChecked()});
//        tableModel.addRow(new Object[]{REBIT_CHECK, REBIT_CHECK.isChecked()});

        //set to the table
        tblTransactionAudit.setModel(tableModel);
        tblTransactionAudit.setRowHeight(24);
        tblTransactionAudit.setDefaultRenderer(TransactionAudit.class, new TransactionAuditTableRenderer());
        tblTransactionAudit.setTableHeader(null);

    }

    private void initData() throws DatabaseException {
        branch = serDayEnd.getCurrentBranch();
        employee = serDayEnd.getCurrentEmployee();

        currentDate = branch.getWorkingDate();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DATE, 1);
        nextDate = calendar.getTime();

        txtUser.setCValue(employee.getName());
        txtBranch.setCValue(branch.getName());
        txtCurrentDate.setCValue(currentDate);
        txtNextDate.setCValue(nextDate);

        initTable();
    }

    private boolean isTransactionsChecked() {
        boolean checked = true;
        for (int i = 0; i < tblTransactionAudit.getRowCount(); i++) {
            checked = checked && (Boolean) tblTransactionAudit.getValueAt(i, 1);
        }

        return checked;
    }

    private void resetTransactionsAudits() {
        for (int i = 0; i < tblTransactionAudit.getRowCount(); i++) {
            tblTransactionAudit.setValueAt(false, i, 1);
        }
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        //service
        serDayEnd = new SERDayEnd(this);

        //reset others
        txtUser.setValueEditable(false);
        txtBranch.setValueEditable(false);
        txtCurrentDate.setValueEditable(false);
        txtNextDate.setValueEditable(false);

        try {
            initData();
        } catch (Exception ex) {
            Logger.getLogger(SystemDayEnd.class.getName()).log(Level.SEVERE, null, ex);
            TabFunctions.closeTab(this);
        }

        //actions
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnNextDate, "doNextDate");
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnClean, "doClean");

        //icons
        btnNextDate.setIcon(FinacResources.getImageIcon(FinacResources.ACTION_NEXT_DATE, 16, 16));
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        btnClean.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_LOOKUP, 16, 16));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        prgStatus = new javax.swing.JProgressBar();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnNextDate = new com.mac.af.component.derived.command.button.CCButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        txtUser = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtNextDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        txtBranch = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCurrentDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblTransactionAudit = new javax.swing.JTable();
        btnClean = new com.mac.af.component.derived.command.button.CCButton();

        cLabel6.setText("Status");

        btnClose.setText("Close");

        btnNextDate.setText("Next Date");

        cLabel1.setText("Login User :");

        cLabel2.setText("Login Branch :");

        cLabel4.setText("Next Date :");

        cLabel3.setText("Current Date :");

        cLabel5.setText("WARNING: ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtBranch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCurrentDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNextDate, javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBranch, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCurrentDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNextDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(jPanel2);

        tblTransactionAudit.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblTransactionAudit);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                .addContainerGap())
        );

        jSplitPane1.setRightComponent(jPanel1);

        btnClean.setText("Clean");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(prgStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnClean, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnNextDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jSplitPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prgStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNextDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClean, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClean;
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnNextDate;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JProgressBar prgStatus;
    private javax.swing.JTable tblTransactionAudit;
    private com.mac.af.component.derived.input.textfield.CIStringField txtBranch;
    private com.mac.af.component.derived.input.textfield.CIDateField txtCurrentDate;
    private com.mac.af.component.derived.input.textfield.CIDateField txtNextDate;
    private com.mac.af.component.derived.input.textfield.CIStringField txtUser;
    // End of variables declaration//GEN-END:variables
    private Branch branch;
    private Employee employee;
    private Date currentDate;
    private Date nextDate;
    private SERDayEnd serDayEnd;
    //transaction
    private static final TransactionAudit LOAN_APPLICATION_CHECK = new TransactionAudit("Loan Applications", null, false, com.mac.loan.loan_application.TRALoanApplication.class);
    private static final TransactionAudit LOAN_CHECK = new TransactionAudit("Loans", null, false, com.mac.loan.loan.TRALoan.class);
    private static final TransactionAudit LOAN_APPROVAL = new TransactionAudit("Loan Approvals", null, false, com.mac.loan.loan_approval.LoanApplicationApproval.class);
    private static final TransactionAudit VOUCHER_CHECK = new TransactionAudit("Vouchers", null, false, com.mac.loan.voucher.Voucher.class);
    private static final TransactionAudit RECEIPT_CHECK = new TransactionAudit("Receipts", null, false, com.mac.loan.receipt.Receipt.class);
//    private static final TransactionAudit OTHER_CHARGE_CHECK = new TransactionAudit("Other Charges", null, false, com.mac.loan.other_charges.OtherCharges.class);
    private static final TransactionAudit REBIT_CHECK = new TransactionAudit("Rebits", null, false, com.mac.loan.rebit.ClientRebit.class);
    private static final TransactionAudit SAVING_DAYEND = new TransactionAudit("CREDITOR", null, false, com.mac.loan.receipt.Receipt.class);
}
