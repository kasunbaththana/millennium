/*
 *  TransactionAudit.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 7, 2014, 4:11:26 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.day_end;

import javax.swing.Icon;
import com.mac.af.core.environment.cpanel.CPanel;

/**
 *
 * @author mohan
 */
public class TransactionAudit {

    private String text;
    private Icon icon;
    private boolean checked;
    private Class<? extends CPanel> panel;

    public TransactionAudit(String text, Icon icon, boolean checked, Class<? extends CPanel> panel) {
        this.text = text;
        this.icon = icon;
        this.checked = checked;
        this.panel = panel;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Class<? extends CPanel> getPanel() {
        return panel;
    }

    public void setPanel(Class<? extends CPanel> panel) {
        this.panel = panel;
    }
}
