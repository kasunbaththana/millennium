/*
 *  SystemTransactions.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Jul 26, 2014, 12:48:30 PM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.transaction.transaction;

import com.mac.af.core.ApplicationException;
import com.mac.zsystem.transaction.transaction.object.Transaction;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.zsystem.transaction.transaction.object.TransactionHistory;
import com.mac.zsystem.transaction.transaction.object.TransactionType;
import java.util.HashMap;

/**
 *
 * @author null
 */
public class SystemTransactions {

    //TRANSACTION CODES
    public static final String LOAN_APPLICATION_TRANSACTION_CODE = "LOAN_APPLICATION";
    public static final String LOAN_APPROVAL_TRANSACTION_CODE = "LOAN_APPROVAL";
    public static final String LOAN_TRANSACTION_CODE = "LOAN";
    public static final String ADVANCE_LOAN_TRANSACTION_CODE = "ADVANCE_LOAN";
//    public static final String LOAN_DOWN_PAYMENT_CODE = "DOWN_PAYMENT";
    public static final String LOAN_HP_TRANSACTION_CODE = "HP";
    public static final String CUSTOMER_PAYMENT = "CUSTOMER_PAYMENT";
    public static final String INSURANCE_TRANSACTION_CODE = "INSURANCE";
    public static final String VOUCHER_TRANSACTION_CODE = "VOUCHER";
    public static final String DISBURSEMENT_TRANSACTION_CODE = "DISBURSEMENT";
    public static final String DISBURSEMENT_REJECT_TRANSACTION_CODE = "DISBURSEMENT_REJECT";
//    public static final String DELIVERY_ORDER = "DELIVERY_ORDER";
    public static final String SUPPLIER_PAYMENT = "SUPPLIER_PAYMENT";
    public static final String OTHER_CHARGE_TRANSACTION_CODE = "OTHER_CHARGE";
    public static final String SEIZED_CHARGE_TRANSACTION_CODE = "SEIZED_CHARGE";
    public static final String REBIT_TRANSACTION_CODE = "REBIT";
    public static final String EARLY_SETTLE_REBIT_TRANSACTION_CODE = "EARLY_SETTLE_REBIT";
    public static final String REBIT_APPROVE_TRANSACTION_CODE = "REBIT_APPROVE";
    public static final String RECEIPT_TRANSACTION_CODE = "RECEIPT";
    public static final String ADVACNE_TRANSACTION_CODE = "ADVANCE_RECEIPT";
    public static final String ADVACNE_REF_TRANSACTION_CODE = "ADVANCE_REFUND";
    public static final String ADVANCE_SETTLEMENT_CODE = "ADVANCE_SETTLEMENT";
    public static final String BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE = "BANK_RECEIPT";
    public static final String TEMPORARY_RECEIPT_TRANSACTION_CODE = "TEMP_RECEIPT";
    public static final String DAY_END_TRANSACTION_CODE = "DAY_END";
    public static final String CHEQUE_DEPOSIT_TRANSACTION_CODE = "CHEQUE_DEPOSIT";
    public static final String CHEQUE_REALIZE_TRANSACTION_CODE = "CHEQUE_REALIZE";
    public static final String CHEQUE_RETURN_TRANSACTION_CODE = "CHEQUE_RETURN";
    public static final String CHEQUE_ISSUE_TRANSACTION_CODE = "CHEQUE_ISSUE";
    public static final String CHEQUE_ISSUE_RETURN_TRANSACTION_CODE = "RETURN_CHEQUE_ISSUE";
    public static final String JOURNEL_TRANSACTION_CODE = "JOURNEL";
    public static final String BANK_ENTRY_TRANSACTION_CODE = "BANK_ENTRY";
    public static final String OPENING_BALANCE_TRANSACTION_CODE = "OPENING_BALANCE";
    public static final String BANK_DEPOSIT_TRANSACTION_CODE = "BANK_DEPOSIT";
    public static final String GENERAL_RECEIPT_TRANSACTION_CODE = "GENERAL_RECEIPT";
    public static final String GENERAL_VOUCHER_TRANSACTION_APPROVE_CODE = "GENERAL_VOUCHER_APPROVE";
    public static final String GENERAL_VOUCHER_TRANSACTION_CODE = "GENERAL_VOUCHER";
    public static final String FUND_ISSUE_VOUCHER_TRANSACTION_CODE = "FUND_ISSUE_VOUCHER";
    public static final String FUND_REGISTRATION_TRANSACTION_CODE = "FUND_REGISTRATION";
    public static final String GENERAL_VOUCHER_2_TRANSACTION_CODE = "GENERAL_VOUCHER_2";
    public static final String SALES_ITEM_TRANSACTION_CODE = "SALES_ITEM";
    public static final String CASHIER_CLOSE_TRANSACTION_CODE = "CASHIER_CLOSE";
    public static final String TRANSACTION_CANCEL_TRANSACTION_CODE = "TRANSACTION_CANCEL";
    public static final String LOAN_CLOSE_TRANSACTION_CODE = "LOAN_CLOSE";
    public static final String LOAN_VALUATION_TRANSACTION_CODE = "LOAN_VALUATION_CLOSE";
    public static final String GROUP_RECEIPT_TRANSACTION_CODE = "GROUP_RECEIPT";
    public static final String ACCOUNT_BUDGET = "ACCOUNT_BUDGET";
    public static final String EMPLOYEE_TARGET = "ACCOUNT_BUDGET";
    public static final String CHEQUE_RECEIPT_TRANSACTION_CODE = "CHEQUE_RECEIPT";
    public static final String WRITE_OFF_TRANSACTION_CODE = "WRITE_OFF";
    //OPNING LOAN
    public static final String OPNING_LOAN_TRANSACTION_CODE = "OPNING_LOAN";
    //FINAC LAND
    public static final String LAND_PURCHASE_TRANSACTION_CODE = "LAND_PURCHASE";
    public static final String LAND_PROJECT_TRANSACTION_CODE = "LAND_PROJECT";
    public static final String LAND_BLOCK_TRANSACTION_CODE = "LAND_BLOCK";
    public static final String LAND_EXPENDITURE_TRANSACTION_CODE = "LAND_EXPENDITURE";
    //SAVING DEPOSIT
    public static final String SAVING_DEPOSIT_CODE = "SAVING_DEPOSIT";
    public static final String SAVING_REBATE_CODE = "SAVING_REBATE";
    public static final String SAVING_CHARGES_CODE = "SAVING_CHARGES";
    public static final String SAVING_DEPOSIT_CANCEL_CODE = "SAVING_DEPOSIT_CANCEL";
    //SAVING WITHDRAW
    public static final String SAVING_WITHDRAW_CODE = "SAVING_WITHDRAW";
    public static final String SAVING_WITHDRAW_CANCEL_CODE = "SAVING_WITHDRAW_CANCEL";
    //SAVING START
    public static final String OP_SAVING_CODE = "OP_SAVING";
    public static final String SAVING_CODE = "SAVING";
    public static final String SAVING_DAYEND = "SAVING_DAYEND";
    
   // public static final String BANK_ENTRY = "BANK_ENTRY";
    
    
    //TRANSACTIONS
    private static final TransactionType SUPPLIER_PAYMENT_TRANSACTION = new TransactionType(SUPPLIER_PAYMENT, "Supplier Payment", new HashSet<Transaction>(0));
    private static final TransactionType OPNING_LOAN_TRANSACTION = new TransactionType(OPNING_LOAN_TRANSACTION_CODE, "Opning Loan", new HashSet<Transaction>(0));
    private static final TransactionType LOAN_APPLICATION_TRANSACTION = new TransactionType(LOAN_APPLICATION_TRANSACTION_CODE, "Loan Application", new HashSet<Transaction>(0));
    private static final TransactionType LOAN_APPROVAL_TRANSACTION = new TransactionType(LOAN_APPROVAL_TRANSACTION_CODE, "Loan Approval", new HashSet<Transaction>(0));
    private static final TransactionType LOAN_TRANSACTION = new TransactionType(LOAN_TRANSACTION_CODE, "Loan", new HashSet<Transaction>(0));
    private static final TransactionType ADVANCE_LOAN_TRANSACTION = new TransactionType(ADVANCE_LOAN_TRANSACTION_CODE, "Advance Loan", new HashSet<Transaction>(0));
//    private static final TransactionType LOAN_DOWN_PAYMENT = new TransactionType(LOAN_DOWN_PAYMENT_CODE, "Down Payment", new HashSet<Transaction>(0));
    private static final TransactionType HP_TRANSACTION = new TransactionType(LOAN_HP_TRANSACTION_CODE, "Hp", new HashSet<Transaction>(0));
    private static final TransactionType INSURANCE_TRANSACTION = new TransactionType(INSURANCE_TRANSACTION_CODE, "Insurance", new HashSet<Transaction>(0));
    private static final TransactionType VOUCHER_TRANSACTION = new TransactionType(VOUCHER_TRANSACTION_CODE, "Voucher", new HashSet<Transaction>(0));
    private static final TransactionType DISBURSEMENT_TRANSACTION = new TransactionType(DISBURSEMENT_TRANSACTION_CODE, "Disbursement", new HashSet<Transaction>(0));
    private static final TransactionType DISBURSEMENT_REJECT_TRANSACTION = new TransactionType(DISBURSEMENT_REJECT_TRANSACTION_CODE, "Disbursement Reject", new HashSet<Transaction>(0));
    private static final TransactionType OTHER_CHARGE_TRANSACTION = new TransactionType(OTHER_CHARGE_TRANSACTION_CODE, "Other Charge", new HashSet<Transaction>(0));
    private static final TransactionType SEIZED_CHARGE_TRANSACTION = new TransactionType(SEIZED_CHARGE_TRANSACTION_CODE, "Seized Charge", new HashSet<Transaction>(0));
    private static final TransactionType REBIT_TRANSACTION = new TransactionType(REBIT_TRANSACTION_CODE, "Rebit", new HashSet<Transaction>(0));
    private static final TransactionType REBIT_APPROVE_TRANSACTION = new TransactionType(REBIT_APPROVE_TRANSACTION_CODE, "Rebit Approve", new HashSet<Transaction>(0));
    private static final TransactionType RECEIPT_TRANSACTION = new TransactionType(RECEIPT_TRANSACTION_CODE, "Receipt", new HashSet<Transaction>(0));
    private static final TransactionType ADVANCE_RECEIPT = new TransactionType(ADVACNE_TRANSACTION_CODE, "Advance Receipt", new HashSet<Transaction>(0));
    private static final TransactionType ADVANCE_REFUND = new TransactionType(ADVACNE_REF_TRANSACTION_CODE, "Advance Refund", new HashSet<Transaction>(0));
    private static final TransactionType ADVANCE_SETTLEMENT = new TransactionType(ADVANCE_SETTLEMENT_CODE, "Advance Settlement", new HashSet<Transaction>(0));
    private static final TransactionType GROUP_RECEIPT_TRANSACTION = new TransactionType(GROUP_RECEIPT_TRANSACTION_CODE, "Group Receipt", new HashSet<Transaction>(0));
    private static final TransactionType BANK_DEPOSIT_RECEIPT_TRANSACTION = new TransactionType(BANK_DEPOSIT_RECEIPT_TRANSACTION_CODE, "Bank Deposit Receipt", new HashSet<Transaction>(0));
    private static final TransactionType TEMPERARY_RECEIPT_TRANSACTION = new TransactionType(TEMPORARY_RECEIPT_TRANSACTION_CODE, "Temperary Receipt", new HashSet<Transaction>(0));
    private static final TransactionType DAY_END_TRANSACTION = new TransactionType(DAY_END_TRANSACTION_CODE, "Day End", new HashSet<Transaction>(0));
    private static final TransactionType CHEQUE_DEPOSIT_TRANSACTION = new TransactionType(CHEQUE_DEPOSIT_TRANSACTION_CODE, "Cheque Deposit", new HashSet<Transaction>(0));
    private static final TransactionType CHEQUE_REALIZE_TRANSACTION = new TransactionType(CHEQUE_REALIZE_TRANSACTION_CODE, "Cheque Realize", new HashSet<Transaction>(0));
    private static final TransactionType CHEQUE_RETURN_TRANSACTION = new TransactionType(CHEQUE_RETURN_TRANSACTION_CODE, "Cheque Return", new HashSet<Transaction>(0));
    private static final TransactionType CHEQUE_ISSUE_TRANSACTION = new TransactionType(CHEQUE_ISSUE_TRANSACTION_CODE, "Cheque Issue", new HashSet<Transaction>(0));
    private static final TransactionType CHEQUE_ISSUE_RETURN_TRANSACTION = new TransactionType(CHEQUE_ISSUE_RETURN_TRANSACTION_CODE, "Return Cheque Issue", new HashSet<Transaction>(0));
    private static final TransactionType JOURNEL_TRANSACTION = new TransactionType(JOURNEL_TRANSACTION_CODE, "Journal Transaction", new HashSet<Transaction>(0));
    private static final TransactionType BANK_ENTRY_TRANSACTION = new TransactionType(BANK_ENTRY_TRANSACTION_CODE, "Bank Entry Transaction", new HashSet<Transaction>(0));
    private static final TransactionType OPENING_BALANCE_TRANSACTION = new TransactionType(OPENING_BALANCE_TRANSACTION_CODE, "Opening Balance", new HashSet<Transaction>(0));
    private static final TransactionType BANK_DEPOSIT_TRANSACTION = new TransactionType(BANK_DEPOSIT_TRANSACTION_CODE, "Bank Deposit", new HashSet<Transaction>(0));
    private static final TransactionType GENERAL_RECEIPT_TRANSACTION = new TransactionType(GENERAL_RECEIPT_TRANSACTION_CODE, "General Receipt", new HashSet<Transaction>(0));
    private static final TransactionType GENERAL_VOUCHER_TRANSACTION = new TransactionType(GENERAL_VOUCHER_TRANSACTION_CODE, "General Voucher", new HashSet<Transaction>(0));
    private static final TransactionType GENERAL_VOUCHER_TRANSACTION_2 = new TransactionType(GENERAL_VOUCHER_2_TRANSACTION_CODE, "General Voucher 2", new HashSet<Transaction>(0));
    private static final TransactionType FUND_ISSUE_VOUCHER_TRANSACTION = new TransactionType(FUND_ISSUE_VOUCHER_TRANSACTION_CODE, "Fund Issue Voucher", new HashSet<Transaction>(0));
    private static final TransactionType FUND_REGISTRATION_TRANSACTION = new TransactionType(FUND_REGISTRATION_TRANSACTION_CODE, "Fund Receive Receipt", new HashSet<Transaction>(0));
    private static final TransactionType SALES_ITEM_TRANSACTION = new TransactionType(SALES_ITEM_TRANSACTION_CODE, "General Item", new HashSet<Transaction>(0));
    private static final TransactionType CASHIER_CLOSE_TRANSACTION = new TransactionType(CASHIER_CLOSE_TRANSACTION_CODE, "Cashier Close", new HashSet<Transaction>(0));
    private static final TransactionType TRANSACTION_CANCEL_TRANSACTION = new TransactionType(TRANSACTION_CANCEL_TRANSACTION_CODE, "Transaction Cancel", new HashSet<Transaction>(0));
    private static final TransactionType LOAN_CLOSE_TRANSACTION = new TransactionType(LOAN_CLOSE_TRANSACTION_CODE, "Loan Close", new HashSet<Transaction>(0));
    private static final TransactionType LOAN_VALUATION_TRANSACTION = new TransactionType(LOAN_VALUATION_TRANSACTION_CODE, "Loan Valuation Close", new HashSet<Transaction>(0));
    private static final TransactionType ACCOUNT_BUDGET_TRANSACTION = new TransactionType(ACCOUNT_BUDGET, "Account Budget", new HashSet<Transaction>(0));
    private static final TransactionType EMPLOYEE_TARGET_TRANSACTION = new TransactionType(EMPLOYEE_TARGET, "Employee Target", new HashSet<Transaction>(0));
    private static final TransactionType CUSTOMER_PAYMENT_TRANSACTION = new TransactionType(CUSTOMER_PAYMENT, "Customer Payment", new HashSet<Transaction>(0));
    private static final TransactionType CHEQUE_RECEIPT_TRANSACTION = new TransactionType(CHEQUE_RECEIPT_TRANSACTION_CODE, "Cheque Receipt", new HashSet<Transaction>(0));
    private static final TransactionType SAVING_DEPOSIT_CODE_TRANSACTION = new TransactionType(SAVING_DEPOSIT_CODE, "Saving Depositt", new HashSet<Transaction>(0));
    private static final TransactionType SAVING_REBATE_CODE_TRANSACTION = new TransactionType(SAVING_REBATE_CODE, "Saving Rebate", new HashSet<Transaction>(0));
    private static final TransactionType SAVING_CHARGES_CODE_TRANSACTION = new TransactionType(SAVING_CHARGES_CODE, "Saving Charges", new HashSet<Transaction>(0));
    private static final TransactionType SAVING_DEPOSIT_CANCEL_CODE_TRANSACTION = new TransactionType(SAVING_DEPOSIT_CANCEL_CODE, "Saving Deposit Cancel", new HashSet<Transaction>(0));
    private static final TransactionType SAVING_WITHDRAW_CODE_TRANSACTION = new TransactionType(SAVING_WITHDRAW_CODE, "Saving Withdraw", new HashSet<Transaction>(0));
    private static final TransactionType SAVING_WITHDRAW_CANCEL_CODE_TRANSACTION = new TransactionType(SAVING_WITHDRAW_CANCEL_CODE, "Saving Withdraw Cancel", new HashSet<Transaction>(0));
    private static final TransactionType EARLY_SETTLE_REBIT_TRANSACTION_CODE_CODE_TRANSACTION = new TransactionType(EARLY_SETTLE_REBIT_TRANSACTION_CODE, "Early Settlement Rebate", new HashSet<Transaction>(0));
    private static final TransactionType OP_SAVING_TRANSACTION = new TransactionType(OP_SAVING_CODE, "S Accunt Opening Start", new HashSet<Transaction>(0));
    private static final TransactionType SAVING_TRANSACTION = new TransactionType(SAVING_CODE, "S Account Start", new HashSet<Transaction>(0));
    private static final TransactionType SAVING_DAYEND_TRANSACTION = new TransactionType(SAVING_DAYEND, "Saving Day End Interest", new HashSet<Transaction>(0));
    private static final TransactionType GENERAL_RECEIPT_TRANSACTION_APPROVE = new TransactionType(GENERAL_VOUCHER_TRANSACTION_APPROVE_CODE, "General Voucher Approve", new HashSet<Transaction>(0));
    private static final TransactionType WRITE_OFF_TRANSACTION = new TransactionType(WRITE_OFF_TRANSACTION_CODE, "Write Off", new HashSet<Transaction>(0));
    //FINAC LAND
    private static final TransactionType LAND_PURCHASE_TRANSACTION = new TransactionType(LAND_PURCHASE_TRANSACTION_CODE, "Land Purchase", new HashSet<Transaction>(0));
    private static final TransactionType LAND_PROJECT_TRANSACTION = new TransactionType(LAND_PROJECT_TRANSACTION_CODE, "Land Project", new HashSet<Transaction>(0));
    private static final TransactionType LAND_BLOCK_TRANSACTION = new TransactionType(LAND_BLOCK_TRANSACTION_CODE, "Land Block", new HashSet<Transaction>(0));
    private static final TransactionType LAND_EXPENDITURE_TRANSACTION = new TransactionType(LAND_EXPENDITURE_TRANSACTION_CODE, "Land Expenditure", new HashSet<Transaction>(0));
    //ACTIONS
    public static final String ACTION_NEW = "NEW";
    public static final String ACTION_EDIT = "EDIT";
    public static final String ACTION_ACCEPT = "ACCEPT";
    public static final String ACTION_SUSPEND = "SUSPEND";
    public static final String ACTION_REJECT = "REJECT";
    public static final String ACTION_DELETE = "DELETE";
    public static final String ACTION_START = "START";
    public static final String ACTION_ROLLBACK = "ROLLBACK";

    public static List<TransactionType> getSystemTransactionTypes() {
        return Arrays.asList(
                LOAN_APPLICATION_TRANSACTION,
                LOAN_APPROVAL_TRANSACTION,
                LOAN_TRANSACTION,
//                LOAN_DOWN_PAYMENT,
                HP_TRANSACTION,
                INSURANCE_TRANSACTION,
                VOUCHER_TRANSACTION,
                OTHER_CHARGE_TRANSACTION,
                REBIT_TRANSACTION,
                REBIT_APPROVE_TRANSACTION,
                RECEIPT_TRANSACTION,
                ADVANCE_RECEIPT,
                GROUP_RECEIPT_TRANSACTION,
                BANK_DEPOSIT_RECEIPT_TRANSACTION,
                TEMPERARY_RECEIPT_TRANSACTION,
                DAY_END_TRANSACTION,
                CHEQUE_DEPOSIT_TRANSACTION,
                CHEQUE_REALIZE_TRANSACTION,
                CHEQUE_RETURN_TRANSACTION,
                CHEQUE_ISSUE_TRANSACTION,
                CHEQUE_ISSUE_RETURN_TRANSACTION,
                JOURNEL_TRANSACTION,
                BANK_ENTRY_TRANSACTION,
                OPENING_BALANCE_TRANSACTION,
                BANK_DEPOSIT_TRANSACTION,
                GENERAL_RECEIPT_TRANSACTION,
                GENERAL_VOUCHER_TRANSACTION_2,
                GENERAL_VOUCHER_TRANSACTION,
                SALES_ITEM_TRANSACTION,
                TRANSACTION_CANCEL_TRANSACTION,
                CASHIER_CLOSE_TRANSACTION,
                LOAN_CLOSE_TRANSACTION,
                ACCOUNT_BUDGET_TRANSACTION,
                EMPLOYEE_TARGET_TRANSACTION,
                CUSTOMER_PAYMENT_TRANSACTION,
                OPNING_LOAN_TRANSACTION,
                CHEQUE_RECEIPT_TRANSACTION,
                GENERAL_RECEIPT_TRANSACTION_APPROVE,
                SUPPLIER_PAYMENT_TRANSACTION,
                OP_SAVING_TRANSACTION,
                SAVING_TRANSACTION,
                SAVING_CHARGES_CODE_TRANSACTION,
                ADVANCE_SETTLEMENT,
                DISBURSEMENT_TRANSACTION,
                DISBURSEMENT_REJECT_TRANSACTION,
                FUND_ISSUE_VOUCHER_TRANSACTION,
                FUND_REGISTRATION_TRANSACTION,
                WRITE_OFF_TRANSACTION,
                //saving
                SAVING_DEPOSIT_CODE_TRANSACTION,
                SAVING_DEPOSIT_CANCEL_CODE_TRANSACTION,
                SAVING_WITHDRAW_CODE_TRANSACTION,
                SAVING_WITHDRAW_CANCEL_CODE_TRANSACTION,
                SAVING_DAYEND_TRANSACTION,
                SAVING_REBATE_CODE_TRANSACTION,
                EARLY_SETTLE_REBIT_TRANSACTION_CODE_CODE_TRANSACTION,
                ADVANCE_LOAN_TRANSACTION,
                ADVANCE_REFUND,
                //FINAC LAND
                LAND_PURCHASE_TRANSACTION,
                LAND_PROJECT_TRANSACTION,
                LAND_BLOCK_TRANSACTION,
                LAND_EXPENDITURE_TRANSACTION,
                LOAN_VALUATION_TRANSACTION,
                SEIZED_CHARGE_TRANSACTION);
    }

    public static int insertTransaction(
            HibernateDatabaseService databaseService,
            String transactionTypeCode,
            String referenceNo,
            String documentNo,
            Integer loan,
            Integer cashierSession,
            String clientCode,
            String note) throws DatabaseException {

        //TRANSACTION
//       int receiptno= getReceiptNo(databaseService, transactionTypeCode);
        Transaction transaction = new Transaction();

        transaction.setTransactionType(getTransaction(transactionTypeCode));
        transaction.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        transaction.setReferenceNo(referenceNo);
        transaction.setDocumentNo(documentNo);
        transaction.setLoan(loan);
        transaction.setCashierSession(cashierSession);
        transaction.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
        transaction.setClient(clientCode);
        transaction.setNote(note);
        if(getTransaction(transactionTypeCode).getCode().equals(SystemTransactions.GENERAL_VOUCHER_TRANSACTION_CODE)){
        transaction.setStatus(SystemTransactionStatus.APPROVE);
        }else{
          transaction.setStatus(SystemTransactionStatus.ACTIVE); 
            
        }
//        transaction.setReceiptno(receiptno);
        

        transaction = (Transaction) databaseService.save(transaction);

        //TRANSACTION HISTORY
        insertTransactionHistory(databaseService, transaction, ACTION_NEW, note);

        return transaction.getIndexNo();
    }
    
     public static int insertTransactionWithOfficer(
            HibernateDatabaseService databaseService,
            String transactionTypeCode,
            String referenceNo,
            String documentNo,
            Integer loan,
            Integer cashierSession,
            String clientCode,
            String officerCode,
            String note,
            Date LoadinDate) throws DatabaseException {

        //TRANSACTION
//       int receiptno= getReceiptNo(databaseService, transactionTypeCode);
        Transaction transaction = new Transaction();

        transaction.setTransactionType(getTransaction(transactionTypeCode));
        transaction.setTransactionDate(LoadinDate);
        transaction.setReferenceNo(referenceNo);
        transaction.setDocumentNo(documentNo);
        transaction.setLoan(loan);
        transaction.setCashierSession(cashierSession);
        transaction.setBranch((String) CApplication.getSessionVariable(CApplication.STORE_ID));
        transaction.setClient(clientCode);
        transaction.setOfficer(officerCode);
        transaction.setNote(note);
        transaction.setStatus(SystemTransactionStatus.ACTIVE);
//        transaction.setReceiptno(receiptno);
        

        transaction = (Transaction) databaseService.save(transaction);

        //TRANSACTION HISTORY
        insertTransactionHistory(databaseService, transaction, ACTION_NEW, note);

        return transaction.getIndexNo();
    }
    
    

    public static void insertTransactionHistory(
            HibernateDatabaseService databaseService,
            Integer transactionIndex,
            String action,
            String note) throws DatabaseException {

        Transaction transaction = (Transaction) databaseService.getObject(Transaction.class, transactionIndex);

        insertTransactionHistory(databaseService, transaction, action, note);
    }

    public static Integer getTransactionIndexNo(
            HibernateDatabaseService databaseService,
            String transactionType,
            String referenceNo) throws DatabaseException, ApplicationException {
        String hql = "FROM com.mac.zsystem.transaction.transaction.object.Transaction WHERE transactionType=:transactionType AND referenceNo=:referenceNo";
        HashMap<String, Object> params = new HashMap<>();
        params.put("transactionType", getTransaction(transactionType));
        params.put("referenceNo", referenceNo);
        List<Transaction> transactions = databaseService.getCollection(hql, params);
        if (transactions.isEmpty()) {
            throw new ApplicationException("No such transaction found");
        }

        if (transactions.size() > 1) {
            throw new ApplicationException("Multiple transactions found");
        }

        return transactions.get(0).getIndexNo();
    }
    
    
//      public static Integer getReceiptNo(
//            HibernateDatabaseService databaseService,
//            String transactionType) throws DatabaseException, ApplicationException {
//        String hql = "SELECT MAX(index_no)+1 FROM com.mac.zsystem.transaction.transaction.object.Transaction WHERE transactionType=:transactionType ";
//        HashMap<String, Object> params = new HashMap<>();
//        params.put("transactionType", getTransaction(transactionType));
//        List<Integer> transactions = databaseService.getCollection(hql, params);
//
//        return transactions.get(0);
//    }

    private static void insertTransactionHistory(
            HibernateDatabaseService databaseService,
            Transaction transaction,
            String action,
            String note) throws DatabaseException {


        TransactionHistory history = new TransactionHistory();
        history.setTransaction(transaction);
        history.setAction(action);
        history.setEmployee((String) CApplication.getSessionVariable(CApplication.USER_ID));
        history.setDateTime(null);
        history.setNote(note);

        databaseService.save(history);
    }

    private static TransactionType getTransaction(String code) {
        for (TransactionType transaction : getSystemTransactionTypes()) {
            if (transaction.getCode().equals(code)) {
                return transaction;
            }
        }

        return null;
    }
//    public static void update(HibernateDatabaseService databaseService, Integer indexNo, String CHEQUE_DEPOSIT_TRANSACTION_CODE, String ACTION_NEW) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    public static void update(HibernateDatabaseService databaseService, Integer indexNo, String LOAN_APPROVAL_TRANSACTION_CODE, String ACTION_SUSPEND, String referenceNo, String documentNo, Integer indexNo0, String code) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
