/*
 *  ReferenceGenerator.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Aug 19, 2014, 9:12:36 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.zsystem.util.generator;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.component.util.generator.CGenerator;
import com.mac.af.core.environment.CApplication;
import com.mac.loan.zobject.Loan;
import com.mac.loan.zobject.LoanType;
import com.mac.registration.branch.object.Branch;
import java.util.ArrayList;
import java.util.Collection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mohan
 */
public class AgreementGenerator implements CGenerator<String, String> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyMMddHHmmss");
    private static DecimalFormat DECIMAL_FORMAT = null;

    private LoanType loanType;

    public AgreementGenerator(LoanType loanType) {
      
        this.loanType = loanType;
    }
 
    

    @Override
    public String getValue() {
        String branch_code = (String) CApplication.getSessionVariable(CApplication.STORE_ID);
        
         Branch b = (Branch) CPanel.GLOBAL.getDatabaseService().initCriteria(Branch.class)
                        .add(Restrictions.eq("code", branch_code))
                        .uniqueResult();
         System.out.println(loanType.getCode());
         
         Collection clCollection=new ArrayList();
         clCollection.add("LOAN_START");
         clCollection.add("LOAN_DISBURSEMENT");
         clCollection.add("SUSPEND");
         clCollection.add("COLLECTED");
         clCollection.add("COMPLETE");
         clCollection.add("CANCEL");
         
        Integer l = ((Integer) CPanel.GLOBAL.getDatabaseService().initCriteria(Loan.class)
                            .setProjection(Projections.count("indexNo"))
                        .add(Restrictions.eq("branch", branch_code))
                        .add(Restrictions.eq("loanType.code", loanType.getCode()))
                        .add(Restrictions.in("status", clCollection))
                        .uniqueResult()).intValue()+1;
         
        String val = String.format("%04d", l);    
        String agr_no = b.getPrefix()+"/"+loanType.getPrefix()+"/"+val;
        
        
        return agr_no;
    }

    @Override
    public String getKey() {
        return loanType.getPrefix();
    }

    public static AgreementGenerator getInstance(LoanType loanType) {
        return new AgreementGenerator(loanType);
    }

    
    private HibernateDatabaseService databaseService = new HibernateDatabaseService(CPanel.GLOBAL);
}
