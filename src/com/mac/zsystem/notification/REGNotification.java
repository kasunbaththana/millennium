/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.notification;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zsystem.notification.object.Notification;

/**
 *
 * @author thilanga
 */
public class REGNotification extends AbstractRegistrationForm<Notification> {

    @Override
    public AbstractObjectCreator<Notification> getObjectCreator() {
        return new PCNotification();
    }

    @Override
    public Class<? extends Notification> getObjectClass() {
           return com.mac.zsystem.notification.object.Notification.class;
    }

    @Override
    public CTableModel<Notification> getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Name", new String[]{"name"}),
            new CTableColumn("Description", new String[]{"descriptionQuery"}),
            new CTableColumn("Active", new String[]{"active"})});
    }
}
