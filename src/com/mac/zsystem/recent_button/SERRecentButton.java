/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.recent_button;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.zsystem.recent_button.object.Transaction;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Mohan
 */
public class SERRecentButton {

    private HibernateDatabaseService databaseService;

    public SERRecentButton(HibernateDatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public HibernateDatabaseService getDatabaseService() {
        return databaseService;
    }

    public List<Transaction> getTransactions(String transactionType) throws DatabaseException {
        Criteria criteria = getDatabaseService().initCriteria(Transaction.class);

        criteria.add(Restrictions.eq("transactionType", transactionType));
        criteria.add(Restrictions.eq("status", "ACTIVE"));

        return criteria.list();
    }
}
