/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.session_button;

import com.mac.af.component.base.button.button.CButton;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.message.mOptionPane;
import com.mac.zsystem.settings.transaction_settings.object.TransactionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;

/**
 *
 * @author NIMESH-PC
 */
public class SessionButton extends CButton {

    private String transactionType;
    private SessionPanel sessionPanel;
    private JDialog recentDialog;
    private TransactionType transactionTypeObject;
    private HibernateDatabaseService databaseService;

    public SessionButton() {
        //recent panel
        this.sessionPanel = new SessionPanel();

        //recent dialog
        this.recentDialog = new JDialog();
        this.recentDialog.setContentPane(sessionPanel);
        this.recentDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.recentDialog.setModal(true);
        this.recentDialog.pack();

        //action listener
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                showRecentDialog();
            }
        });
    }

    private void showRecentDialog() {
        initTransactionData();

        if (this.transactionTypeObject != null) {

            if (this.transactionTypeObject.getReport() != null) {
                this.recentDialog.setTitle(this.transactionTypeObject.getName());
                this.recentDialog.setLocationRelativeTo(null);
                this.recentDialog.setVisible(true);
            } else {
                mOptionPane.showMessageDialog(null, "No report found for transaction type " + transactionTypeObject.getName(), "Print Report", mOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    public void setDatabseService(HibernateDatabaseService databaseService) {
        this.databaseService = databaseService;
        this.sessionPanel.setDatabaseService(databaseService);
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
        this.sessionPanel.setTransactionType(transactionType);
    }

    private void initTransactionData() {
        try {
            this.transactionTypeObject = (TransactionType) databaseService.getObject(TransactionType.class, transactionType);
        } catch (DatabaseException ex) {
            Logger.getLogger(SessionButton.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}