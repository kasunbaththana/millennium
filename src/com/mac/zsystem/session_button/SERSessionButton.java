/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.session_button;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.zsystem.session_button.object.Transaction;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author NIMESH-PC
 */
public class SERSessionButton {
  private HibernateDatabaseService databaseService;

    public SERSessionButton(HibernateDatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public HibernateDatabaseService getDatabaseService() {
        return databaseService;
    }

    public List<Transaction> getTransactions(String transactionType) throws DatabaseException {
        Criteria criteria = getDatabaseService().initCriteria(Transaction.class);

        criteria.add(Restrictions.eq("transactionType", transactionType));

        return criteria.list();
    }   
}
