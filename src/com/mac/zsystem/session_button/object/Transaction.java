package com.mac.zsystem.session_button.object;

/**
  *	@author Channa Mohan
  *	
  *	Created On Sep 7, 2016 3:46:23 PM 
  *	Mohan Hibernate Mapping Generator
  */


import java.util.Date;

/**
 * Transaction generated by hbm2java
 */
public class Transaction  implements java.io.Serializable {


     private Integer indexNo;
     private Date transactionDate;
     private String transactionType;
     private String referenceNo;
     private String documentNo;
     private Integer loan;
     private Integer cashierSession;
     private String branch;
     private String client;
     private String note;
     private String status;

    public Transaction() {
    }

	
    public Transaction(Date transactionDate, String transactionType, String referenceNo, String branch, String status) {
        this.transactionDate = transactionDate;
        this.transactionType = transactionType;
        this.referenceNo = referenceNo;
        this.branch = branch;
        this.status = status;
    }
    public Transaction(Date transactionDate, String transactionType, String referenceNo, String documentNo, Integer loan, Integer cashierSession, String branch, String client, String note, String status) {
       this.transactionDate = transactionDate;
       this.transactionType = transactionType;
       this.referenceNo = referenceNo;
       this.documentNo = documentNo;
       this.loan = loan;
       this.cashierSession = cashierSession;
       this.branch = branch;
       this.client = client;
       this.note = note;
       this.status = status;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public Date getTransactionDate() {
        return this.transactionDate;
    }
    
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
    public String getTransactionType() {
        return this.transactionType;
    }
    
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
    public String getReferenceNo() {
        return this.referenceNo;
    }
    
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
    public String getDocumentNo() {
        return this.documentNo;
    }
    
    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }
    public Integer getLoan() {
        return this.loan;
    }
    
    public void setLoan(Integer loan) {
        this.loan = loan;
    }
    public Integer getCashierSession() {
        return this.cashierSession;
    }
    
    public void setCashierSession(Integer cashierSession) {
        this.cashierSession = cashierSession;
    }
    public String getBranch() {
        return this.branch;
    }
    
    public void setBranch(String branch) {
        this.branch = branch;
    }
    public String getClient() {
        return this.client;
    }
    
    public void setClient(String client) {
        this.client = client;
    }
    public String getNote() {
        return this.note;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }



@Override
public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Transaction) ) return false;
		 Transaction castOther = ( Transaction ) other; 

	if(!java.util.Objects.equals(this.transactionDate, castOther.transactionDate)) {
            return false;
     }
	if(!java.util.Objects.equals(this.transactionType, castOther.transactionType)) {
            return false;
     }
	if(!java.util.Objects.equals(this.referenceNo, castOther.referenceNo)) {
            return false;
     }
	if(!java.util.Objects.equals(this.branch, castOther.branch)) {
            return false;
     }
	if(!java.util.Objects.equals(this.status, castOther.status)) {
            return false;
     }
         
		 return true;
   }

   @Override
   public int hashCode() {
         int result = 17;
         
	result = result * 17 + java.util.Objects.hashCode(this.transactionDate);
	result = result * 17 + java.util.Objects.hashCode(this.transactionType);
	result = result * 17 + java.util.Objects.hashCode(this.referenceNo);
	result = result * 17 + java.util.Objects.hashCode(this.branch);
	result = result * 17 + java.util.Objects.hashCode(this.status);

         return result;
   }   

}


