/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zsystem.session_button;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.resources.ApplicationResources;
import com.mac.zsystem.session_button.object.Transaction;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;

/**
 *
 * @author NIMESH-PC
 */
public class SessionPanel extends javax.swing.JPanel {

    
      private SERSessionButton sessionButton;
      
    public SessionPanel() {
        initComponents();
         initOthers();
    }

    
     @Action
    public void doFind() {
        String text = txtFnd.getCValue();
        
        tblTransaction.find(text);
    }
    
    
    @Action
    public void doRefresh() {
        try {
            List<Transaction> transactions= sessionButton.getTransactions(transactionType);
            
            tblTransaction.setCValue(transactions);
        } catch (Exception ex) {
            Logger.getLogger(SessionPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Action
    public void doClose() {
        System.out.println(this.getParent().getParent().getParent());
        if (getParent().getParent().getParent() instanceof JDialog) {
            ((JDialog) getParent().getParent().getParent()).dispose();
        }
    }

    @Action
    public void doView() {
        Transaction transaction = (Transaction) tblTransaction.getSelectedValue();
        if (transaction != null) {
            Map<String, Object> params = new HashMap<>();
            params.put("CASHIER_SESSION", transaction.getCashierSession());
            TransactionUtil.PrintReport(databaseService, transactionType, params);
            doClose();
            
        }
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public HibernateDatabaseService getDatabaseService() {
        return databaseService;
    }

    public void setDatabaseService(HibernateDatabaseService databaseService) {
        this.databaseService = databaseService;
        this.sessionButton = new SERSessionButton(databaseService);
    }

    
     @SuppressWarnings("unchecked")
    private void initOthers() {
        CTableModel tableModel = new CTableModel(new CTableColumn[]{
            new CTableColumn("Date", "transactionDate"),
            new CTableColumn("Reference No", "referenceNo"),
            new CTableColumn("Document No", "documentNo"),
            new CTableColumn("Loan", "loan"),
        });

        tblTransaction.setCModel(tableModel);

        //icons
        btnFind.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_LOOKUP, 16, 16));
        btnRefresh.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_REFRESH, 16, 16));
        btnView.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_OPEN, 16, 16));
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        
        //actions
        ActionUtil actionUtil= new ActionUtil(this);
        actionUtil.setAction(btnFind, "doFind");
        actionUtil.setAction(btnRefresh, "doRefresh");
        actionUtil.setAction(btnView, "doView");
        actionUtil.setAction(btnClose, "doClose");
    }
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTransaction = new com.mac.af.component.derived.input.table.CITable();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnView = new com.mac.af.component.derived.command.button.CCButton();
        btnRefresh = new com.mac.af.component.derived.command.button.CCButton();
        txtFnd = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        btnFind = new com.mac.af.component.derived.command.button.CCButton();

        tblTransaction.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblTransaction);

        btnClose.setText("Close");

        btnView.setText("View");

        btnRefresh.setText("Refresh");

        cDLabel1.setText("Find :");

        btnFind.setText("Find");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1010, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFnd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFnd, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnFind;
    private com.mac.af.component.derived.command.button.CCButton btnRefresh;
    private com.mac.af.component.derived.command.button.CCButton btnView;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private com.mac.af.component.derived.input.table.CITable tblTransaction;
    private com.mac.af.component.derived.input.textfield.CIStringField txtFnd;
    // End of variables declaration//GEN-END:variables

 private String transactionType;
    private HibernateDatabaseService databaseService;


}
