package com.mac.sarving.object;

/**
  *	@author Channa Mohan
  *	
  *	Created On Sep 14, 2017 11:36:43 AM 
  *	Mohan Hibernate Mapping Generator
  */


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * SvType generated by hbm2java
 */
public class SvType  implements java.io.Serializable {


     private String code;
     private String description;
     private String oc;
     private Date actionDate;
     private Set<SvScheme> svSchemes = new HashSet<SvScheme>(0);
     private Set<SvAccount> svAccounts = new HashSet<SvAccount>(0);

    public SvType() {
    }

	
    public SvType(String code) {
        this.code = code;
    }
    public SvType(String code, String description, String oc, Date actionDate, Set<SvScheme> svSchemes, Set<SvAccount> svAccounts) {
       this.code = code;
       this.description = description;
       this.oc = oc;
       this.actionDate = actionDate;
       this.svSchemes = svSchemes;
//       this.svAccounts = svAccounts;
    }
   
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public String getOc() {
        return this.oc;
    }
    
    public void setOc(String oc) {
        this.oc = oc;
    }
    public Date getActionDate() {
        return this.actionDate;
    }
    
    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }
    public Set<SvScheme> getSvSchemes() {
        return this.svSchemes;
    }
    
    public void setSvSchemes(Set<SvScheme> svSchemes) {
        this.svSchemes = svSchemes;
    }
    public Set<SvAccount> getSvAccounts() {
        return this.svAccounts;
    }
    
    public void setSvAccounts(Set<SvAccount> svAccounts) {
        this.svAccounts = svAccounts;
    }

    @Override
    public String toString() {
        return code+" - "+description;
    }

    

@Override
public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof SvType) ) return false;
		 SvType castOther = ( SvType ) other; 

	if(!java.util.Objects.equals(this.code, castOther.code)) {
            return false;
     }
         
		 return true;
   }

   @Override
   public int hashCode() {
         int result = 17;
         
	result = result * 17 + java.util.Objects.hashCode(this.code);

         return result;
   }   

}


