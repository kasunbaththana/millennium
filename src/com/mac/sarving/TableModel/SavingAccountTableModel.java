/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.sarving.TableModel;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author KASUN
 */
public class SavingAccountTableModel extends CTableModel {
    
  public  SavingAccountTableModel(){
      super(new CTableColumn[]{
          new CTableColumn("Account", "code"),
          new CTableColumn("Description", "svScheme","description"),
          new CTableColumn("Clients", "customer"),
          new CTableColumn("Full Name","customer", "longName"),
      });
  }
    
}
