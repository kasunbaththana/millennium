/*
 *  REGBankAccount.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 4, 2014, 2:12:37 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.sarving.saving_account;


import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.sarving.object.SvAccount;

/**
 *
 * @author kasun
 */

public class REGSaving_Account extends AbstractRegistrationForm<SvAccount>{
   @Override
    public AbstractObjectCreator<SvAccount> getObjectCreator() {
        return new PCSavingAccount();
    }

    @Override
    public Class<? extends SvAccount> getObjectClass() {
        return SvAccount.class;
    }

    @Override
    public CTableModel<SvAccount> getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Code","code"),
                new CTableColumn("Scheme","svScheme"),
                new CTableColumn("Customer","customer"),
                new CTableColumn("Nominee","nominee"),
                new CTableColumn("Active","active")
                );
    }

//    @Override
//    public void doSave() {
//        
//        super.doSave(); 
//        
//       
//        getDatabaseService().save(null);
//        
//    }
    
    
    
}
