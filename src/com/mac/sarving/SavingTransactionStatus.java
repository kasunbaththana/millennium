/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.sarving;

/**
 *
 * @author chathu
 */
public class SavingTransactionStatus {

    public static final String ACTIVE = "ACTIVE";
    public static final String CANCEL = "CANCEL";
    public static final String PENDING = "PENDING";
    
    public static final String CAPITAL = "CAPITAL";
    public static final String INTEREST = "INTEREST";
    public static final String OP_SAVING = "OP_SAVING";
    public static final String SAVING = "SAVING";
    public static final String CL_PENDING = "CL_PENDING";
    public static final String OP_PENDING = "OP_PENDING";
    public static final String SV_PENDING = "SAVING_PENDING";
    public static final String SV_START = "SAVING_START";
    public static final String COLLECTED = "COLLECTED";
    

}
