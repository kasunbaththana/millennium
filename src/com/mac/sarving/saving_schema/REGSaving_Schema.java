/*
 *  REGBankAccount.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 4, 2014, 2:12:37 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.sarving.saving_schema;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.sarving.object.SvScheme;

/**
 *
 * @author kasun
 */
public class REGSaving_Schema extends AbstractRegistrationForm<SvScheme>{

    @Override
    public AbstractObjectCreator<SvScheme> getObjectCreator() {
        return new PCSavingSchema();
    }

    @Override
    public Class<? extends SvScheme> getObjectClass() {
        return SvScheme.class;
    }

    @Override
    public CTableModel<SvScheme> getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Code","code"),
                new CTableColumn("Description","description")
                );
    }

    @Override
    protected void delete(SvScheme object) throws DatabaseException {
       // super.delete(object); //To change body of generated methods, choose Tools | Templates.

       mOptionPane.showMessageDialog(null, "Cannot do this operation \n Please Contact Administrator !", "Type Delete", mOptionPane.WARNING_MESSAGE);
    }

    @Override
    public void doDelete()  {
   
       mOptionPane.showMessageDialog(null, "Cannot do this operation \n Please Contact Administrator !", "Type Delete", mOptionPane.WARNING_MESSAGE);
        //super.doDelete(); //To change body of generated methods, choose Tools | Templates.
    }
  
    
    
}
