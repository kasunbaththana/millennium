/*
 *  PCClient.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.sarving.SAccountApprove;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.af.resources.ApplicationResources;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.sarving.SavingTransactionStatus;
import com.mac.zresources.FinacResources;
import java.awt.Component;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class PCAccountApprove extends DefaultObjectCreator {

  private String logOfficer = (String) CApplication.getSessionVariable(CApplication.USER_ID);

    /**
     * Creates new form PCClient
     */
    public PCAccountApprove() {
        initComponents();
        initOthers();
    }

    @Action
    public void doApprove() {

        if(txtRemark1.getCValue()!=null)
        {
        int q = mOptionPane.showConfirmDialog(null, "Do you sure want to Approve Account ?", "Client Approve", mOptionPane.YES_NO_OPTION);
        try {
            if (q == mOptionPane.YES_OPTION) {
//                SvAccount svAccount = (SvAccount) getValue();
                
                 String hql = "UPDATE "
                + " com.mac.sarving.object.SvAccount "
                + " SET active=:ACTIVE,"
                + " opApprove=:OPAPPROVE,"
                + " opApproveBy=:OPAPPROVEBY,"
                + " opApproveTime=:OPAPPROVETIME,"
                + " opRemark=:OPREMARK,"
                + " status=:OPSTATUS"
                + " WHERE code=:CODE";

        HashMap<String, Object> params = new HashMap<>();
        params.put("CODE", txtCode.getCValue());
        params.put("ACTIVE", false);
        params.put("OPAPPROVE", true);
        params.put("OPAPPROVEBY", logOfficer);
        params.put("OPAPPROVETIME", (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
        params.put("OPREMARK", txtRemark1.getCValue());
        params.put("OPSTATUS", SavingTransactionStatus.CL_PENDING);
       
         getCPanel().getDatabaseService().executeUpdate(hql, params);        
                
                
//                svAccount.setActive(false);
//                svAccount.setOpApprove(true);
//                svAccount.setOpApproveBy(logOfficer);
//                getCPanel().getDatabaseService().update(svAccount);

                ((AbstractGridObject) getCPanel()).refreshTable();
            }
        } catch (DatabaseException ex) {
            Logger.getLogger(SAccountApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        else
        {
            txtRemark1.setCValue("Enter Remark");
        }
    }

    @Action
    public void doClose() {
        TabFunctions.closeTab(this.getCPanel());
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {

      

        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnApprove, "doApprove");
        actionUtil.setAction(btnClose, "doClose");

        btnApprove.setIcon(FinacResources.getImageIcon(FinacResources.ACTION_ACCEPT, 16, 16));
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
    }



    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtCode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtTypecode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtTypeName = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtChemecode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtSchemeName = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtClientCode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtNomineeCode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtOfficer = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        btnApprove = new com.mac.af.component.derived.command.button.CCButton();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        txtRemark1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        jPanel1 = new javax.swing.JPanel();
        cLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBalAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtIntAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtCapAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();

        txtOfficer.setPreferredSize(new java.awt.Dimension(79, 16));

        cLabel1.setText("Code :");

        cLabel4.setText("Type :");

        cLabel6.setText("Scheme :");

        cLabel7.setText("Customer :");

        cLabel10.setText("Nominee :");

        cLabel12.setText("Officer:");

        cLabel15.setText("Note :");

        btnApprove.setText("Approve");

        btnClose.setText("Close");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Creditor Information"));

        cLabel11.setText("Capital Amt:");

        cLabel13.setText("Interest Amt:");

        cLabel14.setText("Final Amt:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(cLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtIntAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                    .addComponent(txtCapAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBalAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCapAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtIntAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtBalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)
                        .addComponent(txtOfficer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtTypecode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtChemecode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTypeName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtSchemeName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(txtRemark1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 225, Short.MAX_VALUE)
                        .addComponent(btnApprove, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNomineeCode, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtClientCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTypecode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTypeName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtChemecode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSchemeName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtClientCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNomineeCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRemark1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(68, 68, 68)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnApprove, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(48, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnApprove;
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private javax.swing.JPanel jPanel1;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBalAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtCapAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtChemecode;
    private com.mac.af.component.derived.input.textfield.CIStringField txtClientCode;
    private com.mac.af.component.derived.input.textfield.CIStringField txtCode;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtIntAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNomineeCode;
    private com.mac.af.component.derived.input.textfield.CIStringField txtOfficer;
    private com.mac.af.component.derived.input.textfield.CIStringField txtRemark1;
    private com.mac.af.component.derived.input.textfield.CIStringField txtSchemeName;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTypeName;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTypecode;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList(
                (Component) txtCode);
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList(
//                (Component) txtRemark1
                
                );
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
//                (Component) txtChemecode,
//                (Component) txtSchemeName,
//                (Component) txtClientCode,
//                (Component) txtNomineeCode,
//                (Component) txtSchemeName,
//                (Component) txtTypecode,
//                (Component) txtRemark,
//                (Component) txtOfficer
                
                );
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
                txtTypecode,
                txtTypeName,
                txtChemecode,
                txtSchemeName,
                txtClientCode,
                txtNomineeCode,
                txtOfficer,
                txtCapAmount,
                txtIntAmount,
                txtBalAmount
                );
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList(
                new CInputComponentBinder(txtCode, "code"),
                new CInputComponentBinder(txtTypecode, "svType"),
                new CInputComponentBinder(txtTypeName, "svType","description"),
                new CInputComponentBinder(txtChemecode, "svScheme"),
                new CInputComponentBinder(txtSchemeName, "svScheme","description"),
                new CInputComponentBinder(txtClientCode, "customer","code"),
                new CInputComponentBinder(txtNomineeCode, "nominee","code"),
                new CInputComponentBinder(txtCapAmount, "capital"),
                new CInputComponentBinder(txtIntAmount, "interest"),
                new CInputComponentBinder(txtBalAmount, "balance"),
                new CInputComponentBinder(txtOfficer, "officer")// new CInputComponentBinder(chkActive, "active")
                );
    }

    @Override
    protected Class getObjectClass() {
        return com.mac.sarving.object.SvAccount.class;
    }

    @Override
    protected void afterNewObject(Object object) {
       // ((com.mac.sarving.object.SvAccount) object).setActive(true);
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        super.initInterface();
        

//        Client client = (Client) getValueAbstract();
//        if (client != null) {
//            try {
//                ImageIcon icon = imageWriter.getImage(client.getCode());
//                if (icon != null) {
//                    lblImage.setIcon(getScaledImage(icon.getImage(), 70, 75));
//                }
//            } catch (SocketException ex) {
//                Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (IOException ex) {
//                Logger.getLogger(PCClient.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } else {
//        }
    }
}
