/*
 *  ClientApproval.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 3, 2014, 7:57:46 AM 
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.sarving.SAccountApprove;


import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.sarving.object.SvAccount;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class SAccountApprovalL1 extends AbstractGridObject<SvAccount> {

    @Override
    protected CTableModel<SvAccount> getTableModel() {
        return new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Code", new String[]{"code"}),
            new CTableColumn("Type", new String[]{"svType"}),
            new CTableColumn("Scheme", new String[]{"svScheme"}),
            new CTableColumn("Customer", new String[]{"customer"}),
            new CTableColumn("Nominee", new String[]{"nominee"})});
    }

    @Override
    protected AbstractObjectCreator<SvAccount> getObjectCreator() {
        return new PCAccountApproveL1();
    }

    @Override
    protected Collection<SvAccount> getTableData() {
        try {
            return getDatabaseService().getCollection("FROM com.mac.sarving.object.SvAccount WHERE active=false and opApprove=true ");
        } catch (DatabaseException ex) {
            Logger.getLogger(PCAccountApproveL1.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }
    
    public List<SvAccount> getObjectClass() {
        try {
            return getDatabaseService().getCollection("FROM com.mac.sarving.object.SvAccount WHERE active=false and opApprove=true ");
        } catch (DatabaseException ex) {
            Logger.getLogger(PCAccountApproveL1.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }
}
