/*
 *  PCClient.java
 *  
 *  @author channa mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jun 3, 2014
 *  
 */
package com.mac.sarving.saving_start;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.panel.object.CInputComponentBinder;
import com.mac.af.panel.object.DefaultObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.af.resources.ApplicationResources;
import com.mac.sarving.SavingTransactionStatus;
import com.mac.sarving.object.SvAccount;
import com.mac.sarving.object.SvTransaction;
import com.mac.zresources.FinacResources;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SSavingOpeningStartAccountInterface;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SSavingStartAccountInterface;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class PCSavingStart extends DefaultObjectCreator {

  private String logOfficer = (String) CApplication.getSessionVariable(CApplication.USER_ID);
  private Date dateNow = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
 private CashierSession cashierSession ;
  private SystemPayment systemPayment;
  
SSavingStart sSavingStart;
    /**
     * Creates new form PCClient
     */
    public PCSavingStart() {
        initComponents();
        initOthers();
       
    }

    @Action(asynchronous = true)
    public void doApprove() {
             SvAccount account= (SvAccount)cboAccount.getCValue();
             SvAccount saccvount= account;
             String typeacc=account.getProfile();
             int  transaction_index;
             try {
                 //saving transaction
             
            cashierSession = SystemCashier.getCurrentCashierSession(getCPanel().getDatabaseService());
      
            getCPanel().getDatabaseService().beginLocalTransaction();
                 switch(typeacc){
                     case SavingTransactionStatus.OP_SAVING:
                              transaction_index = 
                          SystemTransactions.insertTransaction(
                                       getCPanel().getDatabaseService(),
                                       SystemTransactions.OP_SAVING_CODE,
                                       "",
                                      account.getCode(),
                                       null,
                                       cashierSession.getIndexNo(),
                                       null,
                                       "Opening Saving Start");

                               flushTransaction(//capital
                                       transaction_index,
                                       account, 
                                       "Opening Capital Credit",
                                       SavingTransactionStatus.CAPITAL,
                                       SavingTransactionStatus.ACTIVE,
                                       SystemTransactions.OP_SAVING_CODE,
                                       account.getCapital());
                               
                               flushTransaction(//interest
                                       transaction_index,
                                       account, 
                                       "Opening Interest Credit",
                                       SavingTransactionStatus.INTEREST,
                                       SavingTransactionStatus.ACTIVE,
                                       SystemTransactions.OP_SAVING_CODE,
                                       account.getInterest());
                               
//                               if(account.getSvScheme().getIsFd()){
//                                   
//                                   flushTransaction(//interest
//                                       transaction_index,
//                                       account, 
//                                       "Opening LInterest Credit",
//                                       SavingTransactionStatus.INTEREST,
//                                       SavingTransactionStatus.PENDING,
//                                       SystemTransactions.OP_SAVING_CODE,
//                                       interestCalculationEndPeriod());
//                               }
                                AccountTransactionOpening(
                                                         transaction_index,
                                                         saccvount);
                                
                                    String hqlw = "UPDATE "
                  + " com.mac.sarving.object.SvAccount "
                  + " SET status=:OPSTATUS"
                  + " WHERE code=:CODE";

                  HashMap<String, Object> params = new HashMap<>();
                  params.put("CODE", account.getCode());
                  params.put("OPSTATUS", SavingTransactionStatus.SV_START);
                  getCPanel().getDatabaseService().executeUpdate(hqlw, params); 
                  
                  
                  getCPanel().getDatabaseService().commitLocalTransaction();
                         mOptionPane.showMessageDialog(null, "Successfully saved !!!", "Save", mOptionPane.INFORMATION_MESSAGE);
       
                        
                         break;
                         
                     default:
                         
                                     transaction_index = 
                                        SystemTransactions.insertTransaction(
                                        getCPanel().getDatabaseService(),
                                        SystemTransactions.SAVING_CODE,
                                        "",
                                       account.getCode(),
                                        null,
                                        null,
                                        null,
                                        "Saving Start");
                         boolean isPaymentOk = showPaymentDialog(SystemTransactions.SAVING_CODE,account.getBalance());
                    if (isPaymentOk) {

                          savePayment(transaction_index);
                   
                        

                                flushTransaction(//capital
                                        transaction_index,
                                        account, 
                                        "Saving Capital Credit",
                                        SavingTransactionStatus.CAPITAL,
                                        SavingTransactionStatus.ACTIVE,
                                        SystemTransactions.SAVING_CODE,
                                        account.getCapital());
                                flushTransaction(//interest
                                        transaction_index,
                                        account, 
                                        "Saving Interest Credit",
                                        SavingTransactionStatus.INTEREST,
                                        SavingTransactionStatus.ACTIVE,
                                        SystemTransactions.SAVING_CODE,
                                        account.getInterest());
//                          if(account.getSvScheme().getIsFd()){
//                                   
//                                   flushTransaction(//interest
//                                       transaction_index,
//                                       account, 
//                                       "Opening LInterest Credit",
//                                       SavingTransactionStatus.INTEREST,
//                                       SavingTransactionStatus.PENDING,
//                                       SystemTransactions.SAVING_CODE,
//                                       interestCalculationEndPeriod());
//                               }
                           
                                
                                
                         AccountTransaction(
                          transaction_index, 
                          account);
                         
                         
                         
                         String hql = "UPDATE "
                  + " com.mac.sarving.object.SvAccount "
                  + " SET status=:OPSTATUS"
                  + " WHERE code=:CODE";

                  HashMap<String, Object> params1 = new HashMap<>();
                  params1.put("CODE", account.getCode());
                  params1.put("OPSTATUS", SavingTransactionStatus.SV_START);
                  getCPanel().getDatabaseService().executeUpdate(hql, params1); 
                  
                  
                  getCPanel().getDatabaseService().commitLocalTransaction();
                         mOptionPane.showMessageDialog(null, "Successfully saved !!!", "Save", mOptionPane.INFORMATION_MESSAGE);
       
                         
                    }
                
                 }
                 
                  
                  
                 
                 
                
                  
             } catch (Exception e) {
            
        }
    }
    
//     public double interestCalculationEndPeriod()
//    {
//        SvAccount ssvAccount=(SvAccount) cboAccount.getCValue();
//        double intRate=0,capitalAmount=0,intAmount=0,calRate=0;
//        int dayCount=0;
//        try {
//            if(ssvAccount!=null){
//               intRate = ssvAccount.getSvScheme().getIntsRate();
//               
//               capitalAmount = ssvAccount.getBalance();
//               dayCount=ssvAccount.getSvScheme().getPeriod();
//               
//               calRate =  intRate / 12;
//               
//               intAmount=(capitalAmount*calRate/100)*dayCount;
//              
//               cLIntRate.setCValue(intAmount+"");
//               
//            }
//        } catch (Exception e) {
//        }
//        return intAmount;
//    }
    public boolean showPaymentDialog(String transactionTypeCode,double balance) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);
    //    double AmountAc=getInterest()+getCapital();

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getCPanel().getDatabaseService(),
                "",
                "",
                dateNow,
                balance,
                ChequeType.CLIENT);
//        boolean isPaymentOk = systemPayment.showPaymentDialog(
//                getDatabaseService(),
//                receipt.getReferenceNo(),
//                receipt.getDocumentNo(),
//                receipt.getTransactionDate(),
//                receipt.getPaymentAmount(),
//                ChequeType.CLIENT);

        return isPaymentOk;
    }
   
   
     public void savePayment(int transactionIndex) throws DatabaseException {
        systemPayment.savePayment(
                getCPanel().getDatabaseService(),
                cashierSession,
                transactionIndex,
                null);
    }
  
    
    public void flushTransaction(
            int transaction,
            SvAccount account,
            String descriotion,
            String settlementType,
            String status,
            String tranType,
            double amount){
        
        try {
              SvTransaction svTransaction =new SvTransaction();
                         List<SvTransaction> svTransactionList =new ArrayList<>();
                        getCPanel().getDatabaseService().beginLocalTransaction();
                        
                        svTransaction.setAccNo(account);
                        svTransaction.setActionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                        svTransaction.setCreditAmount(amount);
                        svTransaction.setDebitAmount(00.0);
                        svTransaction.setDescription(descriotion);
                        svTransaction.setOc(logOfficer);
                        svTransaction.setSettlementType(settlementType);
                        svTransaction.setStatus(status);
                        svTransaction.setTransaction(transaction);
                        svTransaction.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
                        svTransaction.setTransactionType(tranType);
                        
                        svTransactionList.add(svTransaction);
                        
                 getCPanel().getDatabaseService().save(svTransactionList);       
                        
        } catch (Exception e) {
        }
        
        
    }
    public void AccountTransaction(int transaction_index,SvAccount ssvAccount ){
        
        //ACCOUNT TRANSACTION
            try {
            
            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();
        
        //-----------------------------part 1----------------------------------------  
         //Saving Amount Debit stork 
        accountTransaction.addAccountTransactionQueue(
                SSavingStartAccountInterface.SAVING_CAPITAL_CREDIT_CODE,
                "Creditor Amount Credit",
                ssvAccount.getCapital(),
                AccountTransactionType.AUTO);
        
        //debit Amount  customer suspend
        accountTransaction.addAccountTransactionQueue(
                SSavingStartAccountInterface.SAVING_CAPITAL_DEBIT_CODE,
                "Creditor Amount Debit",
                ssvAccount.getInterest()+ ssvAccount.getCapital(),
                AccountTransactionType.AUTO);
      //-----------------------------part 2---------pyament-------------------------------  
        //saving Amount Credit creditor
        accountTransaction.addAccountTransactionQueue(
                SSavingStartAccountInterface.SAVING_AMOUNT_CREDIT_CODE,
                "Creditor Amount Credit",
                ssvAccount.getBalance(),
                AccountTransactionType.AUTO);
    
      
        
         accountTransaction.flushAccountTransactionQueue(getCPanel().getDatabaseService(), transaction_index,
                SystemTransactions.SAVING_CODE);
        
         
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     public void AccountTransactionOpening(int transaction_index,SvAccount ssvAccount ){
        
        //ACCOUNT TRANSACTION
            try {
            
            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();

        //-----------------------------part 1----------------------------------------  
         //Saving Amount Debit stork 
        accountTransaction.addAccountTransactionQueue(
                SSavingOpeningStartAccountInterface.SAVING_OP_CAPITAL_CREDIT_CODE,
                "Open Saving Amount Credit",
                ssvAccount.getCapital(),
                AccountTransactionType.AUTO);
         //Credit Amount Interest customer
        accountTransaction.addAccountTransactionQueue(
                SSavingOpeningStartAccountInterface.SAVING_OP_INTEREST_CREDIT_CODE,
                "Open Saving Amount Credit",
                ssvAccount.getInterest(),
                AccountTransactionType.AUTO);
        //debit Amount  customer suspend
        accountTransaction.addAccountTransactionQueue(
                SSavingOpeningStartAccountInterface.SAVING_OP_CA_IN_DEBIT_CODE,
                "Open Saving Amount Debit",
                ssvAccount.getInterest()+ ssvAccount.getCapital(),
                AccountTransactionType.AUTO);
      //-----------------------------part 2----------------------------------------  
        //saving Amount Credit creditor
        accountTransaction.addAccountTransactionQueue(
                SSavingOpeningStartAccountInterface.SAVING_OP_AMOUNT_CREDIT_CODE,
                "Open Saving Amount Credit",
                ssvAccount.getBalance(),
                AccountTransactionType.AUTO);
        //saving Amount Debit Cash in hand 
        accountTransaction.addAccountTransactionQueue(
                SSavingOpeningStartAccountInterface.SAVING_OP_AMOUNT_DEBIT_CODE,
                "Open Saving Amount Credit",
                ssvAccount.getBalance(),
                AccountTransactionType.AUTO);
        
         accountTransaction.flushAccountTransactionQueue(getCPanel().getDatabaseService(), transaction_index,
                SystemTransactions.OP_SAVING_CODE);
        
         
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
            

    @Action
    public void doClose() {
        TabFunctions.closeTab(this.getCPanel());
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {

                    txtChemecode.setEnabled(false);
                    txtTypecode.setEnabled(false);
                    txtClientCode.setEnabled(false);
                    txtNomineeCode.setEnabled(false);
                    txtOfficer.setEnabled(false);
                    txtRemark1.setEnabled(false);
      
        cboAccount.setExpressEditable(true);
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnApprove, "doApprove");
        actionUtil.setAction(btnClose, "doClose");

        btnApprove.setIcon(FinacResources.getImageIcon(FinacResources.ACTION_ACCEPT, 16, 16));
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        
        cboAccount.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                SvAccount account= (SvAccount)cboAccount.getCValue();
                    txtChemecode.resetValue();
                    txtTypecode.resetValue();
                    txtClientCode.resetValue();
                    txtNomineeCode.resetValue();
                    txtOfficer.resetValue();
                    txtRemark1.resetValue();
                if(account!=null){
                    txtChemecode.setCValue(account.getSvScheme().toString());
                    txtTypecode.setCValue(account.getSvScheme().getSvType().toString());
                    txtClientCode.setCValue(account.getCustomer().getLongName());
                    txtNomineeCode.setCValue(account.getNominee().getLongName());
                    txtOfficer.setCValue(logOfficer);
                    txtRemark1.setCValue(account.getClRemark());
                  //  interestCalculationEndPeriod();
                }
                
            }
        });
        
    }



    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtTypecode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtChemecode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtClientCode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtNomineeCode = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtOfficer = new com.mac.af.component.derived.input.textfield.CIStringField();
        cLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        cLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        btnApprove = new com.mac.af.component.derived.command.button.CCButton();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        txtRemark1 = new com.mac.af.component.derived.input.textfield.CIStringField();
        cboAccount = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getAccoutList();
            }
        };
        cLIntRate = new com.mac.af.component.derived.display.label.CDLabel();

        txtOfficer.setPreferredSize(new java.awt.Dimension(79, 16));

        cLabel1.setText("Code :");

        cLabel4.setText("Type :");

        cLabel6.setText("Scheme :");

        cLabel7.setText("Customer :");

        cLabel10.setText("Nominee :");

        cLabel12.setText("Officer:");

        cLabel15.setText("Note :");

        btnApprove.setText("Start Account");

        btnClose.setText("Close");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtTypecode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtChemecode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboAccount, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtClientCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNomineeCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 199, Short.MAX_VALUE)
                        .addComponent(btnApprove, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtOfficer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtRemark1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cLIntRate, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTypecode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtChemecode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtClientCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNomineeCode, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRemark1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cLIntRate, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 224, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnApprove, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnApprove;
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.display.label.CDLabel cLIntRate;
    private com.mac.af.component.derived.display.label.CDLabel cLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cLabel7;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAccount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtChemecode;
    private com.mac.af.component.derived.input.textfield.CIStringField txtClientCode;
    private com.mac.af.component.derived.input.textfield.CIStringField txtNomineeCode;
    private com.mac.af.component.derived.input.textfield.CIStringField txtOfficer;
    private com.mac.af.component.derived.input.textfield.CIStringField txtRemark1;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTypecode;
    // End of variables declaration//GEN-END:variables

    @Override
    protected List<Component> getIdentityComponents() {
        return Arrays.asList();
    }

    @Override
    protected List<Component> getEssentialComponents() {
        return Arrays.asList(
                
                );
    }

    @Override
    protected List<Component> getOtherFieldComponents() {
        return Arrays.asList(
                );
    }

    @Override
    protected List getUneditableComponents() {
        return Arrays.asList(
                );
    }

    @Override
    protected List<CInputComponentBinder> getInputComponentBinders() {
        return Arrays.asList();
    }

    @Override
    protected Class getObjectClass() {
        return com.mac.sarving.object.SvAccount.class;
    }

    @Override
    protected void afterNewObject(Object object) {
       // ((com.mac.sarving.object.SvAccount) object).setActive(true);
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        super.initInterface();
        

    }

    @Override
    public void setNewMood() {
        super.setNewMood(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
      public  List<SvAccount> getAccoutList() {
        List<SvAccount> svAccount;

        try {
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            svAccount = databaseService.getCollection("FROM com.mac.sarving.object.SvAccount WHERE status='SAVING_PENDING' ");
            System.out.println("svAccount");
        } catch (Exception e) {
            svAccount = new ArrayList<>();
            Logger.getLogger(PCSavingStart.class.getName()).log(Level.SEVERE, null, e);
        }
        return svAccount;
    }
}
