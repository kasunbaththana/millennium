/*
 *  ClientApproval.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Nov 3, 2014, 7:57:46 AM 
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.sarving.savingclose;

import com.mac.sarving.saving_start.*;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.grid_object.AbstractGridObject;
import com.mac.sarving.object.SvAccount;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohan
 */
public class SSavingClose extends AbstractGridObject<SvAccount> {

    @Override
    protected CTableModel<SvAccount> getTableModel() {
        return new CTableModel(
                new CTableColumn[]{});
    }

    @Override
    protected AbstractObjectCreator<SvAccount> getObjectCreator() {
        return new PCSavingClose();
    }

    @Override
    protected Collection<SvAccount> getTableData() {
        try {
            return getDatabaseService().getCollection("FROM com.mac.sarving.object.SvAccount WHERE status='SAVING_START'  ");
        } catch (DatabaseException ex) {
            Logger.getLogger(SvAccount.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }
  
    
      
   

}
