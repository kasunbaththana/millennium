/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.sarving;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;

/**
 *
 * @author KASUN
 */
public class ClientTableModel extends CTableModel<Object>{

    public ClientTableModel() {
        super (new CTableColumn[]{
            new CTableColumn("Code", "code"),
            new CTableColumn("Name", "longName")
        });
    }
    
}
