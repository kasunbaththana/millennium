/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.sarving.saving_withdraw;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zutil.vehicle_valuation.SERValuation;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author KASUN
 */
public class SERWithdrow extends AbstractService  {

    
       public SERWithdrow(Component component) {
        super(component);
    }

          public List getAccount() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.sarving.object.SvAccount where cl_approve=1 ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERValuation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
          
        public List getClient() {
        List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.sarving.object.Client where client=1 and approved=1 ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERValuation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
        
        public List onLodingCalculation(String account){
            List list;
        try {
            list = getDatabaseService().getCollection("from com.mac.sarving.object.SvTransaction where status='ACTIVE' and accNo='"+account+"' ");
        } catch (DatabaseException ex) {
            list = new ArrayList();
            Logger.getLogger(SERValuation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
            
            
        }
        
        
        
    
}
