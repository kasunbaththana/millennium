/*
 *  LetterGenerator.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 1, 2014, 2:07:07 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.sarving.saving_withdraw;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.sarving.SavingTransactionStatus;
import com.mac.sarving.TableModel.SavingAccountTableModel;
import com.mac.sarving.object.SvAccount;
import com.mac.sarving.object.SvTransaction;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SWithdrawAccountInterface;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import com.mac.zutil.vehicle_valuation.object.Vehicle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kasun
 */
public class PcSWithdraw extends CPanel {
SERWithdrow sERWithdrow;
Vehicle lVehicle;
    
    public PcSWithdraw() {
        initComponents();
        initOthers();
       
    }
    
   
private String logOfficer = (String) CApplication.getSessionVariable(CApplication.USER_ID);    
private Date dateNow = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
private double capBalance = 0,intBalance,totBalance;

    @Action
    public void doClose() {
        
        TabFunctions.closeTab(this);
    }
      @Action
    public void doPrint() {
        this.recentButton.doClick();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        
         
        setLogOfficer(logOfficer);
        //SERVICE
        sERWithdrow = new SERWithdrow(this);

        //ACTIONS
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnSave, "doSave");
        actionUtil.setAction(btnNew, "doNew");
         actionUtil.setAction(btnPrint, "doPrint");
       
   
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
        btnNew.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_RESET, 16, 16));
        btnDelete.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_DELETE, 16, 16));
        btnPrint.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_PRINT_ICON, 16, 16));
      cboAccountNo.setExpressEditable(true);
      
      CTableModel tableModel = new CTableModel(
                new CTableColumn[]{
            new CTableColumn("Description", "description"),
            new CTableColumn("Type", "settlementType"),
            new CTableColumn("Credit", "creditAmount"),
            new CTableColumn("Paid", "debitAmount"),
            
        });
        
        tblTransaction.setCModel(tableModel);
        
        cboAccountNo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               setFieldData();
               lodingCalculation();
              SvAccount svTransaction= (SvAccount) cboAccountNo.getCValue();
              if(svTransaction!=null){
                  
             List listTrans= 
                     sERWithdrow.onLodingCalculation(svTransaction.getCode().toString());
             
             tblTransaction.setCValue(listTrans);
              }
            }
        });
    txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.S_WITHDRAW));
        idleMode();
       txtIntAmount.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
               
            }

            @Override
            public void keyReleased(KeyEvent e) {
              calculateAmount();
             
            }
        });
       txtCapitalAmount.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
               
            }

            @Override
            public void keyReleased(KeyEvent e) {
              calculateAmount();
            }
        });
       
        this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.SAVING_WITHDRAW_CODE);
        this.recentButton.setDatabseService(getDatabaseService());
        
         cboAccountNo.setTableModel(new SavingAccountTableModel());
        

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnClose1 = new com.mac.af.component.derived.command.button.CCButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        pnlLeft = new javax.swing.JPanel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cboAccountNo = new com.mac.af.component.derived.input.combobox.CIComboBox()
        {
            @Override
            public List getComboData(){
                return sERWithdrow.getAccount();
            }
        }

        ;
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        txtIntAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        txtmemo = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtOfficer = new com.mac.af.component.derived.input.textfield.CIStringField();
        txttransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jPanel1 = new javax.swing.JPanel();
        cDLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        txtcName = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtcNamefirst = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnNew = new com.mac.af.component.derived.command.button.CCButton();
        txtType = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtsheme = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnDelete = new com.mac.af.component.derived.command.button.CCButton();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCapBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        txtIntBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel8 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel9 = new com.mac.af.component.derived.display.label.CDLabel();
        txtStartDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel15 = new com.mac.af.component.derived.display.label.CDLabel();
        txtEndDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLInt = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel16 = new com.mac.af.component.derived.display.label.CDLabel();
        txtCapitalAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        btnPrint = new com.mac.af.component.derived.command.button.CCButton();
        pnlRight = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTransaction = new com.mac.af.component.derived.input.table.CITable();

        btnClose1.setText("Close");

        jSplitPane1.setDividerLocation(800);

        cDLabel2.setText("Account No :");

        btnClose.setText("Close");

        btnSave.setText("Save");

        cDLabel10.setText("Int Amount:");

        cDLabel11.setText("Memo :");

        cDLabel13.setText("Officer :");

        cDLabel3.setText("Date :");

        cDLabel4.setText("Type :");

        cDLabel5.setText("Scheme :");

        cDLabel6.setText("Tot Balance :");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Client Information"));

        cDLabel14.setText("Client :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(cDLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtcName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtcNamefirst, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtcNamefirst, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(txtcName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnNew.setText("New");

        cDLabel12.setText("Reference:");

        btnDelete.setText("Delete");

        cDLabel7.setText("Capital Bal :");

        cDLabel8.setText("Int Bal :");

        cDLabel9.setText("Start Date :");

        cDLabel15.setText("End Date :");

        cDLInt.setForeground(new java.awt.Color(153, 153, 153));
        cDLInt.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        cDLabel16.setText("Capital Amount:");

        txtCapitalAmount.setText("0.0");

        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlLeftLayout = new javax.swing.GroupLayout(pnlLeft);
        pnlLeft.setLayout(pnlLeftLayout);
        pnlLeftLayout.setHorizontalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlLeftLayout.createSequentialGroup()
                                .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtCapBalance, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlLeftLayout.createSequentialGroup()
                                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlLeftLayout.createSequentialGroup()
                                        .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtIntBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(pnlLeftLayout.createSequentialGroup()
                                            .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(pnlLeftLayout.createSequentialGroup()
                                                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(20, 20, 20))
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                                                    .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                                            .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(cboAccountNo, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                                                .addComponent(txtReferenceNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addGroup(pnlLeftLayout.createSequentialGroup()
                                            .addComponent(cDLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(40, 40, 40)
                                            .addComponent(txtOfficer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cDLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cDLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
                                    .addComponent(cDLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txttransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                                    .addComponent(txtType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtsheme, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(193, 193, 193))
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlLeftLayout.createSequentialGroup()
                                        .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pnlLeftLayout.createSequentialGroup()
                                        .addComponent(cDLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(pnlLeftLayout.createSequentialGroup()
                                            .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txtBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(pnlLeftLayout.createSequentialGroup()
                                            .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(pnlLeftLayout.createSequentialGroup()
                                                    .addComponent(cDLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(33, 33, 33))
                                                .addGroup(pnlLeftLayout.createSequentialGroup()
                                                    .addComponent(cDLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addGap(16, 16, 16)))
                                            .addGap(2, 2, 2)
                                            .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(txtIntAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                                                .addComponent(txtmemo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                    .addGroup(pnlLeftLayout.createSequentialGroup()
                                        .addComponent(cDLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(7, 7, 7)
                                        .addComponent(txtCapitalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 347, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cDLInt, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(193, 193, 193))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(163, 163, 163))
        );
        pnlLeftLayout.setVerticalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txttransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtType, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAccountNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cDLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtsheme, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cDLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtCapBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cDLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cDLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtIntBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(cDLInt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(21, 21, 21)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIntAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCapitalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtmemo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(46, 46, 46))))
        );

        jSplitPane1.setLeftComponent(pnlLeft);

        pnlRight.setLayout(new javax.swing.BoxLayout(pnlRight, javax.swing.BoxLayout.LINE_AXIS));
        jSplitPane1.setRightComponent(pnlRight);

        tblTransaction.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblTransaction);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 785, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPrintActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnClose1;
    private com.mac.af.component.derived.command.button.CCButton btnDelete;
    private com.mac.af.component.derived.command.button.CCButton btnNew;
    private com.mac.af.component.derived.command.button.CCButton btnPrint;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.display.label.CDLabel cDLInt;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel15;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel16;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel8;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel9;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAccountNo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlRight;
    private com.mac.af.component.derived.input.table.CITable tblTransaction;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBalance;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtCapBalance;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtCapitalAmount;
    private com.mac.af.component.derived.input.textfield.CIDateField txtEndDate;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtIntAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtIntBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtOfficer;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIDateField txtStartDate;
    private com.mac.af.component.derived.input.textfield.CIStringField txtType;
    private com.mac.af.component.derived.input.textfield.CIStringField txtcName;
    private com.mac.af.component.derived.input.textfield.CIStringField txtcNamefirst;
    private com.mac.af.component.derived.input.textfield.CIStringField txtmemo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtsheme;
    private com.mac.af.component.derived.input.textfield.CIDateField txttransactionDate;
    // End of variables declaration//GEN-END:variables
   private SystemPayment systemPayment;
   private CashierSession  cashierSession ;
   private RecentButton recentButton;
    private void idleMode()
    {
        txtcName.setEnabled(false);
        txtcNamefirst.setEnabled(false);
        txtType.setEnabled(false);
        txtsheme.setEnabled(false);
        txttransactionDate.setEnabled(false);
        txtOfficer.setEnabled(false);
        txtReferenceNo.setEnabled(false);
        cboAccountNo.setEnabled(false);
        txtIntAmount.setEnabled(false);
        txtmemo.setEnabled(false);
        txtCapBalance.setEnabled(false);
        txtIntBalance.setEnabled(false);
        txtBalance.setEnabled(false);
        txtStartDate.setEnabled(false);
        txtEndDate.setEnabled(false);
        txtCapitalAmount.setEnabled(false);
    }

    public String getLogOfficer() {
        return logOfficer;
    }

    public void setLogOfficer(String logOfficer) {
        this.logOfficer = logOfficer;
    }
    
    public void calculateAmount(){
  if(isEndPeriod()){
    if((capBalance+intBalance)< txtIntAmount.getCValue())
    {
        txtIntAmount.setCValue((intBalance));
        txtCapitalAmount.setCValue(capBalance);
        setInterest(intBalance);
         setCapital(capBalance);
        
      mOptionPane.showMessageDialog(
              this, 
              "Action Faild !",
              "WARNING_MESSAGE",
              mOptionPane.WARNING_MESSAGE);   
    }else{
           if((intBalance)>=txtIntAmount.getCValue())
    {
        setInterest(txtIntAmount.getCValue());
         setCapital(txtCapitalAmount.getCValue());
    }
    }
    
    
    } else{
       System.out.println("txtIntAmount.getCValue()  "+txtIntAmount.getCValue());
        if((intBalance)>=txtIntAmount.getCValue())
    {
        setInterest(txtIntAmount.getCValue());
         setCapital(txtCapitalAmount.getCValue());
    }
    else
    {
            txtIntAmount.setCValue(intBalance);
             setInterest(intBalance);
         setCapital(0.0);
      mOptionPane.showMessageDialog(
              this, 
              "Action Faild !",
              "WARNING_MESSAGE",
              mOptionPane.WARNING_MESSAGE);   
    }
    }
    }
    
//    public void interestCalculationEndPeriod()
//    {
//        SvAccount ssvAccount=(SvAccount) cboAccountNo.getCValue();
//        double intRate=0,capitalAmount=0,intAmount,calRate=0;
//        int dayCount=0;
//        try {
//            if(ssvAccount!=null){
//               intRate = ssvAccount.getSvScheme().getFreeMatchInt();
//               
//               capitalAmount = ssvAccount.getBalance();
//               dayCount=dateCount(ssvAccount);
//               
//               calRate =  intRate / dayCount;
//               
//               intAmount=(capitalAmount*calRate/100);
//              
//               cDLInt.setCValue(intAmount+"");
//               
//            }
//        } catch (Exception e) {
//        }
//    }
    public int dateCount(SvAccount acc){
        
       Date firstDate= acc.getTransactionDate();
      return  daysBetween(firstDate, dateNow);
    }
    public int daysBetween(Date d1, Date d2){
             return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
     }
    

@Action
    public void doNew() 
{
    txtReferenceNo.resetValue();
        txtcName.setEnabled(false);
        txtcNamefirst.setEnabled(false);
        txtType.setEnabled(false);
        txtsheme.setEnabled(false);
        txttransactionDate.setEnabled(true);
        txtOfficer.setEnabled(false);
        txtReferenceNo.setEnabled(false);
        cboAccountNo.setEnabled(true);
        txtIntAmount.setEnabled(true);
        txtmemo.setEnabled(true);
         btnSave.setEnabled(true);
       btnNew.setEnabled(false);
       txtCapBalance.setEnabled(false);
       txtIntBalance.setEnabled(false);
       txtBalance.setEnabled(false);
       txtCapitalAmount.setEnabled(true);
}
  private void reSetMode()
  {
      txtReferenceNo.resetValue();
   
        txtcName.resetValue();
        txtcNamefirst.resetValue();
        txtType.resetValue();
        txtsheme.resetValue();
        txttransactionDate.resetValue();
        txtBalance.resetValue();
        txtOfficer.resetValue();
        txtReferenceNo.resetValue();
        cboAccountNo.resetValue();
        txtIntAmount.resetValue();
        txtmemo.resetValue();
       btnSave.setEnabled(false);
       btnNew.setEnabled(true);
       txtCapBalance.resetValue();
       txtIntBalance.resetValue();
       txtBalance.resetValue();
       txtCapitalAmount.resetValue();
  }

@Action
    public void doSave() {
    
        int q = mOptionPane.showConfirmDialog(null, "Do you sure want to save this transaction?", "Save", mOptionPane.YES_NO_OPTION);

        
        if (q == mOptionPane.YES_OPTION) {
            
            if(saveData()==1)
            {
                
            mOptionPane.showMessageDialog(null, "Successfully saved !!!", "Save", mOptionPane.INFORMATION_MESSAGE);
            reSetMode();
            }else{
                mOptionPane.showMessageDialog(null, "Save Faild !!!", "Save", mOptionPane.WARNING_MESSAGE);
            }
            
          
        }
    
}

private double interest;
private double capital;
private double calInterest;
private double caCapital;

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public double getCalInterest() {
        return calInterest;
    }

    public void setCalInterest(double calInterest) {
        this.calInterest = calInterest;
    }

    public double getCaCapital() {
        return caCapital;
    }

    public void setCaCapital(double caCapital) {
        this.caCapital = caCapital;
    }

private int saveData()
{
    SvTransaction svTransaction=new SvTransaction();
    SvAccount ssvAccount=(SvAccount) cboAccountNo.getCValue();
    System.out.println("getInterest"+getInterest()+" getCapital "+getCapital());
    double AmountAc=getInterest()+getCapital();
   
     int transaction_index=0;
    if(validation() && AmountAc > 0)
    {
        try {
              //transaction
            cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
             transaction_index = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.SAVING_WITHDRAW_CODE,
                txtReferenceNo.getCValue(),
                "",
                null,
                cashierSession.getIndexNo(),
                null,
                txtmemo.getCValue());
            
             boolean isPaymentOk = showPaymentDialog(SystemTransactions.SAVING_WITHDRAW_CODE);
        if (isPaymentOk) {
            
              savePayment(transaction_index);
            if(transaction_index !=0 )
            {
            if(txtIntAmount.getCValue() > 0){
                
            svTransaction.setAccNo(ssvAccount);
            svTransaction.setTransactionType(SystemTransactions.SAVING_WITHDRAW_CODE);
            svTransaction.setTransaction(transaction_index);
            svTransaction.setTransactionDate(txttransactionDate.getCValue());
            svTransaction.setCreditAmount(00.0);
            svTransaction.setDebitAmount(txtIntAmount.getCValue());
            svTransaction.setSettlementType(SavingTransactionStatus.INTEREST);
            svTransaction.setDescription(txtmemo.getCValue());
            svTransaction.setOc(logOfficer);
            svTransaction.setActionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
            svTransaction.setStatus("ACTIVE");
            
            getDatabaseService().save(svTransaction);
            }if(txtCapitalAmount.getCValue() > 0){
             svTransaction.setAccNo(ssvAccount);
            svTransaction.setTransactionType(SystemTransactions.SAVING_WITHDRAW_CODE);
            svTransaction.setTransaction(transaction_index);
            svTransaction.setTransactionDate(txttransactionDate.getCValue());
            svTransaction.setCreditAmount(00.0);
            svTransaction.setDebitAmount(txtCapitalAmount.getCValue());
            svTransaction.setSettlementType(SavingTransactionStatus.CAPITAL);
            svTransaction.setDescription(txtmemo.getCValue());
            svTransaction.setOc(logOfficer);
            svTransaction.setActionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
            svTransaction.setStatus("ACTIVE");
            
            getDatabaseService().save(svTransaction);
                
            }
                //ACCOUNT TRANSACTION
                SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
            accountTransaction.beginAccountTransactionQueue();
            //credit Amount
//            //debit Amount
            accountTransaction.addAccountTransactionQueue(
                    SWithdrawAccountInterface.SAVING_WITHDRAW_AMOUNT_DEBIT_CODE,
                    "Saving Withdraw Amount Debit",
                    AmountAc,
                    AccountTransactionType.AUTO);
            
            //--------------------------part 2--------------------------------
            
           
            //Interest credit Amount
            accountTransaction.addAccountTransactionQueue(
                    SWithdrawAccountInterface.SAVING_WITHDRAW_INTEREST_CREDIT_CODE,
                    "Creditor Withdraw Interest Credit"+ssvAccount.getCode(),
                    getInterest(),
                    AccountTransactionType.AUTO);
            //Interest Debit Amount
            accountTransaction.addAccountTransactionQueue(
                    SWithdrawAccountInterface.SAVING_WITHDRAW_INTEREST_DEBIT_CODE,
                    "Creditor Withdraw Interest Debit"+ssvAccount.getCode(),
                    getInterest(),
                    AccountTransactionType.AUTO);
        

            accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction_index,
                 SystemTransactions.SAVING_WITHDRAW_CODE);
            
            //REPORT
                 Map<String, Object> paramsc = new HashMap<>();
                paramsc.put("TRANSACTION_NO", transaction_index);
                TransactionUtil.PrintReport(getDatabaseService(), SystemTransactions.SAVING_WITHDRAW_CODE, paramsc);
            
            }
            return 1;
            }
        } catch (ApplicationException | DatabaseException ex) {
            Logger.getLogger(PcSWithdraw.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        finally
        {
            try {
               getDatabaseService().commitLocalTransaction();
               getDatabaseService().close();
            } catch (Exception e) {
            }
        }
}
        return 0;
}

private boolean validation()
{
    if(cboAccountNo.getCValue()==null)
    {
        mOptionPane.showMessageDialog(this, "Action Faild Please select Account !", "WARNING_MESSAGE", mOptionPane.WARNING_MESSAGE);
    }
    else if(txtIntAmount.getCValue() <= 0 && txtCapitalAmount.getCValue() <= 0 )
    {
         mOptionPane.showMessageDialog(this, "Action Faild Please Enter Valid Amount !", "WARNING_MESSAGE", mOptionPane.WARNING_MESSAGE);
    }
    else
    {
        return true;
        
    }
    
    return false;
}
   
private void setFieldData()
{
    SvAccount  account=(SvAccount) cboAccountNo.getCValue();
    
    if(account!=null)
    {
        txtcNamefirst.setCValue(account.getCustomer().getNicNo());
        txtcName.setCValue(account.getCustomer().getLongName());
        txtsheme.setCValue(account.getSvScheme().getCode()+"-"+account.getSvScheme().getDescription());
        txtCapBalance.setCValue(capBalance);
        txtIntBalance.setCValue(intBalance);
        txtBalance.setCValue((capBalance+intBalance));
        txtOfficer.setCValue(getLogOfficer());
        txtEndDate.setCValue(account.getEndDate());
        txtStartDate.setCValue(account.getTransactionDate());
    }
    
    
}
public boolean isEndPeriod() {
    boolean value=false;
     SvAccount  account=(SvAccount) cboAccountNo.getCValue();
    try {
        System.out.println("END-"+account.getEndDate()+"NOW"+dateNow);
       value= (account.getEndDate().before(dateNow));
    } catch (Exception e) {
        value =false;
    }
     lodingCalculation();
     
    return value ;
}



public void lodingCalculation() {
    try {
    SvAccount  account=(SvAccount) cboAccountNo.getCValue();
   capBalance=0;
   intBalance=0; 
    if(account!=null)
    {
   List<SvTransaction> listTrans= sERWithdrow.onLodingCalculation(account.getCode());
        for(SvTransaction svTransaction:listTrans){
            if(svTransaction.getSettlementType().equals(SavingTransactionStatus.CAPITAL)){
            capBalance += svTransaction.getCreditAmount()-svTransaction.getDebitAmount();
            }
            if(svTransaction.getSettlementType().equals(SavingTransactionStatus.INTEREST)){
            intBalance += svTransaction.getCreditAmount()-svTransaction.getDebitAmount();
            }
        }
    }
    } catch (Exception e) {
    }
  //  interestCalculationEndPeriod();
}


   public boolean showPaymentDialog(String transactionTypeCode) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);
double AmountAc=getInterest()+getCapital();

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                txtReferenceNo.getCValue(),
                "",
                txttransactionDate.getCValue(),
                AmountAc,
                ChequeType.COMPANY);
//        boolean isPaymentOk = systemPayment.showPaymentDialog(
//                getDatabaseService(),
//                receipt.getReferenceNo(),
//                receipt.getDocumentNo(),
//                receipt.getTransactionDate(),
//                receipt.getPaymentAmount(),
//                ChequeType.CLIENT);

        return isPaymentOk;
    }
   
   
     public void savePayment(int transactionIndex) throws DatabaseException {
        systemPayment.savePayment(
                getDatabaseService(),
                cashierSession,
                transactionIndex,
                null);
    }
     
}
