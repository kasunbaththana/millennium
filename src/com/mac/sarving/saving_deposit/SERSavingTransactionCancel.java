/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.sarving.saving_deposit;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zsystem.transaction.settlement.SettlementStatus;
import com.mac.zsystem.transaction.transaction.SystemTransactionStatus;
import java.awt.Component;
import java.util.HashMap;

/**
 *
 * @author Thilanga-pc
 */
public class SERSavingTransactionCancel extends AbstractService {

    public SERSavingTransactionCancel(Component component) {
        super(component);
    }
    public void cancel(int indexNo) throws DatabaseException {
        //TRANSACTION
        cancelTransaction(indexNo);

        //ACCOUNT TRANSACTION
        cancelAccountTransaction(indexNo);
        
        //SV TRANSACTION CANCEL
        cancelSvTransCancel(indexNo);
    }
//kasun
    
    public void cancelTransaction(int indexNo) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.Transaction "
                + "SET status=:STATUS "
                + "WHERE indexNo=:INDEX_NO";

        HashMap<String, Object> params = new HashMap<>();
        params.put("INDEX_NO", indexNo);
        params.put("STATUS", SystemTransactionStatus.CANCEL);

        getDatabaseService().executeUpdate(hql, params);
    }

//    cancel account transaction
    public void cancelAccountTransaction(int transaction) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.zsystem.transaction.transaction_cancel.object.AccountTransaction "
                + "SET status=:STATUS "
                + "WHERE transaction=:TRANSACTION";

        HashMap<String, Object> hashMapsettelment = new HashMap<>();
        hashMapsettelment.put("STATUS", SettlementStatus.SETTLEMENT_CANCEL);
        hashMapsettelment.put("TRANSACTION", transaction);
        getDatabaseService().executeUpdate(hql, hashMapsettelment);

    }
    
     public void cancelSvTransCancel(int transaction) throws DatabaseException {
        String hql = "UPDATE "
                + "com.mac.sarving.object.SvTransaction "
                + "SET status=:STATUS "
                + "WHERE transaction=:TRANSACTION";

        HashMap<String, Object> hashMapsettelment = new HashMap<>();
        hashMapsettelment.put("STATUS", SettlementStatus.SETTLEMENT_CANCEL);
        hashMapsettelment.put("TRANSACTION", transaction);
        getDatabaseService().executeUpdate(hql, hashMapsettelment);

    }
}