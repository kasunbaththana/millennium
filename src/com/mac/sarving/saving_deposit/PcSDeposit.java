/*
 *  LetterGenerator.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 1, 2014, 2:07:07 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.sarving.saving_deposit;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.environment.tab.TabFunctions;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.sarving.SavingTransactionStatus;
import com.mac.sarving.object.SvAccount;
import com.mac.sarving.object.SvTransaction;
import com.mac.zsystem.recent_button.RecentButton;
import com.mac.zsystem.settings.transaction_settings.TransactionUtil;
import com.mac.zsystem.transaction.account.SystemAccountTransaction;
import com.mac.zsystem.transaction.account.account_setting.AccountSettingCreditOrDebit;
import com.mac.zsystem.transaction.account.account_setting.AccountTransactionType;
import com.mac.zsystem.transaction.account.account_setting.system_interface.SDepositAccountInterface;
import com.mac.zsystem.transaction.cashier.SystemCashier;
import com.mac.zsystem.transaction.cashier.object.CashierSession;
import com.mac.zsystem.transaction.payment.ChequeType;
import com.mac.zsystem.transaction.payment.SystemPayment;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.util.generator.ReferenceGenerator;
import com.mac.zutil.vehicle_valuation.object.Vehicle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kasun
 */
public class PcSDeposit extends CPanel {
SERDeposit SERDeposit;
Vehicle lVehicle;
private SystemPayment systemPayment;
 private CashierSession cashierSession ;
  private String logOfficer = (String) CApplication.getSessionVariable(CApplication.USER_ID);
  private Date dateNow = (Date) CApplication.getSessionVariable(CApplication.WORKING_DATE);
private RecentButton recentButton;
    
    public PcSDeposit() {
        initComponents();
        initOthers();
       
    }
    
   


    @Action
    public void doClose() {
        
        TabFunctions.closeTab(this);
    }
      @Action
    public void doPrint() {
        this.recentButton.doClick();
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        
         
        setLogOfficer(logOfficer);
        //SERVICE
        SERDeposit = new SERDeposit(this);

        //ACTIONS
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnClose, "doClose");
        actionUtil.setAction(btnSave, "doSave");
        actionUtil.setAction(btnNew, "doNew");
        actionUtil.setAction(btnPrint, "doPrint");
       cboAccountNo.setExpressEditable(true);
   
        btnClose.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_CLOSE, 16, 16));
        btnSave.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_SAVE, 16, 16));
        btnNew.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_RESET, 16, 16));
        btnDelete.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_DELETE, 16, 16));
        btnPrint.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_PRINT_ICON, 16, 16));
        
      
        
        cboAccountNo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               setFieldData();

            }
        });
        
    txtReferenceNo.setGenerator(ReferenceGenerator.getInstance(ReferenceGenerator.S_DEPOSIT));
        

        idleMode();
       cboAccountNo.setExpressEditable(true);
       
       
       this.recentButton = new RecentButton();
        this.recentButton.setTransactionType(SystemTransactions.SAVING_DEPOSIT_CODE);
        this.recentButton.setDatabseService(getDatabaseService());
        
        

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnClose1 = new com.mac.af.component.derived.command.button.CCButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        pnlLeft = new javax.swing.JPanel();
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        btnClose = new com.mac.af.component.derived.command.button.CCButton();
        btnSave = new com.mac.af.component.derived.command.button.CCButton();
        cDLabel10 = new com.mac.af.component.derived.display.label.CDLabel();
        cboAccountNo = new com.mac.af.component.derived.input.combobox.CIComboBox()
        {
            @Override
            public List getComboData(){
                return SERDeposit.getAccount();
            }
        }

        ;
        cDLabel11 = new com.mac.af.component.derived.display.label.CDLabel();
        txtAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        cDLabel13 = new com.mac.af.component.derived.display.label.CDLabel();
        txtmemo = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtOfficer = new com.mac.af.component.derived.input.textfield.CIStringField();
        txttransactionDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        txtBalance = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jPanel1 = new javax.swing.JPanel();
        cDLabel14 = new com.mac.af.component.derived.display.label.CDLabel();
        txtcName = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtcNamefirst = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnNew = new com.mac.af.component.derived.command.button.CCButton();
        txtType = new com.mac.af.component.derived.input.textfield.CIStringField();
        txtsheme = new com.mac.af.component.derived.input.textfield.CIStringField();
        cDLabel12 = new com.mac.af.component.derived.display.label.CDLabel();
        txtReferenceNo = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnDelete = new com.mac.af.component.derived.command.button.CCButton();
        btnPrint = new com.mac.af.component.derived.command.button.CCButton();
        pnlRight = new javax.swing.JPanel();

        btnClose1.setText("Close");

        jSplitPane1.setDividerLocation(800);

        cDLabel2.setText("Account No :");

        btnClose.setText("Close");

        btnSave.setText("Save");

        cDLabel10.setText("Amount:");

        cDLabel11.setText("Memo :");

        cDLabel13.setText("Officer :");

        cDLabel3.setText("Date :");

        cDLabel4.setText("Type :");

        cDLabel5.setText("Scheme :");

        cDLabel6.setText("Balance :");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Client Information"));

        cDLabel14.setText("Client :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(cDLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtcNamefirst, javax.swing.GroupLayout.PREFERRED_SIZE, 559, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcName, javax.swing.GroupLayout.PREFERRED_SIZE, 559, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtcNamefirst, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(txtcName, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnNew.setText("New");

        cDLabel12.setText("Reference:");

        btnDelete.setText("Delete");

        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlLeftLayout = new javax.swing.GroupLayout(pnlLeft);
        pnlLeft.setLayout(pnlLeftLayout);
        pnlLeftLayout.setHorizontalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlLeftLayout.createSequentialGroup()
                                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(cDLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(cDLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(20, 20, 20))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                                        .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtAmount, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboAccountNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtmemo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtReferenceNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addComponent(cDLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(40, 40, 40)
                                .addComponent(txtOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cDLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cDLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cDLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cDLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txttransactionDate, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                                    .addComponent(txtBalance, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtsheme, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                                .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );
        pnlLeftLayout.setVerticalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAccountNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txttransactionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtType, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtmemo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36))
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtsheme, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtReferenceNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cDLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21))
        );

        jSplitPane1.setLeftComponent(pnlLeft);

        pnlRight.setLayout(new javax.swing.BoxLayout(pnlRight, javax.swing.BoxLayout.LINE_AXIS));
        jSplitPane1.setRightComponent(pnlRight);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPrintActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnClose;
    private com.mac.af.component.derived.command.button.CCButton btnClose1;
    private com.mac.af.component.derived.command.button.CCButton btnDelete;
    private com.mac.af.component.derived.command.button.CCButton btnNew;
    private com.mac.af.component.derived.command.button.CCButton btnPrint;
    private com.mac.af.component.derived.command.button.CCButton btnSave;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel10;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel11;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel12;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel13;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel14;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAccountNo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlRight;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAmount;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtBalance;
    private com.mac.af.component.derived.input.textfield.CIStringField txtOfficer;
    private com.mac.af.component.derived.input.textfield.CIStringField txtReferenceNo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtType;
    private com.mac.af.component.derived.input.textfield.CIStringField txtcName;
    private com.mac.af.component.derived.input.textfield.CIStringField txtcNamefirst;
    private com.mac.af.component.derived.input.textfield.CIStringField txtmemo;
    private com.mac.af.component.derived.input.textfield.CIStringField txtsheme;
    private com.mac.af.component.derived.input.textfield.CIDateField txttransactionDate;
    // End of variables declaration//GEN-END:variables
  
    private void idleMode()
    {
        
        txtcName.setEnabled(false);
        txtcNamefirst.setEnabled(false);
        txtType.setEnabled(false);
        txtsheme.setEnabled(false);
        txttransactionDate.setEnabled(false);
        txtBalance.setEnabled(false);
        txtOfficer.setEnabled(false);
        txtReferenceNo.setEnabled(false);
        cboAccountNo.setEnabled(false);
        txtAmount.setEnabled(false);
        txtmemo.setEnabled(false);
        
    }

    public String getLogOfficer() {
        return logOfficer;
    }

    public void setLogOfficer(String logOfficer) {
        this.logOfficer = logOfficer;
    }

@Action
    public void doNew() 
{
        txtReferenceNo.resetValue();
        txtcName.setEnabled(false);
        txtcNamefirst.setEnabled(false);
        txtType.setEnabled(false);
        txtsheme.setEnabled(false);
        txttransactionDate.setEnabled(true);
        txtBalance.setEnabled(false);
        txtOfficer.setEnabled(false);
        txtReferenceNo.setEnabled(false);
        cboAccountNo.setEnabled(true);
        txtAmount.setEnabled(true);
        txtmemo.setEnabled(true);
         btnSave.setEnabled(true);
       btnNew.setEnabled(false);
}
  private void reSetMode()
  {
      txtReferenceNo.resetValue();
   
        txtcName.resetValue();
        txtcNamefirst.resetValue();
        txtType.resetValue();
        txtsheme.resetValue();
        txttransactionDate.resetValue();
        txtBalance.resetValue();
        txtOfficer.resetValue();
        txtReferenceNo.resetValue();
        cboAccountNo.resetValue();
        txtAmount.resetValue();
        txtmemo.resetValue();
      btnSave.setEnabled(false);
       btnNew.setEnabled(true);
  }

@Action
    public void doSave() {
    
        int q = mOptionPane.showConfirmDialog(null, "Do you sure want to save this transaction?", "Save", mOptionPane.YES_NO_OPTION);

        
        if (q == mOptionPane.YES_OPTION) {
            
            if(saveData()==1)
            {
                
            mOptionPane.showMessageDialog(null, "Successfully saved !!!", "Save", mOptionPane.INFORMATION_MESSAGE);
            reSetMode();
            }
            
          
        }
    
}

private int saveData()
{
    SvTransaction svTransaction=new SvTransaction();
    SvAccount ssvAccount=(SvAccount) cboAccountNo.getCValue();
    double AmountAc=txtAmount.getCValue();
   
     int transaction_index=0;
    if(validation())
    {
        try {
            //transaction
             cashierSession = SystemCashier.getCurrentCashierSession(getDatabaseService());
             transaction_index = SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.SAVING_DEPOSIT_CODE,
                txtReferenceNo.getCValue(),
                "",
                null,
                cashierSession.getIndexNo(),
                null,
                txtmemo.getCValue());
            
            if(transaction_index !=0 )
            {
            
            svTransaction.setAccNo(ssvAccount);
            svTransaction.setTransactionType(SystemTransactions.SAVING_DEPOSIT_CODE);
            svTransaction.setTransaction(transaction_index);
            svTransaction.setTransactionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
            svTransaction.setCreditAmount(txtAmount.getCValue());
            svTransaction.setDebitAmount(00.0);
            svTransaction.setDescription(txtmemo.getCValue());
            svTransaction.setSettlementType(SavingTransactionStatus.CAPITAL);
            svTransaction.setOc(logOfficer);
            svTransaction.setActionDate((Date) CApplication.getSessionVariable(CApplication.WORKING_DATE));
            svTransaction.setStatus("ACTIVE");
            
            getDatabaseService().save(svTransaction);
            
            
                boolean isPaymentOk = showPaymentDialog(SystemTransactions.SAVING_DEPOSIT_CODE,AmountAc);
                    if (isPaymentOk) {

                          savePayment(transaction_index);
            
            //ACCOUNT TRANSACTION
            
            SystemAccountTransaction accountTransaction = SystemAccountTransaction.getInstance();
        accountTransaction.beginAccountTransactionQueue();

        accountTransaction.addAccountTransactionQueue(
                SDepositAccountInterface.SAVING_DEPOSIT_AMOUNT_CREDIT_CODE,
                "Creditor Deposit Amount Credit",
                AmountAc,
                AccountTransactionType.AUTO);
//        //debit Amount
//        accountTransaction.addAccountTransactionQueue(
//                SDepositAccountInterface.SAVING_DEPOSIT_AMOUNT_DEBIT_CODE,
//                "Creditor Deposit Amount Debit",
//                AmountAc,
//                AccountTransactionType.AUTO);
        
        //credit Amount
        accountTransaction.addAccountTransactionQueue(
                SDepositAccountInterface.SAVING_DEPOSIT_DUE_CREDIT_CODE,
                "Creditor Due Amount Credit",
                AmountAc,
                AccountTransactionType.AUTO);
        //debit Amount
        accountTransaction.addAccountTransactionQueue(
                SDepositAccountInterface.SAVING_DEPOSIT_DUE_DEBIT_CODE,
                "Creditor Due Amount Debit",
                AmountAc,
                AccountTransactionType.AUTO);
        
           accountTransaction.flushAccountTransactionQueue(getDatabaseService(), transaction_index,
                SystemTransactions.SAVING_DEPOSIT_CODE);
           
           //REPORT
                 Map<String, Object> paramsc = new HashMap<>();
                paramsc.put("TRANSACTION_NO", transaction_index);
                TransactionUtil.PrintReport(getDatabaseService(), SystemTransactions.SAVING_DEPOSIT_CODE, paramsc);
            
            return 1;
            }
            }
        } catch (Exception ex) {
            Logger.getLogger(PcSDeposit.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        finally
        {
            try {
               getDatabaseService().commitLocalTransaction();
               getDatabaseService().close();
            } catch (Exception e) {
            }
        }
}
        return 0;
}
   public boolean showPaymentDialog(String transactionTypeCode,double balance) throws DatabaseException {
        systemPayment = SystemPayment.getInstance(transactionTypeCode);
    //    double AmountAc=getInterest()+getCapital();

        boolean isPaymentOk = systemPayment.showPaymentDialog(
                getDatabaseService(),
                txtReferenceNo.getCValue(),
                "",
                dateNow,
                balance,
                ChequeType.CLIENT);

        return isPaymentOk;
    }
   public void savePayment(int transactionIndex) throws DatabaseException {
        systemPayment.savePayment(
                getDatabaseService(),
                cashierSession,
                transactionIndex,
                null);
    }
  
private boolean validation()
{
    if(cboAccountNo.getCValue()==null)
    {
        mOptionPane.showMessageDialog(this, "Action Faild Please select Account !", "WARNING_MESSAGE", mOptionPane.WARNING_MESSAGE);
    }
    else if(txtAmount.getCValue() <= 0)
    {
         mOptionPane.showMessageDialog(this, "Action Faild Please Enter Valid Amount !", "WARNING_MESSAGE", mOptionPane.WARNING_MESSAGE);
    }
    else
    {
        return true;
        
    }
    
    
    return false;
}
   
private void setFieldData()
{
    SvAccount  account=(SvAccount) cboAccountNo.getCValue();
    
    if(account!=null)
    {
        txtcNamefirst.setCValue(account.getCustomer().getNicNo());
        txtcName.setCValue(account.getCustomer().getLongName());
    //    txtType.setCValue(account.getSvType().getCode()+"-"+account.getSvType().getDescription());
        txtsheme.setCValue(account.getSvScheme().getCode()+"-"+account.getSvScheme().getDescription());
      //  txttransactionDate.setCValue(false);
        txtBalance.setCValue(account.getBalance());
        txtOfficer.setCValue(getLogOfficer());
    }
    
    
}
  
     
    
}
