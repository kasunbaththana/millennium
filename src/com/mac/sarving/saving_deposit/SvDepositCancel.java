/*
 *  ReceiptCancel.java
 * 
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 * 
 *  Created on Oct 15, 2014, 8:15:17 AM
 *  Copyrights channa mohan, All rights reserved.
 * 
 */
package com.mac.sarving.saving_deposit;

import com.mac.af.core.database.DatabaseException;
import com.mac.zsystem.transaction.transaction.SystemTransactions;
import com.mac.zsystem.transaction.transaction_cancel.object.Transaction;

/**
 *
 * @author mohan
 */
public class SvDepositCancel extends SavingTransactionCancel {

    @Override
    protected String getTransactionType() {
        return SystemTransactions.SAVING_DEPOSIT_CODE;
    }

    @Override
    protected void cancelAdditional(Transaction transaction) throws DatabaseException {
        SystemTransactions.insertTransaction(
                getDatabaseService(),
                SystemTransactions.SAVING_DEPOSIT_CANCEL_CODE,
                transaction.getReferenceNo(),
                null,
                null,
                null,
               null,
                "Saving Deposit Cancel");

        
    }

    
    
}
