package com.mac.recovery_visit.object;
// Generated Sep 24, 2015 9:31:01 AM by Hibernate Tools 3.2.1.GA


import java.util.Date;

/**
 * Settlement generated by hbm2java
 */
public class Settlement  implements java.io.Serializable {


     private Integer indexNo;
     private Client client;
     private Loan loan;
     private Date transactionDate;
     private String branch;
     private Integer installmentNo;
     private int transaction;
     private String transactionType;
     private String description;
     private double amount;
     private double balanceAmount;
     private Date dueDate;
     private String settlementType;
     private String status;

    public Settlement() {
    }

	
    public Settlement(Date transactionDate, String branch, int transaction, String transactionType, double amount, double balanceAmount, Date dueDate, String settlementType, String status) {
        this.transactionDate = transactionDate;
        this.branch = branch;
        this.transaction = transaction;
        this.transactionType = transactionType;
        this.amount = amount;
        this.balanceAmount = balanceAmount;
        this.dueDate = dueDate;
        this.settlementType = settlementType;
        this.status = status;
    }
    public Settlement(Client client, Loan loan, Date transactionDate, String branch, Integer installmentNo, int transaction, String transactionType, String description, double amount, double balanceAmount, Date dueDate, String settlementType, String status) {
       this.client = client;
       this.loan = loan;
       this.transactionDate = transactionDate;
       this.branch = branch;
       this.installmentNo = installmentNo;
       this.transaction = transaction;
       this.transactionType = transactionType;
       this.description = description;
       this.amount = amount;
       this.balanceAmount = balanceAmount;
       this.dueDate = dueDate;
       this.settlementType = settlementType;
       this.status = status;
    }
   
    public Integer getIndexNo() {
        return this.indexNo;
    }
    
    public void setIndexNo(Integer indexNo) {
        this.indexNo = indexNo;
    }
    public Client getClient() {
        return this.client;
    }
    
    public void setClient(Client client) {
        this.client = client;
    }
    public Loan getLoan() {
        return this.loan;
    }
    
    public void setLoan(Loan loan) {
        this.loan = loan;
    }
    public Date getTransactionDate() {
        return this.transactionDate;
    }
    
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }
    public String getBranch() {
        return this.branch;
    }
    
    public void setBranch(String branch) {
        this.branch = branch;
    }
    public Integer getInstallmentNo() {
        return this.installmentNo;
    }
    
    public void setInstallmentNo(Integer installmentNo) {
        this.installmentNo = installmentNo;
    }
    public int getTransaction() {
        return this.transaction;
    }
    
    public void setTransaction(int transaction) {
        this.transaction = transaction;
    }
    public String getTransactionType() {
        return this.transactionType;
    }
    
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    public double getAmount() {
        return this.amount;
    }
    
    public void setAmount(double amount) {
        this.amount = amount;
    }
    public double getBalanceAmount() {
        return this.balanceAmount;
    }
    
    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
    public Date getDueDate() {
        return this.dueDate;
    }
    
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
    public String getSettlementType() {
        return this.settlementType;
    }
    
    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }




}


