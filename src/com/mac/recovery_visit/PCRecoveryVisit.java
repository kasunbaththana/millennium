/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.recovery_visit;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.resources.ApplicationResources;
import com.mac.recovery_visit.object.Employee;
import com.mac.recovery_visit.object.Loan;
import com.mac.recovery_visit.object.LoanType;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Udayanga
 */
public class PCRecoveryVisit extends CPanel {

    private SERRecoveryVisit sERRecoveryVisit;
    private String loanType;
    private String loanOfficer;
    private String recoveryOfficer;

    /**
     * Creates new form PCRecoveryVisit
     */
    public PCRecoveryVisit() {
        initComponents();
        initOthers();
    }

    @SuppressWarnings("unchecked")
    public void initOthers() {
        sERRecoveryVisit = new SERRecoveryVisit(this);

        CTableModel cTableModel = new CTableModel(new CTableColumn[]{
            new CTableColumn("Agrement No", "agreementNo"),
            new CTableColumn("Loan Date", "loanDate"),
            new CTableColumn("Arrears Due Date", "arrearsDueDate"),
            new CTableColumn("Client", "client", "name"),
            new CTableColumn("Loan Amount", "loanAmount"),
            new CTableColumn("Installment Amount", "installmentAmount"),
            new CTableColumn("Arrears", "arrears"),
            new CTableColumn("Age", "age"),
            new CTableColumn("Panalty", "panaltyAmount"),
            new CTableColumn("Total", "total"),
            new CTableColumn("Balance Amount", "balanceAmount")
        });
        pnlLoanTable.setCModel(cTableModel);

        cboLoanType.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setLoanType(cboLoanType.getCValue().toString());
            }
        });

        cboLoanOfficer.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setLoanOfficer(cboLoanOfficer.getCValue().toString());
            }
        });

        cboRecoveryOfficer.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                setRecoveryOfficer(cboRecoveryOfficer.getCValue().toString());
            }
        });

        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnSearch, "doSearch");
        actionUtil.setAction(btnRefresh, "doReFresh");
        actionUtil.setAction(btnPrint, "doPrint");
        actionUtil.setAction(btnFind, "doFind");

        btnSearch.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_NEW_ICON));
        btnRefresh.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_REFRESH_ICON));
        btnPrint.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_PRINT_ICON));
        btnFind.setIcon(ApplicationResources.getImageIcon(ApplicationResources.REGISTRATION_FIND_ICON));
    }

    public List<LoanType> getLoanTypes() {
        try {
            return sERRecoveryVisit.getLoanType();
        } catch (DatabaseException ex) {
            Logger.getLogger(PCRecoveryVisit.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    public List<Employee> getLoanOfficerList() {
        try {
            Set<Employee> employees = new HashSet<Employee>();
            List<Loan> loanOfficers = sERRecoveryVisit.getLoanDetails();
            for (Loan loan : loanOfficers) {
                if (loan.getEmployeeByLoanOfficer()!=null) {
                employees.add(loan.getEmployeeByLoanOfficer());
                }
                System.out.println("........................" + loan.getArrearsDueDate());
            }
            return new ArrayList(employees);
        } catch (DatabaseException ex) {
            Logger.getLogger(PCRecoveryVisit.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    public List<Employee> getRecoveryOfficerList() {
        try {
            Set<Employee> employees = new HashSet<Employee>();
            List<Loan> loanOfficers = sERRecoveryVisit.getLoanDetails();
            for (Loan loan : loanOfficers) {
                if (loan.getEmployeeByRecoveryOfficer()!=null) {
                employees.add(loan.getEmployeeByRecoveryOfficer());
                }
            }
            return new ArrayList(employees);
        } catch (DatabaseException ex) {
            Logger.getLogger(PCRecoveryVisit.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    public List<Date> getLoanDateList() {
        try {
            List<Date> employees = new ArrayList<Date>();
            List<Loan> loanOfficers = sERRecoveryVisit.getLoanDetails();
            for (Loan loan : loanOfficers) {
                employees.add(loan.getLoanTransactionDate());
            }
            return employees;
        } catch (DatabaseException ex) {
            Logger.getLogger(PCRecoveryVisit.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    @Action
    public void doSearch() throws DatabaseException {
        double amount = 0.0;
        double instalmentAmount = 0.0;
        double panaltyAmount = 0.0;

        String[] loanType = getLoanType().split(" ");
        String[] loanOfficer = getLoanOfficer().split(" ");
        String[] recoveryOfficer = getRecoveryOfficer().split(" ");
        
        pnlLoanTable.setCValue(sERRecoveryVisit.getLoanDetailss(loanType[0], loanOfficer[0], recoveryOfficer[0], txtDate.getCValue()));

        List<Loan> loansv = (List<Loan>) pnlLoanTable.getCValue();
        for (Loan loanv : loansv) {
            amount += loanv.getLoanAmount();
            instalmentAmount += loanv.getInstallmentAmount();
            panaltyAmount += loanv.getPanaltyAmount();
        }
        txtLoanAmount.setCValue(amount);
        txtTotalInstallmentAmount.setCValue(instalmentAmount);
        txtPanaltyAmount.setCValue(panaltyAmount);
    }

    @Action
    public void doRefresh() throws DatabaseException {
        pnlLoanTable.setCValue(new ArrayList());
    }

    @Action
    public void doFind() throws DatabaseException {
        pnlLoanTable.find(txtTableFind.getCValue());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        cDLabel1 = new com.mac.af.component.derived.display.label.CDLabel();
        cboLoanOfficer = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoanOfficerList();
            }
        };
        cDLabel2 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel3 = new com.mac.af.component.derived.display.label.CDLabel();
        cboRecoveryOfficer = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getRecoveryOfficerList();
            }
        };
        cboLoanType = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoanTypes();
            }
        };
        cDLabel4 = new com.mac.af.component.derived.display.label.CDLabel();
        btnSearch = new com.mac.af.component.derived.command.button.CCButton();
        txtDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        pnlLoanTable = new com.mac.af.component.base.table.CTable();
        btnRefresh = new com.mac.af.component.derived.command.button.CCButton();
        btnPrint = new com.mac.af.component.derived.command.button.CCButton();
        txtTableFind = new com.mac.af.component.derived.input.textfield.CIStringField();
        btnFind = new com.mac.af.component.derived.command.button.CCButton();
        cDLabel5 = new com.mac.af.component.derived.display.label.CDLabel();
        txtLoanAmount = new com.mac.af.component.derived.display.textfield.CDDoubleField();
        txtTotalInstallmentAmount = new com.mac.af.component.derived.display.textfield.CDDoubleField();
        cDLabel6 = new com.mac.af.component.derived.display.label.CDLabel();
        cDLabel7 = new com.mac.af.component.derived.display.label.CDLabel();
        txtPanaltyAmount = new com.mac.af.component.derived.display.textfield.CDDoubleField();

        jSplitPane1.setDividerLocation(300);

        cDLabel1.setText("Date:");

        cDLabel2.setText("Loan Officer:");

        cDLabel3.setText("Recovery Officer:");

        cDLabel4.setText("Loan Type:");

        btnSearch.setText("Search");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cDLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cDLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cDLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cDLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cboLoanType, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                            .addComponent(cboRecoveryOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(cboLoanOfficer, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(txtDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoanOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboRecoveryOfficer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cDLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoanType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(163, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel1);

        pnlLoanTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(pnlLoanTable);

        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnPrint.setText("Print");

        btnFind.setText("Find");

        cDLabel5.setText("Total Loan Amount:");

        cDLabel6.setText("Total Instalment:");

        cDLabel7.setText("Panalty Amount:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtLoanAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalInstallmentAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtPanaltyAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTableFind, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cDLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLoanAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalInstallmentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cDLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPanaltyAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(txtTableFind, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jSplitPane1.setRightComponent(jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRefreshActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnFind;
    private com.mac.af.component.derived.command.button.CCButton btnPrint;
    private com.mac.af.component.derived.command.button.CCButton btnRefresh;
    private com.mac.af.component.derived.command.button.CCButton btnSearch;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel1;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel2;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel3;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel4;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel5;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel6;
    private com.mac.af.component.derived.display.label.CDLabel cDLabel7;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanOfficer;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanType;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboRecoveryOfficer;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private com.mac.af.component.base.table.CTable pnlLoanTable;
    private com.mac.af.component.derived.input.textfield.CIDateField txtDate;
    private com.mac.af.component.derived.display.textfield.CDDoubleField txtLoanAmount;
    private com.mac.af.component.derived.display.textfield.CDDoubleField txtPanaltyAmount;
    private com.mac.af.component.derived.input.textfield.CIStringField txtTableFind;
    private com.mac.af.component.derived.display.textfield.CDDoubleField txtTotalInstallmentAmount;
    // End of variables declaration//GEN-END:variables

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getLoanOfficer() {
        return loanOfficer;
    }

    public void setLoanOfficer(String loanOfficer) {
        this.loanOfficer = loanOfficer;
    }

    public String getRecoveryOfficer() {
        return recoveryOfficer;
    }

    public void setRecoveryOfficer(String recoveryOfficer) {
        this.recoveryOfficer = recoveryOfficer;
    }
}
