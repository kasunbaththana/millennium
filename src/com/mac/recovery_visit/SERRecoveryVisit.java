/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.recovery_visit;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.recovery_visit.object.Loan;
import com.mac.recovery_visit.object.LoanType;
import java.awt.Component;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Udayanga
 */
public class SERRecoveryVisit extends AbstractService {

    public SERRecoveryVisit(Component component) {
        super(component);
    }

    public List<LoanType> getLoanType() throws DatabaseException {
        return getDatabaseService().getCollection(LoanType.class);
    }

    public List getLoanDetails() throws DatabaseException {
        return getDatabaseService().getCollection(Loan.class);
    }

    public List getLoanDetailss(String loanType, String loanOfficer, String loanRecoveryOfficer, Date date) throws DatabaseException {
        List<Loan> loans = getDatabaseService().getCollection("from com.mac.recovery_visit.object.Loan where (loanType='" + loanType + "' or loan_officer ='" + loanOfficer + "' or recovery_officer ='" + loanRecoveryOfficer + "') and status ='LOAN_START'");
        makeModifications(loans, date);
        return loans;
    }

    private void makeModifications(Collection<Loan> loans, Date asAtDate) {
        for (Loan loan : loans) {//getArrearsDueDate();
            Date d = loan.getExpectedLoanDate();
            Date dateStart = asAtDate;
            Date dateEnd = d;
            if((dateEnd != null) && (dateStart!=null)){
            int diff =(int) Math.round((dateEnd.getDay()- dateStart.getDay()));
            loan.setAge(diff);
            }
        }
        System.out.println("");
    }
}
