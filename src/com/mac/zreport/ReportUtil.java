/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport;

import com.mac.zreport.object.ReportTask;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mohan
 */
public class ReportUtil {

    private static void readTasks() {
        REPORT_TASKS.clear();
        File file = new File(REPORT_DIR);

        File[] files = file.listFiles();

        if (files != null) {
            for (File temp : files) {
                if (temp.isDirectory()) {
                    readBands(temp);
                }
            }
        }
    }

    private static void readBands(File taskDir) {
        File[] files = taskDir.listFiles();
        for (File temp : files) {
            if (temp.isDirectory()) {
                readReports(taskDir, temp);
            }
        }
    }

    private static void readReports(File taskDir, File bandDir) {
        File[] reports = bandDir.listFiles();

        for (File temp : reports) {
            if (temp.isFile()
                    ? getExtention(temp).equalsIgnoreCase(REPORT_EXTENSION)
                    : false) {
                //create report
                createReport(taskDir, bandDir, temp);
            }
        }
    }

    private static void createReport(File taskDir, File bandDir, File reportFile) {
        getReportTask(taskDir.getName()).addReport(bandDir.getName(), reportFile);
    }

    private static String getExtention(File file) {
        return file.getName().substring(file.getName().lastIndexOf('.') + 1);
    }

    public static String getFormattedFileName(String file) {
        return file.substring(file.lastIndexOf(File.separatorChar) + 1).replaceAll("." + REPORT_EXTENSION, "");
    }

    private static ReportTask getReportTask(String name) {
        ReportTask reportTask = null;

        for (ReportTask tempTask : REPORT_TASKS) {
            if (tempTask.getName().equals(name)) {
                reportTask = tempTask;
            }
        }

        if (reportTask == null) {
            reportTask = new ReportTask(name);
            REPORT_TASKS.add(reportTask);
        }

        return reportTask;
    }

    public static List<ReportTask> getReportTasks() {
        readTasks();
        return REPORT_TASKS;
    }

    public static String getFormattedString(String text) {
        StringBuilder sb = new StringBuilder();
        for (String s : text.split("_")) {
            sb.append(" ");
            sb.append(Character.toUpperCase(s.charAt(0)));
            if (s.length() > 1) {
                sb.append(s.substring(1, s.length()).toLowerCase());
            }
        }
        sb.replace(0, 1, "");
        return sb.toString();
    }

//    public static void main(String[] args) {
//        System.out.println(getFormattedString("REPORT_DIR_TEST APP"));
//    }
    private static final List<ReportTask> REPORT_TASKS = new ArrayList<>();
    private static final String REPORT_DIR = "report";
    private static final String REPORT_EXTENSION = "jrxml";
}
