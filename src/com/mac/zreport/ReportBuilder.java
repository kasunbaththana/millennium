/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport;

import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.panel.dialog.object_creator_dialog.ObjectCreatorDialog;
import com.mac.registration.branch.object.Branch;
import com.mac.zreport.report_parameter.PCParameterNew;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author mohan
 */
public class ReportBuilder {

    public static JasperPrint buidReport(String fileName) throws JRException, SQLException, FileNotFoundException {
        File file = new File(fileName);
        FileInputStream inputStream = new FileInputStream(file);

        return buidReport(inputStream);
    }

    public static JasperPrint buidReport(InputStream stream) throws JRException, SQLException {
        JasperDesign jasperDesign = JRXmlLoader.load(stream);
        List<JRParameter> allParameters = jasperDesign.getParametersList();
        List<JRParameter> requiredParameters = new ArrayList<>();

        for (JRParameter jRParameter : allParameters) {
            if (!jRParameter.getName().startsWith("H_") && !jRParameter.isSystemDefined()) {
                requiredParameters.add(jRParameter);
            }
        }

        Map<String, Object> params = new HashMap<>();
        if (!requiredParameters.isEmpty()) {
//            String title = ReportUtil.getFormattedString(ReportUtil.getFormattedFileName(stram));
            String title = ReportUtil.getFormattedString(jasperDesign.getName());

            params = getParameters(title, requiredParameters);
            if (params == null) {
                params = new HashMap<>();
            }
        }else{
            initDefaultParameters();
        }

        params.putAll(DEFAULT_PARAMETERS);

        Connection connection = getConnection();

        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);

        releaseConnection(connection);

        return jasperPrint;
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();
        return connection;
    }

    private static void releaseConnection(Connection connection) throws SQLException {
        CConnectionProvider.getInstance().closeConnection(connection);
    }

    private static Map getParameters(String title, List<JRParameter> parameters) {
        //init default parameters
        initDefaultParameters();
        
        //init parameter dialog
        if (OBJECT_CREATOR_DIALOG == null) {
            OBJECT_CREATOR_DIALOG = new ObjectCreatorDialog();

            PARAMETER_NEW = new PCParameterNew();
            OBJECT_CREATOR_DIALOG.setObjectCreator(PARAMETER_NEW);
        }

        OBJECT_CREATOR_DIALOG.setTitle(title);
        PARAMETER_NEW.setReportParameters(parameters);

        OBJECT_CREATOR_DIALOG.pack();

        return OBJECT_CREATOR_DIALOG.getNewObject();
    }

    public static void initDefaultParameters() {
        //init default parameters
        if (DEFAULT_PARAMETERS == null) {
            try {
                DEFAULT_PARAMETERS = new HashMap<>();

                HibernateDatabaseService databaseService = new HibernateDatabaseService(CPanel.GLOBAL);
                Branch branch = (Branch) databaseService.getObject(Branch.class, (Serializable) CApplication.getSessionVariable(CApplication.STORE_ID));

                DEFAULT_PARAMETERS.put("H_COMPANY_NAME", branch.getName());
                DEFAULT_PARAMETERS.put("H_COMPANY_ADDRESS", branch.getAddressLine1() + ", " + branch.getAddressLine2() + ", " + branch.getAddressLine3());
                DEFAULT_PARAMETERS.put("H_COMPANY_TELEPHONE", branch.getTelephone1() + ", " + branch.getTelephone2());
                DEFAULT_PARAMETERS.put("H_COMPANY_FAX", branch.getFax());
                DEFAULT_PARAMETERS.put("H_COMPANY_EMAIL", branch.getEmail());
                DEFAULT_PARAMETERS.put("H_BRANCH", branch.getCode());
            } catch (DatabaseException ex) {
                Logger.getLogger(ReportBuilder.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static Map<String, Object> DEFAULT_PARAMETERS;
    private static ObjectCreatorDialog<HashMap<String, Object>> OBJECT_CREATOR_DIALOG;
    private static PCParameterNew PARAMETER_NEW;
}
