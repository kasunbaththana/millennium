/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport;

/**
 *
 * @author thilanga
 */
public class ReportCategory {

    public static final String LOAN_REPORTS = "LOAN_REPORTS";
    public static final String ACCOUNT_REPORTS = "ACCOUNT_REPORTS";
    public static final String ARREARS_REPORTS = "ARREARS_REPORTS";
    public static final String CASHIER_REPORTS = "CASHIER_REPORTS";
    public static final String ADMINISTRATION_REPORTS = "ADMINISTRATION_REPORTS";
    public static final String SYSTEM_REPORT = "SYSTEM_REPORT";
    public static final String SUMMARY_REPORT = "SUMMARY_REPORT";
    public static final String REGISTRATION_REPORT = "REGISTRATION_REPORT";
    public static final String SAVING_REPORT = "SAVING_REPORT";
    /**
     *
     */
    public static final String ALL[] = {
        LOAN_REPORTS,
        ACCOUNT_REPORTS,
        ARREARS_REPORTS,
        CASHIER_REPORTS,
        ADMINISTRATION_REPORTS,
        SYSTEM_REPORT,
        SUMMARY_REPORT,
        SAVING_REPORT,
        REGISTRATION_REPORT
    };
}
