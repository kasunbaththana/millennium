/*
 *  AdministrationReports.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 30, 2015, 4:06:33 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zreport.reports_viewers;

import com.mac.zreport.ReportCategory;
import com.mac.zreport.report_viewer_permission.ReportViewerPanel;

/**
 *
 * @author mohan
 */
public class AdministrationReports extends ReportViewerPanel{

    public AdministrationReports() {
        super(ReportCategory.ADMINISTRATION_REPORTS);
    }
    
}
