/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.report_registration;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zreport.zobject.Report;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thilanga
 */
public class REGReportRegistration extends AbstractRegistrationForm {

    @Override
    public AbstractObjectCreator getObjectCreator() {
        return new PCReportRegistration();
    }

    @Override
    public Class getObjectClass() {
        return com.mac.zreport.zobject.Report.class;
    }

    @Override
    public CTableModel getTableModel() {
        return new CTableModel(
                new CTableColumn("Code", "code"),
                new CTableColumn("Name", "name"),
                new CTableColumn("Category", "category"));
    }
    
     @Override
    protected List getTableData() throws DatabaseException {
         List<Report> reportDataList;
        try {

            String sql =
                    "SELECT \n"
                    + "	report.*\n"
                    + "FROM\n"
                    + "	report \n"
                    + " order by report.category asc , report.name asc  ";


            HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(sql, Report.class);


            HibernateDatabaseService databaseService = getDatabaseService();
            reportDataList = databaseService.getCollection(hibernateSQLQuery, new HashMap<String, Object>());
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            reportDataList = new ArrayList();
        }
        return reportDataList;
    }

   
    
}
