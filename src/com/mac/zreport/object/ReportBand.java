/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.object;

import com.mac.zreport.ReportUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mohan
 */
public class ReportBand {

    private String name;
    private List<ReportFile> reportFiles = new ArrayList<>();

    public ReportBand(String name) {
        this.name = name;
    }

    public String getName() {
//        return ReportUtil.getFormattedString(name);
        return name;
    }

    public List<ReportFile> getReportFiles() {
        return reportFiles;
    }

    public void addReportFile(File file) {
        reportFiles.add(new ReportFile(file));
    }
}
