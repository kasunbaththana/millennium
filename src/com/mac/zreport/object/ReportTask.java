/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.object;

import com.mac.zreport.ReportUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mohan
 */
public class ReportTask {

    private String name;
    private List<ReportBand> reportBands = new ArrayList<>();

    public ReportTask(String name) {
        this.name = name;
    }

    public String getName() {
//        return ReportUtil.getFormattedString(name);
        return name;
    }

    public List<ReportBand> getReportBands() {
        return reportBands;
    }

    public void addReport(String reportBand, File reportFile) {
        getReportBand(reportBand).addReportFile(reportFile);
    }

    private ReportBand getReportBand(String name) {
        ReportBand reportBand = null;
        for (ReportBand temp : reportBands) {
            if (temp.getName().equals(name)) {
                reportBand = temp;
            }
        }

        if (reportBand == null) {
            reportBand = new ReportBand(name);
            reportBands.add(reportBand);
        }

        return reportBand;
    }
}
