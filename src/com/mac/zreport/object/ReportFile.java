/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.object;

import com.mac.zreport.ReportUtil;
import java.io.File;

/**
 *
 * @author mohan
 */
public class ReportFile {

    private File reportFile;

    public File getReportFile() {
        return reportFile;
    }

    public String getName() {
//        return ReportUtil.getFormattedString(reportFile.getName().substring(0, reportFile.getName().lastIndexOf('.')));
        return reportFile.getName().substring(0, reportFile.getName().lastIndexOf('.'));
    }

    public ReportFile(File reportFile) {
        this.reportFile = reportFile;
    }
}
