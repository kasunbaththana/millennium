/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.excel_export;

import com.mac.af.core.environment.cpanel.CPanel;
import java.util.Date;

/**
 *
 * @author NIMESH-PC
 */
public class PCExcelExport extends CPanel {

    /**
     * Creates new form PCExcelExport
     */
    public PCExcelExport() {
        initComponents();
    }

    private void generetate(Date Fromdate, Date ToDate, String FilePath) {
        String xx = " (SELECT \n"
                + "	loan.agreement_no, \n"
                + "	loan_type.name AS loan_type_name, \n"
                + "	transaction.transaction_date, \n"
                + "	transaction.status,\n"
                + "	client.name AS client_name, \n"
                + "	SUM(settlement_history.settlement_amount) AS amount, \n"
                + "	if(transaction.transaction_type='RECEIPT' , 'CASH' ,'BANK' )as receipt_type \n"
                + "FROM \n"
                + "	transaction\n"
                + "	LEFT JOIN transaction_type ON transaction.transaction_type = transaction_type.code \n"
                + "	LEFT JOIN loan ON loan.index_no = transaction.loan \n"
                + "	LEFT JOIN settlement_history ON settlement_history.transaction = transaction.index_no \n"
                + "	LEFT JOIN loan_type ON loan_type.code = loan.loan_type \n"
                + "	LEFT JOIN client ON client.code = loan.client \n"
                + "WHERE\n"
                + "	(transaction.transaction_date BETWEEN '2016-05-05' AND '2016-05-08') \n"
                + "	AND (transaction.transaction_type = 'RECEIPT'  OR transaction.transaction_type = 'BANK_RECEIPT') \n"
                + "	AND transaction.status <> 'CANCEL'\n"
                + "	AND loan.status <> 'CANCEL' \n"
                + "GROUP BY \n"
                + "	 transaction.index_no \n"
                + "ORDER BY \n"
                + "	loan.loan_type, transaction.transaction_date \n"
                + " \n"
                + "into outfile 'D:\\\\data.csv' \n"
                + "FIELDS TERMINATED BY ',' \n"
                + "OPTIONALLY ENCLOSED BY '\"' \n"
                + "LINES TERMINATED BY ' \\n');";
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtFromDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        lblFromeDate = new com.mac.af.component.derived.display.label.CDLabel();
        lblToDate = new com.mac.af.component.derived.display.label.CDLabel();
        txtToDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        jButton1 = new javax.swing.JButton();

        lblFromeDate.setText("From Date :");

        lblToDate.setText("To Date:");

        jButton1.setText("Genarate");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblFromeDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblToDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtToDate, javax.swing.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)
                            .addComponent(txtFromDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFromeDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFromDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblToDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtToDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        generetate(txtFromDate.getCValue(), txtToDate.getCValue(), "D://");
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private com.mac.af.component.derived.display.label.CDLabel lblFromeDate;
    private com.mac.af.component.derived.display.label.CDLabel lblToDate;
    private com.mac.af.component.derived.input.textfield.CIDateField txtFromDate;
    private com.mac.af.component.derived.input.textfield.CIDateField txtToDate;
    // End of variables declaration//GEN-END:variables
}
