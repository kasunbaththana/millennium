/*
 *  LetterGenerator.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Dec 1, 2014, 2:07:07 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zreport.special_report_view;


import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.derived.input.textfield.CIDateField;
import com.mac.af.core.ApplicationException;
import com.mac.af.core.database.DatabaseException;
import com.mac.af.core.database.connection_pool.CConnectionProvider;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.core.message.mOptionPane;
import com.mac.af.resources.ApplicationResources;
import com.mac.loan.zobject.LoanType;
import com.mac.zreport.ReportBuilder;
import com.mac.zreport.report_parameter.PCParameterNew;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Nimesh
 */
public class SummeryReport extends CPanel {

    /**
     * Creates new form LetterGenerator
     */
 
    public SummeryReport() {
        initComponents();
        initOthers();
       
    }

       public List<LoanType> getLoanType()  {
         List<LoanType> loanType;
        try {
            String hql = "FROM com.mac.loan.zobject.LoanType WHERE active=1  ";
            loanType = getDatabaseService().getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loanType = new ArrayList();
        }
        return loanType;
        
    }

    
    @SuppressWarnings("unchecked")
    private void initOthers() {
         ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnPrint, "doPrint");

        btnPrint.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_GENERATE, 16, 16));
        cboLoanType.setExpressEditable(true);
        
    }
    
       @Action(asynchronous = true)
    public void doPrint() throws ApplicationException 
    {
        callProc(txtFromDate, txtToDate, txtFromDate_B, txtToDate_B);
        veiwreport();
    }
       
       public void callProc(CIDateField fromdate1,CIDateField fromdate2,CIDateField fromdate3,CIDateField fromdate4)
    {
        LoanType type= (LoanType) cboLoanType.getCValue();
          String call = "CALL z_loan_summary_report('"+fromdate1.getCValue()+"','"+fromdate2.getCValue()+"','"+fromdate3.getCValue()+"','"+fromdate4.getCValue()+"','"+type.getCode()+"')";
                 System.out.println("call proc   "+call);  
          try { 
                         getDatabaseService().beginLocalTransaction();
                       getDatabaseService().callUpdateProcedure(call);
                      getDatabaseService().commitLocalTransaction();
                    } catch (DatabaseException ex) {
                        Logger.getLogger(PCParameterNew.class.getName()).log(Level.SEVERE, null, ex);
                    }
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        jSplitPane1 = new javax.swing.JSplitPane();
        pnlLeft = new javax.swing.JPanel();
        lblFromeDate = new com.mac.af.component.derived.display.label.CDLabel();
        txtFromDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        txtToDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        lblToDate = new com.mac.af.component.derived.display.label.CDLabel();
        lblFromeDate_B = new com.mac.af.component.derived.display.label.CDLabel();
        txtFromDate_B = new com.mac.af.component.derived.input.textfield.CIDateField();
        txtToDate_B = new com.mac.af.component.derived.input.textfield.CIDateField();
        btnPrint = new com.mac.af.component.derived.command.button.CCButton();
        lblToDate_B = new com.mac.af.component.derived.display.label.CDLabel();
        jSeparator2 = new javax.swing.JSeparator();
        cboLoanType = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return getLoanType();
            }
        };
        lblFromeDate1 = new com.mac.af.component.derived.display.label.CDLabel();
        pnlRight = new javax.swing.JPanel();

        jSplitPane1.setDividerLocation(300);

        lblFromeDate.setText("From Date :");

        lblToDate.setText("To Date:");

        lblFromeDate_B.setText("From Date  B:");

        btnPrint.setText("Print");

        lblToDate_B.setText("To Date B:");

        lblFromeDate1.setText("Loan Type :");

        javax.swing.GroupLayout pnlLeftLayout = new javax.swing.GroupLayout(pnlLeft);
        pnlLeft.setLayout(pnlLeftLayout);
        pnlLeftLayout.setHorizontalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlLeftLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator2)
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblFromeDate_B, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlLeftLayout.createSequentialGroup()
                                .addComponent(lblToDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(lblToDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblFromeDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblFromeDate1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cboLoanType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtFromDate, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                            .addComponent(txtToDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtFromDate_B, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtToDate_B, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(24, 24, 24))
        );
        pnlLeftLayout.setVerticalGroup(
            pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLeftLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboLoanType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFromeDate1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFromeDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFromDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtToDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlLeftLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(lblToDate, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtFromDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFromeDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlLeftLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtToDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblToDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(147, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(pnlLeft);

        pnlRight.setLayout(new javax.swing.BoxLayout(pnlRight, javax.swing.BoxLayout.LINE_AXIS));
        jSplitPane1.setRightComponent(pnlRight);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnPrint;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanType;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSplitPane jSplitPane1;
    private com.mac.af.component.derived.display.label.CDLabel lblFromeDate;
    private com.mac.af.component.derived.display.label.CDLabel lblFromeDate1;
    private com.mac.af.component.derived.display.label.CDLabel lblFromeDate_B;
    private com.mac.af.component.derived.display.label.CDLabel lblToDate;
    private com.mac.af.component.derived.display.label.CDLabel lblToDate_B;
    private javax.swing.JPanel pnlLeft;
    private javax.swing.JPanel pnlRight;
    private com.mac.af.component.derived.input.textfield.CIDateField txtFromDate;
    private com.mac.af.component.derived.input.textfield.CIDateField txtFromDate_B;
    private com.mac.af.component.derived.input.textfield.CIDateField txtToDate;
    private com.mac.af.component.derived.input.textfield.CIDateField txtToDate_B;
    // End of variables declaration//GEN-END:variables
    private static Connection getConnection() throws SQLException {
        Connection connection = CConnectionProvider.getInstance().getConnection();

        return connection;
    }
     public void veiwreport()
     {
          
        try {
LoanType type= (LoanType) cboLoanType.getCValue();
            JasperDesign jasperDesign1 = JRXmlLoader.load("z_loan_summary_report_royan_new.jrxml");
            JasperReport jasperReport1 = JasperCompileManager.compileReport(jasperDesign1);

            HashMap<String, Object> params = new HashMap<>();
            ReportBuilder.initDefaultParameters();
            params.putAll(ReportBuilder.DEFAULT_PARAMETERS);
            params.put("FROM_DATE", txtFromDate.getCValue());
            params.put("TO_DATE", txtToDate.getCValue());
            params.put("FROM_DATE_B", txtFromDate_B.getCValue());
            params.put("TO_DATE_B", txtToDate_B.getCValue());
            params.put("loan_type", type.getName());
     

            final JasperPrint jasperPrint1 = JasperFillManager.fillReport(jasperReport1, params, getConnection());

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    JRViewer viewer1 = new JRViewer(jasperPrint1);

                    pnlRight.removeAll();

                    pnlRight.add(viewer1);

                }
            };

            CApplication.invokeEventDispatch(runnable);


        } catch (JRException | SQLException e) {
            mOptionPane.showMessageDialog(null, "Unable to generate remind letters.", "Error", mOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
     }
}
