/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.report_parameter;

/**
 *
 * @author thilanga
 */
public class ParameterData {

    public static final String AGREEMENT_NO = "AGREEMENT_NO";
    public static final String LOAN_TYPE = "LOAN_TYPE";
    public static final String LOAN_GROUP = "LOAN_GROUP";
    public static final String EMPLOYEE = "EMPLOYEE";
    public static final String ACCOUNT = "ACCOUNT";
    public static final String CLIENT = "CLIENT";
    public static final String FROME_DATE = "FROM_DATE";
    public static final String TO_DATE = "TO_DATE";
    public static final String FROME_DATE_B = "FROM_DATE_B";
    public static final String TO_DATE_B = "TO_DATE_B";
    public static final String AS_AT_DATE = "AS_AT_DATE";
    public static final String AMOUNT = "AMOUNT";
//    public static final String VOLUME = "VOLUME";
    public static final String CHEQUE_STATUS = "CHEQUE_STATUS";
    public static final String LONE_STATUES = "LONE_STATUES";
    public static final String ROUTE = "ROUTE";
    public static final String BRANCH = "BRANCH";
    public static final String TRANSACTION_TYPE = "TRANSACTION_TYPE";
    public static final String SAVING_ACCOUNT = "SAVING_ACCOUNT";
    public static final String A_MOBILE = "A_MOBILE";
    //
    public static final String[] POSSIBILITES = {
        ParameterData.AGREEMENT_NO,
        ParameterData.LOAN_TYPE,
        ParameterData.LOAN_GROUP,
        ParameterData.EMPLOYEE,
        ParameterData.ACCOUNT,
        ParameterData.CLIENT,
        ParameterData.FROME_DATE,
        ParameterData.TO_DATE,
        ParameterData.FROME_DATE_B,
        ParameterData.TO_DATE_B,
        ParameterData.AS_AT_DATE,
        ParameterData.CHEQUE_STATUS,
        ParameterData.LONE_STATUES,
        ParameterData.AMOUNT,
//        ParameterData.VOLUME,
         ParameterData.ROUTE,
         ParameterData.TRANSACTION_TYPE,
         ParameterData.SAVING_ACCOUNT,
         ParameterData.BRANCH,
         ParameterData.A_MOBILE
    };
}
