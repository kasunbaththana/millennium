/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.report_parameter;

import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.environment.service.AbstractService;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thilanga
 */
public class SERReportParameter extends AbstractService {

    public SERReportParameter(Component component) {
        super(component);
    }

    public List getAgreementNo() {
        List agreementNo;
        try {
            String hql = "FROM com.mac.zreport.report_parameter.object.Loan WHERE agreement_no IS NOT NULL AND status <> 'CANCEL' AND situation = 'LOAN'";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            agreementNo = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            agreementNo = new ArrayList();
        }
        return agreementNo;
    }
    public List getRoute() {
        List route;
        try {
            String hql = "FROM com.mac.zreport.report_parameter.object.Route";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            route = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            route = new ArrayList();
        }
        return route;
    }
     public List getBranch() {
        List route;
        try {
            String hql = "FROM com.mac.zreport.report_parameter.object.Branch";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            route = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            route = new ArrayList();
        }
        return route;
    }
     public List getTransactionType() {
        List transType;
        try {
            String hql = "FROM com.mac.zsystem.transaction.transaction.object.TransactionType";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            transType = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            transType = new ArrayList();
        }
        return transType;
    }

    public List getLoanType() {
        List loanType;
        try {
            String hql = "FROM com.mac.zreport.report_parameter.object.LoanType";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            loanType = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            loanType = new ArrayList();
        }
        return loanType;
    }
    public List getLoanGroup() {
        List LoanGroup;
        try {
            String hql = "FROM com.mac.zreport.report_parameter.object.LoanGroup";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            LoanGroup = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            LoanGroup = new ArrayList();
        }
        return LoanGroup;
    }

    public List getEmployees() {
        List employee;
        try {
            String hql = "FROM  com.mac.zreport.report_parameter.object.Employee";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            employee = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            employee = new ArrayList();
        }
        return employee;
    }

    public List getAccounts() {
        List accounts;
        try {
            String hql = "FROM  com.mac.zreport.report_parameter.object.Account";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            accounts = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            accounts = new ArrayList();
        }
        return accounts;
    }
    public List getSVAccounts() {
        List accounts;
        try {
            String hql = "FROM  com.mac.sarving.object.SvAccount";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            accounts = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            accounts = new ArrayList();
        }
        return accounts;
    }

    public List getClients() {
        List clients;
        try {
            String hql = "FROM com.mac.zreport.report_parameter.object.Client";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            clients = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            clients = new ArrayList();
        }
        return clients;
    }
    public List getAMobileNo() {
        List mobile;
        try {
            String hql = "FROM com.mac.zreport.report_parameter.object.AndroidMobile where status='ACTIVE'";
            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            mobile = databaseService.getCollection(hql);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            mobile = new ArrayList();
        }
        return mobile;
    }
    
    
}
