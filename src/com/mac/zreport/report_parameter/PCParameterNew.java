/*
 *  PCParameterNew.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 22, 2015, 8:22:21 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zreport.report_parameter;

import com.mac.account.ztemplate.cheque_transaction.ChequeStatus;
import com.mac.af.component.base.textfield.CTextField;
import com.mac.af.component.derived.input.CInputComponent;
import com.mac.af.component.derived.input.combobox.CIComboBox;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.panel.object.ObjectCreatorException;
import com.mac.loan.LoanStatus;
import com.mac.sarving.object.SvAccount;
import com.mac.zreport.report_parameter.object.Account;
import com.mac.zreport.report_parameter.object.AndroidMobile;
import com.mac.zreport.report_parameter.object.Branch;
import com.mac.zreport.report_parameter.object.Client;
import com.mac.zreport.report_parameter.object.Employee;
import com.mac.zreport.report_parameter.object.Loan;
import com.mac.zreport.report_parameter.object.LoanGroup;
import com.mac.zreport.report_parameter.object.LoanType;
import com.mac.zreport.report_parameter.object.Route;
import com.mac.zsystem.transaction.account.account_setting.AccountTableModel;
import com.mac.zsystem.transaction.transaction.object.TransactionType;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BoxLayout;
import net.sf.jasperreports.engine.JRParameter;

/**
 *
 * @author mohan
 */
public class PCParameterNew extends AbstractObjectCreator<HashMap<String, Object>> {

    private Map<String, Component> labels;//labels of all possible parameters
    private Map<String, Component> components;//components of all possible parameters
    private List<JRParameter> reportsParameters;//required parameters for the report
    //
    private List<String> acceptableParameters;//parameters which is in possible list
    private SERReportParameter reportsParameter;
    //
    private PCParameter nonAcceptableParameterPanel;

    /**
     * Creates new form PCParameterNew
     */
    public PCParameterNew() {
        initComponents();
        initComponentMaps();
        initNonAcceptableParams();
        initOthers();
    }

    public void setReportParameters(List<JRParameter> parameters) {
        this.reportsParameters = parameters;

        showRequiredComponents();

        showNonAcceptableParams();
    }

    private void initComponentMaps() {

        reportsParameter = new SERReportParameter(this);

        labels = new HashMap<>();
        labels.put(ParameterData.AGREEMENT_NO, lblAgreementNo);
        labels.put(ParameterData.LOAN_TYPE, lblLoanType);
        labels.put(ParameterData.LOAN_GROUP, lblLoanGroup);
        labels.put(ParameterData.EMPLOYEE, lblEmployee);
        labels.put(ParameterData.FROME_DATE, lblFromeDate);
        labels.put(ParameterData.TO_DATE, lblToDate);
        labels.put(ParameterData.FROME_DATE_B, lblFromeDate_B);
        labels.put(ParameterData.TO_DATE_B, lblToDate_B);
        labels.put(ParameterData.AS_AT_DATE, lblAsAtDate);
        labels.put(ParameterData.ACCOUNT, lblAccount);
        labels.put(ParameterData.CLIENT, lblClient);
        labels.put(ParameterData.AMOUNT, lblAmount);
        labels.put(ParameterData.CHEQUE_STATUS, lblchequeStatus);
        labels.put(ParameterData.LONE_STATUES,  lblLoanStatus );
        labels.put(ParameterData.ROUTE,  lblRoute );
        labels.put(ParameterData.BRANCH,  lblBranch );
        labels.put(ParameterData.TRANSACTION_TYPE,  lblTransactionType );
        labels.put(ParameterData.SAVING_ACCOUNT,  lblCrAccount );
        labels.put(ParameterData.A_MOBILE,  lblMobile );

        components = new HashMap<>();
        components.put(ParameterData.AGREEMENT_NO, cboAgreementNo);
        components.put(ParameterData.LOAN_TYPE, cboLoanType);
        components.put(ParameterData.LOAN_GROUP, cboLoanGroup);
        components.put(ParameterData.EMPLOYEE, cboEmployee);
        components.put(ParameterData.FROME_DATE, txtFromDate);
        components.put(ParameterData.TO_DATE, txtToDate);
        components.put(ParameterData.FROME_DATE_B, txtFromDate_B);
        components.put(ParameterData.TO_DATE_B, txtToDate_B);
        components.put(ParameterData.AS_AT_DATE, txtAsAtDate);
        components.put(ParameterData.ACCOUNT, cboAccount);
        components.put(ParameterData.CLIENT, cboClient);
        components.put(ParameterData.AMOUNT, txtAmount);
        components.put(ParameterData.CHEQUE_STATUS, cboChequeStatus);
        components.put(ParameterData.LONE_STATUES, cboLoanStatus);
        components.put(ParameterData.ROUTE, cboRoute);
        components.put(ParameterData.BRANCH, cboBranch);
        components.put(ParameterData.TRANSACTION_TYPE, cboTransactionType);
        components.put(ParameterData.SAVING_ACCOUNT, cboCrAccount);
        components.put(ParameterData.A_MOBILE, cboAmobile);
    }

    private void initNonAcceptableParams() {
        nonAcceptableParameterPanel = new PCParameter();
        jplPCParameter.setLayout(new BoxLayout(jplPCParameter, BoxLayout.LINE_AXIS));
        jplPCParameter.add(nonAcceptableParameterPanel);
    }

    private void showNonAcceptableParams() {
        List<JRParameter> nonAccetpableParams = new ArrayList<>();

        boolean acceptable;
        for (JRParameter reportParameter : reportsParameters) {
            acceptable = false;

            for (String string : acceptableParameters) {
                if (string.equals(reportParameter.getName())) {
                    acceptable = true;
                }
            }

            if (!acceptable) {
                nonAccetpableParams.add(reportParameter);
            }
        }

        nonAcceptableParameterPanel.setReportParameters(nonAccetpableParams);
    }

    private void showRequiredComponents() {
        acceptableParameters = new ArrayList<>();
        
        for (String string : ParameterData.POSSIBILITES) {
            labels.get(string).setVisible(false);
            components.get(string).setVisible(false);
        }

        for (JRParameter parameter : reportsParameters) {
            for (String possibilites : ParameterData.POSSIBILITES) {
                if (possibilites.equals(parameter.getName())) {
                    acceptableParameters.add(possibilites);

  
                    
  
                    labels.get(possibilites).setVisible(true);
                    components.get(possibilites).setVisible(true);
                }
                
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    private void initOthers() {
        cboAccount.setExpressEditable(true);
        cboAgreementNo.setExpressEditable(true);
        cboCrAccount.setExpressEditable(true);
        cboClient.setExpressEditable(true);
        cboEmployee.setExpressEditable(true);
        cboLoanType.setExpressEditable(true);
        cboLoanGroup.setExpressEditable(true);
        cboBranch.setExpressEditable(true);
        cboAmobile.setExpressEditable(true);
        cboAgreementNo.doRefresh();
        
        
         cboAccount.setTableModel(new AccountTableModel());
         
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cboRoute1 = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getRoute();
            }
        };
        lblAgreementNo = new com.mac.af.component.derived.display.label.CDLabel();
        cboAgreementNo = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getAgreementNo();
            }
        };
        lblLoanType = new com.mac.af.component.derived.display.label.CDLabel();
        cboLoanType = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getLoanType();
            }
        };
        cboEmployee = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getEmployees();
            }
        };
        lblEmployee = new com.mac.af.component.derived.display.label.CDLabel();
        lblFromeDate = new com.mac.af.component.derived.display.label.CDLabel();
        txtFromDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        lblAccount = new com.mac.af.component.derived.display.label.CDLabel();
        cboAccount = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getAccounts();
            }
        };
        lblClient = new com.mac.af.component.derived.display.label.CDLabel();
        cboClient = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getClients();
            }
        };
        lblToDate = new com.mac.af.component.derived.display.label.CDLabel();
        txtToDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        lblAsAtDate = new com.mac.af.component.derived.display.label.CDLabel();
        txtAsAtDate = new com.mac.af.component.derived.input.textfield.CIDateField();
        lblAmount = new com.mac.af.component.derived.display.label.CDLabel();
        txtAmount = new com.mac.af.component.derived.input.textfield.CIDoubleField();
        jplPCParameter = new javax.swing.JPanel();
        cboLoanGroup = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getLoanGroup();
            }
        };
        lblLoanGroup = new com.mac.af.component.derived.display.label.CDLabel();
        cboChequeStatus = new com.mac.af.component.derived.input.combobox.CIComboBox(
            ChequeStatus.ALL
        )
        ;
        lblchequeStatus = new com.mac.af.component.derived.display.label.CDLabel();
        lblLoanStatus = new com.mac.af.component.derived.display.label.CDLabel();
        cboLoanStatus = new com.mac.af.component.derived.input.combobox.CIComboBox(
            LoanStatus.ALL_LOAN_STATUS
        )
        ;
        lblRoute = new com.mac.af.component.derived.display.label.CDLabel();
        cboRoute = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getRoute();
            }
        };
        cboBranch = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getBranch();
            }
        };
        lblBranch = new com.mac.af.component.derived.display.label.CDLabel();
        cboTransactionType = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getTransactionType();
            }
        };
        lblTransactionType = new com.mac.af.component.derived.display.label.CDLabel();
        txtToDate_B = new com.mac.af.component.derived.input.textfield.CIDateField();
        lblToDate_B = new com.mac.af.component.derived.display.label.CDLabel();
        lblFromeDate_B = new com.mac.af.component.derived.display.label.CDLabel();
        txtFromDate_B = new com.mac.af.component.derived.input.textfield.CIDateField();
        lblCrAccount = new com.mac.af.component.derived.display.label.CDLabel();
        cboCrAccount = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getSVAccounts();
            }
        };
        cboAmobile = new com.mac.af.component.derived.input.combobox.CIComboBox(){
            @Override
            public List getComboData(){
                return reportsParameter.getAMobileNo();
            }
        };
        lblMobile = new com.mac.af.component.derived.display.label.CDLabel();

        lblAgreementNo.setText("Agreement No.:");

        lblLoanType.setText("Loan Type :");

        lblEmployee.setText("Employee :");

        lblFromeDate.setText("From Date :");

        lblAccount.setText("Account:");

        lblClient.setText("Client:");

        lblToDate.setText("To Date:");

        lblAsAtDate.setText("As At Date:");

        txtAsAtDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAsAtDateActionPerformed(evt);
            }
        });

        lblAmount.setText("Amount:");

        javax.swing.GroupLayout jplPCParameterLayout = new javax.swing.GroupLayout(jplPCParameter);
        jplPCParameter.setLayout(jplPCParameterLayout);
        jplPCParameterLayout.setHorizontalGroup(
            jplPCParameterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jplPCParameterLayout.setVerticalGroup(
            jplPCParameterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 5, Short.MAX_VALUE)
        );

        lblLoanGroup.setText("Loan Group :");

        lblchequeStatus.setText("Cheque  Status :");

        lblLoanStatus.setText("Loan Status :");

        lblRoute.setText("Route :");

        lblBranch.setText("Branch:");

        lblTransactionType.setText("Transaction Type :");

        lblToDate_B.setText("To Date B:");

        lblFromeDate_B.setText("From Date  B:");

        lblCrAccount.setText("Cr Account :");

        lblMobile.setText("Phone :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jplPCParameter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLoanType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblToDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAsAtDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLoanGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblchequeStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLoanStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRoute, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(lblBranch, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblFromeDate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblFromeDate_B, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(lblTransactionType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblToDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCrAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboAmobile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboBranch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAsAtDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtToDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboAccount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAmount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFromDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboEmployee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboLoanType, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboLoanGroup, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                    .addComponent(cboChequeStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboLoanStatus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboRoute, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                    .addComponent(cboTransactionType, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                    .addComponent(txtToDate_B, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFromDate_B, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboCrAccount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAgreementNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblLoanType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoanType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboLoanGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLoanGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblchequeStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboChequeStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblLoanStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboLoanStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblCrAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboCrAccount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboBranch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblBranch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblRoute, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboRoute, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTransactionType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboTransactionType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboAmobile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFromeDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFromDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblToDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtToDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFromeDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFromDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblToDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtToDate_B, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAsAtDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAsAtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jplPCParameter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtAsAtDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAsAtDateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAsAtDateActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAccount;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAgreementNo;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboAmobile;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboBranch;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboChequeStatus;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboClient;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboCrAccount;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboEmployee;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanGroup;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanStatus;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboLoanType;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboRoute;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboRoute1;
    private com.mac.af.component.derived.input.combobox.CIComboBox cboTransactionType;
    private javax.swing.JPanel jplPCParameter;
    private com.mac.af.component.derived.display.label.CDLabel lblAccount;
    private com.mac.af.component.derived.display.label.CDLabel lblAgreementNo;
    private com.mac.af.component.derived.display.label.CDLabel lblAmount;
    private com.mac.af.component.derived.display.label.CDLabel lblAsAtDate;
    private com.mac.af.component.derived.display.label.CDLabel lblBranch;
    private com.mac.af.component.derived.display.label.CDLabel lblClient;
    private com.mac.af.component.derived.display.label.CDLabel lblCrAccount;
    private com.mac.af.component.derived.display.label.CDLabel lblEmployee;
    private com.mac.af.component.derived.display.label.CDLabel lblFromeDate;
    private com.mac.af.component.derived.display.label.CDLabel lblFromeDate_B;
    private com.mac.af.component.derived.display.label.CDLabel lblLoanGroup;
    private com.mac.af.component.derived.display.label.CDLabel lblLoanStatus;
    private com.mac.af.component.derived.display.label.CDLabel lblLoanType;
    private com.mac.af.component.derived.display.label.CDLabel lblMobile;
    private com.mac.af.component.derived.display.label.CDLabel lblRoute;
    private com.mac.af.component.derived.display.label.CDLabel lblToDate;
    private com.mac.af.component.derived.display.label.CDLabel lblToDate_B;
    private com.mac.af.component.derived.display.label.CDLabel lblTransactionType;
    private com.mac.af.component.derived.display.label.CDLabel lblchequeStatus;
    private com.mac.af.component.derived.input.textfield.CIDoubleField txtAmount;
    private com.mac.af.component.derived.input.textfield.CIDateField txtAsAtDate;
    private com.mac.af.component.derived.input.textfield.CIDateField txtFromDate;
    private com.mac.af.component.derived.input.textfield.CIDateField txtFromDate_B;
    private com.mac.af.component.derived.input.textfield.CIDateField txtToDate;
    private com.mac.af.component.derived.input.textfield.CIDateField txtToDate_B;
    // End of variables declaration//GEN-END:variables
    private HashMap<String, Object> parameters;

    @Override
    public void setNewMood() {
        //NOTHING SPECIAL
    }

    @Override
    public void setEditMood() {
        //NOTHING SPECIAL
    }

    @Override
    public void setIdleMood() {
        //NOTHING SPECIAL
    }

    @Override
    public void resetFields() {
        //NOT RESET FORM
    }

    @Override
    protected void setValueAbstract(HashMap<String, Object> t) throws ObjectCreatorException {
        //NOTHING SPECIAL
    }

    @Override
    protected HashMap<String, Object> getValueAbstract() throws ObjectCreatorException {
        return parameters;
    }

    @Override
    protected void initInterface() throws ObjectCreatorException {
        showRequiredComponents();
    }

    @Override
    protected void initObject() throws ObjectCreatorException {
        parameters = new HashMap<>();

        //ACCEPTABLE PARAMETERS
        Component component;
        for (String string : acceptableParameters) {
            component = components.get(string);

            if (component instanceof CTextField) {
                parameters.put(string, ((CInputComponent) component).getCValue());
            }else if (component instanceof CIComboBox) {
                switch (string) {
                    case ParameterData.LOAN_TYPE:
                        parameters.put(string, ((LoanType)((CInputComponent) component).getCValue()).getCode());
                        break;
                    case ParameterData.AGREEMENT_NO:
                        parameters.put(string, ((Loan)((CInputComponent) component).getCValue()).getAgreementNo());
                        break;
                    case ParameterData.LOAN_GROUP:
                        parameters.put(string, ((LoanGroup)((CInputComponent) component).getCValue()).getCode());
                        break;
                    case ParameterData.ACCOUNT:
                        parameters.put(string, ((Account)((CInputComponent) component).getCValue()).getCode());
                        break;
                    case ParameterData.CLIENT:
                        parameters.put(string, ((Client)((CInputComponent) component).getCValue()).getCode());
                        break;
                    case ParameterData.EMPLOYEE:
                        parameters.put(string, ((Employee)((CInputComponent) component).getCValue()).getCode());
                        break;
                   case ParameterData.ROUTE:
                        parameters.put(string, ((Route)((CInputComponent) component).getCValue()).getCode());
                        break;
                   case ParameterData.BRANCH:
                        parameters.put(string, ((Branch)((CInputComponent) component).getCValue()).getCode());
                        break;
                   case ParameterData.TRANSACTION_TYPE:
                        parameters.put(string, ((TransactionType)((CInputComponent) component).getCValue()).getCode());
                        break;
                   case ParameterData.CHEQUE_STATUS:
                        parameters.put(string, ((String)((CInputComponent) component).getCValue()));
                        break;
                   case ParameterData.SAVING_ACCOUNT:
                        parameters.put(string, ((SvAccount)((CInputComponent) component).getCValue()).getCode());
                        break;
                   case ParameterData.A_MOBILE:
                        parameters.put(string, ((AndroidMobile)((CInputComponent) component).getCValue()).getMacAddress());
                        break;
                      
                    default:
                }
            }
        }

        //ADDITIONAL FOR NON-ACCEPTABLE PARAMETERS
        parameters.putAll(nonAcceptableParameterPanel.getValue());
    }
}
