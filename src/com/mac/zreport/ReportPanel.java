/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport;

import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.zreport.report_viewer.ReportViewer;
import java.awt.event.KeyAdapter;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author mohan
 */
public class ReportPanel extends CPanel {

    private File path;

    public ReportPanel(Object path) {
        this.path = (File) path;
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        createViewer();
    }

    private void createViewer() {
        try {
//            JasperPrint jasperPrint = ReportBuilder.buidReport(path);
//            JRViewer viewer = new JRViewer(jasperPrint);
//            viewer.set
            
            ReportViewer viewer = new ReportViewer(path.getCanonicalPath(), true);
//            ReportViewer viewer = new ReportViewer(jasperPrint);
            add(viewer);
        } catch (JRException ex) {
            ex.printStackTrace();
//        } catch (SQLException ex) {
//            Logger.getLogger(ReportPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportPanel.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (SQLException ex) {
//            Logger.getLogger(ReportPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
