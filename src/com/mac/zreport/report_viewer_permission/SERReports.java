/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.report_viewer_permission;


import com.finac.loan.Finac;
import com.mac.af.core.database.hibernate.HibernateDatabaseService;
import com.mac.af.core.database.hibernate.HibernateSQLQuery;
import com.mac.af.core.environment.CApplication;
import com.mac.af.core.environment.service.AbstractService;
import com.mac.zreport.zobject.Report;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thilanga
 */
public class SERReports extends AbstractService {

    public SERReports(Component component) {
        super(component);
    }

    public List getReports(String category) {
        List<Report> reportDataList;
        try {
            String hql = "FROM com.mac.zreport.zobject.Report WHERE category='" + category + "' AND code IN (SELECT report FROM user_role_report WHERE user_role = 'DEVELOPER')";

            String sql =
                    "SELECT \n"
                    + "	report.*\n"
                    + "FROM\n"
                    + "	report \n"
                    + "	LEFT JOIN user_role_report ON user_role_report.report = report.code\n"
                    + "WHERE \n"
                    + "	report.category = \"" + category + "\"\n"
                    + "	AND user_role_report.user_role = '" + CApplication.getSessionVariable(Finac.USER_ROLE)+"'"
                    + " order by report.name asc  ";


            HibernateSQLQuery hibernateSQLQuery = new HibernateSQLQuery(sql, Report.class);


            HibernateDatabaseService databaseService = getCPanel().getDatabaseService();
            reportDataList = databaseService.getCollection(hibernateSQLQuery, new HashMap<String, Object>());
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            reportDataList = new ArrayList();
        }
        return reportDataList;
    }
}
