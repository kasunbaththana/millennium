/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.report_viewer_permission;

import com.mac.af.component.base.button.action.Action;
import com.mac.af.component.base.button.action.ActionUtil;
import com.mac.af.component.model.list.CListModel;
import com.mac.af.core.environment.cpanel.CPanel;
import com.mac.af.resources.ApplicationResources;
import com.mac.zreport.report_viewer.ReportViewer;
import com.mac.zreport.zobject.Report;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.swing.BoxLayout;
import net.sf.jasperreports.engine.JRException;

/**
 *
 * @author thilanga
 */
public class ReportViewerPanel extends CPanel {

    private String category;
    private Report report;
    private ReportViewer reportViewer;
    private SERReports serReports;

    /**
     * Creates new form ReportViewerPanel
     */
    public ReportViewerPanel(String category) {
        this.category = category;
        initComponents();
        initOthers();
    }

    @Action(asynchronous = false)
    public void viewReport() throws JRException {
        report = (Report) lstReports.getSelectedValue();

        if(report!=null){
        InputStream inputStream = new ByteArrayInputStream(report.getReport());
        
        reportViewer = new ReportViewer(inputStream, true);
        
        pnlVewer.removeAll();
        pnlVewer.add(reportViewer);
        }
    }

    @Action(asynchronous = true)
    public void refreshReports() {
        serReports = new SERReports(this);
        lstReports.setCValue(serReports.getReports(category));
    }

    @SuppressWarnings("unchecked")
    private void initOthers() {
        lstReports.setCModel(new CListModel());
        lstReports.setCellRenderer(new ListRenderer());
        
        pnlVewer.setLayout(new BoxLayout(pnlVewer, BoxLayout.LINE_AXIS));
        
        ActionUtil actionUtil = new ActionUtil(this);
        actionUtil.setAction(btnView, "viewReport");
        actionUtil.setAction(btnRefresh, "refreshReports");
        
        btnRefresh.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_REFRESH, 16, 16));
        btnView.setIcon(ApplicationResources.getImageIcon(ApplicationResources.ACTION_OPEN, 16, 16));
        
        btnRefresh.doClick();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        pnlVewer = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnRefresh = new com.mac.af.component.derived.command.button.CCButton();
        btnView = new com.mac.af.component.derived.command.button.CCButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstReports = new com.mac.af.component.base.list.CList();

        jSplitPane1.setDividerLocation(300);

        javax.swing.GroupLayout pnlVewerLayout = new javax.swing.GroupLayout(pnlVewer);
        pnlVewer.setLayout(pnlVewerLayout);
        pnlVewerLayout.setHorizontalGroup(
            pnlVewerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 355, Short.MAX_VALUE)
        );
        pnlVewerLayout.setVerticalGroup(
            pnlVewerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 424, Short.MAX_VALUE)
        );

        jSplitPane1.setRightComponent(pnlVewer);

        btnRefresh.setText("Refresh");

        btnView.setText("View");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(lstReports);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jSplitPane1.setLeftComponent(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnViewActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.mac.af.component.derived.command.button.CCButton btnRefresh;
    private com.mac.af.component.derived.command.button.CCButton btnView;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private com.mac.af.component.base.list.CList lstReports;
    private javax.swing.JPanel pnlVewer;
    // End of variables declaration//GEN-END:variables
}
