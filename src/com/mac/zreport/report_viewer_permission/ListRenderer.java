/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mac.zreport.report_viewer_permission;

import com.mac.af.component.renderer.list.CListCellRenderer;
import com.mac.zreport.zobject.Report;
import com.mac.zresources.FinacResources;
import javax.swing.Icon;
import javax.swing.JList;

/**
 *
 * @author thilanga
 */
public class ListRenderer extends CListCellRenderer {

    private Icon icon;

    public ListRenderer() {
        icon = FinacResources.getImageIcon(FinacResources.REPORT, 24, 24);
    }

    @Override
    protected String getText(JList jlist, Object o, int i, boolean bln, boolean bln1) {
        return getFormattedString(((Report) o).getName());
    }

    @Override
    protected Icon getIcon(JList jlist, Object o, int i, boolean bln, boolean bln1) {

        return icon;
    }

    public static String getFormattedString(String text) {
        StringBuilder sb = new StringBuilder();
        for (String s : text.split("_")) {
            sb.append(" ");
            sb.append(Character.toUpperCase(s.charAt(0)));
            if (s.length() > 1) {
                sb.append(s.substring(1, s.length()).toLowerCase());
            }
        }
        sb.replace(0, 1, "");
        return sb.toString();
    }
}
