/*
 *  REGUserRoleReportPermission.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 22, 2015, 9:41:25 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zreport.report_permission;

import com.mac.af.component.model.table.CTableColumn;
import com.mac.af.component.model.table.CTableModel;
import com.mac.af.panel.object.AbstractObjectCreator;
import com.mac.af.templates.registration.AbstractRegistrationForm;
import com.mac.zreport.zobject.UserRole;

/**
 *
 * @author mohan
 */
public class REGUserRoleReportPermission extends AbstractRegistrationForm<UserRole> {

    @Override
    public AbstractObjectCreator<UserRole> getObjectCreator() {
        return new PCUserRoleReportPermission();
    }

    @Override
    public Class<? extends UserRole> getObjectClass() {
        return UserRole.class;
    }

    @Override
    public CTableModel<UserRole> getTableModel() {
        return new CTableModel<>(
                new CTableColumn("Code", "code"),
                new CTableColumn("User Role", "name"));
    }

//    @Override
//    protected int save(UserRole object) throws DatabaseException {
//        return super.save(object); //To change body of generated methods, choose Tools | Templates.
//        System.out.println("................");
//    }
    
}
