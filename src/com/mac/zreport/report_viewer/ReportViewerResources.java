/*
 *  ReportViewerResources.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 27, 2015, 2:22:02 PM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zreport.report_viewer;

import com.mac.zresources.FinacResources;
import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author mohan
 */
public class ReportViewerResources {

    public static final URL SAVE_ICON_URL = ReportViewerResources.class.getResource("resources/save.png");
    public static final URL PRINT_ICON_URL = ReportViewerResources.class.getResource("resources/print.png");
    public static final URL REFRESH_ICON_URL = ReportViewerResources.class.getResource("resources/refresh.png");
    //
    public static final URL FIRST_ICON_URL = ReportViewerResources.class.getResource("resources/first.png");
    public static final URL BACK_ICON_URL = ReportViewerResources.class.getResource("resources/back.png");
    public static final URL NEXT_ICON_URL = ReportViewerResources.class.getResource("resources/next.png");
    public static final URL LAST_ICON_URL = ReportViewerResources.class.getResource("resources/last.png");
    //
    public static final URL FIT_PAGE_ICON_URL = ReportViewerResources.class.getResource("resources/fit_page.png");
    public static final URL FIT_HEIGHT_ICON_URL = ReportViewerResources.class.getResource("resources/fit_height.png");
    public static final URL FIT_WIDTH_ICON_URL = ReportViewerResources.class.getResource("resources/fit_width.png");
    //
    public static final URL ZOOM_IN_ICON_URL = ReportViewerResources.class.getResource("resources/zoom_in.png");
    public static final URL ZOOM_OUT_ICON_URL = ReportViewerResources.class.getResource("resources/zoom_out.png");

    //
    public static ImageIcon getImageIcon(URL url) {
        ImageIcon icon;
        try {
            icon = new ImageIcon(new ImageIcon(url).getImage().getScaledInstance(20, 20, Image.SCALE_SMOOTH));
        } catch (Exception e) {
            icon = null;
        }

        return icon;
    }
}
