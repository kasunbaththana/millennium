/*
 *  ReportViewer.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 27, 2015, 10:55:53 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zreport.report_viewer;

import java.io.InputStream;
import java.util.Locale;
import java.util.ResourceBundle;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.swing.JRViewerToolbar;

/**
 *
 * @author mohan
 */
public class ReportViewer extends JRViewer {

    //CREATE REPORT WHICH SUPPORT TO REFRESH
    public ReportViewer(String fileName, boolean isXML) throws JRException {
        super(fileName, isXML);
    }

    public ReportViewer(InputStream is, boolean isXML) throws JRException {
        super(is, isXML);
    }

    //OVERRIDES
    @Override
    protected void initViewerContext(JasperReportsContext jasperReportsContext, Locale locale, ResourceBundle resBundle) {
        viewerContext = new ReportViewerController(jasperReportsContext, locale, resBundle);
        setLocale(viewerContext.getLocale());
        viewerContext.addListener(this);
    }

    @Override
    protected JRViewerToolbar createToolbar() {
        return new ReportViewerToolBar(viewerContext);
    }
}
