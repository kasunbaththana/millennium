/*
 *  ReportViewerToolBar.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 27, 2015, 11:01:51 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zreport.report_viewer;

import net.sf.jasperreports.swing.JRViewerController;
import net.sf.jasperreports.swing.JRViewerToolbar;
import static com.mac.zreport.report_viewer.ReportViewerResources.*;
import javax.swing.AbstractButton;

/**
 *
 * @author mohan
 */
public class ReportViewerToolBar extends JRViewerToolbar {

    public ReportViewerToolBar(JRViewerController viewerContext) {
        super(viewerContext);

        setButtonIcons();
    }

    private void setButtonIcons() {
        AbstractButton[] buttons = new AbstractButton[]{
            btnSave,
            btnPrint,
            btnReload,
            btnFirst,
            btnPrevious,
            btnNext,
            btnLast,
            btnActualSize,
            btnFitPage,
            btnFitWidth,
            btnZoomIn,
            btnZoomOut
        };
        
        for (AbstractButton abstractButton : buttons) {
            stylizeButton(abstractButton);
        }
        
        btnSave.setIcon(ReportViewerResources.getImageIcon(SAVE_ICON_URL));
        btnPrint.setIcon(ReportViewerResources.getImageIcon(PRINT_ICON_URL));
        btnReload.setIcon(ReportViewerResources.getImageIcon(REFRESH_ICON_URL));

        btnFirst.setIcon(ReportViewerResources.getImageIcon(FIRST_ICON_URL));
        btnPrevious.setIcon(ReportViewerResources.getImageIcon(BACK_ICON_URL));
        btnNext.setIcon(ReportViewerResources.getImageIcon(NEXT_ICON_URL));
        btnLast.setIcon(ReportViewerResources.getImageIcon(LAST_ICON_URL));

        btnActualSize.setIcon(ReportViewerResources.getImageIcon(FIT_PAGE_ICON_URL));
        btnFitPage.setIcon(ReportViewerResources.getImageIcon(FIT_HEIGHT_ICON_URL));
        btnFitWidth.setIcon(ReportViewerResources.getImageIcon(FIT_WIDTH_ICON_URL));

        btnZoomIn.setIcon(ReportViewerResources.getImageIcon(ZOOM_IN_ICON_URL));
        btnZoomOut.setIcon(ReportViewerResources.getImageIcon(ZOOM_OUT_ICON_URL));
    }

    private void stylizeButton(AbstractButton button) {
        button.setPreferredSize(new java.awt.Dimension(25, 25));
    }
}
