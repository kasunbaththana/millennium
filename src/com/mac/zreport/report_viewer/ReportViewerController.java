/*
 *  ReportViewerController.java
 *  
 *  @author Channa Mohan
 *     hjchanna@gmail.com
 *  
 *  Created on Jan 27, 2015, 11:45:56 AM
 *  Copyrights channa mohan, All rights reserved.
 *  
 */
package com.mac.zreport.report_viewer;

import com.mac.zreport.ReportBuilder;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.swing.JRViewerController;
import net.sf.jasperreports.swing.JRViewerEvent;

/**
 *
 * @author mohan
 */
public class ReportViewerController extends JRViewerController {

    public ReportViewerController(JasperReportsContext jasperReportsContext, Locale locale, ResourceBundle resBundle) {
        super(jasperReportsContext, locale, resBundle);
    }

    private JasperPrint createJasperPrint(String path) {
        JasperPrint jasperPrint = null;
        try {
            jasperPrint = ReportBuilder.buidReport(path);
        } catch (JRException ex) {
            Logger.getLogger(ReportViewerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ReportViewerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportViewerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jasperPrint;
    }
    
    private JasperPrint createJasperPrint(InputStream inputStream) {
        JasperPrint jasperPrint = null;
        try {
            jasperPrint = ReportBuilder.buidReport(inputStream);
        } catch (JRException ex) {
            Logger.getLogger(ReportViewerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ReportViewerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jasperPrint;
    }

    //OVERRIDES
    @Override
    public void loadReport(String fileName, boolean isXmlReport) throws JRException {
//        super.loadReport(fileName, isXmlReport); //To change body of generated methods, choose Tools | Templates.
/*
         if (isXmlReport) {
         jasperPrint = JRPrintXmlLoader.loadFromFile(jasperReportsContext, fileName);
         } else {
         jasperPrint = (JasperPrint) JRLoader.loadObjectFromFile(fileName);
         }

         type = TYPE_FILE_NAME;
         this.isXML = isXmlReport;
         reportFileName = fileName;

         SimpleFileResolver fileResolver = new SimpleFileResolver(Arrays.asList(new File[]{new File(fileName).getParentFile(), new File(".")}));
         fileResolver.setResolveAbsolutePath(true);
         if (localJasperReportsContext == null) {
         localJasperReportsContext = new LocalJasperReportsContext(jasperReportsContext);
         jasperReportsContext = localJasperReportsContext;
         }
         localJasperReportsContext.setFileResolver(fileResolver);

         reloadSupported = true;
         fireListeners(JRViewerEvent.EVENT_REPORT_LOADED);
         setPageIndex(0);
         */




        loadReport(createJasperPrint(fileName));
        type = TYPE_FILE_NAME;
        this.isXML = isXmlReport;
        reportFileName = fileName;
        reloadSupported = true;
        fireListeners(JRViewerEvent.EVENT_REPORT_LOADED);
    }

    @Override
    public void loadReport(InputStream is, boolean isXmlReport) throws JRException {
//        super.loadReport(is, isXmlReport); 

        loadReport(createJasperPrint(is));
        type = TYPE_INPUT_STREAM;
        this.isXML = isXmlReport;
        reloadSupported = true;
        fireListeners(JRViewerEvent.EVENT_REPORT_LOADED);
        setPageIndex(0);
    }
}
